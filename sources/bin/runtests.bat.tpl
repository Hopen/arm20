echo setup environment...
call qtenv.bat
set PATH=%PATH%;%SystemRoot%\System32\Wbem
set ROUTER_HOST=127.0.0.1
set ROUTER_PORT=50000

echo starting unit tests...
call testsapp.exe -maxwarnings 0
pause
