import Qt 4.7

Rectangle {
    id:gauge
    width: 100
    height: width
    property real valueAngle : 0
    property real minAngle: 0
    property real maxAngle: 360
    property real normalAngleSpan: 30
    property real aboveNormalAngleSpan: 210
    property bool inverted: false
    property alias scaleModel: scaleView.model

    Item {
    anchors.centerIn:parent
    width: Math.min(parent.width,parent.height)
    height: width
    Image {
        id: gaugeBackground
        anchors.fill: parent
        source: "gauge_background.png"
        smooth: true
    }

    GaugeThreshold {
        id: severityColors;
        visible: gauge.normalAngleSpan!=0 && gauge.aboveNormalAngleSpan!=0
        anchors.centerIn: parent
        width: parent.width*0.40
        height: width
        sliceWidth:width*0.08
        fromAngle: minAngle
        toAngle: maxAngle
        normalAngleSpan: gauge.normalAngleSpan
        aboveNormalAngleSpan: gauge.aboveNormalAngleSpan
        inverted: gauge.inverted
    }

    GaugeScale {

        id: scaleView
        anchors.centerIn: parent
        radius: parent.width/2*0.6
    }

    Image {
        id:needleShadow
        height: needle.height
        anchors.bottom: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.horizontalCenterOffset:-5
        anchors.verticalCenterOffset:-5
        source: "gauge_needle_shadow.png"
        transform: Rotation {
            origin.x: needleShadow.width/2; origin.y: needleShadow.height
            angle: needleRotation.angle
        }
    }
    Image {
        id: needle
        height: parent.width/2.8
        width: height* 0.1
        anchors.bottom: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        smooth: true

        source: "gauge_needle.png"
        transform: Rotation {
            id: needleRotation
            origin.x: needle.width/2; origin.y: needle.height

            angle: gauge.valueAngle - 180;
            Behavior on angle {
                SpringAnimation {
                    spring: 1.4
                    damping: .15
                }
            }
        }
    }

    Image {
        anchors.centerIn:gaugeBackground
        source: "gauge_central.png"
        scale: gaugeBackground.scale
        width: gauge.width * 0.2
        height: width
    }

    Image {
        anchors.top:gaugeBackground.top
        anchors.left:gaugeBackground.left
        width:gaugeBackground.width*0.9
        height:gaugeBackground.height*0.5
        source: "gauge_overlay.png"
    }
}
}


