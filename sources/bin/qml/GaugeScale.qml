import Qt 4.7

Item {
    id: scaleView

    property real radius: 40;
    property ListModel model: defaultScale

    width: radius*2
    height: radius*2

    ListModel {
        id: defaultScale
        ListElement { label: "0";   angle: 0 }
        ListElement { label: "30";  angle: 30 }
        ListElement { label: "60";  angle: 60 }
        ListElement { label: "90";  angle: 90 }
        ListElement { label: "120"; angle: 120 }
        ListElement { label: "150"; angle: 150 }
        ListElement { label: "180"; angle: 180 }
        ListElement { label: "210"; angle: 210  }
        ListElement { label: "240"; angle: 240  }
        ListElement { label: "270"; angle: 270  }
        ListElement { label: "300"; angle: 300  }
        ListElement { label: "330"; angle: 330  }
    }

    Repeater {
       model: scaleView.model
       delegate: Text {
            property real radians: ((90+angle)*Math.PI*2) /360;
            property real cos: Math.cos(radians)
            property real sin: Math.sin(radians)
            x: scaleView.width/2 + cos * scaleView.radius - width/2
            y: scaleView.height/2 + sin * scaleView.radius - height/2
            text: label
            font.pixelSize: scaleView.width*0.12
        }
    }
}
