import Qt 4.7
import "lib"

PieChart
{
   width:300
   height:300
   property real sliceWidth: width *0.15
   property real gradientAngleSpan: Math.min(10,Math.min(normalAngleSpan, aboveNormalAngleSpan))
   property real fromAngle: 30
   property real toAngle: 330

   property real normalAngleSpan: 100
   property real aboveNormalAngleSpan: 90

   property bool inverted: false

   property color normalValueColor: inverted ? "red" : "green"
   property color aboveNormalValueColor: "yellow"
   property color criticalValueColor: inverted ? "green" : "red"

   slices: [
       ArcSlice {
           id: sliceNN
           width: parent.sliceWidth
           anchors.fill: parent
           startColor: parent.normalValueColor
           endColor: parent.normalValueColor
           fromAngle: -(90 + parent.fromAngle);
           angleSpan: -(parent.normalAngleSpan - parent.gradientAngleSpan + 1 );
       },
       ArcSlice {
           id: sliceNW
           width:parent.sliceWidth
           anchors.fill: parent
           startColor: parent.normalValueColor
           endColor: parent.aboveNormalValueColor
           fromAngle: -(Math.abs(sliceNN.fromAngle) + Math.abs(sliceNN.angleSpan) - 1)
           angleSpan: -(parent.gradientAngleSpan*2 + 1);
       },
       ArcSlice {
           id: sliceWW
           width:parent.sliceWidth
           anchors.fill: parent
           startColor: parent.aboveNormalValueColor
           endColor: parent.aboveNormalValueColor
           fromAngle: -(Math.abs(sliceNW.fromAngle) + Math.abs(sliceNW.angleSpan) - 1)
           angleSpan: -(parent.aboveNormalAngleSpan - parent.gradientAngleSpan + 1);
       },
       ArcSlice {
           id: sliceWC
           width:parent.sliceWidth
           anchors.fill: parent
           startColor: parent.aboveNormalValueColor
           endColor: parent.criticalValueColor
           fromAngle: -(Math.abs(sliceWW.fromAngle) + Math.abs(sliceWW.angleSpan) - 1)
           angleSpan: -(parent.gradientAngleSpan*2 + 1);
       },
       ArcSlice {
           id: sliceCC
           width:parent.sliceWidth
           anchors.fill: parent
           startColor: parent.criticalValueColor
           endColor: parent.criticalValueColor
           fromAngle: -(Math.abs(sliceWC.fromAngle) + Math.abs(sliceWC.angleSpan) - 1)
           angleSpan: -(90 + parent.toAngle - Math.abs(fromAngle))
       }
   ]
}
