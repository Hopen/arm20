import Qt 4.7

Rectangle {
    id: root
    width: 300
    height: 300

    property real unitAngle: (gauge.maxAngle - gauge.minAngle) /
            (indicator.maxValue!=0?indicator.maxValue:1 - indicator.minValue)

    ListModel {
        id: customScale
        ListElement { label: "";   angle: 0 }
    }

    Text {
        anchors.horizontalCenter:parent.horizontalCenter
        pos.y: height/3
        id: title
        text: indicator.name
        font.pixelSize: formattedValue.font.pixelSize
    }

//    Text {
//        id: xxx
//        text: indicator.warningValue + "="+gauge.normalAngleSpan
//    }
//    Text {
//        anchors.top: xxx.bottom
//        text: indicator.criticalValue + "="+gauge.aboveNormalAngleSpan
//    }

    Gauge {
        id:gauge;
        anchors.top: title.bottom
        anchors.topMargin:5
        anchors.horizontalCenter: parent.horizontalCenter
        width: 0.8 * Math.min(parent.width,parent.height)
        height: width
        valueAngle: minAngle + indicator.value * unitAngle
        scaleModel: customScale
        minAngle: 30
        maxAngle: 270
        normalAngleSpan: root.unitAngle * indicator.warningValue
        aboveNormalAngleSpan:  root.unitAngle * (indicator.criticalValue - indicator.warningValue)
        inverted: indicator.isInverted
        function updateScale()
        {
            customScale.clear();
            var valuesCount = 10;
            var valuesStep = (indicator.maxValue - indicator.minValue)/valuesCount;
            var valuesArray = new Array();
            for(var i=0; i<=10; i++)
            {
                var itemValue = Math.round(indicator.minValue + valuesStep*i);
                if(-1==valuesArray.indexOf(itemValue))
                {
                    valuesArray.push(itemValue);
                    customScale.append({"label": itemValue, "angle" : gauge.minAngle + itemValue * unitAngle});
                }
            }
        }

        Component.onCompleted: updateScale();
        Connections {
            target: indicator
            onMaxChanged: gauge.updateScale();
        }
    }

    Item {
        id: infoContainer
        anchors.left: gauge.horizontalCenter
        anchors.top: gauge.verticalCenter
        anchors.leftMargin: -gauge.width*0.1
        width: gauge.width*0.3
        height: gauge.height*0.35

        Text {
            id:formattedValue
            anchors.centerIn: infoContainer
            text: indicator.formattedValue
            font.pixelSize:infoContainer.height*0.3
        }

        property string positiveTrendIcon: indicator.isInverted ? "trend_positive_inverted_24x24.png" : "trend_positive_24x24.png"
        property string negativeTrendIcon: indicator.isInverted ? "trend_negative_inverted_24x24.png" : "trend_negative_24x24.png"
        property string trendIconSource: indicator.trend == 0 ? "" : (indicator.trend == 1 ? positiveTrendIcon : negativeTrendIcon);

        Image {
            anchors.bottom: formattedValue.verticalCenter
            anchors.left: formattedValue.right
            scale: formattedValue.scale
            height: formattedValue.height
            width: height

            id: trendIcon
            source: infoContainer.trendIconSource
        }

    }



}
