QT       -= gui
TEMPLATE = lib
TARGET = ccplugin_qca-ossl
DESTDIR = $$OUT_PWD/../bin

contains( CONFIG, static) {
  CONFIG  += staticlib
  DEFINES += QT_NODLL
  DEFINES += QCA_STATIC
} else {
  CONFIG += plugin
}


INCLUDEPATH += ../

LIBS = -L$$DESTDIR -lappcore

DEFINES += CRYPTOPROVIDER_LIBRARY
INCLUDEPATH += ../thirdparty/qca/include/QtCrypto

HEADERS += cryptoprovider_global.h \
          ../thirdparty/qca/plugins/qca-ossl/qca-ossl.h

SOURCES = ../thirdparty/qca/plugins/qca-ossl/qca-ossl.cpp

win32 {
    INCLUDEPATH += ../thirdparty/openssl/include
    LIBS += -L../thirdparty/openssl/lib
    DEFINES += OSSL_097
    LIBS += -llibeay32 -lssleay32
    LIBS += -lgdi32 -lwsock32
    LIBS += -lcrypt32

    CONFIG(debug,debug|release): LIBS+=-lqcad2
    CONFIG(release,debug|release): LIBS+=-lqca2

    openssl_lib.files = \
        ../thirdparty/openssl/libeay32.dll \
        ../thirdparty/openssl/libssl32.dll \
        ../thirdparty/openssl/ssleay32.dll
    openssl_lib.path=$$DESTDIR
    INSTALLS+=openssl_lib
    PRE_TARGETDEPS += install_openssl_lib

}
else {
    LIBS+=-lqca -lssl
}
