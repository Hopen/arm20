#ifndef CALLSGEN_H
#define CALLSGEN_H

#include <QtGui>
#include "callsgen_global.h"
#include "appcore/iappmodule.h"
#include "operwidgets/ioperwidgetsplugin.h"

using namespace ccappcore;

class CALLSGENSHARED_EXPORT CallsGen
    : public QObject
    , public ccappcore::IAppModule
    , public IOperWidgetsPlugin

{
    Q_OBJECT
    Q_INTERFACES(ccappcore::IAppModule IOperWidgetsPlugin)
public:
    CallsGen();

    virtual void initialize() throw();

    virtual void initializeMainWindow(QMainWindow *);

public slots:
    void showCallsGenForm();
};

#endif // CALLSGEN_H
