# -------------------------------------------------
# Project created by QtCreator 2010-03-13T18:56:16
# -------------------------------------------------
TARGET = ccplugin_callsgen
TEMPLATE = lib
DEFINES += CALLSGEN_LIBRARY
DESTDIR = $$OUT_PWD/../bin

contains( CONFIG, static) {
  CONFIG  += staticlib
  DEFINES += QT_NODLL
  DEFINES += QCA_STATIC
} else {
  CONFIG += plugin
}
INCLUDEPATH += ../

INCLUDEPATH += ../thirdparty/log4qt/src \
    ../thirdparty/psi/src/tools \
    ../thirdparty/cds3

LIBS = -L$$DESTDIR -lappcore -lccplugin_operwidgets
FORMS += callsgenform.ui
RESOURCES += 
SOURCES += callsgen.cpp \
    callsgenform.cpp
HEADERS += callsgen.h \
    callsgen_global.h \
    callsgenform.h
