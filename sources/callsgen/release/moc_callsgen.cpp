/****************************************************************************
** Meta object code from reading C++ file 'callsgen.h'
**
** Created: Tue 26. Mar 14:02:56 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../callsgen.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'callsgen.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CallsGen[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_CallsGen[] = {
    "CallsGen\0\0showCallsGenForm()\0"
};

void CallsGen::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CallsGen *_t = static_cast<CallsGen *>(_o);
        switch (_id) {
        case 0: _t->showCallsGenForm(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData CallsGen::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CallsGen::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_CallsGen,
      qt_meta_data_CallsGen, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CallsGen::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CallsGen::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CallsGen::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CallsGen))
        return static_cast<void*>(const_cast< CallsGen*>(this));
    if (!strcmp(_clname, "ccappcore::IAppModule"))
        return static_cast< ccappcore::IAppModule*>(const_cast< CallsGen*>(this));
    if (!strcmp(_clname, "IOperWidgetsPlugin"))
        return static_cast< IOperWidgetsPlugin*>(const_cast< CallsGen*>(this));
    if (!strcmp(_clname, "ru.forte-it.ccappcore.iappmodule/1.0"))
        return static_cast< ccappcore::IAppModule*>(const_cast< CallsGen*>(this));
    if (!strcmp(_clname, "ru.forte-it.calloper.ioperwidgetsplugin/1.0"))
        return static_cast< IOperWidgetsPlugin*>(const_cast< CallsGen*>(this));
    return QObject::qt_metacast(_clname);
}

int CallsGen::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
