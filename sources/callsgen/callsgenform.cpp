#include "callsgenform.h"
#include "ui_callsgenform.h"

CallsGenForm::CallsGenForm(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::CallsGenForm)
{
    ui->setupUi(this);
}

CallsGenForm::~CallsGenForm()
{
    delete ui;
}

void CallsGenForm::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void CallsGenForm::on_pushButton_clicked()
{
    Message m("OFFER");
    m["A"] = ui->tbANum->text();
    m["B"] = ui->tbBNum->text();
    _routerClient->send(m, ClientAddress::Any());
}
