#ifndef CALLSGENFORM_H
#define CALLSGENFORM_H

#include <QWidget>
#include "appcore/irouterclient.h"
#include "appcore/lateboundobject.h"

namespace Ui {
    class CallsGenForm;
}

using namespace ccappcore;

class CallsGenForm : public QWidget {
    Q_OBJECT
public:
    CallsGenForm(QWidget *parent = 0);
    ~CallsGenForm();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::CallsGenForm *ui;
    LateBoundObject<IRouterClient> _routerClient;

private slots:
    void on_pushButton_clicked();
};

#endif // CALLSGENFORM_H
