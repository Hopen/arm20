/********************************************************************************
** Form generated from reading UI file 'callsgenform.ui'
**
** Created: Tue 26. Mar 14:02:50 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CALLSGENFORM_H
#define UI_CALLSGENFORM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFormLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSplitter>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CallsGenForm
{
public:
    QWidget *layoutWidget;
    QFormLayout *formLayout;
    QSplitter *splitter;
    QLabel *label;
    QLineEdit *tbANum;
    QSplitter *splitter_2;
    QLabel *label_2;
    QLineEdit *tbBNum;
    QPushButton *pushButton;

    void setupUi(QWidget *CallsGenForm)
    {
        if (CallsGenForm->objectName().isEmpty())
            CallsGenForm->setObjectName(QString::fromUtf8("CallsGenForm"));
        CallsGenForm->resize(297, 88);
        layoutWidget = new QWidget(CallsGenForm);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(20, 20, 261, 51));
        formLayout = new QFormLayout(layoutWidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        splitter = new QSplitter(layoutWidget);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        label = new QLabel(splitter);
        label->setObjectName(QString::fromUtf8("label"));
        splitter->addWidget(label);
        tbANum = new QLineEdit(splitter);
        tbANum->setObjectName(QString::fromUtf8("tbANum"));
        splitter->addWidget(tbANum);

        formLayout->setWidget(0, QFormLayout::LabelRole, splitter);

        splitter_2 = new QSplitter(layoutWidget);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setOrientation(Qt::Horizontal);
        label_2 = new QLabel(splitter_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        splitter_2->addWidget(label_2);
        tbBNum = new QLineEdit(splitter_2);
        tbBNum->setObjectName(QString::fromUtf8("tbBNum"));
        splitter_2->addWidget(tbBNum);

        formLayout->setWidget(1, QFormLayout::LabelRole, splitter_2);

        pushButton = new QPushButton(layoutWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        formLayout->setWidget(1, QFormLayout::FieldRole, pushButton);


        retranslateUi(CallsGenForm);

        QMetaObject::connectSlotsByName(CallsGenForm);
    } // setupUi

    void retranslateUi(QWidget *CallsGenForm)
    {
        CallsGenForm->setWindowTitle(QApplication::translate("CallsGenForm", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("CallsGenForm", "\320\220 \320\275\320\276\320\274\320\265\321\200", 0, QApplication::UnicodeUTF8));
        tbANum->setText(QApplication::translate("CallsGenForm", "4950000000", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("CallsGenForm", "\320\221 \320\275\320\276\320\274\320\265\321\200", 0, QApplication::UnicodeUTF8));
        tbBNum->setText(QApplication::translate("CallsGenForm", "100", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("CallsGenForm", "OFFER", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class CallsGenForm: public Ui_CallsGenForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CALLSGENFORM_H
