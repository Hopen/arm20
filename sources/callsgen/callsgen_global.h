#ifndef CALLSGEN_GLOBAL_H
#define CALLSGEN_GLOBAL_H

#include <QtCore/qglobal.h>

#ifndef QT_NODLL
#  if defined(CALLSGEN_LIBRARY)
#    define CALLSGENSHARED_EXPORT Q_DECL_EXPORT
#  else
#    define CALLSGENSHARED_EXPORT Q_DECL_IMPORT
#  endif
#else
#  define CALLSGENSHARED_EXPORT
#endif



#endif // CALLSGEN_GLOBAL_H
