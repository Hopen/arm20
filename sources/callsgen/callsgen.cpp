#include "callsgen.h"
#include "appcore/qobjectutils.h"
#include "callsgenform.h"

#ifndef QT_NODLL
Q_EXPORT_PLUGIN2(pnp_callsgen, CallsGen);
#endif

CallsGen::CallsGen()
{
}

void CallsGen::initialize() throw()
{
    ServiceProvider::instance().registerService(this);
}

void CallsGen::initializeMainWindow(QMainWindow *mainWindow)
{
    QMenuBar* menu = mainWindow->menuBar();
    if(!menu)
        return;

    QList<QAction*> actions = menu->actions();
    QAction* actionHelp = qActionFind(actions, "menuHelp");
    if(!actionHelp)
        return;

    QMenu* menuHelp = actionHelp->menu();
    if(!menuHelp)
        return;

    menuHelp->addAction(tr("Генератор звонков"), this, SLOT(showCallsGenForm()));
}

void CallsGen::showCallsGenForm()
{
    CallsGenForm* frm = new CallsGenForm();
    frm->setAttribute(Qt::WA_DeleteOnClose);
    frm->show();
}
