#ifndef VOIPSETTINGS_H
#define VOIPSETTINGS_H

#include "openphone_global.h"
#include "appcore/appsettings.h"

using namespace ccappcore;

struct CodecName
{
    static const char* G711a;
    static const char* G711m;
    static const char* G729;
};

class VoIPSettings : public QObject,
    public IAppSettingsHandler
{
Q_OBJECT
    Q_INTERFACES(ccappcore::IAppSettingsHandler)
public:
    explicit VoIPSettings(QObject *parent = 0);

    void resetSettings();

    bool isEnabled() const { return _enabled; }
    //bool isAutoIP() const { return _autoIp; }
    QStringList codecs() const { return _mapCodecFrames.keys(); }
    int codecFrames(QString codecName) const { return _mapCodecFrames[codecName]; }
    const QString& moduleName() const { return _moduleName; }

    int soundBuffers() const { return _soundBuffers; }

    bool silenceDetect() const { return _silenceDetect; }
    bool noFastStart() const { return _noFastStart; }
    bool noTunnelling() const { return _noTunnelling; }
    bool noH245InSetup() const { return _noH245InSetup; }

    //const QString& registrarDomain()const { return _registrarDomain;}
    //QString detectValidIpAddress(); move to opt_application

public://IAppSettingsHandler

    //Apply service configuration.
    virtual void applySettings(const AppSettings&);

    //Flush service configuration
    virtual void flushSettings(AppSettings&);

signals:

public slots:

private:

    bool _enabled;
    //bool _autoIp;
    QMap<QString,int> _mapCodecFrames;
    QString _moduleName;
    QString _ipAddress;
    int _soundBuffers;
    bool _silenceDetect;
    bool _noFastStart;
    bool _noTunnelling;
    bool _noH245InSetup;
    //QString _registrarDomain;

};

#endif // VOIPSETTINGS_H
