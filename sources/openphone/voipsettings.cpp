#include "voipsettings.h"
#include <QNetworkInterface>
#include <QtAlgorithms>

using namespace ccappcore;

const char* CodecName::G711a = "G711a";
const char* CodecName::G711m = "G711m";
const char* CodecName::G729 = "G729";



VoIPSettings::VoIPSettings(QObject *parent) :
    QObject(parent)
{
    resetSettings();
}

void VoIPSettings::resetSettings()
{
    _enabled = true;
    //_autoIp  = false;

    _mapCodecFrames[CodecName::G711a] = 30;
    _mapCodecFrames[CodecName::G711m] = 30;
    _mapCodecFrames[CodecName::G729] = 6;

#if defined( Q_WS_WIN)
    _moduleName = "openphone/openphone.exe";
#else
    _moduleName = "openphone/openphone";
#endif
    _soundBuffers = 3;
    _silenceDetect = true;
    _noFastStart = false;
    _noTunnelling = false;
    _noH245InSetup = false;
}

void VoIPSettings::applySettings(const AppSettings& settings)
{
    //_enabled = settings.getOption("voip/enabled", _enabled).toBool();
    _enabled = (CT_VOIP == settings.getOption("connecting/type", CT_TEL).toInt()) ;
    //_autoIp  = settings.getOption("connecting/autoIP", _autoIp).toBool();
    _soundBuffers = settings.getOption("voip/audiodevice.soundBuffers", _soundBuffers).toInt();
    _moduleName = settings.getOption("voip/general.module", _moduleName).toString();
    _ipAddress = settings.getOption("voip/general.ipaddress", _ipAddress).toString();
    _silenceDetect = settings.getOption("voip/general.silenceDetection", _silenceDetect).toBool();
    _noFastStart = settings.getOption("voip/general.noFastStart", _noFastStart).toBool();
    _noTunnelling = settings.getOption("voip/general.noTunnelling", _noTunnelling).toBool();
    _noH245InSetup = settings.getOption("voip/general.noH245InSetup", _noH245InSetup).toBool();
    //_registrarDomain = settings.getOption("voip/RegistrarDomain", _registrarDomain).toString();

    QString codecsString = settings.getOption("voip/codecs.names").toString();
    if(codecsString.isEmpty())
        return;
    QStringList codecList = codecsString.split(",");
    foreach(QString codec, codecList)
    {
        QString path = "voip/codecs." + codec;
        int frames = settings.getOption(path+".frames").toInt();
        _mapCodecFrames[codec] = frames;
    }

};

void VoIPSettings::flushSettings(AppSettings& settings)
{
    QMapIterator<QString,int> it(_mapCodecFrames);

    QString codecNames = QStringList(_mapCodecFrames.keys()).join(",");
    settings.setAppOption("voip/codecs.names", codecNames );
    while(it.hasNext())
    {
        it.next();
        QString path = "voip/codecs." + it.key();
        settings.setAppOption( path+".frames", it.value() );
    }

    //settings.setAppOption("voip/enabled", _enabled);
    settings.setAppOption("voip/audiodevice.soundBuffers", _soundBuffers);
    //settings.setAppOption("voip/general.module", _moduleName);
    settings.setAppOption("voip/general.ipaddress", _ipAddress);
    settings.setAppOption("voip/general.silenceDetection", _silenceDetect);
    settings.setAppOption("voip/general.noFastStart", _noFastStart);
    settings.setAppOption("voip/general.noTunnelling", _noTunnelling);
    settings.setAppOption("voip/general.noH245InSetup", _noH245InSetup);
    //settings.setAppOption("voip/RegistrarDomain", _registrarDomain);
}

//static bool sortByIndex(const QNetworkInterface& i1, const QNetworkInterface& i2)
//{
//    return i1.index()<i2.index();
//}
//static bool sortByMask(const QNetworkAddressEntry& i1, const QNetworkAddressEntry& i2)
//{
//    return i1.netmask().toIPv4Address()<i2.netmask().toIPv4Address();
//}

//QString VoIPSettings::detectValidIpAddress()
//{
//    QList<QHostAddress>	allAddresses = QNetworkInterface::allAddresses();
//    if( !_ipAddress.isEmpty() && allAddresses.contains(QHostAddress(_ipAddress)))
//        return _ipAddress;

//    _ipAddress = QString();
//    QList<QNetworkInterface> allInterfaces = QNetworkInterface::allInterfaces();
//    qSort(allInterfaces.begin(),allInterfaces.end(),&sortByIndex);
//    QNetworkInterface netInterface;
//    qDebug()<<"VoIP: Detecting IP address. Network interface list:";
//    foreach( QNetworkInterface curr, allInterfaces )
//    {
//        qDebug()<<curr;
//        bool isUp= curr.flags().testFlag(QNetworkInterface::IsUp);
//        bool isLoopback = curr.flags().testFlag(QNetworkInterface::IsLoopBack);
//        bool hasEntries = curr.addressEntries().count()>0;
//        if( isUp && !isLoopback && hasEntries)
//        {
//            if(!netInterface.isValid())
//                netInterface = curr;
//        }
//    }
//    qDebug()<<"VoIP: Selected network interface:"<<netInterface;

//    QList<QNetworkAddressEntry> entryList = netInterface.addressEntries();
//    qSort(entryList.begin(),entryList.end(),&sortByMask);
//    QList<QString> addrList;
//    foreach(QNetworkAddressEntry addr, entryList)
//    {
//        if( addr.ip().protocol() == QAbstractSocket::IPv4Protocol )
//        {
//            addrList.append(addr.ip().toString());
//        }
//    }

//    if(addrList.count()>0)
//        _ipAddress = addrList[0];

//    if(_ipAddress.isEmpty() && !allAddresses.isEmpty())
//        _ipAddress = allAddresses[0].toString();
//    return _ipAddress;
//}
