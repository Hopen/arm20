#include "openphone.h"
#include "voipsettings.h"
#include "appcore/sessioncontroller.h"
//#include "appcore/appsettings.h"
//#include "appcore/statusnotifier.h"
//#include <tlhelp32.h>

#ifndef QT_NODLL
Q_EXPORT_PLUGIN2(pnp_openphoneloader, OpenPhone);
#endif
using namespace ccappcore;

//OpenPhone::OpenPhone()
//    : m_hProcess(NULL)
//{
///*    _process = new QProcess(this);*/
//}

OpenPhone::~OpenPhone()
{
    killProcess();
}

void OpenPhone::initialize() throw()
{
    _voipSettings = new VoIPSettings(this);
    ServiceProvider::instance().registerService(_voipSettings.data());

    LateBoundObject<SessionController> session;
    connect(session.instance(),SIGNAL(sessionStarted()),
            this, SLOT(sessionStarted()));
    connect(session.instance(),SIGNAL(sessionStopped()),
            this, SLOT(sessionStopped()));

//    LateBoundObject<AppSettings> appSettings;
//    _voipSettings->applySettings(*appSettings);

//    if(!_voipSettings->isEnabled())
//        return;

//    initSettings();

//    //if (!_voipSettings->isAutoIP())
//    //{
//        //QString ip = _voipSettings->detectValidIpAddress();
//        //QString addr = "TA:" + ip;
//        //appSettings->setAppOption( "device/number", addr );
//        QString addr = appSettings->getOption("device/number", QString()).toString();
//        session->setDeviceNumber( addr );
//    //}



//    LateBoundObject<StatusNotifier> status;
//    status->postInfo(tr("Инициализация VoIP"));
//    startProcess();

/*    connect(_process,SIGNAL(started()),
            this, SLOT (openPhoneStarted()));
    connect(_process,SIGNAL(error(QProcess::ProcessError)),
            this, SLOT (openPhoneProcessingError(QProcess::ProcessError)));
    connect(_process, SIGNAL(finished(int,QProcess::ExitStatus)),
            this, SLOT(openPhoneFinished(int,QProcess::ExitStatus)));*/
}

void OpenPhone::sessionStopped()
{
    if(!_voipSettings->isEnabled())
        return;

    SessionController* session = qobject_cast<SessionController*>(sender());
    Q_ASSERT(session);

    killProcess();
}

void OpenPhone::sessionStarted()
{
    if(!_voipSettings->isEnabled())
        return;

    initSettings();

    LateBoundObject<AppSettings> appSettings;
    _voipSettings->applySettings(*appSettings);

    LateBoundObject<SessionController> session;
    QString addr = appSettings->getOption("device/number", QString()).toString();
    session->setDeviceNumber( addr );

    LateBoundObject<StatusNotifier> status;
    status->postInfo(tr("Инициализация VoIP"));
    startProcess();
}


#if defined(Q_OS_UNIX)

static QString getPath()
{
    QString homePath = QDir::homePath();
    QString userPath;
    char *env = getenv("XDG_CONFIG_HOME");
    if (env == 0)
    {
        userPath = homePath;
        userPath += QLatin1Char('/');
        userPath += QLatin1String(".config");
    }
    else if (*env == '/')
    {
        userPath = QFile::decodeName(env);
    }
    else
    {
        userPath = homePath;
        userPath += QLatin1Char('/');
        userPath += QFile::decodeName(env);
    }

    userPath += QLatin1Char('/');

    return userPath;
}


class SimpleIniWriter
{
    struct TToken
    {
        TToken(const QString& token, const QString& value)
            : _token(token), _value(value)
        {}
        QString _token;
        QString _value;
    };

public:
    SimpleIniWriter(const QString& fileName):_fileName(fileName)
    {

    }

    ~SimpleIniWriter()
    {
        QFile file(_fileName);
        file.open(QIODevice::WriteOnly);
        save(&file);
    }

    void push(const QString& key, const QString& token, const QString& value)
    {
        _tokens.insertMulti(key,TToken(token,value));
        //_values[token] = value;
    }

private:

    void save(QIODevice* ioDevice)
    {
        QTextStream stream(ioDevice);
        stream.setCodec("UTF-8");

        KeysContainer keys = _tokens.uniqueKeys();
        foreach(const QString& key, keys)
        {
            TokensContainer tokens = _tokens.values(key);
            stream << "[" << key << "]" << endl;
            foreach(const TToken& token, tokens)
            {
                stream << token._token << "=" << token._value << endl;
            }

            stream << endl;
        }

        stream.flush();
    }

    QString _fileName;
    typedef QMultiMap <QString, TToken> IniContainer;
    typedef QList <QString> KeysContainer;
    typedef QList <TToken> TokensContainer;
    //typedef QMap <QString, QString> ValuesContainer;

    IniContainer    _tokens;
    //ValuesContainer _values;
};

#endif

void OpenPhone::initSettings()
{
        qDebug()<<"VoIP: Loading settings...";

#if defined(Q_OS_UNIX)
        QString userPath = getPath() + "forte-it/";

        SimpleIniWriter conf(userPath+"openphone.conf");

        conf.push("General"               , "AutoAnswer", "1");
        conf.push("Audio"                 , "MicrophoneVolume", "99");
        conf.push("Audio"                 , "SpeakerVolume", "99");
        conf.push("CurrentVersion/Options","SilenceDetect" , _voipSettings->silenceDetect()?"true":"false");
        conf.push("CurrentVersion/Options","SoundBuffers"  , QString("%1").arg(_voipSettings->soundBuffers()));
        conf.push("CurrentVersion/Options","NoFastStart"   , _voipSettings->noFastStart()?"true":"false");
        conf.push("CurrentVersion/Options","NoTunneling"   , _voipSettings->noTunnelling()?"true":"false");
        conf.push("CurrentVersion/Options","NoH245inSetup" , _voipSettings->noH245InSetup()?"true":"false");
        conf.push("CurrentVersion/Options","AutoAnswer"    , "1");
        conf.push("Appearance"            ,"ActiveView"    , "3");

        conf.push("Tracing"            ,"TraceOptions"        , "12415");
        conf.push("Tracing"            ,"TraceFileName"       , "/Home/openphone.log");
        conf.push("Tracing"            ,"TraceLevelThreshold" , "6");
        conf.push("Tracing"            ,"EnableTracing"       , "1");

        QStringList codecList = _voipSettings->codecs();
        for( int i = 0; i < codecList.count() ; i++ )
        {
            QString codecName=codecList[i];
            int codecFrames=_voipSettings->codecFrames(codecName);

            QString codecInfo;

            if (codecName == CodecName::G711a)
            {
                codecInfo = QString("G.711-ALaw-64k{sw} (On) [%1]").arg(codecFrames);
            }
            else if (codecName == CodecName::G711m)
            {
                codecInfo = QString("G.711-uLaw-64k{sw} (On) [%1]").arg(codecFrames);
            }
            else if (codecName == CodecName::G729)
            {
                codecInfo = QString("G.729{sw} (On) [%1]").arg(codecFrames);
            }
            else
            {
                codecInfo = QString("%1{sw} (On) [%2]").arg(codecName).arg(codecFrames);
            }

            qDebug()<<"VoIP: "<<codecInfo;
            conf.push("CurrentVersion/Codecs", QString("Codec%1").arg(i+1), codecInfo);
        }

        qDebug()<<"VoIP: Settings loaded successfully.";

        qDebug()<<"VoIP openphone 3.10.x: Loading settings...";

        LateBoundObject<SessionController> session;

        LateBoundObject<AppSettings> appSettings;
        QString registrarDomain = appSettings->getOption("voip/RegistrarDomain", QString()).toString();


        conf.push("SIP/Registrars/0001", "RegistrarUsername" , session->login());
        conf.push("SIP/Registrars/0001", "RegistrarPassword" , session->password());
        conf.push("SIP/Registrars/0001", "RegistrarDomain"   , registrarDomain );
        conf.push("SIP/Registrars/0001", "RegistrarUsed"     , "1");
#else


        QSettings settings(QSettings::NativeFormat,QSettings::UserScope,
                           "Vox Lucida", "Open Phone");

//        QSettings settings(QSettings::NativeFormat,QSettings::UserScope,
//                           "Equivalence", "OpenPhone");

        settings.beginGroup("CurrentVersion");
        settings.beginGroup("Options");

        settings.setValue("SilenceDetect", _voipSettings->silenceDetect() );
        settings.setValue("SoundBuffers", _voipSettings->soundBuffers() );
        settings.setValue("NoFastStart", _voipSettings->noFastStart() );

        settings.setValue("NoTunneling", _voipSettings->noTunnelling());
        settings.setValue("NoH245inSetup", _voipSettings->noH245InSetup());

        settings.setValue("AutoAnswer", 1);

        settings.endGroup();
        settings.endGroup();

        settings.beginGroup("CurrentVersion");
        settings.beginGroup("Codecs");

        QStringList codecList = _voipSettings->codecs();
        for( int i = 0; i < codecList.count() ; i++ )
        {
            QString codecName=codecList[i];
            int codecFrames=_voipSettings->codecFrames(codecName);

            QString codecInfo;

            if (codecName == CodecName::G711a)
            {
                codecInfo = QString("G.711-ALaw-64k{sw} (On) [%1]").arg(codecFrames);
            }
            else if (codecName == CodecName::G711m)
            {
                codecInfo = QString("G.711-uLaw-64k{sw} (On) [%1]").arg(codecFrames);
            }
            else if (codecName == CodecName::G729)
            {
                codecInfo = QString("G.729{sw} (On) [%1]").arg(codecFrames);
            }
            else
            {
                codecInfo = QString("%1{sw} (On) [%2]").arg(codecName).arg(codecFrames);
            }

            qDebug()<<"VoIP: "<<codecInfo;
            settings.setValue(QString("Codec%1").arg(i+1), codecInfo);
        }

        settings.endGroup();
        settings.endGroup();

        qDebug()<<"VoIP: Settings loaded successfully.";

        qDebug()<<"VoIP openphone 3.10.x: Loading settings...";
        //QSettings settings2(QSettings::NativeFormat,QSettings::UserScope,
        //                   "Vox Lucida", "Open Phone");

        settings.beginGroup("SIP");
        settings.beginGroup("Registrars");
        settings.beginGroup("0001");

        LateBoundObject<AppSettings> appSettings;
        QString registrarDomain = appSettings->getOption("voip/RegistrarDomain", QString()).toString();

        LateBoundObject<SessionController> session;
        settings.setValue("RegistrarUsername", session->login() );
        settings.setValue("RegistrarPassword", session->password() );
        settings.setValue("RegistrarDomain", registrarDomain/*_voipSettings->registrarDomain()*/ );
        settings.setValue("RegistrarUsed", 1 );

        settings.endGroup();
        settings.endGroup();
        settings.endGroup();

#endif
        qDebug()<<"VoIP openphone 3.10.x: Settings loaded successfully.";

}

//void OpenPhone::startProcess()
//{
//    LateBoundObject<StatusNotifier> status;
//    status->postInfo(tr("Запуск VoIP модуля."));
//    killProcess();
//    qDebug()<<"Starting VoIP module...";
///*    _process->start(_voipSettings->moduleName(), QStringList() << "-d");*/

//    STARTUPINFO si = {};
//    si.cb = sizeof(si);
//    si.dwFlags = STARTF_USESHOWWINDOW;
//    si.wShowWindow = SW_HIDE;//SW_NORMAL;
//    PROCESS_INFORMATION pi = {};
//    //WCHAR appl[] = L"D:/Projects/new/trunk/sources/openphone/OpenPhone/openphone.exe";
//    //std::wstring ss(appl);
//    BOOL bRes = CreateProcess(NULL, /*appl*/ (WCHAR*)_voipSettings->moduleName().utf16(), NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
//    if (bRes)
//    {
//      qDebug()<<"VoIP. VoIP module started successfully.";
//      m_hProcess = pi.hProcess;
//    }
//    else
//      qDebug()<<"VoIP. Failed to start VoIP module. Last error = "<<GetLastError();
//}

//void OpenPhone::killProcess()
//{
///*    if (!_process || (_process->state() != QProcess::Running) )
//        return;


//    _process->close();
//    _process->waitForFinished(3000);*/
//    if(!m_hProcess)
//        m_hProcess = FindProcess(_voipSettings->moduleName().toAscii());
//    if(!m_hProcess)
//        return;

//    TerminateProcess(m_hProcess, 0);
//    ::WaitForSingleObject(m_hProcess,3000);
//    CloseHandle(m_hProcess);
//    m_hProcess = NULL;

//    qDebug()<<"VoIP module stopped.";
//}

//HANDLE OpenPhone::FindProcess(const char* cszProcessName)
//{
//    HANDLE            hSnapShot=NULL;
//    HANDLE            hProcess = NULL;
//    PROCESSENTRY32    processInfo;

//    ::ZeroMemory(&processInfo, sizeof(PROCESSENTRY32));
//    processInfo.dwSize = sizeof(PROCESSENTRY32);
//    hSnapShot = CreateToolhelp32Snapshot(
//        TH32CS_SNAPPROCESS, 0);
//    if(hSnapShot == INVALID_HANDLE_VALUE)
//        return NULL;

//    bool bFirst = true;
//    while((bFirst ? Process32First(hSnapShot,
//        &processInfo) : Process32Next(hSnapShot,
//        &processInfo)))
//    {
//        bFirst = false;
//        QString processName(cszProcessName);
//        QString exeName( QString::fromWCharArray(processInfo.szExeFile) );

//        if(processName.endsWith(exeName, Qt::CaseInsensitive))
//        {
//            hProcess=OpenProcess(
//                SYNCHRONIZE|PROCESS_TERMINATE, FALSE,
//                processInfo.th32ProcessID);

//            return hProcess;
//        }
//    }

//    if(hSnapShot)
//        CloseHandle(hSnapShot);
//    return hProcess;
//}


/*void OpenPhone::openPhoneStarted()
{
    int test = 0;
    qDebug()<<"VoIP. VoIP module started successfully.";
}

void OpenPhone::openPhoneProcessingError(QProcess::ProcessError error)
{
    int test = 0;
    qDebug()<<"VoIP. Failed to start VoIP module. Last error = "<< error;
}

void OpenPhone::openPhoneFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    int test = 0;
    qDebug()<<"VoIP module stopped.";
}*/
