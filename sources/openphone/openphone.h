#ifndef OPENPHONE_H
#define OPENPHONE_H
#include "qglobal.h"

#if defined( Q_WS_WIN)
#   include "openphone_win.h"
#else
#   include "openphone_nix.h"
#endif

#endif // OPENPHONE_H
