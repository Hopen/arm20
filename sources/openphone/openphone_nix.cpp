#include <QApplication>
#include "openphone.h"
#include "voipsettings.h"
#include "appcore/statusnotifier.h"
#include "appcore/appsettings.h"
#include "appcore/appsettings.h"


#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <unistd.h>



using namespace ccappcore;

OpenPhone::OpenPhone(): _pid(0)
{
    _process = new QProcess(this);
}

void OpenPhone::startProcess()
{
    LateBoundObject<StatusNotifier> status;
    status->postInfo(tr("Запуск VoIP модуля."));
    killProcess();
    qDebug()<<"Starting VoIP module...";

////    int pid = fork();
////    if(0 == pid)
////        pid = fork();

////    if(0 == pid)
////    {
////        qDebug()<<"VoIP. VoIP module started successfully.";
////        _pid = pid;
//    QString path = QApplication::applicationDirPath() + tr("/") + _voipSettings->moduleName();
//        //int stat = execl(/*_voipSettings->moduleName()*/path.toAscii(),0,0);
//    int stat = system(path.toAscii());
//        int test = 0;
////    }
////    else
////    {
////        qDebug()<<"VoIP. Failed to create new proccedd to run VoIP module. Last error = " << pid;
////    }

    _process->start(_voipSettings->moduleName()/*, QStringList() << "-hide"*/);

//    STARTUPINFO si = {};
//    si.cb = sizeof(si);
//    si.dwFlags = STARTF_USESHOWWINDOW;
//    si.wShowWindow = SW_HIDE;//SW_NORMAL;
//    PROCESS_INFORMATION pi = {};
//    //WCHAR appl[] = L"D:/Projects/new/trunk/sources/openphone/OpenPhone/openphone.exe";
//    //std::wstring ss(appl);
//    BOOL bRes = CreateProcess(NULL, /*appl*/ (WCHAR*)_voipSettings->moduleName().utf16(), NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
//    if (bRes)
//    {
//      qDebug()<<"VoIP. VoIP module started successfully.";
//      m_hProcess = pi.hProcess;
//    }
//    else
//      qDebug()<<"VoIP. Failed to start VoIP module. Last error = "<<GetLastError();
}

void OpenPhone::killProcess()
{
    if (!_process || (_process->state() != QProcess::Running) )
        return;


    _process->close();
    _process->waitForFinished(3000);

////    if(!m_hProcess)
////        m_hProcess = FindProcess(_voipSettings->moduleName().toAscii());
////    if(!m_hProcess)
////        return;

////    TerminateProcess(m_hProcess, 0);
////    ::WaitForSingleObject(m_hProcess,3000);
////    CloseHandle(m_hProcess);
////    m_hProcess = NULL;

////    if ( _pid <=0 )
////        _pid = FindProcess(_voipSettings->moduleName().toAscii());
//    if ( _pid <=0 )
//        return;

//    kill(_pid, 0);

    qDebug()<<"VoIP module stopped.";
}

//int OpenPhone::FindProcess(const char* cszProcessName)
//{
////    HANDLE            hSnapShot=NULL;
////    HANDLE            hProcess = NULL;
////    PROCESSENTRY32    processInfo;

////    ::ZeroMemory(&processInfo, sizeof(PROCESSENTRY32));
////    processInfo.dwSize = sizeof(PROCESSENTRY32);
////    hSnapShot = CreateToolhelp32Snapshot(
////        TH32CS_SNAPPROCESS, 0);
////    if(hSnapShot == INVALID_HANDLE_VALUE)
////        return NULL;

////    bool bFirst = true;
////    while((bFirst ? Process32First(hSnapShot,
////        &processInfo) : Process32Next(hSnapShot,
////        &processInfo)))
////    {
////        bFirst = false;
////        QString processName(cszProcessName);
////        QString exeName( QString::fromWCharArray(processInfo.szExeFile) );

////        if(processName.endsWith(exeName, Qt::CaseInsensitive))
////        {
////            hProcess=OpenProcess(
////                SYNCHRONIZE|PROCESS_TERMINATE, FALSE,
////                processInfo.th32ProcessID);

////            return hProcess;
////        }
////    }

////    if(hSnapShot)
////        CloseHandle(hSnapShot);
////    return hProcess;

//    return pidof(cszProcessName);
//}

void OpenPhone::openPhoneStarted()
{
    int test = 0;
    qDebug()<<"VoIP. VoIP module started successfully.";
}

void OpenPhone::openPhoneProcessingError(QProcess::ProcessError error)
{
    int test = 0;
    qDebug()<<"VoIP. Failed to start VoIP module. Last error = "<< error;
}

void OpenPhone::openPhoneFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    int test = 0;
    qDebug()<<"VoIP module stopped.";
}


