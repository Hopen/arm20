#ifndef OPENPHONE_CCPLUGIN_GLOBAL_H
#define OPENPHONE_CCPLUGIN_GLOBAL_H

#include <QtCore/qglobal.h>
#include <QtCore>

#ifndef QT_NODLL
#  if defined(OPENPHONE_CCPLUGIN_LIBRARY)
#    define OPENPHONE_EXPORT Q_DECL_EXPORT
#  else
#    define OPENPHONE_EXPORT Q_DECL_IMPORT
#  endif
#else
#  define OPENPHONE_EXPORT
#endif

#endif // OPENPHONE_CCPLUGIN_GLOBAL_H
