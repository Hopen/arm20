#ifndef OPENPHONE_WIN_H
#define OPENPHONE_WIN_H

#include "openphone_global.h"
#include "appcore/iappmodule.h"

#include "QProcess"

#include <windows.h>

class VoIPSettings;
class OPENPHONE_EXPORT OpenPhone : public QObject
        , public ccappcore::IAppModule
{

   Q_OBJECT
   Q_INTERFACES(ccappcore::IAppModule)

public: //IAppModule
   void initialize() throw();

public:
    OpenPhone();
    ~OpenPhone();

public slots:
    void sessionStarted();
    void sessionStopped();

/*    void openPhoneStarted();
    void openPhoneProcessingError(QProcess::ProcessError error);
    void openPhoneFinished(int exitCode, QProcess::ExitStatus exitStatus);
*/
private:
    QPointer<VoIPSettings> _voipSettings;
    void initSettings();
    void startProcess();
    void killProcess();

    HANDLE m_hProcess;
    HANDLE FindProcess(const char* cszProcessName);

/*    QPointer <QProcess> _process;*/
};

#endif // OPENPHONE_WIN_H
