# -------------------------------------------------
# Project created by QtCreator 2010-04-11T15:29:34
# -------------------------------------------------
#QT -= gui
QT += network
TARGET = ccplugin_openphone
TEMPLATE = lib
DESTDIR = $$OUT_PWD/../bin
contains( CONFIG, static) {
  CONFIG  += staticlib
  DEFINES += QT_NODLL
  DEFINES += QCA_STATIC
} else {
  CONFIG += plugin
}

INCLUDEPATH += ../ \
    ../thirdparty/qxmpp-0.7.6/src/base \
    ../thirdparty/qxmpp-0.7.6/src/client
LIBS = -L$$DESTDIR \
    -lappcore

DEFINES += OPENPHONE_CCPLUGIN_LIBRARY
SOURCES += openphone.cpp \
    #openphone_nix.cpp \
    voipsettings.cpp

HEADERS += openphone.h \
    openphone_global.h \
    voipsettings.h  #\
    #openphone_win.h \
    #openphone_nix.h

unix: {
        HEADERS += openphone_nix.h
        SOURCES += openphone_nix.cpp
}
win32: {
        HEADERS += openphone_win.h
        SOURCES += openphone_win.cpp
}








