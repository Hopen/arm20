#ifndef OPENPHONE_NIX_H
#define OPENPHONE_NIX_H

#include "openphone_global.h"
#include "appcore/iappmodule.h"

#include "QProcess"


//class QConfFileSettingsExPrivate;
//class QSettingsExPrivate
//{
//public:
//    static QSettingsPrivate *create(QSettings::Format format, QSettings::Scope scope,
//                                        const QString &organization, const QString &application)
//    {
//        return new QConfFileSettingsExPrivate(format, scope, organization, application);
//    }

//};

//class QConfFileSettingsExPrivate: public QSettingsExPrivate
//{
//public:
//    QConfFileSettingsExPrivate(QSettings::Format format, QSettings::Scope scope,
//                             const QString &organization, const QString &application);
//    ~QConfFileSettingsExPrivate();

//    bool writeIniFile(QIODevice &device, const ParsedSettingsMap &map);

//};

//class QSettingsEx: public QSettings
//{
//    Q_OBJECT
//    Q_DECLARE_PRIVATE(QSettingsEx)
//public:
//    QSettingsEx(Format format, Scope scope, const QString &organization,
//	      const QString &application = QString(), QObject *parent = 0)
//        :QObject(*QSettingsExPrivate::create(NativeFormat, UserScope, organization, application),
//                       parent)

//    {

//    }

//    ~QSettingsEx()
//    {
//    }


//};


class VoIPSettings;
class OPENPHONE_EXPORT OpenPhone : public QObject
        , public ccappcore::IAppModule
{

   Q_OBJECT
   Q_INTERFACES(ccappcore::IAppModule)

public: //IAppModule
   void initialize() throw();

public:
    OpenPhone();
    ~OpenPhone();

public slots:
    void sessionStarted();
    void sessionStopped();

    void openPhoneStarted();
    void openPhoneProcessingError(QProcess::ProcessError error);
    void openPhoneFinished(int exitCode, QProcess::ExitStatus exitStatus);

private:
    QPointer<VoIPSettings> _voipSettings;
    void initSettings();
    void startProcess();
    void killProcess();

    int _pid;
//    HANDLE m_hProcess;
//    HANDLE FindProcess(const char* cszProcessName);
//    int FindProcess(const char* cszProcessName);

    QPointer <QProcess> _process;
};


#endif // OPENPHONE_NIX_H
