#include "openphone.h"
#include "voipsettings.h"
#include "appcore/statusnotifier.h"
#include "appcore/appsettings.h"
#include "appcore/appsettings.h"
#include <tlhelp32.h>

using namespace ccappcore;

OpenPhone::OpenPhone()
    : m_hProcess(NULL)
{
/*    _process = new QProcess(this);*/
}

void OpenPhone::startProcess()
{
    LateBoundObject<StatusNotifier> status;
    status->postInfo(tr("Запуск VoIP модуля."));
    killProcess();
    qDebug()<<"Starting VoIP module...";
/*    _process->start(_voipSettings->moduleName(), QStringList() << "-d");*/

    STARTUPINFO si = {};
    si.cb = sizeof(si);
    si.dwFlags = STARTF_USESHOWWINDOW;
    si.wShowWindow = SW_HIDE;//SW_NORMAL;
    PROCESS_INFORMATION pi = {};
    //WCHAR appl[] = L"D:/Projects/new/trunk/sources/openphone/OpenPhone/openphone.exe";
    //std::wstring ss(appl);
    BOOL bRes = CreateProcess(NULL, /*appl*/ (WCHAR*)_voipSettings->moduleName().utf16(), NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
    if (bRes)
    {
      qDebug()<<"VoIP. VoIP module started successfully.";
      m_hProcess = pi.hProcess;
    }
    else
      qDebug()<<"VoIP. Failed to start VoIP module. Last error = "<<GetLastError();
}

void OpenPhone::killProcess()
{
/*    if (!_process || (_process->state() != QProcess::Running) )
        return;


    _process->close();
    _process->waitForFinished(3000);*/
    if(!m_hProcess)
        m_hProcess = FindProcess(_voipSettings->moduleName().toAscii());
    if(!m_hProcess)
        return;

    TerminateProcess(m_hProcess, 0);
    ::WaitForSingleObject(m_hProcess,3000);
    CloseHandle(m_hProcess);
    m_hProcess = NULL;

    qDebug()<<"VoIP module stopped.";
}

HANDLE OpenPhone::FindProcess(const char* cszProcessName)
{
    HANDLE            hSnapShot=NULL;
    HANDLE            hProcess = NULL;
    PROCESSENTRY32    processInfo;

    ::ZeroMemory(&processInfo, sizeof(PROCESSENTRY32));
    processInfo.dwSize = sizeof(PROCESSENTRY32);
    hSnapShot = CreateToolhelp32Snapshot(
        TH32CS_SNAPPROCESS, 0);
    if(hSnapShot == INVALID_HANDLE_VALUE)
        return NULL;

    bool bFirst = true;
    while((bFirst ? Process32First(hSnapShot,
        &processInfo) : Process32Next(hSnapShot,
        &processInfo)))
    {
        bFirst = false;
        QString processName(cszProcessName);
        QString exeName( QString::fromWCharArray(processInfo.szExeFile) );

        if(processName.endsWith(exeName, Qt::CaseInsensitive))
        {
            hProcess=OpenProcess(
                SYNCHRONIZE|PROCESS_TERMINATE, FALSE,
                processInfo.th32ProcessID);

            return hProcess;
        }
    }

    if(hSnapShot)
        CloseHandle(hSnapShot);
    return hProcess;
}
