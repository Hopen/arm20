#ifndef ACTIONBASE_H
#define ACTIONBASE_H

#include <QObject>

class ActionBase: public QObject
{
    Q_OBJECT;
public:
    ActionBase(QObject* parent) : QObject(parent) {}
    virtual ~ActionBase() {}
    virtual void run() = 0;

signals:
    void progressChanged(int progress, const QString& status) const;
    void finished() const;
    void error(const QString& error) const;
};

#endif // ACTIONBASE_H
