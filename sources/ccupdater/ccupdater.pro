QT += core gui network

TARGET = ccupdater
TEMPLATE = app

SOURCES += main.cpp \
    filedownloadaction.cpp \
    unzipfileaction.cpp \
    executescriptsaction.cpp

HEADERS  += \
    actionbase.h \
    filedownloadaction.h \
    unzipfileaction.h \
    executescriptsaction.h

FORMS    +=
