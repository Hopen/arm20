#include "filedownloadaction.h"

FileDownloadAction::FileDownloadAction( const QUrl& url, QObject *parent )
    : ActionBase(parent)
    , _url(url)
{
}

void FileDownloadAction::run()
{
    QNetworkReply* reply = _qnam.get( QNetworkRequest(_url) );
    connect(reply,
            SIGNAL(finished()),
            SLOT(downloadFinished()));
    connect(reply,
            SIGNAL(downloadProgress(qint64,qint64)),
            SLOT(downloadProgress(qint64,qint64)) );
    connect(reply,
            SIGNAL(error(QNetworkReply::NetworkError)),
            SLOT(networkError(QNetworkReply::NetworkError)));
    connect(reply,
            SIGNAL(readyRead()),
            SLOT(writeDataToFile()));
}

void FileDownloadAction::downloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
    double progress = (double)bytesReceived / (double)bytesTotal;
    emit progressChanged( progress * 100, tr("Загрузка обновления...") );
}

void FileDownloadAction::networkError(QNetworkReply::NetworkError err)
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    reply->deleteLater();

    _file.remove();
    _file.close();
    emit error( tr("Ошибка %1 при загрузке файла %2%")
               .arg(err)
               .arg(_url.toString()) );
}

void FileDownloadAction::downloadFinished()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());

    reply->deleteLater();
    _file.close();

    if (reply->error())
        emit error(tr("Download failed: %1.").arg(reply->errorString()));
    else
        emit finished();
}


void FileDownloadAction::writeDataToFile()
{
    // this slot gets called every time the QNetworkReply has new data.
    // We read all of its new data and write it into the file.
    // That way we use less RAM than when reading it at the finished()
    // signal of the QNetworkReply

    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    Q_ASSERT( reply->bytesAvailable()>=0 );

    if(!_file.isOpen())
    {
        if( !_file.open() )
        {
            emit error( tr("Ошибка открытия файла для записи: ") + _file.fileName() );
            return;
        }
    }

    _file.write(reply->readAll());
}


