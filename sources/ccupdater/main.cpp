#include <QtGui/QApplication>
#include <QProgressDialog>
#include <QTextCodec>

#include "filedownloadaction.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));

    QProgressDialog progress(QObject::tr("Загрузка обновления..."), QObject::tr("Отмена"), 0, 100);
    progress.setWindowModality(Qt::WindowModal);
    FileDownloadAction download(QUrl("http://img-fotki.yandex.ru/get/5901/ledijuli.2e/0_4fe3e_e94616a5_orig"));
    QObject::connect(&download, SIGNAL(progressChanged(int,QString)),&progress,SLOT(setValue(int)));
    download.run();
    download.file().setAutoRemove(false);
    progress.exec();

    return EXIT_SUCCESS;
}
