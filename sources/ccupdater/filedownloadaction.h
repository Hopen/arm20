#ifndef FILEDOWNLOADACTION_H
#define FILEDOWNLOADACTION_H

#include <QObject>
#include <QUrl>
#include <QString>
#include <QScopedPointer>
#include <QTemporaryFile>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#include "actionbase.h"

class FileDownloadAction : public ActionBase
{
    Q_OBJECT;
public:
    explicit FileDownloadAction( const QUrl& sourceUrl, QObject *parent = 0);

    //note that the file will be deleted when the action is destroyed
    QTemporaryFile& file() { return _file; }
public://IActionBase
    virtual void run();

signals:

private slots:
    void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);
    void networkError(QNetworkReply::NetworkError);
    void writeDataToFile();
    void downloadFinished();

private:

    QUrl _url;
    QTemporaryFile _file;
    QNetworkAccessManager _qnam;
};

#endif // FILEDOWNLOADACTION_H
