#ifndef CONTACTSTREEMODELTESTS_H
#define CONTACTSTREEMODELTESTS_H

#include <QObject>
#include "appcore/contactscontroller.h"

using namespace ccappcore;
class ContactsTreeModelTests : public ContactsController
{
Q_OBJECT
public:
    explicit ContactsTreeModelTests(QObject *parent = 0);


    virtual QUrl buildContactUrl(const Contact &) const;

    virtual const QList<Contact>& contacts() const;

    virtual QFuture<Contact> loadContactsAsync();

private slots:

    void run();
private:
    QList<Contact> _contacts;
};

#endif // CONTACTSTREEMODELTESTS_H
