#include <QCoreApplication>
#include <QtTest/QtTest>

#include "appcore/call.h"
#include "appcore/contact.h"

#include "contactstreemodeltests.h"
#include "testrouter.h"
#include "testindicators.h"

#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    qRegisterMetaType<ccappcore::Call>("ccappcore::Call");
    qRegisterMetaType<ccappcore::Contact>("ccappcore::Contact");
    qRegisterMetaType<ccappcore::Message>("ccappcore::Message");

    int retVal = 0;
    retVal += QTest::qExec(new ContactsTreeModelTests(), argc, argv);
    retVal += QTest::qExec(new TestRouter(),argc,argv);
    retVal += QTest::qExec(new TestIndicators(),argc,argv);
    if(!retVal)
        cout<<"SUCCESS"<<endl;
    else
        cout<<"FAILED: "<<retVal<<endl;
    return retVal;
}
