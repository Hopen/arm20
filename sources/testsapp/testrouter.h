#ifndef TESTROUTER_H
#define TESTROUTER_H

#include <QtTest/QTest>
#include <QPluginLoader>

#include "appcore/irouterclient.h"
using namespace ccappcore;

class TestRouter : public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();
    void cleanupTestCase();

    void messageLike();
    void serializeMessage();
    void copyMessage();
    void sendMessage();
private:
    QPointer<IRouterClient> routerClient;
    QPluginLoader loader;
    QTimer timer;
};

#endif // TESTROUTER_H
