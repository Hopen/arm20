#include "testindicators.h"
#include "appcore/quantativeindicator.h"

#include <QtTest/QtTest>

using namespace ccappcore;

TestIndicators::TestIndicators(QObject *parent) :
    QObject(parent)
{
}

void TestIndicators::testTrendCalculation()
{
    QuantativeIndicator indicator;
    QDateTime d1,d2,d3,d4,d5,d6,d7,d8,d9;
    d1 = QDateTime::currentDateTime();
    d2 = d1.addSecs(1);
    d3 = d2.addSecs(1);
    d4 = d3.addSecs(1);
    d5 = d4.addSecs(1);
    d6 = d5.addSecs(1);
    d7 = d6.addSecs(1);
    d8 = d7.addSecs(1);
    d9 = d8.addSecs(1);

    indicator.setValue(d1, 1);
    indicator.setValue(d2, 2);
    indicator.setValue(d3, 3);
    QCOMPARE(indicator.history().size(), unsigned(3));
    QCOMPARE(indicator.trend(), QuantativeIndicator::PositiveTrend);

    indicator.setValue(d1, 3);
    indicator.setValue(d2, 2);
    indicator.setValue(d3, 1);
    QCOMPARE(indicator.history().size(), unsigned(3));
    QCOMPARE(indicator.trend(), QuantativeIndicator::NegativeTrend);

    indicator.setValue(d1, 1);
    indicator.setValue(d2, 2);
    indicator.setValue(d3, 1);
    QCOMPARE(indicator.history().size(), unsigned(3));
    QCOMPARE(indicator.trend(), QuantativeIndicator::NegativeTrend);
}
