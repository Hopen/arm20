#include "testrouter.h"
//#include "routerclient/messageserializer.h"

using namespace ccappcore;
//using namespace is3client;

void TestRouter::initTestCase()
{
    loader.setFileName("ccplugin_routerclient.dll");
    if(!loader.load())
        QFAIL("loader.load() has failed.");

    routerClient = qobject_cast<IRouterClient*>(loader.instance());
    if(!routerClient)
        QFAIL("qobject_cast<IRouterClient>(loader.instance()) has failed.");

    QString host = getenv("ROUTER_HOST");
    QString port = getenv("ROUTER_PORT");
    QVERIFY(!host.isEmpty() && !port.isEmpty());

    routerClient->connectToServer(host,qvariant_cast<int>(port));
    routerClient->waitForConnected(3000);
    QVERIFY(routerClient->isConnected());
}

void TestRouter::cleanupTestCase()
{
    routerClient->disconnectFromServer();
    routerClient->waitForDisconnected(3000);
    QVERIFY(!routerClient->isConnected());
    delete routerClient;
    if(!loader.unload())
        QFAIL("loader.unload() has failed.");
}

void TestRouter::sendMessage()
{
//    for(int i=0;i<100;i++)
//    {
//        int rand = qrand();
//        int msgId =  rand / 100;
//        bool subs = rand % 2;

//        QString msgName = QString("TEST_ROUTER") + qvariant_cast<QString>(msgId);

//        Message msg = Message(msgName)
//                      .add("int32",i)
//                      .add("int64",(quint64)rand)
//                      .add("string",qvariant_cast<QString>(rand))
//                      //.add("datetime",QDateTime::currentDateTime())
//                      //.add("binary",QByteArray("z",rand))
//                      .add("double",(double)rand);
//                      //.add("float",(float)rand);
//        int i = qrand();
//        if(subs)
//            routerClient->subscribeEvent(msg.name());
//        else
//            routerClient->unsubscribeEvent(msg.name());
//        QString iStr = qvariant_cast<QString>(i);
//        routerClient->send(msg);
//        qApp->processEvents(QEventLoop::AllEvents,10);
//    }
}

void TestRouter::messageLike()
{
    Message prototype("C2O_LOGIN");
    prototype["requestResponseCommandID"] = 123987;

    Message msg("C2O_LOGIN");

    msg["requestResponseCommandID"] = 123987;
    msg["comments"] = "this should not fail";
    QVERIFY(msg.like(prototype));

    msg["requestResponseCommandID"] = 123989;
    msg["comments"] = "this should fail";
    QVERIFY(!msg.like(prototype));

    msg["requestResponseCommandID"] = 123987;
    msg.setName("C2O_LOGIN_NOTIFY");
    QVERIFY(!msg.like(prototype));
}

void TestRouter::serializeMessage()
{

  QBuffer buffer;
  QDataStream out;
  out.setDevice(&buffer);
  out.setByteOrder(QDataStream::LittleEndian);
  buffer.open(QIODevice::WriteOnly);


  Message msg("TEST_message");
  msg["p1"] = 1234;
  msg["ui4param2"] = (quint32)1234;
  msg["stringData"] = "This text should actually be stored as QString";
  msg["floatData"] = 0.02f;

  //QFAIL("test not implemented");

//  out<<msg;
//  qint64 expectedSize = sizeof(MESSAGE_HEADER)+sizeof(PARAM_HEADER)*4
//                                +sizeof("This text should actually be stored as QString")*2; //UNICODE

//  buffer.close();
//  buffer.open(QIODevice::ReadOnly);

//  QCOMPARE(expectedSize, buffer.size());
//  QVERIFY(buffer.size()>0);

//  QDataStream in;
//  in.setByteOrder(QDataStream::LittleEndian);
//  in.setDevice(&buffer);

//  Message restored;

//  QCOMPARE(buffer.bytesAvailable(),sizeof(MESSAGE_HEADER));

//  in>>restored;

//  QVERIFY( msg == restored );

//  QCOMPARE(msg.name(),restored.name());
//  QCOMPARE(4U , msg.paramsCount());
//  QCOMPARE(msg.paramsCount(),restored.paramsCount());

//  QCOMPARE(0.02f , (float)restored[3]);
//  QCOMPARE(1234 , (qint32)restored.at(0));
//  QCOMPARE(1234U , (quint32)restored[1]);

//  QVERIFY(msg[0U] == restored[0U]);
//  QVERIFY(msg[1] == restored[1]);
//  QVERIFY(msg[2] == restored[2]);
//  QVERIFY(msg[3] == restored[3]);

//  QCOMPARE( msg["stringData"] , restored["stringData"]);

  buffer.close();
}

void TestRouter::copyMessage()
{
      Message m("TEST_message");
      m["i4param1"] = 1234;
      m["ui4param2"] = (quint32)1234;
      m["stringData"] = "This text should actually be stored as QString";

      Message c1 = m;
      Message c2(m);

      QVERIFY( m == c1);
      QVERIFY( m == c2);

      QVERIFY( &m.params() != &c1.params());
      QVERIFY( &m.params() != &c2.params());

      QVERIFY( &m[0U] != &c1[0U]);

      c2["flTest"]=0.123f;

      QVERIFY( !(m == c2) );
      QVERIFY( m == c1);
}

