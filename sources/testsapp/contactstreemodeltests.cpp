#include <QtTest/QtTest>
#include "contactstreemodeltests.h"

#include "appcore/contactstreemodel.h"
#include "appcore/predicate.h"

using namespace ccappcore;



ContactsTreeModelTests::ContactsTreeModelTests(QObject *parent) :
    ContactsController(parent)
{
    Contact c0(NULL,1), c1(NULL,2), c00(NULL,3), c01(NULL,4), c02(NULL,5);
    c0.setName("c0");
    c1.setName("c1");
    c00.setName("c00");
    c01.setName("c01");
    c02.setName("c02");

    c0.addChild(c00);
    c0.addChild(c01);
    c0.addChild(c02);

    _contacts<<c0<<c1;
}

QUrl ContactsTreeModelTests::buildContactUrl(const Contact &) const
{
    return QUrl();
}

QFuture<Contact> ContactsTreeModelTests::loadContactsAsync()
{
    return QFuture<Contact>();
}

const QList<Contact>& ContactsTreeModelTests::contacts() const
{
    return _contacts;
}

void ContactsTreeModelTests::run()
{
    ContactsTreeModel model;
    model.setController(this);

    QModelIndex i0, i1, i2, i00, i01, i02, i03;
    i0 = model.index(0);
    i1 = model.index(1);
    i2 = model.index(2);
    i00 = model.index(0,0,i0);
    i01 = model.index(1,0,i0);
    i02 = model.index(2,0,i0);
    i03 = model.index(3,0,i0);

    QCOMPARE(model.rowCount(),     2);
    QCOMPARE(model.rowCount(i0),  3);
    QCOMPARE(model.rowCount(i1),  0);
    QVERIFY( !i2.isValid());

    QCOMPARE(model.rowCount(i00), 0);
    QVERIFY( !i03.isValid());

    QCOMPARE(model.data(i0,Qt::DisplayRole),QVariant("c0"));
    QCOMPARE(model.data(i1,Qt::DisplayRole),QVariant("c1"));
    QCOMPARE(model.data(i2,Qt::DisplayRole),QVariant());
    QCOMPARE(model.data(i00,Qt::DisplayRole),QVariant("c00"));
    QCOMPARE(model.data(i01,Qt::DisplayRole),QVariant("c01"));
    QCOMPARE(model.data(i02,Qt::DisplayRole),QVariant("c02"));
    QCOMPARE(model.data(i03,Qt::DisplayRole),QVariant());

    class isC0: public Predicate<Contact>
    {
    public:
        bool operator()(const Contact& c) const
        {
            return c.name() == "c0" || c.name()=="c01" || c.name() == "c02";
        }
    };

    model.addFilter(new isC0());
    QCOMPARE(model.rowCount(),     1);
    QCOMPARE(model.rowCount(i0),  2);
    QCOMPARE(model.data(i0,Qt::DisplayRole),QVariant("c0"));
    QCOMPARE(model.data(i1,Qt::DisplayRole),QVariant());
    QCOMPARE(model.data(i00,Qt::DisplayRole),QVariant("c01"));
    QCOMPARE(model.data(i01,Qt::DisplayRole),QVariant("c02"));
    QCOMPARE(model.data(i02,Qt::DisplayRole),QVariant());
}
