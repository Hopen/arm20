#ifndef TESTINDICATORS_H
#define TESTINDICATORS_H

#include <QObject>

class TestIndicators : public QObject
{
    Q_OBJECT
public:
    explicit TestIndicators(QObject *parent = 0);

private slots:

    void testTrendCalculation();

};

#endif // TESTINDICATORS_H
