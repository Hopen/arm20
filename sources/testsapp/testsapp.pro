#-------------------------------------------------
#
# Project created by QtCreator 2010-09-14T03:01:37
#
#-------------------------------------------------

QT       +=  network

QT       -= gui core

TARGET = testsapp
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

DESTDIR = $$OUT_PWD/../bin

INCLUDEPATH += ../
LIBS = -L$$DESTDIR -lappcore

HEADERS += \
    contactstreemodeltests.h \
    testrouter.h \
    testindicators.h

SOURCES += testsapp.cpp \
    contactstreemodeltests.cpp \
    testrouter.cpp \
    testindicators.cpp

unix: LIBS+=-lqca

windows {
CONFIG(debug  ,debug|release): LIBS+=-lQtTestd4 -Ld:/Qt_source_4.8.3/lib
CONFIG(release,debug|release): LIBS+=-lQtTest4 -Ld:/Qt_source_4.8.3/lib
}

windows {
CONFIG(debug  ,debug|release): LIBS+=-lQtCored4 -Ld:/Qt_source_4.8.3/lib
CONFIG(release,debug|release): LIBS+=-lQtCore4 -Ld:/Qt_source_4.8.3/lib
}

