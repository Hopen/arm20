/********************************************************************************
** Form generated from reading UI file 'opt_callform.ui'
**
** Created: Tue 26. Mar 13:54:36 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPT_CALLFORM_H
#define UI_OPT_CALLFORM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OptionsTabCallForm
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout;
    QListWidget *listSubjects;
    QVBoxLayout *verticalLayout_2;
    QPushButton *bnAddSubject;
    QPushButton *bnRemoveSubject;
    QSpacerItem *verticalSpacer;
    QCheckBox *cbShowCallForm;
    QCheckBox *cbCloseOnEndCall;

    void setupUi(QWidget *OptionsTabCallForm)
    {
        if (OptionsTabCallForm->objectName().isEmpty())
            OptionsTabCallForm->setObjectName(QString::fromUtf8("OptionsTabCallForm"));
        OptionsTabCallForm->resize(363, 286);
        verticalLayout = new QVBoxLayout(OptionsTabCallForm);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox = new QGroupBox(OptionsTabCallForm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        horizontalLayout = new QHBoxLayout(groupBox);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        listSubjects = new QListWidget(groupBox);
        listSubjects->setObjectName(QString::fromUtf8("listSubjects"));
        listSubjects->setMinimumSize(QSize(200, 100));
        listSubjects->setEditTriggers(QAbstractItemView::AnyKeyPressed|QAbstractItemView::DoubleClicked|QAbstractItemView::EditKeyPressed|QAbstractItemView::SelectedClicked);
        listSubjects->setAlternatingRowColors(true);
        listSubjects->setSelectionBehavior(QAbstractItemView::SelectRows);

        horizontalLayout->addWidget(listSubjects);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        bnAddSubject = new QPushButton(groupBox);
        bnAddSubject->setObjectName(QString::fromUtf8("bnAddSubject"));

        verticalLayout_2->addWidget(bnAddSubject);

        bnRemoveSubject = new QPushButton(groupBox);
        bnRemoveSubject->setObjectName(QString::fromUtf8("bnRemoveSubject"));

        verticalLayout_2->addWidget(bnRemoveSubject);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);


        horizontalLayout->addLayout(verticalLayout_2);


        verticalLayout->addWidget(groupBox);

        cbShowCallForm = new QCheckBox(OptionsTabCallForm);
        cbShowCallForm->setObjectName(QString::fromUtf8("cbShowCallForm"));

        verticalLayout->addWidget(cbShowCallForm);

        cbCloseOnEndCall = new QCheckBox(OptionsTabCallForm);
        cbCloseOnEndCall->setObjectName(QString::fromUtf8("cbCloseOnEndCall"));

        verticalLayout->addWidget(cbCloseOnEndCall);


        retranslateUi(OptionsTabCallForm);

        QMetaObject::connectSlotsByName(OptionsTabCallForm);
    } // setupUi

    void retranslateUi(QWidget *OptionsTabCallForm)
    {
        OptionsTabCallForm->setWindowTitle(QApplication::translate("OptionsTabCallForm", "Form", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("OptionsTabCallForm", "\320\242\320\265\320\274\321\213 \320\267\320\262\320\276\320\275\320\272\320\260", 0, QApplication::UnicodeUTF8));
        bnAddSubject->setText(QApplication::translate("OptionsTabCallForm", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        bnRemoveSubject->setText(QApplication::translate("OptionsTabCallForm", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        cbShowCallForm->setToolTip(QApplication::translate("OptionsTabCallForm", "\320\225\321\201\320\273\320\270 \320\262\320\272\320\273\321\216\321\207\320\265\320\275\320\276, \320\272\320\260\321\200\321\202\320\276\321\207\320\272\320\260 \320\267\320\262\320\276\320\275\320\272\320\260 \320\261\321\203\320\264\320\265\321\202 \320\277\320\276\320\272\320\260\320\267\320\260\320\275\320\260 \321\201\321\200\320\260\320\267\321\203 \320\277\320\276 \320\277\321\200\320\270\321\205\320\276\320\264\321\203 \320\267\320\262\320\276\320\275\320\272\320\260 \320\262 \321\201\320\270\321\201\321\202\320\265\320\274\321\203. \320\222 \320\277\321\200\320\276\321\202\320\270\320\262\320\275\320\276\320\274 \321\201\320\273\321\203\321\207\320\260\320\265, \321\207\321\202\320\276\320\261\321\213 \320\277\320\276\320\272\320\260\320\267\320\260\321\202\321\214 \320\272\320\260\321\200\321\202\320\276\321\207\320\272\321\203 \320\267\320\262\320\276\320\275\320\272\320\260, \320\275\320\265\320\276\320\261\321\205\320\276\320\264\320\270\320\274\320\276 \320\262\321\213\320\261\321\200\320\260\321"
                        "\202\321\214 \320\267\320\262\320\276\320\275\320\276\320\272 \320\270\320\267 \321\201\320\277\320\270\321\201\320\272\320\260 \320\270 \320\264\320\262\320\260\320\266\320\264\321\213 \321\211\320\265\320\273\320\272\320\275\321\203\321\202\321\214 \320\274\321\213\321\210\321\214\321\216 \320\277\320\276 \320\275\320\265\320\274\321\203. ", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        cbShowCallForm->setText(QApplication::translate("OptionsTabCallForm", "\320\237\320\276\320\272\320\260\320\267\321\213\320\262\320\260\321\202\321\214 \320\272\320\260\321\200\321\202\320\276\321\207\320\272\321\203 \320\267\320\262\320\276\320\275\320\272\320\260 \321\201\321\200\320\260\320\267\321\203", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        cbCloseOnEndCall->setToolTip(QApplication::translate("OptionsTabCallForm", "\320\225\321\201\320\273\320\270 \320\262\320\272\320\273\321\216\321\207\320\265\320\275\320\276, \320\272\320\260\321\200\321\202\320\276\321\207\320\272\320\260 \320\267\320\262\320\276\320\275\320\272\320\260 \320\261\321\203\320\264\320\265\321\202 \320\277\320\276\320\272\320\260\320\267\320\260\320\275\320\260 \321\201\321\200\320\260\320\267\321\203 \320\277\320\276 \320\277\321\200\320\270\321\205\320\276\320\264\321\203 \320\267\320\262\320\276\320\275\320\272\320\260 \320\262 \321\201\320\270\321\201\321\202\320\265\320\274\321\203. \320\222 \320\277\321\200\320\276\321\202\320\270\320\262\320\275\320\276\320\274 \321\201\320\273\321\203\321\207\320\260\320\265, \321\207\321\202\320\276\320\261\321\213 \320\277\320\276\320\272\320\260\320\267\320\260\321\202\321\214 \320\272\320\260\321\200\321\202\320\276\321\207\320\272\321\203 \320\267\320\262\320\276\320\275\320\272\320\260, \320\275\320\265\320\276\320\261\321\205\320\276\320\264\320\270\320\274\320\276 \320\262\321\213\320\261\321\200\320\260\321"
                        "\202\321\214 \320\267\320\262\320\276\320\275\320\276\320\272 \320\270\320\267 \321\201\320\277\320\270\321\201\320\272\320\260 \320\270 \320\264\320\262\320\260\320\266\320\264\321\213 \321\211\320\265\320\273\320\272\320\275\321\203\321\202\321\214 \320\274\321\213\321\210\321\214\321\216 \320\277\320\276 \320\275\320\265\320\274\321\203. ", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        cbCloseOnEndCall->setText(QApplication::translate("OptionsTabCallForm", "\320\227\320\260\320\272\321\200\321\213\320\262\320\260\321\202\321\214 \320\272\320\260\321\200\321\202\320\276\321\207\320\272\321\203 \320\267\320\262\320\276\320\275\320\272\320\260 \320\277\320\276 \320\276\320\272\320\276\320\275\321\207\320\260\320\275\320\270\320\270 \321\200\320\260\320\267\320\263\320\276\320\262\320\276\321\200\320\260", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class OptionsTabCallForm: public Ui_OptionsTabCallForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPT_CALLFORM_H
