#include "operevents.h"

using namespace ccappcore;

OperEvents::OperEvents(QObject *parent)
    : QObject(parent)
{
    initializeSoundEvents();
}

void OperEvents::initializeSoundEvents()
{
    _soundEvents->registerEvent("call_in",QObject::tr("Входящий звонок"));
    _soundEvents->connectPlayEvent("call_in", _callsController.instance(), SIGNAL(callAssigned(ccappcore::Call)));
    _soundEvents->connectPlayEvent("call_in", _callsController.instance(), SIGNAL(callInRingStarted()));
    _soundEvents->connectStopEvent("call_in", _callsController.instance(), SIGNAL(callFailed(ccappcore::Call,ccappcore::Call::CallResult,QString)));
    _soundEvents->connectStopEvent("call_in", _callsController.instance(), SIGNAL(callConnected(ccappcore::Call)));
    _soundEvents->connectStopEvent("call_in", _callsController.instance(), SIGNAL(callEnded(ccappcore::Call)));

    _soundEvents->registerEvent("call_out",QObject::tr("Исходящий звонок"));
    _soundEvents->connectPlayEvent("call_out", _callsController.instance(), SIGNAL(callOutRingStarted()));
    _soundEvents->connectStopEvent("call_out", _callsController.instance(), SIGNAL(callFailed(ccappcore::Call,ccappcore::Call::CallResult,QString)));
    _soundEvents->connectStopEvent("call_out", _callsController.instance(), SIGNAL(callConnected(ccappcore::Call)));
    _soundEvents->connectStopEvent("call_out", _callsController.instance(), SIGNAL(callEnded(ccappcore::Call)));

    _soundEvents->registerEvent("call_end",QObject::tr("Окончание звонка"));
    _soundEvents->connectPlayEvent("call_end", _callsController.instance(), SIGNAL(callEnded(ccappcore::Call)));
    _soundEvents->connectPlayEvent("call_end", _callsController.instance(), SIGNAL(callFailed(ccappcore::Call,ccappcore::Call::CallResult,QString)));

    connect(_callsController.instance(), SIGNAL(queueCallAdded(ccappcore::Call)), this, SLOT(on_queueCallAdded(ccappcore::Call)));
    _soundEvents->registerEvent("call_queued",QObject::tr("Новый звонок в очереди"));
    _soundEvents->connectPlayEvent("call_queued", this, SIGNAL(queueCallAdded(ccappcore::Call)));

    _soundEvents->registerEvent("private_call_queue",QObject::tr("Звонок в личной очереди"));
    _soundEvents->connectPlayEvent("call_queued", _callsController.instance(), SIGNAL(privateQueueCallAdded(ccappcore::Call)));

    LateBoundObject<AppSettings> appSettings;
    //appSettings->loadUserSettings();
    LateBoundObject<SoundEventsManager> _soundManager;
    _soundManager->applySettings(appSettings.instance());

}

void OperEvents::on_queueCallAdded(ccappcore::Call call)
{
    LateBoundObject<OperSettings> _operSettings;
    if( !_operSettings->showPrivateGroupsOnly() || _callsController->isOwnGroupCall(call) )
        emit queueCallAdded(call);
}
