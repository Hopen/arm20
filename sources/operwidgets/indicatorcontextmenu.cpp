#include "indicatorcontextmenu.h"
#include "editthresholdsdialog.h"

IndicatorContextMenu::IndicatorContextMenu(QWidget *parent) :
    QMenu(parent)
{
    _showGauge = addAction(tr("Показать в граф. виде"), this, SLOT(showGauge()));

    _editThresholds = addAction(tr("Пороговые значения..."), this,SLOT(editThresholds()));
}

void IndicatorContextMenu::popup(QuantativeIndicator* i, const QPoint& pos)
{
    _indicator = i;
    QMenu::popup(pos);
}

void IndicatorContextMenu::showGauge()
{
    emit showGauge(_indicator);
}

void IndicatorContextMenu::editThresholds()
{
    EditThresholdsDialog dlg;
    dlg.setValues(_indicator->isInverted(),
                  _indicator->warningValue(),
                  _indicator->criticalValue());
    if(QDialog::Accepted!=dlg.exec())
        return;

    _indicator->setInverted(dlg.isInverted());
    _indicator->setWarningValue(dlg.warningValue());
    _indicator->setCriticalValue(dlg.criticalValue());
}
