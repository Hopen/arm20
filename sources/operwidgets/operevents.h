#ifndef OPEREVENTS_H
#define OPEREVENTS_H

#include "operwidgets_global.h"
#include "appcore/soundeventsmanager.h"
#include "appcore/callscontroller.h"
#include "opersettings.h"

using namespace ccappcore;

class OperEvents : public QObject
{
Q_OBJECT
public:
    explicit OperEvents(QObject *parent = 0);

    void initializeSoundEvents();
signals:
    void queueCallAdded(ccappcore::Call);
public slots:

private:
    LateBoundObject<CallsController> _callsController;
    LateBoundObject<SoundEventsManager> _soundEvents;
    LateBoundObject<OperSettings> _operSettings;

private slots:
    void on_queueCallAdded(ccappcore::Call);
};

#endif // OPEREVENTS_H
