#ifndef CALLTAB_H
#define CALLTAB_H

#include "operwidgets_global.h"
#include "appcore/serviceprovider.h"
#include "appcore/callscontroller.h"
#include "appcore/operlistcontroller.h"
#include "appcore/sessioncontroller.h"
#include "appcore/callslistmodel.h"
#include "appcore/iappurlhandler.h"

#include "appcontext.h"
#include "opersettings.h"

#include <QFutureWatcher>

using namespace ccappcore;

namespace Ui {
    class CallTab;
}

//enum eTitleCallStatus
//{
//    TCS_ACTIVE = 0,
//    TCS_ALERT,
//    TCS_HOLD,
//    TCS_COMPLETE
//};



class QWebView;
class Brouser : public QWidget
{
    Q_OBJECT
public:
    explicit Brouser(QWidget *parent = 0);
    virtual ~Brouser()
    {}

    QWebView * getWebView()const {return _mainView;}
    //void setWebView(QWebView *_view){_mainView = _view;}
public slots:
    void onWebViewLoadFinished(bool ok);
    void onWebViewLoadStarted();
private:
    QWebView * _mainView;

    QPointer <QToolButton> _btnBack;
    QPointer <QToolButton> _btnForward;
    QPointer <QToolButton> _btnStop;
    QPointer <QToolButton> _btnRefresh;

};

class TransferContext;

class CallTab : public QWidget, public IAppUrlHandler
{
    Q_OBJECT
    Q_DISABLE_COPY(CallTab)
    Q_INTERFACES(ccappcore::IAppUrlHandler)
public:
    explicit CallTab(QWidget *parent = 0);
    virtual ~CallTab();

    const Call& call() const { return _call; }
    const Contact& contact() const { return _contact; }

    void setContact(Contact c);
    void setCall(Call c);

protected:
    virtual void changeEvent(QEvent *e);
    virtual void showEvent(QShowEvent *);
    //virtual bool event(QEvent *);
    void startProcess(const QString& path);
signals:
    void iconChanged();    
    void titleChanged(QString);
    //void titleCallStatusChange(int status);
    //void shutUpISeeYou();
    void readyToQuit();

public: //IAppUrlHandler
    bool openUrl(const QUrl& url);

public slots:
    void openWebPage(const QUrl& url);
    void openCallHistory(int days);
    void onContactSelected(QVariant contact);
    void onScriptSelected(QString scriptName);
    void onPhoneSelected(QString scriptName);
    void onProcessStartError(QProcess::ProcessError error);
    void onAnyWidgetCliked();
    void on_phoneLine_textChanged(QString );
    void playDTMF();
    //void close();
private:
    Ui::CallTab *_ui;
    Call _call;
    Contact _contact;
    LateBoundObject<AppContext> _appContext;
    LateBoundObject<CallsController> _callsController;
    LateBoundObject<OperListController> _operListController;
    LateBoundObject<SessionController> _sessionController;
    LateBoundObject<OperSettings> _operSettings;
    QTimer _stateTimeTimer;
    QMenu _callAdditionalActions;
    void updateHeaderFooter();

    QIcon _collapsedIcon;
    QIcon _expandedIcon;

    QFutureWatcher<Call> _callHistoryFutureWatcher;
    QPointer <TransferContext> _transferContext;
    QPointer <QProcess       > _process;

    QTimer pauseTimoutTimer;
    QString _lastDTMF;
    QString _forcedWebPage;
    QString _externalCommand;

private slots:
    //void on_bnTransfer_toggled(bool checked);
    void on_headerHtml_linkActivated(QString link);
    void on_footerHtml_linkActivated(QString link);
    void on_bnClientDetails_clicked();
    void on_bnCallDetails_clicked();
    void on_bnCallStop_clicked();
    void on_bnCallStart_clicked();
    void on_lineEditCallNote_returnPressed();
    void on_bnAddExtraInfo_clicked();
    void on_bnConnect_customContextMenuRequested(QPoint pos);
    void on_bnConnect_clicked();
    //void on_bnRecord_clicked(bool checked);
    void on_bnMute_clicked(bool checked);
    void on_bnHold_clicked(bool checked);
    void onEndCall(ccappcore::Call);
    void connectCallAction_triggered();
    void updateCallControls();
    void updateCallInfo();
    void updateClientInfo();
    void refreshStateTime();
    void setSubject(QString);
    void requestCallInfo();
    void htmlViewLoadStarted();
    void htmlViewLoadFinished();
    void executeExtraInfo(ccappcore::Call);

    void onCallHistory();
    void onCallHistoryItemClicked(QModelIndex);

    void webViewTitleChanged(const QString&);
    void webViewIconChanged();
};

#endif // CALLTAB_H
