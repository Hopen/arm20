#ifndef LoginDialog_H
#define LoginDialog_H

#include "operwidgets_global.h"

#include "appcore/serviceprovider.h"
#include "appcore/sessioncontroller.h"
#include "appcore/iappcryptoprovider.h"
#include "appcore/statusnotifier.h"
#include "appcore/ntlmauthentication.h"

using namespace ccappcore;

namespace Ui
{
    class LoginDialog;
}

class LoginDialog : public QDialog
{
    Q_OBJECT
    Q_DISABLE_COPY(LoginDialog)
public:

    explicit LoginDialog(QWidget *parent = 0);
    virtual ~LoginDialog();

    void showEvent(QShowEvent *);

protected:
    virtual void changeEvent(QEvent *e);

signals:
    void updateLanguage(QString);

private:
    Ui::LoginDialog *_ui;
    LateBoundObject<SessionController> _session;
    LateBoundObject<AppSettings> _settings;
    LateBoundObject<StatusNotifier> _status;
    LateBoundObject<INtlmLogonProvider> _ntlm;
    QStatusBar* _statusBar;
    bool _loginRequested;

private slots:
    void on_cbAutoLogin_toggled(bool checked);
    void on_cbNtlmAuth_toggled(bool checked);
    void on_bnSettings_clicked();
    void login();

    void sessionStarted();
    void statusInfo(const QString& message);
    void sessionStartingFailed(SessionController::SessionErrorCode, const QString& error);

    void updateControls(bool enable = true);

    //void onChangeLanguage(QString);
};


#endif // LoginDialog_H
