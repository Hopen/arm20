#include "contactsmanager.h"
#include "appcore/domainobjectfilter.h"
#include "appcore/useritemdatarole.h"
#include "appcore/callslistmodel.h"
#include "appcore/contactstreemodel.h"

using namespace ccappcore;

ContactsManager::ContactsManager(QObject* parent)
        : QObject(parent)
        , _hideOfflineOperators(true)
        , _operListByGroupsMode(true)
        , _hideOperators(false)
        , _hideGroups (false)
{

}

void ContactsManager::hideOfflineOperators(bool hide)
{
    _hideOfflineOperators = hide;
    reset();
}

void ContactsManager::setOperListByGroupsMode(bool groupMode)
{
    _operListByGroupsMode = groupMode;
    reset();
}

void ContactsManager::showOnlyPrivateGroups(bool privateOnly)
{
    _operSettings->setShowPrivateGroupsOnly(privateOnly);
    reset();
}

void ContactsManager::hideOperators(bool hide)
{
    _hideOperators = hide;
    reset();
}

void ContactsManager::resetOperFlags()
{
    _hideOperators = false;
    _hideGroups    = false;
    //reset();
}

void ContactsManager::hideGroups(bool hide)
{
    _hideGroups = hide;
    reset();
}

void ContactsManager::reset()
{
    loadContactsModel();
    loadCallsModel();
}

void ContactsManager::searchContactsAsync(const QString& searchText)
{
    foreach(ModelPtr m, _contactsModel.sourceModelList())
    {
        ContactsTreeModel* contactsModel = qobject_cast<ContactsTreeModel*>(m.data());
        if(!contactsModel)
            continue;

        contactsModel->searchAsync(searchText);
    }
}

void ContactsManager::loadContactsModel()
{
    ContactsTreeModel* m = new ContactsTreeModel();

    m->setController(_operListController.instance());
    m->setListMode(!_operListByGroupsMode);

    if(_hideOfflineOperators)
        m->addFilter(new IsContactOnline());

    if(_operSettings->showPrivateGroupsOnly())
        m->addFilter(new IsContactInSessionOwnerGroup());

    if (_hideGroups)
        m->addFilter(new IsOperator());

    if (_hideOperators)
        m->addFilter(new IsGroup());

    ModelList sourceList;
    sourceList.append(ModelPtr(m));

//    QList<ContactsController*> controllerList =
//            ServiceProvider::instance().findServices<ContactsController>();
//    foreach(ContactsController* c, controllerList)
//    {
//        if(c!=_operListController.instance() &&
//           c!=_jabberController  .instance()   )
//        {
//            m = new ContactsTreeModel();
//            m->addFilter(new IsNonEmptyContainer());
//            m->setController(c);
//            c->loadContactsAsync();
//            sourceList.append(ModelPtr(m));
//        }
//    }

    _contactsModel.setSourceModelList(sourceList);
}

void ContactsManager::loadCallsModel()
{
    ModelList sourceList;
    QList<CallsListModel::ColDef> columns;
    CallsListModel* model;

    columns << CallsListModel::AbonPhoneColumn
            << CallsListModel::CallStateColumn
            << CallsListModel::GroupNameColumn
            << CallsListModel::CallTypeColumn
            << CallsListModel::BeginTimeColumn
            << CallsListModel::StateTimeColumn
            << CallsListModel::PriorityColumn
            << CallsListModel::ExtraInfoColumn
            << CallsListModel::LastOperNameColumn
            << CallsListModel::CallLengthColumn;

    model= new CallsListModel();
    model->setVisibleColumns(columns);
    model->addFilter(new IsSessionOwnerCall());
    sourceList.append(ModelPtr(model));

    columns.clear();
    columns << CallsListModel::AbonPhoneColumn
            << CallsListModel::GroupNameColumn;

    model= new CallsListModel();
    model->setVisibleColumns(columns);
    model->addFilter(new IsQueuedCall(_operSettings->showPrivateGroupsOnly()));
    sourceList.append(ModelPtr(model));

    _callsModel.setSourceModelList(sourceList);
}

//Apply service configuration.
void ContactsManager::applySettings(const AppSettings& settings)
{
    _hideOfflineOperators = settings.getOption(
            "view/contacts.hideOfflineOperators" ).toBool();

    _operListByGroupsMode = (bool) settings.getOption(
            "view/contacts.showByGroup" ).toBool();
}

//Flush service configuration
void ContactsManager::flushSettings(AppSettings& settings)
{
    settings.setUserOption("view/contacts.hideOfflineOperators", _hideOfflineOperators);
    settings.setUserOption("view/contacts.showByGroup", _operListByGroupsMode);
}

void ContactsManager::connectView(QAbstractItemView* view)
{
    view->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(view,SIGNAL(customContextMenuRequested(QPoint)),
            this, SLOT(customContextMenuRequested()));
    connect(view,SIGNAL(activated(QModelIndex)),
            this, SLOT(itemActivated(QModelIndex)));
    connect(view,SIGNAL(clicked(QModelIndex)),
            this, SLOT(itemClicked(QModelIndex)));
//    connect(view,SIGNAL(doubleClicked(QModelIndex)),
//            this, SLOT(itemDoubleClicked(QModelIndex)));

}

void ContactsManager::customContextMenuRequested()
{
    QAbstractItemView* view = qobject_cast<QAbstractItemView*>(sender());
    if(!view || !view->selectionModel())
        return;

    QModelIndex current = view->selectionModel()->currentIndex();
    QVariant object = current.data(AssociatedObjectRole);
    if(object.canConvert<Call>())
        _callContextMenu.popup(object.value<Call>());
    if(object.canConvert<Contact>())
    {
        Contact contact = object.value<Contact>();
        if ( (contact.contactId().type() == CT_Operator) ||
             (contact.contactId().type() == CT_Group   )   )
            _operatorContextMenu.popup(contact );
    }
}

void ContactsManager::itemActivated(QModelIndex index)
{
    Call call = index.data(AssociatedObjectRole).value<Call>();
    if(call.isValid())
        _appContext->showCallForm(call);

    Contact contact = index.data(AssociatedObjectRole).value<Contact>();
    if ( (contact.isValid()) && ( (contact.contactId().type() == CT_Operator) ||
                                  (contact.contactId().type() == CT_Group   )   )
       )
        _appContext->showChatForm(contact);

    if ( (contact.isValid()                             ) &&
         (contact.contactId().type() == CT_GlobalAddress)   )
        _appContext->onActivateGlobalAddress(contact);

}

void ContactsManager::itemClicked(QModelIndex index)
{
    Call call = index.data(AssociatedObjectRole).value<Call>();
    if(call.isValid())
        _appContext->onActiveCallChanged(call);

    Contact contact = index.data(AssociatedObjectRole).value<Contact>();
    if ( (contact.isValid()) && ( (contact.contactId().type() == CT_Operator) ||
                                  (contact.contactId().type() == CT_Group   )   )
       )
        _appContext->onActiveContactChanged(contact);

    if ( (contact.isValid()                             ) &&
         (contact.contactId().type() == CT_GlobalAddress)   )
        _appContext->onClickGlobalAddress(contact);

}


//void ContactsManager::itemDoubleClicked(QModelIndex index)
//{
//    Contact contact = index.data(AssociatedObjectRole).value<Contact>();
//    if (contact.isValid())
//        _appContext->onContactSelected(contact);

//}




