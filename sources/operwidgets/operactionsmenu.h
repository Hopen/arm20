#ifndef OPERACTIONSMENU_H
#define OPERACTIONSMENU_H

#include <QMenu>

#include "calloper/contactsmodel.h""
using namespace ccappcore;

class ContactsMenuOperator : public QMenu,
{
    Q_OBJECT
public:
    explicit ContactsMenuOperator(ContactsModel& model, QObject *parent = 0);

signals:

public slots:
    void makeCall_triggered();

private:
    ContactsModel& _model;
    LateBoundObject<OperListController> _operListController;
    LateBoundObject<CallsController> _callsController;

    QPointer<QAction> _makeCallAction;
    QPointer<QAction> _transferCallAction;
};

#endif // OPERACTIONSMENU_H
