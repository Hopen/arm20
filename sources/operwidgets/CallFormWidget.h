#ifndef CALLFORMWIDGET_H
#define CALLFORMWIDGET_H

#include "docktabs.h"
#include "appcore/globaladdressbookcontroller.h"


//class BlueTitleBar : public QWidget
//{
//    Q_OBJECT
//public:
//    BlueTitleBar(QWidget *parent = 0);

//    QSize sizeHint() const { return minimumSizeHint(); }
//    QSize minimumSizeHint() const;
//protected:
//    void paintEvent(QPaintEvent *event);
//    void mousePressEvent(QMouseEvent *event);
//public slots:
//    void updateMask();

//private:
//    QPixmap leftPm, centerPm, rightPm;
//};

class OperSettings;
class CallTab;


class CallFormWidget:public DockTab
{
    Q_OBJECT
public:
    explicit CallFormWidget(ccappcore::Call c, const QString &title, /*bool alerting,*/ QWidget *parent = 0, Qt::WindowFlags flags = 0);

    explicit CallFormWidget(ccappcore::Call c, /*bool alerting,*/ QWidget *parent = 0, Qt::WindowFlags flags = 0)
        :DockTab(parent,flags) {CallFormWidget(c,tr(""),/*alerting,*/parent,flags);}

    virtual ~CallFormWidget();

    virtual QString title()const;

    ccappcore::Call* assignedCall(){return &_call;}

protected:
    virtual void resizeEvent(QResizeEvent *e);

private slots:
    void tabTitleChanged(QString);
    //void tabIconChanged();
    //void tabTitleColorChanged(int status);
public slots:
    void close();
    void onExtraInfoLoaded(const ccappcore::Contact& c, const QString& lookingForValue);



    //virtual void func()const{}
    virtual int type() const{return DTT_CALL;}

private:
    ccappcore::Call _call;
    LateBoundObject<OperSettings> _operSettings;
    LateBoundObject<GlobalAddressBookController> _globalAddressController;
    LateBoundObject<CallsController> _callsController;
    QSharedPointer<CallTab> _callTab;
    //QString _title;
};


#endif // CALLFORMWIDGET_H
