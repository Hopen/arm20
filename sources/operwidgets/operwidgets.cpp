#include "operwidgets.h"

#include <QApplication>

#include "appcore/statusoptions.h"

#include "appcontext.h"
#include "logindialog.h"
#include "trayicon.h"
#include "contactsmanager.h"
#include "operevents.h"
#include "dialingpausehandler.h"

using namespace ccappcore;

#ifndef QT_NODLL
Q_EXPORT_PLUGIN2(pnp_operwidgets, OperWidgets);
#endif

OperWidgets::OperWidgets()
{
}

OperWidgets::~OperWidgets()
{
}

void OperWidgets::initialize() throw()
{
    ServiceProvider& sp = ServiceProvider::instance();
    AppContext* appContext = new AppContext(this);
    sp.registerService(appContext);

    LateBoundObject<AppSettings> appSettings;

//    connect(appContext,SIGNAL(switchLanguage(int)),
//            appSettings.instance(), SIGNAL(switchSessionLanguage(int)));

//    appContext->onOptionsApplied();

    ContactsManager* contactsManager = new ContactsManager(this);
    connect(qApp, SIGNAL(lastWindowClosed()), contactsManager, SLOT(deleteLater()));
    contactsManager->applySettings(*appSettings);

    sp.registerService(contactsManager);
    OperSettings* operSettings = new OperSettings(this);
    operSettings->applySettings(*appSettings);
    sp.registerService(operSettings);

    OperEvents* operEvents = new OperEvents(this);
    sp.registerService(operEvents);

    DialingPauseHandler* dialPauseHandler = new DialingPauseHandler(this);
    sp.registerService(dialPauseHandler);

    TrayIcon* trayIcon = new TrayIcon();
    connect(qApp, SIGNAL(lastWindowClosed()), trayIcon, SLOT(deleteLater()));

    LoginDialog* loginDlg = new LoginDialog();
    loginDlg->setAttribute(Qt::WA_DeleteOnClose);

    connect(loginDlg,SIGNAL(updateLanguage(QString)),
        appSettings.instance(), SIGNAL(switchSessionLanguage(QString)));

//    connect(loginDlg,SIGNAL(updateLanguage(QString)),
//        appSettings.instance(), SLOT(onUpdateLanguage(QString)));


    //connect(loginDlg,SIGNAL(updateLanguage()),appContext,SLOT(onOptionsApplied()));

    QTimer::singleShot(0, loginDlg, SLOT(show()));

    LateBoundObject<SessionController> session;
    if(session->isAutoLoginEnabled())
        QTimer::singleShot(0, session.instance(), SLOT(startSession()));


}

