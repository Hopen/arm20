#ifndef DOCKTABS_H
#define DOCKTABS_H

#include <QWidget>
#include <QDockWidget>
#include "appcore/lateboundobject.h"
#include "appcontext.h"

using namespace ccappcore;

class EmptyTitleBar : public QWidget
{
    Q_OBJECT
public:
    EmptyTitleBar(QWidget *parent = 0): QWidget(parent)
    {}

    QSize sizeHint() const { return minimumSizeHint(); }
    QSize minimumSizeHint() const
    {
        QDockWidget *dw = qobject_cast<QDockWidget*>(parentWidget());
        Q_ASSERT(dw != 0);
        return QSize(0,0);

    }
};

class QDockWidgetEx:public QDockWidget
{
    Q_OBJECT
public:
    explicit QDockWidgetEx(const QString &title, QWidget *parent = 0, Qt::WindowFlags flags = 0);

    explicit QDockWidgetEx(QWidget *parent = 0, Qt::WindowFlags flags = 0);

    virtual ~QDockWidgetEx();

protected:
    void closeEvent(QCloseEvent *event);
    bool event(QEvent *evt);
    QSize minimumSizeHint() const;
public:
signals:
    void closed();
    void childAdded();
};

enum eDockTabType
{
    DTT_UNKNOWN = 0,
    DTT_CALL,
    DTT_CHAT
};

class DockTab: public QDockWidgetEx
{
    Q_OBJECT

public:
    explicit DockTab(const QString &title, QWidget *parent = 0, Qt::WindowFlags flags = 0)
        :QDockWidgetEx(title,parent,flags) {}

    explicit DockTab(QWidget *parent = 0, Qt::WindowFlags flags = 0)
        :QDockWidgetEx(parent,flags) {}

    virtual ~DockTab(){}

    virtual QString title()const=0;

    //virtual void func()const=0;
    virtual int type()const=0;

};

//class DocklTitleBar : public QWidget
//{
//    struct TStyle
//    {
//        QPixmap left;
//        QPixmap center;
//        QPixmap right;

//        TStyle(){}

//        TStyle(const QPixmap &l,const QPixmap &c,const QPixmap &r)
//            :left(l),center(c),right(r)
//        {

//        }
//    };

//    Q_OBJECT
//public:
//    DocklTitleBar(const QString& title, QWidget *parent = 0);

//    QSize sizeHint() const { return minimumSizeHint(); }
//    QSize minimumSizeHint() const;

//    typedef QVector <TStyle> styleList;
//protected:
//    void paintEvent(QPaintEvent *event);
//    void mousePressEvent(QMouseEvent *event);
//public slots:
//    //void updateMask();
//    QString windowTitle()const{return _title;}
//    void setWindowTitle(const QString & title){_title = title;}

//    int activeStyle()const{return _activeStyle;}
//    void setActiveStyle(const int& style);
//    void emitAttentionTimer();
//    void startAlerting();
//    void stopAlerting();

//private:
//    QLabel * _queueStatus;
//    QPixmap _pxmIcon;
//    QPixmap _closeButton;
//    QPixmap _maximizeButton;

//    styleList _styles;

//    QString _title;
//    //bool    _active;
//    int     _activeStyle;
//    int     _attentionStyle;
//    //bool    _operAttention;
//    int     *_pCurrentStyle;
//    QTimer  _operAttentionTimer;
//};




#endif // DOCKTABS_H
