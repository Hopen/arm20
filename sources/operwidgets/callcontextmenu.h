#ifndef CALLCONTACTSMENU_H
#define CALLCONTACTSMENU_H

#include "operwidgets_global.h"
#include "appcore/callscontroller.h"
#include "appcontext.h"
using namespace ccappcore;

class OPERWIDGETS_EXPORT CallContextMenu : public QMenu
{
Q_OBJECT
public:
    explicit CallContextMenu(QWidget *parent = 0);

    void popup(ccappcore::Call);
public slots:

    void showCallForm_triggered();
    void dropCallAction_triggered();
    void redialCallAction_trigered();
    void connectCallAction_triggered();
    void joinCallAction_triggered();
    void unjoinCallAction_triggered();
    void removeConfAction_triggered();
private:
    LateBoundObject<CallsController> _callsController;
    LateBoundObject<AppContext> _appContext;
    ccappcore::Call _associatedCall;
    QPointer<QAction> _showCallFormAction;
    QPointer<QAction> _dropCallAction;
    QPointer<QAction> _redialAction;
    QPointer<QAction> _connectCallAction;
    QPointer<QAction> _joinCallToConfAction;
    QPointer<QAction> _unjoinCallToConfAction;
    QPointer<QAction> _removeConfAction;
};

#endif // CALLCONTACTSMENU_H
