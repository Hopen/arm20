/********************************************************************************
** Form generated from reading UI file 'nocallswidget.ui'
**
** Created: Tue 26. Mar 13:54:37 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NOCALLSWIDGET_H
#define UI_NOCALLSWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QTextBrowser>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_NoCallsWidget
{
public:
    QVBoxLayout *verticalLayout;
    QTextBrowser *textBrowser;

    void setupUi(QWidget *NoCallsWidget)
    {
        if (NoCallsWidget->objectName().isEmpty())
            NoCallsWidget->setObjectName(QString::fromUtf8("NoCallsWidget"));
        NoCallsWidget->resize(307, 227);
        verticalLayout = new QVBoxLayout(NoCallsWidget);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        textBrowser = new QTextBrowser(NoCallsWidget);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setFrameShape(QFrame::NoFrame);
        textBrowser->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        textBrowser->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        verticalLayout->addWidget(textBrowser);


        retranslateUi(NoCallsWidget);

        QMetaObject::connectSlotsByName(NoCallsWidget);
    } // setupUi

    void retranslateUi(QWidget *NoCallsWidget)
    {
        NoCallsWidget->setWindowTitle(QApplication::translate("NoCallsWidget", "Form", 0, QApplication::UnicodeUTF8));
        textBrowser->setHtml(QApplication::translate("NoCallsWidget", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<table border=\"0\" style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" color:#7f7f7f;\">\320\222 \320\264\320\260\320\275\320\275\321\213\320\271 \320\274\320\276\320\274\320\265\320\275\321\202 \321\203 \320\262\320\260\321\201 \320\275\320\265\321\202 </span></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" color:#7f7f7f;\">"
                        "\320\260\320\272\321\202\320\270\320\262\320\275\321\213\321\205 \320\267\320\262\320\276\320\275\320\272\320\276\320\262.</span></p></td></tr></table></body></html>", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class NoCallsWidget: public Ui_NoCallsWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NOCALLSWIDGET_H
