#ifndef DIALPAD_H
#define DIALPAD_H

#include <QWidget>

namespace Ui {
    class DialPad;
}

class DialPad : public QWidget {
    Q_OBJECT
public:
    DialPad(QWidget *parent = 0);
    ~DialPad();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::DialPad *ui;
};

#endif // DIALPAD_H
