#include "scripteventwidget.h"
#include <QTreeView>
#include <QHBoxLayout>

/*****************************************************
********************* MODEL **************************
*****************************************************/

ScriptListModel::ScriptListModel(QObject *parent): QAbstractListModel(parent)
{
    _columnsList << tr("id")
                 << tr("name")
                 << tr("desc");
}

int ScriptListModel::rowCount(const QModelIndex &parent) const
{
    return _controller->scriptsListCount();
}

int ScriptListModel::columnCount(const QModelIndex &parent) const
{
    return _columnsList.size();
}

QVariant ScriptListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (index.row() >= _controller->scriptsListCount())
        return QVariant();
    if (role == Qt::DisplayRole/* || role == Qt::EditRole*/)
    {
        RedirectListController::ScriptEventContainer *scriptList = _controller->scriptsList();
        switch (index.column())
        {
        case IdColumn:
            return (*scriptList)[index.row()].id();
        case NameColumn:
            return (*scriptList)[index.row()].name();
        case DescColumn:
            return (*scriptList)[index.row()].desc();
        default:
            return QVariant();
        }

    }
    return QVariant();
}

QVariant ScriptListModel::headerData(int section, Qt::Orientation orientation,
                                         int role) const
{
       if (role != Qt::DisplayRole)
            return QVariant();

        if (orientation == Qt::Horizontal)
            return _columnsList.value(section);
        else
            return QString("Row %1").arg(section);
}

/*****************************************************
********************** VIEW **************************
*****************************************************/

ScriptEventWidget::ScriptEventWidget(QWidget *parent) :
    QWidget(parent)
{
    _mainModel = new ScriptListModel(this);
    _mainView = new QTreeView(this);
    _mainView->setRootIsDecorated(false);
    _mainView->setModel(_mainModel);
    _mainView->setSelectionBehavior(QAbstractItemView::SelectRows);

    QHBoxLayout *mainLayout = new QHBoxLayout();
    mainLayout->addWidget(_mainView);

    setLayout(mainLayout);
}
