#include <QtGui>
#include "bblistwidget.h"
#include "mainwindow.h"
#include "chattab.h"
#include "appcore/useritemdatarole.h"

//const QString s_messageTemplate (QObject::tr("<font size=\"3\" color=\"%1\" face=\"Arial\">%2</font>"));


//LabelDelegate::LabelDelegate(QObject *parent)
//    : QStyledItemDelegate(parent)
//{
//}

//QWidget *LabelDelegate::createEditor(QWidget *parent,
//    const QStyleOptionViewItem &/* option */,
//    const QModelIndex &/* index */) const
//{
//    QLabel *editor = new QLabel(parent);
//    return editor;
//}
//void LabelDelegate::setEditorData(QWidget *editor,
//                                    const QModelIndex &index) const
//{
//    //int value = index.model()->data(index, Qt::EditRole).toInt();
//    SpamMessage m = index.data(ccappcore::AssociatedObjectRole).value<SpamMessage>();

//    QString htmlDoc = s_messageTemplate;
//    htmlDoc = htmlDoc.arg(_controller->statusColor(m.isRead())).arg(m.text());

//    QLabel *spinBox = static_cast<QLabel*>(editor);
//    spinBox->setText(htmlDoc);
//    spinBox->setWordWrap(true);
//}

////void LabelDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
////                                   const QModelIndex &index) const
////{
////    QLabel *spinBox = static_cast<QLabel*>(editor);
////    //spinBox->interpretText();
////    //int value = spinBox->value();
////    int value = 10;

////    model->setData(index, value, Qt::EditRole);
////}

//void LabelDelegate::updateEditorGeometry(QWidget *editor,
//    const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
//{
//    editor->setGeometry(option.rect);
//}

/******************************/
/********  class MODEL ********/
/******************************/

BBListModel::BBListModel(QObject *parent): QAbstractListModel(parent)
{
    _columnsList //<< GroupIdColumn
                 //<< MsgIdColumn
                 //<< EndColumn
                 //<< BeginColumn
                 //<< OperIdColumn
                 << TextColumn;

//    connect(this,SIGNAL(dataChanged(QModelIndex,QModelIndex)),this,SLOT(on_dataChanged(QModelIndex)));
    connect(_controller.instance(),SIGNAL(messageAdded()),this,SLOT(reset()));

    reset();
}

QString BBListModel::columnNameById(const qint32 &id) const
{
    switch(id)
    {
    case GroupIdColumn:
        return tr("group_id");
    case MsgIdColumn:
        return tr("msg_id");
    case EndColumn:
        return tr("end");
    case OperIdColumn:
        return tr("Оператор");
    case BeginColumn:
        return tr("Дата");
    case TextColumn:
        return tr("Текст сообщения");
    default:
        return tr("Неизвестно");
    }

    return QString();
}


int BBListModel::rowCount(const QModelIndex &parent) const
{
    return _messages.count();
}

int BBListModel::columnCount(const QModelIndex &parent) const
{
    return _columnsList.size();
}

QVariant BBListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (index.row() >= _messages.count())
        return QVariant();


    SpamMessage m = _messages[index.row()];
    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        ColDef column = _columnsList[index.column()];
        switch (column)
        {
        case GroupIdColumn:
            return m.groupId();
        case MsgIdColumn:
            return m.msgId();
        case EndColumn:
            return m.end();
        case OperIdColumn:
        {
//            ContactId personId = _operListController->createOperId((*messageList)[index.row()].operId());
//            Contact whoAdded = _operListController->findById(personId);
//            return whoAdded.name();
//            //return (*messageList)[index.row()].operId();
            return _controller->getContactName(m.operFromId());
        }
        case BeginColumn:
            return m.begin().date();
        case TextColumn:
            //return m.text();
        default:
            return QVariant();
        }

    }
    if(role == Qt::TextColorRole)
    {
        return QColor(_controller->statusColor(m.isRead()));
    }

    if(role == ccappcore::AssociatedObjectRole)
    {
        return QVariant::fromValue(m);
    }



//    if ((role == Qt::CheckStateRole) && (index.column() == StatusColumn))
//    {
//        RedirectListController::RedirProfileContainer *profileList = _controller->profilesList();
//        QString text = (*profileList)[index.row()].status();
//        return (!!text.compare("OFF"))?Qt::Checked:Qt::Unchecked;
//    }
    return QVariant();
}

QVariant BBListModel::headerData(int section, Qt::Orientation orientation,
                                         int role) const
{
       if (role != Qt::DisplayRole)
            return QVariant();

        if (orientation == Qt::Horizontal)
            return columnNameById(_columnsList.value(section));
        else
            return QString("Row %1").arg(section);
}

Qt::ItemFlags BBListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractItemModel::flags(index) | Qt::ItemIsUserCheckable;
}

bool BBListModel::insertRows(int position, int rows, const QModelIndex &index)
{
    beginInsertRows(QModelIndex(), position, position+rows-1);

    for (int row = 0; row < rows; ++row)
    {
        //_controller->createProfile(position);
    }
    endInsertRows();
    return true;
}

bool BBListModel::removeRows(int position, int rows, const QModelIndex &index)
{
    beginRemoveRows(QModelIndex(), position, position+rows-1);

    for (int row = 0; row < rows; ++row)
    {
        //_controller->removeProfile(position);
    }

    endRemoveRows();
    return true;
}

bool BBListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole)
    {
//        RedirectListController::RedirProfileContainer *profileList = _controller->profilesList();
//        switch (index.column())
//        {
//        case IdColumn:
//        {
//            (*profileList)[index.row()].setId(value.toInt());
//            break;
//        }

//        case NameColumn:
//        {
//            (*profileList)[index.row()].setName(value.toString());
//            break;
//        }
//        case TaskIdColumn:
//        {
//            (*profileList)[index.row()].setTaskId(value.toInt());
//            break;
//        }
//        case StatusColumn:
//        {
//            (*profileList)[index.row()].setStatus(value.toString());
//            break;
//        }
//        };

//        emit dataChanged (index, index);
    }

    return false;
}

//template <typename T>
//QList<T> reversed( const QList<T> & in ) {
//    QList<T> result;
//    result.reserve( in.size() ); // reserve is new in Qt 4.7
//    std::reverse_copy( in.begin(), in.end(), std::back_inserter( result ) );
//    return result;
//}

void BBListModel::reset()
{
    emit layoutAboutToBeChanged();

    //_messages = reversed(_controller->messageList());
    _messages = _controller->messageList();

    int columnIndex = _columnsList.indexOf(TextColumn);
    for(int i=0; columnIndex>=0 && i<_messages.count();++i)
    {
        QModelIndex itemIndex = index( i, columnIndex, QModelIndex() );
        messageAdd(itemIndex);
    }

    emit layoutChanged();
}


BBListWidget::BBListWidget(QWidget *parent) :
    //QWidget(parent)
    IAlarmHandler(parent)
{
    _mainModel = new BBListModel(this);
    _mainView = new QTreeView(this);
    _mainView->setRootIsDecorated(false);
    _mainView->setModel(_mainModel);
    //_mainView->setSelectionBehavior(QAbstractItemView::SelectRows);
    //_mainView->setItemDelegateForColumn(2, new LabelDelegate(_mainView));
    _mainView->setItemDelegateForColumn(0, new HTMLColumnDelegate(_mainView));
    connect(this,SIGNAL(resize()),_mainView.data(),SLOT(doItemsLayout()));

    //connect(this,SIGNAL(resize()),_mainView.data(),SLOT(doItemsLayout()));
    //_mainView->setItemDelegateForColumn(2, new LabelDelegate(_mainView));


    connect(_mainView.data(),SIGNAL(clicked(QModelIndex)),this,SLOT(on_messageSelect(QModelIndex)));
    connect(this,SIGNAL(messageHasRead()),_mainModel.data(),SLOT(reset()));
    //connect(_mainModel.data(), SIGNAL(messageAdd(QModelIndex)),this, SLOT(on_messageAdded(QModelIndex)));
    //QPushButton * testButton = new QPushButton(tr("test spam controller"), this);
    //connect(testButton,SIGNAL(clicked()),this,SLOT(testSpamController()));

    QVBoxLayout *topLayout = new QVBoxLayout();
    topLayout->addWidget(_mainView);
    //topLayout->addWidget(testButton);
    setLayout(topLayout);

}

void BBListWidget::on_messageSelect(QModelIndex index)
{
    _controller->setSelected(index.row());
    emit messageHasRead();
}

//void BBListWidget::testSpamController()
//{
//    _controller->on_c2oNeed_o2c_BBListReq(Message());
//}

void BBListWidget::connectToTimer()
{

}

int BBListWidget::getTabIndex()const
{
    return MainWindow::TB_MESSAGES;
}

QIcon BBListWidget::getTabIcon(bool /*_default*/) const
{
    return QIcon(":/resources/icons/new/tab_message16-3.png");
}

bool BBListWidget::event(QEvent *evt)
{
    QEvent::Type type = evt->type();

     if (type == QEvent::Resize)
     {
         emit resize();
     }
    return QWidget::event(evt);
}

//void BBListWidget::on_messageAdded(QModelIndex index)
//{
//    _mainView->openPersistentEditor(index);
//}
