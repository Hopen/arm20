/********************************************************************************
** Form generated from reading UI file 'contactswidget.ui'
**
** Created: Tue 26. Mar 13:54:37 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONTACTSWIDGET_H
#define UI_CONTACTSWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "contactsview.h"
#include "lineeditwithhint.h"

QT_BEGIN_NAMESPACE

class Ui_ContactsWidget
{
public:
    QAction *actionTransferCall;
    QAction *actionMakeCall;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QToolButton *toolButton;
    LineEditWithHint *searchLine;
    QToolButton *bnMakeCall;
    ContactsView *contactsView;

    void setupUi(QWidget *ContactsWidget)
    {
        if (ContactsWidget->objectName().isEmpty())
            ContactsWidget->setObjectName(QString::fromUtf8("ContactsWidget"));
        ContactsWidget->resize(222, 228);
        actionTransferCall = new QAction(ContactsWidget);
        actionTransferCall->setObjectName(QString::fromUtf8("actionTransferCall"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/resources/icons/call_transfer_32x32.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionTransferCall->setIcon(icon);
        actionMakeCall = new QAction(ContactsWidget);
        actionMakeCall->setObjectName(QString::fromUtf8("actionMakeCall"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/resources/icons/call_start_32x32.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionMakeCall->setIcon(icon1);
        verticalLayout = new QVBoxLayout(ContactsWidget);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(3, -1, 3, 2);
        toolButton = new QToolButton(ContactsWidget);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));
        toolButton->setEnabled(false);
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(toolButton->sizePolicy().hasHeightForWidth());
        toolButton->setSizePolicy(sizePolicy);
        toolButton->setMaximumSize(QSize(16, 24));
        toolButton->setAutoRaise(true);
        toolButton->setArrowType(Qt::DownArrow);

        horizontalLayout->addWidget(toolButton);

        searchLine = new LineEditWithHint(ContactsWidget);
        searchLine->setObjectName(QString::fromUtf8("searchLine"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(searchLine->sizePolicy().hasHeightForWidth());
        searchLine->setSizePolicy(sizePolicy1);
        searchLine->setMaximumSize(QSize(16777215, 24));

        horizontalLayout->addWidget(searchLine);

        bnMakeCall = new QToolButton(ContactsWidget);
        bnMakeCall->setObjectName(QString::fromUtf8("bnMakeCall"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(bnMakeCall->sizePolicy().hasHeightForWidth());
        bnMakeCall->setSizePolicy(sizePolicy2);
        bnMakeCall->setMaximumSize(QSize(24, 24));
        bnMakeCall->setLayoutDirection(Qt::RightToLeft);
        bnMakeCall->setIcon(icon1);
        bnMakeCall->setIconSize(QSize(18, 18));
        bnMakeCall->setToolButtonStyle(Qt::ToolButtonIconOnly);
        bnMakeCall->setAutoRaise(true);

        horizontalLayout->addWidget(bnMakeCall);


        verticalLayout->addLayout(horizontalLayout);

        contactsView = new ContactsView(ContactsWidget);
        contactsView->setObjectName(QString::fromUtf8("contactsView"));

        verticalLayout->addWidget(contactsView);


        retranslateUi(ContactsWidget);

        QMetaObject::connectSlotsByName(ContactsWidget);
    } // setupUi

    void retranslateUi(QWidget *ContactsWidget)
    {
        ContactsWidget->setWindowTitle(QApplication::translate("ContactsWidget", "Form", 0, QApplication::UnicodeUTF8));
        actionTransferCall->setText(QApplication::translate("ContactsWidget", "&\320\237\320\265\321\200\320\265\320\262\320\265\321\201\321\202\320\270", 0, QApplication::UnicodeUTF8));
        actionMakeCall->setText(QApplication::translate("ContactsWidget", "&\320\222\321\213\320\267\320\276\320\262", 0, QApplication::UnicodeUTF8));
        toolButton->setText(QApplication::translate("ContactsWidget", "...", 0, QApplication::UnicodeUTF8));
        searchLine->setText(QApplication::translate("ContactsWidget", "\320\237\320\276\320\270\321\201\320\272", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        bnMakeCall->setToolTip(QApplication::translate("ContactsWidget", "\320\222\321\213\320\267\320\276\320\262", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        bnMakeCall->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class ContactsWidget: public Ui_ContactsWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONTACTSWIDGET_H
