/********************************************************************************
** Form generated from reading UI file 'opt_operstatus.ui'
**
** Created: Tue 26. Mar 13:54:36 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPT_OPERSTATUS_H
#define UI_OPT_OPERSTATUS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OptionsTabOperStatus
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout_2;
    QListWidget *listStatusReason;
    QVBoxLayout *verticalLayout_3;
    QPushButton *bnAddStatus;
    QPushButton *bnRemoveStatus;
    QSpacerItem *verticalSpacer_2;
    QCheckBox *cbDisablePauseOnLogon;
    QCheckBox *cbPauseWhenDialing;

    void setupUi(QWidget *OptionsTabOperStatus)
    {
        if (OptionsTabOperStatus->objectName().isEmpty())
            OptionsTabOperStatus->setObjectName(QString::fromUtf8("OptionsTabOperStatus"));
        OptionsTabOperStatus->resize(388, 308);
        verticalLayout = new QVBoxLayout(OptionsTabOperStatus);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox = new QGroupBox(OptionsTabOperStatus);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        horizontalLayout_2 = new QHBoxLayout(groupBox);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        listStatusReason = new QListWidget(groupBox);
        listStatusReason->setObjectName(QString::fromUtf8("listStatusReason"));
        listStatusReason->setMinimumSize(QSize(200, 100));
        listStatusReason->setEditTriggers(QAbstractItemView::AnyKeyPressed|QAbstractItemView::DoubleClicked|QAbstractItemView::EditKeyPressed|QAbstractItemView::SelectedClicked);
        listStatusReason->setAlternatingRowColors(true);
        listStatusReason->setSelectionBehavior(QAbstractItemView::SelectRows);

        horizontalLayout_2->addWidget(listStatusReason);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        bnAddStatus = new QPushButton(groupBox);
        bnAddStatus->setObjectName(QString::fromUtf8("bnAddStatus"));

        verticalLayout_3->addWidget(bnAddStatus);

        bnRemoveStatus = new QPushButton(groupBox);
        bnRemoveStatus->setObjectName(QString::fromUtf8("bnRemoveStatus"));

        verticalLayout_3->addWidget(bnRemoveStatus);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_2);


        horizontalLayout_2->addLayout(verticalLayout_3);


        verticalLayout->addWidget(groupBox);

        cbDisablePauseOnLogon = new QCheckBox(OptionsTabOperStatus);
        cbDisablePauseOnLogon->setObjectName(QString::fromUtf8("cbDisablePauseOnLogon"));

        verticalLayout->addWidget(cbDisablePauseOnLogon);

        cbPauseWhenDialing = new QCheckBox(OptionsTabOperStatus);
        cbPauseWhenDialing->setObjectName(QString::fromUtf8("cbPauseWhenDialing"));

        verticalLayout->addWidget(cbPauseWhenDialing);


        retranslateUi(OptionsTabOperStatus);

        QMetaObject::connectSlotsByName(OptionsTabOperStatus);
    } // setupUi

    void retranslateUi(QWidget *OptionsTabOperStatus)
    {
        OptionsTabOperStatus->setWindowTitle(QApplication::translate("OptionsTabOperStatus", "MainWindow", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("OptionsTabOperStatus", "\320\237\321\200\320\270\321\207\320\270\320\275\321\213 \320\277\320\260\321\203\320\267\321\213", 0, QApplication::UnicodeUTF8));
        bnAddStatus->setText(QApplication::translate("OptionsTabOperStatus", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        bnRemoveStatus->setText(QApplication::translate("OptionsTabOperStatus", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        cbDisablePauseOnLogon->setToolTip(QApplication::translate("OptionsTabOperStatus", "\320\225\321\201\320\273\320\270 \320\262\320\272\320\273\321\216\321\207\320\265\320\275\320\276, \320\272\320\260\321\200\321\202\320\276\321\207\320\272\320\260 \320\267\320\262\320\276\320\275\320\272\320\260 \320\261\321\203\320\264\320\265\321\202 \320\277\320\276\320\272\320\260\320\267\320\260\320\275\320\260 \321\201\321\200\320\260\320\267\321\203 \320\277\320\276 \320\277\321\200\320\270\321\205\320\276\320\264\321\203 \320\267\320\262\320\276\320\275\320\272\320\260 \320\262 \321\201\320\270\321\201\321\202\320\265\320\274\321\203. \320\222 \320\277\321\200\320\276\321\202\320\270\320\262\320\275\320\276\320\274 \321\201\320\273\321\203\321\207\320\260\320\265, \321\207\321\202\320\276\320\261\321\213 \320\277\320\276\320\272\320\260\320\267\320\260\321\202\321\214 \320\272\320\260\321\200\321\202\320\276\321\207\320\272\321\203 \320\267\320\262\320\276\320\275\320\272\320\260, \320\275\320\265\320\276\320\261\321\205\320\276\320\264\320\270\320\274\320\276 \320\262\321\213\320\261\321\200\320\260\321"
                        "\202\321\214 \320\267\320\262\320\276\320\275\320\276\320\272 \320\270\320\267 \321\201\320\277\320\270\321\201\320\272\320\260 \320\270 \320\264\320\262\320\260\320\266\320\264\321\213 \321\211\320\265\320\273\320\272\320\275\321\203\321\202\321\214 \320\274\321\213\321\210\321\214\321\216 \320\277\320\276 \320\275\320\265\320\274\321\203. ", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        cbDisablePauseOnLogon->setText(QApplication::translate("OptionsTabOperStatus", "\320\220\320\262\321\202\320\276\320\274\320\260\321\202\320\270\321\207\320\265\321\201\320\272\320\270 \321\201\320\275\320\270\320\274\320\260\321\202\321\214\321\201\321\217 \321\201 \320\277\320\260\321\203\320\267\321\213 \320\277\321\200\320\270 \320\273\320\276\320\263\320\270\320\275\320\265", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        cbPauseWhenDialing->setToolTip(QApplication::translate("OptionsTabOperStatus", "\320\225\321\201\320\273\320\270 \320\262\320\272\320\273\321\216\321\207\320\265\320\275\320\276, \320\272\320\260\321\200\321\202\320\276\321\207\320\272\320\260 \320\267\320\262\320\276\320\275\320\272\320\260 \320\261\321\203\320\264\320\265\321\202 \320\277\320\276\320\272\320\260\320\267\320\260\320\275\320\260 \321\201\321\200\320\260\320\267\321\203 \320\277\320\276 \320\277\321\200\320\270\321\205\320\276\320\264\321\203 \320\267\320\262\320\276\320\275\320\272\320\260 \320\262 \321\201\320\270\321\201\321\202\320\265\320\274\321\203. \320\222 \320\277\321\200\320\276\321\202\320\270\320\262\320\275\320\276\320\274 \321\201\320\273\321\203\321\207\320\260\320\265, \321\207\321\202\320\276\320\261\321\213 \320\277\320\276\320\272\320\260\320\267\320\260\321\202\321\214 \320\272\320\260\321\200\321\202\320\276\321\207\320\272\321\203 \320\267\320\262\320\276\320\275\320\272\320\260, \320\275\320\265\320\276\320\261\321\205\320\276\320\264\320\270\320\274\320\276 \320\262\321\213\320\261\321\200\320\260\321"
                        "\202\321\214 \320\267\320\262\320\276\320\275\320\276\320\272 \320\270\320\267 \321\201\320\277\320\270\321\201\320\272\320\260 \320\270 \320\264\320\262\320\260\320\266\320\264\321\213 \321\211\320\265\320\273\320\272\320\275\321\203\321\202\321\214 \320\274\321\213\321\210\321\214\321\216 \320\277\320\276 \320\275\320\265\320\274\321\203. ", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        cbPauseWhenDialing->setText(QApplication::translate("OptionsTabOperStatus", "\320\241\321\202\320\260\320\262\320\270\321\202\321\214 \320\275\320\260 \320\277\320\260\321\203\320\267\321\203 \320\277\321\200\320\270 \320\275\320\260\320\261\320\276\321\200\320\265 \320\275\320\276\320\274\320\265\321\200\320\260", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class OptionsTabOperStatus: public Ui_OptionsTabOperStatus {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPT_OPERSTATUS_H
