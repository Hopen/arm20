#include "docktabswidget.h"
#include "CallFormWidget.h"
#include "chatformwidget.h"
#include "appcore/operlistcontroller.h"
#include "messagetrayicon.h"

DockTabsController::DockTabsController(QObject *parent)
    :QObject(parent), _toggleMinMaxWindow(false)
{
    connect(_appContext.instance(), SIGNAL(onShowCallForm(ccappcore::Call)),
            this, SLOT(on_showCallForm(ccappcore::Call)));
    connect(_appContext.instance(), SIGNAL(onShowChatForm(ccappcore::Contact)),
            this, SLOT(on_showChatForm(ccappcore::Contact)));
    connect(_callsController.instance(), SIGNAL(callAssigned(ccappcore::Call)),
            this, SLOT(callStarted(ccappcore::Call)));
    connect(_callsController.instance(), SIGNAL(callStarted(ccappcore::Call)),
            this, SLOT(callStarted(ccappcore::Call)));
    connect(_spamController.instance(),SIGNAL(chatMsgAdded(SpamMessage)),
            this,SLOT(chatMessageIn(SpamMessage)));
    connect(_appContext.instance(), SIGNAL(windowMinimized(bool)),
            this, SLOT(on_toggleMinMaxWindow(bool)));

    connect(this, SIGNAL(raiseDockTab(DockTab*)),
            this, SLOT(on_raiseOrAdd(DockTab*)));
    connect(this, SIGNAL(addDockTab(DockTab*,DockTab*,int)),
            this, SLOT(on_raiseOrAdd(DockTab*)));

}

//void DockTabsController::AddTab(QWidget *tab)
//{
//    _tabs.push_back(tab);
//}

void DockTabsController::callStarted(ccappcore::Call call)
{
    if (!_callsController->isSessionOwnerCall(call))
        return;
    if(!_operSettings->showCallFormImmediately())
        return;

    on_showCallForm(call/*, call.isIncoming()&&call.isAssigned()*/);
    //_appContext->showCallForm(call);
}

void DockTabsController::chatMessageIn(SpamMessage message)
{
    //if (_callsController->sessionOwner().contactId().toInt() != message.operId())
    //    return;
    if (message.msgType() != MT_IN)
        return;
    ContactId personId = _opersController->createOperId(message.operFromId());
    Contact c = _opersController->findById(personId);
    if (!c.isValid())
    {
        RosterId rosterId = _jabberController->createRosterId(message.operFromId());
        c = _jabberController->findById(rosterId);
    }

    if (!c.isValid())
        return;

    DockTab * tab = findDockByContact(c);
    if (!tab)
        _spamController->showBalloon(message);
        //activateIcon(message);
    else
    {
        bool active = tab->isActiveWindow();
        active |= tab->isHidden();
        active |= tab->isVisible();

        if (!tab->isActiveWindow())
            _spamController->showBalloon(message);
            //activateIcon(message);
        else
        {
            on_raiseOrAdd(tab);
            //_spamController->setAllMessagesRead(c->contactId().toInt());
        }
    }
}

void DockTabsController::on_showCallForm(ccappcore::Call c/*, bool alerting*/)
{
//    CallFormWidget* callform = 0;
//    for (TabsCollector::iterator it=_tabs.begin();it!=_tabs.end();++it)
//    {
//        DockTab* tab = *it;
//        if (tab->type() == DTT_CALL)
//        {
//            CallFormWidget* callTab = qobject_cast<CallFormWidget*>(tab);
//            if (callTab && c == *(callTab->assignedCall()))
//            {
//                callform = callTab;
//                break;
//            }
//        }
//    }

//    if (_toggleMinMaxWindow) //if window minimized
//        return;

    if (DockTab *callform = findDockByCall(c))
    {
        emit raiseDockTab(callform);
    }
    else
    {
        CallFormWidget* callform = new CallFormWidget(c,_callsController->callShortInfo(c)/*,alerting*/);
        callform->setAttribute(Qt::WA_DeleteOnClose);
        connect(callform,SIGNAL(closed()),this,SLOT(on_childTabClosed()));
        DockTab * where = *_tabs.begin();
        _tabs.push_front(callform);
        emit addDockTab(callform,where,_tabs.size());
    }
}

void DockTabsController::on_showChatForm(ccappcore::Contact c)
{
//    if (_toggleMinMaxWindow) //if window minimized
//        return;

//    if (c.contactId() == _callsController->sessionOwner().contactId())
//        return;
    if (!c.canSendMessage())
        return;

//    ChatFormWidget* chatForm = new ChatFormWidget(c, c.data(Contact::Name).toString());
//    chatForm->setAttribute(Qt::WA_DeleteOnClose);
//    connect(chatForm,SIGNAL(closed()),this,SLOT(on_childTabClosed()));
//    emit addDockTab(chatForm);

    if (DockTab *chatForm = findDockByContact(c))
    {
        emit raiseDockTab(chatForm);
    }
    else
    {
        ChatFormWidget* chatForm = new ChatFormWidget(c, c.data(Contact::Name).toString());
        chatForm->setAttribute(Qt::WA_DeleteOnClose);
        connect(chatForm,SIGNAL(closed()),this,SLOT(on_childTabClosed()));
        DockTab * where = *_tabs.begin();
        _tabs.push_front(chatForm);
        emit addDockTab(chatForm,where,_tabs.size());

        connect(chatForm,SIGNAL(visibilityChanged(bool)),this,SLOT(on_raise(bool)));
    }

}

void DockTabsController::on_childTabClosed()
{
    DockTab* closedTab = qobject_cast<DockTab*>(sender());
    //foreach(DockTab* tab, _tabs)
    for (TabsCollector::Iterator it=_tabs.begin();it!=_tabs.end();++it)
    {
        if (*it == closedTab)
        {
            _tabs.erase(it); // !!! check destructor calling
            break;
        }

    }

    if (_tabs.empty())
        emit tabsClear();
}

void DockTabsController::activateIcon(const SpamMessage &message)
{
    MessageTrayIcon* icon  = new MessageTrayIcon(message);
    //connect(icon, SIGNAL(activated()), icon, SLOT(deleteLater()));
}

DockTab* DockTabsController::findDockByCall(ccappcore::Call c)
{
    DockTab* value = 0;
    foreach(DockTab* tab, _tabs)
    {
        if(tab->type() == DTT_CALL)
        {
            CallFormWidget* callTab = qobject_cast<CallFormWidget*>(tab);
            if (callTab && c == *(callTab->assignedCall()))
            {
                value = tab;
                break;
            }
        }
    }

    return value;
}

DockTab* DockTabsController::findDockByContact(ccappcore::Contact c)
{
    DockTab* value = 0;
    foreach(DockTab* tab, _tabs)
    {
        if(tab->type() == DTT_CHAT)
        {
            ChatFormWidget* chatTab = qobject_cast<ChatFormWidget*>(tab);
            if (chatTab && c == *(chatTab->assignedContact()))
            {
                value = tab;
                break;
            }
        }
    }

    return value;
}
void DockTabsController::on_toggleMinMaxWindow(bool checked)
{
    _toggleMinMaxWindow = checked;
}

void DockTabsController::on_raiseOrAdd(DockTab *tab)
{
    if (ChatFormWidget* chatTab = qobject_cast<ChatFormWidget*>(tab))
    {
        Contact *c = chatTab->assignedContact();
        if (c && c->isValid())
            _spamController->setAllMessagesRead(c->contactId().toInt());
    }
}

void DockTabsController::on_raise(bool bVisiable)
{
    //int test = 0;
    if (!bVisiable)
        return;

    if (ChatFormWidget* chatTab = qobject_cast<ChatFormWidget*>(sender()))
    {
        on_raiseOrAdd(chatTab);
    }
}

//bool SupportWindow::event(QEvent *evt)
//{
//    QEvent::Type type = evt->type();
//    if (type == QEvent::LayoutRequest)
//    {
//        int test = 0;
//    }


//    QMainWindow::event(evt);
//}

DockTabsWidget::DockTabsWidget(const QString &title, QWidget *parent, Qt::WindowFlags flags) :
    QDockWidgetEx(title,parent,flags)
{
    _window = new QMainWindow(this);
    _window->setTabPosition(Qt::TopDockWidgetArea,QTabWidget::North);

    QWidget *container = new QWidget();
    QHBoxLayout *mainLayout = new QHBoxLayout();
    mainLayout->addWidget(_window);
    container->setLayout(mainLayout);

    setAllowedAreas( Qt::AllDockWidgetAreas );

    setFloating(false);
    setWidget(container);


    _tabsController = new DockTabsController(this);
    connect(_tabsController.data(),SIGNAL(addDockTab(DockTab*,DockTab*,const int&)),
            this,SLOT(addDockTab(DockTab*,DockTab*,const int&)));
    connect(_tabsController.data(),SIGNAL(raiseDockTab(DockTab*)),
            this,SLOT(raiseDockTab(DockTab*)));
    connect(_tabsController.data(),SIGNAL(tabsClear()),
            this,SLOT(on_tabsClear()));

}

DockTabsWidget::DockTabsWidget(QWidget *parent, Qt::WindowFlags flags)
    :QDockWidgetEx(parent,flags)
{
    DockTabsWidget(tr(""),parent,flags);
}

DockTabsWidget::~DockTabsWidget()
{
    qDebug () << tr("DockTabsWidget::~DockTabsWidget()");
}

void DockTabsWidget::addDockTab(DockTab *tab, DockTab* after, const int& tabQuantity)
{
    _window->addDockWidget(Qt::TopDockWidgetArea, tab);
    QApplication::processEvents();

//    if (_tabsController->_tabs.size() == 1)
//    {
//        _window->tabifyDockWidget((DockTab*)*(_tabsController->_tabs.begin()), tab);
//    }
//    if (_tabsController->_tabs.size() > 1)
//    {
//        _window->splitDockWidget((DockTab*)*(_tabsController->_tabs.begin()), tab, Qt::Horizontal);
//        QApplication::processEvents();
//    }

    if (tabQuantity == 2)
    {
        _window->tabifyDockWidget(after, tab);
    }
    if (tabQuantity > 2)
    {
        _window->splitDockWidget(after, tab, Qt::Horizontal);
        QApplication::processEvents();
    }

    //connect(tab,SIGNAL(closed    ()),this,SLOT(on_childTabClosed()));
    //connect(tab,SIGNAL(childAdded()),this,SLOT(on_childTabAdded ()));
    //_tabs.push_front(tab);

    tab->raise();

    QTabBar *tabBar = _window->findChild<QTabBar *>();
    if (tabBar && !tabBar->isMovable())
        tabBar->setMovable(true);

    setSizePolicy(QSizePolicy::Minimum,QSizePolicy::MinimumExpanding);
    show();

}

void DockTabsWidget::raiseDockTab(DockTab *tab)
{
    tab->raise();
    setSizePolicy(QSizePolicy::Minimum,QSizePolicy::MinimumExpanding);
    show();
}

void DockTabsWidget::on_tabsClear()
{
    //close();
    hide();
}

