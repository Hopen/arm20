/********************************************************************************
** Form generated from reading UI file 'opt_opertools.ui'
**
** Created: Tue 26. Mar 13:54:36 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPT_OPERTOOLS_H
#define UI_OPT_OPERTOOLS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QListWidget>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OptionsTabOperToolForm
{
public:
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupOpertoolsDragDropWidget;
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBaseTools;
    QHBoxLayout *horizontalLayout_2;
    QListWidget *listBaseTools;
    QVBoxLayout *verticalLayout;
    QToolButton *toolBtnToOper;
    QToolButton *toolBtnToBase;
    QToolButton *toolBtnToOperAll;
    QToolButton *toolBtnToBaseAll;
    QGroupBox *groupOperTools;
    QHBoxLayout *horizontalLayout_3;
    QListWidget *listOperTools;

    void setupUi(QWidget *OptionsTabOperToolForm)
    {
        if (OptionsTabOperToolForm->objectName().isEmpty())
            OptionsTabOperToolForm->setObjectName(QString::fromUtf8("OptionsTabOperToolForm"));
        OptionsTabOperToolForm->resize(461, 348);
        verticalLayout_2 = new QVBoxLayout(OptionsTabOperToolForm);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupOpertoolsDragDropWidget = new QGroupBox(OptionsTabOperToolForm);
        groupOpertoolsDragDropWidget->setObjectName(QString::fromUtf8("groupOpertoolsDragDropWidget"));
        horizontalLayout = new QHBoxLayout(groupOpertoolsDragDropWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        groupBaseTools = new QGroupBox(groupOpertoolsDragDropWidget);
        groupBaseTools->setObjectName(QString::fromUtf8("groupBaseTools"));
        horizontalLayout_2 = new QHBoxLayout(groupBaseTools);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        listBaseTools = new QListWidget(groupBaseTools);
        listBaseTools->setObjectName(QString::fromUtf8("listBaseTools"));
        listBaseTools->setDragEnabled(true);
        listBaseTools->setDragDropMode(QAbstractItemView::DragDrop);
        listBaseTools->setDefaultDropAction(Qt::MoveAction);
        listBaseTools->setSortingEnabled(true);

        horizontalLayout_2->addWidget(listBaseTools);


        horizontalLayout->addWidget(groupBaseTools);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        toolBtnToOper = new QToolButton(groupOpertoolsDragDropWidget);
        toolBtnToOper->setObjectName(QString::fromUtf8("toolBtnToOper"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/resources/icons/new/navigate_right.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolBtnToOper->setIcon(icon);

        verticalLayout->addWidget(toolBtnToOper);

        toolBtnToBase = new QToolButton(groupOpertoolsDragDropWidget);
        toolBtnToBase->setObjectName(QString::fromUtf8("toolBtnToBase"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/resources/icons/new/navigate_left.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolBtnToBase->setIcon(icon1);

        verticalLayout->addWidget(toolBtnToBase);

        toolBtnToOperAll = new QToolButton(groupOpertoolsDragDropWidget);
        toolBtnToOperAll->setObjectName(QString::fromUtf8("toolBtnToOperAll"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/resources/icons/new/navigate_right2.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolBtnToOperAll->setIcon(icon2);

        verticalLayout->addWidget(toolBtnToOperAll);

        toolBtnToBaseAll = new QToolButton(groupOpertoolsDragDropWidget);
        toolBtnToBaseAll->setObjectName(QString::fromUtf8("toolBtnToBaseAll"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/resources/icons/new/navigate_left2.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolBtnToBaseAll->setIcon(icon3);

        verticalLayout->addWidget(toolBtnToBaseAll);


        horizontalLayout->addLayout(verticalLayout);

        groupOperTools = new QGroupBox(groupOpertoolsDragDropWidget);
        groupOperTools->setObjectName(QString::fromUtf8("groupOperTools"));
        horizontalLayout_3 = new QHBoxLayout(groupOperTools);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        listOperTools = new QListWidget(groupOperTools);
        listOperTools->setObjectName(QString::fromUtf8("listOperTools"));
        listOperTools->setDragEnabled(true);
        listOperTools->setDragDropMode(QAbstractItemView::DragDrop);
        listOperTools->setDefaultDropAction(Qt::MoveAction);

        horizontalLayout_3->addWidget(listOperTools);


        horizontalLayout->addWidget(groupOperTools);


        verticalLayout_2->addWidget(groupOpertoolsDragDropWidget);


        retranslateUi(OptionsTabOperToolForm);

        QMetaObject::connectSlotsByName(OptionsTabOperToolForm);
    } // setupUi

    void retranslateUi(QWidget *OptionsTabOperToolForm)
    {
        OptionsTabOperToolForm->setWindowTitle(QApplication::translate("OptionsTabOperToolForm", "Form", 0, QApplication::UnicodeUTF8));
        groupOpertoolsDragDropWidget->setTitle(QApplication::translate("OptionsTabOperToolForm", "\320\235\320\260\321\201\321\202\321\200\320\276\320\271\320\272\320\260 \321\215\320\273\320\265\320\274\320\265\320\275\321\202\320\276\320\262 \321\203\320\277\321\200\320\260\320\262\320\273\320\265\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        groupBaseTools->setTitle(QApplication::translate("OptionsTabOperToolForm", "\320\236\321\201\320\275\320\276\320\262\320\275\321\213\320\265 \321\204\321\203\320\275\320\272\321\206\320\270\320\270", 0, QApplication::UnicodeUTF8));
        toolBtnToOper->setText(QApplication::translate("OptionsTabOperToolForm", "...", 0, QApplication::UnicodeUTF8));
        toolBtnToBase->setText(QApplication::translate("OptionsTabOperToolForm", "...", 0, QApplication::UnicodeUTF8));
        toolBtnToOperAll->setText(QApplication::translate("OptionsTabOperToolForm", "...", 0, QApplication::UnicodeUTF8));
        toolBtnToBaseAll->setText(QApplication::translate("OptionsTabOperToolForm", "...", 0, QApplication::UnicodeUTF8));
        groupOperTools->setTitle(QApplication::translate("OptionsTabOperToolForm", "\320\244\321\203\320\275\320\272\321\206\320\270\320\270 \320\276\320\277\320\265\321\200\320\260\321\202\320\276\321\200\320\260", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class OptionsTabOperToolForm: public Ui_OptionsTabOperToolForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPT_OPERTOOLS_H
