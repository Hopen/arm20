#include "applicationwidget.h"
#include "QtGui"
#include "toolbuttonoperstatus.h"
#include "appcore/filteredmodel.h"
#include "appcore/domainobjectfilter.h"


ApplicationToolBar::ApplicationToolBar(bool minimize, bool stayOnTop, QWidget *parent)
    :QToolBar(parent)/*,_minimized(minimize)*/
{
    //setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    //bool maximize = !minimize;
    QIcon icon(":/resources/icons/new/pawn_glass_white.png");
    //QIcon icon("D:/Projects/new/trunk/sources/operwidgets/resources/icons/new/pawn_glass_white.png");
    QPixmap pixmap = icon.pixmap(QSize(16, 16), QIcon::Normal, QIcon::On);
    //QPixmap pxm(":/resources/icons/new/pawn_glass_red.png");
    _queueStatus = new QLabel(tr("QS"));
    _queueStatus->setPixmap(pixmap);
    this->addWidget(_queueStatus);

    QString Name = tr("%1 ( 0 )" )
            .arg(_sessionController->sessionOwner().name());
    _opername = new QLabel(Name);
    this->addWidget(_opername);

    _toolButtonStatus = new ToolButtonOperStatusEx(this);
    this->addWidget(_toolButtonStatus);

    if (!minimize)
    {
        this->addSeparator();

        QWidget* spacer = new QWidget();
        spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        // toolBar is a pointer to an existing toolbar
        this->addWidget(spacer);

        _mailButton  = new QToolButton(this);
        _mailButton->setIcon(QIcon(":/resources/icons/new/message green.png"));
        _mailButton->setAutoRaise(true);
        _mailButton->setEnabled(false);
        _mailButton->setToolTip(tr("Голосовая почта"));

        _helpButton = new QToolButton(this);
        _helpButton->setIcon(QIcon(":/resources/icons/new/help on.png"));
        _helpButton->setAutoRaise(true);
        _helpButton->setEnabled(false);
        _helpButton->setToolTip(tr("Помощь"));

        _soundButton = new QToolButton(this);
        _soundButton->setIcon(QIcon(":/resources/icons/new/sound on.png"));
        _soundButton->setAutoRaise(true);
        _soundButton->setToolTip(tr("Громкость"));
        _soundButton->setDisabled(true);

        _settingsButton = new QToolButton(this);
        _settingsButton->setIcon(QIcon(":/resources/icons/new/settings on.png"));
        _settingsButton->setAutoRaise(true);
        _settingsButton->setToolTip(tr("Настройки"));
        connect(_settingsButton.data(),SIGNAL(clicked()),this,SIGNAL(displaySettings()));

        this->addWidget(_mailButton);
        this->addWidget(_soundButton);
        this->addWidget(_settingsButton);
        this->addWidget(_helpButton);

    }

    _miniButton = new QToolButton(this);
    _miniButton->setIcon(QIcon(":/resources/icons/new/mini on.png"));
    _miniButton->setAutoRaise(true);
    _miniButton->setCheckable(true);
    _miniButton->setChecked(minimize);
    _miniButton->setToolTip(tr("Минимизировать"));
    this->addWidget(_miniButton);

    //if (stayOnTop)
    //{
        _staysOnTop = new QToolButton(this);
        _staysOnTop->setIcon(QIcon(":/resources/icons/new/up on16.png"));
        _staysOnTop->setAutoRaise(true);
        _staysOnTop->setCheckable(true);
        _staysOnTop->setChecked(stayOnTop);
        _staysOnTop->setToolTip(tr("Закрепить поверх всех окон"));
        this->addWidget(_staysOnTop);
        //connect(_staysOnTop.data(),SIGNAL(toggled(bool)),this,SIGNAL(staysOnTopWindow(bool)));
        connect(_staysOnTop.data(),SIGNAL(toggled(bool)),_appContext.instance(),SIGNAL(staysOnTop(bool)));
    //}


    connect(_callsController.instance(),SIGNAL(callStateChanged(ccappcore::Call)), this, SLOT(on_callListChanged()) );
    //connect(_miniButton.data(),SIGNAL(toggled(bool)),this,SIGNAL(minimizeWindow(bool)));
    //connect(_miniButton.data(),SIGNAL(toggled(bool)),this,SLOT(on_minimizedWindow(bool)));
    connect(_miniButton.data(),SIGNAL(clicked(bool)),_appContext.instance(),SIGNAL(windowMinimized(bool)));
    connect(_appContext.instance(),SIGNAL(windowMinimized(bool)),this,SLOT(on_minimizedWindow(bool)));
    //connect(_soundButton.data(),SIGNAL(clicked()),this,SIGNAL(showVolumeWidget()));
}

void ApplicationToolBar::on_callListChanged()
{
    FilteredModel<Call>::FilterCriteria _ownerCall = new IsSessionOwnerCall();
    QString Name = tr("%1 ( %2 из %3)" )
            .arg(_sessionController->sessionOwner().name())
            .arg(_callsController->findCalls(*_ownerCall.data()).count())
            .arg(_callsController->countCalls())
            ;

    _opername->setText(Name);

}

void ApplicationToolBar::on_minimizedWindow(bool toggled)
{
    _miniButton->setChecked(toggled);
    _miniButton->setToolTip(toggled?tr("Развернуть"):tr("Минимизировать"));
    ////emit minimizeWindow(toggled);

}

//void ApplicationToolBar::minimize(bool enable)
//{
////    if (enable && !_miniButton->isChecked())
////        _miniButton->setChecked(enable);

////    int width = 0;
////    if (enable)
////    {
////        width += _queueStatus->width();
////        _opername->hide();
////    }
//}

//void ApplicationToolBar::onBtnSoundClicked()
//{
//    //QPointer<QSlider> slider = new QSlider(Qt::Vertical, widget);
//    _soundButton->hide();
//}

