#include "nocontactswidget.h"
#include "ui_nocontactswidget.h"
#include <QtGui/QTextBrowser>

#include <QDesktopServices>

const char* NoContactsHtml = "<html><body>"
        "<table border='0'' style='-qt-table-type: root; margin:4px;'>"
        "<tr>"
        "<td style='border: none;'>"
        "<p align='center' style='margin:0px;-qt-block-indent:0; text-indent:0px;'>"
        "<span style='font-size:11pt; color:#7f7f7f;'>%1</span>"
        "</p>"
        "</td>"
        "</tr>"
        "</table>"
        "</body></html>";

NoContactsWidget::NoContactsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NoContactsWidget)
{
    ui->setupUi(this);    

    connect(ui->textBrowser,SIGNAL(anchorClicked(QUrl)),this,SIGNAL(anchorClicked(QUrl)));
}

NoContactsWidget::~NoContactsWidget()
{
    delete ui;
}

void NoContactsWidget::setHtml(const QString& html)
{
    ui->textBrowser->setHtml( QString(NoContactsHtml).arg(html) );
}

NoContactsWidget2::NoContactsWidget2(QWidget *parent):
    QWidget(parent)
{
    _textBrowser = new QTextBrowser(this);
    connect(_textBrowser.data(),SIGNAL(anchorClicked(QUrl)),this,SIGNAL(anchorClicked(QUrl)));
}

void NoContactsWidget2::setHtml(const QString& html)
{
    _textBrowser->setHtml( QString(NoContactsHtml).arg(html) );
}
