#include "localcallhistorywidget.h"
#include "appcore/domainobjectfilter.h"
#include "mainwindow.h"

LocalCallHistoryModel::LocalCallHistoryModel(QObject *parent): CallsModel(parent)
{
    _columnsList.clear();
    _columnsList
            <<AbonPhoneColumn
            <<CallingPhoneColumn
            <<CallTypeColumn
            <<CallStateColumn
            //<<GroupNameColumn
            <<BeginTimeColumn
            //<<StateTimeColumn
            //<<PriorityColumn
            //<<ExtraInfoColumn
            //<<LastOperNameColumn
            <<CallLengthColumn;
            //<<CallLengthElapsedColumn;


    connect(_controller.instance(),
            SIGNAL(callStateChanged(ccappcore::Call)),
            this,
            SLOT(on_callStateChanged(ccappcore::Call)),
            Qt::QueuedConnection);
}

void LocalCallHistoryModel::on_callStateChanged(ccappcore::Call c)
{
    if(c.isValid() && _filter(c))
        _localCalls[c.callId()] = c;
}

void LocalCallHistoryModel::applyFilter(const PredicateGroup<Call> &filter)
{
    emit layoutAboutToBeChanged();
    _filter = filter;

    _calls = _localCalls.values();
    qSort(_calls);
    emit layoutChanged();

}

LocalCallHistoryWidget::LocalCallHistoryWidget(QWidget *parent) :
    //QWidget(parent)
    IAlarmHandler(parent)
{
    IAlarmHandler::init();

    _mainModel = new LocalCallHistoryModel();
    _mainModel->addFilter(new IsSessionOwnerCall());
    _mainView = new QTreeView(this);
    _mainView->setRootIsDecorated(false);
    _mainView->setModel(_mainModel);
    _mainView->setSelectionBehavior(QAbstractItemView::SelectRows);

    QVBoxLayout *topLayout = new QVBoxLayout();
    topLayout->addWidget(_mainView);
    setLayout(topLayout);
}

void LocalCallHistoryWidget::connectToTimer()
{

}

int LocalCallHistoryWidget::getTabIndex()const
{
    return MainWindow::TB_HISTORY;
}

QIcon LocalCallHistoryWidget::getTabIcon(bool _default) const
{
    return QIcon(":/resources/icons/new/tab_stat16-3.png");
}
