/********************************************************************************
** Form generated from reading UI file 'opt_main.ui'
**
** Created: Thu 19. Nov 12:31:13 2015
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPT_MAIN_H
#define UI_OPT_MAIN_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OptionsTabMainWidget
{
public:
    QVBoxLayout *verticalLayout;
    QCheckBox *cbAutoUpdate;
    QGridLayout *gridLayout;
    QGridLayout *gridLayout_2;
    QComboBox *comboBoxLogLevel;
    QLineEdit *tbLogFile;
    QLabel *lblLogFile;
    QLabel *lblLogLevel;
    QGroupBox *gbWaitTimeOut;
    QGridLayout *gridLayout_3;
    QLabel *lbWaitForAnswer;
    QLabel *lbWaitForClickMessage;
    QLineEdit *eWaitForAnswer;
    QLineEdit *eWaitForClick;
    QHBoxLayout *horizontalLayout;
    QLabel *lbAutoTransfer;
    QLineEdit *eAutoTransfer;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *OptionsTabMainWidget)
    {
        if (OptionsTabMainWidget->objectName().isEmpty())
            OptionsTabMainWidget->setObjectName(QString::fromUtf8("OptionsTabMainWidget"));
        OptionsTabMainWidget->resize(301, 365);
        verticalLayout = new QVBoxLayout(OptionsTabMainWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        cbAutoUpdate = new QCheckBox(OptionsTabMainWidget);
        cbAutoUpdate->setObjectName(QString::fromUtf8("cbAutoUpdate"));

        verticalLayout->addWidget(cbAutoUpdate);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        comboBoxLogLevel = new QComboBox(OptionsTabMainWidget);
        comboBoxLogLevel->setObjectName(QString::fromUtf8("comboBoxLogLevel"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(comboBoxLogLevel->sizePolicy().hasHeightForWidth());
        comboBoxLogLevel->setSizePolicy(sizePolicy);
        comboBoxLogLevel->setMinimumSize(QSize(120, 20));

        gridLayout_2->addWidget(comboBoxLogLevel, 1, 1, 1, 1);

        tbLogFile = new QLineEdit(OptionsTabMainWidget);
        tbLogFile->setObjectName(QString::fromUtf8("tbLogFile"));
        tbLogFile->setMinimumSize(QSize(0, 20));

        gridLayout_2->addWidget(tbLogFile, 0, 1, 1, 1);

        lblLogFile = new QLabel(OptionsTabMainWidget);
        lblLogFile->setObjectName(QString::fromUtf8("lblLogFile"));

        gridLayout_2->addWidget(lblLogFile, 0, 0, 1, 1);

        lblLogLevel = new QLabel(OptionsTabMainWidget);
        lblLogLevel->setObjectName(QString::fromUtf8("lblLogLevel"));

        gridLayout_2->addWidget(lblLogLevel, 1, 0, 1, 1);


        gridLayout->addLayout(gridLayout_2, 0, 0, 1, 1);


        verticalLayout->addLayout(gridLayout);

        gbWaitTimeOut = new QGroupBox(OptionsTabMainWidget);
        gbWaitTimeOut->setObjectName(QString::fromUtf8("gbWaitTimeOut"));
        gridLayout_3 = new QGridLayout(gbWaitTimeOut);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        lbWaitForAnswer = new QLabel(gbWaitTimeOut);
        lbWaitForAnswer->setObjectName(QString::fromUtf8("lbWaitForAnswer"));

        gridLayout_3->addWidget(lbWaitForAnswer, 0, 0, 1, 1);

        lbWaitForClickMessage = new QLabel(gbWaitTimeOut);
        lbWaitForClickMessage->setObjectName(QString::fromUtf8("lbWaitForClickMessage"));

        gridLayout_3->addWidget(lbWaitForClickMessage, 2, 0, 1, 1);

        eWaitForAnswer = new QLineEdit(gbWaitTimeOut);
        eWaitForAnswer->setObjectName(QString::fromUtf8("eWaitForAnswer"));
        sizePolicy.setHeightForWidth(eWaitForAnswer->sizePolicy().hasHeightForWidth());
        eWaitForAnswer->setSizePolicy(sizePolicy);
        eWaitForAnswer->setMinimumSize(QSize(40, 20));
        eWaitForAnswer->setMaximumSize(QSize(60, 20));

        gridLayout_3->addWidget(eWaitForAnswer, 0, 1, 1, 1);

        eWaitForClick = new QLineEdit(gbWaitTimeOut);
        eWaitForClick->setObjectName(QString::fromUtf8("eWaitForClick"));
        sizePolicy.setHeightForWidth(eWaitForClick->sizePolicy().hasHeightForWidth());
        eWaitForClick->setSizePolicy(sizePolicy);
        eWaitForClick->setMinimumSize(QSize(40, 20));
        eWaitForClick->setMaximumSize(QSize(60, 20));

        gridLayout_3->addWidget(eWaitForClick, 2, 1, 1, 1);


        verticalLayout->addWidget(gbWaitTimeOut);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        lbAutoTransfer = new QLabel(OptionsTabMainWidget);
        lbAutoTransfer->setObjectName(QString::fromUtf8("lbAutoTransfer"));

        horizontalLayout->addWidget(lbAutoTransfer);

        eAutoTransfer = new QLineEdit(OptionsTabMainWidget);
        eAutoTransfer->setObjectName(QString::fromUtf8("eAutoTransfer"));
        sizePolicy.setHeightForWidth(eAutoTransfer->sizePolicy().hasHeightForWidth());
        eAutoTransfer->setSizePolicy(sizePolicy);
        eAutoTransfer->setMinimumSize(QSize(40, 20));
        eAutoTransfer->setMaximumSize(QSize(60, 20));

        horizontalLayout->addWidget(eAutoTransfer);


        verticalLayout->addLayout(horizontalLayout);

        verticalSpacer = new QSpacerItem(20, 200, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        cbAutoUpdate->raise();
        gbWaitTimeOut->raise();

        retranslateUi(OptionsTabMainWidget);

        QMetaObject::connectSlotsByName(OptionsTabMainWidget);
    } // setupUi

    void retranslateUi(QWidget *OptionsTabMainWidget)
    {
        OptionsTabMainWidget->setWindowTitle(QApplication::translate("OptionsTabMainWidget", "Form", 0, QApplication::UnicodeUTF8));
        cbAutoUpdate->setText(QApplication::translate("OptionsTabMainWidget", "\320\220\320\262\321\202\320\276\320\276\320\261\320\275\320\276\320\262\320\273\320\265\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        comboBoxLogLevel->clear();
        comboBoxLogLevel->insertItems(0, QStringList()
         << QApplication::translate("OptionsTabMainWidget", "OFF", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("OptionsTabMainWidget", "FATAL", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("OptionsTabMainWidget", "ERROR", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("OptionsTabMainWidget", "WARN", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("OptionsTabMainWidget", "INFO", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("OptionsTabMainWidget", "DEBUG", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("OptionsTabMainWidget", "TRACE", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("OptionsTabMainWidget", "ALL", 0, QApplication::UnicodeUTF8)
        );
        lblLogFile->setText(QApplication::translate("OptionsTabMainWidget", "\320\237\321\203\321\202\321\214 \320\272 \321\204\320\260\320\271\320\273\321\203 \320\273\320\276\320\263\320\276\320\262", 0, QApplication::UnicodeUTF8));
        lblLogLevel->setText(QApplication::translate("OptionsTabMainWidget", "\320\243\321\200\320\276\320\262\320\265\320\275\321\214 \320\277\321\200\320\276\321\202\320\276\320\272\320\276\320\273\320\270\321\200\320\276\320\262\320\260\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        gbWaitTimeOut->setTitle(QApplication::translate("OptionsTabMainWidget", "\320\224\320\273\320\270\321\202\320\265\320\273\321\214\320\275\320\276\321\201\321\202\321\214 \320\262\320\277\320\273\321\213\320\262\320\260\321\216\321\211\320\265\320\263\320\276 \321\203\320\262\320\265\320\264\320\276\320\274\320\273\320\265\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        lbWaitForAnswer->setText(QApplication::translate("OptionsTabMainWidget", "\320\230\320\275\320\264\320\270\320\272\320\260\321\206\320\270\321\217 \320\276 \320\262\321\205\320\276\320\264\321\217\321\211\320\265\320\274 \320\267\320\262\320\276\320\275\320\272\320\265 (\321\201\320\265\320\272)", 0, QApplication::UnicodeUTF8));
        lbWaitForClickMessage->setText(QApplication::translate("OptionsTabMainWidget", "\320\230\320\275\320\264\320\270\320\272\320\260\321\206\320\270\321\217 \320\276 \320\262\321\205\320\276\320\264\321\217\321\211\320\265\320\274 \321\201\320\276\320\276\320\261\321\211\320\265\320\275\320\270\320\270 (\321\201\320\265\320\272)", 0, QApplication::UnicodeUTF8));
        lbAutoTransfer->setText(QApplication::translate("OptionsTabMainWidget", "\320\220\320\262\321\202\320\276\320\277\320\265\321\200\320\265\320\262\320\276\320\264 \320\267\320\262\320\276\320\275\320\272\320\260 \321\207\320\265\321\200\320\265\320\267 (\321\201\320\265\320\272)", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class OptionsTabMainWidget: public Ui_OptionsTabMainWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPT_MAIN_H
