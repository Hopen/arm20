#ifndef CONTACTSWIDGET_H
#define CONTACTSWIDGET_H

#include <QWidget>

#include "appcore/lateboundobject.h"
#include "appcore/callscontroller.h"

#include "nocontactswidget.h"
#include "appcontext.h"

class ContactsManager;
class DialingPauseHandler;
namespace Ui {
    class ContactsWidget;
}

using namespace ccappcore;

class ContactsWidget : public QWidget {
    Q_OBJECT
public:
    ContactsWidget(QWidget *parent = 0);
    ~ContactsWidget();

    void setFocusOnSearch();    
protected:
    void changeEvent(QEvent *e);

private:
    Ui::ContactsWidget *_ui;
    QPointer<NoContactsWidget> _noContactsWidget;
    LateBoundObject<ContactsManager> _contactsManager;
    LateBoundObject<CallsController> _callsController;
    LateBoundObject<DialingPauseHandler> _dialPauseHandler;
    LateBoundObject<AppContext> _appContext;

private slots:
    void callToTransferChanged(ccappcore::Call);
    void on_bnMakeCall_clicked();
    void on_searchLine_textChanged(QString );
};

#endif // CONTACTSWIDGET_H
