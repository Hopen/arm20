/********************************************************************************
** Form generated from reading UI file 'ui_options.ui'
**
** Created: Tue 26. Mar 13:54:36 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UI_OPTIONS_H
#define UI_UI_OPTIONS_H

#include <Qt3Support/Q3MimeSourceFactory>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QStackedWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OptionsUI
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *hboxLayout;
    QListWidget *lv_tabs;
    QVBoxLayout *vboxLayout;
    QLabel *lb_pageTitle;
    QStackedWidget *ws_tabs;
    QWidget *WStackPage;
    QFrame *line1;
    QHBoxLayout *horizontalLayout;
    QDialogButtonBox *buttonBox;
    QPushButton *btnSaveSettings;
    QPushButton *btnLoadSettings;

    void setupUi(QDialog *OptionsUI)
    {
        if (OptionsUI->objectName().isEmpty())
            OptionsUI->setObjectName(QString::fromUtf8("OptionsUI"));
        OptionsUI->resize(486, 369);
        OptionsUI->setMinimumSize(QSize(400, 300));
        verticalLayout = new QVBoxLayout(OptionsUI);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        lv_tabs = new QListWidget(OptionsUI);
        lv_tabs->setObjectName(QString::fromUtf8("lv_tabs"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lv_tabs->sizePolicy().hasHeightForWidth());
        lv_tabs->setSizePolicy(sizePolicy);
        lv_tabs->setMinimumSize(QSize(50, 0));
        lv_tabs->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        hboxLayout->addWidget(lv_tabs);

        vboxLayout = new QVBoxLayout();
        vboxLayout->setSpacing(3);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        lb_pageTitle = new QLabel(OptionsUI);
        lb_pageTitle->setObjectName(QString::fromUtf8("lb_pageTitle"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(lb_pageTitle->sizePolicy().hasHeightForWidth());
        lb_pageTitle->setSizePolicy(sizePolicy1);

        vboxLayout->addWidget(lb_pageTitle);

        ws_tabs = new QStackedWidget(OptionsUI);
        ws_tabs->setObjectName(QString::fromUtf8("ws_tabs"));
        WStackPage = new QWidget();
        WStackPage->setObjectName(QString::fromUtf8("WStackPage"));
        ws_tabs->addWidget(WStackPage);

        vboxLayout->addWidget(ws_tabs);


        hboxLayout->addLayout(vboxLayout);


        verticalLayout->addLayout(hboxLayout);

        line1 = new QFrame(OptionsUI);
        line1->setObjectName(QString::fromUtf8("line1"));
        line1->setFrameShape(QFrame::HLine);
        line1->setFrameShadow(QFrame::Sunken);
        line1->setFrameShape(QFrame::HLine);

        verticalLayout->addWidget(line1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        buttonBox = new QDialogButtonBox(OptionsUI);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Apply|QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        horizontalLayout->addWidget(buttonBox);

        btnSaveSettings = new QPushButton(OptionsUI);
        btnSaveSettings->setObjectName(QString::fromUtf8("btnSaveSettings"));

        horizontalLayout->addWidget(btnSaveSettings);

        btnLoadSettings = new QPushButton(OptionsUI);
        btnLoadSettings->setObjectName(QString::fromUtf8("btnLoadSettings"));

        horizontalLayout->addWidget(btnLoadSettings);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(OptionsUI);

        QMetaObject::connectSlotsByName(OptionsUI);
    } // setupUi

    void retranslateUi(QDialog *OptionsUI)
    {
        OptionsUI->setWindowTitle(QApplication::translate("OptionsUI", "Options", 0, QApplication::UnicodeUTF8));
        lb_pageTitle->setText(QString());
        btnSaveSettings->setText(QApplication::translate("OptionsUI", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214 \320\275\320\260\321\201\321\202\321\200\320\276\320\271\320\272\320\270", 0, QApplication::UnicodeUTF8));
        btnLoadSettings->setText(QApplication::translate("OptionsUI", "\320\227\320\260\320\263\321\200\321\203\320\267\320\270\321\202\321\214 \320\275\320\260\321\201\321\202\321\200\320\276\320\271\320\272\320\270", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class OptionsUI: public Ui_OptionsUI {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UI_OPTIONS_H
