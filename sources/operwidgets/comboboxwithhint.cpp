#include "comboboxwithhint.h"
#include <QKeyEvent>

ComboBoxWithHint::ComboBoxWithHint(QWidget *parent)
    : QComboBox(parent)
    , _timeoutInSeconds(1)
    , _timeoutEnabled(false)
    , _hintMode(true)
{
    setEditable(true);

    connect(this, SIGNAL(currentIndexChanged(QString)),
            this, SIGNAL(textCommitting(QString)));

    connect(lineEdit(),SIGNAL(textChanged(QString)),
            this, SLOT(on_textChanged(QString)));
}

void ComboBoxWithHint::setHintText(const QString& s)
{
    _hintText = s;
    if(isHintVisible())
        setEditText(_hintText);;
}

ComboBoxWithHint::~ComboBoxWithHint()
{
}

void ComboBoxWithHint::showHint()
{
    _hintMode = true;
    setEditText(_hintText);
    clearFocus();
    style()->unpolish(this);
    ensurePolished();
}

bool ComboBoxWithHint::isHintVisible() const
{
    return _hintMode;
}

void ComboBoxWithHint::showEvent(QShowEvent *event)
{
    QComboBox::showEvent(event);
    if( !hasFocus() )
        showHint();
}

void ComboBoxWithHint::focusInEvent(QFocusEvent *event)
{
    QComboBox::focusInEvent(event);
    if(isHintVisible())
    {
        _hintMode = false;
        setEditText(QString());
    }
}

void ComboBoxWithHint::focusOutEvent(QFocusEvent *event)
{
    QComboBox::focusOutEvent(event);

    if( currentText().isEmpty() )
        showHint();
    else
        fireTextChanged();

}

void ComboBoxWithHint::keyPressEvent(QKeyEvent *event)
{
    if(!event)
        return;

    if(event->key() == Qt::Key_Escape)
    {
        showHint();
        event->accept();
    }

    QComboBox::keyPressEvent(event);

    if(event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return )
    {
        fireTextChanged();
        event->accept();
        emit returnPressed();
    }
    else if(_timeoutEnabled)
    {
        _timeoutTimer.singleShot(_timeoutInSeconds*1000,
                                 this,SLOT(fireTextChanged()));
    }
}

void ComboBoxWithHint::fireTextChanged()
{
    if(!currentText().isEmpty() && !isHintVisible() )
    {
        emit textCommitting(currentText());
    }
}

void ComboBoxWithHint::on_textChanged(const QString &text)
{
    if(!text.isEmpty() && text!=_hintText)
        _hintMode = false;
    style()->unpolish(this);
    ensurePolished();
}

