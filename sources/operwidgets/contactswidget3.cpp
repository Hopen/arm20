﻿#include "contactswidget3.h"
#include "contactsmanager.h"
#include "statetimedelegate.h"
#include "appcore/useritemdatarole.h"
#include "appcore/domainobjectfilter.h"
#include "lineeditwithhint.h"
#include "mainwindow.h"

const int column_messagesCount = Contact::UserData + 1;

ContactsModel::ContactsModel(QObject *parent):QAbstractListModel(parent)
{
    _columnsList << /*columnNameById*/ (Contact::Name)
                 << /*columnNameById*/ (Contact::Status)
                 << /*columnNameById*/ (Contact::Phone)
                 << /*columnNameById*/ (Contact::StatusReason)
                 << /*columnNameById*/ (Contact::StatusTime)
                 << column_messagesCount;

    _saveSortColumn = 0;
    _saveSortOrder = Qt::AscendingOrder;

    connect(_controller.instance(),
            SIGNAL(dataChanged()),
            this,
            SLOT(reset()),
            Qt::QueuedConnection);


    connect(_controller.instance(),
            SIGNAL(contactStatusChanged(ccappcore::Contact)),
            this,
            //SIGNAL(layoutChanged()),
            SLOT(sort()),
            Qt::QueuedConnection);

    connect(_controller.instance(),
            SIGNAL(contactGetInOut(ccappcore::Contact)),
            this,
            SLOT(reset()),
            Qt::QueuedConnection);

    connect(_spamController.instance(),
            SIGNAL(updateContactStatus(int)),
            this,
            SLOT(sort()),
            Qt::QueuedConnection);

    connect(_jabber.instance(),
            SIGNAL(dataChanged()),
            this,
            SLOT(reset()),
            Qt::QueuedConnection);

    connect(_jabber.instance(),
            SIGNAL(contactStatusChanged(ccappcore::Contact)),
            this,
            //SIGNAL(layoutChanged()),
            SLOT(sort()),
            Qt::QueuedConnection);

    _alertingCount = 0;
    connect(&_alertingTimer, SIGNAL(timeout()),
            this, SLOT(alertingTimerExpired()));

    _alertingTimer.start(1000);

    reset();
}

QString ContactsModel::columnNameById(const qint32 &id) const
{
    switch(id)
    {
    case Contact::Name:
        return tr("Имя");
    case Contact::Status:
        return tr("Текущий статус");
    case Contact::Phone:
        return tr("Номер телефона");
    case Contact::StatusReason:
        return tr("Последнее действие");
    case Contact::StatusTime:
        return tr("Время последнего действия");

    case column_messagesCount:
        return tr("Сообщения");

    default:
        return tr("Неизвестно");
    }

    return QString();
}

int ContactsModel::rowCount(const QModelIndex &/*parent*/) const
{
    return _contacts/*_controller->operList()*/.size();
}

int ContactsModel::columnCount(const QModelIndex &/*parent*/) const
{
    return _columnsList.size();
}

QVariant ContactsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (index.row() >= _contacts.size()/*_controller->operList().size()*/)
        return QVariant();

    if(index.column() <0 || index.column() >= _columnsList.count() )
        return QVariant();

    Contact c = _contacts[index.row()];
    if(!c.isValid())
        return QVariant();

    if (role == Qt::DisplayRole)
    {

        Contact::ContactInfo dt = (Contact::ContactInfo)_columnsList[index.column()];
        if (dt == column_messagesCount)
            return _spamController->messageChatIn(c.contactId().toInt()).size();

        if (dt == Contact::Status)
            return _controller->statusAsString((Contact::ContactStatus)(c.data(dt).toInt()));

        return c.data(dt);
    }

    if(role == Qt::DecorationRole)
    {
        if (index.column()==0)
        {
            static QIcon groupIcon(":/resources/icons/group_16x16.png");
            if( c.contactId().type() == CT_Group )
                return groupIcon;

            static QIcon msgIcon(":/resources/icons/new/messages.png");
            if (_spamController->isUnreadChatMessage(c.contactId().toInt()))
            {
                if (_alertingCount % 2)
                    return msgIcon;
            }

            return _controller->statusIcon(c.status(),  c.contactId().type() == CT_Roster);
        }
        if ((Contact::ContactInfo)_columnsList[index.column()] == column_messagesCount)
        {
            static QIcon msgIcon(":/resources/icons/new/messages.png");
            if (_spamController->isUnreadChatMessage(c.contactId().toInt()))
                return msgIcon;
        }
    }

    if(role == StateTimeRole && c.statusTime().isValid() )
        return c.statusTime();

    if(role == Qt::TextColorRole)
        return _controller->statusColor(c.status());

    if(role == ccappcore::AssociatedObjectRole)
        return QVariant::fromValue(c);

//    if(role == Qt::FontRole)
//    {
//        QFont font;
//        font.setFamily("Arial");
//        return font;
//    }

    return QVariant();
}

//Qt::ItemFlags ContactsModel::flags(const QModelIndex &index) const
//{
////    if (!index.isValid())
////        return Qt::ItemIsEnabled;

////    return QAbstractItemModel::flags(index) |
////            Qt::ItemIsDragEnabled |
////            Qt::ItemIsDropEnabled ;
//    Qt::ItemFlags defaultFlags = QAbstractListModel::flags(index);

//    if (index.isValid())
//        return Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | defaultFlags;
//    else
//        return Qt::ItemIsDropEnabled | defaultFlags;

//}

//bool ContactsModel::dropMimeData(const QMimeData *data,
//    Qt::DropAction action, int row, int column, const QModelIndex &parent)
//{
//    if (action == Qt::IgnoreAction)
//        return true;

//    return false;
//}

//QMimeData *ContactsModel::mimeData(const QModelIndexList &indexes) const
//{
//    QMimeData *mimeData = new QMimeData();
//    QByteArray encodedData;

//    QDataStream stream(&encodedData, QIODevice::WriteOnly);

//    foreach (QModelIndex index, indexes) {
//        if (index.isValid()) {
//            QString text = data(index, Qt::DisplayRole).toString();
//            stream << index.internalId() << index.row() << index.column() << text;
//            Contact c = _contacts[index.row()];
//            int test = 0;
//        }
//    }

//    mimeData->setData("text/plain", encodedData);
//    return mimeData;
//}

//QStringList ContactsModel::mimeTypes() const
//{
//    QStringList types;
//    types << "text/plain";
//    return types;
//}

//Qt::DropActions ContactsModel::supportedDropActions() const
//{
//    return Qt::TargetMoveAction/* | Qt::MoveAction*/;
//}



QVariant ContactsModel::headerData(int section, Qt::Orientation orientation,
                                         int role) const
{
       if (role != Qt::DisplayRole)
            return QVariant();

        if (orientation == Qt::Horizontal)
            return columnNameById(_columnsList.value(section));
        else
            return QString("Row %1").arg(section);
}

void ContactsModel::applyFilter(const PredicateGroup<Contact> &filter)
{
    emit layoutAboutToBeChanged();

    _filter = filter;

    _contacts.clear();
    _contacts =  _controller->findOpers (_filter);
    _contacts+=  _controller->findGroups(_filter);
    _contacts+=  _jabber->contacts();
    //qSort(_contacts);

    emit layoutChanged();
}

void ContactsModel::reset()
{
    applyFilter(_filter);
}

void ContactsModel::sort()
{
//    emit layoutAboutToBeChanged();
//    //qSort(_contacts);

//    emit layoutChanged();
    sort(_saveSortColumn,_saveSortOrder);
}

void ContactsModel::sort(int column, Qt::SortOrder order)
{
    _saveSortColumn = column;
    _saveSortOrder = order;
    emit layoutAboutToBeChanged();
    switch(_columnsList[column])
    {
    case Contact::Name:
    {
        qSort(_contacts.begin(),_contacts.end(),ContactSortByNamePredicate(order));
        break;
    }
    case Contact::Status:
    {
        qSort(_contacts.begin(),_contacts.end(),ContactSortByStatusPredicate(order));
        break;
    }
    case Contact::Phone:
    {
        qSort(_contacts.begin(),_contacts.end(),ContactSortByPhonePredicate(order));
        break;
    }
    default:
        qSort(_contacts);
    };

//    {
//        qSort(_contacts.begin(),_contacts.end(),ContactSortByNamePredicate(&_contacts,order));
//    }
//    else
//    {
//        qSort(_contacts);
//    }
    emit layoutChanged();
}

void ContactsModel::alertingTimerExpired()
{
    emit layoutAboutToBeChanged(); //redmine #364
    ++_alertingCount;
    emit layoutChanged();
}


ContactsWidget3::ContactsWidget3(QWidget *parent) :
    //QWidget(parent),
    IAlarmHandler( parent/*, true*/),
    _isOnlineFilter(new IsContactOnline()),
    _isOperFilter(new IsOperator()),
    _isGroupFilter(new IsGroup()),
    _isShowPrivateGroup(new IsContactInSessionOwnerGroup())
{
    IAlarmHandler::init();

    _mainModel = new ContactsModel(this);
    _mainView = new QTreeView(this);

//    QFont font = _mainView->font();
//    font.setFamily("Arial");
//    _mainView->setFont(font);

    _mainView->setRootIsDecorated(false);
    _mainView->setModel(_mainModel);
    _mainView->setSelectionBehavior(QAbstractItemView::SelectRows);
    _mainView->setItemDelegateForColumn(0,
        new StateTimeDelegate(StateTimeDelegate::FormatMinutes, _mainView)
        );
    //_mainView->header()->setSortIndicator(0, Qt::AscendingOrder);
    _mainView->header()->setSortIndicatorShown(false);
    _mainView->header()->setClickable(true);
    _mainView->setSortingEnabled(true);

//    _mainView->viewport()->setAcceptDrops(true);
//    _mainView->setDragEnabled(true);
//    _mainView->setDragDropMode(QAbstractItemView::InternalMove);

    //connect(_mainView.data(),SIGNAL(doubleClicked(QModelIndex)),this,SLOT(on_contactSelect(QModelIndex)));
    connect(_appContext.instance(), SIGNAL(activeContactChanged(ccappcore::Contact)), this, SLOT(on_contactChanged(ccappcore::Contact)));

    _btnMakeCall = new QPushButton();
    _btnMakeCall->setText(tr("Позвонить"   ));
    _btnMakeCall->setIcon(QIcon(":/resources/icons/new/call on.png"));

    _btnSendMessage = new QPushButton();
    _btnSendMessage->setText(tr("Написать"));
    _btnSendMessage->setIcon(QIcon(":/resources/icons/new/messages.png"));

    connect(_btnMakeCall   .data(),SIGNAL(clicked()),this,SLOT(onBtnMakeCallCliked()));
    connect(_btnSendMessage.data(),SIGNAL(clicked()),this,SLOT(onBtnSendMessageCliked()));


    QWidget* spacer = new QWidget();
    spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    // toolBar is a pointer to an existing toolbar

    QHBoxLayout* bottomLayout = new QHBoxLayout();
    bottomLayout->addWidget(_btnMakeCall);
    bottomLayout->addWidget(_btnSendMessage);
    bottomLayout->addWidget(spacer);

    QVBoxLayout *topLayout = new QVBoxLayout();
    topLayout->addWidget(_mainView);
    topLayout->addLayout(bottomLayout);
    setLayout(topLayout);

    on_hideGroups(true);

    updateWidgetControls();

    getTabIcon(true);
}

void ContactsWidget3::on_hideOfflineOperators(bool checked)
{
    if (!_isOnlineFilter)
        return;

    if (checked)
        _mainModel->addFilter(_isOnlineFilter);
    else
        _mainModel->removeFilter(_isOnlineFilter);

}

void ContactsWidget3::on_hideOperators(bool checked)
{
    if (!_isGroupFilter)
        return;

    if (checked)
        _mainModel->addFilter(_isGroupFilter);
    else
        _mainModel->removeFilter(_isGroupFilter);

}

void ContactsWidget3::on_hideGroups(bool checked)
{
    if(!_isOperFilter)
        return;

    if (checked)
        _mainModel->addFilter(_isOperFilter);
    else
        _mainModel->removeFilter(_isOperFilter);
}

void ContactsWidget3::on_showPrivateGroupOnly(bool checked)
{
    if (!_isShowPrivateGroup)
        return;

    if (checked)
        _mainModel->addFilter(_isShowPrivateGroup);
    else
        _mainModel->removeFilter(_isShowPrivateGroup);

}

//void ContactsWidget3::on_contactSelect(QModelIndex index)
//{
//    //_appContext->showChatForm();
//}

void ContactsWidget3::on_contactChanged(ccappcore::Contact c)
{
//    if (!c.isValid())
//        return;
    _assignedContact = c;
    updateWidgetControls();
}

void ContactsWidget3::onBtnMakeCallCliked()
{
    if (_assignedContact.isValid())
        _callsController->makeCall(_assignedContact.data(Contact::Phone).toString());
}

void ContactsWidget3::onBtnSendMessageCliked()
{
    if (_assignedContact.isValid())
        _appContext->showChatForm(_assignedContact);

}

void ContactsWidget3::updateWidgetControls()
{
    _btnMakeCall    ->setEnabled(_assignedContact.isValid() && _assignedContact.canMakeCall()
                                 && (_assignedContact.contactId().type()!= CT_Roster));
    _btnSendMessage ->setEnabled(_assignedContact.isValid() && _assignedContact.canSendMessage());

}

void ContactsWidget3::connectToTimer()
{
    connect(_spamController.instance(),SIGNAL(chatMsgAdded(SpamMessage)),this, SLOT(onSpamMessageAdded(SpamMessage)));
}

int ContactsWidget3::getTabIndex()const
{
    return MainWindow::TB_USERS;
}

QIcon ContactsWidget3::getTabIcon(bool _default) const
{

    static QIcon rosterActiveIcon   (":/resources/icons/new/message green.png" );
    static QIcon operatorActiveIcon (":/resources/icons/new/tab_status oper16_1.png");

    if (_default)
    {
        _assignedMessage = ccappcore::SpamMessage();
        return QIcon(":/resources/icons/new/tab_status oper16-3.png");
    }

    if (timerCount()%2 && _assignedMessage.isValid())
    {
        ContactId personId = _operController->createOperId(_assignedMessage.operFromId());
        Contact c = _operController->findById(personId);

        if(c.isValid())
            return operatorActiveIcon;

        RosterId rosterId = _jabberController->createRosterId(_assignedMessage.operFromId());
        c = _jabberController->findById(rosterId);

        if(c.isValid())
            return rosterActiveIcon;

    }
    return QIcon(":/resources/icons/new/tab_status oper16_2.png");
}

void ContactsWidget3::onSpamMessageAdded(SpamMessage message)
{
    if (!message.isValid())
        return;

    if (!_assignedMessage.isValid())
        _assignedMessage = message;
    else
    {
        ContactId personId = _operController->createOperId(_assignedMessage.operFromId());
        Contact oper = _operController->findById(personId);

        RosterId rosterId = _jabberController->createRosterId(message.operFromId());
        Contact roster = _jabberController->findById(rosterId);

        if ( (oper.isValid()) &&
             (roster.isValid())   )
            _assignedMessage = message;
    }

    startAlarm();
}
