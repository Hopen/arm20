#include "contactsview.h"

#include <QContextMenuEvent>
#include <QAbstractItemModel>

#include "statetimedelegate.h"

ContactsView::ContactsView(QWidget* parent /*= NULL*/)
    : QTreeView(parent)
    , _firstColumnSpanned(false)
{
    QTimer* stateTimer = new QTimer(parent);
    connect(stateTimer, SIGNAL(timeout()), this, SLOT(updateStateTime()));
    stateTimer->setSingleShot(false);
    stateTimer->start(5000);

    this->setLayout(new QVBoxLayout());
}

/*virtual */
void ContactsView::setModel(QAbstractItemModel *m)
{
    //if(model())
      //  model()->disconnect(this);
    connect(m,SIGNAL(rowsRemoved(QModelIndex,int,int)),this, SLOT(toggleEmptyWidget()));
    connect(m,SIGNAL(layoutChanged()),this,SLOT(expandAll()));
    QTreeView::setModel(m);
}


void ContactsView::doItemsLayout()
{
    QTreeView::doItemsLayout();
    setFirstColumnSpanned();
    bestFitColumns();
    toggleEmptyWidget();
}


void ContactsView::updateStateTime()
{
    int rowCount = model()->rowCount(QModelIndex());
    if(rowCount == 0)
        return;
    QModelIndex topLeft = model()->index(0,1,QModelIndex());
    QModelIndex bottomRight =
            model()->index(rowCount-1,1,QModelIndex());

    dataChanged(topLeft, bottomRight);
}

void ContactsView::setFirstColumnSpanned(bool spanned)
{
    _firstColumnSpanned = spanned;
    setFirstColumnSpanned();
}

void ContactsView::setFirstColumnSpanned()
{
    if(_firstColumnSpanned)
    {
        for(int i=0;i<model()->rowCount(QModelIndex()); i++)
            QTreeView::setFirstColumnSpanned(i,QModelIndex(), true);
    }

    bestFitColumns();
}

void ContactsView::showEvent(QShowEvent *event)
{
    QTreeView::showEvent(event);
    bestFitColumns();
}

void ContactsView::resizeEvent(QResizeEvent* event)
{
    QTreeView::resizeEvent(event);
    bestFitColumns();
}

void ContactsView::bestFitColumns()
{
    resizeColumnToContents(0);
    resizeColumnToContents(1);
}

void ContactsView::setEmptyWidget(QWidget* w)
{
    if(_emptyWidget)
        layout()->removeWidget(_emptyWidget);
    _emptyWidget = w;
    layout()->addWidget(_emptyWidget);
}

void ContactsView::toggleEmptyWidget()
{
    if(!_emptyWidget)
        return;

    if( !model() || model()->rowCount() > 0 )
        _emptyWidget->hide();
    else
        _emptyWidget->show();
}



