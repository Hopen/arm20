import Qt 4.6

Rectangle {
    id: container

    property string text
    property var icon;
    signal clicked

    SystemPalette { id: activePalette }
    height: text.height + 10
    width: iconText.width + 10
    border.width: 1
    radius: 4; smooth: true
    gradient: Gradient {
        GradientStop { position: 0.0;
            color: if(!mr.pressed){activePalette.light;}else{activePalette.button;}
        }
        GradientStop { position: 1.0;
            color: if(!mr.pressed){activePalette.button;}else{activePalette.dark;}
        }
    }
    MouseRegion { id:mr; anchors.fill: parent; onClicked: container.clicked() }
    Row {
        id: iconText
        anchors.centerIn:parent;
        Image
        {
            id: buttonIcon
            width: container.icon == null ? 0 : 20
            height: container.icon == null ? 0 : 20
            source: container.icon;
        }
        Text {
            id: text;
            font.pointSize: 10;
            text: container.text;
            color: activePalette.buttonText
        }
    }

}
