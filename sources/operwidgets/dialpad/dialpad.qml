import Qt 4.6

Column {
    id: dialpad;
    width: 200
    height: 240
    spacing: 8

    SearchBox {
        id: phone
        width: dialButtons.width;
        anchors.horizontalCenter: dialpad.horizontalCenter;
        text: "Введите номер телефона"
    }

    Grid {

        ListModel {
             id: buttons
             ListElement {  name: "1" }
             ListElement {  name: "2" }
             ListElement {  name: "3" }
             ListElement {  name: "4" }
             ListElement {  name: "5" }
             ListElement {  name: "6" }
             ListElement {  name: "7" }
             ListElement {  name: "8" }
             ListElement {  name: "9" }
             ListElement {  name: "*" }
             ListElement {  name: "0" }
             ListElement {  name: "#" }
        }

        id: dialpadGrid
        rows: 4; columns: 3;
        spacing: dialpad.spacing
        anchors.horizontalCenter: dialpad.horizontalCenter;

        Repeater  {
            model: buttons;
            Button {
                text: name;
                width: phone.width / 3 - dialpad.spacing * 2
            }
        }
    }

    Row {
        id:dialButtons
        anchors.horizontalCenter: dialpad.horizontalCenter;
        spacing: dialpad.spacing
        Button { text: "Позвонить"; icon: "images/dial_32x32.png" }
        Button { text: "Перевести"; icon: "images/call_transfer_32x32.png" }
    }
}
