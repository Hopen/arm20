#ifndef EDITTHRESHOLDSDIALOG_H
#define EDITTHRESHOLDSDIALOG_H

#include "operwidgets_global.h"
#include "appcore/quantativeindicator.h"

using namespace ccappcore;

namespace Ui {
    class EditThresholdsDialog;
}

class EditThresholdsDialog : public QDialog
{
    Q_OBJECT

public:

    explicit EditThresholdsDialog(QWidget *parent = 0);
    ~EditThresholdsDialog();

    bool isInverted() const;
    int warningValue() const;
    int criticalValue() const;
    void setValues(bool inverted, int warning, int critical);

private:
    Ui::EditThresholdsDialog *ui;
private slots:
    void commitData();
};

#endif // EDITTHRESHOLDSDIALOG_H
