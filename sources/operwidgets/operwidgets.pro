# -------------------------------------------------
# Project created by QtCreator 2009-04-30T23:40:16
# -------------------------------------------------
#QT += webkit #network xml  phonon #script
QT += network webkit
TARGET = ccplugin_operwidgets
TEMPLATE = lib

contains( CONFIG, static) {
  CONFIG  += staticlib
  DEFINES += QT_NODLL
  DEFINES += QCA_STATIC
  DEFINES += QXMPP_STATIC
} else {
  CONFIG += plugin
}

DEFINES -= QABSTRACTSOCKET_DEBUG

DESTDIR = $$OUT_PWD/../bin
LIBS = -L$$DESTDIR \
    -lappcore \
    -lthirdparty

#DEPENDPATH += $$PWD/../thirdparty/ArthurStyle  test123
#LIBS += -L$$PWD/../thirdparty/ArthurStyle -ldemo_shared

INCLUDEPATH += $$PWD \
    ../ \
    ../thirdparty/psi/src/tools \
    #../thirdparty/ArthurStyle test123
    ../thirdparty/qxmpp-0.7.6/src/base \
    ../thirdparty/qxmpp-0.7.6/src/client

DEFINES += OPERWIDGETS_LIBRARY
RESOURCES += resources.qrc
include("options/options.pri")
PRECOMPILED_HEADER = operwidgets_global.h
HEADERS += operwidgets.h \
    comboboxwithhint.h \
    lineeditwithhint.h \
    logindialog.h \
    appcontext.h \
    toolbuttonoperstatus.h \
    mainwindow.h \
    tabswindow.h \
    calltab.h \
    contactsview.h \
    dialpad.h \
    dialpadform.h \
    contactstatusmenu.h \
    contactcontextmenu.h \
    callcontextmenu.h \
    trayicon.h \
    contactsmanager.h \
    statetimedelegate.h \
    ioperwidgetsplugin.h \
    opersettings.h \
    operevents.h \
    tabwidgetex.h \
    texteditex.h \
    toolbuttonex.h \
    dialingpausehandler.h \
    dialpadbutton.h \
    contactswidget.h \
    nocontactswidget.h \
    indicatoritemdelegate.h \
    indicatorcontextmenu.h \
    editthresholdsdialog.h \
    applicationwidget.h \
    opertoolswidget.h \
    callswidget.h \
    callsview.h \
    contactswidget2.h \
    redirprofilewidget.h \
    redirectprofileeditwidget.h \
    redirruleswidget.h \
    dialpadbox.h \
    callscriptwidget.h \
    scripteventwidget.h \
    contactswidget3.h \
    callswidget2.h \
    localcallhistorywidget.h \
    bblistwidget.h \
    callswidgetm.h \
    docktabs.h \
    chatformwidget.h \
    docktabswidget.h \
    callformwidget.h \
    transferwidget.h \
    chattab.h \
    volumecontroller.h \
    messagetrayicon.h \
    GlobalAddressBookWidget.h \
    LangCombo.h

SOURCES += operwidgets.cpp \
    comboboxwithhint.cpp \
    lineeditwithhint.cpp \
    logindialog.cpp \
    mainwindow.cpp \
    appcontext.cpp \
    tabswindow.cpp \
    calltab.cpp \
    toolbuttonoperstatus.cpp \
    contactsview.cpp \
    dialpad.cpp \
    dialpadform.cpp \
    contactstatusmenu.cpp \
    contactcontextmenu.cpp \
    callcontextmenu.cpp \
    trayicon.cpp \
    contactsmanager.cpp \
    statetimedelegate.cpp \
    opersettings.cpp \
    operevents.cpp \
    tabwidgetex.cpp \
    texteditex.cpp \
    toolbuttonex.cpp \
    dialingpausehandler.cpp \
    dialpadbutton.cpp \
    contactswidget.cpp \
    nocontactswidget.cpp \
    indicatoritemdelegate.cpp \
    indicatorcontextmenu.cpp \
    editthresholdsdialog.cpp \
    applicationwidget.cpp \
    opertoolswidget.cpp \
    callswidget.cpp \
    callsview.cpp \
    contactswidget2.cpp \
    redirprofilewidget.cpp \
    redirectprofileeditwidget.cpp \
    redirruleswidget.cpp \
    dialpadbox.cpp \
    callscriptwidget.cpp \
    scripteventwidget.cpp \
    contactswidget3.cpp \
    callswidget2.cpp \
    localcallhistorywidget.cpp \
    bblistwidget.cpp \
    callswidgetm.cpp \
    docktabs.cpp \
    chatformwidget.cpp \
    docktabswidget.cpp \
    callformwidget.cpp \
    transferwidget.cpp \
    chattab.cpp \
    volumecontroller.cpp \
    messagetrayicon.cpp \
    GlobalAddressBookWidget.cpp \
    LangCombo.cpp

FORMS += logindialog.ui \
    mainwindow.ui \
    tabswindow.ui \
    calltab.ui \
    dialpad.ui \
    dialpadform.ui \
    contactswidget.ui \
    nocallswidget.ui \
    nocontactswidget.ui \
    editthresholdsdialog.ui

TRANSLATIONS += qt_en.ts


#INCLUDEPATH += $$PWD/../thirdparty/ArthurStyle
#DEPENDPATH += $$PWD/../thirdparty/ArthurStyle

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD \
#   ../thirdparty/ArthurStyle/libdemo_shared.a

windows {
#INCLUDEPATH += "C:/Program Files/Microsoft SDKs/Windows/v7.0A/Lib"
INCLUDEPATH += "D:/MinGW/include"
#INCLUDEPATH += "C:/Program Files/Microsoft SDKs/Windows/v7.0/Include"
LIBS += -lwinmm
}











