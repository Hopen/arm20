#include "mainwindow.h"

#include "appcore/styleloader.h"
#include <QtWebKit/QWebSettings>
#include <QtWebKit/QWebView>

#include "options/optionsdlg.h"
#include "calltab.h"
#include "logindialog.h"
#include "toolbuttonoperstatus.h"
#include "contactsmanager.h"
#include "contactsview.h"
#include "statetimedelegate.h"
#include "dialpadform.h"
#include "contactswidget.h"
#include "contactswidget2.h"
#include "nocontactswidget.h"
#include "applicationwidget.h"
#include "opertoolswidget.h"
//#include "callswidget.h"
#include "callsview.h"
#include "redirprofilewidget.h"
#include "redirruleswidget.h"
#include "scripteventwidget.h"
#include "contactswidget3.h"
#include "callswidget2.h"
#include "localcallhistorywidget.h"
#include "bblistwidget.h"
#include "callswidgetm.h"
#include "docktabs.h"
#include "docktabswidget.h"
#include "chattab.h"
#include "GlobalAddressBookWidget.h"

#ifdef Q_WS_WIN
#   define _WIN32_WINDOWS 0x0500
#   include <windows.h>

    //static DWORD dwTimeout = 0;
#endif

#include "ui_mainwindow.h"
//#include "../../../../Qt1/demos/shared/arthurstyle.h"
#include "../../thirdparty/ArthurStyle/arthurstyle.h"

#include "globalshortcut/globalshortcutmanager.h"

using namespace ccappcore;

//QSize QSliderEx::minimumSizeHint()const
//{
//    QSize size = QSlider::minimumSizeHint();
//    return size;
//}

//MainWindow2::MainWindow2(QWidget *parent)
//    : QMainWindow(parent)
//{

////    //setDockOptions(QMainWindow::VerticalTabs);

////    QMainWindow *main = new QMainWindow();
////    QDockWidget *dw1 = new QDockWidget("1", main);
////    main->addDockWidget(Qt::BottomDockWidgetArea, dw1);

////    QDockWidget *dw2 = new QDockWidget("2", main);
////    main->addDockWidget(Qt::BottomDockWidgetArea, dw2);

////    main->tabifyDockWidget(dw2, dw1);
////    main->setTabPosition(Qt::BottomDockWidgetArea,QTabWidget::North);


////    QWidget *wdg = new QWidget();
////    QHBoxLayout *mainLayout = new QHBoxLayout();
////    mainLayout->addWidget(main);
////    wdg->setLayout(mainLayout);

////    QDockWidget *dwMain = new QDockWidget("main", this);
////    dwMain->setWidget(wdg);
////    addDockWidget(Qt::TopDockWidgetArea, dwMain);

//}

//MainWindow2::~MainWindow2()
//{

//}

//bool MainWindow2::event(QEvent *event)
//{
////    QEvent::Type type = event->type();
////    if (type == QEvent::Hide)
////    {
////        event->ignore();
////        return true;
////    }
////    if (type == QEvent::WindowStateChange)
////    {
////        if (isMinimized())
////        {
////            event->ignore();
////            return true;
////        }
////    }

//    return QMainWindow::event(event);

//}

//void MainWindow2::closeEvent(QCloseEvent* event)
//{
//    event->ignore();
//}

//void MainWindow2::changeEvent(QEvent *event)
//{
//    QEvent::Type type = event->type();
//    if (type == QEvent::WindowStateChange)
//    {
//        if (isMinimized())
//        {
//            event->ignore();
//        }
//    }
//}


//namespace TestCore2
//{

//namespace TestNamespace
//{
//    using namespace TestCore2;
//    const testClass test1(testClass(QString("test1"), QString("test2")));

//    ////DEFINE_GLOBAL(QualifiedName, basefontTag, nullAtom, "basefont", xhtmlNamespaceURI);

//    #define DEFINE_GLOBAL(type, name, ...) \
//        const type name (type(...));

//    DEFINE_GLOBAL(testClass, test2, QString("test1"), QString("test2"))

//}

//}


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent/*, VerticalTabs*/)
    , _ui(new Ui::mainWindow)
    , _minFloatDockWindow(0)
    , _bProgramPropertiesHasChanged(false)
    //, _minimized(false)
{
    _ui->setupUi(this);
    //setTabPosition(Qt::BottomDockWidgetArea,QTabWidget::North);

    qApp->setQuitOnLastWindowClosed(true);
    qApp->setActiveWindow(this);
    QString operName = _sessionController->sessionOwner().name();
    setWindowTitle( operName + tr(" @ Call-o-Call Operator Agent") );

    QWebSettings *ws =  QWebSettings::globalSettings();
    ws->setAttribute(QWebSettings::PluginsEnabled, true);

    setupStatusBar();
    setupToolbars();
    setupMainWindowWidgets();
    //setupCallFormWidgets();
    //setupMenu();
    setupGlobalShortcuts();
    setupTabsWidget();
    setupMinimizedCallWidget();
    setupFloatWindows();
    setupVolumeWidget();

    setupEventHandlers();
    initPlugins();

    _dlgOptions = new OptionsDlg(this);
    connect(_dlgOptions.data(),SIGNAL(applyOptions()),this,SLOT(onActionOptionsApplied()));
    //connect(_dlgOptions.data(),SIGNAL(applyOptions()),_appContext.instance(),SLOT(onOptionsApplied()));

    _layoutManager->addWatchWidget(this);
    //_layoutManager->addWatchWidget(_dialerWidget);
    //_layoutManager->addWatchWidget(_callForm);
    //_layoutManager->addWatchWidget(_tabWidget);
    _layoutManager->addWatchWidget(_dwOperFormTab);
    _layoutManager->addWatchWidget(_dwCallFormTab);

    _presetManager->setMainWindow(this);
    _presetManager->load();

    _styleLoader->load(this);

//#ifdef Q_WS_WIN
////    if (!AllowSetForegroundWindow(ASFW_ANY))
////    {
////        DWORD error = GetLastError();
////        int test = error;
////    }
//    SystemParametersInfo(SPI_GETFOREGROUNDLOCKTIMEOUT, 0, &dwTimeout, 0);
//    if (!SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, 0, 0))
//    {
//        DWORD error = GetLastError();
//        int test = error;
//    }

//#endif

    QTimer::singleShot(0,_presetManager.instance(),SLOT(restoreWidgets()));
}

MainWindow::~MainWindow()
{
    qDebug () << tr("MainWindow::~MainWindow()");
//#ifdef Q_WS_WIN
//    if (dwTimeout > 0)
//        SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, (LPVOID)dwTimeout, 0);
//#endif
    delete _ui;
}

//static void TestDate()
//{
//    //    QString stime;
//    //    int vesYear = 0,
//    //        unvesYear = 0;

//    //    int i = 0;
//    //    int daysCount = 0;
//    //    while (i<112)
//    //    {
//    //        std::auto_ptr<QDate> dt(new QDate(1900 + i, 1, 1));
//    //        stime = dt->toString("dd.MM.yyyy");
//    //        int daysInYear = dt->daysInYear();
//    //        if (daysInYear == 366)
//    //            ++vesYear;
//    //        else
//    //            ++unvesYear;
//    //        daysCount+=daysInYear;
//    //        ++i;
//    //    }

//    //    QDate d1(2012,1,1);
//    //    daysCount += d1.daysTo(QDate::currentDate());

//    //    QDateTime dt1(QDate(1900, 1, 1),  QTime(0,0,0));
//    //    dt1 = dt1.addDays(daysCount);
//    //    stime = dt1.toString("dd.MM.yyyy");

//    //    QDateTime dt(QDate(1900, 1, 1),  QTime(0,0,0));
//    //    stime = dt.toString();
//    //    qreal days = 41122;

//    //    dt = dt.addDays(days);
//    //    //dt = dt.toLocalTime();

//    //    stime = dt.toString("dd.MM.yyyy");
//}

//static void TestStreamData()
//{
//    QByteArray data;

//    if (1)
//    {
//        QBuffer buffer(&data);
//        buffer.open(QIODevice::ReadWrite);

//        QDataStream stream(&buffer);
//        stream.setByteOrder(QDataStream::LittleEndian);
//        stream << qint32(1);
//        buffer.close();
//        stream.unsetDevice();
//    }

//    if (1)
//    {
//        qint32 var1 = 0;

//        QBuffer buffer(&data);
//        buffer.open(QIODevice::ReadWrite);

//        QDataStream stream(&buffer);
//        stream.setByteOrder(QDataStream::LittleEndian);
//        stream >> var1;
//        buffer.close();
//        stream.unsetDevice();
//    }

//}



//void MainWindow::testQScriptEngine()
//{
//    _engine = QSharedPointer<QScriptEngine>(new QScriptEngine());

//    CallsController * instance = _callsController.instance();
//    QScriptValue objectWnd = _engine->newQObject(instance);
//    _engine->globalObject().setProperty("CallsController", objectWnd);

//    QFile scriptFile("d:\\1.script");
//    QString script;
//    if (scriptFile.exists())
//    {
//        scriptFile.open(QIODevice::ReadOnly);
//        script.append(scriptFile.readAll());
//        scriptFile.close();
//    }

//    QScriptValue result = _engine->evaluate(script);
//    if (result.isError())
//        qDebug() << "Script error:" << result.toString();

//    int test = 0;

//}

//static void testQString()
//{
//    QString a_num, b_num;
//    const QString const_url(QObject::tr("callto:89266183308&a_num=74957817626?force_hold"));
//    QString url = const_url.left(const_url.indexOf("?"));

//    QStringList params = url.split("&");
//    foreach(QString param,params)
//    {
//        if (param.contains("callto:"))
//            b_num = param.remove("callto:");
//        if (param.contains("a_num="))
//            a_num = param.remove("a_num=");
//    }

//    int test = 0;



//}

//#include "appcore/JabberController.h"

void MainWindow::setupMainWindowWidgets()
{

//    QKeySequence key = QKeySequence(tr("M"));

//    GlobalShortcutManager::KeyTrigger* t = new GlobalShortcutManager::KeyTrigger(key);

//    QObject::connect(t, SIGNAL(activated()), this, SLOT(togglePause()));

//    QXmppClient client(this);
//    client.logger()->setLoggingType(QXmppLogger::StdoutLogging);
//    client.connectToServer("gubarev-forte-it-ru@jivosite.com", "YtjDby22");


    //testQString();
    //TestStreamData();
    //testQScriptEngine();
    delete _ui->centralWidget;
    _ui->centralWidget = NULL;

//    //setup dialer form
//    //Layout manager will restore its user defined state
//    _dialerWidget = new DialpadForm(this);
//    //_dialerWidget->setWindowFlags(Qt::Window);
//    _dialerWidget->setWindowTitle(tr("Набор номера"));
//    //_styleLoader->load(_dialerWidget);
//    QDockWidget* dwDial = createDockWidget( "dialer",
//                                            tr("Набор номера"),
//                                            _dialerWidget );
//    dwDial->setVisible(false);
//    dwDial->setFloating(true);

//     //setup contacts view
//    _contactsWidget = new ContactsWidget(this);
//    QDockWidget* dwContacts = createDockWidget( "contacts",
//                                                tr("Контакты"),
//                                                _contactsWidget );
//    addDockWidget(Qt::BottomDockWidgetArea,dwContacts);


//    //_ui->menuView->addAction(dwContacts->toggleViewAction());
//    dwContacts->show();

//    //********* 1 *************/
//    //setup calls view
//    _callsView = new ContactsView();
//    _callsView->setHeaderHidden(true);
//    _callsView->setItemDelegateForColumn(1,
//        new StateTimeDelegate(StateTimeDelegate::FormatSeconds, _callsView)
//        );
    NoContactsWidget* noCallsWidget = new NoContactsWidget(_callsView);
    noCallsWidget->setHtml(tr("В данный момент нет активных разговоров."));
//    _callsView->setEmptyWidget(noCallsWidget);
//    _callsView->setModel(_contactsManager->callsModel());
//    _contactsManager->connectView(_callsView);
//    //*************************/

////    QDockWidget* dwCalls = createDockWidget( "calls",
////                                             tr("Звонки"),
////                                             _callsView );
////    addDockWidget(Qt::BottomDockWidgetArea,dwCalls);
////    _ui->menuView->addAction(dwCalls->toggleViewAction());
////    dwCalls->show();

//    _callwidget = new CallsView();
//    _callwidget->setModel(_contactsManager->callsModel());
//    _callwidget->setColumnHidden(true);

//    //setup redirections
    _profiles = new RedirProfileWidget();
//    _rules = new RedirectRulesWidget();
//    QStyle *arthurStyle = new ArthurStyle();
//    _rules->setStyle(arthurStyle);
//    QList<QWidget *> widgets = _rules->findChildren<QWidget *>();
//    foreach (QWidget *w2, widgets)
//        w2->setStyle(arthurStyle);

//    _scripts = new ScriptEventWidget();
//    // end setup redirections


    //********* setup operator calls *************/
    _operCallsWidget = new OperCallsWidget(this);
    noCallsWidget = new NoContactsWidget(_operCallsWidget);
    noCallsWidget->setHtml(tr("В данный момент нет активных разговоров."));
    _operCallsWidget->setEmptyWidget(noCallsWidget);
    _contactsManager->connectView(_operCallsWidget->view());

    //********* setup queue calls *************/
    _queueCallsWidget = new QueueCallsWidget(this);
    noCallsWidget = new NoContactsWidget(_queueCallsWidget);
    noCallsWidget->setHtml(tr("В данный момент очередь пуста."));
    _queueCallsWidget->setEmptyWidget(noCallsWidget);
    _contactsManager->connectView(_queueCallsWidget->view());

    //********* setup contacts list *************/
    _contacts3 = new ContactsWidget3(this);
    _contactsManager->connectView(_contacts3->view());

    //********* setup local history tab *************/
    _localHistory = new LocalCallHistoryWidget(this);

    //********* setup bb mesage list tab *************/
    _bbMessages = new BBListWidget(this);

    //********* setup global address book tab *************/
    _globalAddressBook = new GlobalAddressBookWidget(this);
    _contactsManager->connectView(_globalAddressBook->view());
    _opertools->connectView(_globalAddressBook->infoView());
    //_chat = new ChatWidget(this);

    //_testW = new TestWidget(this);

    _contactsManager->reset();
}


void MainWindow::setupTabsWidget()
{
//    QString url(tr("http://ya.ru"));
//    QWebView* webView = new QWebView();
//     webView->setUrl(url);

    if (!_tabWidget.data())
        _tabWidget = new TabsWidget();
    _dwOperFormTab = new QDockWidgetEx(tr("Карточка оператора"));
    _dwOperFormTab->setWidget( _tabWidget );
    EmptyTitleBar* hiddenTitle = new EmptyTitleBar(this);
    _dwOperFormTab->setTitleBarWidget(hiddenTitle);
    //_dwOperFormTab->setMinimumHeight(height()/4);
    //_dwOperFormTab->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Fixed);


    //connect(_dwOperFormTab,SIGNAL(visibilityChanged(bool)),this,SLOT(on_operform_visibilityChanged(bool)));
    connect(_dwOperFormTab,SIGNAL(closed()),this,SLOT(onOperformClosed()));
    addDockWidget(Qt::TopDockWidgetArea,_dwOperFormTab);
    //_tabWidget->addTab(TB_OLDCALLS   ,_callsView        ,QIcon(":/resources/icons/new/user_headset.png"),tr("звонки"));
    /*_tabWidget->*/addTab(/*TB_USERCALLS  ,*/_operCallsWidget  ,/*QIcon(":/resources/icons/new/tab_oper16-3.png"       ),*/tr("Звонки на операторе" ));
    /*_tabWidget->*/addTab(/*TB_QUEUECALLS ,*/_queueCallsWidget ,/*QIcon(":/resources/icons/new/tab_turn16-3.png"       ),*/tr("Состояние очереди"   ));
    /*_tabWidget->*/addTab(/*TB_USERS*//*_contacts3->getTabIndex()      ,*/_contacts3        ,/*QIcon(":/resources/icons/new/tab_status oper16-3.png"),*/tr("Состояние операторов"));
    /*_tabWidget->*/addTab(/*TB_HISTORY    ,*/_localHistory     ,/*QIcon(":/resources/icons/new/tab_stat16-3.png"       ),*/tr("Текущая статистика"  ));
    /*_tabWidget->*/addTab(/*TB_MESSAGES   ,*/_bbMessages       ,/*QIcon(":/resources/icons/new/tab_message16-3.png"    ),*/tr("Объявления"          ));
    addTab(_globalAddressBook, tr("Адресная книга"));
    ///*_tabWidget->*/addTab(TB_CHAT       ,_chat             ,QIcon(":/resources/icons/new/messages.png"           ),tr("Чат"                 ));
    ///*_tabWidget->*/addTab(TB_TEST   ,_testW       ,QIcon(":/resources/icons/new/tab_message16-3.png"    ),tr("test"          ));
    //_tabWidget->addTab(100, _profiles,QIcon(":/resources/icons/new/messages.png"    ),tr("Профайлы"));
    //_tabWidget->addTab(100, webView,QIcon(":/resources/icons/new/messages.png"    ),url);
    //_tabWidget->addTab(_rules,tr("Правила"));
    //_tabWidget->addTab(_scripts,tr("Скрипты"));
    //_tabWidget->addTab(_contactsWidget,tr("Состояние операторов"));
    //addTab(TB_USERS      ,_contactsWidget        ,QIcon(":/resources/icons/new/tab_status oper16-3.png"),tr("Состояние операторов 2"));
//    tabWidget->addTab(_callwidget,tr("Звонки"));
//    /*_tabWidget->*/addTab(TB_TEST   ,webView       ,QIcon(":/resources/icons/new/tab_message16-3.png"    ),tr("Browser"          ));

    //_tabWidget->show();
    //connect(_tabWidget.data(),SIGNAL(currentChanged(int)),this,SLOT(on_currentTabChanged(int)));

}

int MainWindow::addTab(/*quint32 index, */QWidget *widget, /*const QIcon& icon,*/ const QString &label)
{
    if (IAlarmHandler * handler = dynamic_cast<IAlarmHandler*>(widget))
    {
        return _tabWidget->addTab(handler,label);
    }

    //return _tabWidget->addTab(index,widget,icon,label);
}

//void MainWindow::on_currentTabChanged(int index)
//{
//    const QSizePolicy ignored   ( QSizePolicy::Ignored   , QSizePolicy::Ignored   );
//    const QSizePolicy preferred ( QSizePolicy::Maximum , QSizePolicy::Maximum );

//    for (TabsWidget::tabsCollector::Iterator it=_tabWidget->tabs()->begin();it!=_tabWidget->tabs()->end();++it)
//    {
//        QWidget* tabWidget = _tabWidget->widget(it.key());
//        if (it.key() == index)
//            tabWidget->setSizePolicy(preferred);
//        else
//            tabWidget->setSizePolicy(ignored);
//    }
//    _dwTab->layout()->activate();
//    _dwTab->setFixedSize(minimumSizeHint());
//}

void MainWindow::test()
{
    qDebug() << QString(tr("animation finished"));
}

void MainWindow::setupToolbars()
{
    _appltools = new ApplicationToolBar(false,false);
    connect(_appltools.data(),SIGNAL(displaySettings()),this,SLOT(on_actionSettings_triggered()));  // redmine #372
    /*connect(_appltools.data(),SIGNAL(showVolumeWidget()),this,SLOT(onShowVolumeWidget()));*/

    _opertools = new OperToolBar(false);

    addToolBar(_appltools);
    addToolBarBreak();
    addToolBar(_opertools);
}

void MainWindow::setupMinimizedCallWidget()
{
    _operMinCalls = new CallsWidgetM(true,this);
    connect(_operMinCalls.data(),SIGNAL(setWindowSize(qint32)),this,SLOT(onActionExpandMinimzedWindow(qint32)));

    setupMinFloatDockWindow();
}

void MainWindow::setupMinFloatDockWindow()
{
    _minFloatDockWindow    = new QDockMinimizedWidget(tr("Call-a-Call minimized panel"));
    _minFloatDockWindow->setWidget(_operMinCalls);

    addDockWidget(Qt::TopDockWidgetArea,_minFloatDockWindow);
    _minFloatDockWindow->setFloating(true);
    if(_operMinCalls->isOnTop())
        _minFloatDockWindow->setWindowFlags(_minFloatDockWindow->windowFlags() | Qt::ToolTip | Qt::WindowStaysOnTopHint);

    QRect rc = _minFloatDockWindow->geometry();
    rc.setHeight(30);
    rc.setWidth(0);
    _minFloatDockWindow->setGeometry(rc);
    rc = _minFloatDockWindow->geometry();
    _minFloatDockWindow->setGeometry(rc);
    removeDockWidget(_minFloatDockWindow);

    QRect screenRect = getScreenRect();
    QPoint movePoint = screenRect.center();

    movePoint = screenRect.bottomRight();
    movePoint.setX(movePoint.x() -   _minFloatDockWindow->width ());
    movePoint.setY(movePoint.y() - 2*_minFloatDockWindow->height());

    _minFloatDockWindow->move(movePoint);
    _minFloatDockWindow->hide();

    //addDockWidget(Qt::TopDockWidgetArea,_minFloatDockWindow);
    //_minFloatDockWindow->show();

}

void MainWindow::setupAnimation()
{
    _stateMachine = new QStateMachine(this);

    QState *s1 = new QState();
    QState *s2 = new QState();

    _stateMachine->addState(s1);
    _stateMachine->addState(s2);
    _stateMachine->setInitialState(s1);
}


QDockWidget* MainWindow::createDockWidget(
        const QString& objectName,
        const QString& title,
        QWidget *widget)
{
    QDockWidget* dw = new QDockWidget( title, this );
    dw->setObjectName( objectName );
    dw->setSizePolicy( QSizePolicy::Ignored, QSizePolicy::Ignored );
    moveToScreenCenter( dw );

    dw->setWidget( widget );
    dw->setAllowedAreas( Qt::AllDockWidgetAreas );
    dw->setFeatures( QDockWidget::DockWidgetClosable
                    |QDockWidget::DockWidgetMovable
                    |QDockWidget::DockWidgetFloatable );

    return dw;
}


//void MainWindow::setupCallFormWidgets()
//{
//    _callForm = new TabsWindow();
////    QWidget * dialogWdg = new QWidget(this);
////    QHBoxLayout* dlgLayout = new QHBoxLayout();
////    dlgLayout->setMargin(0);
////    dlgLayout->addWidget(_callForm);
////    dialogWdg->setLayout(dlgLayout);

//    //_callForm->setWindowFlags(_callForm->windowFlags() | Qt::WindowMinMaxButtonsHint);
//    //moveToScreenCenter(_callForm);
//    _dwCallForm = createDockWidget( "callform",
//                                                tr("Карточка звонка"),
//                                                _callForm );
//    addDockWidget(Qt::BottomDockWidgetArea,_dwCallForm);

////    _dwCallForm = new QDockWidget( tr("Карточка звонка"), this );
////    _callForm = new TabsWindow();
////    _dwCallForm->setWidget(_callForm);
////    _dwCallForm->setAllowedAreas( Qt::AllDockWidgetAreas );
////    _dwCallForm->setFeatures( QDockWidget::DockWidgetClosable
////                    |QDockWidget::DockWidgetMovable
////                    |QDockWidget::DockWidgetFloatable );

////    addDockWidget(Qt::BottomDockWidgetArea,_dwCallForm);


//    //_ui->menuView->addAction(dwContacts->toggleViewAction());
//    _dwCallForm->hide();
//    //_callForm->hide();

//}

void MainWindow::showCallForm(ccappcore::Call /*c*/)
{
    //_dwCallFormTab->show();
    _ui->action_callform_visiable->setChecked(true);
    QTimer::singleShot(0,_presetManager.instance(),SLOT(restoreWidgets()));
}

void MainWindow::setupStatusBar()
{
    //_statusBar = new QStatusBar(this);

//    connect(_ui->statusBar, SIGNAL(info(QString)),
//            this, SLOT(statusInfo(QString)));

    //_toolButtonStatus = new ToolButtonOperStatus(_ui->statusBar);
    //_ui->statusBar->insertWidget(0, _toolButtonStatus );
}

void MainWindow::statusInfo(const QString& message)
{
    _ui->statusBar->showMessage(message, 5000);
}


void MainWindow::setupMenu()
{
    //_ui->menuActions->insertMenu(_ui->actionShowDialer, &_toolButtonStatus->menu());
    _ui->actionHideOfflineContacts->setChecked( _contactsManager->hideOfflineOperators() );
    _ui->actionOperListGroupMode->setChecked( _contactsManager->isOperListByGroupMode() );
    _ui->actionPrivateGroupsOnly->setChecked( _operSettings->showPrivateGroupsOnly() );
    _ui->actionOperators_only_test->setChecked(_contactsManager->isHideOperators());
    _ui->actionGroups_only_test->setChecked(_contactsManager->isHideGroups());
}

void MainWindow::initPlugins()
{
    QList<IOperWidgetsPlugin*> plugins =
            ServiceProvider::instance().findServices<IOperWidgetsPlugin>();
    foreach(IOperWidgetsPlugin* plugin, plugins)
        plugin->initializeMainWindow(this);
}

void MainWindow::setupGlobalShortcuts()
{

//    QKeySequence key = QKeySequence(tr("M")); it works!

//    GlobalShortcutManager::connect(key, this, SLOT(togglePause()));

//    _shortcutsMgr->registerShortcut("global.toggleVisibility",
//        tr("Показать/скрыть приложение"));
//    _shortcutsMgr->registerShortcut("global.searchContacts",
//        tr("Поиск контактов"));
//    _shortcutsMgr->registerShortcut("global.togglePause",
//        tr("Поставить на паузу/Снять с паузы"));
//    _shortcutsMgr->registerShortcut("global.dialPhone",
//        tr("Набрать номер"));
//    _shortcutsMgr->registerShortcut("appwide.quit",
//        tr("Закрыть приложение"));

//    _shortcutsMgr->applySettings(*_settings); //load shortcuts

//    _shortcutsMgr->connect("global.toggleVisibility", this, SLOT(toggleVisibility()));
//    _shortcutsMgr->connect("global.searchContacts", this, SLOT(moveFocusOnSearch()));
//    _shortcutsMgr->connect("global.togglePause", this, SLOT(togglePause()));
//    //_shortcutsMgr->connect("global.dialPhone", this, SLOT(showDialer()));
//    _shortcutsMgr->connect("appwide.quit", this, SLOT(maybeQuit()));


}

void MainWindow::setupEventHandlers()
{
    connect(_sessionController.instance(),SIGNAL(sessionStopped()),
            this,SLOT(showLoginDialog()));
    connect(_callsController.instance(),
            SIGNAL(callFailed(ccappcore::Call,ccappcore::Call::CallResult,QString)),
            this,
            SLOT(showCallFailReason(ccappcore::Call,ccappcore::Call::CallResult,QString)));
    connect(_callsController.instance(),
            SIGNAL(joinCallToConfFailed(int,int)),
            this,
            SLOT(showJoinCallToConfFailedReason(int,int)));
    connect(_appContext.instance(), SIGNAL(onToggleVisibility()),
            this, SLOT(toggleVisibility()));

    connect(_appContext.instance(), SIGNAL(callToTransferChanged(ccappcore::Call)),
            this, SLOT(callToTransferChanged(ccappcore::Call)));

//    connect(_appContext.instance(), SIGNAL(onShowDialer(QString)),
//            this, SLOT(showDialer(QString)));

    connect(_appContext.instance(), SIGNAL(onShowCallForm(ccappcore::Call)),
            this, SLOT(showCallForm(ccappcore::Call)));

    connect(_dwCallFormTab,SIGNAL(visibilityChanged(bool)),
            this, SLOT(on_callform_visibilityChanged(bool)));

    connect(_appContext.instance(), SIGNAL(staysOnTop(bool)),
            this, SLOT(onActionStaysOnTopWindow(bool)));
    connect(_appContext.instance(), SIGNAL(windowMinimized(bool)),
            this, SLOT(onActionMinimizeWindow(bool)));
    connect(_appContext.instance(), SIGNAL(activateForegroundWindow()),
            this, SLOT(activateForegroundWindow()));
}

void MainWindow::changeEvent(QEvent *e)
{
    switch (e->type()) {
    case QEvent::LanguageChange:
        _ui->retranslateUi(this);
        break;
    case QEvent::WindowStateChange:
    {
        if (isMinimized()/* || isMaximized()*/)
        {
            QTimer::singleShot(250, this, SLOT(toggleVisibility()));
        }
        break;
    }
    default:
        break;
    }

    QMainWindow::changeEvent(e);
}

bool MainWindow::event(QEvent *event)
{
//    QEvent::Type type = event->type();
//    if (type == QEvent::Hide)
//    {
//        event->ignore();
//        return false;
//    }
//    if (type == QEvent::WindowStateChange)
//    {
//        if (isMinimized())
//        {
//            event->ignore();
//            return true;
//        }
//    }
    return QMainWindow::event(event);
}
void MainWindow::closeEvent(QCloseEvent *e)
{
    if(maybeQuit())
    {
        e->accept();
    }
    else
        e->ignore();
}

void MainWindow::showEvent(QShowEvent *e)
{
    _presetManager->restoreWidgets();
    QMainWindow::showEvent(e);
}

bool MainWindow::maybeQuit()
{
    if(!_sessionController->isSessionStarted())
        return true;

    show();
    raise();
    activateWindow();
    qApp->processEvents();
    if(QMessageBox::Close == QMessageBox::question(this,
            tr( "Выйти из Call-o-Call?" ),
            tr( "Выйти действительно хотите выйти из Call-o-Call?"
                " В этом случае вы не сможете обрабатывать звонки,"
                " а все текущие звонки будут завершены."),
            QMessageBox::Close | QMessageBox::Cancel ))
    {
        qDebug() << tr("MainWindow::maybeQuit() - QMessageBox::Close");
        qApp->quit();
        return true;
    }

    return false;
}

void MainWindow::showLoginDialog()
{
    LateBoundObject<AppSettings> appSettings;

    disconnectHandlers();
    LoginDialog* dlg = new LoginDialog();
    dlg->setAttribute(Qt::WA_DeleteOnClose,true);

    connect(dlg,SIGNAL(updateLanguage(QString)),
        appSettings.instance(), SIGNAL(switchSessionLanguage(QString)));

    dlg->show();
    qDebug() << tr("MainWindow::showLoginDialog() - this->close()");
    this->close();
}

void MainWindow::disconnectHandlers()
{
    _callsController.instance()->disconnect(this);
    _sessionController.instance()->disconnect(this);
    _operListController.instance()->disconnect(this);
}

void MainWindow::on_actionQuit_triggered()
{
    QCoreApplication::instance()->exit(0);
}

void MainWindow::on_actionDisconnect_triggered()
{
    _sessionController->stopSession();
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::about(this, tr("О программе"), tr(
       "<b>Call-o-Call ® Operator Agent @ Forte-IT <a href='http://forte-it.ru'>(http://forte-it.ru)</a></b>"
       "<br/>Версия 2.9.12.10"
       "<br/>Программа предназначена для использования в составе контакт-центра Call-o-Call ®. "       
       ));
}

void MainWindow::on_actionSettings_triggered()
{
    _dlgOptions->exec();
    if (_bProgramPropertiesHasChanged)
    {
        _bProgramPropertiesHasChanged = false;
        if(QMessageBox::Close == QMessageBox::question(this,
                tr( "Применить настройки Call-o-Call?" ),
                tr( "Для применения настроек необходимо заново войти в систему."
                    " В этом случае вы не сможете обрабатывать звонки,"
                    " а все текущие звонки будут завершены."),
                QMessageBox::Close | QMessageBox::Cancel ))
        {
            //qApp->quit();
            _sessionController->stopSession();
        }

    }
//   if (_dlgOptions.exec() == QDialog::Accepted)
//   {
//   }
}

void MainWindow::toggleVisibility()
{
    //QWidget * currentWindow = _minimized?_minFloatDockWindow.data():this;  851: error: conditional expression between distinct pointer types 'QDockMinimizedWidget*' and 'MainWindow*' lacks a cast

#if defined(Q_OS_UNIX)
    return;
#endif

    QWidget * currentWindow = 0;
    if (_operMinCalls->isMinimized())
        currentWindow = _minFloatDockWindow;
    else
        currentWindow = this;

    if (!currentWindow)
        return;

    if (currentWindow->isVisible())
    {
        currentWindow->hide();
        //currentWindow->show();
    }
    else
        //currentWindow->show();
        activateForegroundWindow();

}

void MainWindow::togglePause()
{
    Operator me = _sessionController->sessionOwner();
    if(me.isPaused())
        _operListController->reqChangeStatus(me, Contact::Free, "");
    else
        _operListController->reqChangeStatus(me, Contact::Paused, "");
}

void MainWindow::moveFocusOnSearch()
{
//    if(_contactsWidget->parentWidget())
//    {
//        _contactsWidget->parentWidget()->show();
//        _contactsWidget->parentWidget()->raise();
//        _contactsWidget->parentWidget()->activateWindow();
//    }
//    _contactsWidget->setFocusOnSearch();
}

//void MainWindow::showDialer(const QString& phone)
//{
//    QWidget* w = _dialerWidget->parentWidget() && _dialerWidget->parentWidget()!=this
//                 ? _dialerWidget->parentWidget() : _dialerWidget;
//    w->show();
//    w->raise();
//    w->activateWindow();
//    _dialerWidget->setPhone(phone);
//    _dialerWidget->setFocusOnPhone();
//}

void MainWindow::callToTransferChanged(ccappcore::Call callToTransfer)
{
    if(callToTransfer.isValid())
        moveFocusOnSearch();
}

void MainWindow::moveToScreenCenter(QWidget* w)
{
    QRect screenRect = getScreenRect();
    QRect widgetRect = w->rect();
    widgetRect.moveCenter(screenRect.center());
    w->move(widgetRect.topLeft());
}

void MainWindow::on_actionOperListGroupMode_triggered(bool checked)
{
    _contactsManager->setOperListByGroupsMode(checked);
}

void MainWindow::on_actionHideOfflineContacts_triggered(bool checked)
{
    _contactsManager->hideOfflineOperators(checked);
    _contacts3      ->on_hideOfflineOperators(checked);
}

void MainWindow::on_actionPrivateGroupsOnly_triggered(bool checked)
{
    _contactsManager->showOnlyPrivateGroups(checked);
    _queueCallsWidget->reset();
    _contacts3       ->on_showPrivateGroupOnly(checked);

    emit privateOnly(checked);
}

void MainWindow::on_actionOperators_only_test_triggered(bool checked)
{
    _contactsManager->hideOperators(checked);
    _contacts3      ->on_hideOperators(checked);
}

void MainWindow::on_actionGroups_only_test_triggered(bool checked)
{
    _contactsManager->hideGroups(checked);
    _contacts3      ->on_hideGroups(checked);
}

void MainWindow::on_action_showUserCallTab_triggered(bool checked)
{
    _tabWidget->showTab(TB_USERCALLS,checked);
}

void MainWindow::on_action_showQueueCallTab_triggered(bool checked)
{
    _tabWidget->showTab(TB_QUEUECALLS,checked);
}

void MainWindow::on_action_showUsersTab_triggered(bool checked)
{
    _tabWidget->showTab(TB_USERS,checked);
}

void MainWindow::on_action_showHistoryTab_triggered(bool checked)
{
    _tabWidget->showTab(TB_HISTORY,checked);
}

void MainWindow::on_action_showMessagesTab_triggered(bool checked)
{
    _tabWidget->showTab(TB_MESSAGES,checked);
}

//void MainWindow::on_action_showWGStatusTab_triggered(bool checked)
//{
//    _tabWidget->showTab(TB_WGSTATUS,checked);
//}


void MainWindow::on_action_operform_visiable_triggered(bool checked)
{
    _dwOperFormTab->setVisible(checked);
}

void MainWindow::on_action_callform_visiable_triggered(bool checked)
{
    _dwCallFormTab->setVisible(checked);
    //_dwCallFormTab->setSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::MinimumExpanding);
}

//void MainWindow::on_operform_visibilityChanged(bool checked)
//{
//    _ui->action_operform_visiable->setChecked(checked);
//    _ui->menu_operform_opt->setEnabled(checked);
//}

void MainWindow::on_callform_visibilityChanged(bool visiable)
{
    _ui->action_callform_visiable->setChecked(visiable);
    QTimer::singleShot(0,_presetManager.instance(),SLOT(restoreWidgets()));
}

void MainWindow::onOperformClosed()
{
    _ui->action_operform_visiable->setChecked(false);
    _ui->menu_operform_opt->setEnabled(false);
}

//void MainWindow::onCallformClosed(bool visiable)
//{
//    _ui->action_callform_visiable->setChecked(visiable);
//}


void MainWindow::showCallFailReason(ccappcore::Call call,
                              ccappcore::Call::CallResult,
                              const QString& reason)
{
    if( _callsController->isIncomingCall(call) )
        return; //show call fail message for outgoing calls only

////    QWidget *widget = qApp->activeWindow();

////    if (_operMinCalls->isMinimized())
////        _minFloatDockWindow->show();
////    else
////        this->show();
    activateForegroundWindow();

//    if (_operMinCalls->isMinimized())
//        qApp->setActiveWindow(_minFloatDockWindow);
//    else
//        qApp->setActiveWindow(this);


//    if( QMessageBox::Retry == QMessageBox::information (
//            qApp->activeWindow(), tr("Вызов завершен"), reason,QMessageBox::Ok | QMessageBox::Retry
//            ))
//    {
//        _callsController->makeCall(call);
//    }

    QString message (tr("Вызов завершен: %1").arg(reason));
    statusInfo(message);
}

void MainWindow::showJoinCallToConfFailedReason(int callid, int error)
{
    Call call = _callsController->findByCallId(callid);
    if (!call.isValid())
        return;

    QString sError(tr("Ошибка аппаратуры"));
    if (error == -4)
        sError = tr("Звонок \"%1\" уже участвует в конференции").arg(_callsController->callAbonPhone(call));

    QMessageBox::warning (
                qApp->activeWindow(), tr("Ошибка создания конференции"), sError,QMessageBox::Ok);
}

void MainWindow::on_actionShowDialer_triggered()
{
    //showDialer();
}

void MainWindow::on_actionSavePreset_triggered()
{
    _presetManager->save();
}

QRect MainWindow::getScreenRect()const
{
    int screen = qApp->desktop()->screenNumber(this);
    return qApp->desktop()->screenGeometry(screen<0?0:screen);
}

void MainWindow::onActionMinimizeWindow(bool checked)
{
    qDebug() << QString(tr("minimizeWindow = %1")).arg(checked);
    if (checked)
    {
        this->hide();
        //addDockWidget(Qt::TopDockWidgetArea,_minFloatDockWindow);
        _minFloatDockWindow->show();
    }
    else
    {
        _minFloatDockWindow->hide();
        //removeDockWidget(_minFloatDockWindow);
        this->show();
    }
}

void MainWindow::onActionStaysOnTopWindow(bool checked)
{
    QWidget * currentWindow = 0;
    bool bFloatWidget = false;
    if (_operMinCalls->isMinimized())
    {
        currentWindow = _minFloatDockWindow;
        bFloatWidget = true;
    }
    else
        currentWindow = this;

    if (!currentWindow)
        return;

    if (checked)
        currentWindow->setWindowFlags(currentWindow->windowFlags() | Qt::ToolTip | Qt::WindowStaysOnTopHint);
    else
    {
        currentWindow->setWindowFlags(currentWindow->windowFlags() & ~Qt::ToolTip & ~Qt::WindowStaysOnTopHint);
        if (bFloatWidget)
            _minFloatDockWindow->setFloating(true);
    }

    currentWindow->show();
    currentWindow->activateWindow();

//    if(!_minFloatDockWindow)
//        return;

//    if (checked)
//        _minFloatDockWindow->setWindowFlags(_minFloatDockWindow->windowFlags() | Qt::ToolTip | Qt::WindowStaysOnTopHint);
//    else
//    {
//        _minFloatDockWindow->setWindowFlags(_minFloatDockWindow->windowFlags() & ~Qt::ToolTip & ~Qt::WindowStaysOnTopHint);
//        _minFloatDockWindow->setFloating(true);
//    }

//    _minFloatDockWindow->show();
//    _minFloatDockWindow->activateWindow();
}

void MainWindow::onActionExpandMinimzedWindow(qint32 size)
{
    if (!_minFloatDockWindow)
        return;

    qDebug() << QString(tr("expandMinimzedWindow = %1")).arg(size);
    QPoint windowPos   = _minFloatDockWindow->pos();
    int    windowWidth = _minFloatDockWindow->width();

//    qDebug() << QString(tr("windowPos.x = %1")).arg(windowPos.x());
//    qDebug() << QString(tr("windowPos.y = %1")).arg(windowPos.y());
//    qDebug() << QString(tr("windowWidth = %1")).arg(windowWidth);
//    qDebug() << QString(tr("realsize = %1")).arg(_minFloatDockWindow->realsize());

    QPropertyAnimation *animation1 = new QPropertyAnimation(_minFloatDockWindow,"geometry",this);

    animation1->setDuration(200);
    animation1->setStartValue(QRectF(windowPos.x(),
                                     windowPos.y(),
                                     windowWidth,
                                     30));
    animation1->setEndValue  (QRectF(windowPos.x() + (windowWidth - /*_minFloatDockWindowSaveWidth*/_minFloatDockWindow->realsize()) - size,
                                     windowPos.y(),
                                     _minFloatDockWindow->realsize()/*_minFloatDockWindowSaveWidth*/ + size,
                                     30));
    animation1->setEasingCurve(QEasingCurve::Linear);
    //connect(animation1, SIGNAL(finished()), this, SLOT(test()));

    animation1->start();

}

void MainWindow::setupFloatWindows()
{
//    QMainWindow *main = new QMainWindow();
//    QDockWidget *dw1 = new QDockWidget("1)Incoming +79055505661", main);
//    //dw1->setFeatures(QDockWidget::DockWidgetFloatable);
//    main->addDockWidget(Qt::BottomDockWidgetArea, dw1);

////    QWidget * wdg1 = new QWidget();

////    QPixmap pxm(":/resources/icons/new/pawn_glass_white.png");
////    QLabel * queueStatus = new QLabel(tr("QS"));
////    queueStatus->setPixmap(pxm);

////    QLabel * someTitle = new QLabel(tr("some title"));

////    QToolButton * mailButton  = new QToolButton();
////    mailButton->setIcon(QIcon(":/resources/icons/new/message green.png"));
////    mailButton->setAutoRaise(true);

////    QWidget* spacer = new QWidget();
////    spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

////    QHBoxLayout* mainLayout1 = new QHBoxLayout();
////    mainLayout1->addWidget(queueStatus);
////    mainLayout1->addWidget(someTitle);
////    mainLayout1->addWidget(spacer);
////    mainLayout1->addWidget(mailButton);
////    wdg1->setLayout(mainLayout1);

////    QPalette pal = dw1->palette();
////    pxm = QPixmap(":/style/resources/style/background5.png");
////    pal.setBrush(QPalette::Background, QBrush(pxm));
////    dw1->setPalette(pal);

////    dw1->setFeatures(QDockWidget::NoDockWidgetFeatures);
////    dw1->setTitleBarWidget(wdg1);

////    if (dw1->titleBarWidget())
////    {
////        int test = 0;
////    }

////    CallTab* callWidget = new CallTab( this );
////    dw1->setWidget(callWidget);



////    QDockWidget *dw2 = new QDockWidget("2)Incoming +79067837625", main);
////    main->addDockWidget(Qt::BottomDockWidgetArea, dw2);

////    main->tabifyDockWidget(dw1, dw2);
////    //main->splitDockWidget(dw1, dw2, Qt::Horizontal);
////    main->setTabPosition(Qt::BottomDockWidgetArea,QTabWidget::North);

////    dw2->raise();

//////    QTabBar *tabBar = main->findChild<QTabBar *>();
//////    if (tabBar)
//////    {
//////        int index = tabBar->currentIndex();
//////        //tabBar->moveTab(1,0);
//////        //dw2->raise();
//////        index = tabBar->currentIndex();
//////        int test = index;
//////    }


////    QWidget *wdg = new QWidget();
////    QHBoxLayout *mainLayout = new QHBoxLayout();
////    mainLayout->addWidget(main);
////    wdg->setLayout(mainLayout);

////    _dwCallFormTab = new QDockWidgetEx(tr("Карточка звонка"), this);
////    _dwCallFormTab->setAllowedAreas( Qt::AllDockWidgetAreas );

////    _dwCallFormTab->setFloating(false);
////    _dwCallFormTab->setWidget(wdg);
////    addDockWidget(Qt::BottomDockWidgetArea, _dwCallFormTab);
////    //connect(_dwCallFormTab,SIGNAL(visibilityChanged(bool)),this,SLOT(on_callform_visibilityChanged(bool)));
////    connect(_dwCallFormTab,SIGNAL(closed()),this,SLOT(onCallformClosed()));

//////    QDockWidget *dw3 = new QDockWidget("3)Incoming 89055505661", main);
//////    main->addDockWidget(Qt::BottomDockWidgetArea, dw3);
////////    main->tabifyDockWidget(dw3, dw2);
//////    main->splitDockWidget(dw2, dw3, Qt::Horizontal);
//////    dw3->raise();

    _dwCallFormTab = new DockTabsWidget(tr("Карточка звонка"), this);
    EmptyTitleBar* hiddenTitle = new EmptyTitleBar(this);
    _dwCallFormTab->setAllowedAreas( Qt::AllDockWidgetAreas );
    _dwCallFormTab->setTitleBarWidget(hiddenTitle);
    //_dwCallFormTab->setFloating(false);
    //_dwCallFormTab->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
    //_dwCallFormTab->setMinimumHeight(400);
    addDockWidget(Qt::BottomDockWidgetArea, _dwCallFormTab);
    //connect(_dwCallFormTab,SIGNAL(visibilityChanged(bool)),this,SLOT(onCallformClosed(bool)));
    _dwCallFormTab->hide();
}

void MainWindow::setupVolumeWidget()
{
    /*_volumeSlider = new QSlider(Qt::Vertical);

    //short a = 0xFFFF;
    _volumeSlider->setRange(0x0000,0xFFFE);
    connect(_volumeSlider.data(),SIGNAL(valueChanged(int)),this,SLOT(onVolumeChange(int)));
    connect(_volumeSlider.data(),SIGNAL(sliderReleased()),this,SLOT(onVolumeSliderReleased()));

    _dwVolumeForm = createDockWidget( "Volume",
                                      tr("Звук"),
                                     _volumeSlider );

    _dwVolumeForm->setWindowFlags(_dwVolumeForm->windowFlags() | Qt::ToolTip | Qt::WindowStaysOnTopHint);
    _dwVolumeForm->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);*/
}

void MainWindow::onShowVolumeWidget()
{
    /*_curVolumeValue = _volumeController.getVolume();
    _volumeSlider->setValue(_curVolumeValue);
    addDockWidget(Qt::RightDockWidgetArea,_dwVolumeForm);
    _dwVolumeForm->show();
    QPoint point = QCursor::pos();
    QRect widgetSize = _dwVolumeForm->childrenRect();
    _dwVolumeForm->setMinimumWidth(widgetSize.size().width());
    _dwVolumeForm->setGeometry(widgetSize);
    //_dwVolumeForm->raise();
    point.setY(point.y() - widgetSize.height()/2);
    _dwVolumeForm->move(point);*/
}

void MainWindow::onHideVolumeWidget()
{
    /*_dwVolumeForm->hide();
    removeDockWidget(_dwVolumeForm);*/

}

void MainWindow::onVolumeChange(int value)
{
    /*//_volumeController.setVolume(value);
    _curVolumeValue = value;*/
}

void MainWindow::onVolumeSliderReleased()
{
    /*_volumeController.setVolume(_curVolumeValue);
    _volumeTimer.stop();

    int pauseTimeInSeconds = 3;

    _volumeTimer.setSingleShot(true);
    connect(&_volumeTimer, SIGNAL(timeout()),
            this, SLOT(onHideVolumeWidget()),
            Qt::UniqueConnection);

    _volumeTimer.setInterval(pauseTimeInSeconds * 1000);
    _volumeTimer.start();*/

}

void MainWindow::activateForegroundWindow()
{
    QWidget * currentWindow = 0;
    if (_operMinCalls->isMinimized())
        currentWindow = _minFloatDockWindow;
    else
        currentWindow = this;

    if (!currentWindow)
        return;

    if (currentWindow->isMinimized())
        currentWindow->showMaximized();
    currentWindow->show();
    currentWindow->raise();
    currentWindow->activateWindow();

#ifdef Q_WS_WIN
////    if (!AllowSetForegroundWindow(ASFW_ANY))
////    {
////        DWORD error = GetLastError();
////        int test = error;
////    }

//    WId mwWinId = currentWindow->winId();
//    if (IsIconic(mwWinId))
//        SendMessage(mwWinId, WM_SYSCOMMAND, SC_RESTORE, 0);

//    DWORD foregroundThreadPId = GetWindowThreadProcessId(GetForegroundWindow(), NULL);
//    DWORD mwThreadPId         = GetWindowThreadProcessId(mwWinId, NULL);

//    if (foregroundThreadPId != mwThreadPId)
//    {
//        AttachThreadInput(foregroundThreadPId, mwThreadPId, true);
//        if (!SetForegroundWindow(mwWinId))
//        {
//            int test = 0;
//        }
//        AttachThreadInput(foregroundThreadPId, mwThreadPId, false);
//    }
//    else
//    {
//        SetForegroundWindow(mwWinId);
//    }

//    DWORD dwTimeout;

//    SystemParametersInfo(SPI_GETFOREGROUNDLOCKTIMEOUT, 0, &dwTimeout, 0);
//    if (!SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, 0, 0))
//    {
//        DWORD error = GetLastError();
//        int test = error;
//    }

    // hWnd - дескриптор окна.
    SetForegroundWindow(currentWindow->winId());

//    SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, (LPVOID)dwTimeout, 0);
#endif

}


void MainWindow::onActionOptionsApplied()
{
//     if(QMessageBox::Close == QMessageBox::question(this,
//             tr( "Применить настройки Call-o-Call?" ),
//             tr( "Для применения настроек необходимо заново войти в систему."
//                 " В этом случае вы не сможете обрабатывать звонки,"
//                 " а все текущие звонки будут завершены."),
//             QMessageBox::Close | QMessageBox::Cancel ))
//     {
//         //qApp->quit();
//         _sessionController->stopSession();
//     }
    _bProgramPropertiesHasChanged = true;
}
