#ifndef LOCALCALLHISTORYWIDGET_H
#define LOCALCALLHISTORYWIDGET_H

#include <QWidget>
#include "callswidget2.h"

using namespace ccappcore;

class QTreeView;

class LocalCallHistoryModel: public CallsModel
{
    Q_OBJECT;
public:
    explicit LocalCallHistoryModel(QObject* parent = 0);

protected:
    virtual void applyFilter(const PredicateGroup<Call>& filter);

private slots:
    void on_callStateChanged(ccappcore::Call call);
private:
    typedef QMap<int, Call> callsContainer;
    callsContainer _localCalls;
};

class LocalCallHistoryWidget : //public QWidget
        public IAlarmHandler
{
    Q_OBJECT
public:
    explicit LocalCallHistoryWidget(QWidget *parent = 0);
    
    // IAlarmHandler interface
    void connectToTimer();
    int getTabIndex()const;
    QIcon getTabIcon (bool _default = false) const;


signals:
    
public slots:
    
private:
    QPointer<  LocalCallHistoryModel  > _mainModel;
    QPointer<  QTreeView              > _mainView;

};

#endif // LOCALCALLHISTORYWIDGET_H
