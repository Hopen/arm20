/********************************************************************************
** Form generated from reading UI file 'opt_sound.ui'
**
** Created: Tue 26. Mar 13:54:36 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPT_SOUND_H
#define UI_OPT_SOUND_H

#include <Qt3Support/Q3MimeSourceFactory>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OptSound
{
public:
    QGridLayout *gridLayout;
    QCheckBox *checkBox;

    void setupUi(QWidget *OptSound)
    {
        if (OptSound->objectName().isEmpty())
            OptSound->setObjectName(QString::fromUtf8("OptSound"));
        OptSound->resize(333, 220);
        gridLayout = new QGridLayout(OptSound);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        checkBox = new QCheckBox(OptSound);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));

        gridLayout->addWidget(checkBox, 0, 0, 1, 1);


        retranslateUi(OptSound);

        QMetaObject::connectSlotsByName(OptSound);
    } // setupUi

    void retranslateUi(QWidget *OptSound)
    {
        OptSound->setWindowTitle(QApplication::translate("OptSound", "OptSoundUI", 0, QApplication::UnicodeUTF8));
        checkBox->setText(QApplication::translate("OptSound", "\320\222\320\272\320\273\321\216\321\207\320\270\321\202\321\214 \320\267\320\262\321\203\320\272\320\270", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class OptSound: public Ui_OptSound {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPT_SOUND_H
