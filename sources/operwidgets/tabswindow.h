#ifndef CALLFORM_H
#define CALLFORM_H

#include "operwidgets_global.h"
#include "appcore/callscontroller.h"
#include "appcore/callslistmodel.h"

#include "appcontext.h"
#include "opersettings.h"

//namespace Ui {
//    class TabsWindow;
//}

//using ccappcore::Call;
//using ccappcore::CallsController;
//using ccappcore::LateBoundObject;
//using ccappcore::ServiceProvider;

//namespace Ui {
//    class CallForm;
//}

//class CallTab;

//class TabsWindow : public QDialog {
//    Q_OBJECT
//    Q_DISABLE_COPY(TabsWindow)
//public:
//    TabsWindow(QWidget *parent = 0);
//    ~TabsWindow();

//public slots:
//    void showCall(ccappcore::Call);
//    void showContact(ccappcore::Contact);
//    void callStarted(ccappcore::Call);
//protected:
//    void changeEvent(QEvent *e);

//private:
//    Ui::TabsWindow *_ui;
//    LateBoundObject<CallsController> _callsController;
//    LateBoundObject<AppContext> _appContext;
//    LateBoundObject<OperSettings> _operSettings;
//private slots:
//    void on_tabs_tabCloseRequested(int index);
//    void onTabDestroyed();
//    void tabIconChanged();
//    void tabTitleChanged(const QString&);

//private:
//    void showTab(CallTab*);
//    CallTab* createTab();
//};

class TabsWidget : public QTabWidget
{
    Q_OBJECT
public:
    struct TTab
    {
        QWidget *_head;
        QIcon   _icon;
        QString _label;

        TTab(QWidget *head, const QIcon& icon, const QString& label):
            _head(head), _icon(icon), _label(label)
        {}
        TTab():_head(0)
        {}

    };

public:
    typedef QMap<quint32, TTab> tabsCollector;

    TabsWidget(QWidget *parent = 0);
    ~TabsWidget();

    //int addTab(quint32 index, QWidget *widget, const QIcon& icon, const QString &label);
    void showTab(const qint32& index, const bool& show = true);
    int addTab(IAlarmHandler *widget, const QString &label);

    tabsCollector* tabs(){return &_tabs;}

public slots:
    void onCurrentTabChanged(int index);
    void setIcon(const int& index, const QIcon& icon);

signals:
    void tabActive(const int& index);

private:
    tabsCollector _tabs;
    //QList<int> _index;
};

#endif // CALLFORM_H
