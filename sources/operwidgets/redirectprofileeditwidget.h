#ifndef REDIRECTPROFILEEDITWIDGET_H
#define REDIRECTPROFILEEDITWIDGET_H

#include <QWidget>
#include <QDialog>

class QLineEdit;
class QAbstractItemModel;
class QDataWidgetMapper;
class QComboBox;
class QModelIndex;

//class NonEditTableColumnDelegate : public QItemDelegate
//{
//    Q_OBJECT
//public:
//    NonEditTableColumnDelegate(QObject * parent = 0) : QItemDelegate(parent) {}
//    virtual QWidget * createEditor ( QWidget *, const QStyleOptionViewItem &,
//                                     const QModelIndex &) const
//    {
//        return 0;
//    }
//};

class RedirectProfileEditWidget : public QDialog
{
    Q_OBJECT
public:
    explicit RedirectProfileEditWidget(QAbstractItemModel* profileModel,
                                       QAbstractItemModel* taskModel,
                                       const QModelIndex & index,
                                       QWidget *parent = 0);

signals:

public slots:

    void on_btnOkCliked();
    void on_btnCancelCliked();
    //void on_taskChanged();
private:
     QLineEdit* _nameProfile;
     QComboBox* _currentTask;
     QDataWidgetMapper *_mapper;
};

#endif // REDIRECTPROFILEEDITWIDGET_H
