<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name>AddressExtraInfoModel</name>
    <message>
        <location filename="GlobalAddressBookWidget.cpp" line="179"/>
        <source>Тип</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="GlobalAddressBookWidget.cpp" line="181"/>
        <source>Значение</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="GlobalAddressBookWidget.cpp" line="184"/>
        <location filename="GlobalAddressBookWidget.cpp" line="206"/>
        <source>Неизвестно</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="GlobalAddressBookWidget.cpp" line="195"/>
        <source>Имя</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="GlobalAddressBookWidget.cpp" line="197"/>
        <source>E-mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="GlobalAddressBookWidget.cpp" line="199"/>
        <source>Телефон</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="GlobalAddressBookWidget.cpp" line="201"/>
        <source>Адрес</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="GlobalAddressBookWidget.cpp" line="203"/>
        <source>Организация</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AppContext</name>
    <message>
        <location filename="appcontext.cpp" line="61"/>
        <source>AppContext::callEnded - emit callToTransferChanged</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ApplicationToolBar</name>
    <message>
        <location filename="applicationwidget.cpp" line="17"/>
        <source>QS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="applicationwidget.cpp" line="21"/>
        <source>%1 ( 0 )</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="applicationwidget.cpp" line="42"/>
        <source>Голосовая почта</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="applicationwidget.cpp" line="48"/>
        <source>Помощь</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="applicationwidget.cpp" line="53"/>
        <source>Громкость</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="applicationwidget.cpp" line="59"/>
        <source>Настройки</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="applicationwidget.cpp" line="74"/>
        <location filename="applicationwidget.cpp" line="115"/>
        <source>Минимизировать</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="applicationwidget.cpp" line="84"/>
        <source>Закрепить поверх всех окон</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="applicationwidget.cpp" line="102"/>
        <source>%1 ( %2 из %3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="applicationwidget.cpp" line="115"/>
        <source>Развернуть</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BBListModel</name>
    <message>
        <location filename="bblistwidget.cpp" line="77"/>
        <source>group_id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bblistwidget.cpp" line="79"/>
        <source>msg_id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bblistwidget.cpp" line="81"/>
        <source>end</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bblistwidget.cpp" line="83"/>
        <source>Оператор</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bblistwidget.cpp" line="85"/>
        <source>Дата</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bblistwidget.cpp" line="87"/>
        <source>Текст сообщения</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bblistwidget.cpp" line="89"/>
        <source>Неизвестно</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Brouser</name>
    <message>
        <location filename="calltab.cpp" line="23"/>
        <source>Назад</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="29"/>
        <source>Вперед</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="35"/>
        <source>Остановить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="41"/>
        <source>Обновить</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CallContextMenu</name>
    <message>
        <location filename="callcontextmenu.cpp" line="9"/>
        <source>Открыть карточку звонка</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callcontextmenu.cpp" line="13"/>
        <source>Завершить звонок</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callcontextmenu.cpp" line="17"/>
        <source>Перезвонить абоненту</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callcontextmenu.cpp" line="21"/>
        <source>Соединить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callcontextmenu.cpp" line="25"/>
        <source>Добавить в конференцию</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callcontextmenu.cpp" line="29"/>
        <source>Удалить из конференции</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callcontextmenu.cpp" line="33"/>
        <source>Удалить конференцию</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CallFormWidget</name>
    <message>
        <location filename="callformwidget.cpp" line="205"/>
        <source>CallFormWidget::~CallFormWidget()</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CallPropertyWidget</name>
    <message>
        <location filename="callswidgetm.cpp" line="63"/>
        <source>A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidgetm.cpp" line="69"/>
        <source>B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidgetm.cpp" line="74"/>
        <source>Приоритет</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidgetm.cpp" line="79"/>
        <source>Инфо</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidgetm.cpp" line="118"/>
        <location filename="callswidgetm.cpp" line="119"/>
        <location filename="callswidgetm.cpp" line="120"/>
        <location filename="callswidgetm.cpp" line="121"/>
        <source>%1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CallScriptWidget</name>
    <message>
        <location filename="callscriptwidget.cpp" line="117"/>
        <source>Поиск</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CallTab</name>
    <message>
        <location filename="calltab.ui" line="20"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="26"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Service Level: &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600; color:#ff0000;&quot;&gt;GOLD&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;	Сайт: &lt;/span&gt;&lt;a href=&quot;www.google.com&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;www.google.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="66"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Показать информацию о клиенте&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Показать\скрыть контактную информацию о клиенте&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="112"/>
        <source>Клиент</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="122"/>
        <source>Не ассоциирован</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="137"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Редактировать&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Позволяет ассоциировать звонок с существующим клиентом или создать нового клиента, а так же &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;добавить контактную информацию: телефон, e-mail и т.п.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="244"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt;&quot;&gt;Регион: Не определен&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="316"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Показать информацию о звонке&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Показать\скрыть подробную информацию о звонке&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="362"/>
        <source>Разговор</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="398"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Начало: 14:00:00		Call ID: 2345&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Комментарий: На логический номер 001&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="451"/>
        <location filename="calltab.ui" line="922"/>
        <source>Начать разговор</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="457"/>
        <location filename="calltab.ui" line="504"/>
        <location filename="calltab.ui" line="610"/>
        <location filename="calltab.ui" line="657"/>
        <source>Ответить/Завершить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="498"/>
        <location filename="calltab.ui" line="910"/>
        <source>Завершить разговор</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="550"/>
        <source>Идет разговор</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="604"/>
        <source>Включить\Выключить звук</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="651"/>
        <source>Поставить на удержание</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="698"/>
        <source>Перевести звонок</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="730"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Соединить с...&lt;/span&gt;&lt;br /&gt;&lt;span style=&quot; color:#676767;&quot;&gt;Выберите абонента из выпадающего списка чтобы соединить звонок.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="737"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="799"/>
        <source>Доп. информация</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="850"/>
        <source>Введите доп. информацию к звонку</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="857"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Добавить&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Добавить информацию о звонке&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="889"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Заполнить анкету &lt;/span&gt;&lt;img src=&quot;:/resources/icons/questform_16x16.png&quot; /&gt; &lt;a href=&quot;http://www.auto-strahovka.ru/&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#0000ff;&quot;&gt;&amp;quot;Автострахование&amp;quot;&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="907"/>
        <source>Завершить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="919"/>
        <source>Начать</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="934"/>
        <source>Включить\выключить запись разговора</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="937"/>
        <source>Включитьвыключить запись разговора</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="950"/>
        <source>Включить\выключить звук</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="953"/>
        <source>Включитьвыключить звук</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="965"/>
        <source>Поставить\снять с удержания</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.ui" line="968"/>
        <source>Поставитьснять с удержания</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="110"/>
        <source>&lt;Донабор&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="111"/>
        <location filename="calltab.cpp" line="116"/>
        <source>Тема не задана</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="383"/>
        <source>Соединить с &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="486"/>
        <source>Не определен</source>
        <comment>Карточка звонка, регион не определен</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="487"/>
        <source>Телефон: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="488"/>
        <source>Регион: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="490"/>
        <source>Начало: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="491"/>
        <source>CallID: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="494"/>
        <source>Комментарий: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="503"/>
        <source>Показать историю звонков за:&amp;nbsp;&amp;nbsp;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="505"/>
        <source>Неделя</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="510"/>
        <source>Месяц</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="515"/>
        <source>Год</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="551"/>
        <source>CallTab::updateCallInfo()</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="568"/>
        <source>Имя: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="568"/>
        <source>Нет</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="570"/>
        <source>Полное имя: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="595"/>
        <source>CallTab::updateClientInfo()</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="621"/>
        <source>emit readyToQuit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="783"/>
        <source>История звонков</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="870"/>
        <source>Executing extraInfo(CI_External):&quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="calltab.cpp" line="879"/>
        <source>Error executing extraInfo:&quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CallsModel</name>
    <message>
        <location filename="callswidget2.cpp" line="103"/>
        <source>other call model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidget2.cpp" line="148"/>
        <source>Номер абонента</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidget2.cpp" line="150"/>
        <source>Состояние</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidget2.cpp" line="152"/>
        <source>Группа</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidget2.cpp" line="154"/>
        <source>Тип</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidget2.cpp" line="156"/>
        <source>Время начала</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidget2.cpp" line="158"/>
        <source>Время состояния</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidget2.cpp" line="160"/>
        <source>Приоритет</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidget2.cpp" line="162"/>
        <source>Доп. информация</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidget2.cpp" line="164"/>
        <source>Последний оператор</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidget2.cpp" line="166"/>
        <location filename="callswidget2.cpp" line="168"/>
        <source>Длительность звонка</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidget2.cpp" line="170"/>
        <source>Исходящий номер</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CallsWidget2</name>
    <message>
        <location filename="callswidget2.cpp" line="627"/>
        <source>CallsWidget2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CallsWidgetM</name>
    <message>
        <location filename="callswidgetm.cpp" line="325"/>
        <source>CallsWidgetM::on_callStateChanged - Call::CS_CallFailed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChatContextMenu</name>
    <message>
        <location filename="chattab.cpp" line="250"/>
        <source>Копировать</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chattab.cpp" line="254"/>
        <source>История сообщений</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chattab.cpp" line="258"/>
        <source>Вчера</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chattab.cpp" line="261"/>
        <source>7 дней</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chattab.cpp" line="264"/>
        <source>3 месяца</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chattab.cpp" line="267"/>
        <source>Полгода</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chattab.cpp" line="270"/>
        <source>Год</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChatTab</name>
    <message>
        <location filename="chattab.cpp" line="566"/>
        <source>Отправить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chattab.cpp" line="575"/>
        <source>Закрыть</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chattab.cpp" line="652"/>
        <source>Закрыть текущее</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chattab.cpp" line="653"/>
        <source>Закрыть все, кроме текущего</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContactContextMenu</name>
    <message>
        <location filename="contactcontextmenu.cpp" line="10"/>
        <location filename="contactcontextmenu.cpp" line="39"/>
        <source>Позвонить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactcontextmenu.cpp" line="14"/>
        <location filename="contactcontextmenu.cpp" line="48"/>
        <source>Перевести звонок</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactcontextmenu.cpp" line="19"/>
        <location filename="contactcontextmenu.cpp" line="57"/>
        <source>Написать письмо</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactcontextmenu.cpp" line="41"/>
        <source>Позвонить (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactcontextmenu.cpp" line="50"/>
        <source>Перевести звонок (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactcontextmenu.cpp" line="59"/>
        <source>Написать (%1) </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContactStatusMenu</name>
    <message>
        <location filename="contactstatusmenu.cpp" line="11"/>
        <source>Статус</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactstatusmenu.cpp" line="261"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContactsModel</name>
    <message>
        <location filename="contactswidget3.cpp" line="76"/>
        <source>Имя</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactswidget3.cpp" line="78"/>
        <source>Текущий статус</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactswidget3.cpp" line="80"/>
        <source>Номер телефона</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactswidget3.cpp" line="82"/>
        <source>Последнее действие</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactswidget3.cpp" line="84"/>
        <source>Время последнего действия</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactswidget3.cpp" line="87"/>
        <source>Сообщения</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactswidget3.cpp" line="90"/>
        <source>Неизвестно</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContactsWidget</name>
    <message>
        <location filename="contactswidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactswidget.ui" line="55"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactswidget.ui" line="80"/>
        <source>Поиск</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactswidget.ui" line="99"/>
        <source>Вызов</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactswidget.ui" line="137"/>
        <source>&amp;Перевести</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactswidget.ui" line="146"/>
        <source>&amp;Вызов</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactswidget.cpp" line="90"/>
        <source>Контакты не найдены</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactswidget.cpp" line="95"/>
        <source>Перевести на номер: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactswidget.cpp" line="95"/>
        <source>Позвонить на номер: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactswidget.cpp" line="98"/>
        <source>Контакты не найдены.&lt;br/&gt;&lt;br/&gt;%1&lt;br/&gt;%2&lt;br/&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContactsWidget3</name>
    <message>
        <location filename="contactswidget3.cpp" line="358"/>
        <source>Позвонить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="contactswidget3.cpp" line="362"/>
        <source>Написать</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialPad</name>
    <message>
        <location filename="dialpad.ui" line="38"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpad.ui" line="101"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpad.ui" line="123"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpad.ui" line="142"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpad.ui" line="161"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpad.ui" line="180"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpad.ui" line="199"/>
        <source>6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpad.ui" line="218"/>
        <source>7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpad.ui" line="237"/>
        <source>8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpad.ui" line="256"/>
        <source>9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpad.ui" line="275"/>
        <source>#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpad.ui" line="294"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpad.ui" line="313"/>
        <source>*</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialingPauseHandler</name>
    <message>
        <location filename="dialingpausehandler.cpp" line="13"/>
        <source>Набор номера</source>
        <comment>Статус оператора, устанавливаемый во время набора номера</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialpadForm</name>
    <message>
        <location filename="dialpadform.ui" line="90"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpadform.ui" line="170"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpadform.ui" line="210"/>
        <source>Вызов</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpadform.ui" line="261"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpadform.ui" line="291"/>
        <source>#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpadform.ui" line="315"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpadform.ui" line="339"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpadform.ui" line="363"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpadform.ui" line="387"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpadform.ui" line="411"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpadform.ui" line="435"/>
        <source>6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpadform.ui" line="459"/>
        <source>7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpadform.ui" line="483"/>
        <source>8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpadform.ui" line="507"/>
        <source>9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpadform.ui" line="531"/>
        <source>*</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpadform.ui" line="545"/>
        <source>&amp;Перевести</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpadform.ui" line="554"/>
        <source>&amp;Вызов</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialpadform.cpp" line="12"/>
        <source>Телефон</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DockTabsWidget</name>
    <message>
        <location filename="docktabswidget.cpp" line="287"/>
        <source>DockTabsWidget::~DockTabsWidget()</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditThresholdsDialog</name>
    <message>
        <location filename="editthresholdsdialog.ui" line="20"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editthresholdsdialog.ui" line="26"/>
        <source>Тип пороговых значений</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editthresholdsdialog.ui" line="32"/>
        <source>Прямые</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editthresholdsdialog.ui" line="42"/>
        <source>Обратные</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editthresholdsdialog.ui" line="55"/>
        <source>Значения</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editthresholdsdialog.ui" line="61"/>
        <location filename="editthresholdsdialog.cpp" line="23"/>
        <source>Нормальное (не больше)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editthresholdsdialog.ui" line="78"/>
        <location filename="editthresholdsdialog.cpp" line="25"/>
        <source>Среднее (не больше)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editthresholdsdialog.cpp" line="24"/>
        <source>Среднее (не меньше)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editthresholdsdialog.cpp" line="26"/>
        <source>Нормальное (не меньше)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GlobalAddressBookWidget</name>
    <message>
        <location filename="GlobalAddressBookWidget.cpp" line="364"/>
        <source>Поиск</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HTMLColumnDelegate</name>
    <message>
        <location filename="chattab.cpp" line="61"/>
        <source>%1 (%2):</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IndicatorContextMenu</name>
    <message>
        <location filename="indicatorcontextmenu.cpp" line="7"/>
        <source>Показать в граф. виде</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="indicatorcontextmenu.cpp" line="9"/>
        <source>Пороговые значения...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LayoutOptionsTab</name>
    <message>
        <location filename="options/opt_layout.cpp" line="18"/>
        <source>Внешний вид</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_layout.cpp" line="19"/>
        <source>Настройки внешнего вида и расположения элементов пользовательского интерфейса</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LayoutOptionsWidget</name>
    <message>
        <location filename="options/opt_layout.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_layout.ui" line="22"/>
        <source>Запретить перемещение плавающих окон</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginDialog</name>
    <message>
        <location filename="logindialog.ui" line="23"/>
        <source>Рабочее место оператора</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logindialog.ui" line="127"/>
        <source>Имя пользователя</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logindialog.ui" line="141"/>
        <source>Пароль</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logindialog.ui" line="164"/>
        <source>Входить автоматически</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logindialog.ui" line="171"/>
        <source>Режим аутентификации Windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logindialog.ui" line="188"/>
        <source>Настройки</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logindialog.cpp" line="95"/>
        <location filename="logindialog.cpp" line="110"/>
        <source>Ошибка входа в систему.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logindialog.cpp" line="96"/>
        <source>Оператор с таким именем или параметрами телефонного подключения уже зарегистрирован в системе и будет отключен. Все равно войти?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="161"/>
        <source> @ Call-o-Call Operator Agent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="215"/>
        <source>MainWindow::~MainWindow()</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="394"/>
        <location filename="mainwindow.cpp" line="427"/>
        <source>В данный момент нет активных разговоров.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="434"/>
        <source>В данный момент очередь пуста.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="468"/>
        <source>Карточка оператора</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="480"/>
        <source>Звонки на операторе</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="481"/>
        <source>Состояние очереди</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="482"/>
        <source>Состояние операторов</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="483"/>
        <source>Текущая статистика</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="484"/>
        <source>Объявления</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="485"/>
        <source>Адресная книга</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="531"/>
        <source>animation finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="557"/>
        <source>Call-a-Call minimized panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="825"/>
        <source>Выйти из Call-o-Call?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="826"/>
        <source>Выйти действительно хотите выйти из Call-o-Call? В этом случае вы не сможете обрабатывать звонки, а все текущие звонки будут завершены.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="831"/>
        <source>MainWindow::maybeQuit() - QMessageBox::Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="845"/>
        <source>MainWindow::showLoginDialog() - this-&gt;close()</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="868"/>
        <source>О программе</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="868"/>
        <source>&lt;b&gt;Call-o-Call ® Operator Agent @ Forte-IT &lt;a href=&apos;http://forte-it.ru&apos;&gt;(http://forte-it.ru)&lt;/a&gt;&lt;/b&gt;&lt;br/&gt;Версия 2.4.24.11&lt;br/&gt;Программа предназначена для использования в составе контакт-центра Call-o-Call ®. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="882"/>
        <source>Применить настройки Call-o-Call?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="883"/>
        <source>Для применения настроек необходимо заново войти в систему. В этом случае вы не сможете обрабатывать звонки, а все текущие звонки будут завершены.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1097"/>
        <source>Вызов завершен: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1107"/>
        <source>Ошибка аппаратуры</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1109"/>
        <source>Звонок &quot;%1&quot; уже участвует в конференции</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1112"/>
        <source>Ошибка создания конференции</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1133"/>
        <source>minimizeWindow = %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1195"/>
        <source>expandMinimzedWindow = %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1309"/>
        <source>Карточка звонка</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NoCallsWidget</name>
    <message>
        <location filename="nocallswidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="nocallswidget.ui" line="35"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table border=&quot;0&quot; style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#7f7f7f;&quot;&gt;В данный момент у вас нет &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#7f7f7f;&quot;&gt;активных звонков.&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NoContactsWidget</name>
    <message>
        <location filename="nocontactswidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="nocontactswidget.ui" line="35"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table border=&quot;0&quot; style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; color:#7f7f7f;&quot;&gt;Контакты не найдены&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OperCallsWidget</name>
    <message>
        <location filename="callswidget2.cpp" line="745"/>
        <source>OperCallsWidget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidget2.cpp" line="752"/>
        <source>Соединить звонки</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidget2.cpp" line="756"/>
        <source>Создать конференцию</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidget2.cpp" line="807"/>
        <location filename="callswidget2.cpp" line="831"/>
        <location filename="callswidget2.cpp" line="839"/>
        <location filename="callswidget2.cpp" line="880"/>
        <location filename="callswidget2.cpp" line="888"/>
        <source>Соединение звонков</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidget2.cpp" line="808"/>
        <source>Соединить звонок %1 со звонком %2 ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidget2.cpp" line="832"/>
        <location filename="callswidget2.cpp" line="881"/>
        <source>Добавить звонок %1 в конференцию ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidget2.cpp" line="840"/>
        <location filename="callswidget2.cpp" line="889"/>
        <source>Добавить звонки в конференцию ?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OperToolBar</name>
    <message>
        <location filename="opertoolswidget.cpp" line="34"/>
        <source>&lt;Телефон&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opertoolswidget.cpp" line="41"/>
        <source>*</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opertoolswidget.cpp" line="42"/>
        <source>#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opertoolswidget.cpp" line="72"/>
        <source>Набрать номер</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opertoolswidget.cpp" line="77"/>
        <source>Очистить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opertoolswidget.cpp" line="320"/>
        <source>Личный</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opertoolswidget.cpp" line="321"/>
        <source>Группа</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opertoolswidget.cpp" line="322"/>
        <source>Общий</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opertoolswidget.cpp" line="694"/>
        <source>Перевести звонок на ящик группы,
в которую по-умолчанию входит оператор?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opertoolswidget.cpp" line="698"/>
        <source>Перевести звонок на общий ящик
голосовой почты?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opertoolswidget.cpp" line="702"/>
        <source>Перевести звонок на личную голосовую
почту оператора?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opertoolswidget.cpp" line="706"/>
        <source>Перевод звонка</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptApplication</name>
    <message>
        <location filename="options/opt_application.ui" line="20"/>
        <source>OptApplicationUI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_application.ui" line="26"/>
        <source>Настройки подключения к IS3 Router</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_application.ui" line="32"/>
        <location filename="options/opt_application.ui" line="240"/>
        <source>Имя сервера или IP адрес</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_application.ui" line="42"/>
        <source>Порт</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_application.ui" line="62"/>
        <source>Способ коммутации</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_application.ui" line="81"/>
        <source>Телефонная линия</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_application.ui" line="86"/>
        <source>VoIP-телефония</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_application.ui" line="94"/>
        <source>автоматически определять IP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_application.ui" line="104"/>
        <source>Параметры оборудования</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_application.ui" line="112"/>
        <source>ID маршрута / Поток</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_application.ui" line="141"/>
        <source>Телефонный номер / Линия</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_application.ui" line="163"/>
        <source>Аккаунт Jabber</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_application.ui" line="186"/>
        <source>XMPP Адрес</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_application.ui" line="193"/>
        <source>Пароль</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_application.ui" line="234"/>
        <source>Сервер SIP регистрации</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptShortcuts</name>
    <message>
        <location filename="options/opt_shortcuts.ui" line="26"/>
        <source>OptShortcutsUI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_shortcuts.ui" line="54"/>
        <source>Действие</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_shortcuts.ui" line="59"/>
        <source>Сочетание</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_shortcuts.ui" line="75"/>
        <source>Редактировать...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_shortcuts.ui" line="82"/>
        <source>Удалить</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptSound</name>
    <message>
        <location filename="options/opt_sound.ui" line="14"/>
        <source>OptSoundUI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_sound.ui" line="23"/>
        <source>Включить звуки</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionEditor</name>
    <message>
        <location filename="options/optioneditor.ui" line="30"/>
        <source>Option:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/optioneditor.ui" line="53"/>
        <source>Typ:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/optioneditor.ui" line="66"/>
        <source>Value: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsDlg</name>
    <message>
        <location filename="options/optionsdlg.cpp" line="432"/>
        <source>Настройки</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsTabApplication</name>
    <message>
        <location filename="options/opt_application.cpp" line="30"/>
        <source>Подключение</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_application.cpp" line="31"/>
        <source>Настройки подключения к серверу</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsTabCallForm</name>
    <message>
        <location filename="options/opt_callform.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_callform.ui" line="20"/>
        <source>Темы звонка</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_callform.ui" line="47"/>
        <source>Добавить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_callform.ui" line="54"/>
        <source>Удалить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_callform.ui" line="79"/>
        <location filename="options/opt_callform.ui" line="92"/>
        <source>Если включено, карточка звонка будет показана сразу по приходу звонка в систему. В противном случае, чтобы показать карточку звонка, необходимо выбрать звонок из списка и дважды щелкнуть мышью по нему. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_callform.ui" line="82"/>
        <source>Показывать карточку звонка сразу</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_callform.ui" line="95"/>
        <source>Закрывать карточку звонка по окончании разговора</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_callform.cpp" line="14"/>
        <source>Карточка звонка</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_callform.cpp" line="15"/>
        <source>Свойства карточки звонка</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsTabMain</name>
    <message>
        <location filename="options/opt_main.cpp" line="17"/>
        <source>Общие</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_main.cpp" line="18"/>
        <source>Общие настройки приложения</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsTabMainWidget</name>
    <message>
        <location filename="options/opt_main.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_main.ui" line="20"/>
        <source>Автообновление</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_main.ui" line="44"/>
        <source>OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_main.ui" line="49"/>
        <source>FATAL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_main.ui" line="54"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_main.ui" line="59"/>
        <source>WARN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_main.ui" line="64"/>
        <source>INFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_main.ui" line="69"/>
        <source>DEBUG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_main.ui" line="74"/>
        <source>TRACE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_main.ui" line="79"/>
        <source>ALL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_main.ui" line="97"/>
        <source>Путь к файлу логов</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_main.ui" line="104"/>
        <source>Уровень протоколирования</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_main.ui" line="115"/>
        <source>Длительность вплывающего уведомления</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_main.ui" line="121"/>
        <source>Индикация о входящем звонке (сек)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_main.ui" line="128"/>
        <source>Индикация о входящем сообщении (сек)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_main.ui" line="184"/>
        <source>Автоперевод звонка через (сек)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsTabOperStatus</name>
    <message>
        <location filename="options/opt_operstatus.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_operstatus.ui" line="20"/>
        <source>Причины паузы</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_operstatus.ui" line="47"/>
        <source>Добавить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_operstatus.ui" line="54"/>
        <source>Удалить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_operstatus.ui" line="79"/>
        <location filename="options/opt_operstatus.ui" line="89"/>
        <source>Если включено, карточка звонка будет показана сразу по приходу звонка в систему. В противном случае, чтобы показать карточку звонка, необходимо выбрать звонок из списка и дважды щелкнуть мышью по нему. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_operstatus.ui" line="82"/>
        <source>Автоматически сниматься с паузы при логине</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_operstatus.ui" line="92"/>
        <source>Ставить на паузу при наборе номера</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_operstatus.cpp" line="68"/>
        <source>Статус</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_operstatus.cpp" line="68"/>
        <source>Статус операторов</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsTabOperToolForm</name>
    <message>
        <location filename="options/opt_opertools.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_opertools.ui" line="20"/>
        <location filename="options/opt_opertools.cpp" line="75"/>
        <source>Настройка элементов управления</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_opertools.ui" line="26"/>
        <source>Основные функции</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_opertools.ui" line="53"/>
        <location filename="options/opt_opertools.ui" line="64"/>
        <location filename="options/opt_opertools.ui" line="75"/>
        <location filename="options/opt_opertools.ui" line="86"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_opertools.ui" line="99"/>
        <source>Функции оператора</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_opertools.cpp" line="74"/>
        <source>Элементы управление</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsTabShortcuts</name>
    <message>
        <location filename="options/opt_shortcuts.cpp" line="56"/>
        <source>Сочетания клавиш</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_shortcuts.cpp" line="57"/>
        <source>Сочетания клавиш для быстрого вызова часто исп. функций</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsTabSound</name>
    <message>
        <location filename="options/opt_sound.cpp" line="18"/>
        <source>Звуки</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_sound.cpp" line="19"/>
        <source>Настройка звуков для событий в системе</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_sound.cpp" line="120"/>
        <source>Выберите звуковой файл</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/opt_sound.cpp" line="122"/>
        <source>Звуковый файлы (*.wav)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsUI</name>
    <message>
        <location filename="options/ui_options.ui" line="20"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/ui_options.ui" line="96"/>
        <source>Сохранить на сервер</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/ui_options.ui" line="103"/>
        <source>Загрузить с сервера</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PhoneLineWidget</name>
    <message>
        <location filename="transferwidget.cpp" line="330"/>
        <source>Номер</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProfileListModel</name>
    <message>
        <location filename="redirprofilewidget.cpp" line="57"/>
        <source>id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirprofilewidget.cpp" line="58"/>
        <source>name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirprofilewidget.cpp" line="59"/>
        <source>taskId</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirprofilewidget.cpp" line="60"/>
        <source>status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirprofilewidget.cpp" line="220"/>
        <source>OFF</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProfileSystemMaster</name>
    <message>
        <location filename="redirruleswidget.cpp" line="394"/>
        <location filename="redirruleswidget.cpp" line="404"/>
        <location filename="redirruleswidget.cpp" line="407"/>
        <source>ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirruleswidget.cpp" line="441"/>
        <source>Canceled by user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirruleswidget.cpp" line="448"/>
        <location filename="redirruleswidget.cpp" line="469"/>
        <source>0,1,2,3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirruleswidget.cpp" line="464"/>
        <source>Cannot find voice mail script</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="callscriptwidget.cpp" line="193"/>
        <location filename="transferwidget.cpp" line="304"/>
        <source>Название функции</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callscriptwidget.cpp" line="194"/>
        <location filename="transferwidget.cpp" line="305"/>
        <source>Описание</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidget2.cpp" line="10"/>
        <source>Соединить звонки</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="callswidget2.cpp" line="11"/>
        <source>Создать конференцию</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chattab.cpp" line="11"/>
        <source>&lt;font size=&quot;4&quot; color=&quot;red&quot;  face=&quot;Arial&quot;&gt;%1 (%2): &lt;/font&gt; &lt;font size=&quot;4&quot; color=&quot;black&quot; face=&quot;Arial&quot;&gt;%3&lt;/font&gt;&lt;p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chattab.cpp" line="13"/>
        <source>&lt;font size=&quot;4&quot; color=&quot;blue&quot; face=&quot;Arial&quot;&gt;%1 (%2): &lt;/font&gt; &lt;font size=&quot;4&quot; color=&quot;black&quot; face=&quot;Arial&quot;&gt;%3&lt;/font&gt;&lt;p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="operevents.cpp" line="13"/>
        <source>Входящий звонок</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="operevents.cpp" line="20"/>
        <source>Исходящий звонок</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="operevents.cpp" line="26"/>
        <source>Окончание звонка</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="operevents.cpp" line="31"/>
        <source>Новый звонок в очереди</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="operevents.cpp" line="34"/>
        <source>Звонок в личной очереди</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opersettings.cpp" line="125"/>
        <source>Начать разговор</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opersettings.cpp" line="127"/>
        <source>Повесить трубку</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opersettings.cpp" line="131"/>
        <source>Ожидание</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opersettings.cpp" line="133"/>
        <source>Перевести на IVR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opersettings.cpp" line="135"/>
        <source>Перевести звонок</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opersettings.cpp" line="137"/>
        <source>Голосовая почта</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opersettings.cpp" line="139"/>
        <source>Перезайти</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opersettings.cpp" line="141"/>
        <source>Запись</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opersettings.cpp" line="143"/>
        <source>Перевести на оператора</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opersettings.cpp" line="145"/>
        <source>Перевести на группу</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opersettings.cpp" line="147"/>
        <source>Перевести на номер</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opersettings.cpp" line="150"/>
        <source>Неизвестно</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="volumecontroller.cpp" line="363"/>
        <location filename="volumecontroller.cpp" line="423"/>
        <source>Error: waveOutGetDevCapsA %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="volumecontroller.cpp" line="371"/>
        <source>Error: volume controler does not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="volumecontroller.cpp" line="377"/>
        <source>Error: waveOutSetVolume %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="volumecontroller.cpp" line="410"/>
        <source>Volume set to: %1
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="volumecontroller.cpp" line="431"/>
        <source>Error: volume controler  does not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="volumecontroller.cpp" line="437"/>
        <source>Error: waveOutGetVolume %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="volumecontroller.cpp" line="472"/>
        <source>Windows volume: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QueueCallsWidget</name>
    <message>
        <location filename="callswidget2.cpp" line="921"/>
        <source>QueueCallsWidget</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RadioDelegate</name>
    <message>
        <location filename="redirprofilewidget.cpp" line="11"/>
        <source>text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RedirProfileWidget</name>
    <message>
        <location filename="redirprofilewidget.cpp" line="261"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirprofilewidget.cpp" line="265"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirprofilewidget.cpp" line="270"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RedirectCallsWidget</name>
    <message>
        <location filename="redirruleswidget.cpp" line="8"/>
        <source>Номер телефона</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirruleswidget.cpp" line="14"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirruleswidget.cpp" line="16"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirruleswidget.cpp" line="38"/>
        <source>Переводить звонки</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RedirectProfileEditWidget</name>
    <message>
        <location filename="redirectprofileeditwidget.cpp" line="7"/>
        <source>Имя профайла</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirectprofileeditwidget.cpp" line="12"/>
        <source>Доб. правило</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirectprofileeditwidget.cpp" line="17"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirectprofileeditwidget.cpp" line="19"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirectprofileeditwidget.cpp" line="58"/>
        <source>Создать профиль</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RedirectRulesWidget</name>
    <message>
        <location filename="redirruleswidget.cpp" line="214"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirruleswidget.cpp" line="216"/>
        <source>Звонок на номер</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirruleswidget.cpp" line="217"/>
        <source>Звонок на голосовую почту</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirruleswidget.cpp" line="224"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirruleswidget.cpp" line="228"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RuleListModel</name>
    <message>
        <location filename="redirruleswidget.cpp" line="54"/>
        <source>id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirruleswidget.cpp" line="55"/>
        <source>name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirruleswidget.cpp" line="56"/>
        <source>ifType</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirruleswidget.cpp" line="57"/>
        <source>ifValue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirruleswidget.cpp" line="58"/>
        <source>actType</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirruleswidget.cpp" line="59"/>
        <source>actValue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="redirruleswidget.cpp" line="60"/>
        <source>maxCount</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScriptListModel</name>
    <message>
        <location filename="scripteventwidget.cpp" line="11"/>
        <source>id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scripteventwidget.cpp" line="12"/>
        <source>name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scripteventwidget.cpp" line="13"/>
        <source>desc</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabsWindow</name>
    <message>
        <location filename="tabswindow.ui" line="20"/>
        <source>Звонки</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="tabswindow.ui" line="49"/>
        <source>Tab 2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolButtonOperStatusEx</name>
    <message>
        <location filename="toolbuttonoperstatus.cpp" line="74"/>
        <source>Пауза</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransferContext</name>
    <message>
        <location filename="transferwidget.cpp" line="16"/>
        <source>Перевести на оператора</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transferwidget.cpp" line="17"/>
        <source>Перевести на группу</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transferwidget.cpp" line="18"/>
        <source>Перевести на IVR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transferwidget.cpp" line="19"/>
        <location filename="transferwidget.cpp" line="131"/>
        <source>Перевести на номер</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transferwidget.cpp" line="106"/>
        <source>Скрипты IVR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transferwidget.cpp" line="114"/>
        <source>Операторы</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="transferwidget.cpp" line="123"/>
        <source>Группы</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransferWidget</name>
    <message>
        <location filename="transferwidget.cpp" line="145"/>
        <source>Поиск</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <location filename="trayicon.cpp" line="19"/>
        <source>Call-o-Call® Operator Agent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="28"/>
        <source>Показать\Скрыть главное окно</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="32"/>
        <source>Выйти</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="93"/>
        <source>Объявление</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="155"/>
        <source>TrayIcon::on_quit()</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>mainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="19"/>
        <source>Call-o-Call Operator Agent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="70"/>
        <source>&amp;Общие</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="80"/>
        <source>&amp;Действия</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="87"/>
        <source>&amp;Справка</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="93"/>
        <source>&amp;Вид</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="104"/>
        <source>Окно</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="108"/>
        <source>Настройка карточки оператора</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="134"/>
        <source>&amp;Настройки</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="137"/>
        <source>Настройки подключения</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="146"/>
        <source>&amp;О программе</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="149"/>
        <source>Информация о программе</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="154"/>
        <source>&amp;Выход</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="157"/>
        <source>Закрыть приложение</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="166"/>
        <source>&amp;Отключиться</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="169"/>
        <source>Отключиться от сервера</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="184"/>
        <source>Показывать операторов по &amp;группам</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="202"/>
        <source>Скрыть оффлайн контакты</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="220"/>
        <source>Показывать контакты и очередь только для своих групп</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="229"/>
        <source>Набрать номер</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="241"/>
        <source>Вызвать</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="250"/>
        <source>Перевести</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="255"/>
        <source>Сохранить пресет...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="266"/>
        <source>Operators only test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="277"/>
        <source>Groups only test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="291"/>
        <source>Карточка оператора</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="302"/>
        <source>Карточка звонка</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="317"/>
        <source>Звонки на операторе</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="332"/>
        <source>Состояние очереди</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="347"/>
        <source>Состояние операторов</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="362"/>
        <source>Текущая статистика</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="377"/>
        <source>Объявления</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
