#include "editthresholdsdialog.h"
#include "ui_editthresholdsdialog.h"

#include "appcore/quantativeindicator.h"

EditThresholdsDialog::EditThresholdsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditThresholdsDialog)
{
    ui->setupUi(this);

    connect(ui->buttonBox, SIGNAL(accepted()),SLOT(commitData()));
    connect(ui->radioButtonStandard, SIGNAL(toggled(bool)),SLOT(commitData()));
}

EditThresholdsDialog::~EditThresholdsDialog()
{
    delete ui;
}

void EditThresholdsDialog::commitData()
{
    static QString normalLabel = tr("Нормальное (не больше)");
    static QString normalInvertedLabel = tr("Среднее (не меньше)");
    static QString aboveNormalLabel = tr("Среднее (не больше)");
    static QString aboveNormalInvertedLabel = tr("Нормальное (не меньше)");

    bool inverted = isInverted();
    ui->normalValueLabel->setText( inverted ? normalInvertedLabel : normalLabel );
    ui->aboveNormalLabel->setText( inverted ? aboveNormalInvertedLabel: aboveNormalLabel );
}

bool EditThresholdsDialog::isInverted() const
{
    return ui->radioButtonInverted->isChecked();
}

int EditThresholdsDialog::warningValue() const
{
    return ui->normalValueSpinBox->value();
}

int EditThresholdsDialog::criticalValue() const
{
    return ui->aboveNormalSpinBox->value();
}

void EditThresholdsDialog::setValues(bool inverted, int warning, int critical)
{
    ui->radioButtonInverted->setChecked( inverted );
    ui->normalValueSpinBox->setValue( warning );
    ui->aboveNormalSpinBox->setValue( critical );
}






