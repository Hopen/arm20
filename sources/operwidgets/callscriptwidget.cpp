#include "QtGui"

#include "callscriptwidget.h"
#include "lineeditwithhint.h"
#include "nocontactswidget.h"
//#include "scriptview.h"

//ScriptTableModel::ScriptTableModel(QObject *parent)
//    : QAbstractTableModel(parent) {
//    mColumns << tr("Название функции")
//             << tr("Описание");
//}

////bool ScriptTableModel::insertColumn(int column, const QString &title) {
////    if (column >= 0 && column <= mColumns.size()) {
////        beginInsertColumns(QModelIndex(), column, column);
////        mColumns.insert(column, title);
////        int size = mModelData.size();
////        for (int i = 0; i < size; ++i) {
////            mModelData[i].insert(column, qrand());
////        }
////        reset();
////        endInsertColumns();

////        return true;
////    }
////    return false;
////}

//bool ScriptTableModel::insertItem(int row, int column, const QString &value)
//{
//    beginInsertRows(QModelIndex(),row,row);
//    mModelData[row].insert(column, value);
//    endInsertRows();
//    reset();
//    return true;
//}

//void ScriptTableModel::addRow()
//{
//    mModelData << QVariantList();
//}


////bool ScriptTableModel::removeColumn(int column) {
////    if (column >= 0 && column < mColumns.size()) {
////        beginRemoveColumns(QModelIndex(), column, column);
////        mColumns.removeAt(column);
////        int size = mModelData.size();
////        for (int i = 0; i < size; ++i) {
////            mModelData[i].removeAt(column);
////        }
////        reset();
////        endRemoveColumns();
////        return true;
////    }
////    return false;
////}

//int ScriptTableModel::rowCount(const QModelIndex & /* parent */) const {
//    return mViewData.count();
//}

//int ScriptTableModel::columnCount(const QModelIndex & /* parent */) const {
//    return mColumns.count();
//}

//QVariant ScriptTableModel::data(const QModelIndex &index, int role) const {
//    if (!index.isValid() || role != Qt::DisplayRole)
//        return QVariant();
//    return mViewData.value(index.row()).value(index.column());
//}

//QVariant ScriptTableModel::headerData(int section,
//                             Qt::Orientation orientation,
//                             int role) const {
//    if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
//        return mColumns.value(section);
//    }
//    return QAbstractTableModel::headerData(section, orientation, role);
//}

//void ScriptTableModel::search(QString filter)
//{
//    emit layoutAboutToBeChanged();
//    mViewData.clear();
//    if (filter.isEmpty())
//    {
//        mViewData = mModelData;
//    }
//    else
//    {
//        for (int i=0;i<mModelData.size();++i)
//        {
//            QVariant val1 = mModelData[i].value(0);
//            QVariant val2 = mModelData[i].value(1);
//            if (val1.toString().contains(filter,Qt::CaseInsensitive) || val2.toString().contains(filter,Qt::CaseInsensitive))
//                mViewData.push_back(mModelData[i]);
//        }
//    }

//    emit layoutChanged();
//}

//void ScriptTableModel::reset()
//{
//    mViewData.clear();
//    mViewData = mModelData;
//}


CallScriptWidget::CallScriptWidget(QWidget *parent) :
    QWidget(parent)
{
    _searchLine  = new LineEditWithHint(this);
    _searchLine->setAutoFillBackground(false);
    _searchLine->setHintText(tr("Поиск"));

    //_scriptModel = new ScriptTableModel(this);
    _proxyModel = new QSortFilterProxyModel;
    _proxyModel->setDynamicSortFilter(true);
    _proxyModel->setFilterKeyColumn(1);

    _scriptView = new QTreeView(this);
    _scriptView->setRootIsDecorated(false);
    _scriptView->setAlternatingRowColors(true);
    _scriptView->setModel(/*_scriptModel*/_proxyModel);
    _scriptView->setSortingEnabled(true);

    //_scriptView->verticalHeader()->setVisible(false);
    _scriptView->resize(300, 50);

    //_scriptView->horizontalHeader()->setStretchLastSection(true);
    _scriptView->setSelectionBehavior(QAbstractItemView::SelectRows);

    //_noContactsWidget = new NoContactsWidget(_scriptView);
    //_noContactsWidget->hide();

//    Call::MultiInfo multiinfo;
//    int rowcount = 0;
//    if (c.extraInfo(Call::CI_ScriptFuncName, multiinfo))
//    {
//        foreach(const CallInfo& info, multiinfo)
//        {
//            //setRowCount(this->rowCount()+1);
//            _scriptModel->addRow();
//            QStringList list = info.value().split(";");
//            QString s1 = list[0];
//            QString s2 = list[1];
//            _scriptModel->insertItem(rowcount,0,s1);
//            _scriptModel->insertItem(rowcount,1,s2);
//            ++rowcount;
//        }

//    }

    //_scriptView->resizeColumnsToContents();
    //_scriptView->resizeRowsToContents();
    _scriptView->sortByColumn(1, Qt::AscendingOrder);

    _transferButton = new QToolButton(this);
    QPixmap pixmap = (QIcon(":/resources/icons/new/transfer on.png")).pixmap(QSize(16, 16), QIcon::Disabled, QIcon::On);
    _transferButton->setIcon(QIcon(pixmap));
    _transferButton->setDisabled(true);


    connect(_searchLine.data(), SIGNAL(textChanged(const QString &)), this, SLOT(on_searchLine_textChanged(QString)));
    connect(_transferButton.data(),SIGNAL (clicked(bool)), this, SLOT(on_bnTransfer_clicked(bool)));

    //connect(_scriptModel,SIGNAL(layoutChanged()),this, SLOT(toggleEmptyWidget()));
    connect(_scriptView.data(),SIGNAL(clicked(QModelIndex)),this,SLOT(on_scriptSelect(QModelIndex)));
    connect(_scriptView.data(),SIGNAL(doubleClicked(QModelIndex)),this,SLOT(onScriptSelected(QModelIndex)));

    QVBoxLayout *topLayout = new QVBoxLayout();
    topLayout->addWidget(_scriptView);

    QHBoxLayout *bottomLayout = new QHBoxLayout();
    bottomLayout->addWidget(_searchLine);
    bottomLayout->addWidget(_transferButton);

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addLayout(topLayout);
    mainLayout->addLayout(bottomLayout);
    setLayout(mainLayout);
}

void CallScriptWidget::assignCall(const ccappcore::Call &c)
{
    Call::MultiInfo multiinfo;
    if (c.extraInfo(Call::CI_ScriptFuncName, multiinfo))
    {
        QStandardItemModel *model = new QStandardItemModel(0, 2, this);
        model->setHeaderData(0, Qt::Horizontal, QObject::tr("Название функции"));
        model->setHeaderData(1, Qt::Horizontal, QObject::tr("Описание"));

        foreach(const CallInfo& info, multiinfo)
        {
            QStringList list = info.value().split(";");
            //QString s1 = list[0];
            //QString s2 = list[1];

            model->insertRow(0);
            model->setData(model->index(0, 0), list[0]);
            model->setData(model->index(0, 1), list[1]);


            //setRowCount(this->rowCount()+1);
//                _scriptModel->addRow();
//                QStringList list = info.value().split(";");
//                QString s1 = list[0];
//                QString s2 = list[1];
//                _scriptModel->insertItem(rowcount,0,s1);
//                _scriptModel->insertItem(rowcount,1,s2);
//                ++rowcount;
        }

        setSourceModel(model);

    }
}

void CallScriptWidget::setSourceModel(QAbstractItemModel *model)
{
    _proxyModel->setSourceModel(model);
}


void CallScriptWidget::on_searchLine_textChanged(QString searchText)
{
//    if(searchText.isEmpty())
//    {
//        _noContactsWidget->setHtml(tr("Скрипты не найдены"));
//    }
////    else
////    {
////        QString action = /*_appContext->callToTransfer().isValid() ?*/
////                         tr("Перевести на номер: ") /*: tr("Позвонить на номер: ")*/;
////        QString url = "callto:"+searchText;
////        QString href = "<a href='" + url + "'>"+searchText+"</a>";
////        QString msg = tr("Контакты не найдены.<br/><br/>%1<br/>%2<br/>")
////              .arg(action).arg(href);
////        _noContactsWidget->setHtml(msg);
////    }


    if(!_searchLine->isHintVisible())
    {
        search(searchText);
    }
    else
        search(QString());


}

void CallScriptWidget::search(const QString &text)
{
    QRegExp regExp(text, Qt::CaseInsensitive, QRegExp::FixedString);
    _proxyModel->setFilterRegExp(regExp);
}

void CallScriptWidget::on_bnTransfer_clicked(bool)
{
    if (!_selectedScript.isEmpty())
        emit scriptSelected(_selectedScript);
}

//void CallScriptWidget::toggleEmptyWidget()
//{
//    if(!_noContactsWidget)
//        return;

////    if (_scriptModel->rowCount()>0)
////    {
////        if (!_noContactsWidget->isHidden())
////        {
////            _noContactsWidget->hide();
////            _topLayout->removeWidget(_noContactsWidget);
////            _topLayout->addWidget(_scriptView);

////        }
////    }
////    else
////    {
////        if (_noContactsWidget->isHidden())
////        {
////            _noContactsWidget->show();
////            _topLayout->removeWidget(_scriptView);
////            _topLayout->addWidget(_noContactsWidget);

////        }
////    }
//}


//void CallScriptWidget::onScriptSelected(int row, int)
//{
//    QTableWidgetItem *selectedItem = _scriptView->item(row,0);

//    if (!selectedItem)
//        return;
//    QString scriptName = selectedItem->text();
//    emit scriptSelected(scriptName);

//}

//void CallScriptWidget::on_scriptSelect(int row,int)
//{
//    QTableWidgetItem *selectedItem = _scriptView->item(row,0);
//    if (!selectedItem)
//        return;
//    _selectedScript = selectedItem->text();


//    QPixmap pixmap = (QIcon(":/resources/icons/new/transfer on.png")).pixmap(QSize(16, 16), QIcon::Normal, QIcon::On);
//    _transferButton->setIcon(QIcon(pixmap));
//    _transferButton->setEnabled(true);
//}

void CallScriptWidget::on_scriptSelect(QModelIndex index)
{
     _selectedScript = index.sibling(index.row(),0).data().toString();

     QPixmap pixmap = (QIcon(":/resources/icons/new/transfer on.png")).pixmap(QSize(16, 16), QIcon::Normal, QIcon::On);
     _transferButton->setIcon(QIcon(pixmap));
     _transferButton->setEnabled(true);

}

void CallScriptWidget::onScriptSelected(QModelIndex index)
{
    _selectedScript = index.sibling(index.row(),0).data().toString();
    emit scriptSelected(_selectedScript);
}
