#ifndef SCRIPTEVENTWIDGET_H
#define SCRIPTEVENTWIDGET_H

#include <QWidget>
#include "appcore/appsettings.h"
#include "appcore/lateboundobject.h"
#include "appcore/redirectlistcontroller.h"


class QAbstractListModel;
class QTreeView;

using namespace ccappcore;

class ScriptListModel : public QAbstractListModel
{
    Q_OBJECT

    enum ColDef
    {
        IdColumn = 0,
        NameColumn,
        DescColumn
    };

    public:
        ScriptListModel(QObject *parent = 0);


        int rowCount(const QModelIndex &parent = QModelIndex()) const;
        QVariant data(const QModelIndex &index, int role) const;
        QVariant headerData(int section, Qt::Orientation orientation,
                            int role = Qt::DisplayRole) const;
        int columnCount ( const QModelIndex & parent = QModelIndex() ) const;

    private:
        LateBoundObject<RedirectListController> _controller;
        QStringList _columnsList;
};

class ScriptEventWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ScriptEventWidget(QWidget *parent = 0);
    
signals:
    
public slots:
private:
    QPointer<  QAbstractListModel  > _mainModel;
    QPointer<  QTreeView           > _mainView;

};

#endif // SCRIPTEVENTWIDGET_H
