#ifndef OPERWIDGETS_GLOBAL_H
#define OPERWIDGETS_GLOBAL_H

#include <QtCore>
#include <QtGui>

//#include "appcore/iappmodule.h"

#ifndef QT_NODLL
#  if defined(OPERWIDGETS_LIBRARY)
#    define OPERWIDGETS_EXPORT Q_DECL_EXPORT
#  else
#    define OPERWIDGETS_EXPORT Q_DECL_IMPORT
#  endif
#else
#  define OPERWIDGETS_EXPORT
#endif


#endif // OPERWIDGETS_GLOBAL_H
