/********************************************************************************
** Form generated from reading UI file 'opt_layout.ui'
**
** Created: Thu 19. Nov 12:31:13 2015
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPT_LAYOUT_H
#define UI_OPT_LAYOUT_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QHeaderView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LayoutOptionsWidget
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QCheckBox *cbLockLayout;

    void setupUi(QWidget *LayoutOptionsWidget)
    {
        if (LayoutOptionsWidget->objectName().isEmpty())
            LayoutOptionsWidget->setObjectName(QString::fromUtf8("LayoutOptionsWidget"));
        LayoutOptionsWidget->resize(468, 234);
        verticalLayout_2 = new QVBoxLayout(LayoutOptionsWidget);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        cbLockLayout = new QCheckBox(LayoutOptionsWidget);
        cbLockLayout->setObjectName(QString::fromUtf8("cbLockLayout"));

        verticalLayout->addWidget(cbLockLayout);


        verticalLayout_2->addLayout(verticalLayout);


        retranslateUi(LayoutOptionsWidget);

        QMetaObject::connectSlotsByName(LayoutOptionsWidget);
    } // setupUi

    void retranslateUi(QWidget *LayoutOptionsWidget)
    {
        LayoutOptionsWidget->setWindowTitle(QApplication::translate("LayoutOptionsWidget", "Form", 0, QApplication::UnicodeUTF8));
        cbLockLayout->setText(QApplication::translate("LayoutOptionsWidget", "\320\227\320\260\320\277\321\200\320\265\321\202\320\270\321\202\321\214 \320\277\320\265\321\200\320\265\320\274\320\265\321\211\320\265\320\275\320\270\320\265 \320\277\320\273\320\260\320\262\320\260\321\216\321\211\320\270\321\205 \320\276\320\272\320\276\320\275", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class LayoutOptionsWidget: public Ui_LayoutOptionsWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPT_LAYOUT_H
