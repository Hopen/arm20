#ifndef CONTACTSWIDGETM_H
#define CONTACTSWIDGETM_H

#include <QWidget>
#include "callswidget2.h"


using namespace ccappcore;

class QLineEditEx: public QLineEdit
{
    Q_OBJECT

public:
    explicit QLineEditEx(QWidget *parent = 0):QLineEdit(parent)
    {

    }

    explicit QLineEditEx(const QString &s, QWidget* parent=0):QLineEdit(s,parent)
    {
    }
    virtual ~QLineEditEx(){}

    virtual QSize minimumSizeHint()const
    {
        QSize minSizeHint = QLineEdit::minimumSizeHint();
        minSizeHint = QSize(0,minSizeHint.height());
        return minSizeHint;
    }
};

class QDockMinimizedWidget:public QDockWidget
{
    Q_OBJECT
public:
    explicit QDockMinimizedWidget(const QString &title, QWidget *parent = 0, Qt::WindowFlags flags = 0);

    explicit QDockMinimizedWidget(QWidget *parent = 0, Qt::WindowFlags flags = 0);

    ~QDockMinimizedWidget(){}

    void setGeometry(const QRect &);
    qint32 realsize() const{return _realSize;}
private:
    qint32 _realSize;
};


//class QDockWidgetEx:public QDockWidget
//{
//    Q_OBJECT
//public:
//    explicit QDockWidgetEx(const QString &title, QWidget *parent = 0, Qt::WindowFlags flags = 0);

//    explicit QDockWidgetEx(QWidget *parent = 0, Qt::WindowFlags flags = 0);

//    ~QDockWidgetEx();

//protected:
//    void closeEvent(QCloseEvent *event);
//signals:
//    void closed();
//};
//class QDockMinimizedWidget2:public QDockWidget
//{
//    Q_OBJECT
//public:
//    explicit QDockMinimizedWidget2(const QString &title, QWidget *parent = 0, Qt::WindowFlags flags = 0);

//    explicit QDockMinimizedWidget2(QWidget *parent = 0, Qt::WindowFlags flags = 0);

//    ~QDockMinimizedWidget2();

//};


const int BLINK_COUNT = 5;

class CallPropertyWidget: public QWidget
{
    Q_OBJECT

public:
    explicit CallPropertyWidget(QWidget *parent = 0);

    virtual QSize minimumSizeHint()const;
    void setCall  (ccappcore::Call c);
    void resetCall();
    qint32 realSize()const;
protected:
    void paintEvent(QPaintEvent *event);

signals:
    void setCallComplited(qint32 size);
    //void resetCallCompleted();
private slots:
    void emitCallStateTimeChanged();

private:
    QPointer <QLineEdit > _a;
    QPointer <QLineEdit > _b;
    QPointer <QLineEdit > _priority;
    QPointer <QLineEdit > _extra;

    LateBoundObject < CallsController > _controller;
    QTimer _newCallStateTimer;
    int    _newCallStateCount;
    QColor _backgroundSaveColor;

};

//#include "appcore/domainobjectfilter.h"

class ApplicationToolBar;
class OperToolBar;

class CallsWidgetM : public QWidget
{
    Q_OBJECT
public:
    explicit CallsWidgetM(bool isOnTop, QWidget *parent = 0);
    virtual ~CallsWidgetM();

    bool isOnTop()const {return _stayOnTop;}
    bool isMinimized() const {return _minimized;}
signals:
    //void testExpand();
    //void minimizeWindow(bool);
    //void staysOnTopWindow(bool);
    void setWindowSize(qint32);
    //void packWindow();
    void expand();

public slots:
    void on_callStateChanged(ccappcore::Call);
    bool isOwnerCall(ccappcore::Call);
    void test();
    void on_actionSetCallCompleted(qint32);
    void on_mainWindowMinimized(bool);
    void on_stayOnTop(bool);
    void on_minimized(bool);
    //void on_actionResetCallCompleted();


private:
//    QPointer<  CallsModelM  > _mainModel;
//    QPointer<  QTreeView    > _mainView;

    //QPointer <QLabel > _text;
    //QPointer <QLineEdit > _text;
    QPointer <CallPropertyWidget> _call;

    LateBoundObject < AppContext      > _appContext;
    LateBoundObject < CallsController > _controller;
    QPointer<ApplicationToolBar>    _appltools;
    QPointer<OperToolBar>           _opertools;
    //QPointer<QToolBar>              _commontools;
    QPointer<QToolButton>  _hideCalls;
    QPointer<QStateMachine>   _stateMachine;
    ccappcore::Call _associatedCall;
    bool _stayOnTop;
    bool _minimized;
};


#endif // CONTACTSWIDGETM_H
