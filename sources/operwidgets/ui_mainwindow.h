/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Tue 26. Mar 13:54:37 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QStatusBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_mainWindow
{
public:
    QAction *actionSettings;
    QAction *actionAbout;
    QAction *actionQuit;
    QAction *actionDisconnect;
    QAction *actionOperListGroupMode;
    QAction *actionHideOfflineContacts;
    QAction *actionPrivateGroupsOnly;
    QAction *actionShowDialer;
    QAction *actionMakeCall;
    QAction *actionTransferCall;
    QAction *actionSavePreset;
    QAction *actionOperators_only_test;
    QAction *actionGroups_only_test;
    QAction *action_operform_visiable;
    QAction *action_callform_visiable;
    QAction *action_showUserCallTab;
    QAction *action_showQueueCallTab;
    QAction *action_showUsersTab;
    QAction *action_showHistoryTab;
    QAction *action_showMessagesTab;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QMenuBar *menuBar;
    QMenu *menuGeneral;
    QMenu *menuActions;
    QMenu *menuHelp;
    QMenu *menuView;
    QMenu *menuWindow;
    QMenu *menu_operform_opt;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *mainWindow)
    {
        if (mainWindow->objectName().isEmpty())
            mainWindow->setObjectName(QString::fromUtf8("mainWindow"));
        mainWindow->resize(530, 306);
        QFont font;
        font.setFamily(QString::fromUtf8("Arial"));
        mainWindow->setFont(font);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/resources/icons/appicon_32x32.png"), QSize(), QIcon::Normal, QIcon::Off);
        mainWindow->setWindowIcon(icon);
        mainWindow->setAutoFillBackground(true);
        mainWindow->setStyleSheet(QString::fromUtf8(""));
        mainWindow->setDocumentMode(true);
        mainWindow->setTabShape(QTabWidget::Rounded);
        mainWindow->setDockNestingEnabled(true);
        mainWindow->setDockOptions(QMainWindow::AllowNestedDocks|QMainWindow::AnimatedDocks|QMainWindow::ForceTabbedDocks);
        mainWindow->setUnifiedTitleAndToolBarOnMac(false);
        actionSettings = new QAction(mainWindow);
        actionSettings->setObjectName(QString::fromUtf8("actionSettings"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/resources/icons/gears_16x16.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSettings->setIcon(icon1);
        actionAbout = new QAction(mainWindow);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/resources/icons/about_32x32.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAbout->setIcon(icon2);
        actionQuit = new QAction(mainWindow);
        actionQuit->setObjectName(QString::fromUtf8("actionQuit"));
        actionDisconnect = new QAction(mainWindow);
        actionDisconnect->setObjectName(QString::fromUtf8("actionDisconnect"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/resources/icons/server_connection_16x16.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionDisconnect->setIcon(icon3);
        actionOperListGroupMode = new QAction(mainWindow);
        actionOperListGroupMode->setObjectName(QString::fromUtf8("actionOperListGroupMode"));
        actionOperListGroupMode->setCheckable(true);
        actionOperListGroupMode->setEnabled(false);
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/resources/icons/group_16x16.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionOperListGroupMode->setIcon(icon4);
        actionOperListGroupMode->setVisible(true);
        actionHideOfflineContacts = new QAction(mainWindow);
        actionHideOfflineContacts->setObjectName(QString::fromUtf8("actionHideOfflineContacts"));
        actionHideOfflineContacts->setCheckable(true);
        actionHideOfflineContacts->setEnabled(true);
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/resources/icons/oper_offline_16x16.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionHideOfflineContacts->setIcon(icon5);
        actionHideOfflineContacts->setVisible(true);
        actionPrivateGroupsOnly = new QAction(mainWindow);
        actionPrivateGroupsOnly->setObjectName(QString::fromUtf8("actionPrivateGroupsOnly"));
        actionPrivateGroupsOnly->setCheckable(true);
        actionPrivateGroupsOnly->setEnabled(true);
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/resources/icons/group_private_16x16.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionPrivateGroupsOnly->setIcon(icon6);
        actionShowDialer = new QAction(mainWindow);
        actionShowDialer->setObjectName(QString::fromUtf8("actionShowDialer"));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/resources/icons/dial_32x32.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionShowDialer->setIcon(icon7);
        actionShowDialer->setVisible(false);
        actionMakeCall = new QAction(mainWindow);
        actionMakeCall->setObjectName(QString::fromUtf8("actionMakeCall"));
        QIcon icon8;
        icon8.addFile(QString::fromUtf8(":/resources/icons/call_start_32x32.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionMakeCall->setIcon(icon8);
        actionTransferCall = new QAction(mainWindow);
        actionTransferCall->setObjectName(QString::fromUtf8("actionTransferCall"));
        QIcon icon9;
        icon9.addFile(QString::fromUtf8(":/resources/icons/call_transfer_32x32.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionTransferCall->setIcon(icon9);
        actionSavePreset = new QAction(mainWindow);
        actionSavePreset->setObjectName(QString::fromUtf8("actionSavePreset"));
        actionOperators_only_test = new QAction(mainWindow);
        actionOperators_only_test->setObjectName(QString::fromUtf8("actionOperators_only_test"));
        actionOperators_only_test->setCheckable(true);
        actionOperators_only_test->setEnabled(false);
        actionOperators_only_test->setVisible(false);
        actionGroups_only_test = new QAction(mainWindow);
        actionGroups_only_test->setObjectName(QString::fromUtf8("actionGroups_only_test"));
        actionGroups_only_test->setCheckable(true);
        actionGroups_only_test->setVisible(false);
        action_operform_visiable = new QAction(mainWindow);
        action_operform_visiable->setObjectName(QString::fromUtf8("action_operform_visiable"));
        action_operform_visiable->setCheckable(true);
        action_operform_visiable->setChecked(true);
        action_callform_visiable = new QAction(mainWindow);
        action_callform_visiable->setObjectName(QString::fromUtf8("action_callform_visiable"));
        action_callform_visiable->setCheckable(true);
        action_callform_visiable->setChecked(false);
        action_showUserCallTab = new QAction(mainWindow);
        action_showUserCallTab->setObjectName(QString::fromUtf8("action_showUserCallTab"));
        action_showUserCallTab->setCheckable(true);
        action_showUserCallTab->setChecked(true);
        QIcon icon10;
        icon10.addFile(QString::fromUtf8(":/resources/icons/new/user_headset.png"), QSize(), QIcon::Normal, QIcon::Off);
        action_showUserCallTab->setIcon(icon10);
        action_showQueueCallTab = new QAction(mainWindow);
        action_showQueueCallTab->setObjectName(QString::fromUtf8("action_showQueueCallTab"));
        action_showQueueCallTab->setCheckable(true);
        action_showQueueCallTab->setChecked(true);
        QIcon icon11;
        icon11.addFile(QString::fromUtf8(":/resources/icons/new/server.png"), QSize(), QIcon::Normal, QIcon::Off);
        action_showQueueCallTab->setIcon(icon11);
        action_showUsersTab = new QAction(mainWindow);
        action_showUsersTab->setObjectName(QString::fromUtf8("action_showUsersTab"));
        action_showUsersTab->setCheckable(true);
        action_showUsersTab->setChecked(true);
        QIcon icon12;
        icon12.addFile(QString::fromUtf8(":/resources/icons/new/users1.png"), QSize(), QIcon::Normal, QIcon::Off);
        action_showUsersTab->setIcon(icon12);
        action_showHistoryTab = new QAction(mainWindow);
        action_showHistoryTab->setObjectName(QString::fromUtf8("action_showHistoryTab"));
        action_showHistoryTab->setCheckable(true);
        action_showHistoryTab->setChecked(true);
        QIcon icon13;
        icon13.addFile(QString::fromUtf8(":/resources/icons/new/history2.png"), QSize(), QIcon::Normal, QIcon::Off);
        action_showHistoryTab->setIcon(icon13);
        action_showMessagesTab = new QAction(mainWindow);
        action_showMessagesTab->setObjectName(QString::fromUtf8("action_showMessagesTab"));
        action_showMessagesTab->setCheckable(true);
        action_showMessagesTab->setChecked(true);
        QIcon icon14;
        icon14.addFile(QString::fromUtf8(":/resources/icons/new/messages.png"), QSize(), QIcon::Normal, QIcon::Off);
        action_showMessagesTab->setIcon(icon14);
        centralWidget = new QWidget(mainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetMaximumSize);
        mainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(mainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 530, 21));
        menuGeneral = new QMenu(menuBar);
        menuGeneral->setObjectName(QString::fromUtf8("menuGeneral"));
        menuActions = new QMenu(menuBar);
        menuActions->setObjectName(QString::fromUtf8("menuActions"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        menuView = new QMenu(menuBar);
        menuView->setObjectName(QString::fromUtf8("menuView"));
        menuWindow = new QMenu(menuBar);
        menuWindow->setObjectName(QString::fromUtf8("menuWindow"));
        menu_operform_opt = new QMenu(menuWindow);
        menu_operform_opt->setObjectName(QString::fromUtf8("menu_operform_opt"));
        mainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(mainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        mainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuGeneral->menuAction());
        menuBar->addAction(menuView->menuAction());
        menuBar->addAction(menuActions->menuAction());
        menuBar->addAction(menuWindow->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuGeneral->addAction(actionSettings);
        menuGeneral->addAction(actionSavePreset);
        menuGeneral->addSeparator();
        menuGeneral->addAction(actionDisconnect);
        menuGeneral->addAction(actionQuit);
        menuActions->addSeparator();
        menuActions->addAction(actionShowDialer);
        menuHelp->addAction(actionAbout);
        menuView->addAction(actionOperListGroupMode);
        menuView->addAction(actionHideOfflineContacts);
        menuView->addAction(actionPrivateGroupsOnly);
        menuView->addSeparator();
        menuView->addAction(actionOperators_only_test);
        menuView->addAction(actionGroups_only_test);
        menuWindow->addAction(action_operform_visiable);
        menuWindow->addAction(action_callform_visiable);
        menuWindow->addSeparator();
        menuWindow->addAction(menu_operform_opt->menuAction());
        menu_operform_opt->addAction(action_showUserCallTab);
        menu_operform_opt->addAction(action_showQueueCallTab);
        menu_operform_opt->addAction(action_showUsersTab);
        menu_operform_opt->addAction(action_showHistoryTab);
        menu_operform_opt->addAction(action_showMessagesTab);

        retranslateUi(mainWindow);

        QMetaObject::connectSlotsByName(mainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *mainWindow)
    {
        mainWindow->setWindowTitle(QApplication::translate("mainWindow", "Call-o-Call Operator Agent", 0, QApplication::UnicodeUTF8));
        actionSettings->setText(QApplication::translate("mainWindow", "&\320\235\320\260\321\201\321\202\321\200\320\276\320\271\320\272\320\270", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionSettings->setToolTip(QApplication::translate("mainWindow", "\320\235\320\260\321\201\321\202\321\200\320\276\320\271\320\272\320\270 \320\277\320\276\320\264\320\272\320\273\321\216\321\207\320\265\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionAbout->setText(QApplication::translate("mainWindow", "&\320\236 \320\277\321\200\320\276\320\263\321\200\320\260\320\274\320\274\320\265", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionAbout->setToolTip(QApplication::translate("mainWindow", "\320\230\320\275\321\204\320\276\321\200\320\274\320\260\321\206\320\270\321\217 \320\276 \320\277\321\200\320\276\320\263\321\200\320\260\320\274\320\274\320\265", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionQuit->setText(QApplication::translate("mainWindow", "&\320\222\321\213\321\205\320\276\320\264", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionQuit->setToolTip(QApplication::translate("mainWindow", "\320\227\320\260\320\272\321\200\321\213\321\202\321\214 \320\277\321\200\320\270\320\273\320\276\320\266\320\265\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionDisconnect->setText(QApplication::translate("mainWindow", "&\320\236\321\202\320\272\320\273\321\216\321\207\320\270\321\202\321\214\321\201\321\217", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionDisconnect->setToolTip(QApplication::translate("mainWindow", "\320\236\321\202\320\272\320\273\321\216\321\207\320\270\321\202\321\214\321\201\321\217 \320\276\321\202 \321\201\320\265\321\200\320\262\320\265\321\200\320\260", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionOperListGroupMode->setText(QApplication::translate("mainWindow", "\320\237\320\276\320\272\320\260\320\267\321\213\320\262\320\260\321\202\321\214 \320\276\320\277\320\265\321\200\320\260\321\202\320\276\321\200\320\276\320\262 \320\277\320\276 &\320\263\321\200\321\203\320\277\320\277\320\260\320\274", 0, QApplication::UnicodeUTF8));
        actionHideOfflineContacts->setText(QApplication::translate("mainWindow", "\320\241\320\272\321\200\321\213\321\202\321\214 \320\276\321\204\321\204\320\273\320\260\320\271\320\275 \320\272\320\276\320\275\321\202\320\260\320\272\321\202\321\213", 0, QApplication::UnicodeUTF8));
        actionPrivateGroupsOnly->setText(QApplication::translate("mainWindow", "\320\237\320\276\320\272\320\260\320\267\321\213\320\262\320\260\321\202\321\214 \320\272\320\276\320\275\321\202\320\260\320\272\321\202\321\213 \320\270 \320\276\321\207\320\265\321\200\320\265\320\264\321\214 \321\202\320\276\320\273\321\214\320\272\320\276 \320\264\320\273\321\217 \321\201\320\262\320\276\320\270\321\205 \320\263\321\200\321\203\320\277\320\277", 0, QApplication::UnicodeUTF8));
        actionShowDialer->setText(QApplication::translate("mainWindow", "\320\235\320\260\320\261\321\200\320\260\321\202\321\214 \320\275\320\276\320\274\320\265\321\200", 0, QApplication::UnicodeUTF8));
        actionMakeCall->setText(QApplication::translate("mainWindow", "\320\222\321\213\320\267\320\262\320\260\321\202\321\214", 0, QApplication::UnicodeUTF8));
        actionTransferCall->setText(QApplication::translate("mainWindow", "\320\237\320\265\321\200\320\265\320\262\320\265\321\201\321\202\320\270", 0, QApplication::UnicodeUTF8));
        actionSavePreset->setText(QApplication::translate("mainWindow", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214 \320\277\321\200\320\265\321\201\320\265\321\202...", 0, QApplication::UnicodeUTF8));
        actionOperators_only_test->setText(QApplication::translate("mainWindow", "Operators only test", 0, QApplication::UnicodeUTF8));
        actionGroups_only_test->setText(QApplication::translate("mainWindow", "Groups only test", 0, QApplication::UnicodeUTF8));
        action_operform_visiable->setText(QApplication::translate("mainWindow", "\320\232\320\260\321\200\321\202\320\276\321\207\320\272\320\260 \320\276\320\277\320\265\321\200\320\260\321\202\320\276\321\200\320\260", 0, QApplication::UnicodeUTF8));
        action_callform_visiable->setText(QApplication::translate("mainWindow", "\320\232\320\260\321\200\321\202\320\276\321\207\320\272\320\260 \320\267\320\262\320\276\320\275\320\272\320\260", 0, QApplication::UnicodeUTF8));
        action_showUserCallTab->setText(QApplication::translate("mainWindow", "\320\227\320\262\320\276\320\275\320\272\320\270 \320\275\320\260 \320\276\320\277\320\265\321\200\320\260\321\202\320\276\321\200\320\265", 0, QApplication::UnicodeUTF8));
        action_showQueueCallTab->setText(QApplication::translate("mainWindow", "\320\241\320\276\321\201\321\202\320\276\321\217\320\275\320\270\320\265 \320\276\321\207\320\265\321\200\320\265\320\264\320\270", 0, QApplication::UnicodeUTF8));
        action_showUsersTab->setText(QApplication::translate("mainWindow", "\320\241\320\276\321\201\321\202\320\276\321\217\320\275\320\270\320\265 \320\276\320\277\320\265\321\200\320\260\321\202\320\276\321\200\320\276\320\262", 0, QApplication::UnicodeUTF8));
        action_showHistoryTab->setText(QApplication::translate("mainWindow", "\320\242\320\265\320\272\321\203\321\211\320\260\321\217 \321\201\321\202\320\260\321\202\320\270\321\201\321\202\320\270\320\272\320\260", 0, QApplication::UnicodeUTF8));
        action_showMessagesTab->setText(QApplication::translate("mainWindow", "\320\236\320\261\321\212\321\217\320\262\320\273\320\265\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        menuGeneral->setTitle(QApplication::translate("mainWindow", "&\320\236\320\261\321\211\320\270\320\265", 0, QApplication::UnicodeUTF8));
        menuActions->setTitle(QApplication::translate("mainWindow", "&\320\224\320\265\320\271\321\201\321\202\320\262\320\270\321\217", 0, QApplication::UnicodeUTF8));
        menuHelp->setTitle(QApplication::translate("mainWindow", "&\320\241\320\277\321\200\320\260\320\262\320\272\320\260", 0, QApplication::UnicodeUTF8));
        menuView->setTitle(QApplication::translate("mainWindow", "&\320\222\320\270\320\264", 0, QApplication::UnicodeUTF8));
        menuWindow->setTitle(QApplication::translate("mainWindow", "\320\236\320\272\320\275\320\276", 0, QApplication::UnicodeUTF8));
        menu_operform_opt->setTitle(QApplication::translate("mainWindow", "\320\235\320\260\321\201\321\202\321\200\320\276\320\271\320\272\320\260 \320\272\320\260\321\200\321\202\320\276\321\207\320\272\320\270 \320\276\320\277\320\265\321\200\320\260\321\202\320\276\321\200\320\260", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class mainWindow: public Ui_mainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
