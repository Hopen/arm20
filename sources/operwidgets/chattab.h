#ifndef CHATTAB_H
#define CHATTAB_H

#include <QWidget>
#include "operwidgets_global.h"
#include "appcore/operlistcontroller.h"
#include "appcore/spamlistcontroller.h"

using namespace ccappcore;

class HTMLColumnDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    HTMLColumnDelegate(QObject * parent = 0);

    virtual void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const;
    virtual QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const;
//    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
//                               const QModelIndex &index) const;
//    void setEditorData(QWidget *editor, const QModelIndex &index) const;
////    void setModelData(QWidget *editor, QAbstractItemModel *model,
////                      const QModelIndex &index) const;

//    void updateEditorGeometry(QWidget *editor,
//        const QStyleOptionViewItem &option, const QModelIndex &index) const;

    //virtual void resizeEvent(QResizeEvent *i_pEvent);

private:
    LateBoundObject < SpamListController > _spamController;
};

/*******************************************************
******************** Context menu **********************
*******************************************************/
class ChatTab;
class ChatContextMenu : public QMenu
{
Q_OBJECT
public:
    explicit ChatContextMenu(ChatTab* parent);

    void popup(/*ccappcore::SpamMessage**/bool);
public slots:
    void onCopy();
private:
    QPointer<QAction> _showCopyAction;
    QPointer<QMenu>   _historyMenu;
    QPointer<ChatTab> _parentTab;
};

/*******************************************************
********************* CHAT model ***********************
*******************************************************/

class ChatModel: public QAbstractListModel
{
    Q_OBJECT

    enum ColDef
    {
        OperNameColumn = 0,
        TextColumn
    };

    class MessageSortByDatePredicate
    {
    public:
        explicit MessageSortByDatePredicate()
        {

        }

        bool operator()(const SpamMessage& m1,const SpamMessage& m2)
        {
            return m1.begin()<m2.begin();
        }

    };

public:
        ChatModel(int operId, QObject *parent = 0);


        int rowCount(const QModelIndex &parent = QModelIndex()) const;
        QVariant data(const QModelIndex &index, int role) const;
        //QVariant headerData(int section, Qt::Orientation orientation,
        //                    int role = Qt::DisplayRole) const;
        int columnCount ( const QModelIndex & parent = QModelIndex() ) const;
        //void sort ( int column, Qt::SortOrder order = Qt::AscendingOrder );
        //Qt::ItemFlags flags(const QModelIndex &index) const;
private:
        QString columnNameById(const qint32& id)const;
        //QString getContactName(const int &contactId)const;
        QColor statusColor(int status) const;


signals:
public slots:
        void reset();
        //void sort();
private:
        QList<int> _columnsList;

        LateBoundObject < OperListController > _operListController;
        LateBoundObject < SpamListController > _spamController;
        //LateBoundObject < CallsController    > _callsController;

        SpamContainer _messages;

        int _operId;
        //int _sessionOwner;
};


class ChatTab : public QWidget
{
    Q_OBJECT
public:
    explicit ChatTab(Contact c, QWidget *parent = 0);

    const Contact& contact() const { return _contact; }

    void setContact(Contact c);
    QString getAssignedMessageText()const;
    bool canGetHistory()const;

private:
    //void CreateShortcut( QPointer<QShortcut> & pShortcut, const char * sName, const char * sShortcut, Qt::ShortcutContext Context = Qt::WindowShortcut);
signals:
    void titleStatusChange(int status);
    void resize();
    void readyToQuit();
    void readyToStay();
public slots:
    void on_inputTextChanged( );
    virtual void on_btnSendTriggered();
    //void on_btnEnterTriggered();

    virtual void updateControls();
    void scrollToBottom();
    void onCustomContextMenuRequested();
    void onLoadHistory();
protected:
    virtual bool event(QEvent *e);

protected:

    QPointer<  ChatModel   > _mainModel;
    QPointer<  QTreeView   > _mainView;


    //QPointer< QTextBrowser > _output;
    QPointer< QTextEdit    > _input;
    QPointer< QPushButton  > _btnSend;
    QPointer< QToolButton  > _btnClose;
    //QPointer< QPushButton  > _btnMenu;
    QPointer< QAction      > _actEnter;
    QPointer< QAction      > _actCtrlEnter;

    QPointer< QShortcut    > _pShortcut1;
    QPointer< QShortcut    > _pShortcut2;

    LateBoundObject< SpamListController > _spamController;
    LateBoundObject< OperListController > _operListController;
    LateBoundObject< CallsController    > _callsController;

    Contact _contact;
    //QPointer<QMenu>     _contextMenu;
    QPointer<ChatContextMenu> _contextMenu;
    ccappcore::SpamMessage _assignedMessage;

    //QMenu*          _menu;
};

//class ChatWidget: public ChatTab
//{
//    Q_OBJECT
//public:
//    explicit ChatWidget(QWidget *parent = 0);
////private:
////    LateBoundObject<SpamListController> _spamController;

//public slots:
//    virtual void on_btnSendTriggered();
//    virtual void updateControls();
//};

#endif // CHATTAB_H
