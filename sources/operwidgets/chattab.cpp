#include "chattab.h"
#include "appcore/callscontroller.h"
#include "appcore/useritemdatarole.h"

const quint8 MAX_MESSAGES_BY_PAGE = 20;

/*******************************************************
********************* HTML deligate ********************
*******************************************************/

const QString s_messageTemplateIn (QObject::tr("<font size=\"4\" color=\"red\"  face=\"Arial\">%1 (%2): </font> "
                                               "<font size=\"4\" color=\"black\" face=\"Arial\">%3</font><p>"));
const QString s_messageTemplateOut(QObject::tr("<font size=\"4\" color=\"blue\" face=\"Arial\">%1 (%2): </font> "
                                               "<font size=\"4\" color=\"black\" face=\"Arial\">%3</font><p>"));


HTMLColumnDelegate::HTMLColumnDelegate(QObject *parent): QItemDelegate(parent)
{
}


void HTMLColumnDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QVariant varValue = index.data(ccappcore::AssociatedObjectRole);
    if(!varValue.isValid() || !varValue.canConvert<SpamMessage>())
    {
        QItemDelegate::paint(painter,option,index);
        return;
    }

    SpamMessage m = varValue.value<SpamMessage>();


    QStyleOptionViewItemV4 opt(option);
    opt.showDecorationSelected = true;
    opt.rect.adjust(0, 0, 0, -1);

    painter->save();
    QStyle *style = opt.widget ? opt.widget->style() : QApplication::style();
    style->drawPrimitive(QStyle::PE_PanelItemViewItem, &opt, painter, opt.widget);

    QRect textRect = opt.rect;
//    QPalette::ColorGroup cg = option.state & QStyle::State_Enabled
//                              ? QPalette::Normal : QPalette::Disabled;
//    if (cg == QPalette::Normal && !(option.state & QStyle::State_Active))
//            cg = QPalette::Inactive;
//    if (option.state & QStyle::State_Selected) {
//            painter->setPen(option.palette.color(cg, QPalette::HighlightedText));
//    }
//    else {
//            painter->setPen(option.palette.color(cg, QPalette::Text));
//    }



//    painter->drawText(textRect, m.text(), Qt::AlignLeft | Qt::AlignVCenter);
//    //drawFocus(painter, option, option.rect); // not opt, because drawFocus() expects normal rect

    //QFont font("Times", 12, -1, true);

    QString oper_name(tr("%1 (%2):"));
    oper_name = oper_name.arg(_spamController->getContactName(m.operFromId()))
              .arg(m.begin().toString( "HH:mm:ss dd/MM/yy"));

    QStringList strokes = m.text().split("\n");
    strokes.push_front(oper_name);
    QFont font = opt.font;
    QColor textColor = Qt::blue;
    if (m.msgType() == MT_SYSTEM)
    {
        textColor = QColor(_spamController->statusColor(m.isRead()));
    }
    else
    {
        if (m.msgType() == MT_IN)
        {
            textColor = Qt::red;
        }
    }
    painter->setPen(QPen(textColor));

    qreal vertPos = /*10*/0;
    int indent = /*20*/0;

    foreach(QString stroke,strokes)
    {
        QSharedPointer <QTextLayout> textLayout(new QTextLayout(stroke, font));
        QTextLine line;
        textLayout->beginLayout();
        line = textLayout->createLine();
        while (line.isValid())
        {
            line.setLineWidth(textRect.width());
            line.setPosition(QPointF(indent, vertPos));
            vertPos += line.height();
            line = textLayout->createLine();
        }
        textLayout->endLayout();

        textLayout->draw(painter, QPoint(textRect.x(),textRect.y()));
        //textRect.adjust(0,vertPos,0,0);
    }

//    QSharedPointer <QTextLayout> textLayout(new QTextLayout(m.text(), font));
//    QTextLine line;
//    qreal vertPos = /*10*/0;
//    int indent = /*20*/0;
//    textLayout->beginLayout();
//    line = textLayout->createLine();
//    while (line.isValid())
//    {
//        line.setLineWidth(textRect.width()/* - 2 * indent*/);
//        line.setPosition(QPointF(indent, vertPos));
//        vertPos += line.height();
//        //indent += 20;
//        line = textLayout->createLine();
//    }
//    textLayout->endLayout();

//    textLayout->draw(painter, QPoint(textRect.x(),textRect.y()));

    painter->restore();
}

QSize HTMLColumnDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QVariant varValue = index.data(ccappcore::AssociatedObjectRole);
    if(!varValue.isValid() || !varValue.canConvert<SpamMessage>())
    {
        return QItemDelegate::sizeHint(option, index);;
    }

    SpamMessage m = varValue.value<SpamMessage>();

//    QRect screenRect;
//    QWidget *my_parent = qobject_cast<QWidget*>(parent());
//    if (my_parent)
//    {
//        screenRect = my_parent->rect();
//        screenRect.adjust(0, 0, -4, -1);

//        QStyleOptionViewItemV4 vo = option;
//        QStyle* style = vo.widget->style()?vo.widget->style():QApplication::style();

//        int width  = screenRect.width();// + vo.decorationSize.width  ();
//        int height = 0;
//        QStringList strokes = m.text().split("\n");
//        strokes.push_front(tr("oper name"));
//        foreach(QString stroke,strokes)
//        {
//            if (stroke.isEmpty())
//            {
//                stroke = tr("string to calculate text heigth");
//            }
//            QRect textRect = style->itemTextRect(vo.fontMetrics, vo.rect, 0, true, stroke);
//            height += (textRect.width() < screenRect.width())?textRect.height():textRect.height() * (textRect.width()/screenRect.width() + 1);
//        }

//        return QSize(width, height);

//    }

    QStyleOptionViewItemV4 opt(option);

    QWidget *my_parent = qobject_cast<QWidget*>(parent());
    if (my_parent)
    {
        QRect screenRect = my_parent->rect();;
        screenRect.adjust(0,0,-5,0);
        QStringList strokes = m.text().split("\n");
        strokes.push_front("oper_name");
        QFont font = opt.font;

        int width  = screenRect.width();
        int height = 0;

        foreach(QString stroke,strokes)
        {
            QSharedPointer <QTextLayout> textLayout(new QTextLayout(stroke, font));
            QTextLine line;
            textLayout->beginLayout();
            line = textLayout->createLine();
            while (line.isValid())
            {
                line.setLineWidth(screenRect.width());
                line.setPosition(QPointF(0, height));
                height += line.height();
                line = textLayout->createLine();
            }
            textLayout->endLayout();
        }

        return QSize(width, height);
    }

    return QItemDelegate::sizeHint(option, index);





    //return rect.size();
//    int width  = rect.width  ()+vo.decorationSize.width  ();
//    int height = rect.height ()+vo.decorationSize.height ();
//    height = width > 100? height*2:height;
//    //width = width > 80 ? width : 80;
//    return QSize(width, height);
//    //return QItemDelegate::sizeHint(option, index);
}



//QWidget *HTMLColumnDelegate::createEditor(QWidget *parent,
//     const QStyleOptionViewItem &/* option */,
//     const QModelIndex &/* index */) const
// {
//     QTextBrowser *browser = new QTextBrowser(parent);

//     return browser;
// }

//void HTMLColumnDelegate::setEditorData(QWidget *editor,
//                                     const QModelIndex &index) const
//{
//    SpamMessage m = index.data(ccappcore::AssociatedObjectRole).value<SpamMessage>();

//    QTextBrowser *browser = static_cast<QTextBrowser*>(editor);

//    QString htmlDoc = (m.msgType()==MT_IN)?s_messageTemplateIn:s_messageTemplateOut;
//    htmlDoc = htmlDoc.arg(_spamController->getContactName(m.operFromId()))
//                             .arg(m.begin().toString( "HH:mm:ss dd/MM/yy"))
//                             .arg(m.text());
//    browser->setHtml(htmlDoc);
//    browser->moveCursor(QTextCursor::End);

//}

//void HTMLColumnDelegate::updateEditorGeometry(QWidget *editor,
//    const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
//{
//    editor->setGeometry(option.rect);
//}

/*******************************************************
******************** Context menu **********************
*******************************************************/

ChatContextMenu::ChatContextMenu(ChatTab* parent):QMenu(parent),_parentTab(parent)
{
    _showCopyAction = addAction(tr("Копировать"));
    connect(_showCopyAction, SIGNAL(triggered()), this, SLOT(onCopy()));

    addSeparator();
    /*QMenu* historyMenu*/ _historyMenu= addMenu(tr("История сообщений"));
    QDateTime cur_date = QDateTime::currentDateTime();


    QAction* period = _historyMenu->addAction(tr("Вчера"));
    period->setData(cur_date.addDays(-1));
    connect(period, SIGNAL(triggered()), _parentTab.data(), SLOT(onLoadHistory()));
    period = _historyMenu->addAction(tr("7 дней"));
    period->setData(cur_date.addDays(-7));
    connect(period, SIGNAL(triggered()), _parentTab.data(), SLOT(onLoadHistory()));
    period = _historyMenu->addAction(tr("3 месяца"));
    period->setData(cur_date.addDays(-91));
    connect(period, SIGNAL(triggered()), _parentTab.data(), SLOT(onLoadHistory()));
    period = _historyMenu->addAction(tr("Полгода"));
    period->setData(cur_date.addDays(-182));
    connect(period, SIGNAL(triggered()), _parentTab.data(), SLOT(onLoadHistory()));
    period= _historyMenu->addAction(tr("Год"));
    period->setData(cur_date.addDays(-365));
    connect(period, SIGNAL(triggered()), _parentTab.data(), SLOT(onLoadHistory()));
    //historyMenu->addAction(tr("Всё"));

}

void ChatContextMenu::popup(bool message)
{
    _showCopyAction->setEnabled(!!message);
    _historyMenu   ->setEnabled(_parentTab->canGetHistory());
    QMenu::popup(QCursor::pos());
}

void ChatContextMenu::onCopy()
{
    if (!_parentTab)
        return;
    QClipboard *clipboard = QApplication::clipboard();
    QString originalText = _parentTab->getAssignedMessageText();
    clipboard->setText(originalText);
}

/*******************************************************
********************* CHAT model ***********************
*******************************************************/

ChatModel::ChatModel(int operId, QObject *parent)
    :QAbstractListModel(parent),
      _operId(operId)
{
    _columnsList //<< (OperNameColumn)
                 << (TextColumn);

    //_sessionOwner = _callsController->sessionOwner().contactId().toInt();

    connect(_spamController.instance(),
            SIGNAL(chatMsgAdded(SpamMessage)),
            this,
            SLOT(reset()));

    connect(_spamController.instance(),
            SIGNAL(loadHistoryCompleted()),
            this,
            SLOT(reset()));

    reset();
}

QString ChatModel::columnNameById(const qint32 &id) const
{
//    switch(id)
//    {
//    case Contact::Name:
//        return tr("Имя");
//    case Contact::Status:
//        return tr("Текущий статус");
//    case Contact::Phone:
//        return tr("Номер телефона");
//    case Contact::StatusReason:
//        return tr("Последнее действие");
//    case Contact::StatusTime:
//        return tr("Время последнего действия");

//    case column_messagesCount:
//        return tr("Сообщения");

//    default:
//        return tr("Неизвестно");
//    }

    return QString();
}

int ChatModel::rowCount(const QModelIndex &/*parent*/) const
{
    return _messages.size();
}

int ChatModel::columnCount(const QModelIndex &/*parent*/) const
{
    return _columnsList.size();
}

QVariant ChatModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (index.row() >= _messages.size())
        return QVariant();

    if(index.column() <0 || index.column() >= _columnsList.count() )
        return QVariant();

    SpamMessage m = _messages[index.row()];
    if(!m.isValid())
        return QVariant();

//    if (role == Qt::DisplayRole)
//    {
//        switch(index.column())
//        {
//        case OperNameColumn:
//        {
//            QString OperName = _spamController->getContactName(m.operFromId()) + tr(" (") + m.begin().toString( "HH:mm:ss dd/MM/yy") + tr(") ");
//            //return _spamController->getContactName(m.operFromId());
//            return OperName;
//        }

//        case TextColumn:
//            return m.text();
//        };

//    }

    if(role == Qt::DecorationRole)
    {
    }

    if(role == Qt::TextColorRole)
    {
        return statusColor(m.msgType());
    }

//    if(role == Qt::FontRole)
//    {
//        QFont font;
//        font.setFamily("Arial");
//        font.setWeight(4);
//        return font;
//    }

    if(role == ccappcore::AssociatedObjectRole)
        return QVariant::fromValue(m);


    return QVariant();
}



//QVariant ContactsModel::headerData(int section, Qt::Orientation orientation,
//                                         int role) const
//{
//       if (role != Qt::DisplayRole)
//            return QVariant();

//        if (orientation == Qt::Horizontal)
//            return columnNameById(_columnsList.value(section));
//        else
//            return QString("Row %1").arg(section);
//}

//void ContactsModel::applyFilter(const PredicateGroup<Contact> &filter)
//{
//    emit layoutAboutToBeChanged();

//    _filter = filter;

//    _contacts.clear();
//    _contacts =  _controller->findOpers (_filter);
//    _contacts+=  _controller->findGroups(_filter);
//    //qSort(_contacts);

//    emit layoutChanged();
//}

void ChatModel::reset()
{
    _messages.clear();

    _messages =  _spamController->messageChatIn  ( _operId );
    _messages += _spamController->messageChatOut ( _operId /*_sessionOwner*/);

    qSort(_messages.begin(),_messages.end(),MessageSortByDatePredicate());

    emit layoutChanged();
}

//void ChatModel::sort()
//{

//    emit layoutChanged();
//}

//void ChatModel::sort(int column, Qt::SortOrder order)
//{
//    _saveSortColumn = column;
//    _saveSortOrder = order;
//    emit layoutAboutToBeChanged();
//    switch(_columnsList[column])
//    {
//    case Contact::Name:
//    {
//        qSort(_contacts.begin(),_contacts.end(),ContactSortByNamePredicate(order));
//        break;
//    }
//    case Contact::Status:
//    {
//        qSort(_contacts.begin(),_contacts.end(),ContactSortByStatusPredicate(order));
//        break;
//    }
//    case Contact::Phone:
//    {
//        qSort(_contacts.begin(),_contacts.end(),ContactSortByPhonePredicate(order));
//        break;
//    }
//    default:
//        qSort(_contacts);
//    };

////    {
////        qSort(_contacts.begin(),_contacts.end(),ContactSortByNamePredicate(&_contacts,order));
////    }
////    else
////    {
////        qSort(_contacts);
////    }
//    emit layoutChanged();
//}

//QString ChatModel::getContactName(const int &contactId) const
//{
//    ContactId personId = _operListController->createOperId(contactId);
//    Contact   person     = _operListController->findById(personId);
//    return person.name();
//}

QColor ChatModel::statusColor(int status) const
{
    switch(status)
    {
        case MT_IN:
            return QColor("blue");
        case MT_OUT:
            return QColor("red");
        case MT_SYSTEM:
            return QColor("black");
        case MT_CHAT:
            return QColor("green");
        default:
            return QColor();
    }

}

//Qt::ItemFlags ChatModel::flags(const QModelIndex &index) const
//{

//    if (!index.isValid())
//         return Qt::ItemIsEnabled;

//    if (index.column() == 1)
//        return QAbstractListModel::flags(index) | Qt::ItemIsEditable;

//    return QAbstractListModel::flags(index);
//}

/*******************************************************
********************* CHAT view ************************
*******************************************************/


ChatTab::ChatTab(Contact c, QWidget *parent) :
    QWidget(parent)
{

    _contextMenu = new ChatContextMenu(this);

    setContact(c);

    _mainModel = new ChatModel(c.contactId().toInt(), this);
    _mainView = new QTreeView(this);

    _mainView->setRootIsDecorated(false);
    _mainView->setModel(_mainModel);
    //_mainView->setSelectionBehavior(QAbstractItemView::SelectRows);
    _mainView->setItemDelegateForColumn(0, new HTMLColumnDelegate(_mainView));
    connect(this,SIGNAL(resize()),_mainView.data(),SLOT(doItemsLayout()));

    //_mainView->setItemDelegate(new HTMLColumnDelegate(_mainView));
    _mainView->header()->setHidden(true);
    QFont font = _mainView->font();
    font.setFamily("Arial");
    font.setWeight(4);
    _mainView->setFont(font);

    _mainView->setContextMenuPolicy(Qt::CustomContextMenu);



    //_output = new QTextBrowser (this);

    _input  = new QTextEdit(this);
    //_input->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    _btnSend = new QPushButton(tr("Отправить"), this);
    _btnSend->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
    _btnSend->setEnabled(false);

    _btnClose = new QToolButton(this);
    _btnClose->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    _btnClose->setPopupMode(QToolButton::MenuButtonPopup);
    //_btnClose->setMinimumWidth(100);
    //_btnClose->setAutoRaise(true);
    _btnClose->setText(tr("Закрыть"));
    //_btnClose->setIcon(action->icon());


//    _btnMenu = new QPushButton(this);
//    QRect rc = _btnMenu->geometry();
//    rc.setWidth(10);
//    _btnMenu->setGeometry(rc);

//    QHBoxLayout* buttonsLayout2 = new QHBoxLayout();
//    buttonsLayout2->addWidget(_btnClose,1);
//    buttonsLayout2->addWidget(_btnMenu,0);
//    buttonsLayout2->setMargin(0);

    //QWidget* spacer = new QWidget();
    //spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    // toolBar is a pointer to an existing toolbar

    QVBoxLayout* buttonsLayout = new QVBoxLayout();
    buttonsLayout->addWidget(_btnSend);
    buttonsLayout->addWidget(_btnClose);
    //buttonsLayout->addLayout(buttonsLayout2);

    QHBoxLayout* bottomLayout = new QHBoxLayout();
    bottomLayout->addWidget(_input,1);
    //bottomLayout->addWidget(_btnSend,0);
    bottomLayout->addLayout(buttonsLayout,0);
    bottomLayout->setMargin(0);
    //bottomLayout->addWidget(_btnEnter);
    //bottomLayout->addWidget(spacer);

    QWidget * bottomWidget = new QWidget(this);
    bottomWidget->setLayout(bottomLayout);

    QSplitter *vSplitter = new QSplitter;
    vSplitter->setOrientation(Qt::Vertical);
    vSplitter->addWidget(/*_output*/_mainView);
    vSplitter->addWidget(bottomWidget);
    vSplitter->setChildrenCollapsible(false);
    vSplitter->setStretchFactor(vSplitter->indexOf(/*_output*/_mainView), 1);
    vSplitter->setStretchFactor(vSplitter->indexOf(bottomWidget ), 0);


    QVBoxLayout * mainLayout = new QVBoxLayout();
//    mainLayout->addWidget(_output);
//    mainLayout->addWidget(_input);
    mainLayout->addWidget(vSplitter);
    //mainLayout->addLayout(bottomLayout);
    setLayout(mainLayout);

    connect(_mainView.data(),SIGNAL(customContextMenuRequested(QPoint)),
            this, SLOT(onCustomContextMenuRequested()));

    connect(_input.data(), SIGNAL(textChanged()), this, SLOT(on_inputTextChanged()));
    connect(_btnSend.data(),SIGNAL(pressed()),this,SLOT(on_btnSendTriggered()));
    //connect(_btnEnter.data(),SIGNAL(pressed()),this,SLOT(on_btnEnterTriggered()));
    connect(_btnClose.data(),SIGNAL(pressed()),this,SIGNAL(readyToQuit()));
    connect(_operListController.instance(), SIGNAL(contactStatusChanged(ccappcore::Contact)),
            this, SLOT(updateControls()));
//    connect(_spamController.instance(),SIGNAL(chatMsgAdded(SpamMessage)),
//            this,SLOT(scrollToBottom()));
    connect(_mainModel.data(), SIGNAL(layoutChanged()),
            this, SLOT(scrollToBottom()));

    QAction* act = new QAction("test", this);
    act->setShortcuts(QList<QKeySequence>()
                                   << QKeySequence(QLatin1String("Ctrl+Return"))
                                   << QKeySequence(QLatin1String("Ctrl+Enter")));

    act->setShortcutContext(Qt::WidgetWithChildrenShortcut);
    connect(act, SIGNAL(triggered()), this, SLOT(on_btnSendTriggered()));
    addAction(act);
//    CreateShortcut(_pShortcut1, "Shortcut 1", "Enter");
//    CreateShortcut(_pShortcut2, "Shortcut 2", "Ctrl+Enter");
//    connect(_pShortcut2.data(), SIGNAL(activated()), this, SLOT(on_btnSendTriggered()));

    QMenu *menu = new QMenu(this);
    QAction * closeCurrent = menu->addAction(tr("Закрыть текущее"));
    QAction * closeOthers  = menu->addAction(tr("Закрыть все, кроме текущего"));
    closeOthers->setEnabled(false);

    connect(closeCurrent, SIGNAL(triggered()), this, SIGNAL(readyToQuit()));
    connect(closeOthers , SIGNAL(triggered()), this, SIGNAL(readyToStay()));

    _btnClose->setMenu(menu);
    //_btnMenu->setMenu(menu);
}

void ChatTab::setContact(Contact c)
{
    if (!c.isValid())
        return;

    _contact = c;
    updateControls();
}

void ChatTab::on_inputTextChanged()
{
    _btnSend->setEnabled(!_input->toPlainText().isEmpty());
}

//void ChatTab::on_btnEnterTriggered()
//{
//    updateControls();
//}

void ChatTab::on_btnSendTriggered()
{
    if ((_contact.contactId().type() == CT_Roster) /*&&
        (_contact.data(Contact::BareJid).isValid())*/         )
    {
        if (_contact.data(Contact::BareJid).isValid())
        {
            QString jid = _contact.data(Contact::BareJid).toString();
            _spamController->rosterOUT(_contact.contactId().toInt(),jid,_input->toPlainText());
        }
    }
    else
    {
        _spamController->sendMsg(_contact.contactId().toInt(),_input->toPlainText());
    }
    _input->clear();
}

void ChatTab::updateControls()
{
    emit titleStatusChange(_contact.status());
//    QString htmlDoc = _spamController->printMessages(_contact.contactId().toInt(),MAX_MESSAGES_BY_PAGE);
//    _output->setHtml(htmlDoc);
//    _output->moveCursor(QTextCursor::End);
}

void ChatTab::scrollToBottom()
{
    if (_mainModel->rowCount())
    {
        QModelIndex currentIndex = _mainModel->index(_mainModel->rowCount()-1);
        _mainView->scrollTo(currentIndex);
    }
}

//void ChatTab::CreateShortcut(QPointer<QShortcut>&pShortcut, const char *sName, const char *sShortcut, Qt::ShortcutContext Context)
//{
//    pShortcut = new QShortcut(QKeySequence(QLatin1String(sShortcut)), this);
//    pShortcut->setWhatsThis(tr(sName));
//    pShortcut->setContext(Context);
//    //connect(pShortcut, SIGNAL(activated()), this, sHandler);
//}

bool ChatTab::event(QEvent *evt)
{
    QEvent::Type type = evt->type();
//    if ((type != QEvent::Paint) &&
//        (type != QEvent::Enter) &&
//            (type != QEvent::Leave) &&
//            (type != QEvent::MouseMove) &&
//            (type != QEvent::MouseButtonPress) &&
//            (type != QEvent::MouseButtonRelease) &&
//            (type != QEvent::WindowActivate) &&
//            (type != QEvent::ActivationChange) &&
//            (type != QEvent::UpdateRequest) &&
//            (type != QEvent::WindowDeactivate)
//            )
//    {
//        int test = 0;
//    }

     if (type == QEvent::Resize)
     {
         emit resize();
     }
    //QItemDelegate::changeEvent(evt);
    return QWidget::event(evt);
}

void ChatTab::onCustomContextMenuRequested()
{
    QAbstractItemView* view = qobject_cast<QAbstractItemView*>(sender());
    if(!view || !view->selectionModel())
        return;

    QModelIndex current = view->selectionModel()->currentIndex();
    QVariant varValue = current.data(ccappcore::AssociatedObjectRole);
    if(!varValue.isValid() || !varValue.canConvert<SpamMessage>())
    {
        _assignedMessage = SpamMessage();
    }
    else
    {
        _assignedMessage = varValue.value<SpamMessage>();
    }

    //SpamMessage m = varValue.value<SpamMessage>();
//    QSharedPointer<QMenu> historyMenu;
//    historyMenu->addAction(tr("Вчера"));
//    historyMenu->addAction(tr("7 дней"));
//    historyMenu->addAction(tr("3 месяца"));
//    historyMenu->addAction(tr("Полгода"));
//    historyMenu->addAction(tr("Год"));
//    historyMenu->addAction(tr("Всё"));


    //QSharedPointer<QMenu> contextMenu(new QMenu());


    _contextMenu->popup(_assignedMessage.isValid());

}

QString ChatTab::getAssignedMessageText()const
{
    return SpamMessage::shortInfo(_assignedMessage);
}

void ChatTab::onLoadHistory()
{
    QAction* menuPeriod = qobject_cast<QAction*>(sender());
    if (!menuPeriod)
        return;

    QDateTime date = menuPeriod->data().toDateTime();

    QDateTime to(QDateTime::currentDateTime());
    QDateTime from = date;//to;
    //QTime curtime(0,0);
    //from.setTime(curtime);
    int who = _contact.contactId().toInt();//_callsController->sessionOwner().contactId().toInt();
    _spamController->loadHistory(who,from,to);
}

bool ChatTab::canGetHistory()const
{
    if(!_contact.isValid())
        return false;

    if (_contact.contactId().type() == CT_Roster)
        return false;

    return true;
}

/***************************************************************
******************* SYSTEM CHAT ********************************
***************************************************************/

//ChatWidget::ChatWidget(QWidget *parent):ChatTab(parent)
//{
////    connect(_spamController.instance(),SIGNAL(chatMsgAdded2()),
////            this,SLOT(updateControls()));

//}

//void ChatWidget::on_btnSendTriggered()
//{
//    _spamController->sendMsg2(ccappcore::TO_ALL,_input->toPlainText());
//    _input->clear();
//}

//void ChatWidget::updateControls()
//{
////    QString htmlDoc = _spamController->printChat();
////    _output->setHtml(htmlDoc);
////    _output->moveCursor(QTextCursor::End);

//}
