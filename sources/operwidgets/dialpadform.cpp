#include "dialpadform.h"

#include "ui_dialpadform.h"
#include "appcore/styleloader.h"
#include "appcore/callslistmodel.h"

DialpadForm::DialpadForm(QWidget *parent)
    : QWidget(parent)
    , _ui(new Ui::DialpadForm)
{
    _ui->setupUi(this);
    _ui->phone->setHintText(tr("Телефон"));

    connect(_ui->bn0, SIGNAL(clicked()), this, SLOT(dialpadButton_clicked()));
    connect(_ui->bn1, SIGNAL(clicked()), this, SLOT(dialpadButton_clicked()));
    connect(_ui->bn2, SIGNAL(clicked()), this, SLOT(dialpadButton_clicked()));
    connect(_ui->bn3, SIGNAL(clicked()), this, SLOT(dialpadButton_clicked()));
    connect(_ui->bn4, SIGNAL(clicked()), this, SLOT(dialpadButton_clicked()));
    connect(_ui->bn5, SIGNAL(clicked()), this, SLOT(dialpadButton_clicked()));
    connect(_ui->bn6, SIGNAL(clicked()), this, SLOT(dialpadButton_clicked()));
    connect(_ui->bn7, SIGNAL(clicked()), this, SLOT(dialpadButton_clicked()));
    connect(_ui->bn8, SIGNAL(clicked()), this, SLOT(dialpadButton_clicked()));
    connect(_ui->bn9, SIGNAL(clicked()), this, SLOT(dialpadButton_clicked()));
    connect(_ui->bnStar, SIGNAL(clicked()), this, SLOT(dialpadButton_clicked()));
    connect(_ui->bnPound, SIGNAL(clicked()), this, SLOT(dialpadButton_clicked()));

    connect(_ui->phone, SIGNAL(returnPressed()), _ui->bnMakeCall, SLOT(animateClick()));

    LateBoundObject<StyleLoader> styleLoader;
    if(styleLoader.isValid())
        styleLoader->load(this);

    connect(_callsController.instance(), SIGNAL(lastCallChanged(ccappcore::Call)),
            this, SLOT(updateButtonsState()));

    connect(_appContext.instance(), SIGNAL(callToTransferChanged(ccappcore::Call)),
            this, SLOT(callToTransferChanged(ccappcore::Call)));

    updateButtonsState();
}

DialpadForm::~DialpadForm()
{
    delete _ui;
}

void DialpadForm::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        _ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void DialpadForm::resizeEvent(QResizeEvent *e)
{
    QWidget::resizeEvent(e);
    //bool small = _ui->bnMakeCall->size().height()<_ui->bnMakeCall->baseSize().height();
//    _ui->bnMakeCall->setIconSize(small? QSize(16,16) : QSize(24,24));
}

void DialpadForm::showEvent(QShowEvent *e)
{
    QWidget::showEvent(e);
    _ui->phone->setFocus();
    _dialPause->setPause();
}

void DialpadForm::dialpadButton_clicked()
{
    QAbstractButton* bn = qobject_cast<QAbstractButton*>(sender());

    _dialPause->setPause();
    QString text = _ui->phone->isHintVisible()
                   ? QString() : _ui->phone->text();
    _ui->phone->setText(text+bn->text());;
    _soundManager->playDtmf(bn->text());
}

QString DialpadForm::phone() const
{
    return _ui->phone->text();
}

void DialpadForm::setPhone(QString phoneNumber) const
{
    _ui->phone->setText(phoneNumber);
}

void DialpadForm::callToTransferChanged(ccappcore::Call callToTransfer)
{
    QAction* action = callToTransfer.isValid() ? _ui->actionTransferCall : _ui->actionMakeCall;
    _ui->bnMakeCall->setText(action->text());
    _ui->bnMakeCall->setIcon(action->icon());
}

void DialpadForm::on_bnMakeCall_clicked()
{
    Call callToTransfer = _appContext->callToTransfer();
    if(callToTransfer.isValid())
        _callsController->transferToPhone(callToTransfer, phone());
    else
        _callsController->makeCall(phone());

    onPhoneActionTriggered();
}

void DialpadForm::updateButtonsState()
{
    Call lastCall = _callsController->lastCall();
    bool phoneIsEmpty = _ui->phone->text().length()==0 || _ui->phone->isHintVisible();
    bool canMakeCall = !phoneIsEmpty;

    _ui->bnMakeCall->setEnabled(canMakeCall);
}

void DialpadForm::on_phone_textChanged(QString )
{    
    updateButtonsState();
    if(!_ui->phone->isHintVisible())
        _dialPause->setPause();
}

void DialpadForm::hideIfUndocked()
{
    QDockWidget* dockPanel = qobject_cast<QDockWidget*>(parent());
    if(!dockPanel)
        hide();
    else if(dockPanel && dockPanel->isFloating())
        dockPanel->hide();
}

void DialpadForm::setFocusOnPhone()
{
    _ui->phone->setFocus(Qt::OtherFocusReason);
}

void DialpadForm::onPhoneActionTriggered()
{
    _appContext->clearCallToTransfer();
    _ui->phone->showHint();
    hideIfUndocked();
    _dialPause->clearPause();

    //add phone to drop down
    //int phoneIndex = _ui->phone->findText( phone );
    //if( !phone.isEmpty() && phoneIndex < 0 )
    //    _ui->phone->addItem( phone );
}

//void DialpadForm::on_bnPause_clicked()
//{
//}

//void DialpadForm::on_bnShowCallForm_clicked()
//{
//    Call c = _callsController->lastCall();
//    _appContext->showCallForm(c);
//}

//void DialpadForm::on_bnEndCall_clicked()
//{
//    Call c = _callsController->lastCall();
//    _callsController->endCall( c );
//}

//void DialpadForm::on_bnPause_clicked(bool checked)
//{
//    Call c = _callsController->lastCall();
//    _callsController->holdCall(c, checked );
//}
