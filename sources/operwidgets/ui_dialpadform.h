/********************************************************************************
** Form generated from reading UI file 'dialpadform.ui'
**
** Created: Tue 26. Mar 13:54:37 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALPADFORM_H
#define UI_DIALPADFORM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QToolButton>
#include <QtGui/QWidget>
#include "lineeditwithhint.h"

QT_BEGIN_NAMESPACE

class Ui_DialpadForm
{
public:
    QAction *actionTransferCall;
    QAction *actionMakeCall;
    QGridLayout *gridLayout;
    QWidget *widget;
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout;
    QToolButton *bnHistory;
    LineEditWithHint *phone;
    QToolButton *bnMakeCall;
    QPushButton *bn1;
    QPushButton *bnPound;
    QPushButton *bn0;
    QPushButton *bn2;
    QPushButton *bn3;
    QPushButton *bn4;
    QPushButton *bn5;
    QPushButton *bn6;
    QPushButton *bn7;
    QPushButton *bn8;
    QPushButton *bn9;
    QPushButton *bnStar;

    void setupUi(QWidget *DialpadForm)
    {
        if (DialpadForm->objectName().isEmpty())
            DialpadForm->setObjectName(QString::fromUtf8("DialpadForm"));
        DialpadForm->resize(297, 275);
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(DialpadForm->sizePolicy().hasHeightForWidth());
        DialpadForm->setSizePolicy(sizePolicy);
        DialpadForm->setMinimumSize(QSize(180, 140));
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        palette.setBrush(QPalette::Active, QPalette::Window, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush);
        DialpadForm->setPalette(palette);
        DialpadForm->setAutoFillBackground(true);
        DialpadForm->setStyleSheet(QString::fromUtf8(""));
        actionTransferCall = new QAction(DialpadForm);
        actionTransferCall->setObjectName(QString::fromUtf8("actionTransferCall"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/resources/icons/call_transfer_32x32.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionTransferCall->setIcon(icon);
        actionMakeCall = new QAction(DialpadForm);
        actionMakeCall->setObjectName(QString::fromUtf8("actionMakeCall"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/resources/icons/call_start_32x32.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionMakeCall->setIcon(icon1);
        gridLayout = new QGridLayout(DialpadForm);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(3, 3, 3, 3);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        widget = new QWidget(DialpadForm);
        widget->setObjectName(QString::fromUtf8("widget"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy1);
        widget->setMaximumSize(QSize(220, 180));
        widget->setAutoFillBackground(false);
        widget->setStyleSheet(QString::fromUtf8("#widget {\n"
" border: 1px solid gray;\n"
"	background-color: rgb(232, 232, 232);\n"
"}"));
        gridLayout_2 = new QGridLayout(widget);
        gridLayout_2->setSpacing(3);
        gridLayout_2->setContentsMargins(6, 6, 6, 6);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(6, 3, 0, 3);
        bnHistory = new QToolButton(widget);
        bnHistory->setObjectName(QString::fromUtf8("bnHistory"));
        bnHistory->setEnabled(false);
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::MinimumExpanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(bnHistory->sizePolicy().hasHeightForWidth());
        bnHistory->setSizePolicy(sizePolicy2);
        bnHistory->setMaximumSize(QSize(16, 16777215));
        bnHistory->setAutoRaise(true);
        bnHistory->setArrowType(Qt::DownArrow);

        horizontalLayout->addWidget(bnHistory);

        phone = new LineEditWithHint(widget);
        phone->setObjectName(QString::fromUtf8("phone"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(phone->sizePolicy().hasHeightForWidth());
        phone->setSizePolicy(sizePolicy3);
        QFont font;
        font.setPointSize(12);
        phone->setFont(font);

        horizontalLayout->addWidget(phone);

        bnMakeCall = new QToolButton(widget);
        bnMakeCall->setObjectName(QString::fromUtf8("bnMakeCall"));
        QSizePolicy sizePolicy4(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(bnMakeCall->sizePolicy().hasHeightForWidth());
        bnMakeCall->setSizePolicy(sizePolicy4);
        bnMakeCall->setMinimumSize(QSize(32, 24));
        bnMakeCall->setLayoutDirection(Qt::RightToLeft);
        bnMakeCall->setIcon(icon1);
        bnMakeCall->setIconSize(QSize(24, 24));
        bnMakeCall->setToolButtonStyle(Qt::ToolButtonIconOnly);
        bnMakeCall->setAutoRaise(true);
        bnMakeCall->setArrowType(Qt::NoArrow);

        horizontalLayout->addWidget(bnMakeCall);


        gridLayout_2->addLayout(horizontalLayout, 0, 0, 1, 3);

        bn1 = new QPushButton(widget);
        bn1->setObjectName(QString::fromUtf8("bn1"));
        QSizePolicy sizePolicy5(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(bn1->sizePolicy().hasHeightForWidth());
        bn1->setSizePolicy(sizePolicy5);
        bn1->setMinimumSize(QSize(24, 24));
        bn1->setFont(font);
        bn1->setAutoDefault(false);
        bn1->setFlat(false);

        gridLayout_2->addWidget(bn1, 1, 0, 1, 1);

        bnPound = new QPushButton(widget);
        bnPound->setObjectName(QString::fromUtf8("bnPound"));
        sizePolicy5.setHeightForWidth(bnPound->sizePolicy().hasHeightForWidth());
        bnPound->setSizePolicy(sizePolicy5);
        bnPound->setMinimumSize(QSize(24, 24));
        bnPound->setFont(font);

        gridLayout_2->addWidget(bnPound, 6, 2, 1, 1);

        bn0 = new QPushButton(widget);
        bn0->setObjectName(QString::fromUtf8("bn0"));
        sizePolicy5.setHeightForWidth(bn0->sizePolicy().hasHeightForWidth());
        bn0->setSizePolicy(sizePolicy5);
        bn0->setMinimumSize(QSize(24, 24));
        bn0->setFont(font);

        gridLayout_2->addWidget(bn0, 6, 1, 1, 1);

        bn2 = new QPushButton(widget);
        bn2->setObjectName(QString::fromUtf8("bn2"));
        sizePolicy5.setHeightForWidth(bn2->sizePolicy().hasHeightForWidth());
        bn2->setSizePolicy(sizePolicy5);
        bn2->setMinimumSize(QSize(24, 24));
        bn2->setFont(font);

        gridLayout_2->addWidget(bn2, 1, 1, 1, 1);

        bn3 = new QPushButton(widget);
        bn3->setObjectName(QString::fromUtf8("bn3"));
        sizePolicy5.setHeightForWidth(bn3->sizePolicy().hasHeightForWidth());
        bn3->setSizePolicy(sizePolicy5);
        bn3->setMinimumSize(QSize(24, 24));
        bn3->setFont(font);

        gridLayout_2->addWidget(bn3, 1, 2, 1, 1);

        bn4 = new QPushButton(widget);
        bn4->setObjectName(QString::fromUtf8("bn4"));
        sizePolicy5.setHeightForWidth(bn4->sizePolicy().hasHeightForWidth());
        bn4->setSizePolicy(sizePolicy5);
        bn4->setMinimumSize(QSize(24, 24));
        bn4->setFont(font);

        gridLayout_2->addWidget(bn4, 3, 0, 1, 1);

        bn5 = new QPushButton(widget);
        bn5->setObjectName(QString::fromUtf8("bn5"));
        sizePolicy5.setHeightForWidth(bn5->sizePolicy().hasHeightForWidth());
        bn5->setSizePolicy(sizePolicy5);
        bn5->setMinimumSize(QSize(24, 24));
        bn5->setFont(font);

        gridLayout_2->addWidget(bn5, 3, 1, 1, 1);

        bn6 = new QPushButton(widget);
        bn6->setObjectName(QString::fromUtf8("bn6"));
        sizePolicy5.setHeightForWidth(bn6->sizePolicy().hasHeightForWidth());
        bn6->setSizePolicy(sizePolicy5);
        bn6->setMinimumSize(QSize(24, 24));
        bn6->setFont(font);

        gridLayout_2->addWidget(bn6, 3, 2, 1, 1);

        bn7 = new QPushButton(widget);
        bn7->setObjectName(QString::fromUtf8("bn7"));
        sizePolicy5.setHeightForWidth(bn7->sizePolicy().hasHeightForWidth());
        bn7->setSizePolicy(sizePolicy5);
        bn7->setMinimumSize(QSize(24, 24));
        bn7->setFont(font);

        gridLayout_2->addWidget(bn7, 5, 0, 1, 1);

        bn8 = new QPushButton(widget);
        bn8->setObjectName(QString::fromUtf8("bn8"));
        sizePolicy5.setHeightForWidth(bn8->sizePolicy().hasHeightForWidth());
        bn8->setSizePolicy(sizePolicy5);
        bn8->setMinimumSize(QSize(24, 24));
        bn8->setFont(font);

        gridLayout_2->addWidget(bn8, 5, 1, 1, 1);

        bn9 = new QPushButton(widget);
        bn9->setObjectName(QString::fromUtf8("bn9"));
        sizePolicy5.setHeightForWidth(bn9->sizePolicy().hasHeightForWidth());
        bn9->setSizePolicy(sizePolicy5);
        bn9->setMinimumSize(QSize(24, 24));
        bn9->setFont(font);

        gridLayout_2->addWidget(bn9, 5, 2, 1, 1);

        bnStar = new QPushButton(widget);
        bnStar->setObjectName(QString::fromUtf8("bnStar"));
        sizePolicy5.setHeightForWidth(bnStar->sizePolicy().hasHeightForWidth());
        bnStar->setSizePolicy(sizePolicy5);
        bnStar->setMinimumSize(QSize(24, 24));
        bnStar->setFont(font);

        gridLayout_2->addWidget(bnStar, 6, 0, 1, 1);


        gridLayout->addWidget(widget, 0, 0, 1, 1);


        retranslateUi(DialpadForm);

        QMetaObject::connectSlotsByName(DialpadForm);
    } // setupUi

    void retranslateUi(QWidget *DialpadForm)
    {
        DialpadForm->setWindowTitle(QApplication::translate("DialpadForm", "Form", 0, QApplication::UnicodeUTF8));
        actionTransferCall->setText(QApplication::translate("DialpadForm", "&\320\237\320\265\321\200\320\265\320\262\320\265\321\201\321\202\320\270", 0, QApplication::UnicodeUTF8));
        actionMakeCall->setText(QApplication::translate("DialpadForm", "&\320\222\321\213\320\267\320\276\320\262", 0, QApplication::UnicodeUTF8));
        bnHistory->setText(QApplication::translate("DialpadForm", "...", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        bnMakeCall->setToolTip(QApplication::translate("DialpadForm", "\320\222\321\213\320\267\320\276\320\262", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        bnMakeCall->setText(QString());
        bn1->setText(QApplication::translate("DialpadForm", "1", 0, QApplication::UnicodeUTF8));
        bnPound->setText(QApplication::translate("DialpadForm", "#", 0, QApplication::UnicodeUTF8));
        bn0->setText(QApplication::translate("DialpadForm", "0", 0, QApplication::UnicodeUTF8));
        bn2->setText(QApplication::translate("DialpadForm", "2", 0, QApplication::UnicodeUTF8));
        bn3->setText(QApplication::translate("DialpadForm", "3", 0, QApplication::UnicodeUTF8));
        bn4->setText(QApplication::translate("DialpadForm", "4", 0, QApplication::UnicodeUTF8));
        bn5->setText(QApplication::translate("DialpadForm", "5", 0, QApplication::UnicodeUTF8));
        bn6->setText(QApplication::translate("DialpadForm", "6", 0, QApplication::UnicodeUTF8));
        bn7->setText(QApplication::translate("DialpadForm", "7", 0, QApplication::UnicodeUTF8));
        bn8->setText(QApplication::translate("DialpadForm", "8", 0, QApplication::UnicodeUTF8));
        bn9->setText(QApplication::translate("DialpadForm", "9", 0, QApplication::UnicodeUTF8));
        bnStar->setText(QApplication::translate("DialpadForm", "*", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class DialpadForm: public Ui_DialpadForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALPADFORM_H
