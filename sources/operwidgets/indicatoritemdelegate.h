#ifndef INDICATORITEMDELEGATE_H
#define INDICATORITEMDELEGATE_H

#include "operwidgets_global.h"
#include <QStyledItemDelegate>

class OPERWIDGETS_EXPORT IndicatorItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit IndicatorItemDelegate(QObject *parent = 0);


    // painting
    virtual void paint(
            QPainter *painter,
            const QStyleOptionViewItem &option,
            const QModelIndex &index
            ) const;

    virtual QSize sizeHint(
            const QStyleOptionViewItem &option,
            const QModelIndex &index
            ) const;

signals:

public slots:

};

#endif // INDICATORITEMDELEGATE_H
