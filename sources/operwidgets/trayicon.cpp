#include "trayicon.h"
#include "appcore/sessioncontroller.h"
#include "appcore/shortcutmanager.h"
#include "appcontext.h"

//#ifdef Q_WS_WIN
//#   define _WIN32_WINDOWS 0x0500
//#   include <windows.h>
//#endif


using namespace ccappcore;

TrayIcon::TrayIcon(QObject *parent)
    : QSystemTrayIcon( QIcon(":/resources/icons/appicon_32x32.png"), parent),
      _mainWindowMinimized(false)/*,_messageType(BMT_UNKNOWN)*/
{

    setToolTip(tr("Call-o-Call® Operator Agent"));
    connect(this, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(activate(QSystemTrayIcon::ActivationReason)));

    _statusMenu.bindOperator(_sessionController->sessionOwner());
    LateBoundObject<ShortcutManager> scManager;
    _contextMenu.addMenu(&_statusMenu);
    QKeySequence toggleVisibilityKey = scManager->key("global.toggleVisibility");
    _contextMenu.addSeparator();
    _contextMenu.addAction(tr("Показать\\Скрыть главное окно"),
                           _appContext.instance(),
                           SLOT(toggleVisibility()), toggleVisibilityKey );

    QAction* action_quit = _contextMenu.addAction(tr("Выйти"), qApp, SLOT(quit()));
    connect(action_quit,SIGNAL(triggered()),this,SLOT(on_quit()));

    setContextMenu(&_contextMenu);

    _statusMenu.actionGroup().setVisible(_callsController->sessionOwner().isValid());

    LateBoundObject<SessionController> sessionController;
    connect(sessionController.instance(), SIGNAL(sessionStarted()),
            this, SLOT(operSessionStarted()));

    connect(_callsController.instance(), SIGNAL(callAssigned(ccappcore::Call)),
            this, SLOT(showCallMessage(ccappcore::Call)));
    connect(_callsController.instance(), SIGNAL(callStarted(ccappcore::Call)),
            this, SLOT(on_callStarted(ccappcore::Call)));
    connect(this,SIGNAL(messageClicked()), this, SLOT(on_messageClicked()));
    connect(_spamController.instance(), SIGNAL(newMessage(SpamMessage)),
            this, SLOT(showSpamMessage(SpamMessage)));
    connect(_appContext.instance(),SIGNAL(windowMinimized(bool)),
            this,SLOT(on_windowMinimized(bool)));

    connect(qApp, SIGNAL(aboutToQuit()), this, SLOT(hide()));
    show();
}

TrayIcon::~TrayIcon()
{
    hide();
}

void TrayIcon::activate(QSystemTrayIcon::ActivationReason reason)
{
    if(reason == QSystemTrayIcon::DoubleClick)
        _appContext->toggleVisibility();
}

void TrayIcon::operSessionStarted()
{
    _statusMenu.bindOperator(_sessionController->sessionOwner());
    _statusMenu.actionGroup().setVisible(_sessionController->sessionOwner().isValid());
}

void TrayIcon::showCallMessage(ccappcore::Call call)
{
    _associatedMessageCall = call;
    _messageType = BMT_CALL;
    if(_mainWindowMinimized)
        return;

    this->showMessage("", _callsController->callShortInfo(call),
                  QSystemTrayIcon::Information,
                  _callsController->noAnswerTimeoutSeconds()*1000 );
}

void TrayIcon::showSpamMessage(SpamMessage message)
{
    _associatedMessageChat = message;
    _messageType = BMT_CHAT;
    if(_mainWindowMinimized)
        return;

    this->showMessage( (message.msgType() == MT_SYSTEM)? tr("Объявление"):"",
                       SpamMessage::shortInfo(message),
                  QSystemTrayIcon::Information,
                       _spamController->spamMessageTimeout()*1000);
}

void TrayIcon::on_callStarted(ccappcore::Call c)
{
    if( /*!_operSettings->showCallFormImmediately() &&*/ _callsController->isIncomingCall(c) )
    { // when incoming call comes (C2O_INCOMING_CALL) baloon message tell about it
        // unusful showCallFormImmediately option here
        showCallMessage(c);
    }
}

void TrayIcon::on_messageClicked()
{
    LateBoundObject<AppContext> appContext;

    switch (_messageType)
    {
    case BMT_CALL:
    {
        if (_associatedMessageCall.isValid())
        {
            appContext->showCallForm(_associatedMessageCall);
        }
        break;
    }
    case BMT_CHAT:
    {
        if (_associatedMessageChat.isValid() && _associatedMessageChat.msgType() != MT_SYSTEM)
        {
            //_associatedMessageChat
            ContactId personId = _opersController->createOperId(_associatedMessageChat.operFromId());
            Contact c = _opersController->findById(personId);
            if (!c.isValid())
            {
                RosterId rosterId = _jabberController->createRosterId(_associatedMessageChat.operFromId());
                c = _jabberController->findById(rosterId);
            }
            if (!c.isValid())
                return;

            appContext->showChatForm(c);
        }
        break;
    }
    default:
        return;
    }

    appContext->onActivateForegroundWindow();
}
void TrayIcon::on_windowMinimized(bool minimized)
{
    _mainWindowMinimized = minimized;
}


void TrayIcon::on_quit()
{
    qDebug() << tr("TrayIcon::on_quit()");
}
