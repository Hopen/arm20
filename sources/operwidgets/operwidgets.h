#ifndef OPERWIDGETS_H
#define OPERWIDGETS_H

#include "operwidgets_global.h"
#include "appcore/iappmodule.h"

class OPERWIDGETS_EXPORT OperWidgets:
        public QObject
        , public ccappcore::IAppModule
{
       Q_OBJECT
       Q_INTERFACES(ccappcore::IAppModule)

public:
    OperWidgets();
    ~OperWidgets();

    void initialize() throw();

};


#endif // OPERWIDGETS_H
