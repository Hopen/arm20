#include "dialpad.h"
#include "ui_dialpad.h"

DialPad::DialPad(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DialPad)
{
    ui->setupUi(this);
}

DialPad::~DialPad()
{
    delete ui;
}

void DialPad::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
