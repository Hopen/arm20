#include "statetimedelegate.h"
#include "appcore/timeutils.h"
#include "appcore/useritemdatarole.h"

using namespace ccappcore;

StateTimeDelegate::StateTimeDelegate(StateTimeFormat format, QObject *parent)
    : QStyledItemDelegate(parent)
    ,_stateTimeFormat(format)
{
}

// painting
void StateTimeDelegate::paint(QPainter *painter,
           const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStyledItemDelegate::paint(painter,option,index);
    QString displayText = index.data(Qt::DisplayRole).toString();
    QString stateReason = index.data(ccappcore::StateReasonRole).toString();
    QDateTime  stateTime = index.data(ccappcore::StateTimeRole).toDateTime();

    if(stateReason.isEmpty() && !stateTime.isValid())
        return;

    QVariant varAlign = index.data(Qt::TextAlignmentRole);
    Qt::Alignment displayTextAlign = varAlign.isValid()
                              ? (Qt::Alignment)varAlign.toInt()
                              : Qt::AlignLeft | Qt::AlignVCenter;

    Qt::Alignment stateAlign =  (displayTextAlign & ~Qt::AlignHorizontal_Mask) | Qt::AlignRight;

    QStyleOptionViewItemV4 vo = option;
    QStyle* style = vo.widget->style() ? vo.widget->style() : qApp->style();
    QRect displayTextRect = style->itemTextRect( vo.fontMetrics,
                                                     vo.rect,
                                                     displayTextAlign,
                                                     true,
                                                     displayText
                                                    );

    displayTextRect.adjust(vo.decorationSize.width(),0,vo.decorationSize.width(),0);
    QColor textColor = index.data(Qt::TextColorRole).value<QColor>();
    if(textColor.isValid())
        vo.palette.setColor(QPalette::Text,textColor);

//    static QIcon msgIcon(":/resources/icons/new/messages.png");
//    QRect iconRect (displayTextRect);
//    iconRect.setWidth(16);
//    displayTextRect.adjust(16,0,16,0);
//    style->drawItemPixmap(
//                painter,
//                iconRect,
//                stateAlign,
//                msgIcon.pixmap(QSize(16,16))
//                );

    QRect stateTimeRect;
    if(stateTime.isValid())
    {
        int stateSeconds = stateTime.secsTo(QDateTime::currentDateTime());

        QString stateTimeString;
        switch(_stateTimeFormat)
        {
            case FormatSeconds:
                stateTimeString = TimeUtils::SecsToShortHMS(stateSeconds);
                break;
            case FormatMinutes:
            default:
                stateTimeString = TimeUtils::SecsToLongHMS(stateSeconds);
        }


        if(!stateTimeString.isEmpty())
        {
            stateTimeRect = style->itemTextRect( vo.fontMetrics,
                                                             vo.rect,
                                                             stateAlign,
                                                             true,
                                                             stateTimeString
                                                            );
            stateTimeRect.adjust(-15,0,-5,0);
            if(!stateTimeRect.intersects(displayTextRect))
            {
                style->drawItemText( painter,
                                     stateTimeRect,
                                     stateAlign,
                                     vo.palette, true,
                                     stateTimeString, QPalette::Text );
            }
        }
    }

    if(!stateReason.isEmpty())
    {
        QPoint bottomRight = stateTimeRect.isValid() ?
                             stateTimeRect.bottomLeft() : vo.rect.bottomRight();

        QRect stateReasonRect = QRect(displayTextRect.topRight(), bottomRight );
        stateReasonRect.adjust(5,0,-5,0);

        QString stateReasonElided = vo.fontMetrics.elidedText(stateReason,Qt::ElideRight,stateReasonRect.width(),0);
        style->drawItemText( painter,
                             stateReasonRect,
                             stateAlign,
                             vo.palette, true,
                             stateReasonElided, QPalette::Text );
    }
}

QSize StateTimeDelegate::sizeHint(const QStyleOptionViewItem &option,
               const QModelIndex &index) const
{
    return QStyledItemDelegate::sizeHint(option, index);
}

