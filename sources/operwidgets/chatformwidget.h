#ifndef CHATFORMWIDGET_H
#define CHATFORMWIDGET_H
#include "docktabs.h"


class ChatFormWidget: public DockTab
{
    Q_OBJECT
public:
    explicit ChatFormWidget(ccappcore::Contact c, const QString &title, QWidget *parent = 0, Qt::WindowFlags flags = 0);

    explicit ChatFormWidget(ccappcore::Contact c, QWidget *parent = 0, Qt::WindowFlags flags = 0)
        :DockTab(parent,flags) {ChatFormWidget(c, tr(""),parent,flags);}

    virtual ~ChatFormWidget();
    virtual QString title() const;

    //virtual void func()const{}
    virtual int type() const{return DTT_CHAT;}

    ccappcore::Contact* assignedContact(){return &_contact;}

//public slots:
//    void tabTitleColorChanged(int status);
public slots:
    void close();


private:
    QString _title;
    ccappcore::Contact _contact;
};



#endif // CHATFORMWIDGET_H
