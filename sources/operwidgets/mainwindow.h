#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui>
//#include <QtScript/qscriptengine.h>

#include "operwidgets_global.h"
#include "appcore/sessioncontroller.h"
#include "appcore/operlistcontroller.h"
#include "appcore/callscontroller.h"
#include "appcore/shortcutmanager.h"
#include "appcore/layoutmanager.h"
#include "appcore/styleloader.h"
#include "appcore/presetmanager.h"

#include "ioperwidgetsplugin.h"

#include "opersettings.h"
#include "appcontext.h"
#include "contactsmanager.h"
#include "tabswindow.h"
#include "volumecontroller.h"

using ccappcore::OperListController;
using ccappcore::CallsController;
using ccappcore::SessionController;

class SearchBar;
class ToolButtonOperStatus;
class ContactsView;
//class DialpadForm;
class ContactsWidget;
//class ContactsWidget2;
class ContactsWidget3;
class ApplicationWidget;
class OperToolsWidget;
//class CallsWidget;
class CallsView;
class ApplicationToolBar;
class OperToolBar;
class RedirProfileWidget;
class RedirectRulesWidget;
class ScriptEventWidget;
class CallsWidget2;
class LocalCallHistoryWidget;
class BBListWidget;
class CallsWidgetM;
class QDockMinimizedWidget;
class QDockWidgetEx;
class DockTabsWidget;
class ChatWidget;
class OptionsDlg;
class GlobalAddressBookWidget;
//class QDockMinimizedWidget2;

namespace Ui {
    class mainWindow;
}

//class OPERWIDGETS_EXPORT MainWindow2 : public QMainWindow
//{
//    Q_OBJECT

//public:
//    MainWindow2(QWidget *parent = 0);
//    ~MainWindow2();

//protected:
//    bool event(QEvent *event);
//    void closeEvent(QCloseEvent *);
//    void changeEvent(QEvent*);
//};

//class QSliderEx:public QSlider
//{
//    Q_OBJECT
//public:
//    explicit QSliderEx(QWidget* parent = 0)
//        :QSlider(parent)
//    {

//    }
//    explicit QSliderEx(Qt::Orientation orientation, QWidget *parent = 0)
//        :QSlider(orientation,parent)
//    {

//    }

//    QSize minimumSizeHint() const;
//};

namespace TestCore2
{
class testClass
{
public:
    testClass(QString str1, QString str2)
    {
        _str = str1;
    }

    testClass(const testClass& rhs)
    {
        _str = rhs._str;
    }

private:
    QString _str;
};

namespace TestNamespace
{
    extern const TestCore2::testClass test1;
}
}




class OPERWIDGETS_EXPORT MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QDockWidget* createDockWidget(
            const QString& objectName,
            const QString& title,
            QWidget *widget
            );

    //void testQScriptEngine();
    //QSharedPointer<QScriptEngine> _engine;

    enum TabsPosition
    {
        TB_USERCALLS = 0,
        TB_QUEUECALLS,
        TB_USERS,
        TB_HISTORY,
        TB_MESSAGES,
        TB_OLDCALLS,
        TB_CHAT,
        TB_WGSTATUS,
        TB_PERFORMANCE,
        TB_BOOK,
        TB_TEST
    };

    //void addTabWidget(QWidget*);
    //QDockWidget * createContactsWidget2();

signals:
    void privateOnly(bool);

public slots:
    void toggleVisibility();
    void togglePause();
    void moveFocusOnSearch();
    //void showDialer(const QString& phone = QString());
    void showCallForm(ccappcore::Call c);
    void showLoginDialog();
    void test();
    void activateForegroundWindow();
//    void on_operform_visibilityChanged(bool checked);
    void on_callform_visibilityChanged(bool checked);
    void onOperformClosed();
    //void onCallformClosed(bool);
    void onShowVolumeWidget();
    void onHideVolumeWidget();
    void onVolumeChange(int value);
    void onVolumeSliderReleased();

protected:
    void changeEvent(QEvent *e);
    void closeEvent(QCloseEvent *);
    void showEvent(QShowEvent *);
    bool event(QEvent *e);

private:

    void setupMainWindowWidgets();
    //void setupCallFormWidgets();
    void setupStatusBar();
    void setupMenu();
    void setupGlobalShortcuts();
    void setupEventHandlers();
    void setupTabsWidget();
    void setupToolbars();
    void initPlugins();
    void setupMinimizedCallWidget();
    void setupMinFloatDockWindow();
    void setupAnimation();
    void setupFloatWindows();
    void setupVolumeWidget();
    QRect getScreenRect()const;

    int addTab(/*quint32 index, */QWidget *widget, /*const QIcon& icon,*/ const QString &label);

    Ui::mainWindow *_ui;

    LateBoundObject<AppContext>         _appContext;
    LateBoundObject<SessionController>  _sessionController;
    LateBoundObject<OperListController> _operListController;
    LateBoundObject<CallsController>    _callsController;
    LateBoundObject<ShortcutManager>    _shortcutsMgr;
    LateBoundObject<OperSettings>       _operSettings;
    LateBoundObject<StatusOptions>      _statusOptions;
    LateBoundObject<ContactsManager>    _contactsManager;
    LateBoundObject<LayoutManager>      _layoutManager;
    LateBoundObject<PresetManager>      _presetManager;
    LateBoundObject<StyleLoader>        _styleLoader;
    LateBoundObject<AppSettings>        _settings;

    //QPointer<TabsWindow>            _callForm;
    //QPointer<ToolButtonOperStatus>    _toolButtonStatus;
    //QPointer<QStatusBar>                _statusBar;

    //QPointer<DialpadForm>             _dialerWidget;
    //QPointer<ContactsWidget>          _contactsWidget;
    QPointer<ContactsView>            _callsView;

    QPointer<ApplicationWidget>       _applwidget;
    QPointer<OperToolsWidget>         _operwidget;
    //QPointer<CallsWidget>             _callwidget;
    //QPointer<CallsView>               _callwidget;
    QPointer<ApplicationToolBar>      _appltools;
    QPointer<OperToolBar>             _opertools;
    QPointer<ContactsWidget3>         _contacts3;
    QPointer<RedirProfileWidget>      _profiles;
    QPointer<RedirectRulesWidget>     _rules;
    QPointer<ScriptEventWidget>       _scripts;
    QPointer<CallsWidget2>            _operCallsWidget;
    QPointer<CallsWidget2>            _queueCallsWidget;
    QPointer<LocalCallHistoryWidget>  _localHistory;
    QPointer<BBListWidget>            _bbMessages;
    QPointer<GlobalAddressBookWidget> _globalAddressBook;

    QPointer<TabsWidget>            _tabWidget;
    QDockWidgetEx*                  _dwOperFormTab;
    QDockWidgetEx*                  _dwCallFormTab;
    //DockTabsWidget*                 _dwCallFormTab;
    QDockMinimizedWidget*                    _minFloatDockWindow;
    //QDockWidget*                    _dwCallForm;
    QDockWidget*                    _dwVolumeForm;
    QPointer<QSlider>               _volumeSlider;

    QPointer<QStateMachine>   _stateMachine;
    QPointer<CallsWidgetM>          _operMinCalls;
    //QPointer <ChatWidget>     _chat;
    //QPointer<TestWidget> _testW;

    /*VolumeController          _volumeController;
    QTimer                    _volumeTimer;
    int                       _curVolumeValue;*/

    QPointer<OptionsDlg>               _dlgOptions;
    bool                    _bProgramPropertiesHasChanged;

    void moveToScreenCenter(QWidget* w);
private slots:
    void on_actionSavePreset_triggered();
    void on_actionShowDialer_triggered();
    void on_actionPrivateGroupsOnly_triggered(bool checked);
    void on_actionHideOfflineContacts_triggered(bool checked);
    void on_actionOperListGroupMode_triggered(bool checked);
    void on_actionSettings_triggered();
    void on_actionAbout_triggered();
    void on_actionDisconnect_triggered();
    void on_actionQuit_triggered();    
    void callToTransferChanged(ccappcore::Call);
    void onActionExpandMinimzedWindow      (qint32 size);
    void onActionOptionsApplied();
    //void on_currentTabChanged(int index);


    void on_actionOperators_only_test_triggered(bool checked);
    void on_actionGroups_only_test_triggered(bool checked);

    void on_action_showUserCallTab_triggered (bool checked);
    void on_action_showQueueCallTab_triggered(bool checked);
    void on_action_showUsersTab_triggered    (bool checked);
    void on_action_showHistoryTab_triggered  (bool checked);
    void on_action_showMessagesTab_triggered (bool checked);
    void onActionMinimizeWindow            (bool checked);
    void onActionStaysOnTopWindow          (bool checked);
    void on_action_operform_visiable_triggered(bool checked);
    void on_action_callform_visiable_triggered(bool checked);
    //void on_action_showWGStatusTab_triggered (bool checked = false);


    void disconnectHandlers();
    bool maybeQuit();
    void statusInfo(const QString& message);

private slots:
    void showCallFailReason(ccappcore::Call,ccappcore::Call::CallResult,const QString&);
    void showJoinCallToConfFailedReason(int callid, int error);

    friend class Dashboard;
};
#endif // MAINWINDOW_H
