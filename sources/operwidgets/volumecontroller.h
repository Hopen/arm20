#ifndef VOLUMECONTROLLER_H
#define VOLUMECONTROLLER_H

class VolumeController
{
public:
    VolumeController();
    virtual ~VolumeController();
    unsigned long getVolume()const;

public slots:
    void setVolume(quint32);
private:
    unsigned long m_saveVolume;
};

#endif // VOLUMECONTROLLER_H
