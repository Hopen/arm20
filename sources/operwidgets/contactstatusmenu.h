#ifndef OPERSTATUSMENU_H
#define OPERSTATUSMENU_H

#include "operwidgets_global.h"

#include "appcore/lateboundobject.h"
#include "appcore/operlistcontroller.h"
#include "appcore/statusoptions.h"
#include "appcore/appsettings.h"
#include "appcore/redirectlistcontroller.h"
#include "redirruleswidget.h"

using namespace ccappcore;

class ContactStatusMenu
    : public QMenu
{
    Q_OBJECT;
public:
    ContactStatusMenu(QWidget* parent = 0);
    ~ContactStatusMenu();

    void bindOperator(ccappcore::Contact op);
    void unbindOperator();
    ccappcore::Contact boundOperator() { return _oper; }

    QActionGroup& actionGroup() { return _actionGroup; }
    QList<QAction*> actions() { return _actionGroup.actions(); }
    QAction* checkedAction() { return _actionGroup.checkedAction(); }


public slots:
    void requestStatus(ccappcore::Contact::ContactStatus, const QString& reason);
    void loadCustomReasons();
signals:
    void checkedActionChanged(const QAction*);
private:
    LateBoundObject<OperListController> _operListController;
    LateBoundObject<StatusOptions> _statusReasons;
    LateBoundObject<RedirectListController> _redirListController;

    QAction* _actionOnline;
    QAction* _actionPaused;
    QAction* _actionReadyByNumber;
    QAction* _actionReadyByVoicemail;
    QActionGroup _actionGroup;
    QActionGroup _profileActionGroup;
    QList<QAction*> _customReasons;

    QAction* append( ccappcore::Contact::ContactStatus status,
                     const QString& displayName );
    QAction* append( ccappcore::RedirectRule::ePredefineRules status,
                     const QString& displayName );

    ccappcore::Contact _oper;
    //ProfileSystemMaster* _ruleMaster;
private slots:
    void operStatusChanged(ccappcore::Contact);
    void operListChanged();
    void actionTriggered();
    void profileTriggered();
    void on_profileActivated(qint32);
    void on_profileActivateError(QString errorText);
};

#endif // OPERSTATUSMENU_H
