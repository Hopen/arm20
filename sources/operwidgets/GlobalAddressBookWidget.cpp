#include "GlobalAddressBookWidget.h"
#include "mainwindow.h"
#include "lineeditwithhint.h"
#include "appcore/focuseventtranslator.h"
#include "appcore/domainobjectfilter.h"
#include "appcore/useritemdatarole.h"

//GlobalAddressModel::GlobalAddressModel(QObject *parent):QAbstractListModel(parent)
//{
//    _columnsList << IdColumn
//                 << NameColumn
//                 << FullNameColumn
//                 << ParentIdColumn
//                 << TypeColumn
//                 << BookIdColumn
//                 << BookNameColumn;

//    connect(_controller.instance(),
//            SIGNAL(onLoadFinished()),
//            this,
//            SLOT(reset()),
//            Qt::QueuedConnection);


//    reset();
//}


//QString GlobalAddressModel::columnNameById(const qint32 &id) const
//{
//    switch(id)
//    {
//    case IdColumn:
//        return tr("АйДи");
//    case NameColumn:
//        return tr("Имя");
//    case FullNameColumn:
//        return tr("Полное имя");
//    case ParentIdColumn:
//        return tr("ПарентАйДи");
//    case TypeColumn:
//        return tr("Тип");
//    case BookIdColumn:
//        return tr("БукАйДи");
//    case BookNameColumn:
//        return tr("БукИмя");

//    default:
//        return tr("Неизвестно");
//    }

//    return QString();
//}

//int GlobalAddressModel::rowCount(const QModelIndex &/*parent*/) const
//{
//    return _persons.size();
//}

//int GlobalAddressModel::columnCount(const QModelIndex &/*parent*/) const
//{
//    return _columnsList.size();
//}

//QVariant GlobalAddressModel::data(const QModelIndex &index, int role) const
//{
//    if (!index.isValid())
//        return QVariant();
//    if (index.row() >= _persons.size())
//        return QVariant();

//    if(index.column() <0 || index.column() >= _columnsList.count() )
//        return QVariant();

//    GlobalAddress g = _persons[index.row()];
//    if(!g.isValid())
//        return QVariant();

//    ColDef column = _columnsList[index.column()];

//    if (role == Qt::DisplayRole)
//    {

////        switch(column)
////        {
////        case IdColumn:
////            return g.id();
////        case NameColumn:
////            return g.name();
////        case FullNameColumn:
////            return g.full_name();
////        case ParentIdColumn:
////            return g.parent_id();
////        case TypeColumn:
////            return g.type();
////        case BookIdColumn:
////            return g.book_id();
////        case BookNameColumn:
////            return g.book_name();

////        default:
////            return tr("");
////        }
//    }

//    if(role == Qt::DecorationRole)
//    {
//    }

////    if(role == Qt::TextColorRole)
////        return _controller->statusColor(c.status());

////    if(role == ccappcore::AssociatedObjectRole)
////        return QVariant::fromValue(c);


//    return QVariant();
//}

//QVariant GlobalAddressModel::headerData(int section, Qt::Orientation orientation,
//                                         int role) const
//{
//       if (role != Qt::DisplayRole)
//            return QVariant();

//        if (orientation == Qt::Horizontal)
//            return columnNameById(_columnsList.value(section));
//        else
//            return QString("Row %1").arg(section);
//}

//void GlobalAddressModel::applyFilter(const PredicateGroup<GlobalAddress> &filter)
//{
//    emit layoutAboutToBeChanged();

//    _filter = filter;

//    _persons.clear();
//    _persons =  _controller->findGlobalAddresses(_filter);

//    emit layoutChanged();
//}

//void GlobalAddressModel::reset()
//{
//    applyFilter(_filter);
//}

//void GlobalAddressModel::sort()
//{
////    emit layoutAboutToBeChanged();
////    //qSort(_contacts);

////    emit layoutChanged();
//}


AddressExtraInfoModel::AddressExtraInfoModel(QObject *parent):QAbstractListModel(parent)
{
    _columnsList << TypeColumn
                 << ValueColumn;

    connect(_controller.instance(),
            SIGNAL(onLoadInfoCompleted(ccappcore::Contact)),
            this,
            SLOT(onExtraInfoLoaded(ccappcore::Contact)),
            Qt::QueuedConnection);


    reset();
}


QString AddressExtraInfoModel::columnNameById(const qint32 &id) const
{
    switch(id)
    {
    case TypeColumn:
        return tr("Тип");
    case ValueColumn:
        return tr("Значение");

    default:
        return tr("Неизвестно");
    }

    return QString();
}

QString AddressExtraInfoModel::typeNameById(const qint32 &id) const
{
    switch(id)
    {
    case ETI_NAME:
        return tr("Имя");
    case ETI_EMAIL:
        return tr("E-mail");
    case ETI_PHONE:
        return tr("Телефон");
    case EIT_ADDRESS:
        return tr("Адрес");
    case EIT_ORGANIZATION:
        return tr("Организация");

    default:
        return tr("Неизвестно");
    }

    return QString();
}


int AddressExtraInfoModel::rowCount(const QModelIndex &/*parent*/) const
{
    return _info.size();
}

int AddressExtraInfoModel::columnCount(const QModelIndex &/*parent*/) const
{
    return _columnsList.size();
}

QVariant AddressExtraInfoModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (index.row() >= _info.size())
        return QVariant();

    if(index.column() <0 || index.column() >= _columnsList.count() )
        return QVariant();

    GlobalAddressBookController::AddressExtraInfo info = _info[index.row()];
//    if(!info.isValid())
//        return QVariant();

    ColDef column = _columnsList[index.column()];

    if (role == Qt::DisplayRole)
    {

        switch(column)
        {
        case TypeColumn:
            return typeNameById(info.type_id);
        case ValueColumn:
            return info.info;



        default:
            return tr("");
        }
    }

    if (role == ccappcore::AddressTypeRole)
    {
        return info.type_id;
    }

    if (role == ccappcore::AddressValueRole)
    {
        return info.info;
    }

    if(role == Qt::DecorationRole)
    {
    }

//    if(role == Qt::TextColorRole)
//        return _controller->statusColor(c.status());

//    if(role == ccappcore::AssociatedObjectRole)
//        return QVariant::fromValue(c);


    return QVariant();
}

QVariant AddressExtraInfoModel::headerData(int section, Qt::Orientation orientation,
                                         int role) const
{
       if (role != Qt::DisplayRole)
            return QVariant();

        if (orientation == Qt::Horizontal)
            return columnNameById(_columnsList.value(section));
        else
            return QString("Row %1").arg(section);
}


void AddressExtraInfoModel::onExtraInfoLoaded(const ccappcore::Contact &c)
{
    _assignedAddress = c;
    reset();
}

void AddressExtraInfoModel::reset()
{
    emit layoutAboutToBeChanged();

    if (_assignedAddress.isValid())
        _info = _controller->getExtraInfoList(_assignedAddress.contactId().toInt());
    else
        _info.clear();

    emit layoutChanged();
}



GlobalAddressBookWidget::GlobalAddressBookWidget(QWidget *parent): IAlarmHandler(parent)
{
    IAlarmHandler::init();

    //_mainModel = new GlobalAddressModel(this);
    _mainModel = new ContactsTreeModel();

    _mainModel->setController(_controller.instance());
    _mainModel->setListMode(false);
    //_mainModel->addFilter(new IsContactOnline());

    QList<int> columns;
    //columns<<Contact::BookName;
    columns<<Contact::Name;
    columns<<Contact::LongName;
    _mainModel->setColumns(columns);

    _mainView = new QTreeView(this);

    //_mainView->setRootIsDecorated(false);
    _mainView->setModel(_mainModel);
    _mainView->setSelectionBehavior(QAbstractItemView::SelectRows);

//    onResetModel();



//    _mainView->header()->setSortIndicatorShown(false);
//    _mainView->header()->setClickable(true);
//    _mainView->setSortingEnabled(true);

    //connect(_mainView.data(),SIGNAL(doubleClicked(QModelIndex)),this,SLOT(on_contactSelect(QModelIndex)));
//    connect(_appContext.instance(), SIGNAL(activeContactChanged(ccappcore::Contact)), this, SLOT(on_contactChanged(ccappcore::Contact)));

    _infoView = new QTreeView(this);
    QAbstractItemModel * infoModel = new AddressExtraInfoModel();
    _infoView->setModel(infoModel);
    _infoView->setSelectionBehavior(QAbstractItemView::SelectRows);
    _infoView->setRootIsDecorated(false);


    pauseTimoutTimer.setSingleShot(true);
    //TODO: move timeout to settings?
    pauseTimoutTimer.setInterval(2000);

    connect(&pauseTimoutTimer, SIGNAL(timeout()),
            this, SLOT(searching()));


    _searchLine  = new LineEditWithHint(this);
    _searchLine->setAutoFillBackground(false);
    _searchLine->setHintText(tr("Поиск"));
    connect(_searchLine.data(), SIGNAL(textChanged(const QString &)), this, SLOT(on_searchLine_textChanged(QString)));

    FocusEventTranslator* focusHandler = new FocusEventTranslator(_searchLine);
    connect(focusHandler, SIGNAL(focusIn()),
            this, SLOT(startTimeout()));

    QSplitter *vSplitter = new QSplitter;
    vSplitter->setOrientation(Qt::Horizontal);
    vSplitter->addWidget(_mainView);
    vSplitter->addWidget(_infoView);
    vSplitter->setChildrenCollapsible(false);

    QVBoxLayout *topLayout = new QVBoxLayout();
    topLayout->addWidget(vSplitter);
    topLayout->addWidget(_searchLine);
    setLayout(topLayout);


    connect(_appContext.instance(), SIGNAL(activeGlobalAddressChanged(ccappcore::Contact)),
            this, SLOT(onGlobalAddressChanged(ccappcore::Contact)));

    connect(_appContext.instance(), SIGNAL(activeGlobalAddressSelected(ccappcore::Contact)),
            this, SLOT(onGlobalAddressSelected(ccappcore::Contact)));

    connect(_controller.instance(), SIGNAL(onLoadInfoCompleted(ccappcore::Contact)),
            this, SLOT(onLoadPersonInfoCompleted(ccappcore::Contact)));

//    connect(_controller.instance(), SIGNAL(resetModel()),
//            this, SLOT(onResetModel()));
}

int GlobalAddressBookWidget::getTabIndex()const
{
    return MainWindow::TB_BOOK;
}

QIcon GlobalAddressBookWidget::getTabIcon(bool /*_default*/) const
{
    return QIcon(":/resources/icons/file_browse_16x16.png");
}

void GlobalAddressBookWidget::connectToTimer()
{
}

void GlobalAddressBookWidget::startTimeout()
{
    pauseTimoutTimer.start();
}

void GlobalAddressBookWidget::searching()
{
    QString searchText = _searchLine->text();
    if (searchText.isEmpty())
    {
        if (!_lastSearchString.isEmpty())
        {
            _lastSearchString.clear();
            _controller->resetList();
        }
        return;
    }
    if (_searchLine->isHintVisible())
        return;
    if (_lastSearchString == searchText)
        return;
    _lastSearchString = searchText;

    _controller->search(searchText, true);
}

void GlobalAddressBookWidget::on_searchLine_textChanged(QString text)
{
    Q_UNUSED(text)

    startTimeout();
}


void GlobalAddressBookWidget::onGlobalAddressChanged(ccappcore::Contact c)
{
    _controller->getPersonInfo(c.contactId());
}

void GlobalAddressBookWidget::onGlobalAddressSelected(ccappcore::Contact c)
{

}

void GlobalAddressBookWidget::onLoadPersonInfoCompleted(const ccappcore::Contact &c)
{

}

void GlobalAddressBookWidget::onResetModel()
{
//    _mainModel = QSharedPointer< ContactsTreeModel   >(new ContactsTreeModel());

//    _mainModel->setController(_controller.instance());
//    _mainModel->setListMode(false);
//    //_mainModel->addFilter(new IsContactOnline());

//    QList<int> columns;
//    //columns<<Contact::BookName;
//    columns<<Contact::Name;
//    columns<<Contact::LongName;
//    _mainModel->setColumns(columns);

//    //_mainView->setRootIsDecorated(false);
//    _mainView->setModel(_mainModel.data());
//    _mainView->setSelectionBehavior(QAbstractItemView::SelectRows);


    if (_searchLine && !_searchLine->text().isEmpty())
        _mainView->expandAll();
}
