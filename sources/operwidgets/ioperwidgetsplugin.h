#ifndef IOPERWIDGETSPLUGIN_H
#define IOPERWIDGETSPLUGIN_H

#include "operwidgets_global.h"
#include "appcore/serviceprovider.h"

class OPERWIDGETS_EXPORT IOperWidgetsPlugin
{
    public:
    virtual ~IOperWidgetsPlugin() {}
    virtual void initializeMainWindow(QMainWindow*) {}
};

Q_DECLARE_INTERFACE(IOperWidgetsPlugin, "ru.forte-it.calloper.ioperwidgetsplugin/1.0")

#endif // IOPERWIDGETSPLUGIN_H
