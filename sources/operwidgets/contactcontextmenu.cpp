#include "contactcontextmenu.h"

ContactContextMenu::ContactContextMenu(QWidget *parent)
    : QMenu(parent)
{    
    _statusMenuItem = addMenu( &_statusMenu );
    addSeparator();

    _makeCallAction = addAction(QIcon(":/resources/icons/dial_32x32.png"),
                                tr("Позвонить"),
                                this, SLOT(doMakeCall()));

    _transferCallAction = addAction(QIcon(":/resources/icons/call_transfer_32x32.png"),
                                    tr("Перевести звонок"),
                                    this, SLOT(doTransfer())
                                    );

    _mailToAction = addAction(QIcon(":/resources/icons/mail_new_16x16.png"),
                              tr("Написать письмо"),
                              this, SLOT(doNewMail())
                              );
}


void ContactContextMenu::popup(ccappcore::Operator op)
{
    _statusMenuItem->setVisible(op.controller()->canChangeStatus(op));
    _statusMenu.bindOperator(op);
    _op = op;

    QList<int> phoneInfoIdList;
    phoneInfoIdList<<Contact::Phone<<Contact::MobilePhone<<Contact::OfficePhone<<Contact::Fax;

    QString phone = _op.data(Contact::Phone).toString();

    //handle makecall action
    bool canMakeCall = _op.canMakeCall();
    _makeCallAction->setVisible(canMakeCall);
    _makeCallAction->setText(tr("Позвонить"));
    if(!addContactInfo(phoneInfoIdList, _makeCallAction))
        _makeCallAction->setText( tr("Позвонить (%1)").arg(phone) );

    //handle transfer call action
    Call callToTransfer = _appContext->callToTransfer();
    bool canTransfer = _callsController->canTransferCall(callToTransfer)
                       && op.canTransferCall( callToTransfer );
    _transferCallAction->setVisible( canTransfer );
    _transferCallAction->setText(tr("Перевести звонок"));
    if( canTransfer && !addContactInfo(phoneInfoIdList, _transferCallAction) )
        _transferCallAction->setText(tr("Перевести звонок (%2)").arg(phone));

    //handle new mail action
    QString email = _op.data(Contact::Email).toString();
    _mailToAction->setVisible(!email.isEmpty());
    if(!email.isEmpty())
    {
        _mailToAction->setText(tr("Написать письмо")+email);
        if(!addContactInfo(QList<int>()<<Contact::Email,_mailToAction))
            _mailToAction->setText(tr("Написать (%1) ").arg(email));
    }

    //show menu
    QMenu::popup(QCursor::pos());
}

void ContactContextMenu::doMakeCall()
{
    QAction* action = qobject_cast<QAction*>(sender());
    Q_ASSERT(action);    
    QString phone = action->data().isValid()
                    ? action->data().toString()
                    : _op.data(Contact::Phone).toString();
    _callsController->makeCall(phone);
}

void ContactContextMenu::doTransfer()
{    
    QAction* action = qobject_cast<QAction*>(sender());
    Q_ASSERT(action);
    QString phone = action->data().isValid()
                    ? action->data().toString()
                    : _op.data(Contact::Phone).toString();
    Call callToTransfer = _appContext->callToTransfer();
    _callsController->transferToPhone(callToTransfer, phone);
}

void ContactContextMenu::doNewMail()
{
    QAction* action = qobject_cast<QAction*>(sender());
    Q_ASSERT(action);
    QString email = action->data().isValid()
                    ? action->data().toString()
                    : _op.data(Contact::Email).toString();
    QDesktopServices::openUrl("mailto:"+email);
}

bool ContactContextMenu::addContactInfo(const QList<int>& listInfoId, QAction* target)
{
    if(target->menu())
        delete target->menu();

    QMultiMap<QString, QString> nameValueMap;

    QMenu* menu = new QMenu(this);
    foreach(int infoId, listInfoId)
        foreach(QVariant v, _op.dataAsList(infoId))
            nameValueMap.insertMulti(_op.infoName(infoId), v.toString());

    if(nameValueMap.count()<=1)
        return false;

    QMapIterator<QString,QString> it(nameValueMap);
    while(it.hasNext())
    {
        it.next();
        QAction* a = menu->addAction(
                it.key() + ": " + it.value(),
                target, SLOT(trigger())
                );
        a->setData(it.value());
    }


    target->setMenu(menu);
    return true;
}

