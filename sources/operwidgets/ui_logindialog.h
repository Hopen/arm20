/********************************************************************************
** Form generated from reading UI file 'logindialog.ui'
**
** Created: Wed 18. Nov 16:00:27 2015
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINDIALOG_H
#define UI_LOGINDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include "langcombo.h"

QT_BEGIN_NAMESPACE

class Ui_LoginDialog
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *labelImage;
    QGridLayout *gridLayout;
    QLabel *lblUsername;
    QLineEdit *tbUsername;
    QLabel *lblPassword;
    QLineEdit *tbPassword;
    QCheckBox *cbAutoLogin;
    QCheckBox *cbNtlmAuth;
    QHBoxLayout *buttonsLayout;
    QPushButton *bnSettings;
    LangComboBoxWidget *widget;
    QDialogButtonBox *bnOkCancel;

    void setupUi(QDialog *LoginDialog)
    {
        if (LoginDialog->objectName().isEmpty())
            LoginDialog->setObjectName(QString::fromUtf8("LoginDialog"));
        LoginDialog->setWindowModality(Qt::WindowModal);
        LoginDialog->resize(400, 252);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(LoginDialog->sizePolicy().hasHeightForWidth());
        LoginDialog->setSizePolicy(sizePolicy);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/resources/icons/appicon_32x32.png"), QSize(), QIcon::Normal, QIcon::Off);
        LoginDialog->setWindowIcon(icon);
        LoginDialog->setStyleSheet(QString::fromUtf8("QDialog {\n"
"     background: white;\n"
"}\n"
"\n"
"QLineEdit, QPushButton {\n"
"     font-size:18px;\n"
"     font-weight: bold;\n"
"     border: 1px solid #c5dbec;\n"
"     border-radius: 5px;\n"
"     padding: 0 8px;\n"
"     background: #dfeffc;\n"
"     selection-background-color: orange;\n"
"     color: #2e6e9e;     \n"
" }\n"
"\n"
"QLineEdit[echoMode=\"2\"] {\n"
"    font-weight: regular;\n"
"    lineedit-password-character: 9679;\n"
" }\n"
"QLabel {\n"
"	color:black;\n"
"}\n"
"QCheckBox {\n"
"	font-size:12px;\n"
"}\n"
"\n"
"QPushButton {\n"
"    font-weight: normal;\n"
"    font-size: 14px;\n"
"	min-width:80px;\n"
"	min-height:25px;\n"
"}\n"
"\n"
"QPushButton:hover, QPushButton:focus {\n"
"   border: 1px solid #79b7e7; \n"
"   background: #d0e5f5; \n"
"   font-weight: bold; \n"
"   outline: none;\n"
"   color: #1d5987;\n"
"}\n"
"\n"
"QPushButton:disabled, QLineEdit:disabled {\n"
"   color: gray;\n"
"   border: 1px solid rgb(210, 210, 210);\n"
"   background: rgb(227, 227, 227); \n"
"}\n"
"\n"
"QLabel:"
                        "disabled, QCheckBox:disabled {\n"
"   color: gray;\n"
"}\n"
"\n"
"QStatusBar {\n"
"    font-size: 12px;\n"
"	max-height:20px;\n"
"}\n"
"\n"
"\n"
"\n"
""));
        LoginDialog->setSizeGripEnabled(false);
        LoginDialog->setModal(false);
        verticalLayout = new QVBoxLayout(LoginDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetFixedSize);
        labelImage = new QLabel(LoginDialog);
        labelImage->setObjectName(QString::fromUtf8("labelImage"));
        labelImage->setMaximumSize(QSize(400, 110));
        labelImage->setPixmap(QPixmap(QString::fromUtf8(":/images/resources/images/logo login_02.png")));
        labelImage->setScaledContents(true);

        verticalLayout->addWidget(labelImage);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        lblUsername = new QLabel(LoginDialog);
        lblUsername->setObjectName(QString::fromUtf8("lblUsername"));

        gridLayout->addWidget(lblUsername, 0, 0, 1, 1);

        tbUsername = new QLineEdit(LoginDialog);
        tbUsername->setObjectName(QString::fromUtf8("tbUsername"));

        gridLayout->addWidget(tbUsername, 0, 1, 1, 1);

        lblPassword = new QLabel(LoginDialog);
        lblPassword->setObjectName(QString::fromUtf8("lblPassword"));

        gridLayout->addWidget(lblPassword, 1, 0, 1, 1);

        tbPassword = new QLineEdit(LoginDialog);
        tbPassword->setObjectName(QString::fromUtf8("tbPassword"));
        tbPassword->setEnabled(true);
        tbPassword->setEchoMode(QLineEdit::Password);

        gridLayout->addWidget(tbPassword, 1, 1, 1, 1);

        cbAutoLogin = new QCheckBox(LoginDialog);
        cbAutoLogin->setObjectName(QString::fromUtf8("cbAutoLogin"));

        gridLayout->addWidget(cbAutoLogin, 2, 1, 1, 1);

        cbNtlmAuth = new QCheckBox(LoginDialog);
        cbNtlmAuth->setObjectName(QString::fromUtf8("cbNtlmAuth"));

        gridLayout->addWidget(cbNtlmAuth, 3, 1, 1, 1);


        verticalLayout->addLayout(gridLayout);

        buttonsLayout = new QHBoxLayout();
        buttonsLayout->setContentsMargins(0, 0, 0, 0);
        buttonsLayout->setObjectName(QString::fromUtf8("buttonsLayout"));
        bnSettings = new QPushButton(LoginDialog);
        bnSettings->setObjectName(QString::fromUtf8("bnSettings"));
        bnSettings->setEnabled(true);

        buttonsLayout->addWidget(bnSettings);

        widget = new LangComboBoxWidget(LoginDialog);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setMinimumSize(QSize(25, 25));
        widget->setMaximumSize(QSize(25, 25));

        buttonsLayout->addWidget(widget);

        bnOkCancel = new QDialogButtonBox(LoginDialog);
        bnOkCancel->setObjectName(QString::fromUtf8("bnOkCancel"));
        bnOkCancel->setEnabled(true);
        bnOkCancel->setOrientation(Qt::Horizontal);
        bnOkCancel->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        bnOkCancel->setCenterButtons(false);

        buttonsLayout->addWidget(bnOkCancel);


        verticalLayout->addLayout(buttonsLayout);

#ifndef QT_NO_SHORTCUT
#endif // QT_NO_SHORTCUT
        QWidget::setTabOrder(tbUsername, tbPassword);
        QWidget::setTabOrder(tbPassword, cbAutoLogin);
        QWidget::setTabOrder(cbAutoLogin, bnOkCancel);
        QWidget::setTabOrder(bnOkCancel, bnSettings);

        retranslateUi(LoginDialog);

        QMetaObject::connectSlotsByName(LoginDialog);
    } // setupUi

    void retranslateUi(QDialog *LoginDialog)
    {
        LoginDialog->setWindowTitle(QApplication::translate("LoginDialog", "\320\240\320\260\320\261\320\276\321\207\320\265\320\265 \320\274\320\265\321\201\321\202\320\276 \320\276\320\277\320\265\321\200\320\260\321\202\320\276\321\200\320\260", 0, QApplication::UnicodeUTF8));
        labelImage->setText(QString());
        lblUsername->setText(QApplication::translate("LoginDialog", "\320\230\320\274\321\217 \320\277\320\276\320\273\321\214\320\267\320\276\320\262\320\260\321\202\320\265\320\273\321\217", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_WHATSTHIS
        tbUsername->setWhatsThis(QString());
#endif // QT_NO_WHATSTHIS
        lblPassword->setText(QApplication::translate("LoginDialog", "\320\237\320\260\321\200\320\276\320\273\321\214", 0, QApplication::UnicodeUTF8));
        tbPassword->setInputMask(QString());
        tbPassword->setText(QString());
        cbAutoLogin->setText(QApplication::translate("LoginDialog", "\320\222\321\205\320\276\320\264\320\270\321\202\321\214 \320\260\320\262\321\202\320\276\320\274\320\260\321\202\320\270\321\207\320\265\321\201\320\272\320\270", 0, QApplication::UnicodeUTF8));
        cbNtlmAuth->setText(QApplication::translate("LoginDialog", "\320\240\320\265\320\266\320\270\320\274 \320\260\321\203\321\202\320\265\320\275\321\202\320\270\321\204\320\270\320\272\320\260\321\206\320\270\320\270 Windows", 0, QApplication::UnicodeUTF8));
        bnSettings->setText(QApplication::translate("LoginDialog", "\320\235\320\260\321\201\321\202\321\200\320\276\320\271\320\272\320\270", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class LoginDialog: public Ui_LoginDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINDIALOG_H
