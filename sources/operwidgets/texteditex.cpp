#include "texteditex.h"
#include <QMouseEvent>
#include <QDesktopServices>
#include <QUrl>
#include <QApplication>

TextEditEx::TextEditEx(QWidget *parent) :
    QTextEdit(parent)
{
    setMouseTracking(true);
}

void TextEditEx::mouseMoveEvent(QMouseEvent *e)
{    
    QTextEdit::mouseMoveEvent(e);
    QString link = this->anchorAt( e->pos() );
    if( !link.isEmpty() )
    {
        if(!qApp->overrideCursor() ||
           qApp->overrideCursor()->shape() != Qt::PointingHandCursor)
            qApp->setOverrideCursor(Qt::PointingHandCursor);
    }
    else
        qApp->restoreOverrideCursor();
}

void TextEditEx::mouseReleaseEvent(QMouseEvent *e)
{
    QTextEdit::mouseReleaseEvent(e);
    QString link = this->anchorAt( e->pos() );
    if( !link.isEmpty() )
    {
        QDesktopServices::openUrl(QUrl(link));
        qApp->restoreOverrideCursor();
    }
}


