/********************************************************************************
** Form generated from reading UI file 'optioneditor.ui'
**
** Created: Tue 26. Mar 13:54:36 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPTIONEDITOR_H
#define UI_OPTIONEDITOR_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_OptionEditor
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QLabel *label;
    QLineEdit *le_option;
    QHBoxLayout *hboxLayout1;
    QLabel *label_2;
    QComboBox *cb_typ;
    QLabel *label_3;
    QLineEdit *le_value;
    QLabel *lb_comment;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *OptionEditor)
    {
        if (OptionEditor->objectName().isEmpty())
            OptionEditor->setObjectName(QString::fromUtf8("OptionEditor"));
        OptionEditor->resize(324, 146);
        vboxLayout = new QVBoxLayout(OptionEditor);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        label = new QLabel(OptionEditor);
        label->setObjectName(QString::fromUtf8("label"));

        hboxLayout->addWidget(label);

        le_option = new QLineEdit(OptionEditor);
        le_option->setObjectName(QString::fromUtf8("le_option"));

        hboxLayout->addWidget(le_option);


        vboxLayout->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        label_2 = new QLabel(OptionEditor);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        hboxLayout1->addWidget(label_2);

        cb_typ = new QComboBox(OptionEditor);
        cb_typ->setObjectName(QString::fromUtf8("cb_typ"));

        hboxLayout1->addWidget(cb_typ);

        label_3 = new QLabel(OptionEditor);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        hboxLayout1->addWidget(label_3);

        le_value = new QLineEdit(OptionEditor);
        le_value->setObjectName(QString::fromUtf8("le_value"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(3), static_cast<QSizePolicy::Policy>(0));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(le_value->sizePolicy().hasHeightForWidth());
        le_value->setSizePolicy(sizePolicy);
        le_value->setMinimumSize(QSize(150, 0));

        hboxLayout1->addWidget(le_value);


        vboxLayout->addLayout(hboxLayout1);

        lb_comment = new QLabel(OptionEditor);
        lb_comment->setObjectName(QString::fromUtf8("lb_comment"));
        lb_comment->setTextFormat(Qt::PlainText);
        lb_comment->setWordWrap(true);
        lb_comment->setTextInteractionFlags(Qt::TextSelectableByMouse);

        vboxLayout->addWidget(lb_comment);

        buttonBox = new QDialogButtonBox(OptionEditor);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::NoButton|QDialogButtonBox::Ok);

        vboxLayout->addWidget(buttonBox);

#ifndef QT_NO_SHORTCUT
        label->setBuddy(le_option);
        label_2->setBuddy(le_value);
        label_3->setBuddy(le_value);
#endif // QT_NO_SHORTCUT

        retranslateUi(OptionEditor);
        QObject::connect(buttonBox, SIGNAL(rejected()), OptionEditor, SLOT(reject()));

        QMetaObject::connectSlotsByName(OptionEditor);
    } // setupUi

    void retranslateUi(QDialog *OptionEditor)
    {
        label->setText(QApplication::translate("OptionEditor", "Option:", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("OptionEditor", "Typ:", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("OptionEditor", "Value: ", 0, QApplication::UnicodeUTF8));
        lb_comment->setText(QString());
        Q_UNUSED(OptionEditor);
    } // retranslateUi

};

namespace Ui {
    class OptionEditor: public Ui_OptionEditor {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPTIONEDITOR_H
