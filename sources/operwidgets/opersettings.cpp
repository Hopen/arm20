#include "opersettings.h"
#include "appcore/operationcontroller.h"

using namespace ccappcore;

OperSettings::OperSettings(QObject *parent)
    : QObject(parent)
    , _showPrivateGroupsOnly(false)
    , _showCallFormImmediately(true)
    , _closeCallFormOnEndCall(false)
    //, _bDefaultOperTools(false)
{
    _add(":/resources/icons/new/call on.png",OperationString(OO_CALL),OO_CALL);
    _add(":/resources/icons/new/call end on.png",OperationString(OO_CALLEND),OO_CALLEND);
    //_add(":/resources/icons/new/call back on.png",OperationString(OO_CALLBACK),OO_CALLBACK);
    _add(":/resources/icons/new/waiting on.png",OperationString(OO_WAITING),OO_WAITING);
    _add(":/resources/icons/new/ivr on.png",OperationString(OO_IVR),OO_IVR);
    _add(":/resources/icons/new/transfer on.png",OperationString(OO_TRANSFER),OO_TRANSFER);
    _add(":/resources/icons/new/voice mail on.png",OperationString(OO_VOICEMAIL),OO_VOICEMAIL);
    _add(":/resources/icons/new/login on.png",OperationString(OO_LOGIN),OO_LOGIN);
    _add(":/resources/icons/new/rec on.png", OperationString(OO_REC), OO_REC);
    _add(":/resources/icons/new/forward 32.png", OperationString(OO_TRANSFER_FORWARD), OO_TRANSFER_FORWARD);
    _add(":/resources/icons/new/forward group 32.png", OperationString(OO_TRANSFER_GROUP), OO_TRANSFER_GROUP);
    _add(":/resources/icons/new/call back on.png",OperationString(OO_TRANSFER_PHONE),OO_TRANSFER_PHONE);

    registerMetaTypes();

}

//namespace testnamespace
//{
//struct Test23
//{
//public:
//    Test23(){}
//    Test23(const int& a, const int& b, const QString& s)
//        :a1(a),a2(b),s1(s)
//    {}

//private:
//    int a1;
//    int a2;
//    QString s1;

//    friend QDataStream &operator<<(QDataStream &out, const Test23 &myObj);
//    friend QDataStream &operator>>(QDataStream &in, Test23 &myObj);
//};

//inline QDataStream &operator<<(QDataStream &out, const Test23 &myObj)
//{
//    out << myObj.a1
//           << myObj.a2
//              << myObj.s1;
//    return out;
//}

//inline QDataStream &operator>>(QDataStream &in, Test23 &myObj)
//{
//    in >> myObj.a1
//    >> myObj.a2
//    >> myObj.s1;
//    return in;
//}

//}Q_DECLARE_METATYPE(testnamespace::Test23)

void OperSettings::applySettings(const AppSettings& settings)
{
    _showCallFormImmediately = settings.getOption("callform/showImmediately").toBool();
    _closeCallFormOnEndCall = settings.getOption("callform/closeOnEndCall").toBool();
    _subjectList = settings.getOption("callform/subjects").toStringList();

    QVariant var = settings.getOption("opertoolsform/operations", QVariant(QVariant::Invalid));
    if (var.type() == QVariant::Invalid)
        //_bDefaultOperTools = true;
        _operationList = _mainOperationList;
    else
        _operationList = settings.getOption("opertoolsform/operations").toList();

    //QVariantList list = settings.getOption("test/test2").toList();

    _showPrivateGroupsOnly = settings.getOption("view/contacts.showPrivateGroupsOnly").toBool();
}

void OperSettings::flushSettings(AppSettings& settings)
{
    settings.setUserOption("callform/showImmediately", _showCallFormImmediately);
    settings.setUserOption("callform/closeOnEndCall", _closeCallFormOnEndCall);
    settings.setUserOption("callform/subjects",QVariant(_subjectList));
    settings.setUserOption("opertoolsform/operations",QVariant(_operationList));

//    qRegisterMetaType<testnamespace::Test23>();
//    qRegisterMetaTypeStreamOperators<testnamespace::Test23>();

//    QVariantList list;
//    testnamespace::Test23 a1(1,2,tr("123"));
//    testnamespace::Test23 a2(5,7,tr("asd"));
//    QVariant var1, var2;
//    var1.setValue<testnamespace::Test23>(a1);
//    var2.setValue<testnamespace::Test23>(a2);
//    list.push_back(var1);
//    list.push_back(var2);
//    QVariant Commonlist(list);
//    QVariantList list2 = Commonlist.toList();

//    settings.setUserOption("test/test2",QVariant(list2));

    settings.setUserOption("view/contacts.showPrivateGroupsOnly",_showPrivateGroupsOnly);
}

void OperSettings::_add(const QString &icon, const QString& text, const quint8& opernum)
{
    OperOperation op(text,icon,opernum);
    QVariant var;
    var.setValue<ccappcore::OperOperation>(op);
    //var << op;
    _mainOperationList.push_back(var);
}

QString OperSettings::OperationString(const quint8& operNum)const
{
    switch(operNum)
    {
        case OO_CALL:
            return QObject::tr("Начать разговор");
        case OO_CALLEND:
            return QObject::tr("Повесить трубку");
//        case OO_CALLBACK:
//            return QObject::tr("Перезвонить");
        case OO_WAITING:
            return QObject::tr("Ожидание");
        case OO_IVR:
            return QObject::tr("Перевести на IVR");
        case OO_TRANSFER:
            return QObject::tr("Перевести звонок");
        case OO_VOICEMAIL:
            return QObject::tr("Голосовая почта");
        case OO_LOGIN:
            return QObject::tr("Перезайти");
        case OO_REC:
            return QObject::tr("Запись");
        case OO_TRANSFER_FORWARD:
            return QObject::tr("Перевести на оператора");
        case OO_TRANSFER_GROUP:
            return QObject::tr("Перевести на группу");
        case OO_TRANSFER_PHONE:
            return QObject::tr("Перевести на номер");

        default:
            return QObject::tr("Неизвестно");
    }
}

void OperSettings::registerMetaTypes()const
{
    qRegisterMetaType<ccappcore::OperOperation>();
    qRegisterMetaTypeStreamOperators<ccappcore::OperOperation>();
}

//void OperSettings::on_OperOperationChanged()
//{
//    emit OperOperationChanged();
//}

