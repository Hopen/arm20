#include "operatorcontactsmenu.h"

OperatorContactsMenu::OperatorContactsMenu(ContactsModel& model, QWidget *parent)
    : QMenu(parent)
    , _model(model)
    , _operListController(model.serviceProvider())
{
    _makeCallAction = addAction(QIcon(":/resources/icons/dial_32x32.png"),
                                tr("Позвонить"),
                                this, SLOT(makeCall_triggered()));
}


void OperatorContactsMenu::show(ccappcore::Operator& op)
{
    _makeCallAction->setEnabled(_callsController->canMakeCall(op));
}

void OperatorContactsMenu::makeCall_triggered()
{
    TSpecificFunctor<CallsController,ccappcore::Operator>
            makeCallAction(_callsController,&CallsController::makeCall);

    _model.applyAction(_model.selectionModel()->currentIndex(), makeCallAction);
}
