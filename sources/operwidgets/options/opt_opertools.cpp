#include "opt_opertools.h"
#include "ui_opt_opertools.h"
//#include "appcore/operationcontroller.h"
enum E_ITEMSDATA
{
    ID_ICON = Qt::UserRole,
    ID_TEXT,
    ID_OPERNUM
};

OptOperToolForm::OptOperToolForm(QWidget* parent) : QWidget(parent)
{
    setupUi(this);
    listBaseTools->setIconSize(QSize(24, 24));
    listOperTools->setIconSize(QSize(24, 24));
    listBaseTools->sortItems(Qt::DescendingOrder);
}

void OptOperToolForm::addOperTool(const QString &icon, const QString& text, const quint8& opernum, QListWidget* list)
{
    QListWidgetItem *toolItem = new QListWidgetItem(QIcon(icon),text);
    toolItem->setData(ID_ICON, icon);
    toolItem->setData(ID_TEXT, text);
    toolItem->setData(ID_OPERNUM, opernum);
    toolItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable
                        | Qt::ItemIsDragEnabled);
    list->addItem(toolItem);
}

void OptOperToolForm::removeOperTool(QListWidget *list)
{
    delete list->takeItem(list->currentRow());
}

void OptOperToolForm::removeAllOperTool(QListWidget *list)
{
    list->clear();
}

void OptOperToolForm::moveOperTool(QListWidget *from, QListWidget *to)
{
    if (QListWidgetItem *toolItem = from->takeItem(from->currentRow()))
        to->insertItem(to->currentRow(),toolItem);
}

void OptOperToolForm::moveAllOperTool(QListWidget *from, QListWidget *to)
{
    from->setCurrentRow(0);
    while (QListWidgetItem *toolItem = from->takeItem(from->currentRow()))
    {
        to->insertItem(to->currentRow(),toolItem);
    }
}

bool OptOperToolForm::StandOnOperation(const quint8& operNum, QListWidget *where)const
{
    for (int i=0;i<where->count();++i)
    {
        QListWidgetItem *toolItem = where->item(i);
        if (toolItem->data(ID_OPERNUM) == operNum)
        {
            where->setCurrentRow(i);
            return true;
        }
    }
    return false;
}

void OptOperToolForm::changeEvent(QEvent *e)
{
    switch (e->type()) {
    case QEvent::LanguageChange:
        this->retranslateUi(this);
        break;
    default:
        break;
    }
}

//----------------------------------------------------
OptionsTabOperToolForm::OptionsTabOperToolForm(QObject *parent)
    : OptionsTab( parent,
                "options.opertoolform",
                "",
                tr("Элементы управление"),
                tr("Настройка элементов управления"),
                ":/resources/icons/call_form_16x16.png" )
    , _ui(new OptOperToolForm()), bCreated(false)
{
    //connect(_ui->listSubjects, SIGNAL(currentTextChanged(QString)),
    //        this, SIGNAL(dataChanged()));

    connect(_ui->listBaseTools, SIGNAL(currentTextChanged(QString)),
            this, SIGNAL(dataChanged()));
    connect(_ui->listOperTools, SIGNAL(currentTextChanged(QString)),
            this, SIGNAL(dataChanged()));

   //RestoreOperations();


//    _ui->addOperTool(":/resources/icons/new/call on.png", tr("Сделать звонок"), 1, _ui->listBaseTools);
//    _ui->addOperTool(":/resources/icons/new/call end on.png", tr("Повесить трубку"), 1, _ui->listBaseTools);
//    _ui->addOperTool(":/resources/icons/new/call back on.png", tr("Перезвонить"), 1, _ui->listBaseTools);
//    _ui->addOperTool(":/resources/icons/new/waiting on.png", tr("Ожидание"), 1, _ui->listBaseTools);
//    _ui->addOperTool(":/resources/icons/new/ivr on.png", tr("Перевести на IVR"), 1, _ui->listBaseTools);
//    _ui->addOperTool(":/resources/icons/new/transfer on.png", tr("Перевести звонок"), 1, _ui->listBaseTools);
//    _ui->addOperTool(":/resources/icons/new/voice mail on.png", tr("Голосовая почта"), 1, _ui->listBaseTools);
//    _ui->addOperTool(":/resources/icons/new/login on.png", tr("Зайти"), 1, _ui->listOperTools);


    connect(_ui->toolBtnToOper   , SIGNAL(clicked()), this, SLOT(on_btnToOperClick   ()));
    connect(_ui->toolBtnToBase   , SIGNAL(clicked()), this, SLOT(on_btnToBaseClick   ()));
    connect(_ui->toolBtnToOperAll, SIGNAL(clicked()), this, SLOT(on_btnToOperAllClick()));
    connect(_ui->toolBtnToBaseAll, SIGNAL(clicked()), this, SLOT(on_btnToBaseAllClick()));

    connect(this,SIGNAL(Changed()), _operSettings.instance(),SIGNAL(OperOperationChanged()));
    //connect(this,SIGNAL(Changed()), _operSettings.instance(),SLOT(on_OperOperationChanged()));
}

OptionsTabOperToolForm::~OptionsTabOperToolForm()
{
    delete _ui;
}

QWidget* OptionsTabOperToolForm::widget()
{
    return _ui;
}

void OptionsTabOperToolForm::RestoreOperations()
{
//    OperationController::OperationList *applOperList = _operationController->GetApplOperationList();
//    for (OperationController::OperationList::const_iterator cit=applOperList->begin();cit!=applOperList->end();++cit)
//    {
//        ccappcore::OperOperation op = cit->value<ccappcore::OperOperation>();
//        _ui->addOperTool(op.iconPath(),op.text(),op.operNum(),_ui->listBaseTools);
//    }

//    OperationController::OperationList *userOperList = _operationController->GetUserOperationList();
//    for (OperationController::OperationList::const_iterator cit=userOperList->begin();cit!=userOperList->end();++cit)
//    {
//        ccappcore::OperOperation op = cit->value<ccappcore::OperOperation>();
//        if (op.operNum() < OO_USER)
//        {
//            if (_ui->StandOnOperation(op.operNum(), _ui->listBaseTools))
//                on_btnToOperClick(); // move operation form base list to oper list
//        }
//        else
//            _ui->addOperTool(op.iconPath(),op.text(),op.operNum(),_ui->listBaseTools);
//    }

    bCreated = true;

    // clear lists
    _ui->removeAllOperTool(_ui->listBaseTools);
    _ui->removeAllOperTool(_ui->listOperTools);

//    if (_operSettings->isDefaultOperTools())
//    {
//        QVariantList operlist = _operSettings->mainOperationList();
//        for (QVariantList::const_iterator cit=operlist.begin();cit!=operlist.end();++cit)
//        {
//            ccappcore::OperOperation op = cit->value<ccappcore::OperOperation>();
//           _ui->addOperTool(op.iconPath(),op.text(),op.operNum(),_ui->listOperTools);
//        }
//    }
//    else
//    {
        QVariantList operlist = _operSettings->mainOperationList();
        for (QVariantList::const_iterator cit=operlist.begin();cit!=operlist.end();++cit)
        {
            ccappcore::OperOperation op = cit->value<ccappcore::OperOperation>();
            //ccappcore::OperOperation op; *cit >> op;
            QString name = QObject::tr(op.text().toUtf8());
           _ui->addOperTool(op.iconPath(),name,op.operNum(),_ui->listBaseTools);
        }

        operlist = _operSettings->operationList();
        for (QVariantList::const_iterator cit=operlist.begin();cit!=operlist.end();++cit)
        {
            ccappcore::OperOperation op = cit->value<ccappcore::OperOperation>();
            //ccappcore::OperOperation op; *cit >> op;
            if (op.operNum() < OO_USER)
            {
                if (_ui->StandOnOperation(op.operNum(), _ui->listBaseTools))
                    //on_btnToOperClick(); // move operation form base list to oper list
                    _ui->removeOperTool(_ui->listBaseTools);
            }
            //else
            _ui->addOperTool(op.iconPath(),op.text(),op.operNum(),_ui->listOperTools);
        }

    //}


}

void OptionsTabOperToolForm::applyOptions()
{
    //_operSettings->setShowCallFormImmediately(_ui->showCallForm());
//    _operSettings->setCloseCallFormOnEndCall(_ui->closeOnEndCall());
    //_operSettings->setSubjectList(_ui->subjectList());
    //QList<QListWidgetItem *> items = _ui->listOperTools->findItems(QString(tr("*")), Qt::MatchRegExp);

    if (!bCreated)
        return;

    QVariantList operOperationList;
//    for(QList<QListWidgetItem *>::const_iterator cit=items.begin();cit!=items.end();++cit)
//    {
//        OperOperation op(((QListWidgetItem*)(*cit))->data(ID_ICON).toChar(),
//                ((QListWidgetItem*)(*cit))->data(ID_TEXT).toChar(),
//                ((QListWidgetItem*)(*cit))->data(ID_OPERNUM).toUInt()
//        );

//        QVariant var;
//        var.setValue<ccappcore::OperOperation>(op);
//        operOperationList.push_back(var);

////        _operationController->addUserOperation(
////                    ((QListWidgetItem*)(*cit))->data(ID_ICON).toChar(),
////                    ((QListWidgetItem*)(*cit))->data(ID_TEXT).toChar(),
////                    ((QListWidgetItem*)(*cit))->data(ID_OPERNUM).toUInt()
////                    );
//    }
//    //OperationController::OperationList *userOperList = _operationController->GetUserOperationList();
//    //_operSettings->setOperationList(*userOperList);
    for (int i=0;i<_ui->listOperTools->count();++i)
    {
        QListWidgetItem *toolItem = _ui->listOperTools->item(i);

        OperOperation op(toolItem->data(ID_TEXT).toString(),
                         toolItem->data(ID_ICON).toString(),
                         toolItem->data(ID_OPERNUM).toUInt()
                      );
        QVariant var;
        var.setValue<ccappcore::OperOperation>(op);
        operOperationList.push_back(var);
    }

    _operSettings->setOperationList(operOperationList);
    _operSettings->flushSettings(*_settings);

    emit Changed();
}

void OptionsTabOperToolForm::restoreOptions()
{
//    _ui->setSubjectList(_operSettings->subjectList());
//    _ui->showCallForm(_operSettings->showCallFormImmediately());
//    _ui->closeOnEndCall(_operSettings->closeCallFormOnEndCall());
    RestoreOperations();
}


void OptionsTabOperToolForm::on_btnToOperClick()
{
    _ui->moveOperTool(_ui->listBaseTools, _ui->listOperTools);
}

void OptionsTabOperToolForm::on_btnToBaseClick()
{
    _ui->moveOperTool(_ui->listOperTools, _ui->listBaseTools);
}

void OptionsTabOperToolForm::on_btnToOperAllClick()
{
    _ui->moveAllOperTool(_ui->listBaseTools, _ui->listOperTools);
}

void OptionsTabOperToolForm::on_btnToBaseAllClick()
{
    _ui->moveAllOperTool(_ui->listOperTools, _ui->listBaseTools);
}

//opt_opertools::opt_opertools(QWidget *parent) :
//    QWidget(parent),
//    ui(new Ui::opt_opertools)
//{
//    ui->setupUi(this);
//}

//opt_opertools::~opt_opertools()
//{
//    delete ui;
//}
