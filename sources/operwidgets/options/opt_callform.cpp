#include "opt_callform.h"

#include <QInputDialog>

#include "appcore/appsettings.h"
#include "appcore/lateboundobject.h"

using namespace ccappcore;

OptionsTabCallForm::OptionsTabCallForm(QObject *parent)
    : OptionsTab( parent,
                "options.callform",
                "",
                tr("Карточка звонка"),
                tr("Свойства карточки звонка"),
                ":/resources/icons/call_form_16x16.png" )
    , _ui(new OptCallForm())
    , _bWidgetInitialized(false)
{
    connect(_ui->listSubjects, SIGNAL(currentTextChanged(QString)),
            this, SIGNAL(dataChanged()));

    connect(_ui->bnAddSubject, SIGNAL(clicked()),
            this, SIGNAL(dataChanged()));
}

OptionsTabCallForm::~OptionsTabCallForm()
{
    delete _ui;
}

QWidget* OptionsTabCallForm::widget()
{
    return _ui;
}

void OptionsTabCallForm::applyOptions()
{
    if (!_bWidgetInitialized) // save nothing
        return;
    _operSettings->setShowCallFormImmediately(_ui->showCallForm());
    _operSettings->setCloseCallFormOnEndCall(_ui->closeOnEndCall());
    _operSettings->setSubjectList(_ui->subjectList());
    _operSettings->flushSettings(*_settings);
}

void OptionsTabCallForm::restoreOptions()
{
    _bWidgetInitialized = true;
    _ui->setSubjectList(_operSettings->subjectList());
    _ui->showCallForm(_operSettings->showCallFormImmediately());
    _ui->closeOnEndCall(_operSettings->closeCallFormOnEndCall());
}

QStringList OptCallForm::subjectList() const
{
  QStringList r; r.clear();
  for(int i=0; i<listSubjects->count(); i++)
      r.append(listSubjects->item(i)->text());
  return r;
}

void OptCallForm::setSubjectList(const QStringList& values)
{
    foreach(QString s, values)
    {
        QListWidgetItem* i = new QListWidgetItem();
        i->setFlags(Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        i->setText(s);
        listSubjects->addItem(i);
    }
}


void OptCallForm::on_bnAddSubject_clicked()
{
    QListWidgetItem* i = new QListWidgetItem();
    i->setFlags(Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    listSubjects->addItem(i);
    listSubjects->editItem(i);
}

void OptCallForm::on_listSubjects_itemSelectionChanged()
{
    bnRemoveSubject->setEnabled(listSubjects->currentRow()>=0);
}

void OptCallForm::on_bnRemoveSubject_clicked()
{
    delete listSubjects->currentItem();
}

bool OptCallForm::showCallForm() const
{
    return cbShowCallForm->isChecked();
}

void OptCallForm::showCallForm(bool value)
{
    cbShowCallForm->setChecked(value);
}

bool OptCallForm::closeOnEndCall() const
{
    return cbCloseOnEndCall->isChecked();
}

void OptCallForm::closeOnEndCall(bool value)
{
    cbCloseOnEndCall->setChecked(value);
}

//void OptCallForm::changeEvent(QEvent *e)
//{
//    switch (e->type()) {
//    case QEvent::LanguageChange:
//        this->retranslateUi(this);
//        break;
//    default:
//        break;
//    }
//}
