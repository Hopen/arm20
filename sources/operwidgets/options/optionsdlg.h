/*
 * optionsdlg.cpp
 * Copyright (C) 2003-2009  Michail Pishchagin
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifndef OPTIONSDLG_H
#define OPTIONSDLG_H

#include "ui_ui_options.h"
#include <QDialog>

#include "appcore/serviceprovider.h"
#include "appcore/lateboundobject.h"
#include "appcore/sessioncontroller.h"

using namespace ccappcore;

class OptionsDlg : public QDialog, public Ui::OptionsUI
{
	Q_OBJECT
public:
        OptionsDlg(QWidget *parent = 0);
	~OptionsDlg();

	void openTab(const QString& id);

signals:
	void applyOptions();

private slots:
	void doOk();
	void doApply();
    void doLoadCDSSettings();
    void doSaveCDSSettings();

public:
	class Private;
private:
	Private *d;
	friend class Private;

	QPushButton* pb_apply;
    QPushButton* pb_save;
    QPushButton* pb_load;
};

class OptionsTab;

class OptionsDlg::Private : public QObject
{
        Q_OBJECT
public:
        Private(OptionsDlg *d);

public slots:
        void doApply();
        void openTab(QString id);
        void doLoadCDSSettings();
        void doSaveCDSSettings();
        void on_loadSettingsCompleted();

private slots:
        void currentItemChanged(QListWidgetItem* current, QListWidgetItem* previous);
        void dataChanged();
        void noDirtySlot(bool);
        void createTabs();
        void createChangedMap();

        //void addWidgetChangedSignal(QString widgetName, QCString signal);
        void connectDataChanged(QWidget *);

public:
        OptionsDlg *dlg;
        bool dirty, noDirty;
        QHash<QString, QWidget*> id2widget;
        QList<OptionsTab*> tabs;

        QMap<QString, QByteArray> changedMap;
        LateBoundObject<SessionController> _sessionController;
        LateBoundObject<AppSettings      > _settings;
        QFutureWatcher<void> _loadSetiingsFuture;
};
#endif
