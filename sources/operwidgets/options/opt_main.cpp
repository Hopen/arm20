#include "opt_main.h"
#include "ui_opt_main.h"

using namespace ccappcore;

//OptionsTabMainWidget::OptionsTabMainWidget(QWidget *parent) :
//    QWidget(parent),
//    ui(new Ui::OptionsTabMainWidget)
//{
//    ui->setupUi(this);
//}

////void OptionsTabMainWidget::changeEvent(QEvent *e)
////{
////    switch (e->type()) {
////    case QEvent::LanguageChange:
////        ui->retranslateUi(this);
////        break;
////    default:
////        break;
////    }
////}

//OptionsTabMainWidget::~OptionsTabMainWidget()
//{
//    delete ui;
//}

class OptionsTabMainWidgetUI : public QWidget, public Ui::OptionsTabMainWidget
{
public:
    OptionsTabMainWidgetUI() : QWidget() { setupUi(this); }
};

OptionsTabMain::OptionsTabMain(QObject* parent)
    : OptionsTab( parent,
                  "mainoptions", "",
                  tr("Общие"),
                  tr("Общие настройки приложения"),
                  ":/resources/icons/gears_16x16.png" )
{
    _widget = 0;
}

QWidget *OptionsTabMain::widget()
{
    if(!_widget)
        _widget = new OptionsTabMainWidgetUI();
    return _widget;
}

void OptionsTabMain::applyOptions()
{
    if(!_widget)
        return;

    OptionsTabMainWidgetUI *d = (OptionsTabMainWidgetUI *)_widget;

#ifdef Q_OS_UNIX
#else
    _settings->setAppOption("update/auto",d->cbAutoUpdate->isChecked());
#endif

    _settings->setAppOption("calls/noAnswerTimeout",d->eWaitForAnswer->text());
    _settings->setAppOption("calls/responseTimeout",d->eAutoTransfer->text());
    _settings->setAppOption("messages/spamMessageTimeout",d->eWaitForClick->text());

    _settings->setAppOption("logging/fileName", d->tbLogFile->text());
    _settings->setAppOption("logging/logLevel", d->comboBoxLogLevel->currentText());

}

void OptionsTabMain::restoreOptions()
{
    if(!_widget)
        return;

    OptionsTabMainWidgetUI *d = (OptionsTabMainWidgetUI *)_widget;

#ifdef Q_OS_UNIX
    d->cbAutoUpdate->setChecked(false);
    d->cbAutoUpdate->setEnabled(false);
#else
    d->cbAutoUpdate->setChecked(_settings->getOption("update/auto").toBool());
#endif

    d->eWaitForAnswer->setText ( _settings->getOption("calls/noAnswerTimeout").toString());
    d->eAutoTransfer->setText ( _settings->getOption("calls/responseTimeout").toString());
    d->eWaitForClick->setText ( _settings->getOption("messages/spamMessageTimeout").toString());

    d->tbLogFile->setText( _settings->getOption("logging/fileName").toString() );
    int index = d->comboBoxLogLevel->findText(
            _settings->getOption("logging/logLevel").toString() );
    if(index>=0 && index<d->comboBoxLogLevel->count())
        d->comboBoxLogLevel->setCurrentIndex(index);


}
