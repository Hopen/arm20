#include "opt_operstatus.h"

using namespace ccappcore;

enum ePauseType
{
    PT_LOCAL = 0,
    PT_GLOBAL
};

const int PAUSEREASONROLE = 1234;

OptionsTabStatusWidget::OptionsTabStatusWidget(QWidget* parent)
    : QWidget(parent)
{
    setupUi(this);

    bnRemoveStatus->setEnabled(false);
}

QStringList OptionsTabStatusWidget::pauseReasons() const
{
    QStringList pauseReasons;
    for(int row = 0; row < listStatusReason->count(); ++row)
    {
        QString reason = listStatusReason->item(row)->text();
        pauseReasons.append(reason);
    }
    return pauseReasons;
}

void OptionsTabStatusWidget::setPauseReasons(const PauseReasonContainer& pauseReasons)
{
    listStatusReason->clear();
    foreach(const TPauseReason& reason, pauseReasons)
    {
        QListWidgetItem* i = new QListWidgetItem();
        i->setFlags(reason.pauseType == PT_LOCAL? Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled : Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        i->setText(reason.pauseName);
        i->setData(PAUSEREASONROLE, reason.pauseType);
        listStatusReason->addItem(i);
    }
}

void OptionsTabStatusWidget::on_bnAddStatus_clicked()
{
    QListWidgetItem* i = new QListWidgetItem();
    i->setFlags(Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    i->setData(PAUSEREASONROLE, PT_LOCAL);
    listStatusReason->addItem(i);
    listStatusReason->editItem(i);
}

void OptionsTabStatusWidget::on_bnRemoveStatus_clicked()
{
   delete listStatusReason->currentItem();
}

void OptionsTabStatusWidget::on_listStatusReason_itemSelectionChanged()
{
    bnRemoveStatus->setEnabled( listStatusReason->currentRow()>=0 && listStatusReason->currentItem()->data(PAUSEREASONROLE) == PT_LOCAL);
}

//void OptionsTabStatusWidget::changeEvent(QEvent *e)
//{
//    switch (e->type()) {
//    case QEvent::LanguageChange:
//        this->retranslateUi(this);
//        break;
//    default:
//        break;
//    }
//}

//----------------------------------------------------

OptionsTabOperStatus::OptionsTabOperStatus(QObject *parent)
    : OptionsTab(parent,
                 "status", "", tr("Статус"), tr("Статус операторов"),
                 ":/resources/icons/oper_free_16x16.png" )
    , w(NULL)
{
}

OptionsTabOperStatus::~OptionsTabOperStatus()
{
    if(w)
        delete w;
}

QWidget* OptionsTabOperStatus::widget()
{
    if(w)
        return w;

    w = new OptionsTabStatusWidget();

    connect(w->listStatusReason, SIGNAL(currentTextChanged(QString)),
            this, SIGNAL(dataChanged()));

    connect(w->bnAddStatus, SIGNAL(clicked()),
        this, SIGNAL(dataChanged()));


    return w;
}

void OptionsTabOperStatus::applyOptions()
{
    if(!w)
        return;

    QStringList pauseReasonsList = w->pauseReasons();
    QStringList pauseGlobalList  = _statusOptions->pauseGlobalReasons();
    QStringList newPauseReasons;
    foreach (const QString& reason, pauseReasonsList)
    {
        if(!pauseGlobalList.contains(reason))
            newPauseReasons.push_back(reason);
    }

    _statusOptions->setPauseReasons( newPauseReasons/*w->pauseReasons()*/ );
    _statusOptions->setPauseAfterLogon( !w->cbDisablePauseOnLogon->isChecked() );
    _statusOptions->setPauseWhenDialing( w->cbPauseWhenDialing->isChecked() );
    _statusOptions->flushSettings( *_settings );
}

void OptionsTabOperStatus::restoreOptions()
{
    if(!w)
        return;

    PauseReasonContainer pauseReasonsList;

    if (_statusOptions->isEnableOtherPauseReasons())
    {
        foreach(const QString& reason, _statusOptions->pauseReasons())
        {
            pauseReasonsList.push_back(TPauseReason(reason, PT_LOCAL ));
        }
    }

    foreach(const QString& reason, _statusOptions->pauseGlobalReasons())
    {
        pauseReasonsList.push_back(TPauseReason(reason, PT_GLOBAL));
    }


    //QStringList pauseReasonsList = _statusOptions->pauseReasons() + _statusOptions->pauseGlobalReasons();
    w->setPauseReasons(pauseReasonsList);
    w->cbDisablePauseOnLogon->setChecked( !_statusOptions->pauseAfterLogon() );
    w->cbPauseWhenDialing->setChecked( _statusOptions->pauseWhenDialing() );
    w->bnAddStatus->setEnabled(_statusOptions->isEnableOtherPauseReasons());
}

