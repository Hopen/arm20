#include "optionstab.h"

#include <qtabwidget.h>
#include <qlayout.h>
#include <qmap.h>
#include <QVBoxLayout>

//----------------------------------------------------------------------------
// OptionsTab
//----------------------------------------------------------------------------

OptionsTab::OptionsTab(QObject *parent, const char *name)
: QObject(parent)
{
	setObjectName(name);
}

OptionsTab::OptionsTab(QObject *parent, QByteArray _id, QByteArray _parentId, QString _name, QString _desc, QString _tabIconName, QString _iconName)
: QObject(parent)
{
	setObjectName(_name);
	v_id = _id;
	v_parentId = _parentId;
	v_name = _name;
	v_desc = _desc;
	v_tabIconName = _tabIconName;
	v_iconName = _iconName;
        _tabIcon = QIcon(v_tabIconName);
}

OptionsTab::~OptionsTab()
{
}

QByteArray OptionsTab::id() const
{
	return v_id;
}

QByteArray OptionsTab::parentId() const
{
	return v_parentId;
}

QString OptionsTab::tabName() const
{
	return v_name;
}

const QIcon& OptionsTab::tabIcon() const
{
        return _tabIcon;
}

QString OptionsTab::name() const
{
	return v_name;
}

QString OptionsTab::desc() const
{
	return v_desc;
}

void OptionsTab::applyOptions()
{
}

void OptionsTab::restoreOptions()
{
}

void OptionsTab::tabAdded(OptionsTab *)
{
}

bool OptionsTab::stretchable() const
{
	return false;
}

//----------------------------------------------------------------------------
// OptionsTabWidget
//----------------------------------------------------------------------------

class OptionsTabWidget : public QTabWidget
{
	Q_OBJECT
public:
	OptionsTabWidget(QWidget *parent);
	void addTab(OptionsTab *);
	void restoreOptions();

signals:
	void connectDataChanged(QWidget *);
	void noDirty(bool);

private slots:
	void updateCurrent(QWidget *);

private:
	struct TabData {
		TabData() { tab = 0; initialized = false; }
		TabData(OptionsTab *t) { tab = t; initialized = false; }
		OptionsTab *tab;
		bool initialized;
	};
	QMap<QWidget *, TabData> w2tab;
};

OptionsTabWidget::OptionsTabWidget(QWidget *parent)
: QTabWidget(parent)
{
	connect(this, SIGNAL(currentChanged(QWidget *)), SLOT(updateCurrent(QWidget *)));
}

void OptionsTabWidget::addTab(OptionsTab *tab)
{
	if ( tab->tabName().isEmpty() )
		return; // skip the dummy tabs

	// the widget will have no parent; it will be reparented
	// when inserting it with "addTab"
	QWidget *w = new QWidget(0);
	w->setObjectName(tab->name());

	if ( !tab->desc().isEmpty() )
		setTabToolTip(indexOf(w), tab->desc());

	w2tab[w] = TabData(tab);
	
        QTabWidget::addTab(w, tab->tabIcon(), tab->tabName());

	//FIXME: this is safe for our current use of addTab, but may
	//be inconvenient in the future (Qt circa 4.2 had a bug which stopped
	//setCurrentIndex(0); from working)
	setCurrentIndex(1);
	setCurrentIndex(0);
}

void OptionsTabWidget::updateCurrent(QWidget *w)
{
	if ( !w2tab[w].initialized ) {
		QVBoxLayout *vbox = new QVBoxLayout(w);
		vbox->setMargin(5);
		OptionsTab *opttab = w2tab[w].tab;

		QWidget *tab = opttab->widget();
		if ( !tab )
			return;

		tab->setParent(w);
		vbox->addWidget(tab);
		if ( !opttab->stretchable() )
			vbox->addStretch();

		emit noDirty(true);
		opttab->restoreOptions();
		emit noDirty(false);
		
		emit connectDataChanged(tab);

		tab->show();
		w2tab[w].initialized = true;
	}
}

void OptionsTabWidget::restoreOptions()
{
	emit noDirty(true);
	w2tab[currentWidget()].tab->restoreOptions();
	emit noDirty(false);
}

#include "optionstab.moc"
