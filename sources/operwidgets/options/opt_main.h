#ifndef OPT_MAIN_H
#define OPT_MAIN_H


#include "optionstab.h"

//namespace Ui {
//class OptionsTabMainWidget;
//}

//class OptionsTabMainWidget : public QWidget
//{
//    Q_OBJECT
    
//public:
//    explicit OptionsTabMainWidget(QWidget *parent = 0);
//    ~OptionsTabMainWidget();

////protected:
//    //void changeEvent(QEvent *e);



//public:
//    Ui::OptionsTabMainWidget *ui;
//};

class OptionsTabMain : public OptionsTab
{
    Q_OBJECT
public:
    OptionsTabMain(QObject* parent = 0);
    virtual QWidget *widget();

    virtual void applyOptions();
    virtual void restoreOptions();

private:
    //QPointer<OptionsTabMainWidget> _widget;
    QWidget * _widget;

};

#endif // OPT_MAIN_H
