#include "opt_layout.h"
#include "ui_opt_layout.h"

LayoutOptionsWidget::LayoutOptionsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LayoutOptionsWidget)
{
    ui->setupUi(this);
}

LayoutOptionsWidget::~LayoutOptionsWidget()
{
    delete ui;
}

void LayoutOptionsWidget::changeEvent(QEvent *e)
{
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}


LayoutOptionsTab::LayoutOptionsTab(QObject* parent)
    : OptionsTab( parent, "layoutoptions", "", tr("Внешний вид"),
                  tr("Настройки внешнего вида и расположения элементов пользовательского интерфейса"),
                  ":/resources/icons/layoutoptions_32x32.png" )
{

}

QWidget *LayoutOptionsTab::widget()
{
    if(_widget.isNull())
        _widget = new LayoutOptionsWidget();
    return _widget;
}

void LayoutOptionsTab::applyOptions()
{
    if(!_widget)
        return;
    _layoutManager->lockLayout(_widget->ui->cbLockLayout->isChecked());

//    _settings->setAppOption("logging/fileName", _widget->ui->tbLogFile->text());
//    _settings->setAppOption("logging/logLevel", _widget->ui->comboBoxLogLevel->currentText());

}

void LayoutOptionsTab::restoreOptions()
{
    if(!_widget)
        return;

    _widget->ui->cbLockLayout->setChecked(_layoutManager->isLayoutLocked());

//    _widget->ui->tbLogFile->setText( _settings->getOption("logging/fileName").toString() );
//    int index = _widget->ui->comboBoxLogLevel->findText(
//            _settings->getOption("logging/logLevel").toString() );
//    if(index>=0 && index<_widget->ui->comboBoxLogLevel->count())
//        _widget->ui->comboBoxLogLevel->setCurrentIndex(index);

}
