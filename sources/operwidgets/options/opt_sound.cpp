#include "opt_sound.h"
#include "ui_opt_sound.h"

using namespace ccappcore;

class OptSoundUI : public QWidget, public Ui::OptSound
{
public:
        OptSoundUI() : QWidget() { setupUi(this); }
};

//----------------------------------------------------------------------------
// OptionsTabSound
//----------------------------------------------------------------------------

OptionsTabSound::OptionsTabSound(QObject *parent)
    : OptionsTab(parent, "sound", "",
             tr("Звуки"),
             tr("Настройка звуков для событий в системе"),
             ":/resources/icons/sounds_16x16.png")
    , w(0)
{

}

OptionsTabSound::~OptionsTabSound()
{
    if(w)
        delete w;
}

QWidget *OptionsTabSound::widget()
{
    if ( w )
                return w;

    w = new OptSoundUI();

        int row = w->gridLayout->rowCount();
        foreach(QPointer<SoundEvent> ev, _soundEvents->allEvents())
        {
            if(!ev.isNull())
                addSoundEvent(row++, ev.data());
        }

        QSpacerItem* verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
        w->gridLayout->addItem(verticalSpacer, row, 0, 1, 1);

        restoreOptions();

    return w;
}

void OptionsTabSound::addSoundEvent(int row, SoundEvent *ev)
{
    QLabel* lblSoundEvent = new QLabel(w);
    lblSoundEvent->setObjectName(ev->name());
    QString desc = QObject::tr(ev->description().toUtf8());
    lblSoundEvent->setText(/*ev->description()*/desc);
    w->gridLayout->addWidget(lblSoundEvent, row, 0, 1, 1);

    QLineEdit* tbSoundFile = new QLineEdit(w);
    tbSoundFile->setObjectName(ev->name());
    tbSoundFile->setText(ev->file());
    w->gridLayout->addWidget(tbSoundFile, row, 1, 1, 1);

    QToolButton* bnOpenFile = new QToolButton(w);
    bnOpenFile->setObjectName(ev->name());
    bnOpenFile->setIcon( QIcon(":/resources/icons/file_browse_16x16.png") );
    w->gridLayout->addWidget(bnOpenFile, row, 2, 1, 1);
    connect(bnOpenFile, SIGNAL(clicked()), SLOT(chooseSoundEvent()));

    QToolButton* bnPlaySound = new QToolButton(w);
    bnPlaySound->setObjectName(ev->name());
    bnPlaySound->setIcon( QIcon(":/resources/icons/sounds_play_16x16.png") );
    w->gridLayout->addWidget(bnPlaySound, row, 3, 1, 1);
    connect(bnPlaySound, SIGNAL(clicked()),SLOT(previewSoundEvent()));
}

void OptionsTabSound::applyOptions()
{
    if ( !w )
		return;

        _soundEvents->setSoundsEnabled(w->checkBox->isChecked());
        foreach(QPointer<SoundEvent> ev, _soundEvents->allEvents())
        {
            if(!ev.isNull())
            {
                QLineEdit* leFileName = w->findChild<QLineEdit*>(ev->name());
                ev->setFile(leFileName->text());
            }
        }	
        _soundEvents->flushSettings(*_settings);
}

void OptionsTabSound::restoreOptions()
{
    if ( !w )
		return;

        w->checkBox->setChecked(_soundEvents->soundsEnabled());
        foreach(QPointer<SoundEvent> ev, _soundEvents->allEvents())
        {
            if(!ev.isNull())
            {
                QLineEdit* leFileName = w->findChild<QLineEdit*>(ev->name());
                leFileName->setText(ev->file());
            }
        }
}

void OptionsTabSound::chooseSoundEvent()
{
    QLineEdit* leFileName = w->findChild<QLineEdit*>(sender()->objectName());
    if( !leFileName )
        return;

    QString path = leFileName->text();
        QString str = QFileDialog::getOpenFileName( w,
                                      tr("Выберите звуковой файл"),
                                      path,
                                      tr("Звуковый файлы (*.wav)") );
	if (!str.isEmpty()) {
            leFileName->setText(str);
            emit dataChanged();
	}
}

void OptionsTabSound::previewSoundEvent()
{
    QLineEdit* leFileName = w->findChild<QLineEdit*>(sender()->objectName());
    if( !leFileName )
        return;

    QString fileName = leFileName->text();
    if(!fileName.isEmpty())
        QSound::play(fileName);
}
