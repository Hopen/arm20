#ifndef OPT_OPERTOOLS_H
#define OPT_OPERTOOLS_H

//#include <QWidget>
#include "ui_opt_opertools.h"
#include "optionstab.h"
#include "opersettings.h"
#include "appcore/operationcontroller.h"

using namespace ccappcore;


class OptOperToolForm
        : public QWidget
        , public Ui::OptionsTabOperToolForm
{
    Q_OBJECT

protected:
    void changeEvent(QEvent *e);
public:
    OptOperToolForm(QWidget* parent = 0);
    void addOperTool   (const QString& icon, const QString& text, const quint8& opernum, QListWidget* list);
    void removeOperTool(QListWidget* list);
    void removeAllOperTool(QListWidget *list);
    void moveOperTool(QListWidget *from, QListWidget *to);
    void moveAllOperTool(QListWidget *from, QListWidget *to);
    bool StandOnOperation(const quint8& operNum, QListWidget *where)const;

};

class OptionsTabOperToolForm : public OptionsTab {
    Q_OBJECT
public:
    OptionsTabOperToolForm(QObject *parent = 0);
    ~OptionsTabOperToolForm();

    virtual QWidget* widget();

public:
    virtual void applyOptions();
    virtual void restoreOptions();

private:
    void RestoreOperations();

private slots:
    void on_btnToOperClick();
    void on_btnToBaseClick();
    void on_btnToOperAllClick();
    void on_btnToBaseAllClick();

signals:
    void Changed();

private:
    OptOperToolForm *_ui;
    LateBoundObject<OperSettings> _operSettings;
    bool bCreated;
    //LateBoundObject<OperationController> _operationController;
};

//namespace Ui {
//    class opt_opertools;
//}

//class opt_opertools : public QWidget
//{
//    Q_OBJECT

//public:
//    explicit opt_opertools(QWidget *parent = 0);
//    ~opt_opertools();

//private:
//    Ui::opt_opertools *ui;
//};

#endif // OPT_OPERTOOLS_H
