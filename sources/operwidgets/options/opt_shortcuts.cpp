/*
 * opt_shortcuts.cpp - an OptionsTab for setting the Keyboard Shortcuts of Psi
 * Copyright (C) 2006 Cestonaro Thilo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "opt_shortcuts.h"

#include <QMessageBox>

#include "appcore/shortcutmanager.h"
#include "grepshortcutkeydialog/grepshortcutkeydialog.h"

#include "ui_opt_shortcuts.h"

#include "appcore/appsettings.h"
#include "appcore/lateboundobject.h"
using namespace ccappcore;


#define SHORTCUTNAME    Qt::UserRole + 1

/**
 * \class the Ui for the Options Tab Shortcuts
 */
class OptShortcutsUI : public QWidget, public Ui::OptShortcuts
{
public:
	OptShortcutsUI() : QWidget() { setupUi(this); }
};

//----------------------------------------------------------------------------
// OptionsTabShortcuts
//----------------------------------------------------------------------------

/**
 * \brief Constructor of the Options Tab Shortcuts Class
 */
OptionsTabShortcuts::OptionsTabShortcuts(QObject *parent)
    : OptionsTab(parent,
                 "shortcuts", "",
                 tr("Сочетания клавиш"),
                 tr("Сочетания клавиш для быстрого вызова часто исп. функций"),
                 ":/resources/icons/shortcuts_16x16.png")
{
	w = 0;
}

/**
 * \brief Destructor of the Options Tab Shortcuts Class
 */
OptionsTabShortcuts::~OptionsTabShortcuts()
{
}

/**
 * \brief widget, creates the Options Tab Shortcuts Widget
 * \return QWidget*, points to the previously created widget
 */
QWidget *OptionsTabShortcuts::widget()
{
	if ( w )
		return 0;

	w = new OptShortcutsUI();
	OptShortcutsUI *d = (OptShortcutsUI *)w;
	
	d->treeShortcuts->setColumnWidth(0, 320);	

	d->remove->setEnabled(false);
	d->edit->setEnabled(false);

	connect(d->treeShortcuts, SIGNAL(itemSelectionChanged()), this, SLOT(onItemSelectionChanged()));
	connect(d->treeShortcuts, SIGNAL(itemDoubleClicked(QTreeWidgetItem *, int)), this, SLOT(onItemDoubleClicked(QTreeWidgetItem *, int)));
	connect(d->remove, SIGNAL(clicked()), this, SLOT(onRemove()));
	connect(d->edit, SIGNAL(clicked()), this, SLOT(onEdit()));
	return w;
}

/**
 * \brief	applyOptions, if options have changed, they will be applied by calling this function
 * \param	opt, unused, totally ignored
 */
void OptionsTabShortcuts::applyOptions() {
	if ( !w )
		return;

	OptShortcutsUI *d = (OptShortcutsUI *)w;

        LateBoundObject<ShortcutManager> shortcutsManager;

        int shortcutItemsCount = d->treeShortcuts->topLevelItemCount();
	int keyItemsCount;
        /* step through the Shortcut Items */
        for(int shortcutItemIndex = 0; shortcutItemIndex < shortcutItemsCount; shortcutItemIndex++)
        {
            QTreeWidgetItem* shortcutItem = d->treeShortcuts->topLevelItem(shortcutItemIndex);
            keyItemsCount = shortcutItem->childCount();
            /* get the Options Path of the Shortcut Item */
            QString shortcutName = shortcutItem->data(0, SHORTCUTNAME).toString();
            QKeySequence key = QKeySequence(shortcutItem->text(1));
            shortcutsManager->setKey(shortcutName, key);
        }

        shortcutsManager->flushSettings(*_settings);
}

/**
 * \brief	restoreOptions, reads in the currently set options
 */
void OptionsTabShortcuts::restoreOptions()
{
	if ( !w )
		return;
	

        readShortcuts();
}

/**
 * \brief	readShortcuts, reads shortcuts from given PsiOptions instance
 */
void OptionsTabShortcuts::readShortcuts()
{
	OptShortcutsUI *d = (OptShortcutsUI *)w;

        LateBoundObject<ShortcutManager> shortcutsManager;

        QStringList allNames = shortcutsManager->allNames();
	/* step through the toplevel items */
        foreach(const QString shortcutName, allNames)
        {
            QString comment = shortcutsManager->description(shortcutName);
            if (comment.isNull())
              comment = shortcutName;
            QKeySequence key = shortcutsManager->key(shortcutName);

            /* create the TreeWidgetItem and set the Data the Kind and it's Optionspath and append it */
            QTreeWidgetItem *shortcutItem = new QTreeWidgetItem();
            shortcutItem->setText(0, comment);
            shortcutItem->setData(0, SHORTCUTNAME, QVariant(shortcutName));
            shortcutItem->setText(1, key.toString(QKeySequence::NativeText));
            d->treeShortcuts->addTopLevelItem(shortcutItem);
	}
}

/**
 * \brief Button Edit pressed, edits the currently selected item if it is a key
 */
void OptionsTabShortcuts::onEdit() {
        OptShortcutsUI *d = (OptShortcutsUI *)w;
        QList<QTreeWidgetItem *> selectedItems = d->treeShortcuts->selectedItems();

        if(selectedItems.count() == 0)
                return;

        grep();
}

/**
 * \brief Button Remove pressed, removes the currently selected item, if it is a Keyitem
 */
void OptionsTabShortcuts::onRemove() {
	OptShortcutsUI *d = (OptShortcutsUI *)w;
	QList<QTreeWidgetItem *> selectedItems = d->treeShortcuts->selectedItems();

	if(selectedItems.count() == 0)
		return;

        QTreeWidgetItem	*keyItem = selectedItems[0];
        keyItem->setText(1,"");

        emit dataChanged();
}

/**
 * \brief Opens grep dialog.
 */
void OptionsTabShortcuts::grep()
{    
        GrepShortcutKeyDialog* grep = new GrepShortcutKeyDialog();
        connect(grep, SIGNAL(newShortcutKey(const QKeySequence&)), this, SLOT(onNewShortcutKey(const QKeySequence&)));
        grep->show();
}

/**
 * \brief	in the treeview, the selected item has changed, the add and remove buttons are
 * 			enabled or disabled, depening on the selected item type
 */
void OptionsTabShortcuts::onItemSelectionChanged() {
	OptShortcutsUI *d = (OptShortcutsUI *)w;
	QList<QTreeWidgetItem *> selectedItems = d->treeShortcuts->selectedItems();

        /* zero selected Item(s), so we can't add or remove anything, disable the buttons */
        d->remove->setEnabled(selectedItems.count() != 0);
        d->edit->setEnabled(selectedItems.count() != 0);
}

/**
 * \brief	an item of the treeview is double clicked, if it is a Keyitem, the GrepShortcutKeyDialog is shown
 */
void OptionsTabShortcuts::onItemDoubleClicked(QTreeWidgetItem *item, int column)
{
	Q_UNUSED(column);

	if (!item)
		return;

        grep();
}

/**
 * \brief	slot onNewShortcutKey is called when the grepShortcutKeyDialog has captured a KeySequence,
 *			so the new KeySequence can be set to the KeyItem
 * \param	the new KeySequence for the keyitem
 */
void OptionsTabShortcuts::onNewShortcutKey(const QKeySequence& key) {
	OptShortcutsUI *d = (OptShortcutsUI *)w;
	QTreeWidgetItem	*keyItem;
	QList<QTreeWidgetItem *> selectedItems = d->treeShortcuts->selectedItems();
	if(selectedItems.count() == 0)
		return;

	keyItem = selectedItems[0];
        keyItem->setText(1, key.toString(QKeySequence::NativeText));
        emit dataChanged();
}

/**
 * \brief	Translate the \param comment in the "Shortcuts" translation Context
 * \param	comment the text to be translated
 */
