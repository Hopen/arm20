#ifndef OPT_APPLICATION_H
#define OPT_APPLICATION_H

#include "optionstab.h"
#include "appcore/serviceprovider.h"
#include "appcore/sessioncontroller.h"

using ccappcore::ServiceProvider;

class QWidget;
class QButtonGroup;

class OptionsTabApplication : public OptionsTab
{
	Q_OBJECT
public:
        OptionsTabApplication(QObject *parent);
	~OptionsTabApplication();

	QWidget *widget();
	void applyOptions();
	void restoreOptions();

        //static QString detectValidIpAddress();

public slots:
        void onConnectionTypeChanged(const int& index);
        void onAutoIpChanged();

private:
	QWidget *w;

        LateBoundObject<SessionController> session;
};

#endif
