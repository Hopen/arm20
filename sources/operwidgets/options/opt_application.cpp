#include "opt_application.h"

#include <qcheckbox.h>
#include <qcombobox.h>
#include <qlineedit.h>
#include <QList>
//#include <QNetworkInterface>
#include <QtAlgorithms>

#include "ui_opt_application.h"

#include "appcore/lateboundobject.h"
#include "appcore/appsettings.h"
#include "appcore/irouterclient.h"

using namespace ccappcore;

class OptApplicationUI : public QWidget, public Ui::OptApplication
{
public:
	OptApplicationUI() : QWidget() { setupUi(this); }

protected:
    //void changeEvent(QEvent *e);
};

//void OptApplicationUI::changeEvent(QEvent *e)
//{
//    switch (e->type()) {
//    case QEvent::LanguageChange:
//        this->retranslateUi(this);
//        break;
//    default:
//        break;
//    }
//}

//----------------------------------------------------------------------------
// OptionsTabApplication
//----------------------------------------------------------------------------

OptionsTabApplication::OptionsTabApplication(QObject *parent)
    : OptionsTab( parent, "application", "",
                 tr("Подключение"),
                 tr("Настройки подключения к серверу"),
                 ":/resources/icons/gear_connection_16x16.png")
{
	w = 0;
}

OptionsTabApplication::~OptionsTabApplication()
{
}

QWidget *OptionsTabApplication::widget()
{
	if ( w )
		return 0;

	w = new OptApplicationUI();
        OptApplicationUI *d = (OptApplicationUI *)w;

        connect(d->cbConnectionType, SIGNAL(currentIndexChanged(int)),
                this, SLOT(onConnectionTypeChanged(int)));

        connect(d->chkbAutoIP, SIGNAL(clicked()),
                this, SLOT(onAutoIpChanged()));


        int index = 0;
        index  = _settings->getOption("connecting/type", index).toInt();
        onConnectionTypeChanged(index);

	return w;
}

void OptionsTabApplication::applyOptions()
{
	if ( !w )
		return;

	OptApplicationUI *d = (OptApplicationUI *)w;

        _settings->setAppOption("device/route", d->tbTrunk->value() );
        _settings->setAppOption("device/number", d->tbPhone->text() );

        _settings->setAppOption("router/hostName", d->tbServer->text());
        _settings->setAppOption("router/port", d->tbPort->value());

//        _settings->setAppOption("logging/fileName", d->tbLogFile->text());
//        _settings->setAppOption("logging/logLevel", d->comboBoxLogLevel->currentText());

        _settings->setAppOption("jabber/xmppAddress", d->tbXmppAddress->text());
        _settings->setAppOption("jabber/xmppPassword", d->tbXmppPassword->text());

        _settings->setAppOption("connecting/type", d->cbConnectionType->currentIndex());
        _settings->setAppOption("connecting/autoIP", d->chkbAutoIP->isChecked());

        if (CT_VOIP == d->cbConnectionType->currentIndex())
        {
            session->setDeviceNumber( d->tbPhone->text() );
            _settings->setAppOption("voip/RegistrarDomain", d->tbRegistarDomain->text());
        }

}

void OptionsTabApplication::restoreOptions()
{
	if ( !w )
		return;

	OptApplicationUI *d = (OptApplicationUI *)w;

        d->tbTrunk->setValue( _settings->getOption("device/route").toInt() );
        d->tbPhone->setText( _settings->getOption("device/number").toString() );

        d->tbServer->setText( _settings->getOption("router/hostName").toString() );
        d->tbPort->setValue( _settings->getOption("router/port").toInt() );

//        d->tbLogFile->setText( _settings->getOption("logging/fileName").toString() );
//        int index = d->comboBoxLogLevel->findText(
//                _settings->getOption("logging/logLevel").toString() );
//        if(index>=0 && index<d->comboBoxLogLevel->count())
//            d->comboBoxLogLevel->setCurrentIndex(index);

        int index = _settings->getOption("connecting/type").toInt();
        if(index>=0 && index<d->cbConnectionType->count())
            d->cbConnectionType->setCurrentIndex(index);

        d->chkbAutoIP->setChecked(_settings->getOption("connecting/autoIP").toBool());

//        if (d->chkbAutoIP->isChecked())
//        {
//            session->setDeviceNumber( _settings->getOption("router/hostName").toString() );
//        }

        d->tbXmppAddress->setText( _settings->getOption("jabber/xmppAddress").toString() );
        d->tbXmppPassword->setText( _settings->getOption("jabber/xmppPassword").toString() );

        d->tbRegistarDomain->setText(_settings->getOption("voip/RegistrarDomain", QString()).toString());

        bool isChecked = d->chkbAutoIP->isChecked();
        d->tbPhone->setEnabled(!isChecked);

}

void OptionsTabApplication::onConnectionTypeChanged(const int &index)
{
    if ( !w )
            return;

    OptApplicationUI *d = (OptApplicationUI *)w;

    if(index != ccappcore::CT_VOIP)
    {
        d->chkbAutoIP->setChecked(false);
        d->groupRegistarDomain->setChecked(false);
    }
    d->chkbAutoIP->setEnabled(index == ccappcore::CT_VOIP);
    d->tbPhone->setEnabled(true);
    d->groupRegistarDomain->setEnabled(index == ccappcore::CT_VOIP);
}

void OptionsTabApplication::onAutoIpChanged()
{
    if ( !w )
            return;

    OptApplicationUI *d = (OptApplicationUI *)w;

    if (d->chkbAutoIP->isChecked())
    {
        QString ip = ccappcore::SessionController::detectValidIpAddress();
        QString addr = "TA:" + ip;
        //appSettings->setAppOption( "device/number", addr );
        d->tbPhone->setText(addr);
    }

    bool isChecked = d->chkbAutoIP->isChecked();
    d->tbPhone->setEnabled(!isChecked);
}

//static bool sortByIndex(const QNetworkInterface& i1, const QNetworkInterface& i2)
//{
//    return i1.index()<i2.index();
//}
//static bool sortByMask(const QNetworkAddressEntry& i1, const QNetworkAddressEntry& i2)
//{
//    return i1.netmask().toIPv4Address()<i2.netmask().toIPv4Address();
//}


//QString OptionsTabApplication::detectValidIpAddress()
//{
//    QList<QHostAddress>	allAddresses = QNetworkInterface::allAddresses();
////    if( !_ipAddress.isEmpty() && allAddresses.contains(QHostAddress(_ipAddress)))
////        return _ipAddress;

////    _ipAddress = QString();
//    QString ipAddress;

//    QList<QNetworkInterface> allInterfaces = QNetworkInterface::allInterfaces();
//    qSort(allInterfaces.begin(),allInterfaces.end(),&sortByIndex);
//    QNetworkInterface netInterface;
//    qDebug()<<"VoIP: Detecting IP address. Network interface list:";
//    foreach( QNetworkInterface curr, allInterfaces )
//    {
//        qDebug()<<curr;
//        bool isUp= curr.flags().testFlag(QNetworkInterface::IsUp);
//        bool isLoopback = curr.flags().testFlag(QNetworkInterface::IsLoopBack);
//        bool hasEntries = curr.addressEntries().count()>0;
//        if( isUp && !isLoopback && hasEntries)
//        {
//            if(!netInterface.isValid())
//                netInterface = curr;
//        }
//    }
//    qDebug()<<"VoIP: Selected network interface:"<<netInterface;

//    QList<QNetworkAddressEntry> entryList = netInterface.addressEntries();
//    qSort(entryList.begin(),entryList.end(),&sortByMask);
//    QList<QString> addrList;
//    foreach(QNetworkAddressEntry addr, entryList)
//    {
//        if( addr.ip().protocol() == QAbstractSocket::IPv4Protocol )
//        {
//            addrList.append(addr.ip().toString());
//        }
//    }

//    if(addrList.count()>0)
//        ipAddress = addrList[0];

//    if(ipAddress.isEmpty() && !allAddresses.isEmpty())
//        ipAddress = allAddresses[0].toString();
//    return ipAddress;
//}
