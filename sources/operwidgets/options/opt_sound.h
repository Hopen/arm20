#ifndef OPT_SOUND_H
#define OPT_SOUND_H

#include "operwidgets.h"

#include "appcore/lateboundobject.h"
#include "appcore/soundeventsmanager.h"

#include "optionstab.h"


using namespace ccappcore;
namespace ccappcore
{
class SoundEvent;
}

class OptSoundUI;

class OptionsTabSound : public OptionsTab
{
	Q_OBJECT
public:
        OptionsTabSound(QObject *parent);
	~OptionsTabSound();

	QWidget *widget();
	void applyOptions();
	void restoreOptions();

private slots:
        void chooseSoundEvent();
        void previewSoundEvent();

private:
        LateBoundObject<SoundEventsManager> _soundEvents;
        void addSoundEvent(int row, SoundEvent*);
        OptSoundUI *w;
};

#endif
