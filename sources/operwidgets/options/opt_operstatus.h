#ifndef OPT_OPERSTATUS_H
#define OPT_OPERSTATUS_H

#include "options/optionstab.h"
#include "appcore/statusoptions.h"
#include "appcore/lateboundobject.h"
#include "ui_opt_operstatus.h"

using namespace ccappcore;

struct TPauseReason
{
    TPauseReason(const QString& _name, const int& _type):
        pauseName(_name),
        pauseType(_type)
    {

    }

    QString pauseName;
    int     pauseType;
};

typedef QList <TPauseReason> PauseReasonContainer;

class OptionsTabStatusWidget
    : public QWidget
    , public Ui::OptionsTabOperStatus
{
    Q_OBJECT
public:
    OptionsTabStatusWidget(QWidget* parent = 0);

    QStringList pauseReasons() const;
    void setPauseReasons(const PauseReasonContainer&);

//protected:
//    void changeEvent(QEvent *e);

private slots:
    void on_listStatusReason_itemSelectionChanged();
    void on_bnRemoveStatus_clicked();
    void on_bnAddStatus_clicked();
};

class OptionsTabOperStatus : public OptionsTab {
    Q_OBJECT
public:
    OptionsTabOperStatus(QObject *parent = 0);
    ~OptionsTabOperStatus();


public://OptionTab

    virtual QWidget* widget();
    virtual void applyOptions();
    virtual void restoreOptions();

private:
    LateBoundObject<StatusOptions> _statusOptions;
    OptionsTabStatusWidget *w;
};


#endif // OPT_OPERSTATUS_H
