#ifndef OPT_LAYOUT_H
#define OPT_LAYOUT_H

#include "appcore/layoutmanager.h"
#include "optionstab.h"

namespace Ui {
    class LayoutOptionsWidget;
}

class LayoutOptionsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit LayoutOptionsWidget(QWidget *parent = 0);
    ~LayoutOptionsWidget();

    Ui::LayoutOptionsWidget *ui;

protected:
    void changeEvent(QEvent *e);
};

class LayoutOptionsTab : public OptionsTab
{
    Q_OBJECT
public:
    LayoutOptionsTab(QObject* parent = 0);
    virtual QWidget *widget();

    virtual void applyOptions();
    virtual void restoreOptions();

private:
    QPointer<LayoutOptionsWidget> _widget;
    LateBoundObject<LayoutManager> _layoutManager;
};



#endif // OPT_LAYOUT_H
