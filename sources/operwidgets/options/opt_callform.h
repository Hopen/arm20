#ifndef OPT_CALLFORM_H
#define OPT_CALLFORM_H

#include "optionstab.h"
#include "ui_opt_callform.h"
#include "opersettings.h"

using namespace ccappcore;

class OptCallForm : public QWidget, public Ui::OptionsTabCallForm
{
    Q_OBJECT;
public:
    OptCallForm() : QWidget() { setupUi(this); }

    QStringList subjectList() const;
    void setSubjectList(const QStringList&);

    bool showCallForm() const;
    void showCallForm(bool value);

    bool closeOnEndCall() const;
    void closeOnEndCall(bool value);

//protected:
//    void changeEvent(QEvent *e);

private slots:
    void on_bnRemoveSubject_clicked();
    void on_listSubjects_itemSelectionChanged();
    void on_bnAddSubject_clicked();
};

class OptionsTabCallForm : public OptionsTab {
    Q_OBJECT
public:
    OptionsTabCallForm(QObject *parent = 0);
    ~OptionsTabCallForm();

    virtual QWidget* widget();

public:
    virtual void applyOptions();
    virtual void restoreOptions();

private:
    OptCallForm *_ui;
    LateBoundObject<OperSettings> _operSettings;

    bool _bWidgetInitialized;
};

#endif // OPT_CALLFORM_H
