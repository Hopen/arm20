INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD
HEADERS += optionsdlg.h \
    optionstab.h \
    options/opt_application.h \
    options/opt_shortcuts.h \
    options/opt_callform.h \
    options/opt_sound.h \
    options/opt_operstatus.h \
    options/opt_layout.h \
    options/opt_opertools.h \
    options/opt_main.h
SOURCES += optionstab.cpp \
    optionsdlg.cpp \
    options/opt_application.cpp \
    options/opt_shortcuts.cpp \
    options/opt_callform.cpp \
    options/opt_sound.cpp \
    options/opt_operstatus.cpp \
    options/opt_layout.cpp \
    options/opt_opertools.cpp \
    options/opt_main.cpp
FORMS += ui_options.ui \
    options/opt_application.ui \
    options/opt_shortcuts.ui \
    optioneditor.ui \
    options/opt_callform.ui \
    options/opt_sound.ui \
    options/opt_operstatus.ui \
    options/opt_layout.ui \
    options/opt_opertools.ui \
    options/opt_main.ui





