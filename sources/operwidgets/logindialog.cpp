#include "logindialog.h"

#include <QMessageBox>

#include "ui_logindialog.h"

#include "options/optionsdlg.h"
#include "appcore/irouterclient.h"
#include "mainwindow.h"

using namespace ccappcore;

LoginDialog::LoginDialog(QWidget *parent)
        : QDialog(parent)
        , _ui(new Ui::LoginDialog)
{
    _ui->setupUi(this);

    _statusBar = new QStatusBar(this);
    _statusBar->setSizeGripEnabled(false);
   _ui->verticalLayout->addWidget(_statusBar);

    connect(_ui->bnOkCancel, SIGNAL(accepted()), this, SLOT(login()));
    connect(_ui->bnOkCancel, SIGNAL(rejected()), this, SLOT(close()));

    connect(_status.instance(), SIGNAL(info(QString)),
            this, SLOT(statusInfo(QString)));

    connect(_session.instance(), SIGNAL(sessionError(SessionController::SessionErrorCode,QString)),
            this, SLOT(sessionStartingFailed(SessionController::SessionErrorCode,QString)));

    connect(_session.instance(), SIGNAL(sessionStarted()),
            this, SLOT(sessionStarted()));

    connect(_ui->widget, SIGNAL(changeLanguage(QString)),
            this, SIGNAL(updateLanguage(QString)));

//    connect(_ui->widget, SIGNAL(changeLanguage(QString)),
//            this, SLOT(onChangeLanguage(QString)));

}

//void LoginDialog::onChangeLanguage(QString text)
//{
//    emit updateLanguage(text);
//}

void LoginDialog::showEvent(QShowEvent *ev)
{   
    updateControls(true);

    QDialog::showEvent(ev);
}

LoginDialog::~LoginDialog()
{    
    delete _ui;
}

void LoginDialog::changeEvent(QEvent *e)
{
    switch (e->type()) {
    case QEvent::LanguageChange:
        _ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void LoginDialog::statusInfo(const QString& message)
{
    updateControls(false);
    _statusBar->showMessage(message);
}

void LoginDialog::sessionStarted()
{
    _session->disconnect(this);
    MainWindow* mainWnd = new MainWindow();
    mainWnd->setAttribute(Qt::WA_DeleteOnClose);
    mainWnd->show();

//    MainWindow2* mainWnd = new MainWindow2();
//    mainWnd->setAttribute(Qt::WA_DeleteOnClose);
//    mainWnd->show();

    close();
}

void LoginDialog::sessionStartingFailed(SessionController::SessionErrorCode reason,
                              const QString& error)
{
    _statusBar->showMessage(error);
    if(!isVisible())
        show();

    updateControls(true);
    if(reason == SessionController::LoginCanceled)
        return;

    bool alreadyLoggedIn = SessionController::AlreadyLoggedIn == reason;
    bool forcedLoginEnabled = _session->isForcedLoginEnabled();
    if( alreadyLoggedIn && !forcedLoginEnabled  )
    {
        int ret = QMessageBox::warning(this,
                             tr("Ошибка входа в систему."),
                             tr("Оператор с таким именем или параметрами телефонного подключения "\
                                "уже зарегистрирован в системе и будет отключен. "\
                                "Все равно войти?"),
                             QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes);

        if(ret == QMessageBox::Yes)
        {
            _session->setForcedLoginEnabled(true);
            _session->startSession();
        }
        return;
    }

    QMessageBox::warning(this,
                         tr("Ошибка входа в систему."),
                         error,
                         QMessageBox::Ok,
                         QMessageBox::Ok);
}

void LoginDialog::login()
{
    _session->setLogin(_ui->tbUsername->text());
    _session->setPassword(_ui->tbPassword->text());
    _session->setAutoLoginEnabled(_ui->cbAutoLogin->isChecked());
    _session->setForcedLoginEnabled(false);
    _session->setLanguage(_ui->widget->text());
    _session->startSession();
}

void LoginDialog::updateControls(bool enable)
{
    bool ntlmEnabled = _ntlm.isValid() && _session->isNtlmLoginEnabled();
    _ui->cbNtlmAuth->setVisible( _ntlm.isValid() );
    _ui->cbNtlmAuth->setEnabled( enable );
    _ui->tbPassword->setEnabled( enable && !ntlmEnabled );
    _ui->tbUsername->setEnabled( enable && !ntlmEnabled );
    _ui->bnOkCancel->setEnabled( enable );
    _ui->bnSettings->setEnabled( enable );
    _ui->cbAutoLogin->setEnabled( enable );

    QString loginId = ntlmEnabled ? _ntlm->queryUserName() : _session->login();
    _ui->tbUsername->setText(loginId);
    _ui->cbAutoLogin->setChecked(_session->isAutoLoginEnabled());
    _ui->cbNtlmAuth->setChecked(ntlmEnabled);
    if(!ntlmEnabled && _session->login().length()>0)
        _ui->tbPassword->setFocus();

    _ui->widget->setText(_session->getLanguage());

}

void LoginDialog::on_bnSettings_clicked()
{
    _session->stopSession();
    OptionsDlg d;
    connect (&d, SIGNAL(applyOptions()), this, SIGNAL(updateLanguage()));
    d.exec();
    updateControls(true);
}

void LoginDialog::on_cbNtlmAuth_toggled(bool checked)
{
    _session->setNtlmLoginEnabled(checked);
    updateControls(true);
}

void LoginDialog::on_cbAutoLogin_toggled(bool checked)
{
    _session->setAutoLoginEnabled(checked);
}
