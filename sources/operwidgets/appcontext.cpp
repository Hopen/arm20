#include "appcontext.h"

using namespace ccappcore;

AppContext::AppContext(QObject *parent)
    : QObject(parent)/*, _langId(0)*/
{
    connect(_callsController.instance(),SIGNAL(callEnded(ccappcore::Call)),
            this, SLOT(callEnded(ccappcore::Call)));
}

void AppContext::showCallForm(ccappcore::Call c)
{
    emit onShowCallForm(c);
}

void AppContext::showChatForm(ccappcore::Contact c)
{
    emit onShowChatForm(c);
}

void AppContext::showDialer(QString phone)
{
    //emit onShowDialer(phone);
}

void AppContext::toggleVisibility()
{
    emit onToggleVisibility();
}

void AppContext::onActivateForegroundWindow()
{
    emit activateForegroundWindow();
}

void AppContext::setCallToTransfer(ccappcore::Call c)
{
    if(c!=_callToTransfer)
    {
        _callToTransfer = c;
        emit callToTransferChanged(c);
    }
}

void AppContext::clearCallToTransfer()
{
    if(_callToTransfer!=Call::invalid())
    {
        _callToTransfer = Call::invalid();
        emit callToTransferChanged(_callToTransfer);
    }
}

void AppContext::callEnded(ccappcore::Call c)
{
    if(c == _callToTransfer)
    {
        _callToTransfer = Call::invalid();
        emit callToTransferChanged(_callToTransfer);
        qDebug() << tr("AppContext::callEnded - emit callToTransferChanged");
    }
}

void AppContext::onActiveCallChanged(ccappcore::Call c)
{
    emit activeCallChanged(c);
}

//void AppContext::onActiveCallSelected(ccappcore::Call c)
//{
//    emit activeCallSelected(c);
//}


void AppContext::onActiveContactChanged(ccappcore::Contact c)
{
    emit activeContactChanged(c);
}
//void AppContext::onContactSelected(ccappcore::Contact c)
//{
//    emit activeContactSelected(c);
//}

//void AppContext::on_windowMinimized(bool checked)
//{
//    if (checked!=_toggleMinMaxWindow)
//    {
//        _toggleMinMaxWindow = checked;
//        emit toggledMinMaxWindows(_toggleMinMaxWindow);
//    }
//}

void AppContext::onStopAlerting(const ccappcore::Call& c)
{
    emit stopAlerting(c);
}

void AppContext::onActivateGlobalAddress(const ccappcore::Contact& c)
{
    emit activeGlobalAddressSelected(c);
}

void AppContext::onClickGlobalAddress(const ccappcore::Contact &c)
{
    emit activeGlobalAddressChanged(c);
}

//void AppContext::onOptionsApplied()
//{
//    LateBoundObject<AppSettings>        _settings;
//    int id = _settings->getOption("messages/language").toInt(0);

//    if (_langId != id)
//        _langId = id;
//    else
//        return;

//    emit switchLanguage(_langId);
//}

IAlarmHandler::IAlarmHandler(QWidget* parent/*, bool activate*/)
    : QWidget(parent),
      _isTimerActive(/*activate*/false),
      _isHandlerActive(false),
      _timerCount(0)
{
    connect(&_alarmTimer, SIGNAL(timeout()),
            this, SLOT(alarmingTimerExpired()));

}

void IAlarmHandler::init()
{
    connectToTimer();
}

void IAlarmHandler::startAlarm()
{
    if (_isHandlerActive)
        return; // tab has already activated

    if (!_isTimerActive)
        _alarmTimer.start(1000);

    _isTimerActive = true;
}

void IAlarmHandler::stopAlarm()
{
    _alarmTimer.stop();
    _timerCount = 0;
    _isTimerActive = false;

    emit changeIcon(getTabIndex(), getTabIcon(true));
}

void IAlarmHandler::alarmingTimerExpired()
{
    ++_timerCount;
    emit changeIcon(getTabIndex(), getTabIcon());
}

void IAlarmHandler::stopHandler(const int &index)
{
    _isHandlerActive = index == getTabIndex();

    if (!_isHandlerActive)
        return; // signal stop to another tab

    if (_isTimerActive)
        stopAlarm();
}


