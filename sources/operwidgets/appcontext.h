#ifndef APPCONTEXT_H
#define APPCONTEXT_H

#include "operwidgets_global.h"
#include "appcore/callscontroller.h"
#include "appcore/appsettings.h"

using namespace ccappcore;

class AppContext : public QObject
{
Q_OBJECT
public:
    explicit AppContext(QObject *parent = 0);


    Call callToTransfer() const { return _callToTransfer; }
public slots:
    void showCallForm(ccappcore::Call);
    void showChatForm(ccappcore::Contact);
    void toggleVisibility();
    void setCallToTransfer(ccappcore::Call);
    void clearCallToTransfer();
    void showDialer(QString phone = QString());
    void onActiveCallChanged(ccappcore::Call);
    //void onActiveCallSelected(ccappcore::Call);
    void onActiveContactChanged(ccappcore::Contact);
    //void onContactSelected(ccappcore::Contact);
    void onActivateForegroundWindow();
    void onStopAlerting(const ccappcore::Call&);

    void onActivateGlobalAddress(const ccappcore::Contact&);
    void onClickGlobalAddress(const ccappcore::Contact&);

    //void onOptionsApplied();

signals:
    void onShowCallForm(ccappcore::Call);
    void onShowChatForm(ccappcore::Contact);
    void onToggleVisibility();
    void callToTransferChanged(ccappcore::Call);
    void onShowDialer(QString phone);
    void activeCallChanged(ccappcore::Call);
    //void activeCallSelected(ccappcore::Call);
    void activeContactChanged(ccappcore::Contact);
    void activeContactSelected(ccappcore::Contact);
    void windowMinimized(bool);
    //void toggledMinMaxWindows(bool);
    void staysOnTop(bool);
    void activateForegroundWindow();
    void stopAlerting(const ccappcore::Call&);

    void activeGlobalAddressChanged(ccappcore::Contact);
    void activeGlobalAddressSelected(ccappcore::Contact);
    //void switchLanguage(int id);

private slots:
    void callEnded(ccappcore::Call);

private:
    LateBoundObject<CallsController> _callsController;
    Call _callToTransfer;
    //int _langId;

};




class IAlarmHandler: public QWidget
{
    Q_OBJECT
public:
    IAlarmHandler(QWidget* parent = 0/*, bool activate = false*/);

protected:
    void init();

    void startAlarm();
    void stopAlarm();

    virtual void connectToTimer() = 0;
public:
    virtual int getTabIndex  ()const = 0;
    virtual QIcon getTabIcon (bool _default = false) const = 0;

signals:
    void changeIcon(const int& index, const QIcon& icon);

private slots:
    void alarmingTimerExpired();
    void stopHandler(const int& index);

protected:
    int timerCount()const {return _timerCount;}
private:
    QTimer _alarmTimer;
    bool _isTimerActive;
    bool _isHandlerActive;

    int _timerCount;

    //int _tabIndex;
};


#endif // APPCONTEXT_H
