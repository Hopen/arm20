#include "tabswindow.h"
#include "ui_tabswindow.h"

#include "calltab.h"
#include "appcore/styleloader.h"

using namespace ccappcore;

//TabsWindow::TabsWindow(QWidget *parent)
//    : QDialog(parent)
//    , _ui(new Ui::TabsWindow)
//{
//    _ui->setupUi(this);
//    _ui->tabs->removeTab(0);

//    connect(_callsController.instance(), SIGNAL(callStarted(ccappcore::Call)),
//            this, SLOT(callStarted(ccappcore::Call)));
//    connect(_appContext.instance(),SIGNAL(onShowCallForm(ccappcore::Call)),
//            this, SLOT(showCall(ccappcore::Call)));

//    LateBoundObject<StyleLoader> styleLoader;
//    styleLoader->load(this);
//}

//TabsWindow::~TabsWindow()
//{
//    delete _ui;
//}

//void TabsWindow::changeEvent(QEvent *e)
//{
//    QWidget::changeEvent(e);
//    switch (e->type()) {
//    case QEvent::LanguageChange:
//        _ui->retranslateUi(this);
//        break;
//    default:
//        break;
//    }
//}

//void TabsWindow::showCall(ccappcore::Call call)
//{
//    foreach(CallTab* tab, findChildren<CallTab*>())
//    {
//        if(tab->call() == call)
//        {
//            showTab(tab);
//            return;
//        }
//    }

//    CallTab* callWidget = createTab();
//    callWidget->setCall(call);
//    showTab(callWidget);
//}


//void TabsWindow::showContact(ccappcore::Contact c)
//{
//    foreach(CallTab* tab, findChildren<CallTab*>())
//    {
//        if(tab->call().contact() == c)
//        {
//            showTab(tab);
//            return;
//        }
//    }
//    CallTab* callWidget = createTab();
//    callWidget->setContact(c);
//    showTab(callWidget);
//}

//CallTab* TabsWindow::createTab()
//{
//    CallTab* callWidget = new CallTab( _ui->tabs );
//    callWidget->setAttribute(Qt::WA_DeleteOnClose);
//    _ui->tabs->addTab( callWidget, "" );
//    connect(callWidget, SIGNAL(destroyed()), this, SLOT(onTabDestroyed()));
//    connect(callWidget, SIGNAL(iconChanged()), this, SLOT(tabIconChanged()));
//    connect(callWidget, SIGNAL(titleChanged(QString)), this, SLOT(tabTitleChanged(QString)));
//    return callWidget;
//}

//void TabsWindow::showTab(CallTab* callWidget)
//{
//    _ui->tabs->setCurrentWidget(callWidget);

//    if(!isVisible())
//    {
//        QDockWidget* parentWidget = qobject_cast<QDockWidget*>(parent());
//        if(parentWidget)
//            parentWidget->show();

//        show();
//    }

//    raise();
//    activateWindow();

//}

//void TabsWindow::callStarted(ccappcore::Call call)
//{
//    if(!isVisible() && !_operSettings->showCallFormImmediately())
//        return;

//    showCall(call);
//}

//void TabsWindow::on_tabs_tabCloseRequested(int index)
//{
//    QWidget* tabWidget = _ui->tabs->widget(index);
//    CallTab* callWidget = qobject_cast<CallTab*>(tabWidget);
//    if(callWidget)
//        callWidget->close();
//}

//void TabsWindow::onTabDestroyed()
//{
//    QWidget* tab = qobject_cast<QWidget*>(sender());

//    int idx = _ui->tabs->indexOf(tab);
//    if(idx>=0)
//        _ui->tabs->removeTab(idx);

//    if(_ui->tabs->count()==0)
//    {
//        //QDockWidget* parentWidget = qobject_cast<QDockWidget*>(parent());
//        //if(!parentWidget || parentWidget->isFloating())
//        //    hide();
//        if(QDockWidget* parentWidget = qobject_cast<QDockWidget*>(parent()))
//            parentWidget->hide();
//        hide();
//    }
//}

//void TabsWindow::tabIconChanged()
//{
//    QWidget* tab = qobject_cast<QWidget*>(sender());
//    int idx = _ui->tabs->indexOf(tab);
//    if(idx<0)
//        return;

//    _ui->tabs->setTabIcon(idx, tab->windowIcon());
//}

//void TabsWindow::tabTitleChanged(const QString& title)
//{
//    QWidget* tab = qobject_cast<QWidget*>(sender());
//    int idx = _ui->tabs->indexOf(tab);
//    if(idx<0)
//        return;

//    _ui->tabs->setTabText(idx, title);
//}

/***************************************************************
****************************************************************
***************************************************************/

TabsWidget::TabsWidget(QWidget *parent):QTabWidget(parent)
{
    setMovable(true);

    connect(this,SIGNAL(currentChanged(int)),this,SLOT(onCurrentTabChanged(int)));
}

TabsWidget::~TabsWidget()
{

}

//int TabsWidget::addTab(quint32 index, QWidget *widget, const QIcon& icon, const QString &label)
//{
//    _tabs.insert(index,TTab(widget,icon,label));
//    //_index.insert(index,index);
//    return QTabWidget::insertTab(index,widget,icon,label);
//}

void TabsWidget::showTab(const qint32 &index, const bool &show)
{
    if (!show)
    {
//        for(int i=0;i<_index.size();++i)
//        {
//            if (index == _index[i])
//            {
//                removeTab(i);
//                _index.removeAt(index);
//                break;
//            }
//        }

        TTab& tb = _tabs[index];
        removeTab(indexOf(tb._head));
    }
    else
    {
//        int i=0;
//        for(;i<_index.size();++i)
//        {
//            if (index < _index[i])
//            {
//                TTab& tb = _tabs[index];
//                insertTab(i,tb._body,tb._icon,tb._label);
//                _index.insert(index,index);
//                return;
//            }
//        }
//        if (i == _index.size())
//        {
//            TTab& tb = _tabs[index];
//            insertTab(i,tb._body,tb._icon,tb._label);
//            _index.insert(index,index);
//        }

          TTab& tb = _tabs[index];
          QTabWidget::addTab(tb._head,tb._icon,tb._label);
    }
}

void TabsWidget::onCurrentTabChanged(int index)
{
    if (IAlarmHandler* handler = dynamic_cast<IAlarmHandler*>(widget(index)))
    {
        emit tabActive(handler->getTabIndex());
    }

}

void TabsWidget::setIcon(const int& index, const QIcon& icon)
{
    TTab& tb = _tabs[index];
    int tab_index = indexOf(tb._head);
    QTabWidget::setTabIcon(tab_index, icon);
}


int TabsWidget::addTab(IAlarmHandler *widget, const QString &label)
{
    connect(widget,SIGNAL(changeIcon(int,QIcon)),this,SLOT(setIcon(int,QIcon)));
    connect(this,SIGNAL(tabActive(int)),widget,SLOT(stopHandler(int)));

    int index = widget->getTabIndex();

    _tabs.insert(index,TTab(widget,widget->getTabIcon(true),label));
    //_index.insert(index,index);
    return QTabWidget::insertTab(index,widget,widget->getTabIcon(true),label);

}
