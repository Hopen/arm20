/********************************************************************************
** Form generated from reading UI file 'editthresholdsdialog.ui'
**
** Created: Tue 26. Mar 13:54:37 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDITTHRESHOLDSDIALOG_H
#define UI_EDITTHRESHOLDSDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QRadioButton>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_EditThresholdsDialog
{
public:
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBoxType;
    QVBoxLayout *verticalLayout;
    QRadioButton *radioButtonStandard;
    QRadioButton *radioButtonInverted;
    QGroupBox *groupBoxValues;
    QVBoxLayout *verticalLayout_2;
    QLabel *normalValueLabel;
    QSpinBox *normalValueSpinBox;
    QLabel *aboveNormalLabel;
    QSpinBox *aboveNormalSpinBox;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *EditThresholdsDialog)
    {
        if (EditThresholdsDialog->objectName().isEmpty())
            EditThresholdsDialog->setObjectName(QString::fromUtf8("EditThresholdsDialog"));
        EditThresholdsDialog->resize(217, 285);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(EditThresholdsDialog->sizePolicy().hasHeightForWidth());
        EditThresholdsDialog->setSizePolicy(sizePolicy);
        verticalLayout_3 = new QVBoxLayout(EditThresholdsDialog);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        groupBoxType = new QGroupBox(EditThresholdsDialog);
        groupBoxType->setObjectName(QString::fromUtf8("groupBoxType"));
        verticalLayout = new QVBoxLayout(groupBoxType);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        radioButtonStandard = new QRadioButton(groupBoxType);
        radioButtonStandard->setObjectName(QString::fromUtf8("radioButtonStandard"));
        radioButtonStandard->setChecked(true);

        verticalLayout->addWidget(radioButtonStandard);

        radioButtonInverted = new QRadioButton(groupBoxType);
        radioButtonInverted->setObjectName(QString::fromUtf8("radioButtonInverted"));
        radioButtonInverted->setChecked(false);

        verticalLayout->addWidget(radioButtonInverted);


        verticalLayout_3->addWidget(groupBoxType);

        groupBoxValues = new QGroupBox(EditThresholdsDialog);
        groupBoxValues->setObjectName(QString::fromUtf8("groupBoxValues"));
        verticalLayout_2 = new QVBoxLayout(groupBoxValues);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        normalValueLabel = new QLabel(groupBoxValues);
        normalValueLabel->setObjectName(QString::fromUtf8("normalValueLabel"));

        verticalLayout_2->addWidget(normalValueLabel);

        normalValueSpinBox = new QSpinBox(groupBoxValues);
        normalValueSpinBox->setObjectName(QString::fromUtf8("normalValueSpinBox"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(normalValueSpinBox->sizePolicy().hasHeightForWidth());
        normalValueSpinBox->setSizePolicy(sizePolicy1);

        verticalLayout_2->addWidget(normalValueSpinBox);

        aboveNormalLabel = new QLabel(groupBoxValues);
        aboveNormalLabel->setObjectName(QString::fromUtf8("aboveNormalLabel"));

        verticalLayout_2->addWidget(aboveNormalLabel);

        aboveNormalSpinBox = new QSpinBox(groupBoxValues);
        aboveNormalSpinBox->setObjectName(QString::fromUtf8("aboveNormalSpinBox"));
        sizePolicy1.setHeightForWidth(aboveNormalSpinBox->sizePolicy().hasHeightForWidth());
        aboveNormalSpinBox->setSizePolicy(sizePolicy1);

        verticalLayout_2->addWidget(aboveNormalSpinBox);


        verticalLayout_3->addWidget(groupBoxValues);

        buttonBox = new QDialogButtonBox(EditThresholdsDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        buttonBox->setCenterButtons(true);

        verticalLayout_3->addWidget(buttonBox);


        retranslateUi(EditThresholdsDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), EditThresholdsDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), EditThresholdsDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(EditThresholdsDialog);
    } // setupUi

    void retranslateUi(QDialog *EditThresholdsDialog)
    {
        EditThresholdsDialog->setWindowTitle(QApplication::translate("EditThresholdsDialog", "Dialog", 0, QApplication::UnicodeUTF8));
        groupBoxType->setTitle(QApplication::translate("EditThresholdsDialog", "\320\242\320\270\320\277 \320\277\320\276\321\200\320\276\320\263\320\276\320\262\321\213\321\205 \320\267\320\275\320\260\321\207\320\265\320\275\320\270\320\271", 0, QApplication::UnicodeUTF8));
        radioButtonStandard->setText(QApplication::translate("EditThresholdsDialog", "\320\237\321\200\321\217\320\274\321\213\320\265", 0, QApplication::UnicodeUTF8));
        radioButtonInverted->setText(QApplication::translate("EditThresholdsDialog", "\320\236\320\261\321\200\320\260\321\202\320\275\321\213\320\265", 0, QApplication::UnicodeUTF8));
        groupBoxValues->setTitle(QApplication::translate("EditThresholdsDialog", "\320\227\320\275\320\260\321\207\320\265\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        normalValueLabel->setText(QApplication::translate("EditThresholdsDialog", "\320\235\320\276\321\200\320\274\320\260\320\273\321\214\320\275\320\276\320\265 (\320\275\320\265 \320\261\320\276\320\273\321\214\321\210\320\265)", 0, QApplication::UnicodeUTF8));
        aboveNormalLabel->setText(QApplication::translate("EditThresholdsDialog", "\320\241\321\200\320\265\320\264\320\275\320\265\320\265 (\320\275\320\265 \320\261\320\276\320\273\321\214\321\210\320\265)", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class EditThresholdsDialog: public Ui_EditThresholdsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDITTHRESHOLDSDIALOG_H
