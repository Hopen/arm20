#ifndef REDIRPROFILEWIDGET_H
#define REDIRPROFILEWIDGET_H

//#include "QtGui"
#include <QStyledItemDelegate>
#include "appcore/appsettings.h"
#include "appcore/lateboundobject.h"
#include "appcore/redirectlistcontroller.h"

using namespace ccappcore;

class QToolButton;
class QTreeView;
class RedirectProfileEditWidget;
class QStyledItemDelegate;

//class DELEGATE
class RadioDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit RadioDelegate(QWidget * parent = 0)
        :QStyledItemDelegate(parent)
    {}

    void paint(QPainter *painter, const QStyleOptionViewItem &option,
                    const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option,
                        const QModelIndex &index) const;
//    QWidget *createEditor(QWidget *parent,
//                          const QStyleOptionViewItem &option,
//                          const QModelIndex &index) const;
//    void setEditorData(QWidget *editor, const QModelIndex &index) const;
//    void setModelData(QWidget *editor, QAbstractItemModel *model,
//                           const QModelIndex &index) const;
};

//class MODEL
class ProfileListModel : public QAbstractListModel
{
    Q_OBJECT

public:

    enum ColDef
    {
        IdColumn = 0,
        NameColumn,
        TaskIdColumn,
        StatusColumn
    };

        ProfileListModel(/*const QPointer<RedirectListController> controller,*/ QObject *parent = 0);


        int rowCount(const QModelIndex &parent = QModelIndex()) const;
        QVariant data(const QModelIndex &index, int role) const;
        QVariant headerData(int section, Qt::Orientation orientation,
                            int role = Qt::DisplayRole) const;
        int columnCount ( const QModelIndex & parent = QModelIndex() ) const;

        bool insertRows(int position, int rows, const QModelIndex &index = QModelIndex());
        bool removeRows(int position, int rows, const QModelIndex &index = QModelIndex());

        Qt::ItemFlags flags(const QModelIndex &index) const;
        bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

signals:
        void activateProfile(int position, bool turnOFF = false);
public slots:
        void on_dataChanged(const QModelIndex &index);
        void on_activateProfile(int pos, bool turnOFF = false);

    private:
        //QPointer<RedirectListController> _controller;
        LateBoundObject<RedirectListController> _controller;
        QStringList _columnsList;
};

// class VIEW
class RedirProfileWidget: public QWidget
{
    Q_OBJECT
public:
    explicit RedirProfileWidget(/*const QPointer<RedirectListController> controller,*/ QWidget *parent = NULL);

public slots:
    void on_btnEdit_clicked(bool checked = false);
    void on_btnNew_clicked(bool checked = false);
    void on_btnDelete_clicked(bool checked = false);

    //void on_sysProfileCreateNeeded(QString name);

    //void on_profileAdded();

    void on_profileSelect(QModelIndex);
private:
    //QPointer<RedirectListController> _controller;

    QPointer<  QAbstractListModel  > _scriptModel;
    QPointer<  QTreeView           > _scriptView;
    QPointer<  QAbstractListModel  > _taskModel;

    //QPointer< RedirectProfileEditWidget> _editPanel;
    //QPointer<  QToolButton         > _newButton;
//    QPointer<  QToolButton         > _deleteButton;
//    QPointer<  QToolButton         > _editButton;
    //QModelIndex _curIndex;
    QToolButton *_deleteButton;
    QToolButton *_editButton;

    LateBoundObject<RedirectListController> _controller;
};

#endif // REDIRPROFILEWIDGET_H
