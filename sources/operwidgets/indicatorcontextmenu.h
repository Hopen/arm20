#ifndef INDICATORCONTEXTMENU_H
#define INDICATORCONTEXTMENU_H

#include "operwidgets_global.h"
#include "appcore/quantativeindicator.h"

using namespace ccappcore;

class OPERWIDGETS_EXPORT IndicatorContextMenu : public QMenu
{
    Q_OBJECT
public:
    explicit IndicatorContextMenu(QWidget *parent = 0);

    void popup(QuantativeIndicator*, const QPoint& pos);
signals:

    void showGauge(ccappcore::QuantativeIndicator* i);

public slots:
    void showGauge();
    void editThresholds();

private:
    QPointer<QuantativeIndicator> _indicator;
    QPointer<QAction> _showGauge;
    QPointer<QAction> _editThresholds;
};

#endif // INDICATORCONTEXTMENU_H
