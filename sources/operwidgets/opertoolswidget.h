#ifndef OPERTOOLSWIDGET_H
#define OPERTOOLSWIDGET_H

#include <QWidget>
#include <QPointer>
#include "appcore/lateboundobject.h"
#include "appcore/callscontroller.h"
#include "appcore/soundeventsmanager.h"
#include "opersettings.h"
#include "contactsmanager.h"

class LineEditWithHint;
class QToolButton;
//class OperToolBtn;
class DialingPauseHandler;
class AppContext;
class TransferContext;

using namespace ccappcore;

//class OperToolBtn : public QWidget
//{
//    Q_OBJECT
//    Q_PROPERTY(bool available READ Available WRITE SetBtnAvailable)
//public:
//    OperToolBtn(QWidget* parent, QString iconPath);
//    virtual ~OperToolBtn();
//    bool Available() const
//    {
//        return _available;
//    }
//    void SetBtnAvailable(const bool& available)
//    {
//        _available = available;
//    }
//    QPointer<QToolButton> Get()
//    {
//        return _btn;
//    }

//private:
//    QPointer<QToolButton> _btn;
//    bool _available;
//};

//class OperToolsWidget : public QWidget
//{
//    Q_OBJECT
//public:
//    explicit OperToolsWidget(QWidget *parent = 0);
//private:
//    void SetupTools();

//signals:

//public slots:
//    void on_bnDialpad_clicked(bool checked = false);
//    void on_bnCallBtn_clicked(bool checked = false);
//    void on_phoneLine_textChanged(QString );
//    void on_OperToolsChanged();

//private:
//    typedef QSharedPointer<OperToolBtn> SafeButton;
//    typedef QVector <SafeButton> OperTools;
//    OperTools _tools;

//    QPointer<LineEditWithHint> _phoneLine;
//    QPointer<QToolButton> _dialpad;
//    QPointer<QHBoxLayout> _toolsLayout;
//    //QPointer<QHBoxLayout> _mainLayout;
//    LateBoundObject<DialingPauseHandler> _dialPauseHandler;
//    LateBoundObject<CallsController> _callsController;
//    LateBoundObject<AppContext> _appContext;
//    LateBoundObject<OperSettings> _operSettings;
//    //OperSettings* pNewSet;

//};

class QToolBar;
class MainWindow;
//class ContactsWidget2;

//#include "appcore/operationcontroller.h"
//class IconButton: public QAction, public ccappcore::OperOperation
//{
//    Q_OBJECT
//public:
//    explicit IconButton(QAction *parent1, const ccappcore::OperOperation& parent2): QAction(parent1), ccappcore::OperOperation(parent2)
//    {

//    }

//    QObject *operator()()
//    {
//        return static_cast<QObject *>(this);
//    }


//};

//class QDockMinimizedWidget: public QDockWidget
//{
//    Q_OBJECT
//public:
//    explicit QDockMinimizedWidget(QWidget *parent = 0):QDockWidget(parent)
//    {

//    }

//private:
//    LateBoundObject<ContactsManager> _contactManager;

//    void closeEvent( QCloseEvent * )
//    {
//        if (_contactManager.instance())
//            _contactManager->resetOperFlags();

//    }

//};

class Button : public QToolButton
{
    Q_OBJECT

public:
    Button(const QString &text, QWidget *parent = 0):QToolButton(parent)
    {
        setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        setText(text);
        setToolTip(text);
    }

    QSize sizeHint() const
    {
        QSize size = QToolButton::sizeHint();
        size.rheight() += 10;
        size.rwidth() = qMax(size.width(), size.height());
        return size;
    }
};

const QString VOICEMAIL_OPERATOR_FUNC = "VoiceMail";
const QString VOICEMAIL_GROUP_FUNC    = "VoiceMailGroup";
const QString VOICEMAIL_SYSTEM_FUNC   = "VoiceMailSystem";


class OperToolBar : public QToolBar
{
    Q_OBJECT
public:
    explicit OperToolBar(bool minimize, QWidget *parent = 0);
    void setIconSize(QSize);
    //void setMainWindow(MainWindow*);
    void connectView(QAbstractItemView* view);
private:
    void setupTools();
    //void setupContacts();
    //void setupIVR();
    void transferToScript(const QString& );
    Button *createButton(const QString &text, const char *member);

signals:
    void updateCallControls(ccappcore::Call);
    void callTransfer(bool);

public slots:
    void on_bnCallBtn_clicked();
    void on_bnCallEndBtn_clicked();
    void on_bnHold_clicked(bool checked);
    void on_bnRec_clicked(bool checked);
    void on_bnCancel_clicked(bool);
    void on_phoneLine_textChanged(QString );
    void on_updateCallControls(ccappcore::Call c);
//    void on_bnTransfer_clicked();
//    void on_bnTransferToOper_clicked();
//    void on_bnTransferToGroup_clicked();
//    void on_bnTransferToIVR_clicked();
    void on_bnVoiceMailTo_clicked();
    void on_bnVoiceMailToOperator_clicked();
    void on_bnVoiceMailToGroup_clicked();
    void on_bnVoiceMailToSystem_clicked();

    void on_OperToolsChanged();
    void on_callActivate(ccappcore::Call);
    //void on_callSelected(ccappcore::Call);
    //void on_contactSelected(ccappcore::Contact);
    void onContactSelected(QVariant contact);
    void onScriptSelected(QString scriptName);
    void onPhoneSelected(QString phone);
    //void on_contactsFrameClosed();
    void on_digitClicked();
    void on_c2o_record(ccappcore::Call call, bool onOff, bool forced);
    void onSetPhoneText(QString text);
    void onItemClicked(const QModelIndex &index);
    //void startTimeout();
    //void playDTMF();

private:
    typedef QSharedPointer<QAction> SafeButton;
    typedef QList <SafeButton> OperTools;
    OperTools _tools;
    ccappcore::Call _associatedCall;
    //QString _transferPhone;

    QPointer<LineEditWithHint> _phoneLine;
    QPointer<QToolButton> _cancelBtn;
    //MainWindow*    _mainWindow;
    //QPointer<ContactsWidget2> _contactsView;
    //QPointer<QDockWidget>  _contactsFrame;
    //QPointer<QDockWidget>  _scriptFrame;
    //QPointer<QHBoxLayout> _toolsLayout;
    LateBoundObject<DialingPauseHandler> _dialPauseHandler;
    LateBoundObject<CallsController> _callsController;
    LateBoundObject<AppContext> _appContext;
    LateBoundObject<OperSettings> _operSettings;
    LateBoundObject<ContactsManager> _contactManager;
    LateBoundObject<OperListController> _operListController;
    QString _lastVoiceMailChoice;
    quint32 _lastTransferChoice;

    enum { NumDigitButtons = 12 };
    typedef QPointer <Button> smartButton;
    QList <smartButton> digitButtons2;

    LateBoundObject<SoundEventsManager> _soundManager;
    QSize _iconSize;
    QPointer <TransferContext> _transferContext;
    LateBoundObject<DialingPauseHandler> _dialPause;
    //bool _forcedRecord;

//    QTimer pauseTimoutTimer;
//    QString _lastDTMF;


};

#endif // OPERTOOLSWIDGET_H
