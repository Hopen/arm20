/********************************************************************************
** Form generated from reading UI file 'opt_application.ui'
**
** Created: Thu 11. Sep 13:32:19 2014
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPT_APPLICATION_H
#define UI_OPT_APPLICATION_H

#include <Qt3Support/Q3MimeSourceFactory>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OptApplication
{
public:
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupConnectionSettings;
    QHBoxLayout *horizontalLayout;
    QLabel *lblServer;
    QLineEdit *tbServer;
    QLabel *lblPort;
    QSpinBox *tbPort;
    QGroupBox *groupConnectionType;
    QGridLayout *gridLayout_4;
    QComboBox *cbConnectionType;
    QCheckBox *chkbAutoIP;
    QGroupBox *groupHardware;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLabel *lblTrunk;
    QSpinBox *tbTrunk;
    QLabel *lblPhone;
    QLineEdit *tbPhone;
    QGroupBox *groupJabber;
    QVBoxLayout *groupJabberLayout;
    QGridLayout *gridLayout_3;
    QLabel *lblXmppAddress;
    QLabel *lblXmppPassword;
    QLineEdit *tbXmppAddress;
    QLineEdit *tbXmppPassword;
    QGroupBox *groupRegistarDomain;
    QHBoxLayout *horizontalLayout_2;
    QLabel *lblRegistarDomain;
    QLineEdit *tbRegistarDomain;

    void setupUi(QWidget *OptApplication)
    {
        if (OptApplication->objectName().isEmpty())
            OptApplication->setObjectName(QString::fromUtf8("OptApplication"));
        OptApplication->resize(400, 373);
        OptApplication->setMinimumSize(QSize(400, 300));
        verticalLayout_2 = new QVBoxLayout(OptApplication);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupConnectionSettings = new QGroupBox(OptApplication);
        groupConnectionSettings->setObjectName(QString::fromUtf8("groupConnectionSettings"));
        horizontalLayout = new QHBoxLayout(groupConnectionSettings);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        lblServer = new QLabel(groupConnectionSettings);
        lblServer->setObjectName(QString::fromUtf8("lblServer"));

        horizontalLayout->addWidget(lblServer);

        tbServer = new QLineEdit(groupConnectionSettings);
        tbServer->setObjectName(QString::fromUtf8("tbServer"));

        horizontalLayout->addWidget(tbServer);

        lblPort = new QLabel(groupConnectionSettings);
        lblPort->setObjectName(QString::fromUtf8("lblPort"));

        horizontalLayout->addWidget(lblPort);

        tbPort = new QSpinBox(groupConnectionSettings);
        tbPort->setObjectName(QString::fromUtf8("tbPort"));
        tbPort->setMaximum(65535);
        tbPort->setValue(50000);

        horizontalLayout->addWidget(tbPort);


        verticalLayout_2->addWidget(groupConnectionSettings);

        groupConnectionType = new QGroupBox(OptApplication);
        groupConnectionType->setObjectName(QString::fromUtf8("groupConnectionType"));
        gridLayout_4 = new QGridLayout(groupConnectionType);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        cbConnectionType = new QComboBox(groupConnectionType);
        cbConnectionType->setObjectName(QString::fromUtf8("cbConnectionType"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(cbConnectionType->sizePolicy().hasHeightForWidth());
        cbConnectionType->setSizePolicy(sizePolicy);
        cbConnectionType->setMinimumSize(QSize(200, 20));

        gridLayout_4->addWidget(cbConnectionType, 0, 0, 1, 1);

        chkbAutoIP = new QCheckBox(groupConnectionType);
        chkbAutoIP->setObjectName(QString::fromUtf8("chkbAutoIP"));

        gridLayout_4->addWidget(chkbAutoIP, 1, 0, 1, 1);


        verticalLayout_2->addWidget(groupConnectionType);

        groupHardware = new QGroupBox(OptApplication);
        groupHardware->setObjectName(QString::fromUtf8("groupHardware"));
        verticalLayout = new QVBoxLayout(groupHardware);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        lblTrunk = new QLabel(groupHardware);
        lblTrunk->setObjectName(QString::fromUtf8("lblTrunk"));

        gridLayout->addWidget(lblTrunk, 0, 0, 1, 1);

        tbTrunk = new QSpinBox(groupHardware);
        tbTrunk->setObjectName(QString::fromUtf8("tbTrunk"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(tbTrunk->sizePolicy().hasHeightForWidth());
        tbTrunk->setSizePolicy(sizePolicy1);
        tbTrunk->setMinimumSize(QSize(0, 20));
        tbTrunk->setMaximum(100);
        tbTrunk->setValue(0);

        gridLayout->addWidget(tbTrunk, 0, 1, 1, 1);

        lblPhone = new QLabel(groupHardware);
        lblPhone->setObjectName(QString::fromUtf8("lblPhone"));

        gridLayout->addWidget(lblPhone, 1, 0, 1, 1);

        tbPhone = new QLineEdit(groupHardware);
        tbPhone->setObjectName(QString::fromUtf8("tbPhone"));
        tbPhone->setMinimumSize(QSize(0, 20));

        gridLayout->addWidget(tbPhone, 1, 1, 1, 1);


        verticalLayout->addLayout(gridLayout);


        verticalLayout_2->addWidget(groupHardware);

        groupJabber = new QGroupBox(OptApplication);
        groupJabber->setObjectName(QString::fromUtf8("groupJabber"));
        groupJabberLayout = new QVBoxLayout(groupJabber);
        groupJabberLayout->setSpacing(6);
        groupJabberLayout->setContentsMargins(9, 9, 9, 9);
        groupJabberLayout->setObjectName(QString::fromUtf8("groupJabberLayout"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        lblXmppAddress = new QLabel(groupJabber);
        lblXmppAddress->setObjectName(QString::fromUtf8("lblXmppAddress"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(lblXmppAddress->sizePolicy().hasHeightForWidth());
        lblXmppAddress->setSizePolicy(sizePolicy2);
        lblXmppAddress->setMinimumSize(QSize(140, 0));

        gridLayout_3->addWidget(lblXmppAddress, 0, 0, 1, 1);

        lblXmppPassword = new QLabel(groupJabber);
        lblXmppPassword->setObjectName(QString::fromUtf8("lblXmppPassword"));

        gridLayout_3->addWidget(lblXmppPassword, 1, 0, 1, 1);

        tbXmppAddress = new QLineEdit(groupJabber);
        tbXmppAddress->setObjectName(QString::fromUtf8("tbXmppAddress"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(tbXmppAddress->sizePolicy().hasHeightForWidth());
        tbXmppAddress->setSizePolicy(sizePolicy3);
        tbXmppAddress->setMinimumSize(QSize(0, 20));

        gridLayout_3->addWidget(tbXmppAddress, 0, 1, 1, 1);

        tbXmppPassword = new QLineEdit(groupJabber);
        tbXmppPassword->setObjectName(QString::fromUtf8("tbXmppPassword"));
        tbXmppPassword->setMinimumSize(QSize(0, 20));
        tbXmppPassword->setEchoMode(QLineEdit::Password);

        gridLayout_3->addWidget(tbXmppPassword, 1, 1, 1, 1);


        groupJabberLayout->addLayout(gridLayout_3);


        verticalLayout_2->addWidget(groupJabber);

        groupRegistarDomain = new QGroupBox(OptApplication);
        groupRegistarDomain->setObjectName(QString::fromUtf8("groupRegistarDomain"));
        horizontalLayout_2 = new QHBoxLayout(groupRegistarDomain);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        lblRegistarDomain = new QLabel(groupRegistarDomain);
        lblRegistarDomain->setObjectName(QString::fromUtf8("lblRegistarDomain"));

        horizontalLayout_2->addWidget(lblRegistarDomain);

        tbRegistarDomain = new QLineEdit(groupRegistarDomain);
        tbRegistarDomain->setObjectName(QString::fromUtf8("tbRegistarDomain"));

        horizontalLayout_2->addWidget(tbRegistarDomain);


        verticalLayout_2->addWidget(groupRegistarDomain);


        retranslateUi(OptApplication);

        QMetaObject::connectSlotsByName(OptApplication);
    } // setupUi

    void retranslateUi(QWidget *OptApplication)
    {
        OptApplication->setWindowTitle(QApplication::translate("OptApplication", "OptApplicationUI", 0, QApplication::UnicodeUTF8));
        groupConnectionSettings->setTitle(QApplication::translate("OptApplication", "\320\235\320\260\321\201\321\202\321\200\320\276\320\271\320\272\320\270 \320\277\320\276\320\264\320\272\320\273\321\216\321\207\320\265\320\275\320\270\321\217 \320\272 IS3 Router", 0, QApplication::UnicodeUTF8));
        lblServer->setText(QApplication::translate("OptApplication", "\320\230\320\274\321\217 \321\201\320\265\321\200\320\262\320\265\321\200\320\260 \320\270\320\273\320\270 IP \320\260\320\264\321\200\320\265\321\201", 0, QApplication::UnicodeUTF8));
        lblPort->setText(QApplication::translate("OptApplication", "\320\237\320\276\321\200\321\202", 0, QApplication::UnicodeUTF8));
        groupConnectionType->setTitle(QApplication::translate("OptApplication", "\320\241\320\277\320\276\321\201\320\276\320\261 \320\272\320\276\320\274\320\274\321\203\321\202\320\260\321\206\320\270\320\270", 0, QApplication::UnicodeUTF8));
        cbConnectionType->clear();
        cbConnectionType->insertItems(0, QStringList()
         << QApplication::translate("OptApplication", "\320\242\320\265\320\273\320\265\321\204\320\276\320\275\320\275\320\260\321\217 \320\273\320\270\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("OptApplication", "VoIP-\321\202\320\265\320\273\320\265\321\204\320\276\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8)
        );
        chkbAutoIP->setText(QApplication::translate("OptApplication", "\320\260\320\262\321\202\320\276\320\274\320\260\321\202\320\270\321\207\320\265\321\201\320\272\320\270 \320\276\320\277\321\200\320\265\320\264\320\265\320\273\321\217\321\202\321\214 IP", 0, QApplication::UnicodeUTF8));
        groupHardware->setTitle(QApplication::translate("OptApplication", "\320\237\320\260\321\200\320\260\320\274\320\265\321\202\321\200\321\213 \320\276\320\261\320\276\321\200\321\203\320\264\320\276\320\262\320\260\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        lblTrunk->setText(QApplication::translate("OptApplication", "ID \320\274\320\260\321\200\321\210\321\200\321\203\321\202\320\260 / \320\237\320\276\321\202\320\276\320\272", 0, QApplication::UnicodeUTF8));
        lblPhone->setText(QApplication::translate("OptApplication", "\320\242\320\265\320\273\320\265\321\204\320\276\320\275\320\275\321\213\320\271 \320\275\320\276\320\274\320\265\321\200 / \320\233\320\270\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        groupJabber->setTitle(QApplication::translate("OptApplication", "\320\220\320\272\320\272\320\260\321\203\320\275\321\202 Jabber", 0, QApplication::UnicodeUTF8));
        lblXmppAddress->setText(QApplication::translate("OptApplication", "XMPP \320\220\320\264\321\200\320\265\321\201", 0, QApplication::UnicodeUTF8));
        lblXmppPassword->setText(QApplication::translate("OptApplication", "\320\237\320\260\321\200\320\276\320\273\321\214", 0, QApplication::UnicodeUTF8));
        groupRegistarDomain->setTitle(QApplication::translate("OptApplication", "\320\241\320\265\321\200\320\262\320\265\321\200 SIP \321\200\320\265\320\263\320\270\321\201\321\202\321\200\320\260\321\206\320\270\320\270", 0, QApplication::UnicodeUTF8));
        lblRegistarDomain->setText(QApplication::translate("OptApplication", "\320\230\320\274\321\217 \321\201\320\265\321\200\320\262\320\265\321\200\320\260 \320\270\320\273\320\270 IP \320\260\320\264\321\200\320\265\321\201", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class OptApplication: public Ui_OptApplication {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPT_APPLICATION_H
