#include "tabwidgetex.h"
TabWidgetEx::TabWidgetEx(QWidget *parent)
    : QTabWidget(parent)
    , _autoHideTabBar(false)
    , _deleteWidgetOnClose(false)
{
    connect(this, SIGNAL(tabCloseRequested(int)),
            this, SLOT(tabCloseRequested(int)));
}


void TabWidgetEx::tabInserted(int index)
{
    Q_UNUSED(index);
    maybeHideTabBar();
}

void TabWidgetEx::tabRemoved(int index)
{
    Q_UNUSED(index);
    maybeHideTabBar();
}

void TabWidgetEx::tabCloseRequested(int index)
{
    QWidget* w = widget(index);
    removeTab(index);
    maybeDeleteTabWidget(w);
}

void TabWidgetEx::removeCloseButton(int index)
{
    QWidget* bn = this->tabBar()->tabButton(index,QTabBar::RightSide);
    if(!bn)
        return;

    this->tabBar()->setTabButton(index, QTabBar::RightSide, NULL);
    delete bn;
}

void TabWidgetEx::setAutoHideTabBar(bool hide)
{
    _autoHideTabBar = hide;
    maybeHideTabBar();
}

void TabWidgetEx::maybeHideTabBar()
{
    if(!autoHideTabBar())
    {
        tabBar()->setVisible(true);
        return ;
    }
    tabBar()->setVisible( count()>1 );
}

void TabWidgetEx::maybeDeleteTabWidget(QWidget* tab)
{
    if(deleteWidgetOnClose())
        delete tab;
}
