#ifndef CONTACTSFORM_H
#define CONTACTSFORM_H

#include "operwidgets_global.h"
#include "appcore/callslistmodel.h"
#include "appcore/soundeventsmanager.h"

#include "contactsmanager.h"
#include "appcontext.h"
#include "dialingpausehandler.h"

using namespace ccappcore;

namespace Ui {
    class DialpadForm;
}

class DialpadForm : public QWidget {
    Q_OBJECT
public:
    DialpadForm(QWidget *parent = 0);
    ~DialpadForm();

    QString phone() const;
    void setPhone(QString phone) const;
public slots:
    void setFocusOnPhone();

protected:
    void changeEvent(QEvent *e);
    void resizeEvent(QResizeEvent *);
    void showEvent(QShowEvent *);
private:

    Ui::DialpadForm *_ui;
    LateBoundObject<AppContext> _appContext;
    LateBoundObject<SessionController> _sessionController;
    LateBoundObject<OperListController> _operListController;
    LateBoundObject<CallsController> _callsController;
    LateBoundObject<StatusOptions> _statusOptions;
    LateBoundObject<DialingPauseHandler> _dialPause;
    LateBoundObject<SoundEventsManager> _soundManager;

private slots:
    //void on_bnPause_clicked(bool checked);
    //void on_bnEndCall_clicked();
    //void on_bnShowCallForm_clicked();
    //void on_bnPause_clicked();
    void on_phone_textChanged(QString );
    void on_bnMakeCall_clicked();
    void dialpadButton_clicked();
    void callToTransferChanged(ccappcore::Call);

    void updateButtonsState();
    void hideIfUndocked();

private:
    void onPhoneActionTriggered();
};

#endif // CONTACTSFORM_H
