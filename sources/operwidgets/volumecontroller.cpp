#include <QDebug>
#include "volumecontroller.h"
#ifdef Q_WS_WIN
//#define _INC_WINDOWS
#include <_mingw.h>
#include <windows.h>
#include <MMSystem.h>
//#include <mmdeviceapi.h>
//#include <windows.h>
//#include <commctrl.h>
//#include <mmdeviceapi.h>
//#include <endpointvolume.h>
#endif

////void CALLBACK MasterVolumeChanged( DWORD dwCurrentVolume, DWORD dwUserValue );

////{{{ Audio specific functions
//#define AUDFREQ			22050	// Frequency
//#define AUDCHANNELS		1		// Number of channels
//#define AUDBITSMPL		16		// Number of bits per sample
//inline
//void SetDeviceType( WAVEFORMATEX* pwfe )
//{
//        memset( pwfe, 0, sizeof(WAVEFORMATEX) );
//        WORD  nBlockAlign = (AUDCHANNELS*AUDBITSMPL)/8;
//        DWORD nSamplesPerSec = AUDFREQ;
//        pwfe->wFormatTag = WAVE_FORMAT_PCM;
//        pwfe->nChannels = AUDCHANNELS;
//        pwfe->nBlockAlign = nBlockAlign;
//        pwfe->nSamplesPerSec = nSamplesPerSec;
//        pwfe->wBitsPerSample = AUDBITSMPL;
//        pwfe->nAvgBytesPerSec = nSamplesPerSec*nBlockAlign;
//}
////}}} Audio specific functions

//DWORD	m_dwMixerHandle;
//DWORD	m_dwVolumeControlID;
//int		m_nChannelCount;


VolumeController::VolumeController():
    m_saveVolume(0)
{
#ifdef Q_WS_WIN
//    qDebug() << QObject::tr("Audio output devices count: %1").arg(waveOutGetNumDevs());

////    IVolume* pMasterVolume = (IVolume*)new CVolumeOutMaster();
////    if ( !pMasterVolume || !pMasterVolume->IsAvailable() )
////    {
////        qDebug() << QObject::tr("cannot create soundmaster");
////    }
////    pMasterVolume->Enable();
////    //pMasterVolume->RegisterNotificationSink( MasterVolumeChanged, dwAnyUserValue );

////    DWORD dwCurrentVolume = pMasterVolume->GetCurrentVolume();

//    // Mixer Info
//    bool	m_bAvailable;

//    // Mixer Info
//    UINT	m_uMixerID;
//    //DWORD	m_dwMixerHandle;

//    DWORD	m_dwLineID;
//    //DWORD	m_dwVolumeControlID;
//    //int		m_nChannelCount;
//    DWORD	m_dwMinimalVolume;
//    DWORD	m_dwMaximalVolume;
//    DWORD	m_dwVolumeStep;


//    if ( !mixerGetNumDevs() )
//            return;

//    HWAVEOUT hwaveOut;
//    MMRESULT mmResult;
//    WAVEFORMATEX WaveFmt;
//    SetDeviceType( &WaveFmt );
//    mmResult = waveOutOpen( &hwaveOut, WAVE_MAPPER, &WaveFmt, 0L, 0L, CALLBACK_NULL );
//    if ( mmResult != MMSYSERR_NOERROR )
//    {
//            qDebug() << QObject::tr(".WaveOutputVolume: FAILURE: Could not open WaveOut Mapper. mmResult=%1\n").arg(mmResult);
//            return;
//    } else {
//            mmResult = mixerGetID( (HMIXEROBJ)hwaveOut, &m_uMixerID, MIXER_OBJECTF_HWAVEOUT );
//            waveOutClose( hwaveOut );
//            if ( mmResult != MMSYSERR_NOERROR )
//            {
//                    qDebug() << QObject::tr(".WaveOutputVolume: FAILURE: WaveOut Mapper in Mixer is not available. mmResult=%1\n").arg(mmResult);
//                    return ;
//            }
//    }

//    mmResult = mixerOpen( (LPHMIXER)&m_dwMixerHandle, m_uMixerID, 0, 0L, 0 );
//    if ( mmResult != MMSYSERR_NOERROR )
//    {
//            qDebug() << QObject::tr(".MasterOutputVolume: FAILURE: Could not open Mixer. mmResult=%1\n").arg(mmResult);
//            return;
//    }

//    //TRACE(".MasterOutputVolume: Initializing for Source MasterOut Line ..\n" );
//    qDebug() << QObject::tr(".MasterOutputVolume: Initializing for Source MasterOut Line ..\n");
//    MIXERLINE MixerLine;
//    memset( &MixerLine, 0, sizeof(MIXERLINE) );
//    MixerLine.cbStruct = sizeof(MIXERLINE);
//    MixerLine.dwComponentType = MIXERLINE_COMPONENTTYPE_DST_SPEAKERS;
//    mmResult = mixerGetLineInfo( (HMIXEROBJ)m_dwMixerHandle, &MixerLine, MIXER_GETLINEINFOF_COMPONENTTYPE );
//    if ( mmResult != MMSYSERR_NOERROR )
//    {
//            //TRACE(".MasterOutputVolume: FAILURE: Could not get Speakers Destionation Line for the MasterOut Source Line while initilaizing. mmResult=%d\n", mmResult );
//        qDebug() << QObject::tr(".MasterOutputVolume: FAILURE: Could not get Speakers Destionation Line for the MasterOut Source Line while initilaizing. mmResult=%1\n").arg(mmResult);
//            return;
//    }

//    MIXERCONTROL Control;
//    memset( &Control, 0, sizeof(MIXERCONTROL) );
//    Control.cbStruct = sizeof(MIXERCONTROL);

//    MIXERLINECONTROLS LineControls;
//    memset( &LineControls, 0, sizeof(MIXERLINECONTROLS) );
//    LineControls.cbStruct = sizeof(MIXERLINECONTROLS);

//    LineControls.dwControlType = MIXERCONTROL_CONTROLTYPE_VOLUME;
//    LineControls.dwLineID = MixerLine.dwLineID;
//    LineControls.cControls = 1;
//    LineControls.cbmxctrl = sizeof(MIXERCONTROL);
//    LineControls.pamxctrl = &Control;
//    mmResult = mixerGetLineControls( (HMIXEROBJ)m_dwMixerHandle, &LineControls, MIXER_GETLINECONTROLSF_ONEBYTYPE );
//    if ( mmResult == MMSYSERR_NOERROR )
//    {
//            if ( !(Control.fdwControl & MIXERCONTROL_CONTROLF_DISABLED) )
//            {
//                    m_bAvailable = true;
//                    //TRACE(".MasterOutputVolume: \"%s\" Volume control for the Speakers Source Line adopted.\n", Control.szShortName );
//                    qDebug() << QObject::tr(".MasterOutputVolume: \"\" Volume control for the Speakers Source Line adopted.\n");
//            } else {
//                    //TRACE(".MasterOutputVolume: WARNING: The Volume Control is disabled.\n" );
//                    qDebug() << QObject::tr(".MasterOutputVolume: WARNING: The Volume Control is disabled.\n");
//            }
//    } else {
//            //TRACE(".MasterOutputVolume: WARNING: Could not get the Speakers Source line Volume Control while initilaizing. mmResult=%d\n", mmResult );
//        qDebug() << QObject::tr(".MasterOutputVolume: WARNING: Could not get the Speakers Source line Volume Control while initilaizing. mmResult=%1\n").arg(mmResult);
//    }

//    m_nChannelCount = MixerLine.cChannels;
//    m_dwLineID = LineControls.dwLineID;
//    m_dwVolumeControlID = Control.dwControlID;
//    m_dwMinimalVolume = Control.Bounds.dwMinimum;
//    m_dwMaximalVolume = Control.Bounds.dwMaximum;
//    m_dwVolumeStep = Control.Metrics.cSteps;


//    /* Enable*/

//    bool bAnyEnabled = false;
//    bool bEnable = true;

//    MIXERLINE lineDestination;
//    memset( &lineDestination, 0, sizeof(MIXERLINE) );
//    lineDestination.cbStruct = sizeof(MIXERLINE);
//    lineDestination.dwLineID = m_dwLineID;
//    mmResult = mixerGetLineInfo( (HMIXEROBJ)m_dwMixerHandle, &lineDestination, MIXER_GETLINEINFOF_LINEID );
//    if ( mmResult != MMSYSERR_NOERROR )
//    {
//            if ( bEnable )
//            {
//                    //TRACE(".MasterOutputVolume: FAILURE: Could not get the Speakers Destination Line while enabling. mmResult=%d\n", mmResult );
//                qDebug() << QObject::tr(".MasterOutputVolume: FAILURE: Could not get the Speakers Destination Line while enabling. mmResult=%1\n").arg(mmResult);
//            } else {
//                    //TRACE(".MasterOutputVolume: FAILURE: Could not get the Speakers Destination Line while disabling. mmResult=%d\n", mmResult );
//                qDebug() << QObject::tr(".MasterOutputVolume: FAILURE: Could not get the Speakers Destination Line while disabling. mmResult=%1\n").arg(mmResult);
//            }
//            return;
//    }
//    // Getting all line's controls
//    int nControlCount = lineDestination.cControls;
//    int nChannelCount = lineDestination.cChannels;
//    memset( &LineControls, 0, sizeof(MIXERLINECONTROLS) );
//    MIXERCONTROL* aControls = (MIXERCONTROL*)malloc( nControlCount * sizeof(MIXERCONTROL) );
//    if ( !aControls )
//    {
//            if ( bEnable )
//            {
//                    //TRACE(".MasterOutputVolume: FAILURE: Out of memory while enabling the line.\n" );
//                    qDebug() << QObject::tr(".MasterOutputVolume: FAILURE: Out of memory while enabling the line.\n");
//            } else {
//                    //TRACE(".MasterOutputVolume: FAILURE: Out of memory while disabling the line.\n" );
//                qDebug() << QObject::tr(".MasterOutputVolume: FAILURE: Out of memory while disabling the line.\n");
//            }
//            return;
//    }
//    memset( &aControls[0], 0, sizeof(nControlCount * sizeof(MIXERCONTROL)) );
//    for ( int i = 0; i < nControlCount; i++ )
//    {
//            aControls[i].cbStruct = sizeof(MIXERCONTROL);
//    }
//    LineControls.cbStruct = sizeof(MIXERLINECONTROLS);
//    LineControls.dwLineID = lineDestination.dwLineID;
//    LineControls.cControls = nControlCount;
//    LineControls.cbmxctrl = sizeof(MIXERCONTROL);
//    LineControls.pamxctrl = &aControls[0];
//    mmResult = mixerGetLineControls( (HMIXEROBJ)m_dwMixerHandle, &LineControls, MIXER_GETLINECONTROLSF_ALL );
//    if ( mmResult == MMSYSERR_NOERROR )
//    {
//            for (int i = 0; i < nControlCount; i++ )
//            {
//                    LONG lValue;
//                    bool bReadyToSet = false;
//                    switch (aControls[i].dwControlType)
//                    {
//                    case MIXERCONTROL_CONTROLTYPE_MUTE:
//                            lValue = (BOOL)!bEnable;
//                            bReadyToSet = true;
//                            break;
//                    case MIXERCONTROL_CONTROLTYPE_SINGLESELECT:
//                            lValue = (BOOL)bEnable;
//                            bReadyToSet = true;
//                            break;
//                    case MIXERCONTROL_CONTROLTYPE_MUX:
//                            lValue = (BOOL)bEnable;
//                            bReadyToSet = true;
//                            break;
//                    case MIXERCONTROL_CONTROLTYPE_MULTIPLESELECT:
//                            lValue = (BOOL)bEnable;
//                            bReadyToSet = true;
//                            break;
//                    case MIXERCONTROL_CONTROLTYPE_MIXER:
//                            lValue = (BOOL)bEnable;
//                            bReadyToSet = true;
//                            break;
//                    }
//                    if ( bReadyToSet )
//                    {
//                            MIXERCONTROLDETAILS_BOOLEAN* aDetails = NULL;
//                            int nMultipleItems = aControls[i].cMultipleItems;
//                            int nChannels = nChannelCount;
//                            // MIXERCONTROLDETAILS
//                            MIXERCONTROLDETAILS ControlDetails;
//                            memset( &ControlDetails, 0, sizeof(MIXERCONTROLDETAILS) );
//                            ControlDetails.cbStruct = sizeof(MIXERCONTROLDETAILS);
//                            ControlDetails.dwControlID = aControls[i].dwControlID;
//                            if ( aControls[i].fdwControl & MIXERCONTROL_CONTROLF_UNIFORM )
//                            {
//                                    nChannels = 1;
//                            }
//                            if ( aControls[i].fdwControl & MIXERCONTROL_CONTROLF_MULTIPLE )
//                            {
//                                    nMultipleItems = aControls[i].cMultipleItems;
//                                    aDetails = (MIXERCONTROLDETAILS_BOOLEAN*)malloc(nMultipleItems*nChannels*sizeof(MIXERCONTROLDETAILS_BOOLEAN));
//                                    if ( !aDetails )
//                                    {
//                                            if ( bEnable )
//                                            {
//                                                    //TRACE(".MasterOutputVolume: FAILURE: Out of memory while enabling the line.\n" );
//                                                qDebug() << QObject::tr(".MasterOutputVolume: FAILURE: Out of memory while enabling the line.\n");
//                                            } else {
//                                                    //TRACE(".MasterOutputVolume: FAILURE: Out of memory while disabling the line.\n" );
//                                                qDebug() << QObject::tr(".MasterOutputVolume: FAILURE: Out of memory while disabling the line.\n");
//                                            }
//                                            continue;
//                                    }
//                                    for ( int nItem = 0; nItem < nMultipleItems; nItem++ )
//                                    {
//                                            /*
//                                            if ( ( aControls[i].dwControlType & MIXERCONTROL_CONTROLTYPE_SINGLESELECT )
//                                              && ( nItem > 0 ) )
//                                            {
//                                                    lValue = (LONG)!((BOOL)lValue);
//                                            }
//                                            */
//                                            for ( int nChannel = 0; nChannel < nChannels; nChannel++ )
//                                            {
//                                                    aDetails[nItem+nChannel].fValue = lValue;
//                                            }
//                                    }
//                            } else {
//                                    nMultipleItems = 0;
//                                    aDetails = (MIXERCONTROLDETAILS_BOOLEAN*)malloc(nChannels*sizeof(MIXERCONTROLDETAILS_BOOLEAN));
//                                    if ( !aDetails )
//                                    {
//                                            if ( bEnable )
//                                            {
//                                                    //TRACE(".MasterOutputVolume: FAILURE: Out of memory while enabling the line.\n" );
//                                                qDebug() << QObject::tr(".MasterOutputVolume: FAILURE: Out of memory while enabling the line.\n");
//                                            } else {
//                                                    //TRACE(".MasterOutputVolume: FAILURE: Out of memory while disabling the line.\n" );
//                                                qDebug() << QObject::tr(".MasterOutputVolume: FAILURE: Out of memory while disabling the line.\n");
//                                            }
//                                            continue;
//                                    }
//                                    for ( int nChannel = 0; nChannel < nChannels; nChannel++ )
//                                    {
//                                            aDetails[nChannel].fValue = (LONG)lValue;
//                                    }
//                            }
//                            ControlDetails.cChannels = nChannels;
//                            ControlDetails.cMultipleItems = nMultipleItems;
//                            ControlDetails.cbDetails = sizeof(MIXERCONTROLDETAILS_BOOLEAN);
//                            ControlDetails.paDetails = &aDetails[0];
//                            mmResult = mixerSetControlDetails( (HMIXEROBJ)m_dwMixerHandle, &ControlDetails, 0L );
//                            if ( mmResult == MMSYSERR_NOERROR )
//                            {
//                                    if ( bEnable )
//                                    {
//                                            //TRACE(".MasterOutputVolume: Enabling Line: Speakers Line control \"%s\"(0x%X) has been set to %d.\n", aControls[i].szShortName, aControls[i].dwControlType, lValue );
//                                        qDebug() << QObject::tr(".MasterOutputVolume: Enabling Line: Speakers Line control \"\"(0x%2) has been set to %3.\n").arg(aControls[i].dwControlType).arg(lValue);
//                                    } else {
//                                            //TRACE(".MasterOutputVolume: Disabling Line: Speakers Line control \"%s\"(0x%X) has been set to %d.\n", aControls[i].szShortName, aControls[i].dwControlType, lValue );
//                                        qDebug() << QObject::tr(".MasterOutputVolume: Disabling Line: Speakers Line control \"\"(0x%2) has been set to %3.\n").arg(aControls[i].dwControlType).arg(lValue);
//                                    }
//                                    bAnyEnabled = true;
//                            }
//                            free( aDetails );
//                    }
//            }
//    } else {
//            if ( bEnable )
//            {
//                    //TRACE(".MasterOutputVolume: FAILURE: Could not get the line controls while enabling. mmResult=%d\n", mmResult );
//                qDebug() << QObject::tr(".MasterOutputVolume: FAILURE: Could not get the line controls while enabling. mmResult=%1\n").arg(mmResult);
//            } else {
//                    //TRACE(".MasterOutputVolume: FAILURE: Could not get the line controls while disabling. mmResult=%d\n", mmResult );
//                qDebug() << QObject::tr(".MasterOutputVolume: FAILURE: Could not get the line controls while disabling. mmResult=%1\n").arg(mmResult);
//            }
//    }
//    free( aControls );
//    if ( !bAnyEnabled )
//    {
//            if ( bEnable )
//            {
//                    //TRACE(".MasterOutputVolume: WARNING: No controls were found for enabling the line.\n" );
//                qDebug() << QObject::tr(".MasterOutputVolume: WARNING: No controls were found for enabling the line.\n");
//            } else {
//                    //TRACE(".MasterOutputVolume: WARNING: No controls were found for disabling the line.\n" );
//                qDebug() << QObject::tr(".MasterOutputVolume: WARNING: No controls were found for disabling the line.\n");
//            }
//    }



//    m_saveVolume = dw;
#else
#endif

    m_saveVolume = getVolume();
    //setVolume(60000);

}

VolumeController::~VolumeController()
{
    setVolume(m_saveVolume);
}

void VolumeController::setVolume(quint32 size)
{
#ifdef Q_WS_WIN
    WAVEOUTCAPSA Woc;
    HWAVEOUT hWave = NULL;
    MMRESULT m = waveOutGetDevCapsA(WAVE_MAPPER, &Woc, sizeof(Woc));
    if(m!=MMSYSERR_NOERROR)
    {
        qDebug() << QObject::tr("Error: waveOutGetDevCapsA %1").arg(MMSYSERR_NOERROR);
        return;
    }
    DWORD dw = size | (size << 16);
    if (Woc.dwSupport && WAVECAPS_VOLUME == WAVECAPS_VOLUME)
        m = waveOutSetVolume(hWave, dw);
    else
    {
        qDebug() << QObject::tr("Error: volume controler does not supported");
        return;
    }

    if(m!=MMSYSERR_NOERROR)
    {
        qDebug() << QObject::tr("Error: waveOutSetVolume %1").arg(MMSYSERR_NOERROR);
    }

//    MIXERCONTROLDETAILS_UNSIGNED* aDetails = (MIXERCONTROLDETAILS_UNSIGNED*)malloc(m_nChannelCount*sizeof(MIXERCONTROLDETAILS_UNSIGNED));
//    if ( !aDetails )
//            return;
//    DWORD dwVolume = MAKELONG(size,size);
//    for ( int i = 0; i < m_nChannelCount; i++ )
//    {
//            aDetails[i].dwValue = dwVolume;
//    }
//    MIXERCONTROLDETAILS ControlDetails;
//    memset( &ControlDetails, 0, sizeof(MIXERCONTROLDETAILS) );
//    ControlDetails.cbStruct = sizeof(MIXERCONTROLDETAILS);
//    ControlDetails.dwControlID = m_dwVolumeControlID;
//    ControlDetails.cChannels = m_nChannelCount;
//    ControlDetails.cMultipleItems = 0;
//    ControlDetails.cbDetails = sizeof(MIXERCONTROLDETAILS_UNSIGNED);
//    ControlDetails.paDetails = &aDetails[0];
//    MMRESULT mmResult = mixerSetControlDetails( (HMIXEROBJ)m_dwMixerHandle, &ControlDetails, MIXER_SETCONTROLDETAILSF_VALUE );
//    free( aDetails );
//    if ( mmResult != MMSYSERR_NOERROR )
//    {
//            //TRACE(".MasterOutputVolume: FAILURE: Could not set volume(%d) mmResult=%d\n", dwValue, mmResult );
//        qDebug() << QObject::tr(".MasterOutputVolume: FAILURE: Could not set volume(%1) mmResult=%1\n").arg(size).arg(mmResult);
//    }
////    HWAVEOUT hWave = NULL;
////    unsigned int volume = size;//double(barVolume->Position) / double(barVolume->Max) * 0xFFFF;
////    DWORD dwVolume = volume | (volume << 16);
////    waveOutSetVolume(hWave, dwVolume);

#else
#endif
    qDebug() << QObject::tr("Volume set to: %1\n").arg(size);

}

unsigned long VolumeController::getVolume()const
{
    unsigned long volume = 0;
#ifdef Q_WS_WIN
    WAVEOUTCAPSA Woc;
    DWORD Volume = 0;
    MMRESULT m = waveOutGetDevCapsA(WAVE_MAPPER, &Woc, sizeof(Woc));
    if(m!=MMSYSERR_NOERROR)
    {
        qDebug() << QObject::tr("Error: waveOutGetDevCapsA %1").arg(MMSYSERR_NOERROR);
        return 0;
    }

    if (Woc.dwSupport && WAVECAPS_VOLUME == WAVECAPS_VOLUME)
        m = waveOutGetVolume(HWAVEOUT(0), &Volume);
    else
    {
        qDebug() << QObject::tr("Error: volume controler  does not supported");
        return 0;
    }

    if(m!=MMSYSERR_NOERROR)
    {
        qDebug() << QObject::tr("Error: waveOutGetVolume %1").arg(MMSYSERR_NOERROR);
        return 0;
    }
    Volume = Volume & 0xFFFF;
    volume = Volume;

////////    HRESULT hr = S_OK;
////////    IMMDeviceEnumerator *pMMDeviceEnumerator = NULL;

////////    hr = CoCreateInstance(__uuidof(MMDeviceEnumerator), NULL, CLSCTX_ALL,
////////                                     __uuidof(IMMDeviceEnumerator),
////////                                     (void**)&pMMDeviceEnumerator);
//    MIXERCONTROLDETAILS_UNSIGNED* aDetails = (MIXERCONTROLDETAILS_UNSIGNED*)malloc(m_nChannelCount*sizeof(MIXERCONTROLDETAILS_UNSIGNED));
//    if ( !aDetails )
//            return 0;
//    MIXERCONTROLDETAILS ControlDetails;
//    memset( &ControlDetails, 0, sizeof(MIXERCONTROLDETAILS) );
//    ControlDetails.cbStruct = sizeof(MIXERCONTROLDETAILS);
//    ControlDetails.dwControlID = m_dwVolumeControlID;
//    ControlDetails.cChannels = m_nChannelCount;
//    ControlDetails.cMultipleItems = 0;
//    ControlDetails.cbDetails = sizeof(MIXERCONTROLDETAILS_UNSIGNED);
//    ControlDetails.paDetails = &aDetails[0];
//    MMRESULT mmResult = mixerGetControlDetails( (HMIXEROBJ)m_dwMixerHandle, &ControlDetails, MIXER_GETCONTROLDETAILSF_VALUE );
//    DWORD dw = aDetails[0].dwValue;
//    free( aDetails );
//    if ( mmResult != MMSYSERR_NOERROR )
//    {
//            //TRACE(".MasterOutputVolume: FAILURE: Could not get volume. mmResult=%d\n", mmResult );
//        qDebug() << QObject::tr(".MasterOutputVolume: FAILURE: Could not get volume. mmResult=%1\n").arg(mmResult);
//            return 0;
//    }
//    volume = dw;
#else
#endif
    qDebug() << QObject::tr("Windows volume: %1").arg(volume);
    return volume;
}
