﻿#ifndef CONTACTSWIDGET3_H
#define CONTACTSWIDGET3_H

#include <QWidget>
#include "appcore/lateboundobject.h"

//#include "appcore/proxymodeltree.h"
//#include "appcore/proxymodellist.h"
#include "appcore/contact.h"
#include "appcore/operlistcontroller.h"
#include "appcore/filteredmodel.h"
#include "appcontext.h"
#include "appcore/spamlistcontroller.h"
#include "appcore/JabberController.h"

#include "appcontext.h"

using namespace ccappcore;

//class ContactsManager;
QT_BEGIN_NAMESPACE
class QToolButton;
class LineEditWithHint;
class QTreeView;
class QSortFilterProxyModel;
QT_END_NAMESPACE

class ContactsModel : public QAbstractListModel,
                      public FilteredModel<Contact>
{
    Q_OBJECT

public:

//    enum ColDef
//    {
//        IdColumn = 0,
//        NameColumn,
//        TaskIdColumn,
//        StatusColumn
//    };

        ContactsModel(QObject *parent = 0);


        int rowCount(const QModelIndex &parent = QModelIndex()) const;
        QVariant data(const QModelIndex &index, int role) const;
        QVariant headerData(int section, Qt::Orientation orientation,
                            int role = Qt::DisplayRole) const;
        int columnCount ( const QModelIndex & parent = QModelIndex() ) const;

        //bool insertRows(int position, int rows, const QModelIndex &index = QModelIndex());
        //bool removeRows(int position, int rows, const QModelIndex &index = QModelIndex());

//        Qt::ItemFlags flags(const QModelIndex &index) const;
//        //bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

//        bool dropMimeData(const QMimeData *data, Qt::DropAction action,
//                          int row, int column, const QModelIndex &parent);
//        QMimeData *mimeData(const QModelIndexList &indexes) const;
//        QStringList mimeTypes() const;
//        Qt::DropActions supportedDropActions() const;
        void sort ( int column, Qt::SortOrder order = Qt::AscendingOrder );

        typedef QList<Contact> ContactCollector;

protected: //FilteredModel
    virtual void applyFilter(const PredicateGroup<Contact>& filter);


private:
        QString columnNameById(const qint32& id)const;
signals:
public slots:
        void reset();
        void sort();

        void alertingTimerExpired();
private:
        //LateBoundObject < ContactsManager > _contactsManager;
        LateBoundObject < OperListController > _controller;
        LateBoundObject < SpamListController > _spamController;
        LateBoundObject < JabberController > _jabber;

        //QStringList _columnsList;
        QList<int> _columnsList;
        ContactCollector _contacts; //currently displayed contacts
        qint32           _saveSortColumn;
        Qt::SortOrder    _saveSortOrder;
        QTimer _alertingTimer;
        quint8 _alertingCount;
};

class ContactsWidget3 : //public QWidget,
                        public IAlarmHandler
{
    Q_OBJECT
public:
    explicit ContactsWidget3(QWidget *parent = 0);
    
    QAbstractItemView* view()const{return _mainView.data();}
signals:
    
public slots:
    void on_hideOfflineOperators(bool checked);
    void on_hideOperators       (bool checked);
    void on_hideGroups          (bool checked);
    void on_showPrivateGroupOnly(bool checked);
    //void on_contactSelect       (QModelIndex );
    void on_contactChanged(ccappcore::Contact);

    void onBtnMakeCallCliked    ();
    void onBtnSendMessageCliked ();

    void updateWidgetControls();

    void connectToTimer();

    int getTabIndex()const;
    QIcon getTabIcon (bool _default = false) const;

public slots:
    void onSpamMessageAdded(SpamMessage);

protected:
    //LateBoundObject < ContactsManager > _contactsManager;
    QPointer<  ContactsModel       > _mainModel;
    QPointer<  QTreeView           > _mainView;

    FilteredModel<Contact>::FilterCriteria   _isOnlineFilter;
    FilteredModel<Contact>::FilterCriteria   _isOperFilter;
    FilteredModel<Contact>::FilterCriteria   _isGroupFilter;
    FilteredModel<Contact>::FilterCriteria   _isShowPrivateGroup;

    LateBoundObject<AppContext  > _appContext;
    LateBoundObject<CallsController> _callsController;
    LateBoundObject<SpamListController> _spamController;
    LateBoundObject<OperListController> _operController;
    LateBoundObject<JabberController> _jabberController;

private:
    QPointer<QPushButton> _btnMakeCall;
    QPointer<QPushButton> _btnSendMessage;

    ccappcore::Contact _assignedContact;
    mutable ccappcore::SpamMessage _assignedMessage;
};

//class OperContactsWidget: public ContactsWidget3
//{
//    Q_OBJECT
//public:
//    explicit OperContactsWidget(QWidget *parent = 0);
//private:
//    void search(const QString& text);

//public slots:
//    void on_bnTransfer_clicked(bool);
//    void on_searchLine_textChanged(QString );

//signals:
//    void lineSelected(QString);

//protected:
//    QPointer < LineEditWithHint > _searchLine;
//    QPointer < QToolButton      > _transferButton;
//    QPointer< QSortFilterProxyModel> _proxyModel;
//    QString                       _selectedLine;

//};

//class GroupContactsWidget: public ContactsWidget3
//{
//    Q_OBJECT
//public:
//    explicit GroupContactsWidget(QWidget *parent = 0);
//};
class ContactSortPredicate
{
public:
    explicit ContactSortPredicate(Qt::SortOrder order = Qt::AscendingOrder)
        :_order(order)
    {

    }

    virtual bool operator()(const Contact& c1,const Contact& c2)=0;

protected:
    Qt::SortOrder                    _order;
};

class ContactSortByNamePredicate: public ContactSortPredicate
{
public:
    explicit ContactSortByNamePredicate(Qt::SortOrder order = Qt::AscendingOrder)
        :ContactSortPredicate(order)
    {

    }

    bool operator()(const Contact& c1,const Contact& c2)
    {
//        if (_order == Qt::AscendingOrder)
//            return c1.name() < c2.name();
//        return c1.name() >= c2.name();

        bool retVal = c1.name() < c2.name();
        retVal = (_order == Qt::AscendingOrder) ? retVal:!retVal;
        return retVal;
    }

};

class ContactSortByStatusPredicate: public ContactSortPredicate
{
public:
    explicit ContactSortByStatusPredicate(Qt::SortOrder order = Qt::AscendingOrder)
        :ContactSortPredicate(order)
    {

    }

    bool operator()(const Contact& c1,const Contact& c2)
    {
//        if (_order == Qt::AscendingOrder)
//        {
//            if(c1.status()<c2.status())
//                return true;
//            if(c1.status()>c2.status())
//                return false;
//            if( c1.isContainer() && !c2.isContainer() )
//                return true;
//            if( !c1.isContainer() && c2.isContainer() )
//                return false;

//            return c1.name() < c2.name();
//        }

//        if(c1.status()<c2.status())
//            return false;
//        if(c1.status()>c2.status())
//            return true;
//        if( c1.isContainer() && !c2.isContainer() )
//            return false;
//        if( !c1.isContainer() && c2.isContainer() )
//            return true;

//        return c1.name() >= c2.name();

        bool retVal = c1 < c2;
        retVal = (_order == Qt::AscendingOrder) ? retVal:!retVal;
        return retVal;
    }
};

class ContactSortByPhonePredicate: public ContactSortPredicate
{
public:
    explicit ContactSortByPhonePredicate(Qt::SortOrder order = Qt::AscendingOrder)
        :ContactSortPredicate(order)
    {

    }

    bool operator()(const Contact& c1,const Contact& c2)
    {
        bool retVal = c1.data(Contact::Phone).toInt() < c2.data(Contact::Phone).toInt();
        retVal = (_order == Qt::AscendingOrder) ? retVal:!retVal;
        return retVal;
    }

};



#endif // CONTACTSWIDGET3_H
