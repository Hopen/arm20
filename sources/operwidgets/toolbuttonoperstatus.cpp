#include "toolbuttonoperstatus.h"
#include "appcore/sessioncontroller.h"

using namespace ccappcore;

ToolButtonOperStatus::ToolButtonOperStatus(QWidget *parent)
    : QToolButton(parent)
{
    connect(&_operStatusMenu, SIGNAL(checkedActionChanged(const QAction*)),
            this, SLOT(onCheckedActionChanged(const QAction*)));
    connect(this, SIGNAL(clicked()),
            this, SLOT(onClicked()));
    connect(this, SIGNAL(updateControlsStatus()),
            this, SLOT(onControlsStatusChanged()));

    setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    setPopupMode(QToolButton::MenuButtonPopup);
    setMenu(&_operStatusMenu);
    setMinimumWidth(100);
    setCheckable(true);
    setAutoRaise(true);
}

ToolButtonOperStatus::~ToolButtonOperStatus()
{
}

void ToolButtonOperStatus::showEvent(QShowEvent *)
{
    Operator sessionOper = _sessionController->sessionOwner();
    _operStatusMenu.bindOperator(sessionOper);
}

void ToolButtonOperStatus::onCheckedActionChanged(const QAction* action)
{
    if(!action)
        return;

    setText(action->text());
    //qDebug() << tr("ToolButtonOperStatus::onCheckedActionChanged - action->text() == \"%1\"").arg(action->text());

    setIcon(action->icon());

    bool bIsPause = _operStatusMenu.boundOperator().isPaused();
    //qDebug() << tr("Is bound operator on pause - %1").arg(bIsPause);
    setChecked(bIsPause);
    emit changed(bIsPause);
    emit updateControlsStatus();
}

void ToolButtonOperStatus::onClicked()
{
    const Operator& op = _operStatusMenu.boundOperator();
    Contact::ContactStatus status = op.isPaused() ? Contact::Free : Contact::Paused;

    _operListController->reqChangeStatus( op,status, _operListController->statusAsString(status));
}

void ToolButtonOperStatus::onControlsStatusChanged()
{
    const Operator& op = _operStatusMenu.boundOperator();
    /*_operStatusMenu.*/setDisabled(op.isPaused());

}

ToolButtonOperStatusEx::ToolButtonOperStatusEx(QWidget *parent)
    : QWidget(parent)
{
    _toolButtonStatus = new ToolButtonOperStatus(this);
    _quickPause  = new QToolButton(this);
    _quickPause->setIcon(QIcon(":/resources/icons/new/status pause on 32.png"));
    _quickPause->setCheckable(true);
    _quickPause->setAutoRaise(true);
    _quickPause->setToolTip(tr("Пауза"));

    connect(_toolButtonStatus.data(), SIGNAL (changed(bool)), this, SLOT (onToolButtonOperStatusChanged(bool)));
    connect(_quickPause.data(), SIGNAL(clicked()),
            _toolButtonStatus.data(), SLOT(onClicked()));


    QHBoxLayout *Layout = new QHBoxLayout();
    Layout->addWidget(_toolButtonStatus);
    Layout->addWidget(_quickPause);
    setLayout(Layout);
    //setMaximumHeight(40);
}

void ToolButtonOperStatusEx::onToolButtonOperStatusChanged(const bool &isPause)
{
    if (_quickPause->isChecked() && isPause)
        return;
    _quickPause->setChecked(isPause);
}
