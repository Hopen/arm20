#include <QtGui>
#include "callswidget2.h"
#include "appcore/timeutils.h"
#include "appcore/domainobjectfilter.h"
#include "appcore/useritemdatarole.h"
#include "opersettings.h"
#include "tabswindow.h"
#include "mainwindow.h"

const QString CONNECT_CALLS     = QObject::tr("Соединить звонки"   );
const QString CREATE_CONFERENCE = QObject::tr("Создать конференцию");

CallsModel::CallsModel(QObject *parent): QAbstractListModel(parent)
{
    _columnsList
            //<<CallSelectColumn
            <<AbonPhoneColumn
            <<CallingPhoneColumn
            <<CallTypeColumn
            <<CallStateColumn
            //<<GroupNameColumn
            //<<BeginTimeColumn
            //<<StateTimeColumn
            <<PriorityColumn
            <<ExtraInfoColumn
            //<<LastOperNameColumn
            //<<CallLengthColumn
            <<CallLengthElapsedColumn;

    setVisibleColumns();

//    connect(_controller.instance(),
//            SIGNAL(callAdded(ccappcore::Call)),
//            this,
//            SLOT(reset()),
//            Qt::QueuedConnection);

//    connect(_controller.instance(),
//            SIGNAL(callRemoved(ccappcore::Call)),
//            this,
//            SLOT(reset()),
//            Qt::QueuedConnection);

    connect(_controller.instance(),
            SIGNAL(callStateChanged(ccappcore::Call)),
            this,
            SLOT(reset(ccappcore::Call)),
            Qt::QueuedConnection);

    connect(&_columeTimer, SIGNAL(timeout()),
            this, SLOT(columeTimerExpired()));

//    connect(_controller.instance(),
//            SIGNAL(callAssigned(ccappcore::Call)),
//            this,
//            SLOT(addToConferenceList(ccappcore::Call)));
//    connect(_controller.instance(),
//            SIGNAL(callStarted(ccappcore::Call)),
//            this,
//            SLOT(addToConferenceList(ccappcore::Call)));

//    connect(_controller.instance(),
//            SIGNAL(callAssigned(ccappcore::Call)),
//            this,
//            SLOT(addToAlertingList(ccappcore::Call)));
//    connect(_controller.instance(),
//            SIGNAL(callStarted(ccappcore::Call)),
//            this,
//            SLOT(addToAlertingList(ccappcore::Call)));
//    connect(_controller.instance(),
//            SIGNAL(callEnded(ccappcore::Call)),
//            this,
//            SLOT(deleteFromAlertingList(ccappcore::Call)));
//    connect(_controller.instance(),
//            SIGNAL(callFailed(ccappcore::Call,ccappcore::Call::CallResult,QString)),
//            this,
//            SLOT(deleteFromAlertingList(ccappcore::Call)));
//    connect(_appContext.instance(),
//            SIGNAL(activeCallChanged(ccappcore::Call)),
//            this,
//            SLOT(deleteFromAlertingList(ccappcore::Call)));

    connect(_controller.instance(),
            SIGNAL(callStateChanged(ccappcore::Call)),
            this,
            SLOT(onCallStateChanged(ccappcore::Call))/*,
            Qt::QueuedConnection*/);

//    connect(_appContext.instance(),
//            SIGNAL(stopAlerting(ccappcore::Call)),
//            this,
//            SLOT(deleteFromAlertingList(ccappcore::Call)));

    reset(Call::invalid());

//    _alertingCount = 0;
//    connect(&_alertingTimer, SIGNAL(timeout()),
//            this, SLOT(alertingTimerExpired()));


    s_test = false;

    name = tr("other call model");
}

void CallsModel::applyFilter(const PredicateGroup<Call>& filter)
{
    emit layoutAboutToBeChanged();
    _filter = filter;

    _calls.clear();

    _calls = _controller->findCalls(filter);

//    CallIDCollector collector;
//    for (QList<Call>::const_iterator cit = _calls.begin();cit!=_calls.end();++cit)
//    {
//        int callID = cit->callId();
//        if (_connectingCalls.contains(callID))
//            collector.push_back(callID);
//    }
//    _connectingCalls = collector;

    qSort(_calls);

    emit layoutChanged();
}

QVariant CallsModel::headerData(int section,
                                    Qt::Orientation orientation,
                                    int role ) const
{
    if(orientation!=Qt::Horizontal)
        return QAbstractListModel::headerData(section,orientation,role);

    if(section<0 || section>=_columnsList.count())
        return QVariant();

    if(role == Qt::TextAlignmentRole)
        return Qt::AlignLeft;

    ColDef column = _columnsList[section];
    if(role == Qt::DisplayRole)
    {
        switch(column)
        {
        case AbonPhoneColumn:
                return tr("Номер абонента");
        case CallStateColumn:
                return tr("Состояние");
        case GroupNameColumn:
                return tr("Группа");
        case CallTypeColumn:
                return tr("Тип");
        case BeginTimeColumn:
                return tr("Время начала");
        case StateTimeColumn:
                return tr("Время состояния");
        case PriorityColumn:
                return tr("Приоритет");
        case ExtraInfoColumn:
                return tr("Доп. информация");
        case LastOperNameColumn:
                return tr("Последний оператор");
        case CallLengthElapsedColumn:
                return tr("Длительность звонка");
        case CallLengthColumn:
                return tr("Длительность звонка");
        case CallingPhoneColumn:
            return tr("Исходящий номер");
        default:
            return tr("");
        }
    }
    return QAbstractListModel::headerData(section,orientation,role);
}

int CallsModel::rowCount(const QModelIndex& parent) const
{
    if(parent.isValid())
        return 0; //no childs

    return _calls.count();
}

int CallsModel::columnCount(const QModelIndex &parent) const
{
    return _columnsList.size();
}

QVariant CallsModel::data(const QModelIndex& index, int role) const
{
    Call call = callAt(index);
    if(!call.isValid())
        return QVariant();

    if(index.column()<0 || index.column()>=_columnsList.count())
        return QVariant();

    if(role == ccappcore::AssociatedObjectRole)
        return QVariant::fromValue(call);

    if(role == ccappcore::StateTimeRole)
    {
        if( call.isCompleted() )
            return QVariant(); //do not display state time for completed calls

        return call.stateTime();
    }

    if(role == ccappcore::StateReasonRole)
    {
        return call.stateReason();
    }

    ColDef column = _columnsList[index.column()];

    if(role == Qt::DisplayRole)
    {
        switch(column)
        {
        case AbonPhoneColumn:
            return _controller->callAbonPhone(call);
        case CallStateColumn:
            return call.callStateString();
        case GroupNameColumn:
            return _controller->callGroup(call).name();
        case CallTypeColumn:
            return call.callTypeString();
        case BeginTimeColumn:
                return call.beginDateTime();
        case StateTimeColumn:
                return TimeUtils::timeElapsed(call.stateTime(), QDateTime::currentDateTime()).toString("HH:mm:ss");
        case PriorityColumn:
                return call.priority();
        case ExtraInfoColumn:
                return call.callExtraInfo();
        case LastOperNameColumn:
                return _controller->callOper(call).name();
        case CallLengthElapsedColumn:
        {
                //qDebug() << tr("CallsModel::data - Elapsed time has updated");
                //return TimeUtils::timeElapsed(call.localBeginDateTime(), QDateTime::currentDateTime());
            {
                int secs = call.localBeginDateTime().secsTo(QDateTime::currentDateTime());
                return TimeUtils::SecsToShortHMS(secs);

            }
        }
        case CallLengthColumn:
                return TimeUtils::timeElapsed(call./*beginDateTime*/localBeginDateTime(), call.stateTime());
        case CallingPhoneColumn:
            return _controller->callOperPhone(call);
        default:
            return tr("");
        }
    }

    if ((role == Qt::CheckStateRole) && (index.column() == 0))
    {
        return _connectingCalls.contains(call.callId())?Qt::Checked:Qt::Unchecked;
    }


    if(role == Qt::DecorationRole && index.column() == 0)
        return getCallStatusIcon(call);

//    if(role == Qt::TextColorRole && _alertingCalls.contains(call.callId()) && _alertingCount%2)
//    {
//        //TBD
//        return QColor("red");
//    }

//    if(role == Qt::BackgroundRole && _alertingCalls.contains(call.callId()) && _alertingCount%2)
//    {
//        //TBD
//        return QColor(Qt::red);
//    }

//    if(role  == Qt::BackgroundColorRole && _alertingCalls.contains(call.callId()) && _alertingCount%2)
//    {
//        return QColor("red");
//    }


    return QVariant();
}

Call CallsModel::callAt(const QModelIndex& index) const
{
    if( !index.isValid() )
        return Call::invalid();

    //simple list mode
    if( index.row() < 0 || index.row() >= _calls.count() )
        return Call::invalid();

    Call call = _calls.at( index.row() );
    return call;
}

Call CallsModel::callAt(const int& index) const
{
    //simple list mode
    if( index < 0 || index >= _calls.count() )
        return Call::invalid();

    Call call = _calls.at( index );
    return call;
}

QModelIndex CallsModel::indexOf(ccappcore::Call call) const
{
    int row = _calls.indexOf(call);
    QModelIndex itemIndex = index( row, 0, QModelIndex() );
    return itemIndex;
}


QIcon CallsModel::getCallStatusIcon(ccappcore::Call call) const
{

    if(call.callType() == Call::CT_Incoming )
    {
        if(call.isCompleted())
            return QIcon(":/resources/icons/call_in_offline_16x16.png");
        else
            return QIcon(":/resources/icons/call_in_online_16x16.png");
    }

    //TODO: Deal with op call: Is OP call incoming or outgoing?
    if(call.isCompleted())
        return QIcon(":/resources/icons/call_out_offline_16x16.png");
    else
        return QIcon(":/resources/icons/call_out_online_16x16.png");
}

void CallsModel::reset(ccappcore::Call c)
{
    applyFilter(_filter);

////    if (c.isValid() && _filter.matches(c) && !_connectingCalls.contains(c.callId()))
////        _connectingCalls.push_back(c.callId());


////    if (c.isValid() && !_filter.matches(c) && _connectingCalls.contains(c.callId()))
////        _connectingCalls.removeOne(c.callId());
//    deleteFromConferenceList(c);
}


void CallsModel::setVisibleColumns()
{
//   emit layoutAboutToBeChanged();
//   emit layoutChanged();

   if(_columnsList.contains(CallLengthElapsedColumn))
       _columeTimer.start(5000);
   else
       _columeTimer.stop();
}

void CallsModel::columeTimerExpired()
{
    int columnIndex = _columnsList.indexOf(CallLengthElapsedColumn);
    if(columnIndex<0)
        return;

    //int count = rowCount(QModelIndex());
    int count = _calls.count();
    if(count <= 0)
        return;

    QModelIndex topLeft = index(0,columnIndex,QModelIndex());
    QModelIndex bottomRight = index(count-1,columnIndex,QModelIndex());
    emit dataChanged(topLeft, bottomRight);
}

//void CallsModel::alertingTimerExpired()
//{
//    ++_alertingCount;
//    emit layoutChanged();
//}

//Qt::ItemFlags CallsModel::flags(const QModelIndex &index) const
//{
//    if (!index.isValid())
//        return Qt::ItemIsEnabled;

//    return QAbstractItemModel::flags(index) |
//            Qt::ItemIsDragEnabled |
//            Qt::ItemIsDropEnabled |
//            Qt::ItemIsUserCheckable;
//}

bool CallsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    Call call = callAt(index);
    if(!call.isValid())
        return false;


    if (index.isValid() && role == Qt::EditRole)
    {
    }
    if (index.isValid() && role == Qt::CheckStateRole && index.column() == 0)
    {
        int callID = call.callId();
        if (_connectingCalls.contains(callID))
            _connectingCalls.removeOne(callID);
        else
            _connectingCalls.push_back(callID);

        emit connectingCallsListChanged(callID);
        emit layoutChanged();
    }
    return false;
}


Qt::ItemFlags CallsModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags defaultFlags = QAbstractListModel::flags(index);

    if (index.isValid())
        return Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsUserCheckable |defaultFlags;
    else
        return Qt::ItemIsDropEnabled | defaultFlags;

}

bool CallsModel::dropMimeData(const QMimeData *data,
    Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
    if (action == Qt::IgnoreAction)
        return true;

    Call parentCall = callAt(parent);
    if(!parentCall.isValid())
        return false;

    if (!data->hasFormat("text/plain"))
        return false;

//    int beginRow;

//    if (row != -1)
//        beginRow = row;
//    else if (parent.isValid())
//        beginRow = 0;
//    else
//        beginRow = rowCount(QModelIndex());

    QByteArray encodedData = data->data("text/plain");
    QDataStream stream(&encodedData, QIODevice::ReadOnly);

    _connectingCalls.clear();
    while (!stream.atEnd()) {
        int callId;
        stream >> callId;
        _connectingCalls.push_back(callId);
    }

    if (_connectingCalls.isEmpty())
        return false;

    if (parentCall.callState() == Call::CS_Conferencing)
    {
        emit joinCalls(parentCall.confId());
    }
    else
    {
        _connectingCalls.push_back(parentCall.callId());
        emit connectCalls();
    }
    emit connectingCallsListChanged(-1); // "-1" - why?

    return true;
}

QMimeData *CallsModel::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *mimeData = new QMimeData();
    QByteArray encodedData;

    QDataStream stream(&encodedData, QIODevice::WriteOnly);

    QModelIndex index = indexes.first();

//    foreach (QModelIndex index, indexes) {
        if (index.isValid()) {

            Call call = callAt(index);
            //if(!call.isValid())
            //    continue;

            if(call.isValid())
                stream << call.callId();
        }
//    }

    mimeData->setData("text/plain", encodedData);
    return mimeData;
}

QStringList CallsModel::mimeTypes() const
{
    QStringList types;
    types << "text/plain";
    return types;
}

Qt::DropActions CallsModel::supportedDropActions() const
{
    return Qt::TargetMoveAction/* | Qt::MoveAction*/;
}

//void CallsModel::addToConferenceList(const ccappcore::Call& c)
//{
//    if (c.isValid() && _filter.matches(c) && !_connectingCalls.contains(c.callId()))
//    {
//        _connectingCalls.push_back(c.callId());
//        emit connectingCallsListChanged(c.callId());
//    }
//}

//void CallsModel::deleteFromConferenceList(const ccappcore::Call& c)
//{
//    if (c.isValid() && !_filter.matches(c) && _connectingCalls.contains(c.callId()))
//    {
//        _connectingCalls.removeOne(c.callId());
//        emit connectingCallsListChanged(c.callId());
//    }
//}

//void CallsModel::addToAlertingList(const ccappcore::Call& c)
//{
//    if (c.isValid() && _filter.matches(c) && !_alertingCalls.contains(c.callId()))
//    {
//        _alertingCalls.push_back(c.callId());
//        //emit connectingCallsListChanged(c.callId());
//    }
//}

//void CallsModel::deleteFromAlertingList(const ccappcore::Call& c)
//{
//    if (c.isValid() /*&& !_filter.matches(c)*/ && _alertingCalls.contains(c.callId()))
//    {
//        _alertingCalls.removeOne(c.callId());
//        //emit connectingCallsListChanged(c.callId());
//        if (_alertingCalls.isEmpty())
//        {
//            _alertingTimer.stop();
//        }
//        //emit layoutChanged();
//    }
//}

void CallsModel::onCallStateChanged(Call c)
{
    if (!c.isValid())
        return;

    Call::CallState callState = c.callState();
    switch (callState)
    {
    case Call::CS_Assigned:
    case Call::CS_Calling: // return, due to marking and assigning incoming call
    case Call::CS_RingStarted: // the same shit
    {
        if ( !_filter.matches(c))
            return;

        int callIndex = _calls.indexOf(c);
        if (callIndex < 0)
            return;

        //_appContext->onActiveCallChanged(c); // select incoming call
        if (!_connectingCalls.contains(c.callId()))
        {
            _connectingCalls.push_back(c.callId());
            emit connectingCallsListChanged(c.callId());

            emit incomingCall(callIndex);
        }
//        if (_alertingCalls.isEmpty())
//        {
//            emit dropSelectedCall();
//            _alertingCount = 0;
//            _alertingTimer.start(500);
//        }
//        if (!_alertingCalls.contains(c.callId()))
//        {
//            _alertingCalls.push_back(c.callId());
//        }
        break;
    }

    case Call::CS_Completed:
    case Call::CS_CallFailed:
    {
        if ( _filter.matches(c))
            return;

        if (_connectingCalls.contains(c.callId()))
        {
            _connectingCalls.removeOne(c.callId());
            emit connectingCallsListChanged(c.callId());
        }
////        if (_alertingCalls.contains(c.callId()))
////        {
////            _alertingCalls.removeOne(c.callId());
////        }
////        if (_alertingCalls.isEmpty())
////        {
////            _alertingTimer.stop();
////        }
//        qDebug() << tr("CallsModel::onCallStateChanged - Call::CS_Completed");
        break;
    }

    default:
    {

    }
    }

}


CallsWidget2::CallsWidget2(QWidget *parent/*, bool bIsSessionOwnerCall*/) :
    //QWidget(parent)
    IAlarmHandler(parent)
{
    _mainModel = new CallsModel();
    _mainModel->name = tr("CallsWidget2");
//    if (bIsSessionOwnerCall)
//        _mainModel->addFilter(new IsSessionOwnerCall());

    connect(_mainModel,SIGNAL(layoutChanged()),this, SLOT(toggleEmptyWidget()));

    _mainView = new QTreeView(this);
    _mainView->setRootIsDecorated(false);
    _mainView->setModel(_mainModel);
    _mainView->setSelectionBehavior(QAbstractItemView::SelectRows);
//    _mainView->setItemDelegateForColumn(0,
//        new StateTimeDelegate(StateTimeDelegate::FormatMinutes, _mainView)
//        );

    //connect(_mainView.data(),SIGNAL(clicked      (QModelIndex)),this,SLOT(on_callChange(QModelIndex)));
    //connect(_mainView.data(),SIGNAL(doubleClicked(QModelIndex)),this,SLOT(on_callSelect(QModelIndex)));
    //connect(_mainModel.data(),SIGNAL(dropSelectedCall()),this,SLOT(onDropSelectedCall()));
    //connect(_mainModel.data(),SIGNAL(incomingCall(int)),this,SLOT(onIncomingCall(int)));

    _topLayout = new QVBoxLayout();
    _viewLayout = new QVBoxLayout();
    _viewLayout->addWidget(_mainView);
    _topLayout->addLayout(_viewLayout);
    setLayout(_topLayout);

}

//void CallsWidget2::on_callChange(QModelIndex index)
//{
//    Call call = index.data(AssociatedObjectRole).value<Call>();
//    if(call.isValid())
//        _appContext->onActiveCallChanged(call);
//}

//void CallsWidget2::on_callSelect(QModelIndex index)
//{
//    Call call = index.data(AssociatedObjectRole).value<Call>();
//    if(call.isValid())
//        _appContext->onActiveCallSelected(call);
//}


void CallsWidget2::setEmptyWidget(QWidget* w)
{
    if(_emptyWidget)
        /*layout()*/_viewLayout->removeWidget(_emptyWidget);
    _emptyWidget = w;
    /*layout()*/_viewLayout->addWidget(_emptyWidget);

    //_emptyWidget->hide();
    toggleEmptyWidget();
}

void CallsWidget2::toggleEmptyWidget()
{
    if(!_emptyWidget)
        return;

    if( _mainModel->rowCount() > 0 )
    {
        _emptyWidget->hide();
        _mainView   ->show();
    }
    else
    {
        _emptyWidget->show();
        _mainView   ->hide();
    }
}

//void CallsWidget2::onDropSelectedCall()
//{
//    QItemSelectionModel *selectionModel = _mainView->selectionModel();
//    if (selectionModel->hasSelection())
//    {
//        selectionModel->reset();
//    }
//}


void OperCallsWidget::onIncomingCall(const int &index)
{
    _incomingCallsID.push_back(index);
    QTimer::singleShot(500, this, SLOT(onIncomingCall2()));
}

void OperCallsWidget::onIncomingCall2()
{
    if (_incomingCallsID.empty())
        return;

    int index = _incomingCallsID.first();
    _incomingCallsID.removeFirst();

    Call call = _mainModel->callAt(index);
    if (!call.isValid())
        return;

    _appContext->onActiveCallChanged(call); // select incoming call

    QItemSelectionModel *selectionModel = _mainView->selectionModel();

    QModelIndex topLeft;
    QModelIndex bottomRight;

    topLeft = _mainModel->index(index, 0, QModelIndex());
    bottomRight = _mainModel->index(index,
                _mainModel->columnCount(QModelIndex())-1, QModelIndex());

    QItemSelection selection(topLeft, bottomRight);
    selectionModel->select(selection, QItemSelectionModel::Select);
}

OperCallsWidget::OperCallsWidget(QWidget *parent): CallsWidget2(parent)
{
    IAlarmHandler::init();

    _mainModel->addFilter(new IsSessionOwnerCall());
    _mainModel->name = tr("OperCallsWidget");

    _mainView->viewport()->setAcceptDrops(true);
    _mainView->setDragEnabled(true);
    _mainView->setDragDropMode(QAbstractItemView::InternalMove);

    _btnConnectCalls = new QPushButton();
    _btnConnectCalls->setText(tr("Соединить звонки"   ));
    _btnConnectCalls->setIcon(QIcon(":/resources/icons/new/conference01.png"));

    _btnConferenceCalls = new QPushButton();
    _btnConferenceCalls->setText(tr("Создать конференцию"));
    _btnConferenceCalls->setIcon(QIcon(":/resources/icons/new/conference02.png"));

    QWidget* spacer = new QWidget();
    spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    // toolBar is a pointer to an existing toolbar

    QHBoxLayout* bottomLayout = new QHBoxLayout();
    bottomLayout->addWidget(_btnConnectCalls);
    bottomLayout->addWidget(_btnConferenceCalls);
    bottomLayout->addWidget(spacer);
    _topLayout->addLayout(bottomLayout);

    connect(_btnConnectCalls   .data(),SIGNAL(clicked()),this,SLOT(on_btnConnectCallsCliked()));
    connect(_btnConferenceCalls.data(),SIGNAL(clicked()),this,SLOT(on_btnConferenceCallsCliked()));
    connect(_mainModel.data(),SIGNAL(connectCalls()),_btnConnectCalls.data(),SIGNAL(clicked()));
    connect(_mainModel.data(),SIGNAL(connectingCallsListChanged(int)),this,SLOT(updateWidgetControls()));

    connect(_mainModel.data(),SIGNAL(incomingCall(int)),this,SLOT(onIncomingCall(int)));

    updateWidgetControls();
//    _callForm = new TabsWindow();
//    //_callForm->setWindowFlags(_callForm->windowFlags() | Qt::WindowMinMaxButtonsHint);
//    //moveToScreenCenter(_callForm);
//    QHBoxLayout* tabsLayout = new QHBoxLayout();
//    tabsLayout->setMargin(0);
//    tabsLayout->addWidget(_callForm);
//    _topLayout->addLayout(tabsLayout);

//    _callForm->hide();

//    connect(_appContext.instance(),SIGNAL(onShowCallForm(ccappcore::Call)),this, SLOT(on_showCallForm(ccappcore::Call)));

}

void OperCallsWidget::on_btnConnectCallsCliked()
{
    CallsController::CallIDCollector *callsToConnect = _mainModel->connectingCalls();
//    if (callsToConnect->size() == 2) // connecting calls
//    {
//        _callsController->connectCalls((*callsToConnect)[0],(*callsToConnect)[1]);
//    }
//    else if (callsToConnect->size() > 2) // create conference
//    {

//    }

    int callId1 = (*callsToConnect)[0];
    int callId2 = (*callsToConnect)[1];

    if(QMessageBox::Ok == QMessageBox::question(this,
            tr( "Соединение звонков" ),
            tr( "Соединить звонок %1 со звонком %2 ?")
                       .arg(_callsController->callAbonPhone(_callsController->findByCallId(callId1)))
                       .arg(_callsController->callAbonPhone(_callsController->findByCallId(callId2))),
            QMessageBox::Ok | QMessageBox::Cancel ))
    {
        _callsController->connectCalls(callId1,callId2);
    }

    callsToConnect->clear();

    updateWidgetControls();
}

void OperCallsWidget::on_btnConferenceCallsCliked()
{
    CallsController::CallIDCollector *callsToConnect = _mainModel->connectingCalls();

    int operAnswer = QMessageBox::Cancel;

    if (callsToConnect->size() == 1)
    {
        int callId1 = (*callsToConnect)[0];
        operAnswer = QMessageBox::question(this,
                                           tr( "Соединение звонков" ),
                                           tr( "Добавить звонок %1 в конференцию ?")
                                                      .arg(_callsController->callAbonPhone(_callsController->findByCallId(callId1))),
                                           QMessageBox::Ok | QMessageBox::Cancel );
    }
    else
    {
        operAnswer = QMessageBox::question(this,
                    tr( "Соединение звонков" ),
                    tr( "Добавить звонки в конференцию ?"),
                    QMessageBox::Ok | QMessageBox::Cancel );
    }

    if(QMessageBox::Ok == operAnswer)
    {
        _callsController->createConference(callsToConnect);
    }

    callsToConnect->clear();
    updateWidgetControls();
}

void OperCallsWidget::on_showCallForm(ccappcore::Call c)
{
    //_callForm->show();
}

void OperCallsWidget::updateWidgetControls()
{
    //_btnConnectCalls->setText((_mainModel->connectingCalls()->size()>2)?CREATE_CONFERENCE:CONNECT_CALLS );

    _btnConnectCalls   ->setEnabled(_mainModel->connectingCalls()->size()  == 2);
    _btnConferenceCalls->setEnabled(_mainModel->connectingCalls()->size()  >= 2);
}

void OperCallsWidget::reset()
{
}

void OperCallsWidget::on_joinCalls(const int &conf_id)
{
    CallsController::CallIDCollector *callsToConnect = _mainModel->connectingCalls();

    int operAnswer = QMessageBox::Cancel;

    if (callsToConnect->size() == 1)
    {
        int callId1 = (*callsToConnect)[0];
        operAnswer = QMessageBox::question(this,
                                           tr( "Соединение звонков" ),
                                           tr( "Добавить звонок %1 в конференцию ?")
                                                      .arg(_callsController->callAbonPhone(_callsController->findByCallId(callId1))),
                                           QMessageBox::Ok | QMessageBox::Cancel );
    }
    else
    {
        operAnswer = QMessageBox::question(this,
                    tr( "Соединение звонков" ),
                    tr( "Добавить звонки в конференцию ?"),
                    QMessageBox::Ok | QMessageBox::Cancel );
    }

    if(QMessageBox::Ok == operAnswer)
    {
        _callsController->joinCallsToConference(callsToConnect,conf_id);
    }

    callsToConnect->clear();
    updateWidgetControls();
}

void OperCallsWidget::connectToTimer()
{
}

int OperCallsWidget::getTabIndex()const
{
    return MainWindow::TB_USERCALLS;
}

QIcon OperCallsWidget::getTabIcon(bool) const
{
    return QIcon(":/resources/icons/new/tab_oper16-3.png");
}

QueueCallsWidget::QueueCallsWidget(QWidget *parent)
    : CallsWidget2(parent),
     _isQueuedCall(new IsQueuedCall(_operSettings->showPrivateGroupsOnly()))
{
    IAlarmHandler::init();
    _mainModel->name = tr("QueueCallsWidget");
    //_mainModel->addFilter(new IsQueuedCall(_operSettings->showPrivateGroupsOnly()));
    //reset();
    _mainModel->addFilter(_isQueuedCall);
}

void QueueCallsWidget::reset()
{
    //_mainModel->addFilter(new IsQueuedCall(_operSettings->showPrivateGroupsOnly()));
    if (_isQueuedCall)
        _mainModel->removeFilter(_isQueuedCall);

    _isQueuedCall = FilteredModel<Call>::FilterCriteria (new IsQueuedCall(_operSettings->showPrivateGroupsOnly()));
    _mainModel->addFilter(_isQueuedCall);
}

void QueueCallsWidget::connectToTimer()
{
}

int QueueCallsWidget::getTabIndex()const
{
    return MainWindow::TB_QUEUECALLS;
}

QIcon QueueCallsWidget::getTabIcon(bool) const
{
    return QIcon(":/resources/icons/new/tab_turn16-3.png");
}

//

//TestModel::TestModel(QObject *parent)
//    :QAbstractListModel(parent)
//{
//    _columnsList << columnNameById(GroupColumn);
//    _columnsList << columnNameById(NameColumn);
//    _columnsList << columnNameById(TaskIdColumn);
//    _columnsList << columnNameById(StatusColumn);

//    for (int i=0;i<20;++i)
//        source.push_back(i);

//}

//int TestModel::rowCount(const QModelIndex &parent) const
//{
//    return source.count();
//}

//Qt::ItemFlags TestModel::flags(const QModelIndex &index) const
//{
//    Qt::ItemFlags defaultFlags = QAbstractListModel::flags(index);

//    if (index.isValid())
//        return /*Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled |*/ Qt::ItemIsUserCheckable |defaultFlags;
//    else
//        return /*Qt::ItemIsDropEnabled |*/ defaultFlags;
//}

////void TestModel::applyFilter(const PredicateGroup<Contact> &filter)
////{
////    emit layoutAboutToBeChanged();

////    _filter = filter;



////    emit layoutChanged();
////}

////void TestModel::reset()
////{
////    applyFilter(_filter);
////    sort(0);
////}

////void TestModel::sort(int column, Qt::SortOrder order)
////{
////    Q_UNUSED(column);
////    Q_UNUSED(order);

//////    emit layoutAboutToBeChanged();
//////    qSort(_groups.begin(),_groups.end(),ActiveGroupSortPredicate(&_activeGroups));

//////    emit layoutChanged();
////}

//QVariant TestModel::data(const QModelIndex &index, int role) const
//{
//    if (!index.isValid())
//        return QVariant();
//    if (index.row() >= source.size())
//        return QVariant();

//    if(index.column() <0 || index.column() >= columnCount() )
//        return QVariant();


//    if (role == Qt::DisplayRole)
//    {
//        return source[index.row()];
//    }

//    if(role == Qt::DecorationRole)
//    {
//    }

//    if(role == Qt::TextColorRole)
//    {
//        return QColor("gray");
//    }

//    if (role == Qt::CheckStateRole/* && index.column() == 0*/)
//    {
//        //return _activeGroups.contains(/*c*/groupId)?Qt::Checked:Qt::Unchecked;

//        return QVariant(Qt::Unchecked);
//    }


//    return QVariant();
//}


//QVariant TestModel::headerData(int section, Qt::Orientation orientation,
//                                         int role) const
//{
//       if (role != Qt::DisplayRole)
//            return QVariant();

//        if (orientation == Qt::Horizontal)
//        {
////            if (section > 0)
////            {
////                _manager->categoryIndicators()
////            }
//            //return columnNameById(_columnsList.value(section));
//            return _columnsList.value(section);
//        }
//        else
//            return QString("Row %1").arg(section);
//}

//QString TestModel::columnNameById(const qint32 &id) const
//{
//    switch(id)
//    {
//    case GroupColumn:
//        return tr("Группа");
//    default:
//        return tr("Неизвестно");
//    }

//    return QString();
//}

//int TestModel::columnCount(const QModelIndex &parent) const
//{
//    return _columnsList.size();// + _manager->categoryIndicators(GroupIndicator::CallStateIndicator).size();
//}

////void WGStatusModel::setOwnGroupsChecked()
////{
////    FilteredModel<Contact>::FilterCriteria filter (new IsGroupWithOper(true));
////    _activeGroups = _controller->findGroups(filter);

////    reset();
////}
//TestWidget::TestWidget(QWidget *parent) :
//    QWidget(parent)
//{
//    _mainModel = new TestModel(this);
//    //_mainModel->setOwnGroupsChecked();
//    _mainView = new QTreeView(this);
//    _mainView->setRootIsDecorated(false);
//    _mainView->setModel(_mainModel);
//    _mainView->setSelectionBehavior(QAbstractItemView::SelectRows);
//    //_mainView->setItemDelegate(new IndicatorItemDelegate(_mainView));

//    //connect(_mainView.data(),SIGNAL(doubleClicked(QModelIndex)),this,SLOT(on_contactSelect(QModelIndex)));

//    QVBoxLayout *topLayout = new QVBoxLayout();
//    topLayout->addWidget(_mainView);
//    setLayout(topLayout);

//    //reset(true);
//}
