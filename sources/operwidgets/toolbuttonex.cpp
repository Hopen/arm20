#include "toolbuttonex.h"
#include <QResizeEvent>

ToolButtonEx::ToolButtonEx(QWidget *parent) :
    QToolButton(parent)
{
    if(!parent)
        return;

    parent->installEventFilter(this);
}


bool ToolButtonEx::eventFilter(QObject *obj, QEvent *event)
{
    bool bResult = QObject::eventFilter(obj, event);
    if (event->type() != QEvent::Resize)
        return bResult;

    updateNarrowWidthLayout();
    return bResult;
}

void ToolButtonEx::updateNarrowWidthLayout()
{
    if(!parent())
        return;
    QSize parentBaseSize = parentWidget()->baseSize();
    QSize parentSize = parentWidget()->size();
    if(parentBaseSize.isEmpty())
        return;
    bool bNarrow = parentSize.width() < parentBaseSize.width();
    setToolButtonStyle( bNarrow ? Qt::ToolButtonIconOnly : Qt::ToolButtonTextBesideIcon );
    setMaximumSize( bNarrow ? minimumSize() : baseSize() );
}
