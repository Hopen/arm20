#include "transferwidget.h"
#include "appcontext.h"
//#include "contactswidget2.h"
//#include "contactsmanager.h"
#include "contactswidget3.h"
#include "lineeditwithhint.h"
#include "appcore/domainobjectfilter.h"
#include "appcore/useritemdatarole.h"

TransferContext::TransferContext(QWidget *parent)
    :QWidget(parent)
{
    _lastTransferChoice = 0;

    _menu = new QMenu(this);
    QAction * transToOper  = _menu->addAction(QIcon(":/resources/icons/new/forward 32.png"      ), tr("Перевести на оператора"));
    QAction * transToGroup = _menu->addAction(QIcon(":/resources/icons/new/forward group 32.png"), tr("Перевести на группу"));
    QAction * transToIVR   = _menu->addAction(QIcon(":/resources/icons/new/ivr on.png"          ), tr("Перевести на IVR"));
    QAction * transToPhone = _menu->addAction(QIcon(":/resources/icons/new/call back on.png"    ), tr("Перевести на номер"));

    connect(transToOper , SIGNAL(triggered()), this, SLOT(on_operTransfer ()));
    connect(transToGroup, SIGNAL(triggered()), this, SLOT(on_groupTransfer()));
    connect(transToIVR  , SIGNAL(triggered()), this, SLOT(on_ivrTransfer  ()));
    connect(transToPhone, SIGNAL(triggered()), this, SLOT(on_phoneTransfer()));

}

void TransferContext::on_defaulTransfer()
{
    switch (_lastTransferChoice)
    {
    case TC_OPER:
    {
        on_operTransfer();
        break;
    }
    case TC_GROUP:
    {
        on_groupTransfer();
        break;
    }
    case TC_IVR:
    {
        on_ivrTransfer();
        break;
    }
    default:
    {
        //on_bnTransferToOper_clicked();
    }
    }
}

void TransferContext::on_operTransfer()
{
    if (!_associatedCall.isValid())
        return;

    _lastTransferChoice = TC_OPER;

    _appContext->setCallToTransfer(_associatedCall);
    setupOpers();
}

void TransferContext::on_groupTransfer()
{
    if (!_associatedCall.isValid())
        return;

    _lastTransferChoice = TC_GROUP;

    _appContext->setCallToTransfer(_associatedCall);
    setupGroups();
}

void TransferContext::on_ivrTransfer()
{
    if (!_associatedCall.isValid())
        return;

    _lastTransferChoice = TC_IVR;

    _appContext->setCallToTransfer(_associatedCall);
    setupIVR();
}

void TransferContext::on_phoneTransfer()
{
    if (!_associatedCall.isValid())
        return;

    _appContext->setCallToTransfer(_associatedCall);
    setupPhone();
}

void TransferContext::setTransferCall(ccappcore::Call c)
{
    if(!c.isValid())
        return;
    _associatedCall = c;
}

void TransferContext::setupIVR()
{
    QSharedPointer<IvrScriptWidget> ivrTransfer (new IvrScriptWidget(_associatedCall));
    ivrTransfer->setWindowTitle(tr("Скрипты IVR"));
    connect(ivrTransfer.data(),SIGNAL(lineSelected(QString)),this,SIGNAL(ivrScriptSelected(QString)));
    ivrTransfer->exec();
}

void TransferContext::setupOpers()
{
    QSharedPointer<OperContactsWidget> operatorsTransfer(new OperContactsWidget());
    operatorsTransfer->setWindowTitle(tr("Операторы"));
    connect(operatorsTransfer.data(),SIGNAL(lineSelected(QVariant)),this,SIGNAL(operPhoneSelected(QVariant)));
    operatorsTransfer->exec();
}


void TransferContext::setupGroups()
{
    QSharedPointer<GroupContactsWidget> groupsTransfer (new GroupContactsWidget());
    groupsTransfer->setWindowTitle(tr("Группы"));
    connect(groupsTransfer.data(),SIGNAL(lineSelected(QVariant)),this,SIGNAL(groupPhoneSelected(QVariant)));
    groupsTransfer->exec();
}

void TransferContext::setupPhone()
{
    QSharedPointer <PhoneLineWidget> phoneTransfer (new PhoneLineWidget());
    phoneTransfer->setWindowTitle(tr("Перевести на номер"));
    connect(phoneTransfer.data(),SIGNAL(phoneSelected(QString)) ,this,SIGNAL(phoneLineSelected(QString)));
    phoneTransfer->exec();
}

/*****************************************************************
***********************  TransferWidget  *************************
*****************************************************************/

TransferWidget::TransferWidget(QWidget *parent)
    :QDialog(parent)
{
    _searchLine  = new LineEditWithHint(this);
    _searchLine->setAutoFillBackground(false);
    _searchLine->setHintText(tr("Поиск"));

    _proxyModel = new QSortFilterProxyModel;
    _proxyModel->setDynamicSortFilter(true);

    _mainView = new QTreeView(this);
    _mainView->setRootIsDecorated(false);
    _mainView->setSelectionBehavior(QAbstractItemView::SelectRows);
//    _mainView->setItemDelegateForColumn(0,
//        new StateTimeDelegate(StateTimeDelegate::FormatMinutes, _mainView)
//        );

    //_mainView->sortByColumn(1, Qt::AscendingOrder);
    _mainView->setModel(_proxyModel);

    _mainView->header()->setSortIndicatorShown(false);
    _mainView->header()->setClickable(true);
    _mainView->setSortingEnabled(true);


    _transferButton = new QToolButton(this);
    QPixmap pixmap = (QIcon(":/resources/icons/new/transfer on.png")).pixmap(QSize(16, 16), QIcon::Disabled, QIcon::On);
    _transferButton->setIcon(QIcon(pixmap));
    _transferButton->setDisabled(true);


    connect(_searchLine.data(), SIGNAL(textChanged(const QString &)), this, SLOT(on_searchLine_textChanged(QString)));
    connect(_transferButton.data(),SIGNAL (clicked(bool)), this, SLOT(on_bnTransfer_clicked(bool)));

    connect(_mainView.data(),SIGNAL(clicked(QModelIndex)),this,SLOT(on_lineSelect(QModelIndex)));
    connect(_mainView.data(),SIGNAL(doubleClicked(QModelIndex)),this,SLOT(on_lineSelected(QModelIndex)));

    QVBoxLayout *topLayout = new QVBoxLayout();
    topLayout->addWidget(_mainView);

    QHBoxLayout *bottomLayout = new QHBoxLayout();
    bottomLayout->addWidget(_searchLine);
    bottomLayout->addWidget(_transferButton);

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addLayout(topLayout);
    mainLayout->addLayout(bottomLayout);
    setLayout(mainLayout);
}


void TransferWidget::assignSourceModel(QAbstractItemModel *sourceModel, quint32 filterKey)
{
    _proxyModel->setSourceModel(sourceModel);
    _proxyModel->setFilterKeyColumn(filterKey);
}

void TransferWidget::on_searchLine_textChanged(QString searchText)
{
    if(!_searchLine->isHintVisible())
    {
        search(searchText);
    }
    else
        search(QString());
}

void TransferWidget::search(const QString &text)
{
    QRegExp regExp(text, Qt::CaseInsensitive, QRegExp::FixedString);
    _proxyModel->setFilterRegExp(regExp);
}

void TransferWidget::on_bnTransfer_clicked(bool)
{
    if (!_selectedLine.isEmpty())
    {
        emit lineSelected(_selectedLine);
    }


    if (_selectedObject.isValid())
    {
        emit lineSelected(_selectedObject);
    }

    done(QDialog::Accepted);
}

void TransferWidget::on_lineSelect(QModelIndex index)
{
    QVariant varCall = index.data(AssociatedObjectRole);
    if(varCall.isValid())
    {
        _selectedObject = varCall;
    }
    else
    {
        _selectedLine   = index.sibling(index.row(),0).data().toString();
    }

     QPixmap pixmap = (QIcon(":/resources/icons/new/transfer on.png")).pixmap(QSize(16, 16), QIcon::Normal, QIcon::On);
     _transferButton->setIcon(QIcon(pixmap));
     _transferButton->setEnabled(true);

}

void TransferWidget::on_lineSelected(QModelIndex index)
{
    //_selectedLine = index.sibling(index.row(),0).data().toString();
    QVariant varCall = index.data(AssociatedObjectRole);
    if(varCall.isValid())
    {
        _selectedObject = varCall;
        emit lineSelected(_selectedObject);
    }
    else
    {
        _selectedLine   = index.sibling(index.row(),0).data().toString();
        emit lineSelected(_selectedLine);
    }

    done(QDialog::Accepted);
}

/*****************************************************************
*********************  GroupContactsWidget  **********************
*****************************************************************/

GroupContactsWidget::GroupContactsWidget(QWidget *parent)
    :TransferWidget(parent)
{
    _mainModel = new ContactsModel(this);
    _mainModel->addFilter(new ccappcore::IsGroup());

    assignSourceModel(_mainModel, 0);
}

/*****************************************************************
*********************  OperContactsWidget  ***********************
*****************************************************************/

OperContactsWidget::OperContactsWidget(QWidget *parent)
    :TransferWidget(parent)
{
    _mainModel = new ContactsModel(this);
    _mainModel->addFilter(new ccappcore::IsOperator());

    assignSourceModel(_mainModel, 0);
}

/*****************************************************************
**********************  IvrScriptWidget  *************************
*****************************************************************/

IvrScriptWidget::IvrScriptWidget(ccappcore::Call c, QWidget *parent)
    :TransferWidget(parent)
{
    if (!c.isValid())
        return;
    Call::MultiInfo multiinfo;
    if (c.extraInfo(Call::CI_ScriptFuncName, multiinfo))
    {
        QStandardItemModel *model = new QStandardItemModel(0, 2, this);
        model->setHeaderData(0, Qt::Horizontal, QObject::tr("Название функции"));
        model->setHeaderData(1, Qt::Horizontal, QObject::tr("Описание"));

        foreach(const CallInfo& info, multiinfo)
        {
            QStringList list = info.value().split(";");

            model->insertRow(0);
            model->setData(model->index(0, 0), list[0]);
            model->setData(model->index(0, 1), list[1]);
        }

        assignSourceModel(model, 1);
    }

}

/*****************************************************************
***********************  PhoneLineWidget  ************************
*****************************************************************/

PhoneLineWidget::PhoneLineWidget(QWidget *parent)
    :QDialog(parent)
{
    _searchLine  = new LineEditWithHint(this);
    _searchLine->setAutoFillBackground(false);
    _searchLine->setHintText(tr("Номер"));

    _transferButton = new QToolButton(this);
    QPixmap pixmap = (QIcon(":/resources/icons/new/transfer on.png")).pixmap(QSize(16, 16), QIcon::Normal, QIcon::On);
    _transferButton->setIcon(QIcon(pixmap));
    _transferButton->setDisabled(true);

    connect(_searchLine.data(), SIGNAL(textChanged(const QString &)), this, SLOT(on_searchLine_textChanged(QString)));
    connect(_transferButton.data(),SIGNAL (clicked(bool)), this, SLOT(on_bnTransfer_clicked(bool)));
    connect(_searchLine.data(), SIGNAL(returnPressed()),  _transferButton.data(), SLOT(animateClick()) );

    QHBoxLayout *bottomLayout = new QHBoxLayout();
    bottomLayout->addWidget(_searchLine);
    bottomLayout->addWidget(_transferButton);

    setLayout(bottomLayout);
}

void PhoneLineWidget::on_searchLine_textChanged(QString searchText)
{
    _transferButton->setEnabled(!_searchLine->isHintVisible() && !searchText.isEmpty());
}

void PhoneLineWidget::on_bnTransfer_clicked(bool)
{
    if (!_searchLine->isHintVisible()  && !_searchLine->text().isEmpty())
    {
        qDebug() << "PhoneLineWidget::on_bnTransfer_clicked";
        emit phoneSelected(_searchLine->text());

        done(QDialog::Accepted);
    }
}
