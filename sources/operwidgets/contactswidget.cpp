#include "contactswidget.h"

#include "appcore/focuseventtranslator.h"
#include "dialingpausehandler.h"
#include "statetimedelegate.h"
#include "nocontactswidget.h"

#include "ui_contactswidget.h"

using namespace ccappcore;

ContactsWidget::ContactsWidget(QWidget *parent) :
    QWidget(parent),
    _ui(new Ui::ContactsWidget)
{
    _ui->setupUi(this);

    //setup contacts search bar
    connect(_ui->searchLine, SIGNAL(returnPressed()), _ui->bnMakeCall, SLOT(animateClick()));

    //set dialing pause when search line is focused and session owner is free
    FocusEventTranslator* focusHandler = new FocusEventTranslator(_ui->searchLine);
    connect(focusHandler, SIGNAL(focusIn()),
            _dialPauseHandler.instance(), SLOT(setPause()));

    //setup contacts view
    _ui->contactsView->setObjectName("contactsview");
    _ui->contactsView->setModel(_contactsManager->contactsModel());
    //_ui->contactsView->setHeaderHidden(true);
    _ui->contactsView->setItemDelegateForColumn(0,
        new StateTimeDelegate(StateTimeDelegate::FormatMinutes, _ui->contactsView)
        );
    _contactsManager->connectView(_ui->contactsView);


    _noContactsWidget = new NoContactsWidget(_ui->contactsView);
    connect(_noContactsWidget.data(),SIGNAL(anchorClicked(QUrl)),this, SLOT(on_bnMakeCall_clicked()));
    _ui->contactsView->setEmptyWidget(_noContactsWidget);

    connect(_appContext.instance(),SIGNAL(callToTransferChanged(ccappcore::Call)),
            this,SLOT(callToTransferChanged(ccappcore::Call)));
}

ContactsWidget::~ContactsWidget()
{
    delete _ui;
}

void ContactsWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        _ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void ContactsWidget::callToTransferChanged(ccappcore::Call callToTransfer)
{
    QAction* makeCallAction = callToTransfer.isValid()
                              ? _ui->actionTransferCall : _ui->actionMakeCall;
    _ui->bnMakeCall->setText(makeCallAction->text());
    _ui->bnMakeCall->setIcon(makeCallAction->icon());
}

void ContactsWidget::on_bnMakeCall_clicked()
{
    QString phone = _ui->searchLine->text();
    if(phone.isEmpty() || _ui->searchLine->isHintVisible())
    {
        _appContext->showDialer();
        return;
    }

    Call callToTransfer = _appContext->callToTransfer();
    if(callToTransfer.isValid())
        _callsController->transferToPhone(callToTransfer, phone);
    else
        _callsController->makeCall(phone);
    _ui->searchLine->clear();
}

void ContactsWidget::on_searchLine_textChanged(QString searchText)
{
    if(searchText.isEmpty())
    {
        _noContactsWidget->setHtml(tr("Контакты не найдены"));
    }
    else
    {
        QString action = _appContext->callToTransfer().isValid() ?
                         tr("Перевести на номер: ") : tr("Позвонить на номер: ");
        QString url = "callto:"+searchText;
        QString href = "<a href='" + url + "'>"+searchText+"</a>";
        QString msg = tr("Контакты не найдены.<br/><br/>%1<br/>%2<br/>")
              .arg(action).arg(href);
        _noContactsWidget->setHtml(msg);
    }


    if(!_ui->searchLine->isHintVisible())
    {
        _dialPauseHandler->setPause();
        _contactsManager->searchContactsAsync(searchText);
    }
    else
        _contactsManager->searchContactsAsync(QString());
}

void ContactsWidget::setFocusOnSearch()
{
    _ui->searchLine->setFocus(Qt::OtherFocusReason);
}
