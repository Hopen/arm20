#ifndef SCRIPTWIDGET_H
#define SCRIPTWIDGET_H

#include <QWidget>
#include "appcore/lateboundobject.h"
#include "appcore/call.h"


//class ScriptTableModel : public QAbstractTableModel {
//    Q_OBJECT

//public:
//    ScriptTableModel(QObject *parent = 0);

//    //bool insertColumn(int i, const QString &title);
//    //bool removeColumn(int i);

//    bool insertItem(int row, int column, const QString& value);
//    void addRow();

//    int rowCount(const QModelIndex &parent = QModelIndex()) const;
//    int columnCount(const QModelIndex &parent = QModelIndex()) const;

//    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
//    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

//    void search(QString filter);
//    void reset();

//private:
//    QStringList mColumns;
//    QList<QVariantList> mModelData;
//    QList<QVariantList> mViewData;
//};


using namespace ccappcore;

QT_BEGIN_NAMESPACE
class QTableView;
class QListView;
class QTreeView;
//class ScriptView;
class QToolButton;
class LineEditWithHint;
//class NoContactsWidget;
//class QVBoxLayout;
class QAbstractItemModel;
class QSortFilterProxyModel;
QT_END_NAMESPACE

class CallScriptWidget: public QWidget
{
    Q_OBJECT

public:
    CallScriptWidget(/*const ccappcore::Call& c,*/ QWidget *parent = NULL);
    void assignCall(const ccappcore::Call& c);
private:
    void setSourceModel(QAbstractItemModel *model);
    void search(const QString& text);
public slots:
    void on_bnTransfer_clicked(bool);
    void on_searchLine_textChanged(QString );
    //void toggleEmptyWidget();
    //void onScriptSelected(int, int);
    //void on_scriptSelect(int, int);
    void on_scriptSelect(QModelIndex);
    void onScriptSelected(QModelIndex);

signals:
    void scriptSelected(QString);
private:
    //QPointer < ScriptView       > _scriptView;
    QPointer < LineEditWithHint > _searchLine;
    //QPointer < NoContactsWidget > _noContactsWidget;
    QPointer < QToolButton      > _transferButton;
    //QPointer < QVBoxLayout      > _topLayout;

    //QPointer<  ScriptTableModel > _scriptModel;
    QPointer<  QTreeView       > _scriptView;

    QString                       _selectedScript;

    QPointer< QSortFilterProxyModel> _proxyModel;

};

#endif // SCRIPTWIDGET_H
