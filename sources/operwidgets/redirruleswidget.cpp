#include "QtGui"
#include "redirruleswidget.h"
#include "opertoolswidget.h"


RedirectCallsWidget::RedirectCallsWidget(QWidget *parent):QDialog(parent)
{
    QLabel* labelPhoneNum = new QLabel(tr("Номер телефона"));
    _phoneNum = new QLineEdit(this);
    //_phoneNum->setInputMask("+7 (999) 999-9999");
    labelPhoneNum->setBuddy(_phoneNum);

    QPushButton* btnOK = new QPushButton();
    btnOK->setText(tr("OK"));
    QPushButton* btnCancel = new QPushButton();
    btnCancel->setText(tr("Cancel"));

    connect(btnOK    ,SIGNAL(clicked()),this,SLOT(on_btnOkCliked    ()));
    connect(btnCancel,SIGNAL(clicked()),this,SLOT(on_btnCancelCliked()));

    QGridLayout *layout = new QGridLayout();
    layout->addWidget(labelPhoneNum, 0, 0, 1, 1);
    layout->addWidget(_phoneNum    , 0, 1, 1, 1);

    QHBoxLayout *bottomLayout = new QHBoxLayout();
    bottomLayout->addStretch();
    bottomLayout->addWidget(btnOK);
    bottomLayout->addWidget(btnCancel);
    bottomLayout->addStretch();


    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addLayout(layout);
    mainLayout->addLayout(bottomLayout);

    setLayout(mainLayout);

    setWindowTitle(tr("Переводить звонки"));
}

void RedirectCallsWidget::on_btnOkCliked()
{
    _inputPhoneNumber = _phoneNum->text();
    done(QDialog::Accepted);
}

void RedirectCallsWidget::on_btnCancelCliked()
{
    done(QDialog::Rejected);
}

RuleListModel::RuleListModel(QObject *parent): QAbstractListModel(parent)
{
    _columnsList << tr("id")
                 << tr("name")
                 << tr("ifType")
                 << tr("ifValue")
                 << tr("actType")
                 << tr("actValue")
                 << tr("maxCount");

    connect(this,SIGNAL(dataChanged(QModelIndex,QModelIndex)),this,SLOT(on_dataChanged(QModelIndex)));
}

int RuleListModel::rowCount(const QModelIndex &parent) const
{
    return _controller->rulesListCount();
}

int RuleListModel::columnCount(const QModelIndex &parent) const
{
    return _columnsList.size();
}

QVariant RuleListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (index.row() >= _controller->rulesListCount())
        return QVariant();
    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        RedirectListController::RedirRuleContainer *ruleList = _controller->rulesList();
        switch (index.column())
        {
        case IdColumn:
            return (*ruleList)[index.row()].id();
        case NameColumn:
            return (*ruleList)[index.row()].name();
        case IfTypeColumn:
            return (*ruleList)[index.row()].if_type();
        case IfValueColumn:
            return (*ruleList)[index.row()].if_value();
        case ActTypeColumn:
            return (*ruleList)[index.row()].act_type();
        case ActValueColumn:
            return (*ruleList)[index.row()].act_value();
        case MaxCountColumn:
            return (*ruleList)[index.row()].max_count();
        default:
            return QVariant();
        }

    }
    return QVariant();
}

QVariant RuleListModel::headerData(int section, Qt::Orientation orientation,
                                         int role) const
{
       if (role != Qt::DisplayRole)
            return QVariant();

        if (orientation == Qt::Horizontal)
            return _columnsList.value(section);
        else
            return QString("Row %1").arg(section);
}


bool RuleListModel::insertRows(int position, int rows, const QModelIndex &index)
{
    beginInsertRows(QModelIndex(), position, position+rows-1);

    for (int row = 0; row < rows; ++row)
    {
        _controller->createRule(position);
    }
    endInsertRows();
    return true;
}

bool RuleListModel::removeRows(int position, int rows, const QModelIndex &index)
{
    beginRemoveRows(QModelIndex(), position, position+rows-1);

    for (int row = 0; row < rows; ++row)
    {
        _controller->removeRule(position);
    }

    endRemoveRows();
    return true;
}

bool RuleListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole)
    {
        RedirectListController::RedirRuleContainer *ruleList = _controller->rulesList();
        switch (index.column())
        {
        case IdColumn:
        {
            (*ruleList)[index.row()].setId(value.toInt());
            break;
        }

        case NameColumn:
        {
            (*ruleList)[index.row()].setName(value.toString());
            break;
        }
        case IfTypeColumn:
        {
            (*ruleList)[index.row()].setIfType(value.toInt());
            break;
        }
        case IfValueColumn:
        {
            (*ruleList)[index.row()].setIfValue(value.toString());
            break;
        }
        case ActTypeColumn:
        {
            (*ruleList)[index.row()].setActType(value.toInt());
            break;
        }
        case ActValueColumn:
        {
            (*ruleList)[index.row()].setActValue(value.toString());
            break;
        }
        case MaxCountColumn:
        {
            (*ruleList)[index.row()].setMaxCount(value.toInt());
            break;
        }
        };

        emit dataChanged (index, index);
    }
    return false;
}

void RuleListModel::on_dataChanged(const QModelIndex &index)
{
//    if (index.isValid() && index.column() == NameColumn)
//        _controller->editRule(index.row());
}

RedirectRulesWidget::RedirectRulesWidget(QWidget *parent) :
    QWidget(parent)
{
    _mainModel = new RuleListModel(this);
    _mainView = new QTreeView(this);
    _mainView->setRootIsDecorated(false);
    _mainView->setModel(_mainModel);
    _mainView->setSelectionBehavior(QAbstractItemView::SelectRows);

    _mainView->resize(250, 50);

    QPushButton *newButton = new QPushButton(this);
    newButton->setText(tr("New"));
    QMenu * menu = new QMenu(this);
    QAction * call2NumRule       = menu->addAction(tr("Звонок на номер"));
    QAction * call2VoiceMailRule = menu->addAction(tr("Звонок на голосовую почту"));
    newButton->setMenu(menu);
    connect(call2NumRule       , SIGNAL(triggered()), this, SLOT(on_call2NumClicked      ()));
    connect(call2VoiceMailRule , SIGNAL(triggered()), this, SLOT(on_call2VoiceMailClicked()));


    _deleteButton = new QPushButton(this);
    _deleteButton->setText(tr("Delete"));
    _deleteButton->setDisabled(true);

    _editButton = new QPushButton(this);
    _editButton->setText(tr("Edit"));
    _editButton->setDisabled(true);

    connect(newButton    ,SIGNAL (clicked()), this, SLOT(on_btnNew_clicked   ()));
    connect(_editButton  ,SIGNAL (clicked()), this, SLOT(on_btnEdit_clicked  ()));
    connect(_deleteButton,SIGNAL (clicked()), this, SLOT(on_btnDelete_clicked()));

    connect(_mainView.data(),SIGNAL(clicked(QModelIndex)),this,SLOT(on_ruleSelect(QModelIndex)));
    //connect(_controller.instance(),SIGNAL(ruleAdded()),this,SLOT(on_ruleAdded()));

    QVBoxLayout *leftLayout = new QVBoxLayout();
    leftLayout->addWidget(_mainView);

    QVBoxLayout *rightLayout = new QVBoxLayout();
    rightLayout->addWidget(newButton);
    rightLayout->addWidget(_deleteButton);
    rightLayout->addWidget(_editButton);
    rightLayout->addStretch();

    QHBoxLayout *mainLayout = new QHBoxLayout();
    mainLayout->addLayout(leftLayout);
    mainLayout->addLayout(rightLayout);
    setLayout(mainLayout);
}

void RedirectRulesWidget::on_ruleSelect(QModelIndex)
{
    //_editButton->setEnabled(true);
    _deleteButton->setEnabled(true);
}

void RedirectRulesWidget::on_btnNew_clicked()
{

}

void RedirectRulesWidget::on_call2NumClicked()
{
//    _curRule = RedirectRule::PR_CALL2NUM;
//    _mainModel->insertRow(_mainModel->rowCount());
}

void RedirectRulesWidget::on_call2VoiceMailClicked()
{
//    _curRule = RedirectRule::PR_CALL2VM;
//    _mainModel->insertRow(_mainModel->rowCount());
}

void RedirectRulesWidget::on_btnEdit_clicked()
{

}

void RedirectRulesWidget::on_btnDelete_clicked()
{
    QModelIndex currentIndex = _mainView->currentIndex();
    _mainModel->removeRow(currentIndex.row());
}

//void RedirectRulesWidget::on_ruleAdded()
//{
//    int row = _mainModel->rowCount()-1;
//    RedirectListController::RedirRuleContainer *ruleList = _controller->rulesList();
//    switch (_curRule)
//    {
//    case RedirectRule::PR_CALL2NUM:
//    {
//        RedirectCallsWidget dialog;
//        if (dialog.exec() == QDialog::Rejected)
//        {
//            // remove string
//            _mainModel->removeRow(row);
//            return;
//        }
//        else
//        {
//            (*ruleList)[row].setName(SYSTEM_CALL2NUM_RULENAME);
//            (*ruleList)[row].setIfType(RedirectRule::RC_OperState);
//            (*ruleList)[row].setIfValue(tr("0,1,2,3"));
//            (*ruleList)[row].setActType(RedirectRule::RA_Phone);
//            (*ruleList)[row].setActValue(dialog.inputPhoneNumber());
//        }
//        break;
//    }
//    case RedirectRule::PR_CALL2VM:
//    {
//        (*ruleList)[row].setName(SYSTEM_CALL2VM_RULENAME);
//        (*ruleList)[row].setIfType(RedirectRule::RC_OperState);;
//        (*ruleList)[row].setIfValue(tr("0,1,2,3"));
//        (*ruleList)[row].setActType(RedirectRule::RA_Script);
//        (*ruleList)[row].setActValue(VOICEMAIL_OPERATOR_FUNC);
//        break;
//    }
//    default:
//    {
//        // unknown rule
//        return;
//    }

//    }


//    _controller->editRule(row);

//}

ProfileSystemMaster::ProfileSystemMaster(QWidget *parent):QWidget(parent)
{
    connect(_controller.instance(),SIGNAL(profileAdded()),this,SLOT(on_profileAdded()));
    connect(_controller.instance(),SIGNAL(profileEdited(qint32)),this,SLOT(on_profileEdited(qint32)));
    connect(_controller.instance(),SIGNAL(ruleAdded()),this,SLOT(on_ruleAdded()));
    connect(_controller.instance(),SIGNAL(ruleEdited(qint32)),this,SLOT(on_ruleEdited(qint32)));

//    RedirectListController::RedirProfileContainer *profileList = _controller->profilesList();
//    qint32 pos = _controller->findProfileByName(SYSTEM_CALL2NUM_RULENAME);
//    if (pos<_controller->profilesListCount())
//        _call2NumActive = !(*profileList)[pos].status().compare(tr("ON"));
//    pos = _controller->findProfileByName(SYSTEM_CALL2VM_RULENAME);
//    if (pos<_controller->profilesListCount())
//        _call2VoicemailActive = !(*profileList)[pos].status().compare(tr("ON"));
}

ProfileSystemMaster::~ProfileSystemMaster()
{
    disconnect(_controller.instance(),SIGNAL(profileAdded()),this,SLOT(on_profileAdded()));
    disconnect(_controller.instance(),SIGNAL(profileEdited(qint32)),this,SLOT(on_profileEdited(qint32)));
    disconnect(_controller.instance(),SIGNAL(ruleAdded()),this,SLOT(on_ruleAdded()));
    disconnect(_controller.instance(),SIGNAL(ruleEdited(qint32)),this,SLOT(on_ruleEdited(qint32)));

}

void ProfileSystemMaster::activateCall2NumProfile()
{
    _ruleId = RedirectRule::PR_CALL2NUM;
    activateSystemProfile(SYSTEM_CALL2NUM_RULENAME);
}

void ProfileSystemMaster::activateCall2VoicemailProfile()
{
    _ruleId = RedirectRule::PR_CALL2VM;
    activateSystemProfile(SYSTEM_CALL2VM_RULENAME);
}

void ProfileSystemMaster::activateSystemProfile(const QString &sysName)
{
    _name = sysName;
    _controller->removeProfile(_name);
    _controller->removeRule   (_name);

     _controller->createRule   (_controller->rulesListCount   ());
}

void ProfileSystemMaster::deactivateCall2NumProfile()
{
    deactivateSystemProfile(SYSTEM_CALL2NUM_RULENAME);
}

void ProfileSystemMaster::deactivateCall2VoicemailProfile()
{
    deactivateSystemProfile(SYSTEM_CALL2VM_RULENAME);
}

void ProfileSystemMaster::deactivateSystemProfile(const QString &sysName)
{
    RedirectListController::RedirProfileContainer *profileList = _controller->profilesList();
    qint32 pos = _controller->findProfileByName(sysName);
    if ((pos<_controller->profilesListCount()) && (!(*profileList)[pos].status().compare(tr("ON"))))
    {
        _controller->setActiveProfile(pos,true);
    }
}

bool ProfileSystemMaster::IsAnySystemProfileActive()const
{
    RedirectListController::RedirProfileContainer *profileList = _controller->profilesList();
    qint32 pos = _controller->findProfileByName(SYSTEM_CALL2NUM_RULENAME);
    if ((pos<_controller->profilesListCount()) && (!(*profileList)[pos].status().compare(tr("ON"))))
        return true;
    pos = _controller->findProfileByName(SYSTEM_CALL2VM_RULENAME);
    if ((pos<_controller->profilesListCount()) && (!(*profileList)[pos].status().compare(tr("ON"))))
        return true;

    return false;
}


void ProfileSystemMaster::on_profileAdded()
{
    int row = _controller->profilesListCount()-1;
    RedirectListController::RedirProfileContainer *profileList = _controller->profilesList();
    (*profileList)[row].setName(_name);
    (*profileList)[row].setTaskId(_taskId);

    _controller->editProfile(row);
}

void ProfileSystemMaster::on_profileEdited(qint32 position)
{
    _controller->setActiveProfile(position);
    emit profileActivated(_ruleId);
}

void ProfileSystemMaster::on_ruleAdded()
{
    int row = _controller->rulesListCount()-1;
    RedirectListController::RedirRuleContainer *ruleList = _controller->rulesList();
    if (!_name.compare(SYSTEM_CALL2NUM_RULENAME))
    {
        RedirectCallsWidget dialog;
        if (dialog.exec() == QDialog::Rejected)
        {
            // remove string
            _controller->removeRule(row);
            emit profileActivateError(tr("Canceled by user"));
            return;
        }
        else
        {
            (*ruleList)[row].setName(SYSTEM_CALL2NUM_RULENAME);
            (*ruleList)[row].setIfType(RedirectRule::RC_OperState);
            (*ruleList)[row].setIfValue(tr("0,1,2,3"));
            (*ruleList)[row].setActType(RedirectRule::RA_Phone);
            (*ruleList)[row].setActValue(dialog.inputPhoneNumber());
        }
    }
    else if (!_name.compare(SYSTEM_CALL2VM_RULENAME))
    {
        QString voiceMailId;
        qint32 pos = _controller->findScriptByName(VOICEMAIL_OPERATOR_FUNC);
        if (pos < _controller->scriptsListCount())
        {
            RedirectListController::ScriptEventContainer *scriptsList = _controller->scriptsList();
            voiceMailId = QString("%1").arg((*scriptsList)[pos].id());
        }
        else
        {
            emit profileActivateError(tr("Cannot find voice mail script"));
            return;
        }
        (*ruleList)[row].setName(SYSTEM_CALL2VM_RULENAME);
        (*ruleList)[row].setIfType(RedirectRule::RC_OperState);;
        (*ruleList)[row].setIfValue(tr("0,1,2,3"));
        (*ruleList)[row].setActType(RedirectRule::RA_Script);
        (*ruleList)[row].setActValue(voiceMailId);
    }

    _controller->editRule(row);
}

void ProfileSystemMaster::on_ruleEdited(qint32 position)
{
    RedirectListController::RedirRuleContainer *ruleList = _controller->rulesList();
    _taskId = (*ruleList)[position].id();
    _controller->createProfile(_controller->profilesListCount());
}
