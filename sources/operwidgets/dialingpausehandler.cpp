#include "dialingpausehandler.h"

using namespace ccappcore;

//const QString s_dialingPauseReason( QObject::tr("Набор номера",
//            "Статус оператора, устанавливаемый во время набора номера") );

static QString s_dialingPauseReason;

DialingPauseHandler::DialingPauseHandler(QObject *parent) :
    QObject(parent)
{
    s_dialingPauseReason = QString(tr("Набор номера",
                                      "Статус оператора, устанавливаемый во время набора номера") );

    pauseTimoutTimer.setSingleShot(true);
    //TODO: move timeout to settings?
    pauseTimoutTimer.setInterval(5000);

    connect(&pauseTimoutTimer, SIGNAL(timeout()),
            this, SLOT(clearPause()));
}

void DialingPauseHandler::setPause()
{
    if(!_statusOptions->pauseWhenDialing())
        return;

    Operator sessionOwner = _sessionController->sessionOwner();
    if ( sessionOwner.isFree() )
    {
        _operListController->reqChangeStatus( sessionOwner,
                                                Contact::Paused,
                                                s_dialingPauseReason );
        pauseTimoutTimer.start();;
    }
    if( sessionOwner.isPaused() &&
        sessionOwner.statusReason()==s_dialingPauseReason )
    {
        qDebug()<<"DialingPauseHandler: Dialing pause timout timer restarted.";
        //restart timer if we received setDualingPause while already on pause
        pauseTimoutTimer.start();
    }
}

void DialingPauseHandler::clearPause()
{
    Operator sessionOwner = _sessionController->sessionOwner();
    if( sessionOwner.isPaused() && sessionOwner.statusReason() == s_dialingPauseReason )
    {
        _operListController->reqChangeStatus( sessionOwner, Contact::Free );
    }
}
