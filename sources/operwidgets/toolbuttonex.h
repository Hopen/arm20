#ifndef TOOLBUTTONEX_H
#define TOOLBUTTONEX_H

#include <QToolButton>

class ToolButtonEx : public QToolButton
{
Q_OBJECT
public:
    explicit ToolButtonEx(QWidget *parent = 0);

protected:
    bool eventFilter(QObject *obj, QEvent *event);
    void updateNarrowWidthLayout();
};

#endif // TOOLBUTTONEX_H
