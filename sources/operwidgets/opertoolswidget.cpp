#include <QtGui>
#include "opertoolswidget.h"
#include "appcore/focuseventtranslator.h"
#include "appcontext.h"
#include "lineeditwithhint.h"
#include "dialingpausehandler.h"
#include "appcore/operationcontroller.h"
#include "contactswidget2.h"
#include "mainwindow.h"
//#include "scriptview.h"
#include "callscriptwidget.h"
//#include "dialpadbox.h"
#include "transferwidget.h"
#include "appcore/useritemdatarole.h"

OperToolBar::OperToolBar(bool minimize, QWidget *parent) :
    QToolBar(parent), _lastTransferChoice(TC_OPER)/*, _forcedRecord(false)*/
{

    _transferContext = new TransferContext();
    connect(_transferContext.data(),SIGNAL(operPhoneSelected (QVariant)),this,SLOT(onContactSelected(QVariant)));
    connect(_transferContext.data(),SIGNAL(groupPhoneSelected(QVariant)),this,SLOT(onContactSelected(QVariant)));
    connect(_transferContext.data(),SIGNAL(ivrScriptSelected (QString )),this,SLOT(onScriptSelected (QString )));
    connect(_transferContext.data(),SIGNAL(phoneLineSelected (QString )),this,SLOT(onPhoneSelected  (QString )));

    //setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    QGroupBox *phoneGroup = new QGroupBox(this);
    phoneGroup->setFixedWidth(200);
    //phoneGroup->setFixedHeight(40);

    _phoneLine  = new LineEditWithHint();
    _phoneLine->setAutoFillBackground(false);
    _phoneLine->setHintText(tr("<Телефон>"));

    // create digit keyboard
    for (int i = 0; i < NumDigitButtons-2; ++i)
    {
        digitButtons2.push_back(createButton(QString::number(i), SLOT(on_digitClicked())));
    }
    digitButtons2.push_back(createButton(tr("*"), SLOT(on_digitClicked())));
    digitButtons2.push_back(createButton(tr("#"), SLOT(on_digitClicked())));


    QGridLayout *buttonLayout = new QGridLayout;
    buttonLayout->setSizeConstraint(QLayout::SetFixedSize);
    buttonLayout->setSpacing(0);
    for (int i = 1; i < digitButtons2.size()-2; ++i)
    {
        int row = ((i - 1) / 3);
        int column = ((i - 1) % 3);
        buttonLayout->addWidget(digitButtons2[i].data(), row, column);
    }
    buttonLayout->addWidget(digitButtons2[digitButtons2.size()-2].data(), 3, 0);
    buttonLayout->addWidget(digitButtons2[0].data(), 3, 1);
    buttonLayout->addWidget(digitButtons2[digitButtons2.size()-1].data(), 3, 2);

    QMenu* menu = new QMenu();

    QWidgetAction * wa = new QWidgetAction(this);
    QWidget* backgroundWidget = new QWidget();
    backgroundWidget->setLayout(buttonLayout);
    wa->setDefaultWidget(backgroundWidget);

    menu->addAction(wa);

    QToolButton* tb = new QToolButton();
    tb->setIcon(QIcon(":/resources/icons/new/table.png"));
    tb->setAutoRaise(true);
    tb->setPopupMode( QToolButton::InstantPopup );
    tb->setMenu( menu );
    tb->setToolTip(tr("Набрать номер"));

    _cancelBtn  = new QToolButton();
    _cancelBtn->setIcon(QIcon(":/resources/icons/new/delete.png"));
    _cancelBtn->setAutoRaise(true);
    _cancelBtn->setToolTip(tr("Очистить"));


    QHBoxLayout *groupLayout = new QHBoxLayout();
    groupLayout->setSpacing(0);
    groupLayout->addWidget(tb);
    groupLayout->addWidget(_phoneLine.data());
    groupLayout->addWidget(_cancelBtn.data());
    phoneGroup->setLayout(groupLayout);
//    _dialpad = new QAction(QIcon(":/resources/icons/new/table.png"), "werwe", phoneGroup);
//    phoneGroup->addAction(_dialpad.data());
    connect(_cancelBtn.data(), SIGNAL (clicked(bool)), this, SLOT(on_bnCancel_clicked(bool)));

    addWidget(phoneGroup);
    addSeparator();

    setIconSize(minimize?QSize(24,24):QSize(32,32));

    //setup contacts search bar
    connect(_phoneLine.data(), SIGNAL(textChanged(const QString &)), this, SLOT(on_phoneLine_textChanged(QString)));

    //set dialing pause when search line is focused and session owner is free
    FocusEventTranslator* focusHandler = new FocusEventTranslator(_phoneLine);
    connect(focusHandler, SIGNAL(focusIn()),
            _dialPauseHandler.instance(), SLOT(setPause()));

    connect(this,SIGNAL(updateCallControls(ccappcore::Call)),this,SLOT(on_updateCallControls(ccappcore::Call)));

    setupTools();

//    pauseTimoutTimer.setSingleShot(true);
//    //TODO: move timeout to settings?
//    pauseTimoutTimer.setInterval(300);

//    connect(&pauseTimoutTimer, SIGNAL(timeout()),
//            this, SLOT(playDTMF()));


    connect(_operSettings.instance(),SIGNAL(OperOperationChanged()), this, SLOT(on_OperToolsChanged()));
    connect(_appContext.instance(), SIGNAL(activeCallChanged(ccappcore::Call)), this, SLOT(on_callActivate(ccappcore::Call)));
    //connect(_appContext.instance(), SIGNAL(activeCallSelected(ccappcore::Call)), this, SLOT(on_callSelected(ccappcore::Call)));
    connect(_callsController.instance(),/*SIGNAL(callStateChanged(ccappcore::Call))*/SIGNAL(callRemoved(ccappcore::Call)), this, SLOT(on_updateCallControls(ccappcore::Call)) );
    connect(_callsController.instance(),/*SIGNAL(callStateChanged(ccappcore::Call))*/SIGNAL(queueCallRemoved(ccappcore::Call)), this, SLOT(on_updateCallControls(ccappcore::Call)) );
    connect(_callsController.instance(),SIGNAL(recordCall(ccappcore::Call,bool,bool)),this, SLOT(on_c2o_record(ccappcore::Call,bool,bool)));

//    _contactsView = new ContactsWidget2();
//    connect(_contactsView.data(), SIGNAL(contactSelected(QString)),this,SLOT(on_contactSelected(QString)));

}

//void OperToolBar::setMainWindow(MainWindow * w)
//{
//    _mainWindow = w;
//}

void OperToolBar::on_bnCallBtn_clicked()
{

    //Call callToTransfer = _appContext->callToTransfer();
    //if(callToTransfer.isValid())
    //    _callsController->transferToPhone(callToTransfer, phone);
    //else

    if( _associatedCall.isAssigned() || _associatedCall.isQueued() )
        _callsController->answerCall(_associatedCall);
    else
    {
        QString phone = _phoneLine->text();
        if(phone.isEmpty() || _phoneLine->isHintVisible())
        {
            //_appContext->showDialer();
            return;
        }
        _dialPause->clearPause();
        _callsController->makeCall(phone);
        _phoneLine->clear();
    }
}

void OperToolBar::on_bnCallEndBtn_clicked()
{
    if (!_associatedCall.isValid())
        _associatedCall = _callsController->lastCall();

    if (_associatedCall.isValid())
        _callsController->endCall(_associatedCall);
}

void OperToolBar::on_bnHold_clicked(bool checked)
{
    _callsController->holdCall(_associatedCall, checked);
}

void OperToolBar::on_bnRec_clicked(bool checked)
{
    _callsController->recordCall(_associatedCall, checked);
}


//void OperToolBar::on_bnTransfer_clicked()
//{
//    switch (_lastTransferChoice)
//    {
//    case TC_OPER:
//    {
//        on_bnTransferToOper_clicked();
//        break;
//    }
//    case TC_GROUP:
//    {
//        on_bnTransferToGroup_clicked();
//        break;
//    }
//    case TC_IVR:
//    {
//        on_bnTransferToIVR_clicked();
//        break;
//    }
//    default:
//    {
//        on_bnTransferToOper_clicked();
//    }
//    }
//}

void OperToolBar::on_phoneLine_textChanged(QString text)
{
    Q_UNUSED(text)

    //startTimeout();

//    if(_associatedCall.isValid())
//    {
//        Call::CallState callState = _associatedCall.callState();
//        if( callState >= Call::CS_Assigned && callState < Call::CS_CallFailed )
//            _callsController->playDTMF(_associatedCall, text);
//    }

////    if(searchText.isEmpty())
////    {
////        _noContactsWidget->setHtml(tr("Контакты не найдены"));
////    }
////    else
////    {
////        QString action = _appContext->callToTransfer().isValid() ?
////                         tr("Перевести на номер: ") : tr("Позвонить на номер: ");
////        QString url = "callto:"+searchText;
////        QString href = "<a href='" + url + "'>"+searchText+"</a>";
////        QString msg = tr("Контакты не найдены.<br/><br/>%1<br/>%2<br/>")
////              .arg(action).arg(href);
////        _noContactsWidget->setHtml(msg);
////    }


//    if(!_phoneLine->isHintVisible())
//    //{
//        _dialPauseHandler->setPause();
//        //_contactsManager->searchContactsAsync(searchText);
//    //}
//    //else
//    //    _contactsManager->searchContactsAsync(QString());
}

void OperToolBar::on_OperToolsChanged()
{
    setupTools();
    updateGeometry();
}

void OperToolBar::setIconSize(QSize iconSize)
{
    _iconSize = iconSize;
    QToolBar::setIconSize(_iconSize);
}

void OperToolBar::setupTools()
{
    // delete all old tool buttons
//    for (qint8 i=0;i<_tools.size();++i)
//    {
//        ///*_toolsLayout->*/removeWidget(_tools[i]->Get());
//    }
    for (OperTools::const_iterator cit=_tools.begin();cit!=_tools.end();++cit)
    {
        removeAction(((QSharedPointer<QAction>)(*cit)).data());
    }
    _tools.clear();
    // install new
    QVariantList operlist = _operSettings->operationList();
    for (QVariantList::const_iterator cit=operlist.begin();cit!=operlist.end();++cit)
    {
        ccappcore::OperOperation op = cit->value<ccappcore::OperOperation>();
        //ccappcore::OperOperation op; *cit >> op;
        QPixmap pixmap = (QIcon(op.iconPath())).pixmap(_iconSize, QIcon::Normal, QIcon::On);
        SafeButton operAction (new QAction(QIcon(pixmap), op.text(), this));
        QVariant var;
        var.setValue<ccappcore::OperOperation>(op);
        operAction->setData(var);
        operAction->setStatusTip(op.text());
        switch (op.operNum())
        {
        case OO_CALL:
        {
            connect(operAction.data(), SIGNAL (triggered()), this, SLOT(on_bnCallBtn_clicked()));
            connect(_phoneLine.data(), SIGNAL(returnPressed()), operAction.data(), SLOT(trigger()));
            break;
        }
        case OO_CALLEND:
        {
            connect(operAction.data(), SIGNAL (triggered()), this, SLOT(on_bnCallEndBtn_clicked()));
            break;
        }
        case OO_WAITING:
        {
            operAction->setCheckable(true);
            connect(operAction.data(), SIGNAL(triggered(bool)), this, SLOT(on_bnHold_clicked(bool)));
            break;
        }
        case OO_REC:
        {
            operAction->setCheckable(true);
            connect(operAction.data(), SIGNAL(triggered(bool)), this, SLOT(on_bnRec_clicked(bool)));
            break;
        }
        case OO_TRANSFER:
        {
//            QMenu * menu = new QMenu(this);
//            QAction * transToOper  = menu->addAction(QIcon(":/resources/icons/oper_free_16x16.png"), tr("Перевести на оператора"));
//            QAction * transToGroup = menu->addAction(QIcon(":/resources/icons/group_16x16.png"    ), tr("Перевести на группу"));
//            QAction * transToIVR   = menu->addAction(QIcon(":/resources/icons/new/ivr on.png"     ), tr("Перевести на IVR"));
            operAction->setMenu(_transferContext->menu());
            connect(operAction.data(), SIGNAL(triggered()), _transferContext.data(), SLOT(on_defaulTransfer()));
//            //operAction->setCheckable(true);
//            connect(operAction.data(), SIGNAL(triggered()), this, SLOT(on_bnTransfer_clicked()));
//            connect(transToOper , SIGNAL(triggered()), this, SLOT(on_bnTransferToOper_clicked()));
//            connect(transToGroup, SIGNAL(triggered()), this, SLOT(on_bnTransferToGroup_clicked()));
//            connect(transToIVR  , SIGNAL(triggered()), this, SLOT(on_bnTransferToIVR_clicked()));
            break;
        }

        case OO_VOICEMAIL:
        {
            QMenu * menu = new QMenu(this);
            QAction * mailToOper  = menu->addAction(QIcon(":/resources/icons/new/message red.png"    ), tr("Личный"));
            QAction * mailToGroup = menu->addAction(QIcon(":/resources/icons/new/message yellow.png" ), tr("Группа"));
            QAction * mailToIVR   = menu->addAction(QIcon(":/resources/icons/new/message green.png"  ), tr("Общий"));
            operAction->setMenu(menu);
            connect(mailToOper , SIGNAL(triggered()), this, SLOT(on_bnVoiceMailToOperator_clicked()));
            connect(mailToGroup, SIGNAL(triggered()), this, SLOT(on_bnVoiceMailToGroup_clicked()));
            connect(mailToIVR  , SIGNAL(triggered()), this, SLOT(on_bnVoiceMailToSystem_clicked()));
            connect(operAction.data(), SIGNAL (triggered()), this, SLOT(on_bnVoiceMailTo_clicked()));
            break;
        }
        case OO_IVR:
        {
            //connect(operAction.data(), SIGNAL(triggered()), this, SLOT(on_bnTransferToIVR_clicked()));
            connect(operAction.data(), SIGNAL(triggered()), _transferContext.data(),SLOT(on_ivrTransfer()));
            break;
        }
        case OO_TRANSFER_FORWARD:
        {
            //connect(operAction.data(), SIGNAL(triggered()), this, SLOT(on_bnTransferToOper_clicked()));
            connect(operAction.data(), SIGNAL(triggered()), _transferContext.data(),SLOT(on_operTransfer()));
            break;
        }
        case OO_TRANSFER_GROUP:
        {
            //connect(operAction.data(), SIGNAL(triggered()), this, SLOT(on_bnTransferToGroup_clicked()));
            connect(operAction.data(), SIGNAL(triggered()), _transferContext.data(),SLOT(on_groupTransfer()));
            break;
        }
        case OO_TRANSFER_PHONE:
        {
            //connect(operAction.data(), SIGNAL(triggered()), this, SLOT(on_bnTransferToGroup_clicked()));
            connect(operAction.data(), SIGNAL(triggered()), _transferContext.data(),SLOT(on_phoneTransfer()));
            break;
        }
        };


        _tools.push_back(operAction);
    }

//    for (qint8 i=0;i<_tools.size();++i)
//        _tools[i]->SetBtnAvailable(true);


//    for (qint8 i=0;i<_tools.size();++i)
//    {
//        if (_tools[i]->Available())
//            /*_toolsLayout->*/addWidget(_tools[i]->Get());
//    }

    //addActions(_tools);
    for (OperTools::const_iterator cit=_tools.begin();cit!=_tools.end();++cit)
    {
        addAction(cit->data());
    }

    emit updateCallControls(ccappcore::Call());
}

void OperToolBar::on_bnCancel_clicked(bool)
{
    _phoneLine->clear();
    _dialPause->clearPause();
    //_appContext->showDialer();

}

void OperToolBar::on_digitClicked()
{
    Button *clickedButton = qobject_cast<Button *>(sender());
    QString digitValue = clickedButton->text();
    QString text = _phoneLine->isHintVisible()
                   ? QString() : _phoneLine->text();

    _dialPause->setPause();
    _phoneLine->setText(text+digitValue);;
    _soundManager->playDtmf(digitValue);

    //startTimeout();
}


void OperToolBar::on_callActivate(ccappcore::Call c)
{
    if(!c.isValid())
        return;
    _associatedCall = c;
    _callsController->reqCallInfo(_associatedCall);
    _transferContext->setTransferCall(c);

    emit updateCallControls(c);
}

//void OperToolBar::on_callSelected(ccappcore::Call c)
//{
//    if(!c.isValid())
//        return;
//    if (_associatedCall.isValid() && !_associatedCall.isOnHold())
//    {
//        // onHold
//        _callsController->holdCall(_associatedCall, true);
//    }
//    _associatedCall = c;
//    if (_associatedCall.isOnHold()) // on hold
//    {
//        _callsController->holdCall(_associatedCall, false);
//    }
//    else // incoming call
//    {
//        _callsController->reqCallInfo(_associatedCall);
//        on_bnCallBtn_clicked(); // just answer
//    }

//    emit updateCallControls(c);
//}


void OperToolBar::on_updateCallControls(ccappcore::Call /*c*/)
{
//    if (_associatedCall!=c)
//        return;
    foreach(const SafeButton& action, _tools)
    {
        ccappcore::OperOperation op = action->data().value<ccappcore::OperOperation>();

        switch (op.operNum()) // pressed on/off
        {
        case OO_REC:
        {
            action->setChecked(_associatedCall.isRecording());
            break;
        }
        case OO_WAITING:
        {
            action->setChecked(_associatedCall.isOnHold());
            break;
        }
        }


        switch (op.operNum()) // enable/disable
        {
        case OO_REC:
        {
            bool bCallEnd = (_associatedCall.isValid()) && (_associatedCall.callState() != Call::CS_Completed) && (_associatedCall.callState() != Call::CS_Ivr);
            //bCallEnd&=!c.isForcedRecording();
            bCallEnd&=!_associatedCall.isForcedRecording();
            bCallEnd&=_associatedCall.isTalking() || _associatedCall.isRecording();
            QPixmap pixmap = (QIcon(op.iconPath())).pixmap(QSize(32,32),
                                                       (bCallEnd)?QIcon::Active:QIcon::Disabled,
                                                       QIcon::On);
            action->setIcon(QIcon(pixmap));
            action->setEnabled(bCallEnd);
            break;
        }
        case OO_CALLEND:
        case OO_IVR:
        case OO_TRANSFER:
        case OO_VOICEMAIL:
        case OO_TRANSFER_FORWARD:
        case OO_TRANSFER_GROUP:
        case OO_TRANSFER_PHONE:

        {
            bool bCallEnd = (_associatedCall.isValid()) && (_associatedCall.callState() != Call::CS_Completed) && (_associatedCall.callState() != Call::CS_Ivr);
            QPixmap pixmap = (QIcon(op.iconPath())).pixmap(QSize(32,32),
                                                       (bCallEnd)?QIcon::Active:QIcon::Disabled,
                                                       QIcon::On);
            action->setIcon(QIcon(pixmap));
            action->setEnabled(bCallEnd);
            break;
        }
        case OO_WAITING:
        {
            bool bCallEnd = (_associatedCall.isValid()) && (_associatedCall.callState() != Call::CS_Completed) && (_associatedCall.callState() != Call::CS_Ivr);
            bCallEnd&=_associatedCall.isTalking() || _associatedCall.isOnHold();
            QPixmap pixmap = (QIcon(op.iconPath())).pixmap(QSize(32,32),
                                                       (bCallEnd)?QIcon::Active:QIcon::Disabled,
                                                       QIcon::On);
            action->setIcon(QIcon(pixmap));
            action->setEnabled(bCallEnd);
            break;

        }
        //case OO_CALLBACK:
        case OO_LOGIN:
        {
            action->setEnabled(false);
            break;
        }
        }

    }

}

//void OperToolBar::on_bnTransferToOper_clicked()
//{
//    if (_associatedCall.isValid())
//        _appContext->setCallToTransfer(_associatedCall);
//    _contactManager->hideGroups(true);

//    _lastTransferChoice = TC_OPER;
//    setupContacts();
//}

//void OperToolBar::on_bnTransferToGroup_clicked()
//{
//    if (_associatedCall.isValid())
//        _appContext->setCallToTransfer(_associatedCall);
//    _contactManager->hideOperators(true);

//    _lastTransferChoice = TC_GROUP;
//    setupContacts();
//}

//void OperToolBar::on_bnTransferToIVR_clicked()
//{
//    if (!_associatedCall.isValid())
//        return;

//    _appContext->setCallToTransfer(_associatedCall);

//    _lastTransferChoice = TC_IVR;
//    setupIVR();
//    //_callsController->transferToIVR(_associatedCall);
//}

//void OperToolBar::on_contactSelected(ccappcore::Contact c)
//{
//    _transferPhone = c.data(Contact::Phone).toString();
//}

//void OperToolBar::setupContacts()
//{
//    //_contactsFrame = _mainWindow->createDockWidget("contacts",tr("Контакты"), _contactsView.data());

//    _contactsFrame = new QDockWidget( tr("Контакты"), this );
//    _contactsFrame->setObjectName( "contacts" );
//    _contactsFrame->setSizePolicy( QSizePolicy::Ignored, QSizePolicy::Ignored );
//    //moveToScreenCenter( dw );

//    _contactsFrame->setWidget( _contactsView.data() );
//    _contactsFrame->setAllowedAreas( Qt::AllDockWidgetAreas );
//    _contactsFrame->setFeatures( QDockWidget::DockWidgetClosable
//                    |QDockWidget::DockWidgetMovable
//                    |QDockWidget::DockWidgetFloatable );


//    //connect(_contactsFrame.data(),SIGNAL(destroyed()),this,SLOT(on_contactsFrameClosed()));
//    _contactsFrame->layout()->setMargin(9);
//    _contactsFrame->setBaseSize(320,240);
//    _contactsFrame->setFloating(true);
//    _contactsFrame->show();
//}

//void OperToolBar::setupIVR()
//{
//    CallScriptWidget *scriptWidget = new CallScriptWidget();
//    scriptWidget->assignCall(_associatedCall);
//    connect(scriptWidget,SIGNAL(scriptSelected(QString)),this,SLOT(onScriptSelected(QString)));

//    _scriptFrame = new QDockWidget( tr("Скрипты"), this );
//    _scriptFrame->setObjectName( "IVRscripts" );
//    _scriptFrame->setSizePolicy( QSizePolicy::Ignored, QSizePolicy::Ignored );
//    //moveToScreenCenter( dw );

//    _scriptFrame->setWidget( scriptWidget );
//    _scriptFrame->setAllowedAreas( Qt::AllDockWidgetAreas );
//    _scriptFrame->setFeatures( QDockWidget::DockWidgetClosable
//                    |QDockWidget::DockWidgetMovable
//                    |QDockWidget::DockWidgetFloatable );

//    _scriptFrame->layout()->setMargin(9);
//    _scriptFrame->setBaseSize(320,240);
//    _scriptFrame->setFloating(true);
//    _scriptFrame->show();
//}

//void OperToolBar::onContactSelected(QVariant varContact)
//{
////    if (_contactsFrame.isNull())
////        return;

////    _contactsFrame->close();

//    //_operListController
//    //c.data(Contact::Phone).toString();
//    if(!varContact.isValid())
//        return;
//    Contact c = varContact.value<ccappcore::Contact>();
//    if(!c.isValid())
//        return;

//    QString phone = c.data(Contact::Phone).toString();;

//    Call callToTransfer = _appContext->callToTransfer();
//    if(callToTransfer.isValid())
//        _callsController->transferToPhone(callToTransfer, phone);

//}

void OperToolBar::onContactSelected(QVariant varContact)
{
    if(!varContact.isValid())
        return;
    Contact c = varContact.value<ccappcore::Contact>();
    if(!c.isValid())
        return;

    Call callToTransfer = _appContext->callToTransfer();
    if(!callToTransfer.isValid())
        return;

    if( c.contactId().type() == CT_Group )
    {
        _callsController->transferToGroup(callToTransfer, c);
    }
    else
    {
        _callsController->transferToOperator(callToTransfer, c);
    }
}

void OperToolBar::onPhoneSelected(QString phone)
{
    if (phone.isEmpty())
        return;

    Call callToTransfer = _appContext->callToTransfer();
    if(!callToTransfer.isValid())
    {
        qWarning()<<"OperToolBar::onPhoneSelected() - invalid call";
        return;
    }

    _callsController->transferToPhone(callToTransfer,phone);

}

void OperToolBar::transferToScript(const QString &scriptName)
{
    Call callToTransfer = _appContext->callToTransfer();
    if(callToTransfer.isValid())
        _callsController->transferToIVR(callToTransfer, scriptName);
}

void OperToolBar::onScriptSelected(QString scriptName)
{
//    _scriptFrame->close();
    transferToScript(scriptName);

}

void OperToolBar::on_bnVoiceMailToOperator_clicked()
{
    transferToScript(_lastVoiceMailChoice = VOICEMAIL_OPERATOR_FUNC);
}

void OperToolBar::on_bnVoiceMailToGroup_clicked()
{
    transferToScript(_lastVoiceMailChoice = VOICEMAIL_GROUP_FUNC);
}


void OperToolBar::on_bnVoiceMailToSystem_clicked()
{
    transferToScript(_lastVoiceMailChoice = VOICEMAIL_SYSTEM_FUNC);
}

void OperToolBar::on_bnVoiceMailTo_clicked()
{
    QString voicemailScript = _lastVoiceMailChoice;
    QString question;
    if (!voicemailScript.compare(VOICEMAIL_GROUP_FUNC))
    {
        question = tr("Перевести звонок на ящик группы,\nв которую по-умолчанию входит оператор?");
    }
    else if (!voicemailScript.compare(VOICEMAIL_SYSTEM_FUNC))
    {
        question = tr("Перевести звонок на общий ящик\nголосовой почты?");
    }
    else //  to define: transfer to own mail
    {
        question = tr("Перевести звонок на личную голосовую\nпочту оператора?");
        voicemailScript = VOICEMAIL_OPERATOR_FUNC;
    }

    int r = QMessageBox::question(this, tr("Перевод звонка"),
                                  question,
                                  QMessageBox::Yes|QMessageBox::No);
    if (r == QMessageBox::Yes)
    {
        transferToScript(voicemailScript);
    }
}

Button *OperToolBar::createButton(const QString &text, const char *member)
 {
     Button *button = new Button(text);
     connect(button, SIGNAL(clicked()), this, member);
     return button;
 }

void OperToolBar::on_c2o_record(Call call, bool /*onOff*/, bool /*forced*/)
{
    emit updateCallControls(call);
}

void OperToolBar::onSetPhoneText(QString text)
{
    _phoneLine->setText(text);
}

void OperToolBar::connectView(QAbstractItemView* view)
{
    connect(view,SIGNAL(clicked(QModelIndex)),
            this, SLOT(onItemClicked(QModelIndex)));
}

void OperToolBar::onItemClicked(const QModelIndex &index)
{
    int type = index.data(AddressTypeRole).value<int>();
    QString phone = index.data(AddressValueRole).value<QString>();
    if(type == ETI_PHONE && !phone.isEmpty())
        onSetPhoneText(phone);

}

//void OperToolBar::startTimeout()
//{
//    pauseTimoutTimer.start();
//}


//void OperToolBar::playDTMF()
//{
//    if (_phoneLine->isHintVisible())
//        return;

//    QString text = _phoneLine->text();
////    if(_associatedCall.isValid() && !text.isEmpty() && _lastDTMF!=text)
////    {
////        _lastDTMF = text;
////        //Call::CallState callState = _associatedCall.callState();
////        //if( callState >= Call::CS_Assigned && callState < Call::CS_CallFailed )
////        if( _associatedCall.callState() == Call::CS_Connected)
////            _callsController->playDTMF(_associatedCall, text);
////    }

//    if(!_associatedCall.isValid())
//        return;

//    if (!text.isEmpty() && _lastDTMF!=text)
//    {
//        QString newDTMF = text;
//        if (!_lastDTMF.isEmpty() && 0 == newDTMF.indexOf(_lastDTMF))
//        {
//            newDTMF = newDTMF.right(newDTMF.length() - _lastDTMF.length());
//        }

//        if( _associatedCall.callState() == Call::CS_Connected)
//            _callsController->playDTMF(_associatedCall, newDTMF);

//    }

//    _lastDTMF = text;

//}
