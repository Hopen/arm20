#ifndef DIALINGPAUSEHANDLER_H
#define DIALINGPAUSEHANDLER_H

#include <QObject>

#include "appcore/sessioncontroller.h"
#include "appcore/operlistcontroller.h"
#include "appcore/statusoptions.h"

using namespace ccappcore;

class DialingPauseHandler : public QObject
{
Q_OBJECT
public:
    explicit DialingPauseHandler(QObject *parent = 0);

signals:

public slots:
    void setPause();
    void clearPause();
private:
    LateBoundObject<SessionController> _sessionController;
    LateBoundObject<OperListController> _operListController;
    LateBoundObject<StatusOptions> _statusOptions;
    QTimer pauseTimoutTimer;
};

#endif // DIALINGPAUSEHANDLER_H
