#ifndef BBLISTWIDGET_H
#define BBLISTWIDGET_H

#include <QWidget>
#include "appcore/appsettings.h"
#include "appcore/lateboundobject.h"
#include "appcore/spamlistcontroller.h"
#include "appcore/operlistcontroller.h"
#include "appcontext.h"

using namespace ccappcore;

class QTreeView;

//class LabelDelegate : public QStyledItemDelegate
//{
//    Q_OBJECT

//public:
//    LabelDelegate(QObject *parent = 0);

//    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
//                          const QModelIndex &index) const;

//    void setEditorData(QWidget *editor, const QModelIndex &index) const;
////    void setModelData(QWidget *editor, QAbstractItemModel *model,
////                      const QModelIndex &index) const;

//    void updateEditorGeometry(QWidget *editor,
//        const QStyleOptionViewItem &option, const QModelIndex &index) const;

//private:
//    LateBoundObject<SpamListController> _controller;
//};

class BBListModel : public QAbstractListModel
{
    Q_OBJECT

public:

    enum ColDef
    {
        GroupIdColumn = 0,
        MsgIdColumn,
        EndColumn,
        OperIdColumn,
        BeginColumn,
        TextColumn
    };

        BBListModel(QObject *parent = 0);


        int rowCount(const QModelIndex &parent = QModelIndex()) const;
        QVariant data(const QModelIndex &index, int role) const;
        QVariant headerData(int section, Qt::Orientation orientation,
                            int role = Qt::DisplayRole) const;
        int columnCount ( const QModelIndex & parent = QModelIndex() ) const;

        bool insertRows(int position, int rows, const QModelIndex &index = QModelIndex());
        bool removeRows(int position, int rows, const QModelIndex &index = QModelIndex());

        Qt::ItemFlags flags(const QModelIndex &index) const;
        bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
public slots:
        void reset();

protected:
        QString columnNameById(const qint32 &id) const;


signals:
        void messageAdd(QModelIndex index);

public slots:
        //void on_dataChanged(const QModelIndex &index);

    private:
        LateBoundObject<SpamListController> _controller;
        LateBoundObject<OperListController> _operListController;
        //QStringList _columnsList;
        QList<ColDef> _columnsList;
        SpamContainer _messages;
};

class BBListWidget : //public QWidget
        public IAlarmHandler
{
    Q_OBJECT
public:
    explicit BBListWidget(QWidget *parent = 0);
    

    // IAlarmHandler interface
    void connectToTimer();
    int getTabIndex()const;
    QIcon getTabIcon (bool _default = false) const;

signals:
    void messageHasRead();
    void resize();
public slots:
    void on_messageSelect(QModelIndex);
    //void on_messageAdded(QModelIndex);

    //void testSpamController();

protected:
    virtual bool event(QEvent *e);


private:
    QPointer<  QAbstractListModel  > _mainModel;
    QPointer<  QTreeView           > _mainView;

    LateBoundObject<SpamListController> _controller;

    
};

#endif // BBLISTWIDGET_H
