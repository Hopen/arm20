#ifndef TEXTEDITEX_H
#define TEXTEDITEX_H

#include <QTextEdit>

class TextEditEx : public QTextEdit
{
Q_OBJECT
public:
    explicit TextEditEx(QWidget *parent = 0);

protected:
    virtual void mouseMoveEvent(QMouseEvent *e);
    virtual void mouseReleaseEvent(QMouseEvent *e);
signals:

public slots:

};

#endif // TEXTEDITEX_H
