#ifndef NOCONTACTSWIDGET_H
#define NOCONTACTSWIDGET_H

#include <QWidget>
#include <QUrl>
#include <QPointer>

namespace Ui {
    class NoContactsWidget;
}

class NoContactsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit NoContactsWidget(QWidget *parent = 0);
    ~NoContactsWidget();

signals:
    void anchorClicked(QUrl);
public slots:
    void setHtml(const QString& html);

private:
    Ui::NoContactsWidget *ui;

private slots:
};


class QTextBrowser;

class NoContactsWidget2: public QWidget
{
    Q_OBJECT
public:
    explicit NoContactsWidget2(QWidget *parent = 0);
    //~NoContactsWidget2();

signals:
    void anchorClicked(QUrl);
public slots:
    void setHtml(const QString& html);

private:
    QPointer<QTextBrowser> _textBrowser;
};

#endif // NOCONTACTSWIDGET_H
