#ifndef TABWIDGETEX_H
#define TABWIDGETEX_H

#include "operwidgets.h"

class TabWidgetEx : public QTabWidget
{
Q_OBJECT
public:
    explicit TabWidgetEx(QWidget *parent = 0);

    void removeCloseButton(int index);

    bool autoHideTabBar() const { return _autoHideTabBar; }
    void setAutoHideTabBar(bool hide);

    bool deleteWidgetOnClose() const { return _deleteWidgetOnClose; }
    void setDeleteWidgetOnClose(bool val) { _deleteWidgetOnClose = val; }

protected:
    virtual void tabInserted(int index);
    virtual void tabRemoved(int index);

signals:

public slots:

private slots:
    void tabCloseRequested(int index);
    
private:
    bool _autoHideTabBar; //hide tab bar when zero or one tab
    void maybeHideTabBar();    

    bool _deleteWidgetOnClose;
    void maybeDeleteTabWidget(QWidget* tab);
};

#endif // TABWIDGETEX_H
