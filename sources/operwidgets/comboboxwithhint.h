#ifndef COMBOBOXWITHHINT_H
#define COMBOBOXWITHHINT_H

#include "operwidgets_global.h"
#include "lineeditwithhint.h"

class ComboBoxWithHint : public QComboBox
{
Q_OBJECT

Q_PROPERTY(bool isHintVisible READ isHintVisible);
public:
    ComboBoxWithHint(QWidget *parent = 0);
    ~ComboBoxWithHint();

    void setHintText(const QString& s);

    void showHint();
    bool isHintVisible() const;


    int timeoutInSeconds() const { return _timeoutInSeconds; }
    void setTimeout(int seconds) { _timeoutInSeconds = seconds; }

signals:

    //fires when Enter key pressed while editing text,
    //when the drop down list item is selected,
    //when keypress timeout accurs,
    //or when focus is lost
    void textCommitting(QString);
    void returnPressed();
protected:
    void showEvent(QShowEvent *);
    void focusInEvent(QFocusEvent *);
    void focusOutEvent(QFocusEvent *);
    void keyPressEvent(QKeyEvent *);
private slots:
    void fireTextChanged();

private:
    int _timeoutInSeconds; //keypress timeout
    bool _timeoutEnabled;
    QTimer _timeoutTimer;
    QString _hintText;
    bool _hintMode;

private slots:
    void on_textChanged(const QString &text);

};

#endif // COMBOBOXWITHHINT_H
