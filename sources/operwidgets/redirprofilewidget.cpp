#include "QtGui"
#include "redirprofilewidget.h"
#include "redirectprofileeditwidget.h"
#include "redirruleswidget.h"


void RadioDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStyleOptionViewItemV4 opt = option;
    initStyleOption(&opt, index);
    QRadioButton radio(tr("text"),0);

    QStyle *style = opt.widget ? opt.widget->style() : QApplication::style();
    style->drawControl(QStyle::CE_RadioButton, &opt, painter, &radio);

////    QStyleOptionViewItemV4 opt = QStyleOptionViewItemV4(option);
////        initStyleOption(&opt, index);
////        //opt.checkState = Qt::Checked;
////        const QWidget *widget = opt.widget;
////        QStyle *style = widget ? widget->style() : QApplication::style();
////        painter->save();
////        style->drawControl(QStyle::CE_RadioButton, &opt, painter);
////        painter->restore();
//    QString text = index.model()->data(index).toString();
//        painter->save();
//        QPoint bottomLeft = option.rect.bottomLeft();
//        int size = option.rect.height()/2;
//        QFont font = painter->font();
//        font.setPixelSize(size);
//        painter->setFont(font);
//        painter->setPen(QPen(QColor("black")));
//        painter->drawText(bottomLeft, text);
//        painter->restore();
}

QSize RadioDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    return QSize(80,20);
}
//QWidget* RadioDelegate::createEditor(QWidget *parent,
//                      const QStyleOptionViewItem &option,
//                      const QModelIndex &index) const

//{
//    QCheckBox *checkBox = new QCheckBox(parent);
//    return checkBox;
//}



/******************************/
/********  class MODEL ********/
/******************************/

ProfileListModel::ProfileListModel(QObject *parent): QAbstractListModel(parent)
{
    _columnsList << tr("id")
                 << tr("name")
                 << tr("taskId")
                 << tr("status");

    connect(this,SIGNAL(dataChanged(QModelIndex,QModelIndex)),this,SLOT(on_dataChanged(QModelIndex)));
    connect(this,SIGNAL(activateProfile(int, bool)),this,SLOT(on_activateProfile(int, bool)));
//    connect(_controller.instance(),
//            SIGNAL(profileEdited(qint32)),
//            this,
//            SIGNAL(layoutChanged());
}

int ProfileListModel::rowCount(const QModelIndex &parent) const
{
    return _controller->profilesListCount();
}

int ProfileListModel::columnCount(const QModelIndex &parent) const
{
    return _columnsList.size();
}

QVariant ProfileListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (index.row() >= _controller->profilesListCount())
        return QVariant();
    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        RedirectListController::RedirProfileContainer *profileList = _controller->profilesList();
        switch (index.column())
        {
        case IdColumn:
            return (*profileList)[index.row()].id();
        case NameColumn:
            return (*profileList)[index.row()].name();
        case TaskIdColumn:
            return (*profileList)[index.row()].taskId();
        case StatusColumn:
            return (*profileList)[index.row()].status();
        default:
            return QVariant();
        }

    }

    if ((role == Qt::CheckStateRole) && (index.column() == StatusColumn))
    {
        RedirectListController::RedirProfileContainer *profileList = _controller->profilesList();
        QString text = (*profileList)[index.row()].status();
        return (!!text.compare("OFF"))?Qt::Checked:Qt::Unchecked;
    }
    return QVariant();
}

QVariant ProfileListModel::headerData(int section, Qt::Orientation orientation,
                                         int role) const
{
       if (role != Qt::DisplayRole)
            return QVariant();

        if (orientation == Qt::Horizontal)
            //return QString("Column %1").arg(section);
            return _columnsList.value(section);
        else
            return QString("Row %1").arg(section);
}

Qt::ItemFlags ProfileListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractItemModel::flags(index) | Qt::ItemIsUserCheckable;
}

bool ProfileListModel::insertRows(int position, int rows, const QModelIndex &index)
{
    beginInsertRows(QModelIndex(), position, position+rows-1);

    for (int row = 0; row < rows; ++row)
    {
        //_controller->addInvalidProfile(position);
        _controller->createProfile(position);
    }
    endInsertRows();
    return true;
}

bool ProfileListModel::removeRows(int position, int rows, const QModelIndex &index)
{
    beginRemoveRows(QModelIndex(), position, position+rows-1);

    for (int row = 0; row < rows; ++row)
    {
        _controller->removeProfile(position);
    }

    endRemoveRows();
    return true;
}

bool ProfileListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole)
    {
        RedirectListController::RedirProfileContainer *profileList = _controller->profilesList();
        switch (index.column())
        {
        case IdColumn:
        {
            (*profileList)[index.row()].setId(value.toInt());
            break;
        }

        case NameColumn:
        {
            (*profileList)[index.row()].setName(value.toString());
            break;
        }
        case TaskIdColumn:
        {
            (*profileList)[index.row()].setTaskId(value.toInt());
            break;
        }
        case StatusColumn:
        {
            (*profileList)[index.row()].setStatus(value.toString());
            break;
        }
        };

        emit dataChanged (index, index);
    }
    if (index.isValid() && role == Qt::CheckStateRole && index.column() == StatusColumn)
    {
//        RedirectListController::RedirProfileContainer *profileList = _controller->profilesList();
////        foreach (RedirectProfile *p, profileList)
////        {

////        }
//        for (int i=0;i<profileList->size();++i)
//        {
//            if (i == index.row())
//            {
//                QString status = (*profileList)[i].status();
//                if (!status.compare(tr("OFF")))
//                    (*profileList)[i].setStatus(tr("ON"));
//                else
//                {
//                    (*profileList)[i].setStatus(tr("OFF")); // turning off
//                    break;
//                }

//                continue;
//            }
//            (*profileList)[i].setStatus(tr("OFF"));
//        }
//        //reset();
//        emit activateProfile(index.row());
         QString text = index.model()->data(index).toString();
         if (!text.compare(tr("OFF")))
             emit activateProfile(index.row(), false);
         else
             emit activateProfile(index.row(), true);

         emit layoutChanged();
    }
    return false;
}

void ProfileListModel::on_dataChanged(const QModelIndex &index)
{
    if (index.isValid() && index.column() == TaskIdColumn)
        //_controller->createProfile(index.row());
        _controller->editProfile(index.row());
}

void ProfileListModel::on_activateProfile(int pos, bool turnOFF)
{
    _controller->setActiveProfile(pos, turnOFF);
}

RedirProfileWidget::RedirProfileWidget(/*const QPointer<RedirectListController> controller,*/ QWidget *parent):
    QWidget(parent)/*,
    _controller(controller)*/
{
    _scriptModel = new ProfileListModel(/*controller,*/ this);
    _scriptView = new QTreeView(this);
    _scriptView->setRootIsDecorated(false);
    _scriptView->setModel(_scriptModel);
    _scriptView->setSelectionBehavior(QAbstractItemView::SelectRows);

    //_scriptView->resize(250, 50);
    //_scriptView->setItemDelegateForColumn(ProfileListModel::StatusColumn, new RadioDelegate());

    //QStringList items;
    //items <<  tr("") << tr("Звонок на номер") << tr("Звонок на голосовую почту");
    //_taskModel = new QStringListModel(items, this);
    _taskModel = new RuleListModel(this);

    QToolButton *newButton = new QToolButton(this);
    newButton->setText(tr("New"));
    newButton->setAutoRaise(true);

    /*QToolButton **/_deleteButton = new QToolButton(this);
    _deleteButton->setText(tr("Delete"));
    _deleteButton->setAutoRaise(true);
    _deleteButton->setDisabled(true);

    /*QToolButton **/ _editButton = new QToolButton(this);
    _editButton->setText(tr("Edit"));
    _editButton->setAutoRaise(true);
    _editButton->setDisabled(true);

    connect(newButton   ,SIGNAL (clicked(bool)), this, SLOT(on_btnNew_clicked   (bool)));
    connect(_editButton  ,SIGNAL (clicked(bool)), this, SLOT(on_btnEdit_clicked  (bool)));
    connect(_deleteButton,SIGNAL (clicked(bool)), this, SLOT(on_btnDelete_clicked(bool)));

    connect(_scriptView.data(),SIGNAL(clicked(QModelIndex)),this,SLOT(on_profileSelect(QModelIndex)));
    //connect(_controller.instance(),SIGNAL(createSysProfile(QString)),this,SLOT(on_sysProfileCreateNeeded(QString)));

    QVBoxLayout *leftLayout = new QVBoxLayout();
    leftLayout->addWidget(_scriptView);

//    QHBoxLayout *bottomLayout = new QHBoxLayout();
//    bottomLayout->addWidget(_searchLine);
//    bottomLayout->addWidget(_transferButton);

    QVBoxLayout *rightLayout = new QVBoxLayout();
    rightLayout->addWidget(newButton);
    rightLayout->addWidget(_deleteButton);
    rightLayout->addWidget(_editButton);
    rightLayout->addStretch();

    QHBoxLayout *mainLayout = new QHBoxLayout();
    mainLayout->addLayout(leftLayout);
    mainLayout->addLayout(rightLayout);
    setLayout(mainLayout);


}


void RedirProfileWidget::on_btnNew_clicked(bool)
{
    if (!_scriptModel->insertRow(_scriptModel->rowCount()))
        return;
    //_curIndex = _scriptModel->index(_scriptModel->rowCount()-1);
    QModelIndex currentIndex = _scriptModel->index(_scriptModel->rowCount()-1);
    RedirectProfileEditWidget dialog(_scriptModel,_taskModel, currentIndex);
    if (dialog.exec() == QDialog::Rejected)
    {
            //removeRows
        _scriptModel->removeRow(_scriptModel->rowCount()-1);
    }
    else
    {

        _scriptView->scrollTo(currentIndex);
    }
 }

void RedirProfileWidget::on_btnEdit_clicked(bool)
{
    RedirectProfileEditWidget dialog(_scriptModel,_taskModel, _scriptView->currentIndex());

    if (dialog.exec() == QDialog::Accepted)
    {
        // setData
    }
    else
    {
        // removeRows
    }
}

void RedirProfileWidget::on_btnDelete_clicked(bool)
{
    QModelIndex currentIndex = _scriptView->currentIndex();
    _scriptModel->removeRow(currentIndex.row());
}

void RedirProfileWidget::on_profileSelect(QModelIndex /*index*/)
{
    //_curIndex = index;
    _editButton->setEnabled(true);
    _deleteButton->setEnabled(true);
}

//void RedirProfileWidget::on_sysProfileCreateNeeded(QString name)
//{
//    if (!_scriptModel->insertRow(_scriptModel->rowCount()))
//        return;
//    //_curIndex = _scriptModel->index(_scriptModel->rowCount()-1);
//    QModelIndex currentIndex = _scriptModel->index(_scriptModel->rowCount()-1);
//    RedirectProfileEditWidget dialog(_scriptModel,_taskModel, currentIndex);
//    if (dialog.exec() == QDialog::Rejected)
//    {
//            //removeRows
//        _scriptModel->removeRow(_scriptModel->rowCount()-1);
//    }
//    else
//    {

//        _scriptView->scrollTo(currentIndex);
//    }

//}

