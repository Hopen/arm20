#ifndef CONTACTSTREEVIEW_H
#define CONTACTSTREEVIEW_H

#include "operwidgets_global.h"
#include "contactsmanager.h"

class ContactsView : public QTreeView
{
    Q_OBJECT;
public:
    ContactsView(QWidget* parent = NULL);

    virtual void setModel(QAbstractItemModel *model);
    void setFirstColumnSpanned(bool spanned);

    QWidget* emptyWidget() { return _emptyWidget; }
    void setEmptyWidget(QWidget* w);

    virtual void doItemsLayout();

protected:

    void showEvent(QShowEvent *);
    void resizeEvent(QResizeEvent *event);
private slots:

    void bestFitColumns();
    void setFirstColumnSpanned();
    void updateStateTime();
    void toggleEmptyWidget();



private:
    bool _firstColumnSpanned;
    QPointer<QWidget> _emptyWidget;
};

#endif // CONTACTSTREEVIEW_H
