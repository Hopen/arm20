#ifndef TRAYICON_H
#define TRAYICON_H

#include "operwidgets_global.h"

#include "appcore/callscontroller.h"
#include "appcontext.h"
#include "opersettings.h"
#include "contactstatusmenu.h"
#include "appcore/spamlistcontroller.h"
#include "appcore/JabberController.h"

using namespace ccappcore;

class TrayIcon : public QSystemTrayIcon
{
    enum eBallonMessageType
    {
        BMT_UNKNOWN = 0,
        BMT_CALL,
        BMT_CHAT
    };

Q_OBJECT
public:
    explicit TrayIcon(QObject *parent = 0);
    ~TrayIcon();
signals:

public slots:

private:
    LateBoundObject < SessionController  > _sessionController;
    LateBoundObject < CallsController    > _callsController;
    LateBoundObject < AppContext         > _appContext;
    LateBoundObject < OperSettings       > _operSettings;
    LateBoundObject < SpamListController > _spamController;
    LateBoundObject < OperListController > _opersController;
    LateBoundObject < JabberController   > _jabberController;
    QMenu _contextMenu;
    ContactStatusMenu _statusMenu;
    ccappcore::Call _associatedMessageCall;
    SpamMessage     _associatedMessageChat;
    bool            _mainWindowMinimized;
    int             _messageType;
private slots:
    void activate(QSystemTrayIcon::ActivationReason/* = QSystemTrayIcon::DoubleClick*/);
    void operSessionStarted();
    void showCallMessage(ccappcore::Call);
    void showSpamMessage(SpamMessage);
    void on_callStarted(ccappcore::Call);
    void on_messageClicked();
    void on_windowMinimized(bool);
    void on_quit();
};

#endif // TRAYICON_H
