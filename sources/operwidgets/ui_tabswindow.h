/********************************************************************************
** Form generated from reading UI file 'tabswindow.ui'
**
** Created: Tue 26. Mar 13:54:37 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TABSWINDOW_H
#define UI_TABSWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QTabWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TabsWindow
{
public:
    QHBoxLayout *horizontalLayout;
    QTabWidget *tabs;
    QWidget *tab0;

    void setupUi(QWidget *TabsWindow)
    {
        if (TabsWindow->objectName().isEmpty())
            TabsWindow->setObjectName(QString::fromUtf8("TabsWindow"));
        TabsWindow->resize(480, 360);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(TabsWindow->sizePolicy().hasHeightForWidth());
        TabsWindow->setSizePolicy(sizePolicy);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/resources/icons/appicon_32x32.png"), QSize(), QIcon::Normal, QIcon::Off);
        TabsWindow->setWindowIcon(icon);
        TabsWindow->setProperty("sizeGripEnabled", QVariant(false));
        horizontalLayout = new QHBoxLayout(TabsWindow);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        tabs = new QTabWidget(TabsWindow);
        tabs->setObjectName(QString::fromUtf8("tabs"));
        tabs->setTabPosition(QTabWidget::North);
        tabs->setTabShape(QTabWidget::Rounded);
        tabs->setTabsClosable(true);
        tabs->setMovable(false);
        tab0 = new QWidget();
        tab0->setObjectName(QString::fromUtf8("tab0"));
        tabs->addTab(tab0, QString());

        horizontalLayout->addWidget(tabs);


        retranslateUi(TabsWindow);

        QMetaObject::connectSlotsByName(TabsWindow);
    } // setupUi

    void retranslateUi(QWidget *TabsWindow)
    {
        TabsWindow->setWindowTitle(QApplication::translate("TabsWindow", "\320\227\320\262\320\276\320\275\320\272\320\270", 0, QApplication::UnicodeUTF8));
        tabs->setTabText(tabs->indexOf(tab0), QApplication::translate("TabsWindow", "Tab 2", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class TabsWindow: public Ui_TabsWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TABSWINDOW_H
