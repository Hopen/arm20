/********************************************************************************
** Form generated from reading UI file 'opt_shortcuts.ui'
**
** Created: Tue 26. Mar 13:54:36 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPT_SHORTCUTS_H
#define UI_OPT_SHORTCUTS_H

#include <Qt3Support/Q3MimeSourceFactory>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTreeWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OptShortcuts
{
public:
    QGridLayout *gridLayout;
    QTreeWidget *treeShortcuts;
    QHBoxLayout *hboxLayout;
    QPushButton *edit;
    QPushButton *remove;
    QSpacerItem *spacerItem;

    void setupUi(QWidget *OptShortcuts)
    {
        if (OptShortcuts->objectName().isEmpty())
            OptShortcuts->setObjectName(QString::fromUtf8("OptShortcuts"));
        OptShortcuts->resize(400, 300);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(OptShortcuts->sizePolicy().hasHeightForWidth());
        OptShortcuts->setSizePolicy(sizePolicy);
        OptShortcuts->setMinimumSize(QSize(400, 300));
        gridLayout = new QGridLayout(OptShortcuts);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(9, 9, 9, 9);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        treeShortcuts = new QTreeWidget(OptShortcuts);
        treeShortcuts->setObjectName(QString::fromUtf8("treeShortcuts"));
        treeShortcuts->setLayoutDirection(Qt::LeftToRight);
        treeShortcuts->setAlternatingRowColors(true);
        treeShortcuts->setRootIsDecorated(true);
        treeShortcuts->setAllColumnsShowFocus(true);
        treeShortcuts->setColumnCount(2);

        gridLayout->addWidget(treeShortcuts, 0, 0, 1, 2);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        edit = new QPushButton(OptShortcuts);
        edit->setObjectName(QString::fromUtf8("edit"));

        hboxLayout->addWidget(edit);

        remove = new QPushButton(OptShortcuts);
        remove->setObjectName(QString::fromUtf8("remove"));

        hboxLayout->addWidget(remove);

        spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);


        gridLayout->addLayout(hboxLayout, 1, 0, 2, 2);


        retranslateUi(OptShortcuts);

        QMetaObject::connectSlotsByName(OptShortcuts);
    } // setupUi

    void retranslateUi(QWidget *OptShortcuts)
    {
        OptShortcuts->setWindowTitle(QApplication::translate("OptShortcuts", "OptShortcutsUI", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem = treeShortcuts->headerItem();
        ___qtreewidgetitem->setText(1, QApplication::translate("OptShortcuts", "\320\241\320\276\321\207\320\265\321\202\320\260\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        ___qtreewidgetitem->setText(0, QApplication::translate("OptShortcuts", "\320\224\320\265\320\271\321\201\321\202\320\262\320\270\320\265", 0, QApplication::UnicodeUTF8));
        edit->setText(QApplication::translate("OptShortcuts", "\320\240\320\265\320\264\320\260\320\272\321\202\320\270\321\200\320\276\320\262\320\260\321\202\321\214...", 0, QApplication::UnicodeUTF8));
        remove->setText(QApplication::translate("OptShortcuts", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class OptShortcuts: public Ui_OptShortcuts {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPT_SHORTCUTS_H
