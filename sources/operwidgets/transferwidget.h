#ifndef TRANSFERWIDGET_H
#define TRANSFERWIDGET_H

#include <QWidget>
#include <QDialog>
#include "appcore/lateboundobject.h"
#include "appcore/callscontroller.h"

using namespace ccappcore;

QT_BEGIN_NAMESPACE
class QMenu;
class QToolButton;
class LineEditWithHint;
class QTreeView;
class QSortFilterProxyModel;
class QDockWidget;
QT_END_NAMESPACE

class AppContext;
//class ContactsWidget2;
//class ContactsManager;
class ContactsModel;

enum E_TRANSFER_CHOICE
{
    TC_OPER = 1,
    TC_GROUP,
    TC_IVR
};

class TransferContext : public QWidget
{
Q_OBJECT
public:
    explicit TransferContext(QWidget *parent = 0);
    virtual ~TransferContext(){}

    QMenu* menu()const{return _menu;}

private:
    void setupOpers();
    void setupGroups();
    void setupIVR();
    void setupPhone();

    //QDockWidget* createDockWidget(const QString& title, const QString& className, QWidget* parent = 0);

signals:
    void operPhoneSelected (QVariant);
    void groupPhoneSelected(QVariant);
    void ivrScriptSelected (QString);
    void phoneLineSelected (QString);

public slots:
    void on_defaulTransfer();
    void on_operTransfer();
    void on_groupTransfer();
    void on_ivrTransfer();
    void on_phoneTransfer();

    void setTransferCall(ccappcore::Call c);

private:
    LateBoundObject< AppContext     > _appContext;
    //LateBoundObject< ContactsManager> _contactManager;

    //QPointer< ContactsWidget2 > _contactsView;
    //QPointer< QMenu           > _menu;

    quint32         _lastTransferChoice;
    QMenu*          _menu;
    ccappcore::Call _associatedCall;

//    QPointer<QDockWidget>  _opersFrame;
//    QPointer<QDockWidget>  _groupsFrame;
//    QPointer<QDockWidget>  _scriptsFrame;
};

class TransferWidget: public QDialog
{
    Q_OBJECT
public:
    explicit TransferWidget(QWidget *parent = 0);
    virtual ~TransferWidget(){}

    void assignSourceModel(QAbstractItemModel *sourceModel, quint32 filterKey);
private:
    void search(const QString& text);

public slots:
    void on_bnTransfer_clicked(bool);
    void on_searchLine_textChanged(QString );
    void on_lineSelect(QModelIndex);
    void on_lineSelected(QModelIndex);


signals:
    void lineSelected(QString);
    void lineSelected(QVariant);

protected:

    QPointer<  QTreeView           > _mainView;

    QPointer < LineEditWithHint > _searchLine;
    QPointer < QToolButton      > _transferButton;
    QPointer< QSortFilterProxyModel> _proxyModel;
    QString                       _selectedLine;
    QVariant                      _selectedObject;

};


class OperContactsWidget: public TransferWidget
{
    Q_OBJECT
public:
    explicit OperContactsWidget(QWidget *parent = 0);
private:
    ContactsModel* _mainModel;

};

class GroupContactsWidget: public TransferWidget
{
    Q_OBJECT
public:
    explicit GroupContactsWidget(QWidget *parent = 0);
private:
    ContactsModel* _mainModel;
};

class IvrScriptWidget: public TransferWidget
{
    Q_OBJECT
public:
    explicit IvrScriptWidget(ccappcore::Call c, QWidget *parent = 0);
};

class PhoneLineWidget: public QDialog
{
    Q_OBJECT
public:
    explicit PhoneLineWidget(QWidget *parent = 0);
    virtual ~PhoneLineWidget(){}

signals:
    void phoneSelected(QString phone);

public slots:
    void on_bnTransfer_clicked(bool);
    void on_searchLine_textChanged(QString );

protected:
    QPointer < LineEditWithHint > _searchLine;
    QPointer < QToolButton      > _transferButton;

    //LateBoundObject <CallsController> _callsController;
};

#endif // TRANSFERWIDGET_H
