#include "contactstatusmenu.h"

using namespace ccappcore;

ContactStatusMenu::ContactStatusMenu(QWidget* parent)
        : QMenu(parent)
        , _actionGroup(this)
        , _profileActionGroup(this)
        //, _ruleMaster(NULL)
{
    setTitle(tr("Статус"));

     _actionOnline           = append( Contact::Free             , _operListController->statusAsString(Contact::Free)/*tr("Онлайн"          )*/);
     _actionPaused           = append( Contact::Paused           , _operListController->statusAsString(Contact::Paused)/*tr("На паузе"        )*/);
     //_actionReadyByNumber    = append( RedirectRule::PR_CALL2NUM , tr("На номер"        ));
     //_actionReadyByVoicemail = append( RedirectRule::PR_CALL2VM  , tr("Голосовая почта" ));


    connect(_statusReasons.instance(), SIGNAL(dataChanged()),
            this, SLOT(loadCustomReasons()));
    loadCustomReasons();
}

ContactStatusMenu::~ContactStatusMenu()
{
}

void ContactStatusMenu::unbindOperator()
{
    _operListController->disconnect(this);
    _oper = Operator();
}

void ContactStatusMenu::loadCustomReasons()
{
    while(!_customReasons.empty())
    {
        QAction* action = *_customReasons.begin();
        _actionGroup.removeAction(action);
        _customReasons.removeOne(action);
        delete action;
    }

    if (_statusReasons->isEnableOtherPauseReasons())
    {
        foreach(QString reason, _statusReasons->pauseReasons() )
            _customReasons.append(append(Contact::Paused, reason ));
    }

    foreach(QString reason, _statusReasons->pauseGlobalReasons() )
        _customReasons.append(append(Contact::Paused, reason ));

}

void ContactStatusMenu::bindOperator(ccappcore::Operator op)
{
    unbindOperator();

    _oper = op;

    connect(_operListController.instance(), SIGNAL(contactStatusChanged(ccappcore::Contact)),
            this, SLOT(operStatusChanged(ccappcore::Contact)));
    connect(_operListController.instance(), SIGNAL(dataChanged()),
            this, SLOT(operListChanged()));

    operStatusChanged(op);
}

QAction* ContactStatusMenu::append( Contact::ContactStatus status,
                                      const QString& displayName )
{
    QAction* action = new QAction( displayName, this);
    action->setData((int)status);    
    action->setCheckable(true);
    action->setIcon(_operListController->statusIcon(status));
    connect(action, SIGNAL(triggered()), this, SLOT(actionTriggered()));
    _actionGroup.addAction(action);
    addAction(action);
    return action;
}

QAction* ContactStatusMenu::append( ccappcore::RedirectRule::ePredefineRules status,
                                      const QString& displayName )
{
    QAction* action = new QAction( displayName, this);
    action->setData((int)status);
    action->setCheckable(true);
    action->setIcon(_redirListController->statusIcon(status));
    connect(action, SIGNAL(triggered()), this, SLOT(profileTriggered()));
    _profileActionGroup.addAction(action);
    addAction(action);
    return action;
}

void ContactStatusMenu::requestStatus(
        Contact::ContactStatus newStatus,
        const QString& newReason)
{
    if(!_oper.isValid())
    {
        qWarning() << "OperStatusMenu::requestStatus(...) failed."
                <<" Unknown Operator.";
        return;
    }

    if(_oper.status()!=Contact::Busy && newStatus != Contact::Free)
        _operListController->reqChangeStatus(_oper, newStatus, newReason );
    else
        _operListController->reqChangeStatus(_oper, newStatus, "");

}

void ContactStatusMenu::actionTriggered()
{
    QAction* action = qobject_cast<QAction*>(sender());
    if(!action)
    {
        qWarning()<<"OperStatusMenu::actionTriggered() - Invalid signal sender";
        return;
    }

    Contact::ContactStatus status =
            static_cast<Contact::ContactStatus>(action->data().toInt());
    QString statusReason = action->text();
    requestStatus(status, statusReason);

//    bool bIsAnySystemProfileActive = false;
//    if (true)
//    {
//        ProfileSystemMaster master;
//        bIsAnySystemProfileActive = master.IsAnySystemProfileActive();
//    }
//    if (bIsAnySystemProfileActive)
//        operStatusChanged(_oper);
}

void ContactStatusMenu::profileTriggered()
{
    QAction* action = qobject_cast<QAction*>(sender());
    if(!action)
    {
        qWarning()<<"OperStatusMenu::actionTriggered() - Invalid signal sender";
        return;
    }
//    RedirectRule::ePredefineRules rule =
//            static_cast<RedirectRule::ePredefineRules>(action->data().toInt());
//    if (_ruleMaster)
//        delete _ruleMaster;
//    _ruleMaster = new ProfileSystemMaster();
//    connect(_ruleMaster,SIGNAL(profileActivated(qint32)),this,SLOT(on_profileActivated(qint32)));
//    connect(_ruleMaster,SIGNAL(profileActivateError(QString)),this,SLOT(on_profileActivateError(QString)));

//    switch (rule)
//    {
//    case RedirectRule::PR_CALL2NUM:
//    {
//        _ruleMaster->activateCall2NumProfile();
//        break;
//    }
//    case RedirectRule::PR_CALL2VM:
//    {
//        _ruleMaster->activateCall2VoicemailProfile();
//        break;
//    }

//    }

}

void ContactStatusMenu::operStatusChanged(ccappcore::Contact op)
{
    if(op != _oper || !op.isValid() )
        return;

    //qDebug() << "ContactStatusMenu::operStatusChanged";
    //qDebug() << tr("Oper \"%1\" status: %2").arg(op.name()).arg(op.status());

//    if (true) // delete immediately
//    {
//        ProfileSystemMaster master;
//        master.deactivateCall2NumProfile();
//        master.deactivateCall2VoicemailProfile();
//    }
    QAction* statusAction = 0;
    if(op.isFree() || op.isBusy())
    {
        statusAction = _actionOnline;
        _actionOnline->setIcon(_operListController->statusIcon(op.status()));
        if(op.statusReason()!="call" && op.statusReason()!="login")
        {
            //hack: do not display some predefined statuses
            _actionOnline->setText( op.statusReason().isEmpty()
                                ? _operListController->statusAsString(Contact::Free)/*tr("Онлайн")*/ : op.statusReason() );
        }
    }
    else
    {
        foreach (QAction* action, _actionGroup.actions())
        {
            Contact::ContactStatus status = (Contact::ContactStatus) action->data().toInt();
            QString reason = action->text();

            if( status == op.status() )
            {
                if(!statusAction)
                    statusAction = action;

                if( !reason.compare( op.statusReason(), Qt::CaseInsensitive ) )
                    statusAction = action;
            }
        }
    }

    if(statusAction)
    {
        statusAction->setChecked(true);
        setIcon(statusAction->icon());
    }

    emit checkedActionChanged(statusAction);
    //qDebug() << tr("emit checkedActionChanged(%1)").arg(op.status());
}


void ContactStatusMenu::operListChanged()
{
    bindOperator(_oper);
}

void ContactStatusMenu::on_profileActivated(qint32 _ruleId)
{
//    if (_ruleMaster)
//    {
//        delete _ruleMaster;
//        _ruleMaster = 0;
//    }
    QAction *statusAction = NULL;
    foreach (QAction* action, _profileActionGroup.actions())
    {
        RedirectRule::ePredefineRules rule = (RedirectRule::ePredefineRules) action->data().toInt();

        if( rule == _ruleId )
        {
            if(!statusAction)
                statusAction = action;
        }
    }


    if(statusAction)
    {
        statusAction->setChecked(true);
        setIcon(statusAction->icon());
    }

    emit checkedActionChanged(statusAction);
}

void ContactStatusMenu::on_profileActivateError(QString errorText)
{
    QMessageBox::warning(0,tr("Warning"),errorText);
//    if (_ruleMaster)
//    {
//        delete _ruleMaster;
//        _ruleMaster = 0;
//    }
}
