#ifndef MESSAGETRAYICON_H
#define MESSAGETRAYICON_H

#include <QSystemTrayIcon>

#include "operwidgets_global.h"
#include "appcontext.h"
#include "appcore/spamlistcontroller.h"

using namespace ccappcore;

class MessageTrayIcon : public QSystemTrayIcon
{
    Q_OBJECT

public:
    explicit MessageTrayIcon(const SpamMessage& message, QObject *parent = 0);
    ~MessageTrayIcon();

signals:
    //void activated();
public slots:
    void on_messageClicked();
    void activate(QSystemTrayIcon::ActivationReason/* = QSystemTrayIcon::DoubleClick*/);
    void onMessageActivate();

private:
    LateBoundObject < OperListController > _opersController;

    SpamMessage _associatedMessage;
};

#endif // MESSAGETRAYICON_H
