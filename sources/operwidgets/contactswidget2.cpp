//#include "QtGui"

//#include "contactswidget2.h"
//#include "lineeditwithhint.h"
//#include "contactsview.h"
//#include "statetimedelegate.h"
//#include "nocontactswidget.h"
//#include "appcontext.h"

//ContactsWidget2::ContactsWidget2(QWidget *parent) :
//    QWidget(parent)
//{

//    _searchLine  = new LineEditWithHint(this);
//    _searchLine->setAutoFillBackground(false);
//    _searchLine->setHintText(tr("Поиск"));

//    _contactsView = new ContactsView(this);
//    _contactsView->setObjectName("contactsview");
//    _contactsView->setModel(_contactsManager->contactsModel());
//    _contactsView->setHeaderHidden(true);
//    _contactsView->setItemDelegateForColumn(0,
//        new StateTimeDelegate(StateTimeDelegate::FormatMinutes, _contactsView)
//        );
//    _contactsManager->connectView(_contactsView);

//    _noContactsWidget = new NoContactsWidget(_contactsView);
//    _contactsView->setEmptyWidget(_noContactsWidget);

//    _transferButton = new QToolButton(this);
//    _transferButton->setIcon(QIcon(":/resources/icons/new/transfer on.png"));
//    _transferButton->setAutoRaise(true);


//    connect(_searchLine.data(), SIGNAL(textChanged(const QString &)), this, SLOT(on_searchLine_textChanged(QString)));
//    connect(_transferButton.data(),SIGNAL (clicked(bool)), this, SLOT(on_bnTransfer_clicked(bool)));
//    connect(_searchLine.data(), SIGNAL(returnPressed()), _transferButton.data(), SLOT(animateClick()));

//    connect(_appContext.instance(), SIGNAL(activeContactSelected(ccappcore::Contact)), this, SLOT(on_contactSelected(ccappcore::Contact)));
//    connect(_appContext.instance(), SIGNAL(activeContactChanged(ccappcore::Contact)), this, SLOT(on_contactChanged(ccappcore::Contact)));

//    QVBoxLayout *topLayout = new QVBoxLayout();
//    topLayout->addWidget(_contactsView);

//    QHBoxLayout *bottomLayout = new QHBoxLayout();
//    bottomLayout->addWidget(_searchLine);
//    bottomLayout->addWidget(_transferButton);

//    QVBoxLayout *mainLayout = new QVBoxLayout();
//    mainLayout->addLayout(topLayout);
//    mainLayout->addLayout(bottomLayout);
//    setLayout(mainLayout);
//}

//ContactsWidget2::~ContactsWidget2()
//{

//}

//void ContactsWidget2::reset()
//{
//    if (_contactsManager.instance())
//        _contactsManager->resetOperFlags();
//    _assignedContact = ccappcore::Contact();
//}

//void ContactsWidget2::on_searchLine_textChanged(QString searchText)
//{
//    if(searchText.isEmpty())
//    {
//        _noContactsWidget->setHtml(tr("Контакты не найдены"));
//    }
//    else
//    {
//        QString action = /*_appContext->callToTransfer().isValid() ?*/
//                         tr("Перевести на номер: ") /*: tr("Позвонить на номер: ")*/;
//        QString url = "callto:"+searchText;
//        QString href = "<a href='" + url + "'>"+searchText+"</a>";
//        QString msg = tr("Контакты не найдены.<br/><br/>%1<br/>%2<br/>")
//              .arg(action).arg(href);
//        _noContactsWidget->setHtml(msg);
//    }


//    if(!_searchLine->isHintVisible())
//    {
//        _contactsManager->searchContactsAsync(searchText);
//    }
//    else
//        _contactsManager->searchContactsAsync(QString());
//}

//void ContactsWidget2::on_bnTransfer_clicked(bool)
//{
//    QString phone = _searchLine->text();

//    if (_assignedContact.isValid())
//    {
//        on_contactSelected(_assignedContact);
//    }
//    else if(phone.isEmpty() || _searchLine->isHintVisible())
//    {
//        //_appContext->showDialer();
//        return;
//    }
//    else
//        emit contactSelected(phone);

//    _searchLine->clear();

//}

//void ContactsWidget2::on_contactChanged(ccappcore::Contact c)
//{
//    if (!c.isValid())
//        return;
//    _assignedContact = c;
//}

//void ContactsWidget2::on_contactSelected(ccappcore::Contact c)
//{
//    if (!c.isValid())
//        return;
//    QString phone = c.data(Contact::Phone).toString();
//    emit contactSelected(phone);
//}

//bool ContactsWidget2::event(QEvent *event)
//{

//    QEvent::Type type = event->type();
//    if (type == QEvent::Paint ||
//            type == QEvent::Enter ||
//            type == QEvent::Leave)
//    {
//        return true;
//    }
//    if (event->type() == QEvent::Hide)
//    {
//        reset();
//    }
//    return true;
//}

//void ContactsWidget2::hideEvent(QHideEvent *)
//{
//    //reset();
//}
