/****************************************************************************
** Meta object code from reading C++ file 'redirruleswidget.h'
**
** Created: Tue 26. Mar 13:57:21 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../redirruleswidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'redirruleswidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_RedirectCallsWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      21,   20,   20,   20, 0x0a,
      38,   20,   20,   20, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_RedirectCallsWidget[] = {
    "RedirectCallsWidget\0\0on_btnOkCliked()\0"
    "on_btnCancelCliked()\0"
};

void RedirectCallsWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        RedirectCallsWidget *_t = static_cast<RedirectCallsWidget *>(_o);
        switch (_id) {
        case 0: _t->on_btnOkCliked(); break;
        case 1: _t->on_btnCancelCliked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData RedirectCallsWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject RedirectCallsWidget::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_RedirectCallsWidget,
      qt_meta_data_RedirectCallsWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &RedirectCallsWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *RedirectCallsWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *RedirectCallsWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RedirectCallsWidget))
        return static_cast<void*>(const_cast< RedirectCallsWidget*>(this));
    return QDialog::qt_metacast(_clname);
}

int RedirectCallsWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
static const uint qt_meta_data_RuleListModel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      21,   15,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_RuleListModel[] = {
    "RuleListModel\0\0index\0on_dataChanged(QModelIndex)\0"
};

void RuleListModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        RuleListModel *_t = static_cast<RuleListModel *>(_o);
        switch (_id) {
        case 0: _t->on_dataChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData RuleListModel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject RuleListModel::staticMetaObject = {
    { &QAbstractListModel::staticMetaObject, qt_meta_stringdata_RuleListModel,
      qt_meta_data_RuleListModel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &RuleListModel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *RuleListModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *RuleListModel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RuleListModel))
        return static_cast<void*>(const_cast< RuleListModel*>(this));
    return QAbstractListModel::qt_metacast(_clname);
}

int RuleListModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractListModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_RedirectRulesWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      21,   20,   20,   20, 0x0a,
      48,   20,   20,   20, 0x0a,
      69,   20,   20,   20, 0x0a,
      89,   20,   20,   20, 0x0a,
     112,   20,   20,   20, 0x0a,
     133,   20,   20,   20, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_RedirectRulesWidget[] = {
    "RedirectRulesWidget\0\0on_ruleSelect(QModelIndex)\0"
    "on_btnEdit_clicked()\0on_btnNew_clicked()\0"
    "on_btnDelete_clicked()\0on_call2NumClicked()\0"
    "on_call2VoiceMailClicked()\0"
};

void RedirectRulesWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        RedirectRulesWidget *_t = static_cast<RedirectRulesWidget *>(_o);
        switch (_id) {
        case 0: _t->on_ruleSelect((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 1: _t->on_btnEdit_clicked(); break;
        case 2: _t->on_btnNew_clicked(); break;
        case 3: _t->on_btnDelete_clicked(); break;
        case 4: _t->on_call2NumClicked(); break;
        case 5: _t->on_call2VoiceMailClicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData RedirectRulesWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject RedirectRulesWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_RedirectRulesWidget,
      qt_meta_data_RedirectRulesWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &RedirectRulesWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *RedirectRulesWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *RedirectRulesWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RedirectRulesWidget))
        return static_cast<void*>(const_cast< RedirectRulesWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int RedirectRulesWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}
static const uint qt_meta_data_ProfileSystemMaster[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      21,   20,   20,   20, 0x05,
      52,   46,   20,   20, 0x05,

 // slots: signature, parameters, type, tag, flags
      82,   20,   20,   20, 0x0a,
     109,  100,   20,   20, 0x0a,
     134,   20,   20,   20, 0x0a,
     149,  100,   20,   20, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ProfileSystemMaster[] = {
    "ProfileSystemMaster\0\0profileActivated(qint32)\0"
    "error\0profileActivateError(QString)\0"
    "on_profileAdded()\0position\0"
    "on_profileEdited(qint32)\0on_ruleAdded()\0"
    "on_ruleEdited(qint32)\0"
};

void ProfileSystemMaster::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ProfileSystemMaster *_t = static_cast<ProfileSystemMaster *>(_o);
        switch (_id) {
        case 0: _t->profileActivated((*reinterpret_cast< qint32(*)>(_a[1]))); break;
        case 1: _t->profileActivateError((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->on_profileAdded(); break;
        case 3: _t->on_profileEdited((*reinterpret_cast< qint32(*)>(_a[1]))); break;
        case 4: _t->on_ruleAdded(); break;
        case 5: _t->on_ruleEdited((*reinterpret_cast< qint32(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ProfileSystemMaster::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ProfileSystemMaster::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_ProfileSystemMaster,
      qt_meta_data_ProfileSystemMaster, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ProfileSystemMaster::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ProfileSystemMaster::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ProfileSystemMaster::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ProfileSystemMaster))
        return static_cast<void*>(const_cast< ProfileSystemMaster*>(this));
    return QWidget::qt_metacast(_clname);
}

int ProfileSystemMaster::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void ProfileSystemMaster::profileActivated(qint32 _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ProfileSystemMaster::profileActivateError(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
