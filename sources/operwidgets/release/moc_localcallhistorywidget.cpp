/****************************************************************************
** Meta object code from reading C++ file 'localcallhistorywidget.h'
**
** Created: Tue 26. Mar 13:57:29 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../localcallhistorywidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'localcallhistorywidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_LocalCallHistoryModel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      28,   23,   22,   22, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_LocalCallHistoryModel[] = {
    "LocalCallHistoryModel\0\0call\0"
    "on_callStateChanged(ccappcore::Call)\0"
};

void LocalCallHistoryModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        LocalCallHistoryModel *_t = static_cast<LocalCallHistoryModel *>(_o);
        switch (_id) {
        case 0: _t->on_callStateChanged((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData LocalCallHistoryModel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject LocalCallHistoryModel::staticMetaObject = {
    { &CallsModel::staticMetaObject, qt_meta_stringdata_LocalCallHistoryModel,
      qt_meta_data_LocalCallHistoryModel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LocalCallHistoryModel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LocalCallHistoryModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LocalCallHistoryModel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LocalCallHistoryModel))
        return static_cast<void*>(const_cast< LocalCallHistoryModel*>(this));
    return CallsModel::qt_metacast(_clname);
}

int LocalCallHistoryModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CallsModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_LocalCallHistoryWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_LocalCallHistoryWidget[] = {
    "LocalCallHistoryWidget\0"
};

void LocalCallHistoryWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData LocalCallHistoryWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject LocalCallHistoryWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_LocalCallHistoryWidget,
      qt_meta_data_LocalCallHistoryWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LocalCallHistoryWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LocalCallHistoryWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LocalCallHistoryWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LocalCallHistoryWidget))
        return static_cast<void*>(const_cast< LocalCallHistoryWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int LocalCallHistoryWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
