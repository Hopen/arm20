/****************************************************************************
** Meta object code from reading C++ file 'nocontactswidget.h'
**
** Created: Tue 26. Mar 13:57:06 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../nocontactswidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'nocontactswidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_NoContactsWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      18,   17,   17,   17, 0x05,

 // slots: signature, parameters, type, tag, flags
      43,   38,   17,   17, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_NoContactsWidget[] = {
    "NoContactsWidget\0\0anchorClicked(QUrl)\0"
    "html\0setHtml(QString)\0"
};

void NoContactsWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        NoContactsWidget *_t = static_cast<NoContactsWidget *>(_o);
        switch (_id) {
        case 0: _t->anchorClicked((*reinterpret_cast< QUrl(*)>(_a[1]))); break;
        case 1: _t->setHtml((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData NoContactsWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject NoContactsWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_NoContactsWidget,
      qt_meta_data_NoContactsWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &NoContactsWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *NoContactsWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *NoContactsWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_NoContactsWidget))
        return static_cast<void*>(const_cast< NoContactsWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int NoContactsWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void NoContactsWidget::anchorClicked(QUrl _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
static const uint qt_meta_data_NoContactsWidget2[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      19,   18,   18,   18, 0x05,

 // slots: signature, parameters, type, tag, flags
      44,   39,   18,   18, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_NoContactsWidget2[] = {
    "NoContactsWidget2\0\0anchorClicked(QUrl)\0"
    "html\0setHtml(QString)\0"
};

void NoContactsWidget2::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        NoContactsWidget2 *_t = static_cast<NoContactsWidget2 *>(_o);
        switch (_id) {
        case 0: _t->anchorClicked((*reinterpret_cast< QUrl(*)>(_a[1]))); break;
        case 1: _t->setHtml((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData NoContactsWidget2::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject NoContactsWidget2::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_NoContactsWidget2,
      qt_meta_data_NoContactsWidget2, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &NoContactsWidget2::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *NoContactsWidget2::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *NoContactsWidget2::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_NoContactsWidget2))
        return static_cast<void*>(const_cast< NoContactsWidget2*>(this));
    return QWidget::qt_metacast(_clname);
}

int NoContactsWidget2::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void NoContactsWidget2::anchorClicked(QUrl _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
