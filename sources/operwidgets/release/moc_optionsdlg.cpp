/****************************************************************************
** Meta object code from reading C++ file 'optionsdlg.h'
**
** Created: Tue 26. Mar 13:56:10 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../options/optionsdlg.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'optionsdlg.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_OptionsDlg[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
      27,   11,   11,   11, 0x08,
      34,   11,   11,   11, 0x08,
      44,   11,   11,   11, 0x08,
      64,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_OptionsDlg[] = {
    "OptionsDlg\0\0applyOptions()\0doOk()\0"
    "doApply()\0doLoadCDSSettings()\0"
    "doSaveCDSSettings()\0"
};

void OptionsDlg::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        OptionsDlg *_t = static_cast<OptionsDlg *>(_o);
        switch (_id) {
        case 0: _t->applyOptions(); break;
        case 1: _t->doOk(); break;
        case 2: _t->doApply(); break;
        case 3: _t->doLoadCDSSettings(); break;
        case 4: _t->doSaveCDSSettings(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData OptionsDlg::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject OptionsDlg::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_OptionsDlg,
      qt_meta_data_OptionsDlg, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &OptionsDlg::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *OptionsDlg::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *OptionsDlg::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_OptionsDlg))
        return static_cast<void*>(const_cast< OptionsDlg*>(this));
    if (!strcmp(_clname, "Ui::OptionsUI"))
        return static_cast< Ui::OptionsUI*>(const_cast< OptionsDlg*>(this));
    return QDialog::qt_metacast(_clname);
}

int OptionsDlg::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void OptionsDlg::applyOptions()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
static const uint qt_meta_data_OptionsDlg__Private[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      21,   20,   20,   20, 0x0a,
      34,   31,   20,   20, 0x0a,
      51,   20,   20,   20, 0x0a,
      71,   20,   20,   20, 0x0a,
      91,   20,   20,   20, 0x0a,
     135,  118,   20,   20, 0x08,
     189,   20,   20,   20, 0x08,
     203,   20,   20,   20, 0x08,
     221,   20,   20,   20, 0x08,
     234,   20,   20,   20, 0x08,
     253,   20,   20,   20, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_OptionsDlg__Private[] = {
    "OptionsDlg::Private\0\0doApply()\0id\0"
    "openTab(QString)\0doLoadCDSSettings()\0"
    "doSaveCDSSettings()\0on_loadSettingsCompleted()\0"
    "current,previous\0"
    "currentItemChanged(QListWidgetItem*,QListWidgetItem*)\0"
    "dataChanged()\0noDirtySlot(bool)\0"
    "createTabs()\0createChangedMap()\0"
    "connectDataChanged(QWidget*)\0"
};

void OptionsDlg::Private::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Private *_t = static_cast<Private *>(_o);
        switch (_id) {
        case 0: _t->doApply(); break;
        case 1: _t->openTab((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->doLoadCDSSettings(); break;
        case 3: _t->doSaveCDSSettings(); break;
        case 4: _t->on_loadSettingsCompleted(); break;
        case 5: _t->currentItemChanged((*reinterpret_cast< QListWidgetItem*(*)>(_a[1])),(*reinterpret_cast< QListWidgetItem*(*)>(_a[2]))); break;
        case 6: _t->dataChanged(); break;
        case 7: _t->noDirtySlot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->createTabs(); break;
        case 9: _t->createChangedMap(); break;
        case 10: _t->connectDataChanged((*reinterpret_cast< QWidget*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData OptionsDlg::Private::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject OptionsDlg::Private::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_OptionsDlg__Private,
      qt_meta_data_OptionsDlg__Private, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &OptionsDlg::Private::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *OptionsDlg::Private::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *OptionsDlg::Private::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_OptionsDlg__Private))
        return static_cast<void*>(const_cast< Private*>(this));
    return QObject::qt_metacast(_clname);
}

int OptionsDlg::Private::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
