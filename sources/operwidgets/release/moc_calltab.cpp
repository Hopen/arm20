/****************************************************************************
** Meta object code from reading C++ file 'calltab.h'
**
** Created: Tue 26. Mar 13:56:39 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../calltab.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'calltab.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CallTab[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      36,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
       9,    8,    8,    8, 0x05,
      23,    8,    8,    8, 0x05,
      45,    8,    8,    8, 0x05,

 // slots: signature, parameters, type, tag, flags
      63,   59,    8,    8, 0x0a,
      86,   81,    8,    8, 0x0a,
     115,  107,    8,    8, 0x0a,
     154,  143,    8,    8, 0x0a,
     186,  180,    8,    8, 0x0a,
     230,    8,    8,    8, 0x0a,
     255,  250,    8,    8, 0x08,
     292,  250,    8,    8, 0x08,
     329,    8,    8,    8, 0x08,
     358,    8,    8,    8, 0x08,
     385,    8,    8,    8, 0x08,
     409,    8,    8,    8, 0x08,
     434,    8,    8,    8, 0x08,
     470,    8,    8,    8, 0x08,
     502,  498,    8,    8, 0x08,
     550,    8,    8,    8, 0x08,
     581,  573,    8,    8, 0x08,
     605,  573,    8,    8, 0x08,
     629,    8,    8,    8, 0x08,
     656,    8,    8,    8, 0x08,
     686,    8,    8,    8, 0x08,
     707,    8,    8,    8, 0x08,
     724,    8,    8,    8, 0x08,
     743,    8,    8,    8, 0x08,
     762,    8,    8,    8, 0x08,
     782,    8,    8,    8, 0x08,
     800,    8,    8,    8, 0x08,
     822,    8,    8,    8, 0x08,
     845,    8,    8,    8, 0x08,
     864,    8,    8,    8, 0x08,
     880,    8,    8,    8, 0x08,
     918,    8,    8,    8, 0x08,
     947,    8,    8,    8, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_CallTab[] = {
    "CallTab\0\0iconChanged()\0titleChanged(QString)\0"
    "readyToQuit()\0url\0openWebPage(QUrl)\0"
    "days\0openCallHistory(int)\0contact\0"
    "onContactSelected(QVariant)\0scriptName\0"
    "onScriptSelected(QString)\0error\0"
    "onProcessStartError(QProcess::ProcessError)\0"
    "onAnyWidgetCliked()\0link\0"
    "on_headerHtml_linkActivated(QString)\0"
    "on_footerHtml_linkActivated(QString)\0"
    "on_bnClientDetails_clicked()\0"
    "on_bnCallDetails_clicked()\0"
    "on_bnCallStop_clicked()\0"
    "on_bnCallStart_clicked()\0"
    "on_lineEditCallNote_returnPressed()\0"
    "on_bnAddExtraInfo_clicked()\0pos\0"
    "on_bnConnect_customContextMenuRequested(QPoint)\0"
    "on_bnConnect_clicked()\0checked\0"
    "on_bnMute_clicked(bool)\0on_bnHold_clicked(bool)\0"
    "onEndCall(ccappcore::Call)\0"
    "connectCallAction_triggered()\0"
    "updateCallControls()\0updateCallInfo()\0"
    "updateClientInfo()\0refreshStateTime()\0"
    "setSubject(QString)\0requestCallInfo()\0"
    "htmlViewLoadStarted()\0htmlViewLoadFinished()\0"
    "executeExtraInfo()\0onCallHistory()\0"
    "onCallHistoryItemClicked(QModelIndex)\0"
    "webViewTitleChanged(QString)\0"
    "webViewIconChanged()\0"
};

void CallTab::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CallTab *_t = static_cast<CallTab *>(_o);
        switch (_id) {
        case 0: _t->iconChanged(); break;
        case 1: _t->titleChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->readyToQuit(); break;
        case 3: _t->openWebPage((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 4: _t->openCallHistory((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->onContactSelected((*reinterpret_cast< QVariant(*)>(_a[1]))); break;
        case 6: _t->onScriptSelected((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 7: _t->onProcessStartError((*reinterpret_cast< QProcess::ProcessError(*)>(_a[1]))); break;
        case 8: _t->onAnyWidgetCliked(); break;
        case 9: _t->on_headerHtml_linkActivated((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 10: _t->on_footerHtml_linkActivated((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 11: _t->on_bnClientDetails_clicked(); break;
        case 12: _t->on_bnCallDetails_clicked(); break;
        case 13: _t->on_bnCallStop_clicked(); break;
        case 14: _t->on_bnCallStart_clicked(); break;
        case 15: _t->on_lineEditCallNote_returnPressed(); break;
        case 16: _t->on_bnAddExtraInfo_clicked(); break;
        case 17: _t->on_bnConnect_customContextMenuRequested((*reinterpret_cast< QPoint(*)>(_a[1]))); break;
        case 18: _t->on_bnConnect_clicked(); break;
        case 19: _t->on_bnMute_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 20: _t->on_bnHold_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 21: _t->onEndCall((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 22: _t->connectCallAction_triggered(); break;
        case 23: _t->updateCallControls(); break;
        case 24: _t->updateCallInfo(); break;
        case 25: _t->updateClientInfo(); break;
        case 26: _t->refreshStateTime(); break;
        case 27: _t->setSubject((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 28: _t->requestCallInfo(); break;
        case 29: _t->htmlViewLoadStarted(); break;
        case 30: _t->htmlViewLoadFinished(); break;
        case 31: _t->executeExtraInfo(); break;
        case 32: _t->onCallHistory(); break;
        case 33: _t->onCallHistoryItemClicked((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 34: _t->webViewTitleChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 35: _t->webViewIconChanged(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CallTab::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CallTab::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_CallTab,
      qt_meta_data_CallTab, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CallTab::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CallTab::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CallTab::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CallTab))
        return static_cast<void*>(const_cast< CallTab*>(this));
    if (!strcmp(_clname, "IAppUrlHandler"))
        return static_cast< IAppUrlHandler*>(const_cast< CallTab*>(this));
    if (!strcmp(_clname, "ru.forte-it.ccappcore.iappurlhandler/1.0"))
        return static_cast< ccappcore::IAppUrlHandler*>(const_cast< CallTab*>(this));
    return QWidget::qt_metacast(_clname);
}

int CallTab::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 36)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 36;
    }
    return _id;
}

// SIGNAL 0
void CallTab::iconChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void CallTab::titleChanged(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void CallTab::readyToQuit()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
QT_END_MOC_NAMESPACE
