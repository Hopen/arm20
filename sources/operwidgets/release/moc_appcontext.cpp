/****************************************************************************
** Meta object code from reading C++ file 'appcontext.h'
**
** Created: Tue 26. Mar 13:56:31 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../appcontext.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'appcontext.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_AppContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      24,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      12,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x05,
      44,   11,   11,   11, 0x05,
      79,   11,   11,   11, 0x05,
     100,   11,   11,   11, 0x05,
     145,  139,   11,   11, 0x05,
     167,   11,   11,   11, 0x05,
     202,   11,   11,   11, 0x05,
     243,   11,   11,   11, 0x05,
     285,   11,   11,   11, 0x05,
     307,   11,   11,   11, 0x05,
     324,   11,   11,   11, 0x05,
     351,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
     381,   11,   11,   11, 0x0a,
     411,   11,   11,   11, 0x0a,
     444,   11,   11,   11, 0x0a,
     463,   11,   11,   11, 0x0a,
     498,   11,   11,   11, 0x0a,
     520,  139,   11,   11, 0x0a,
     540,   11,   11,   11, 0x2a,
     553,   11,   11,   11, 0x0a,
     590,   11,   11,   11, 0x0a,
     633,   11,   11,   11, 0x0a,
     662,   11,   11,   11, 0x0a,
     694,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_AppContext[] = {
    "AppContext\0\0onShowCallForm(ccappcore::Call)\0"
    "onShowChatForm(ccappcore::Contact)\0"
    "onToggleVisibility()\0"
    "callToTransferChanged(ccappcore::Call)\0"
    "phone\0onShowDialer(QString)\0"
    "activeCallChanged(ccappcore::Call)\0"
    "activeContactChanged(ccappcore::Contact)\0"
    "activeContactSelected(ccappcore::Contact)\0"
    "windowMinimized(bool)\0staysOnTop(bool)\0"
    "activateForegroundWindow()\0"
    "stopAlerting(ccappcore::Call)\0"
    "showCallForm(ccappcore::Call)\0"
    "showChatForm(ccappcore::Contact)\0"
    "toggleVisibility()\0"
    "setCallToTransfer(ccappcore::Call)\0"
    "clearCallToTransfer()\0showDialer(QString)\0"
    "showDialer()\0onActiveCallChanged(ccappcore::Call)\0"
    "onActiveContactChanged(ccappcore::Contact)\0"
    "onActivateForegroundWindow()\0"
    "onStopAlerting(ccappcore::Call)\0"
    "callEnded(ccappcore::Call)\0"
};

void AppContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AppContext *_t = static_cast<AppContext *>(_o);
        switch (_id) {
        case 0: _t->onShowCallForm((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 1: _t->onShowChatForm((*reinterpret_cast< ccappcore::Contact(*)>(_a[1]))); break;
        case 2: _t->onToggleVisibility(); break;
        case 3: _t->callToTransferChanged((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 4: _t->onShowDialer((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: _t->activeCallChanged((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 6: _t->activeContactChanged((*reinterpret_cast< ccappcore::Contact(*)>(_a[1]))); break;
        case 7: _t->activeContactSelected((*reinterpret_cast< ccappcore::Contact(*)>(_a[1]))); break;
        case 8: _t->windowMinimized((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->staysOnTop((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->activateForegroundWindow(); break;
        case 11: _t->stopAlerting((*reinterpret_cast< const ccappcore::Call(*)>(_a[1]))); break;
        case 12: _t->showCallForm((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 13: _t->showChatForm((*reinterpret_cast< ccappcore::Contact(*)>(_a[1]))); break;
        case 14: _t->toggleVisibility(); break;
        case 15: _t->setCallToTransfer((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 16: _t->clearCallToTransfer(); break;
        case 17: _t->showDialer((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 18: _t->showDialer(); break;
        case 19: _t->onActiveCallChanged((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 20: _t->onActiveContactChanged((*reinterpret_cast< ccappcore::Contact(*)>(_a[1]))); break;
        case 21: _t->onActivateForegroundWindow(); break;
        case 22: _t->onStopAlerting((*reinterpret_cast< const ccappcore::Call(*)>(_a[1]))); break;
        case 23: _t->callEnded((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData AppContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject AppContext::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_AppContext,
      qt_meta_data_AppContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AppContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AppContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AppContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AppContext))
        return static_cast<void*>(const_cast< AppContext*>(this));
    return QObject::qt_metacast(_clname);
}

int AppContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 24)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    }
    return _id;
}

// SIGNAL 0
void AppContext::onShowCallForm(ccappcore::Call _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void AppContext::onShowChatForm(ccappcore::Contact _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void AppContext::onToggleVisibility()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void AppContext::callToTransferChanged(ccappcore::Call _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void AppContext::onShowDialer(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void AppContext::activeCallChanged(ccappcore::Call _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void AppContext::activeContactChanged(ccappcore::Contact _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void AppContext::activeContactSelected(ccappcore::Contact _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void AppContext::windowMinimized(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void AppContext::staysOnTop(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void AppContext::activateForegroundWindow()
{
    QMetaObject::activate(this, &staticMetaObject, 10, 0);
}

// SIGNAL 11
void AppContext::stopAlerting(const ccappcore::Call & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}
QT_END_MOC_NAMESPACE
