/****************************************************************************
** Meta object code from reading C++ file 'docktabswidget.h'
**
** Created: Tue 26. Mar 13:57:37 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../docktabswidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'docktabswidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DockTabsController[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      42,   20,   19,   19, 0x05,
      80,   76,   19,   19, 0x05,
     103,   19,   19,   19, 0x05,

 // slots: signature, parameters, type, tag, flags
     117,  115,   19,   19, 0x0a,
     150,  115,   19,   19, 0x0a,
     187,  179,   19,   19, 0x0a,
     214,  115,   19,   19, 0x0a,
     250,   19,   19,   19, 0x0a,
     270,  179,   19,   19, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_DockTabsController[] = {
    "DockTabsController\0\0tab,after,tabQuantity\0"
    "addDockTab(DockTab*,DockTab*,int)\0tab\0"
    "raiseDockTab(DockTab*)\0tabsClear()\0c\0"
    "on_showCallForm(ccappcore::Call)\0"
    "callStarted(ccappcore::Call)\0message\0"
    "chatMessageIn(SpamMessage)\0"
    "on_showChatForm(ccappcore::Contact)\0"
    "on_childTabClosed()\0activateIcon(SpamMessage)\0"
};

void DockTabsController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DockTabsController *_t = static_cast<DockTabsController *>(_o);
        switch (_id) {
        case 0: _t->addDockTab((*reinterpret_cast< DockTab*(*)>(_a[1])),(*reinterpret_cast< DockTab*(*)>(_a[2])),(*reinterpret_cast< const int(*)>(_a[3]))); break;
        case 1: _t->raiseDockTab((*reinterpret_cast< DockTab*(*)>(_a[1]))); break;
        case 2: _t->tabsClear(); break;
        case 3: _t->on_showCallForm((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 4: _t->callStarted((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 5: _t->chatMessageIn((*reinterpret_cast< SpamMessage(*)>(_a[1]))); break;
        case 6: _t->on_showChatForm((*reinterpret_cast< ccappcore::Contact(*)>(_a[1]))); break;
        case 7: _t->on_childTabClosed(); break;
        case 8: _t->activateIcon((*reinterpret_cast< const SpamMessage(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData DockTabsController::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DockTabsController::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_DockTabsController,
      qt_meta_data_DockTabsController, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DockTabsController::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DockTabsController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DockTabsController::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DockTabsController))
        return static_cast<void*>(const_cast< DockTabsController*>(this));
    return QObject::qt_metacast(_clname);
}

int DockTabsController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void DockTabsController::addDockTab(DockTab * _t1, DockTab * _t2, const int & _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void DockTabsController::raiseDockTab(DockTab * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void DockTabsController::tabsClear()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
static const uint qt_meta_data_DockTabsWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      38,   16,   15,   15, 0x0a,
      76,   72,   15,   15, 0x0a,
      99,   15,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_DockTabsWidget[] = {
    "DockTabsWidget\0\0tab,after,tabQuantity\0"
    "addDockTab(DockTab*,DockTab*,int)\0tab\0"
    "raiseDockTab(DockTab*)\0on_tabsClear()\0"
};

void DockTabsWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DockTabsWidget *_t = static_cast<DockTabsWidget *>(_o);
        switch (_id) {
        case 0: _t->addDockTab((*reinterpret_cast< DockTab*(*)>(_a[1])),(*reinterpret_cast< DockTab*(*)>(_a[2])),(*reinterpret_cast< const int(*)>(_a[3]))); break;
        case 1: _t->raiseDockTab((*reinterpret_cast< DockTab*(*)>(_a[1]))); break;
        case 2: _t->on_tabsClear(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData DockTabsWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DockTabsWidget::staticMetaObject = {
    { &QDockWidgetEx::staticMetaObject, qt_meta_stringdata_DockTabsWidget,
      qt_meta_data_DockTabsWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DockTabsWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DockTabsWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DockTabsWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DockTabsWidget))
        return static_cast<void*>(const_cast< DockTabsWidget*>(this));
    return QDockWidgetEx::qt_metacast(_clname);
}

int DockTabsWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDockWidgetEx::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
