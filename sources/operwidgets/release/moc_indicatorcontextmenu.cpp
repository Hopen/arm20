/****************************************************************************
** Meta object code from reading C++ file 'indicatorcontextmenu.h'
**
** Created: Tue 26. Mar 13:57:09 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../indicatorcontextmenu.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'indicatorcontextmenu.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_IndicatorContextMenu[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      24,   22,   21,   21, 0x05,

 // slots: signature, parameters, type, tag, flags
      67,   21,   21,   21, 0x0a,
      79,   21,   21,   21, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_IndicatorContextMenu[] = {
    "IndicatorContextMenu\0\0i\0"
    "showGauge(ccappcore::QuantativeIndicator*)\0"
    "showGauge()\0editThresholds()\0"
};

void IndicatorContextMenu::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        IndicatorContextMenu *_t = static_cast<IndicatorContextMenu *>(_o);
        switch (_id) {
        case 0: _t->showGauge((*reinterpret_cast< ccappcore::QuantativeIndicator*(*)>(_a[1]))); break;
        case 1: _t->showGauge(); break;
        case 2: _t->editThresholds(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData IndicatorContextMenu::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject IndicatorContextMenu::staticMetaObject = {
    { &QMenu::staticMetaObject, qt_meta_stringdata_IndicatorContextMenu,
      qt_meta_data_IndicatorContextMenu, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &IndicatorContextMenu::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *IndicatorContextMenu::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *IndicatorContextMenu::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_IndicatorContextMenu))
        return static_cast<void*>(const_cast< IndicatorContextMenu*>(this));
    return QMenu::qt_metacast(_clname);
}

int IndicatorContextMenu::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMenu::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void IndicatorContextMenu::showGauge(ccappcore::QuantativeIndicator * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
