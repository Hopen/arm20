/****************************************************************************
** Meta object code from reading C++ file 'docktabs.h'
**
** Created: Tue 26. Mar 13:57:34 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../docktabs.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'docktabs.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_EmptyTitleBar[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_EmptyTitleBar[] = {
    "EmptyTitleBar\0"
};

void EmptyTitleBar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData EmptyTitleBar::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject EmptyTitleBar::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_EmptyTitleBar,
      qt_meta_data_EmptyTitleBar, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &EmptyTitleBar::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *EmptyTitleBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *EmptyTitleBar::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_EmptyTitleBar))
        return static_cast<void*>(const_cast< EmptyTitleBar*>(this));
    return QWidget::qt_metacast(_clname);
}

int EmptyTitleBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QDockWidgetEx[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x05,
      24,   14,   14,   14, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QDockWidgetEx[] = {
    "QDockWidgetEx\0\0closed()\0childAdded()\0"
};

void QDockWidgetEx::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QDockWidgetEx *_t = static_cast<QDockWidgetEx *>(_o);
        switch (_id) {
        case 0: _t->closed(); break;
        case 1: _t->childAdded(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QDockWidgetEx::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QDockWidgetEx::staticMetaObject = {
    { &QDockWidget::staticMetaObject, qt_meta_stringdata_QDockWidgetEx,
      qt_meta_data_QDockWidgetEx, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QDockWidgetEx::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QDockWidgetEx::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QDockWidgetEx::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QDockWidgetEx))
        return static_cast<void*>(const_cast< QDockWidgetEx*>(this));
    return QDockWidget::qt_metacast(_clname);
}

int QDockWidgetEx::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDockWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void QDockWidgetEx::closed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void QDockWidgetEx::childAdded()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
static const uint qt_meta_data_DockTab[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_DockTab[] = {
    "DockTab\0"
};

void DockTab::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData DockTab::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DockTab::staticMetaObject = {
    { &QDockWidgetEx::staticMetaObject, qt_meta_stringdata_DockTab,
      qt_meta_data_DockTab, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DockTab::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DockTab::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DockTab::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DockTab))
        return static_cast<void*>(const_cast< DockTab*>(this));
    return QDockWidgetEx::qt_metacast(_clname);
}

int DockTab::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDockWidgetEx::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
