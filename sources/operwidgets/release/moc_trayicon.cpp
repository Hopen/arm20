/****************************************************************************
** Meta object code from reading C++ file 'trayicon.h'
**
** Created: Tue 26. Mar 13:56:50 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../trayicon.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'trayicon.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TrayIcon[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x08,
      54,    9,    9,    9, 0x08,
      75,    9,    9,    9, 0x08,
     108,    9,    9,    9, 0x08,
     140,    9,    9,    9, 0x08,
     160,    9,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_TrayIcon[] = {
    "TrayIcon\0\0activate(QSystemTrayIcon::ActivationReason)\0"
    "operSessionStarted()\0"
    "showCallMessage(ccappcore::Call)\0"
    "on_callStarted(ccappcore::Call)\0"
    "on_messageClicked()\0on_windowMinimized(bool)\0"
};

void TrayIcon::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TrayIcon *_t = static_cast<TrayIcon *>(_o);
        switch (_id) {
        case 0: _t->activate((*reinterpret_cast< QSystemTrayIcon::ActivationReason(*)>(_a[1]))); break;
        case 1: _t->operSessionStarted(); break;
        case 2: _t->showCallMessage((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 3: _t->on_callStarted((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 4: _t->on_messageClicked(); break;
        case 5: _t->on_windowMinimized((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData TrayIcon::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TrayIcon::staticMetaObject = {
    { &QSystemTrayIcon::staticMetaObject, qt_meta_stringdata_TrayIcon,
      qt_meta_data_TrayIcon, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TrayIcon::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TrayIcon::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TrayIcon::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TrayIcon))
        return static_cast<void*>(const_cast< TrayIcon*>(this));
    return QSystemTrayIcon::qt_metacast(_clname);
}

int TrayIcon::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QSystemTrayIcon::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
