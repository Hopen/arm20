/****************************************************************************
** Meta object code from reading C++ file 'callcontextmenu.h'
**
** Created: Tue 26. Mar 13:56:48 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../callcontextmenu.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'callcontextmenu.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CallContextMenu[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x0a,
      42,   16,   16,   16, 0x0a,
      69,   16,   16,   16, 0x0a,
      97,   16,   16,   16, 0x0a,
     127,   16,   16,   16, 0x0a,
     154,   16,   16,   16, 0x0a,
     183,   16,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_CallContextMenu[] = {
    "CallContextMenu\0\0showCallForm_triggered()\0"
    "dropCallAction_triggered()\0"
    "redialCallAction_trigered()\0"
    "connectCallAction_triggered()\0"
    "joinCallAction_triggered()\0"
    "unjoinCallAction_triggered()\0"
    "removeConfAction_triggered()\0"
};

void CallContextMenu::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CallContextMenu *_t = static_cast<CallContextMenu *>(_o);
        switch (_id) {
        case 0: _t->showCallForm_triggered(); break;
        case 1: _t->dropCallAction_triggered(); break;
        case 2: _t->redialCallAction_trigered(); break;
        case 3: _t->connectCallAction_triggered(); break;
        case 4: _t->joinCallAction_triggered(); break;
        case 5: _t->unjoinCallAction_triggered(); break;
        case 6: _t->removeConfAction_triggered(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData CallContextMenu::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CallContextMenu::staticMetaObject = {
    { &QMenu::staticMetaObject, qt_meta_stringdata_CallContextMenu,
      qt_meta_data_CallContextMenu, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CallContextMenu::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CallContextMenu::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CallContextMenu::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CallContextMenu))
        return static_cast<void*>(const_cast< CallContextMenu*>(this));
    return QMenu::qt_metacast(_clname);
}

int CallContextMenu::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMenu::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
