/****************************************************************************
** Meta object code from reading C++ file 'opt_opertools.h'
**
** Created: Tue 26. Mar 13:56:23 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../options/opt_opertools.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'opt_opertools.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_OptOperToolForm[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_OptOperToolForm[] = {
    "OptOperToolForm\0"
};

void OptOperToolForm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData OptOperToolForm::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject OptOperToolForm::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_OptOperToolForm,
      qt_meta_data_OptOperToolForm, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &OptOperToolForm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *OptOperToolForm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *OptOperToolForm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_OptOperToolForm))
        return static_cast<void*>(const_cast< OptOperToolForm*>(this));
    if (!strcmp(_clname, "Ui::OptionsTabOperToolForm"))
        return static_cast< Ui::OptionsTabOperToolForm*>(const_cast< OptOperToolForm*>(this));
    return QWidget::qt_metacast(_clname);
}

int OptOperToolForm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_OptionsTabOperToolForm[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      24,   23,   23,   23, 0x05,

 // slots: signature, parameters, type, tag, flags
      34,   23,   23,   23, 0x08,
      54,   23,   23,   23, 0x08,
      74,   23,   23,   23, 0x08,
      97,   23,   23,   23, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_OptionsTabOperToolForm[] = {
    "OptionsTabOperToolForm\0\0Changed()\0"
    "on_btnToOperClick()\0on_btnToBaseClick()\0"
    "on_btnToOperAllClick()\0on_btnToBaseAllClick()\0"
};

void OptionsTabOperToolForm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        OptionsTabOperToolForm *_t = static_cast<OptionsTabOperToolForm *>(_o);
        switch (_id) {
        case 0: _t->Changed(); break;
        case 1: _t->on_btnToOperClick(); break;
        case 2: _t->on_btnToBaseClick(); break;
        case 3: _t->on_btnToOperAllClick(); break;
        case 4: _t->on_btnToBaseAllClick(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData OptionsTabOperToolForm::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject OptionsTabOperToolForm::staticMetaObject = {
    { &OptionsTab::staticMetaObject, qt_meta_stringdata_OptionsTabOperToolForm,
      qt_meta_data_OptionsTabOperToolForm, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &OptionsTabOperToolForm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *OptionsTabOperToolForm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *OptionsTabOperToolForm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_OptionsTabOperToolForm))
        return static_cast<void*>(const_cast< OptionsTabOperToolForm*>(this));
    return OptionsTab::qt_metacast(_clname);
}

int OptionsTabOperToolForm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = OptionsTab::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void OptionsTabOperToolForm::Changed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
