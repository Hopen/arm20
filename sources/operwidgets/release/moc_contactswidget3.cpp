/****************************************************************************
** Meta object code from reading C++ file 'contactswidget3.h'
**
** Created: Tue 26. Mar 13:57:25 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../contactswidget3.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'contactswidget3.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ContactsModel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x0a,
      23,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ContactsModel[] = {
    "ContactsModel\0\0reset()\0sort()\0"
};

void ContactsModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ContactsModel *_t = static_cast<ContactsModel *>(_o);
        switch (_id) {
        case 0: _t->reset(); break;
        case 1: _t->sort(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData ContactsModel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ContactsModel::staticMetaObject = {
    { &QAbstractListModel::staticMetaObject, qt_meta_stringdata_ContactsModel,
      qt_meta_data_ContactsModel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ContactsModel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ContactsModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ContactsModel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ContactsModel))
        return static_cast<void*>(const_cast< ContactsModel*>(this));
    if (!strcmp(_clname, "FilteredModel<Contact>"))
        return static_cast< FilteredModel<Contact>*>(const_cast< ContactsModel*>(this));
    return QAbstractListModel::qt_metacast(_clname);
}

int ContactsModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractListModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
static const uint qt_meta_data_ContactsWidget3[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      25,   17,   16,   16, 0x0a,
      55,   17,   16,   16, 0x0a,
      78,   17,   16,   16, 0x0a,
      98,   17,   16,   16, 0x0a,
     128,   16,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ContactsWidget3[] = {
    "ContactsWidget3\0\0checked\0"
    "on_hideOfflineOperators(bool)\0"
    "on_hideOperators(bool)\0on_hideGroups(bool)\0"
    "on_showPrivateGroupOnly(bool)\0"
    "on_contactSelect(QModelIndex)\0"
};

void ContactsWidget3::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ContactsWidget3 *_t = static_cast<ContactsWidget3 *>(_o);
        switch (_id) {
        case 0: _t->on_hideOfflineOperators((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->on_hideOperators((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->on_hideGroups((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->on_showPrivateGroupOnly((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->on_contactSelect((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ContactsWidget3::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ContactsWidget3::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_ContactsWidget3,
      qt_meta_data_ContactsWidget3, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ContactsWidget3::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ContactsWidget3::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ContactsWidget3::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ContactsWidget3))
        return static_cast<void*>(const_cast< ContactsWidget3*>(this));
    return QWidget::qt_metacast(_clname);
}

int ContactsWidget3::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
