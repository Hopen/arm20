/****************************************************************************
** Meta object code from reading C++ file 'callswidget2.h'
**
** Created: Tue 26. Mar 13:57:27 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../callswidget2.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'callswidget2.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CallsModel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: signature, parameters, type, tag, flags
      19,   12,   11,   11, 0x05,
      51,   11,   11,   11, 0x05,
      74,   66,   11,   11, 0x05,
      96,   89,   11,   11, 0x05,
     118,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
     137,   11,   11,   11, 0x0a,
     160,   11,   11,   11, 0x0a,
     200,   11,   11,   11, 0x0a,
     236,   11,   11,   11, 0x08,
     257,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_CallsModel[] = {
    "CallsModel\0\0callID\0connectingCallsListChanged(int)\0"
    "connectCalls()\0conf_id\0joinCalls(int)\0"
    "callId\0unjoinConference(int)\0"
    "dropSelectedCall()\0reset(ccappcore::Call)\0"
    "deleteFromAlertingList(ccappcore::Call)\0"
    "onCallStateChanged(ccappcore::Call)\0"
    "columeTimerExpired()\0alertingTimerExpired()\0"
};

void CallsModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CallsModel *_t = static_cast<CallsModel *>(_o);
        switch (_id) {
        case 0: _t->connectingCallsListChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->connectCalls(); break;
        case 2: _t->joinCalls((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 3: _t->unjoinConference((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->dropSelectedCall(); break;
        case 5: _t->reset((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 6: _t->deleteFromAlertingList((*reinterpret_cast< const ccappcore::Call(*)>(_a[1]))); break;
        case 7: _t->onCallStateChanged((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 8: _t->columeTimerExpired(); break;
        case 9: _t->alertingTimerExpired(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CallsModel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CallsModel::staticMetaObject = {
    { &QAbstractListModel::staticMetaObject, qt_meta_stringdata_CallsModel,
      qt_meta_data_CallsModel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CallsModel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CallsModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CallsModel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CallsModel))
        return static_cast<void*>(const_cast< CallsModel*>(this));
    if (!strcmp(_clname, "FilteredModel<ccappcore::Call>"))
        return static_cast< FilteredModel<ccappcore::Call>*>(const_cast< CallsModel*>(this));
    return QAbstractListModel::qt_metacast(_clname);
}

int CallsModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractListModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void CallsModel::connectingCallsListChanged(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void CallsModel::connectCalls()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void CallsModel::joinCalls(const int & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void CallsModel::unjoinConference(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void CallsModel::dropSelectedCall()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}
static const uint qt_meta_data_CallsWidget2[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x0a,
      35,   13,   13,   13, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_CallsWidget2[] = {
    "CallsWidget2\0\0onDropSelectedCall()\0"
    "toggleEmptyWidget()\0"
};

void CallsWidget2::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CallsWidget2 *_t = static_cast<CallsWidget2 *>(_o);
        switch (_id) {
        case 0: _t->onDropSelectedCall(); break;
        case 1: _t->toggleEmptyWidget(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData CallsWidget2::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CallsWidget2::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_CallsWidget2,
      qt_meta_data_CallsWidget2, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CallsWidget2::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CallsWidget2::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CallsWidget2::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CallsWidget2))
        return static_cast<void*>(const_cast< CallsWidget2*>(this));
    return QWidget::qt_metacast(_clname);
}

int CallsWidget2::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
static const uint qt_meta_data_OperCallsWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      19,   17,   16,   16, 0x08,
      52,   16,   16,   16, 0x08,
      79,   16,   16,   16, 0x08,
     109,   16,   16,   16, 0x08,
     140,  132,   16,   16, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_OperCallsWidget[] = {
    "OperCallsWidget\0\0c\0on_showCallForm(ccappcore::Call)\0"
    "on_btnConnectCallsCliked()\0"
    "on_btnConferenceCallsCliked()\0"
    "updateWidgetControls()\0conf_id\0"
    "on_joinCalls(int)\0"
};

void OperCallsWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        OperCallsWidget *_t = static_cast<OperCallsWidget *>(_o);
        switch (_id) {
        case 0: _t->on_showCallForm((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 1: _t->on_btnConnectCallsCliked(); break;
        case 2: _t->on_btnConferenceCallsCliked(); break;
        case 3: _t->updateWidgetControls(); break;
        case 4: _t->on_joinCalls((*reinterpret_cast< const int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData OperCallsWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject OperCallsWidget::staticMetaObject = {
    { &CallsWidget2::staticMetaObject, qt_meta_stringdata_OperCallsWidget,
      qt_meta_data_OperCallsWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &OperCallsWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *OperCallsWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *OperCallsWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_OperCallsWidget))
        return static_cast<void*>(const_cast< OperCallsWidget*>(this));
    return CallsWidget2::qt_metacast(_clname);
}

int OperCallsWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CallsWidget2::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}
static const uint qt_meta_data_QueueCallsWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QueueCallsWidget[] = {
    "QueueCallsWidget\0"
};

void QueueCallsWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QueueCallsWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QueueCallsWidget::staticMetaObject = {
    { &CallsWidget2::staticMetaObject, qt_meta_stringdata_QueueCallsWidget,
      qt_meta_data_QueueCallsWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QueueCallsWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QueueCallsWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QueueCallsWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QueueCallsWidget))
        return static_cast<void*>(const_cast< QueueCallsWidget*>(this));
    return CallsWidget2::qt_metacast(_clname);
}

int QueueCallsWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CallsWidget2::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
