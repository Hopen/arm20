/****************************************************************************
** Meta object code from reading C++ file 'opt_shortcuts.h'
**
** Created: Tue 26. Mar 13:56:15 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../options/opt_shortcuts.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'opt_shortcuts.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_OptionsTabShortcuts[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      21,   20,   20,   20, 0x08,
      32,   20,   20,   20, 0x08,
      53,   41,   20,   20, 0x08,
      95,   20,   20,   20, 0x08,
     124,  120,   20,   20, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_OptionsTabShortcuts[] = {
    "OptionsTabShortcuts\0\0onRemove()\0"
    "onEdit()\0item,column\0"
    "onItemDoubleClicked(QTreeWidgetItem*,int)\0"
    "onItemSelectionChanged()\0key\0"
    "onNewShortcutKey(QKeySequence)\0"
};

void OptionsTabShortcuts::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        OptionsTabShortcuts *_t = static_cast<OptionsTabShortcuts *>(_o);
        switch (_id) {
        case 0: _t->onRemove(); break;
        case 1: _t->onEdit(); break;
        case 2: _t->onItemDoubleClicked((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: _t->onItemSelectionChanged(); break;
        case 4: _t->onNewShortcutKey((*reinterpret_cast< const QKeySequence(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData OptionsTabShortcuts::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject OptionsTabShortcuts::staticMetaObject = {
    { &OptionsTab::staticMetaObject, qt_meta_stringdata_OptionsTabShortcuts,
      qt_meta_data_OptionsTabShortcuts, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &OptionsTabShortcuts::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *OptionsTabShortcuts::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *OptionsTabShortcuts::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_OptionsTabShortcuts))
        return static_cast<void*>(const_cast< OptionsTabShortcuts*>(this));
    return OptionsTab::qt_metacast(_clname);
}

int OptionsTabShortcuts::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = OptionsTab::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
