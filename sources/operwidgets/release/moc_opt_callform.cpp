/****************************************************************************
** Meta object code from reading C++ file 'opt_callform.h'
**
** Created: Tue 26. Mar 13:56:17 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../options/opt_callform.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'opt_callform.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_OptCallForm[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x08,
      42,   12,   12,   12, 0x08,
      81,   12,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_OptCallForm[] = {
    "OptCallForm\0\0on_bnRemoveSubject_clicked()\0"
    "on_listSubjects_itemSelectionChanged()\0"
    "on_bnAddSubject_clicked()\0"
};

void OptCallForm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        OptCallForm *_t = static_cast<OptCallForm *>(_o);
        switch (_id) {
        case 0: _t->on_bnRemoveSubject_clicked(); break;
        case 1: _t->on_listSubjects_itemSelectionChanged(); break;
        case 2: _t->on_bnAddSubject_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData OptCallForm::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject OptCallForm::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_OptCallForm,
      qt_meta_data_OptCallForm, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &OptCallForm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *OptCallForm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *OptCallForm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_OptCallForm))
        return static_cast<void*>(const_cast< OptCallForm*>(this));
    if (!strcmp(_clname, "Ui::OptionsTabCallForm"))
        return static_cast< Ui::OptionsTabCallForm*>(const_cast< OptCallForm*>(this));
    return QWidget::qt_metacast(_clname);
}

int OptCallForm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
static const uint qt_meta_data_OptionsTabCallForm[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_OptionsTabCallForm[] = {
    "OptionsTabCallForm\0"
};

void OptionsTabCallForm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData OptionsTabCallForm::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject OptionsTabCallForm::staticMetaObject = {
    { &OptionsTab::staticMetaObject, qt_meta_stringdata_OptionsTabCallForm,
      qt_meta_data_OptionsTabCallForm, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &OptionsTabCallForm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *OptionsTabCallForm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *OptionsTabCallForm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_OptionsTabCallForm))
        return static_cast<void*>(const_cast< OptionsTabCallForm*>(this));
    return OptionsTab::qt_metacast(_clname);
}

int OptionsTabCallForm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = OptionsTab::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
