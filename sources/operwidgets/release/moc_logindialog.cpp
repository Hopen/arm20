/****************************************************************************
** Meta object code from reading C++ file 'logindialog.h'
**
** Created: Tue 26. Mar 13:56:30 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../logindialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'logindialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_LoginDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      21,   13,   12,   12, 0x08,
      50,   13,   12,   12, 0x08,
      78,   12,   12,   12, 0x08,
     102,   12,   12,   12, 0x08,
     110,   12,   12,   12, 0x08,
     135,  127,   12,   12, 0x08,
     162,  155,   12,   12, 0x08,
     236,  229,   12,   12, 0x08,
     257,   12,   12,   12, 0x28,

       0        // eod
};

static const char qt_meta_stringdata_LoginDialog[] = {
    "LoginDialog\0\0checked\0on_cbAutoLogin_toggled(bool)\0"
    "on_cbNtlmAuth_toggled(bool)\0"
    "on_bnSettings_clicked()\0login()\0"
    "sessionStarted()\0message\0statusInfo(QString)\0"
    ",error\0"
    "sessionStartingFailed(SessionController::SessionErrorCode,QString)\0"
    "enable\0updateControls(bool)\0"
    "updateControls()\0"
};

void LoginDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        LoginDialog *_t = static_cast<LoginDialog *>(_o);
        switch (_id) {
        case 0: _t->on_cbAutoLogin_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->on_cbNtlmAuth_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->on_bnSettings_clicked(); break;
        case 3: _t->login(); break;
        case 4: _t->sessionStarted(); break;
        case 5: _t->statusInfo((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->sessionStartingFailed((*reinterpret_cast< SessionController::SessionErrorCode(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 7: _t->updateControls((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->updateControls(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData LoginDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject LoginDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_LoginDialog,
      qt_meta_data_LoginDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LoginDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LoginDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LoginDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LoginDialog))
        return static_cast<void*>(const_cast< LoginDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int LoginDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
