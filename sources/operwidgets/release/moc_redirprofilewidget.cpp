/****************************************************************************
** Meta object code from reading C++ file 'redirprofilewidget.h'
**
** Created: Tue 26. Mar 13:57:18 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../redirprofilewidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'redirprofilewidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_RadioDelegate[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_RadioDelegate[] = {
    "RadioDelegate\0"
};

void RadioDelegate::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData RadioDelegate::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject RadioDelegate::staticMetaObject = {
    { &QStyledItemDelegate::staticMetaObject, qt_meta_stringdata_RadioDelegate,
      qt_meta_data_RadioDelegate, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &RadioDelegate::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *RadioDelegate::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *RadioDelegate::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RadioDelegate))
        return static_cast<void*>(const_cast< RadioDelegate*>(this));
    return QStyledItemDelegate::qt_metacast(_clname);
}

int RadioDelegate::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QStyledItemDelegate::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_ProfileListModel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      35,   18,   17,   17, 0x05,
      70,   61,   17,   17, 0x25,

 // slots: signature, parameters, type, tag, flags
      97,   91,   17,   17, 0x0a,
     137,  125,   17,   17, 0x0a,
     170,  166,   17,   17, 0x2a,

       0        // eod
};

static const char qt_meta_stringdata_ProfileListModel[] = {
    "ProfileListModel\0\0position,turnOFF\0"
    "activateProfile(int,bool)\0position\0"
    "activateProfile(int)\0index\0"
    "on_dataChanged(QModelIndex)\0pos,turnOFF\0"
    "on_activateProfile(int,bool)\0pos\0"
    "on_activateProfile(int)\0"
};

void ProfileListModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ProfileListModel *_t = static_cast<ProfileListModel *>(_o);
        switch (_id) {
        case 0: _t->activateProfile((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 1: _t->activateProfile((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->on_dataChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 3: _t->on_activateProfile((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 4: _t->on_activateProfile((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ProfileListModel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ProfileListModel::staticMetaObject = {
    { &QAbstractListModel::staticMetaObject, qt_meta_stringdata_ProfileListModel,
      qt_meta_data_ProfileListModel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ProfileListModel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ProfileListModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ProfileListModel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ProfileListModel))
        return static_cast<void*>(const_cast< ProfileListModel*>(this));
    return QAbstractListModel::qt_metacast(_clname);
}

int ProfileListModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractListModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void ProfileListModel::activateProfile(int _t1, bool _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
static const uint qt_meta_data_RedirProfileWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      28,   20,   19,   19, 0x0a,
      53,   19,   19,   19, 0x2a,
      74,   20,   19,   19, 0x0a,
      98,   19,   19,   19, 0x2a,
     118,   20,   19,   19, 0x0a,
     145,   19,   19,   19, 0x2a,
     168,   19,   19,   19, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_RedirProfileWidget[] = {
    "RedirProfileWidget\0\0checked\0"
    "on_btnEdit_clicked(bool)\0on_btnEdit_clicked()\0"
    "on_btnNew_clicked(bool)\0on_btnNew_clicked()\0"
    "on_btnDelete_clicked(bool)\0"
    "on_btnDelete_clicked()\0"
    "on_profileSelect(QModelIndex)\0"
};

void RedirProfileWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        RedirProfileWidget *_t = static_cast<RedirProfileWidget *>(_o);
        switch (_id) {
        case 0: _t->on_btnEdit_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->on_btnEdit_clicked(); break;
        case 2: _t->on_btnNew_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->on_btnNew_clicked(); break;
        case 4: _t->on_btnDelete_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->on_btnDelete_clicked(); break;
        case 6: _t->on_profileSelect((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData RedirProfileWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject RedirProfileWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_RedirProfileWidget,
      qt_meta_data_RedirProfileWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &RedirProfileWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *RedirProfileWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *RedirProfileWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RedirProfileWidget))
        return static_cast<void*>(const_cast< RedirProfileWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int RedirProfileWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
