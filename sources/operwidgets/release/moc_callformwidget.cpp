/****************************************************************************
** Meta object code from reading C++ file 'callformwidget.h'
**
** Created: Tue 26. Mar 13:57:39 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../callformwidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'callformwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CallFormWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x08,
      45,   15,   41,   15, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_CallFormWidget[] = {
    "CallFormWidget\0\0tabTitleChanged(QString)\0"
    "int\0type()\0"
};

void CallFormWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CallFormWidget *_t = static_cast<CallFormWidget *>(_o);
        switch (_id) {
        case 0: _t->tabTitleChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: { int _r = _t->type();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CallFormWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CallFormWidget::staticMetaObject = {
    { &DockTab::staticMetaObject, qt_meta_stringdata_CallFormWidget,
      qt_meta_data_CallFormWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CallFormWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CallFormWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CallFormWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CallFormWidget))
        return static_cast<void*>(const_cast< CallFormWidget*>(this));
    return DockTab::qt_metacast(_clname);
}

int CallFormWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = DockTab::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
