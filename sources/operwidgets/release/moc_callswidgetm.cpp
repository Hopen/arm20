/****************************************************************************
** Meta object code from reading C++ file 'callswidgetm.h'
**
** Created: Tue 26. Mar 13:57:32 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../callswidgetm.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'callswidgetm.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QLineEditEx[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QLineEditEx[] = {
    "QLineEditEx\0"
};

void QLineEditEx::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QLineEditEx::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QLineEditEx::staticMetaObject = {
    { &QLineEdit::staticMetaObject, qt_meta_stringdata_QLineEditEx,
      qt_meta_data_QLineEditEx, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QLineEditEx::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QLineEditEx::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QLineEditEx::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QLineEditEx))
        return static_cast<void*>(const_cast< QLineEditEx*>(this));
    return QLineEdit::qt_metacast(_clname);
}

int QLineEditEx::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QLineEdit::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QDockMinimizedWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QDockMinimizedWidget[] = {
    "QDockMinimizedWidget\0"
};

void QDockMinimizedWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QDockMinimizedWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QDockMinimizedWidget::staticMetaObject = {
    { &QDockWidget::staticMetaObject, qt_meta_stringdata_QDockMinimizedWidget,
      qt_meta_data_QDockMinimizedWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QDockMinimizedWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QDockMinimizedWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QDockMinimizedWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QDockMinimizedWidget))
        return static_cast<void*>(const_cast< QDockMinimizedWidget*>(this));
    return QDockWidget::qt_metacast(_clname);
}

int QDockMinimizedWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDockWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_CallPropertyWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      25,   20,   19,   19, 0x05,

 // slots: signature, parameters, type, tag, flags
      50,   19,   19,   19, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_CallPropertyWidget[] = {
    "CallPropertyWidget\0\0size\0"
    "setCallComplited(qint32)\0"
    "emitCallStateTimeChanged()\0"
};

void CallPropertyWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CallPropertyWidget *_t = static_cast<CallPropertyWidget *>(_o);
        switch (_id) {
        case 0: _t->setCallComplited((*reinterpret_cast< qint32(*)>(_a[1]))); break;
        case 1: _t->emitCallStateTimeChanged(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CallPropertyWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CallPropertyWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_CallPropertyWidget,
      qt_meta_data_CallPropertyWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CallPropertyWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CallPropertyWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CallPropertyWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CallPropertyWidget))
        return static_cast<void*>(const_cast< CallPropertyWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int CallPropertyWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void CallPropertyWidget::setCallComplited(qint32 _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
static const uint qt_meta_data_CallsWidgetM[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x05,
      36,   13,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      45,   13,   13,   13, 0x0a,
      87,   13,   82,   13, 0x0a,
     116,   13,   13,   13, 0x0a,
     123,   13,   13,   13, 0x0a,
     157,   13,   13,   13, 0x0a,
     186,   13,   13,   13, 0x0a,
     205,   13,   13,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_CallsWidgetM[] = {
    "CallsWidgetM\0\0setWindowSize(qint32)\0"
    "expand()\0on_callStateChanged(ccappcore::Call)\0"
    "bool\0isOwnerCall(ccappcore::Call)\0"
    "test()\0on_actionSetCallCompleted(qint32)\0"
    "on_mainWindowMinimized(bool)\0"
    "on_stayOnTop(bool)\0on_minimized(bool)\0"
};

void CallsWidgetM::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CallsWidgetM *_t = static_cast<CallsWidgetM *>(_o);
        switch (_id) {
        case 0: _t->setWindowSize((*reinterpret_cast< qint32(*)>(_a[1]))); break;
        case 1: _t->expand(); break;
        case 2: _t->on_callStateChanged((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 3: { bool _r = _t->isOwnerCall((*reinterpret_cast< ccappcore::Call(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 4: _t->test(); break;
        case 5: _t->on_actionSetCallCompleted((*reinterpret_cast< qint32(*)>(_a[1]))); break;
        case 6: _t->on_mainWindowMinimized((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->on_stayOnTop((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->on_minimized((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CallsWidgetM::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CallsWidgetM::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_CallsWidgetM,
      qt_meta_data_CallsWidgetM, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CallsWidgetM::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CallsWidgetM::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CallsWidgetM::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CallsWidgetM))
        return static_cast<void*>(const_cast< CallsWidgetM*>(this));
    return QWidget::qt_metacast(_clname);
}

int CallsWidgetM::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void CallsWidgetM::setWindowSize(qint32 _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void CallsWidgetM::expand()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE
