/****************************************************************************
** Meta object code from reading C++ file 'opt_operstatus.h'
**
** Created: Tue 26. Mar 13:56:20 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../options/opt_operstatus.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'opt_operstatus.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_OptionsTabStatusWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      24,   23,   23,   23, 0x08,
      67,   23,   23,   23, 0x08,
      95,   23,   23,   23, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_OptionsTabStatusWidget[] = {
    "OptionsTabStatusWidget\0\0"
    "on_listStatusReason_itemSelectionChanged()\0"
    "on_bnRemoveStatus_clicked()\0"
    "on_bnAddStatus_clicked()\0"
};

void OptionsTabStatusWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        OptionsTabStatusWidget *_t = static_cast<OptionsTabStatusWidget *>(_o);
        switch (_id) {
        case 0: _t->on_listStatusReason_itemSelectionChanged(); break;
        case 1: _t->on_bnRemoveStatus_clicked(); break;
        case 2: _t->on_bnAddStatus_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData OptionsTabStatusWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject OptionsTabStatusWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_OptionsTabStatusWidget,
      qt_meta_data_OptionsTabStatusWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &OptionsTabStatusWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *OptionsTabStatusWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *OptionsTabStatusWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_OptionsTabStatusWidget))
        return static_cast<void*>(const_cast< OptionsTabStatusWidget*>(this));
    if (!strcmp(_clname, "Ui::OptionsTabOperStatus"))
        return static_cast< Ui::OptionsTabOperStatus*>(const_cast< OptionsTabStatusWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int OptionsTabStatusWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
static const uint qt_meta_data_OptionsTabOperStatus[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_OptionsTabOperStatus[] = {
    "OptionsTabOperStatus\0"
};

void OptionsTabOperStatus::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData OptionsTabOperStatus::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject OptionsTabOperStatus::staticMetaObject = {
    { &OptionsTab::staticMetaObject, qt_meta_stringdata_OptionsTabOperStatus,
      qt_meta_data_OptionsTabOperStatus, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &OptionsTabOperStatus::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *OptionsTabOperStatus::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *OptionsTabOperStatus::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_OptionsTabOperStatus))
        return static_cast<void*>(const_cast< OptionsTabOperStatus*>(this));
    return OptionsTab::qt_metacast(_clname);
}

int OptionsTabOperStatus::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = OptionsTab::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
