/****************************************************************************
** Meta object code from reading C++ file 'contactstatusmenu.h'
**
** Created: Tue 26. Mar 13:56:45 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../contactstatusmenu.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'contactstatusmenu.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ContactStatusMenu[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      19,   18,   18,   18, 0x05,

 // slots: signature, parameters, type, tag, flags
      64,   56,   18,   18, 0x0a,
     121,   18,   18,   18, 0x0a,
     141,   18,   18,   18, 0x08,
     179,   18,   18,   18, 0x08,
     197,   18,   18,   18, 0x08,
     215,   18,   18,   18, 0x08,
     234,   18,   18,   18, 0x08,
     272,  262,   18,   18, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ContactStatusMenu[] = {
    "ContactStatusMenu\0\0"
    "checkedActionChanged(const QAction*)\0"
    ",reason\0"
    "requestStatus(ccappcore::Contact::ContactStatus,QString)\0"
    "loadCustomReasons()\0"
    "operStatusChanged(ccappcore::Contact)\0"
    "operListChanged()\0actionTriggered()\0"
    "profileTriggered()\0on_profileActivated(qint32)\0"
    "errorText\0on_profileActivateError(QString)\0"
};

void ContactStatusMenu::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ContactStatusMenu *_t = static_cast<ContactStatusMenu *>(_o);
        switch (_id) {
        case 0: _t->checkedActionChanged((*reinterpret_cast< const QAction*(*)>(_a[1]))); break;
        case 1: _t->requestStatus((*reinterpret_cast< ccappcore::Contact::ContactStatus(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 2: _t->loadCustomReasons(); break;
        case 3: _t->operStatusChanged((*reinterpret_cast< ccappcore::Contact(*)>(_a[1]))); break;
        case 4: _t->operListChanged(); break;
        case 5: _t->actionTriggered(); break;
        case 6: _t->profileTriggered(); break;
        case 7: _t->on_profileActivated((*reinterpret_cast< qint32(*)>(_a[1]))); break;
        case 8: _t->on_profileActivateError((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ContactStatusMenu::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ContactStatusMenu::staticMetaObject = {
    { &QMenu::staticMetaObject, qt_meta_stringdata_ContactStatusMenu,
      qt_meta_data_ContactStatusMenu, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ContactStatusMenu::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ContactStatusMenu::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ContactStatusMenu::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ContactStatusMenu))
        return static_cast<void*>(const_cast< ContactStatusMenu*>(this));
    return QMenu::qt_metacast(_clname);
}

int ContactStatusMenu::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMenu::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void ContactStatusMenu::checkedActionChanged(const QAction * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
