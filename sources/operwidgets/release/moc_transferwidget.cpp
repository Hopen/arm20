/****************************************************************************
** Meta object code from reading C++ file 'transferwidget.h'
**
** Created: Tue 26. Mar 13:57:41 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../transferwidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'transferwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TransferContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x05,
      45,   16,   16,   16, 0x05,
      74,   16,   16,   16, 0x05,

 // slots: signature, parameters, type, tag, flags
     101,   16,   16,   16, 0x0a,
     121,   16,   16,   16, 0x0a,
     139,   16,   16,   16, 0x0a,
     158,   16,   16,   16, 0x0a,
     177,  175,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_TransferContext[] = {
    "TransferContext\0\0operPhoneSelected(QVariant)\0"
    "groupPhoneSelected(QVariant)\0"
    "ivrScriptSelected(QString)\0"
    "on_defaulTransfer()\0on_operTransfer()\0"
    "on_groupTransfer()\0on_ivrTransfer()\0"
    "c\0setTransferCall(ccappcore::Call)\0"
};

void TransferContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TransferContext *_t = static_cast<TransferContext *>(_o);
        switch (_id) {
        case 0: _t->operPhoneSelected((*reinterpret_cast< QVariant(*)>(_a[1]))); break;
        case 1: _t->groupPhoneSelected((*reinterpret_cast< QVariant(*)>(_a[1]))); break;
        case 2: _t->ivrScriptSelected((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->on_defaulTransfer(); break;
        case 4: _t->on_operTransfer(); break;
        case 5: _t->on_groupTransfer(); break;
        case 6: _t->on_ivrTransfer(); break;
        case 7: _t->setTransferCall((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData TransferContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TransferContext::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_TransferContext,
      qt_meta_data_TransferContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TransferContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TransferContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TransferContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TransferContext))
        return static_cast<void*>(const_cast< TransferContext*>(this));
    return QWidget::qt_metacast(_clname);
}

int TransferContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void TransferContext::operPhoneSelected(QVariant _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void TransferContext::groupPhoneSelected(QVariant _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void TransferContext::ivrScriptSelected(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
static const uint qt_meta_data_TransferWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x05,
      38,   15,   15,   15, 0x05,

 // slots: signature, parameters, type, tag, flags
      61,   15,   15,   15, 0x0a,
      89,   15,   15,   15, 0x0a,
     124,   15,   15,   15, 0x0a,
     151,   15,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_TransferWidget[] = {
    "TransferWidget\0\0lineSelected(QString)\0"
    "lineSelected(QVariant)\0"
    "on_bnTransfer_clicked(bool)\0"
    "on_searchLine_textChanged(QString)\0"
    "on_lineSelect(QModelIndex)\0"
    "on_lineSelected(QModelIndex)\0"
};

void TransferWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TransferWidget *_t = static_cast<TransferWidget *>(_o);
        switch (_id) {
        case 0: _t->lineSelected((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->lineSelected((*reinterpret_cast< QVariant(*)>(_a[1]))); break;
        case 2: _t->on_bnTransfer_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->on_searchLine_textChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->on_lineSelect((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 5: _t->on_lineSelected((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData TransferWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TransferWidget::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_TransferWidget,
      qt_meta_data_TransferWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TransferWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TransferWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TransferWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TransferWidget))
        return static_cast<void*>(const_cast< TransferWidget*>(this));
    return QDialog::qt_metacast(_clname);
}

int TransferWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void TransferWidget::lineSelected(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void TransferWidget::lineSelected(QVariant _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
static const uint qt_meta_data_OperContactsWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_OperContactsWidget[] = {
    "OperContactsWidget\0"
};

void OperContactsWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData OperContactsWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject OperContactsWidget::staticMetaObject = {
    { &TransferWidget::staticMetaObject, qt_meta_stringdata_OperContactsWidget,
      qt_meta_data_OperContactsWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &OperContactsWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *OperContactsWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *OperContactsWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_OperContactsWidget))
        return static_cast<void*>(const_cast< OperContactsWidget*>(this));
    return TransferWidget::qt_metacast(_clname);
}

int OperContactsWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TransferWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_GroupContactsWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_GroupContactsWidget[] = {
    "GroupContactsWidget\0"
};

void GroupContactsWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData GroupContactsWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject GroupContactsWidget::staticMetaObject = {
    { &TransferWidget::staticMetaObject, qt_meta_stringdata_GroupContactsWidget,
      qt_meta_data_GroupContactsWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &GroupContactsWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *GroupContactsWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *GroupContactsWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_GroupContactsWidget))
        return static_cast<void*>(const_cast< GroupContactsWidget*>(this));
    return TransferWidget::qt_metacast(_clname);
}

int GroupContactsWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TransferWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_IvrScriptWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_IvrScriptWidget[] = {
    "IvrScriptWidget\0"
};

void IvrScriptWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData IvrScriptWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject IvrScriptWidget::staticMetaObject = {
    { &TransferWidget::staticMetaObject, qt_meta_stringdata_IvrScriptWidget,
      qt_meta_data_IvrScriptWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &IvrScriptWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *IvrScriptWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *IvrScriptWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_IvrScriptWidget))
        return static_cast<void*>(const_cast< IvrScriptWidget*>(this));
    return TransferWidget::qt_metacast(_clname);
}

int IvrScriptWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TransferWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
