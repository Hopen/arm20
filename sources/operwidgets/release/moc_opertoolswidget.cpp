/****************************************************************************
** Meta object code from reading C++ file 'opertoolswidget.h'
**
** Created: Tue 26. Mar 13:57:14 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../opertoolswidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'opertoolswidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Button[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_Button[] = {
    "Button\0"
};

void Button::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData Button::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Button::staticMetaObject = {
    { &QToolButton::staticMetaObject, qt_meta_stringdata_Button,
      qt_meta_data_Button, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Button::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Button::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Button::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Button))
        return static_cast<void*>(const_cast< Button*>(this));
    return QToolButton::qt_metacast(_clname);
}

int Button::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QToolButton::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_OperToolBar[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x05,
      49,   12,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
      68,   12,   12,   12, 0x0a,
      91,   12,   12,   12, 0x0a,
     125,  117,   12,   12, 0x0a,
     149,  117,   12,   12, 0x0a,
     172,   12,   12,   12, 0x0a,
     198,   12,   12,   12, 0x0a,
     234,  232,   12,   12, 0x0a,
     273,   12,   12,   12, 0x0a,
     300,   12,   12,   12, 0x0a,
     335,   12,   12,   12, 0x0a,
     367,   12,   12,   12, 0x0a,
     400,   12,   12,   12, 0x0a,
     422,   12,   12,   12, 0x0a,
     463,  455,   12,   12, 0x0a,
     502,  491,   12,   12, 0x0a,
     528,   12,   12,   12, 0x0a,
     564,  546,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_OperToolBar[] = {
    "OperToolBar\0\0updateCallControls(ccappcore::Call)\0"
    "callTransfer(bool)\0on_bnCallBtn_clicked()\0"
    "on_bnCallEndBtn_clicked()\0checked\0"
    "on_bnHold_clicked(bool)\0on_bnRec_clicked(bool)\0"
    "on_bnCancel_clicked(bool)\0"
    "on_phoneLine_textChanged(QString)\0c\0"
    "on_updateCallControls(ccappcore::Call)\0"
    "on_bnVoiceMailTo_clicked()\0"
    "on_bnVoiceMailToOperator_clicked()\0"
    "on_bnVoiceMailToGroup_clicked()\0"
    "on_bnVoiceMailToSystem_clicked()\0"
    "on_OperToolsChanged()\0"
    "on_callActivate(ccappcore::Call)\0"
    "contact\0onContactSelected(QVariant)\0"
    "scriptName\0onScriptSelected(QString)\0"
    "on_digitClicked()\0call,onOff,forced\0"
    "on_c2o_record(ccappcore::Call,bool,bool)\0"
};

void OperToolBar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        OperToolBar *_t = static_cast<OperToolBar *>(_o);
        switch (_id) {
        case 0: _t->updateCallControls((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 1: _t->callTransfer((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->on_bnCallBtn_clicked(); break;
        case 3: _t->on_bnCallEndBtn_clicked(); break;
        case 4: _t->on_bnHold_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->on_bnRec_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->on_bnCancel_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->on_phoneLine_textChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 8: _t->on_updateCallControls((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 9: _t->on_bnVoiceMailTo_clicked(); break;
        case 10: _t->on_bnVoiceMailToOperator_clicked(); break;
        case 11: _t->on_bnVoiceMailToGroup_clicked(); break;
        case 12: _t->on_bnVoiceMailToSystem_clicked(); break;
        case 13: _t->on_OperToolsChanged(); break;
        case 14: _t->on_callActivate((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 15: _t->onContactSelected((*reinterpret_cast< QVariant(*)>(_a[1]))); break;
        case 16: _t->onScriptSelected((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 17: _t->on_digitClicked(); break;
        case 18: _t->on_c2o_record((*reinterpret_cast< ccappcore::Call(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData OperToolBar::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject OperToolBar::staticMetaObject = {
    { &QToolBar::staticMetaObject, qt_meta_stringdata_OperToolBar,
      qt_meta_data_OperToolBar, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &OperToolBar::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *OperToolBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *OperToolBar::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_OperToolBar))
        return static_cast<void*>(const_cast< OperToolBar*>(this));
    return QToolBar::qt_metacast(_clname);
}

int OperToolBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QToolBar::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    }
    return _id;
}

// SIGNAL 0
void OperToolBar::updateCallControls(ccappcore::Call _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void OperToolBar::callTransfer(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
