/****************************************************************************
** Meta object code from reading C++ file 'chattab.h'
**
** Created: Tue 26. Mar 13:57:42 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../chattab.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'chattab.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ChatTab[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,    9,    8,    8, 0x05,

 // slots: signature, parameters, type, tag, flags
      39,    8,    8,    8, 0x0a,
      61,    8,    8,    8, 0x0a,
      83,    8,    8,    8, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ChatTab[] = {
    "ChatTab\0\0status\0titleStatusChange(int)\0"
    "on_inputTextChanged()\0on_btnSendTriggered()\0"
    "updateControls()\0"
};

void ChatTab::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ChatTab *_t = static_cast<ChatTab *>(_o);
        switch (_id) {
        case 0: _t->titleStatusChange((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_inputTextChanged(); break;
        case 2: _t->on_btnSendTriggered(); break;
        case 3: _t->updateControls(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ChatTab::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ChatTab::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_ChatTab,
      qt_meta_data_ChatTab, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ChatTab::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ChatTab::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ChatTab::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ChatTab))
        return static_cast<void*>(const_cast< ChatTab*>(this));
    return QWidget::qt_metacast(_clname);
}

int ChatTab::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void ChatTab::titleStatusChange(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
