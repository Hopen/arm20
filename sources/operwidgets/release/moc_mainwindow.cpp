/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created: Tue 26. Mar 13:56:35 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      45,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
      30,   11,   11,   11, 0x0a,
      49,   11,   11,   11, 0x0a,
      63,   11,   11,   11, 0x0a,
      89,   83,   11,   11, 0x0a,
     109,   11,   11,   11, 0x2a,
     124,  122,   11,   11, 0x0a,
     154,   11,   11,   11, 0x0a,
     172,   11,   11,   11, 0x0a,
     179,   11,   11,   11, 0x0a,
     214,  206,   11,   11, 0x0a,
     250,   11,   11,   11, 0x0a,
     269,   11,   11,   11, 0x0a,
     288,   11,   11,   11, 0x0a,
     309,   11,   11,   11, 0x0a,
     336,  330,   11,   11, 0x0a,
     356,   11,   11,   11, 0x0a,
     381,   11,   11,   11, 0x08,
     413,   11,   11,   11, 0x08,
     445,  206,   11,   11, 0x08,
     488,  206,   11,   11, 0x08,
     533,  206,   11,   11, 0x08,
     576,   11,   11,   11, 0x08,
     606,   11,   11,   11, 0x08,
     633,   11,   11,   11, 0x08,
     665,   11,   11,   11, 0x08,
     691,   11,   11,   11, 0x08,
     735,  730,   11,   11, 0x08,
     772,  206,   11,   11, 0x08,
     817,  206,   11,   11, 0x08,
     859,  206,   11,   11, 0x08,
     901,  206,   11,   11, 0x08,
     944,  206,   11,   11, 0x08,
     983,  206,   11,   11, 0x08,
    1024,  206,   11,   11, 0x08,
    1066,  206,   11,   11, 0x08,
    1095,  206,   11,   11, 0x08,
    1126,  206,   11,   11, 0x08,
    1170,  206,   11,   11, 0x08,
    1214,  206,   11,   11, 0x08,
    1256,   11,   11,   11, 0x28,
    1294,   11,   11,   11, 0x08,
    1320,   11, 1315,   11, 0x08,
    1335, 1332,   11,   11, 0x08,
    1420, 1407,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0privateOnly(bool)\0"
    "toggleVisibility()\0togglePause()\0"
    "moveFocusOnSearch()\0phone\0showDialer(QString)\0"
    "showDialer()\0c\0showCallForm(ccappcore::Call)\0"
    "showLoginDialog()\0test()\0"
    "activateForegroundWindow()\0checked\0"
    "on_callform_visibilityChanged(bool)\0"
    "onOperformClosed()\0onCallformClosed()\0"
    "onShowVolumeWidget()\0onHideVolumeWidget()\0"
    "value\0onVolumeChange(int)\0"
    "onVolumeSliderReleased()\0"
    "on_actionSavePreset_triggered()\0"
    "on_actionShowDialer_triggered()\0"
    "on_actionPrivateGroupsOnly_triggered(bool)\0"
    "on_actionHideOfflineContacts_triggered(bool)\0"
    "on_actionOperListGroupMode_triggered(bool)\0"
    "on_actionSettings_triggered()\0"
    "on_actionAbout_triggered()\0"
    "on_actionDisconnect_triggered()\0"
    "on_actionQuit_triggered()\0"
    "callToTransferChanged(ccappcore::Call)\0"
    "size\0onActionExpandMinimzedWindow(qint32)\0"
    "on_actionOperators_only_test_triggered(bool)\0"
    "on_actionGroups_only_test_triggered(bool)\0"
    "on_action_showUserCallTab_triggered(bool)\0"
    "on_action_showQueueCallTab_triggered(bool)\0"
    "on_action_showUsersTab_triggered(bool)\0"
    "on_action_showHistoryTab_triggered(bool)\0"
    "on_action_showMessagesTab_triggered(bool)\0"
    "onActionMinimizeWindow(bool)\0"
    "onActionStaysOnTopWindow(bool)\0"
    "on_action_operform_visiable_triggered(bool)\0"
    "on_action_callform_visiable_triggered(bool)\0"
    "on_action_showWGStatusTab_triggered(bool)\0"
    "on_action_showWGStatusTab_triggered()\0"
    "disconnectHandlers()\0bool\0maybeQuit()\0"
    ",,\0"
    "showCallFailReason(ccappcore::Call,ccappcore::Call::CallResult,QString"
    ")\0"
    "callid,error\0showJoinCallToConfFailedReason(int,int)\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->privateOnly((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->toggleVisibility(); break;
        case 2: _t->togglePause(); break;
        case 3: _t->moveFocusOnSearch(); break;
        case 4: _t->showDialer((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->showDialer(); break;
        case 6: _t->showCallForm((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 7: _t->showLoginDialog(); break;
        case 8: _t->test(); break;
        case 9: _t->activateForegroundWindow(); break;
        case 10: _t->on_callform_visibilityChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: _t->onOperformClosed(); break;
        case 12: _t->onCallformClosed(); break;
        case 13: _t->onShowVolumeWidget(); break;
        case 14: _t->onHideVolumeWidget(); break;
        case 15: _t->onVolumeChange((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->onVolumeSliderReleased(); break;
        case 17: _t->on_actionSavePreset_triggered(); break;
        case 18: _t->on_actionShowDialer_triggered(); break;
        case 19: _t->on_actionPrivateGroupsOnly_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 20: _t->on_actionHideOfflineContacts_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 21: _t->on_actionOperListGroupMode_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 22: _t->on_actionSettings_triggered(); break;
        case 23: _t->on_actionAbout_triggered(); break;
        case 24: _t->on_actionDisconnect_triggered(); break;
        case 25: _t->on_actionQuit_triggered(); break;
        case 26: _t->callToTransferChanged((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 27: _t->onActionExpandMinimzedWindow((*reinterpret_cast< qint32(*)>(_a[1]))); break;
        case 28: _t->on_actionOperators_only_test_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 29: _t->on_actionGroups_only_test_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 30: _t->on_action_showUserCallTab_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 31: _t->on_action_showQueueCallTab_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 32: _t->on_action_showUsersTab_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 33: _t->on_action_showHistoryTab_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 34: _t->on_action_showMessagesTab_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 35: _t->onActionMinimizeWindow((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 36: _t->onActionStaysOnTopWindow((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 37: _t->on_action_operform_visiable_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 38: _t->on_action_callform_visiable_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 39: _t->on_action_showWGStatusTab_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 40: _t->on_action_showWGStatusTab_triggered(); break;
        case 41: _t->disconnectHandlers(); break;
        case 42: { bool _r = _t->maybeQuit();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 43: _t->showCallFailReason((*reinterpret_cast< ccappcore::Call(*)>(_a[1])),(*reinterpret_cast< ccappcore::Call::CallResult(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 44: _t->showJoinCallToConfFailedReason((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 45)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 45;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::privateOnly(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
