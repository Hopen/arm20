/****************************************************************************
** Meta object code from reading C++ file 'dialpadform.h'
**
** Created: Tue 26. Mar 13:56:43 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../dialpadform.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dialpadform.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DialpadForm[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x0a,
      31,   12,   12,   12, 0x08,
      61,   12,   12,   12, 0x08,
      85,   12,   12,   12, 0x08,
     109,   12,   12,   12, 0x08,
     148,   12,   12,   12, 0x08,
     169,   12,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_DialpadForm[] = {
    "DialpadForm\0\0setFocusOnPhone()\0"
    "on_phone_textChanged(QString)\0"
    "on_bnMakeCall_clicked()\0dialpadButton_clicked()\0"
    "callToTransferChanged(ccappcore::Call)\0"
    "updateButtonsState()\0hideIfUndocked()\0"
};

void DialpadForm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DialpadForm *_t = static_cast<DialpadForm *>(_o);
        switch (_id) {
        case 0: _t->setFocusOnPhone(); break;
        case 1: _t->on_phone_textChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->on_bnMakeCall_clicked(); break;
        case 3: _t->dialpadButton_clicked(); break;
        case 4: _t->callToTransferChanged((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 5: _t->updateButtonsState(); break;
        case 6: _t->hideIfUndocked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData DialpadForm::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DialpadForm::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_DialpadForm,
      qt_meta_data_DialpadForm, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DialpadForm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DialpadForm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DialpadForm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DialpadForm))
        return static_cast<void*>(const_cast< DialpadForm*>(this));
    return QWidget::qt_metacast(_clname);
}

int DialpadForm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
