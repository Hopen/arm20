/****************************************************************************
** Meta object code from reading C++ file 'toolbuttonoperstatus.h'
**
** Created: Tue 26. Mar 13:56:34 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../toolbuttonoperstatus.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'toolbuttonoperstatus.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ToolButtonOperStatus[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      28,   22,   21,   21, 0x05,
      42,   21,   21,   21, 0x25,

 // slots: signature, parameters, type, tag, flags
      52,   21,   21,   21, 0x08,
      71,   64,   21,   21, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ToolButtonOperStatus[] = {
    "ToolButtonOperStatus\0\0Pause\0changed(bool)\0"
    "changed()\0onClicked()\0action\0"
    "onCheckedActionChanged(const QAction*)\0"
};

void ToolButtonOperStatus::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ToolButtonOperStatus *_t = static_cast<ToolButtonOperStatus *>(_o);
        switch (_id) {
        case 0: _t->changed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->changed(); break;
        case 2: _t->onClicked(); break;
        case 3: _t->onCheckedActionChanged((*reinterpret_cast< const QAction*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ToolButtonOperStatus::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ToolButtonOperStatus::staticMetaObject = {
    { &QToolButton::staticMetaObject, qt_meta_stringdata_ToolButtonOperStatus,
      qt_meta_data_ToolButtonOperStatus, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ToolButtonOperStatus::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ToolButtonOperStatus::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ToolButtonOperStatus::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ToolButtonOperStatus))
        return static_cast<void*>(const_cast< ToolButtonOperStatus*>(this));
    return QToolButton::qt_metacast(_clname);
}

int ToolButtonOperStatus::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QToolButton::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void ToolButtonOperStatus::changed(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
static const uint qt_meta_data_ToolButtonOperStatusEx[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      32,   24,   23,   23, 0x05,

 // slots: signature, parameters, type, tag, flags
      54,   24,   23,   23, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ToolButtonOperStatusEx[] = {
    "ToolButtonOperStatusEx\0\0isPause\0"
    "quickBtnClicked(bool)\0"
    "onToolButtonOperStatusChanged(bool)\0"
};

void ToolButtonOperStatusEx::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ToolButtonOperStatusEx *_t = static_cast<ToolButtonOperStatusEx *>(_o);
        switch (_id) {
        case 0: _t->quickBtnClicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->onToolButtonOperStatusChanged((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ToolButtonOperStatusEx::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ToolButtonOperStatusEx::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_ToolButtonOperStatusEx,
      qt_meta_data_ToolButtonOperStatusEx, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ToolButtonOperStatusEx::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ToolButtonOperStatusEx::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ToolButtonOperStatusEx::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ToolButtonOperStatusEx))
        return static_cast<void*>(const_cast< ToolButtonOperStatusEx*>(this));
    return QWidget::qt_metacast(_clname);
}

int ToolButtonOperStatusEx::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void ToolButtonOperStatusEx::quickBtnClicked(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
