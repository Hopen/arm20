#ifndef CONTACTSMANAGER_H
#define CONTACTSMANAGER_H

#include "operwidgets_global.h"
#include "appcore/appsettings.h"
#include "appcore/proxymodeltree.h"
#include "appcore/proxymodellist.h"

#include "opersettings.h"
#include "contactstatusmenu.h"
#include "contactcontextmenu.h"
#include "callcontextmenu.h"

using namespace ccappcore;

class ContactsManager
    : public QObject
    , public IAppSettingsHandler
{
    Q_OBJECT
    Q_INTERFACES(ccappcore::IAppSettingsHandler)
public:
    ContactsManager(QObject* parent=0);

    void reset();

    bool isOperListByGroupMode() const { return _operListByGroupsMode; }
    bool hideOfflineOperators() const { return _hideOfflineOperators; }
    bool isHideOperators()const {return _hideOperators;}
    bool isHideGroups()const {return _hideGroups;}

    QAbstractItemModel* contactsModel() { return &_contactsModel; }
    QAbstractItemModel* callsModel() { return &_callsModel; }

    ccappcore::Operator operAt(QModelIndex index) const;

    void connectView(QAbstractItemView* view);

public slots:
    void searchContactsAsync(const QString& searchText);

    void hideOfflineOperators(bool hide);
    void setOperListByGroupsMode(bool hide);
    void showOnlyPrivateGroups(bool privateGroupsOnly);

    void hideOperators(bool hide);
    void hideGroups(bool hide);
    void resetOperFlags();

private slots:
    void customContextMenuRequested();
    void itemActivated(QModelIndex);
    void itemClicked(QModelIndex);
    //void itemDoubleClicked(QModelIndex);
public: //IAppSettingsHandler
    //Apply service configuration.
    virtual void applySettings(const AppSettings&);

    //Flush service configuration
    virtual void flushSettings(AppSettings&);

private:
    LateBoundObject<OperListController> _operListController;
    LateBoundObject<CallsController>    _callController;
    LateBoundObject<OperSettings>       _operSettings;
    LateBoundObject<AppContext>         _appContext;
    LateBoundObject<JabberController>   _jabberController;

    ContactContextMenu  _operatorContextMenu;
    CallContextMenu     _callContextMenu;

    ProxyModelList _contactsModel;
    ProxyModelList _callsModel;

    bool _hideOfflineOperators;
    bool _operListByGroupsMode;
    bool _hideOperators;
    bool _hideGroups;

    void loadContactsModel();
    void loadCallsModel();
};

#endif // CONTACTSMANAGER_H
