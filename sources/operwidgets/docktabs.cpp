#include "docktabs.h"

QDockWidgetEx::QDockWidgetEx(const QString &title, QWidget *parent, Qt::WindowFlags flags)
    :QDockWidget(title,parent,flags)
{
}

QDockWidgetEx::QDockWidgetEx(QWidget *parent, Qt::WindowFlags flags)
    :QDockWidget(parent,flags)
{
    QDockWidgetEx(tr(""),parent,flags);
}

QDockWidgetEx::~QDockWidgetEx()
{
    //int test = 0;
}

void QDockWidgetEx::closeEvent(QCloseEvent *)
{
    emit closed();
}

bool QDockWidgetEx::event(QEvent *evt)
{
    if (evt->type() == QEvent::ChildAdded)
    {
        emit childAdded();
    }

    return QDockWidget::event(evt);
}

QSize QDockWidgetEx::minimumSizeHint() const
{
    QSize size = QDockWidget::minimumSizeHint();
    return size;
}

const int closeButtonRight = 5;

//DocklTitleBar::DocklTitleBar(const QString& title, QWidget *parent)
//    :QWidget(parent), _title(title), _activeStyle(0),/*_operAttention(true),*/_pCurrentStyle(&_activeStyle)
//{
////    _leftPm   = QPixmap(":/style/resources/style/dockbkg1Left.png");
////    _centerPm = QPixmap(":/style/resources/style/dockbkg1Center.png");
////    _rightPm = QPixmap(":/style/resources/style/dockbkg1Right.png");

////    _leftPmGray   = QPixmap(":/style/resources/style/dockbkg2Left.png");
////    _centerPmGray = QPixmap(":/style/resources/style/dockbkg2Center.png");
////    _rightPmGray  = QPixmap(":/style/resources/style/dockbkg2Right.png");

//    //active - free
//    _styles.push_back(TStyle(QPixmap(":/style/resources/style/dockbkg3Left.png"),
//                            QPixmap(":/style/resources/style/dockbkg3Center.png"),
//                            QPixmap(":/style/resources/style/dockbkg3Right.png")));

//    //alert - busy
//    _styles.push_back(TStyle(QPixmap(":/style/resources/style/dockbkg4Left.png"),
//                            QPixmap(":/style/resources/style/dockbkg4Center.png"),
//                            QPixmap(":/style/resources/style/dockbkg4Right.png")));

//    //hold - pause
//    _styles.push_back(TStyle(QPixmap(":/style/resources/style/dockbkg1Left.png"),
//                            QPixmap(":/style/resources/style/dockbkg1Center.png"),
//                            QPixmap(":/style/resources/style/dockbkg1Right.png")));

//    //complete - offline
//    _styles.push_back(TStyle(QPixmap(":/style/resources/style/dockbkg2Left.png"),
//                            QPixmap(":/style/resources/style/dockbkg2Center.png"),
//                            QPixmap(":/style/resources/style/dockbkg2Right.png")));
//    _attentionStyle = 1;

//    _closeButton = QPixmap(":/resources/icons/new/delete.png");

//    connect(&_operAttentionTimer, SIGNAL(timeout()),
//            this, SLOT(emitAttentionTimer()));


////    _pxmIcon = QPixmap(":/resources/icons/new/pawn_glass_white.png");
////    _queueStatus = new QLabel(tr("QS"));
////    _queueStatus->setPixmap(_pxmIcon);

////    QLabel * someTitle = new QLabel(tr("some title"));


////    QWidget* spacer = new QWidget();
////    spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

////    QHBoxLayout* mainLayout1 = new QHBoxLayout();
////    mainLayout1->addWidget(_queueStatus);
////    mainLayout1->addWidget(someTitle);
////    mainLayout1->addWidget(spacer);
////    mainLayout1->addWidget(mailButton);
////    setLayout(mainLayout1);
//}

//void DocklTitleBar::setActiveStyle(const int &style)
//{
//    if (style>=_styles.size())
//        return;
//    _activeStyle = style;
//    repaint();
//}

//void DocklTitleBar::paintEvent(QPaintEvent*)
//{
//    QPainter painter(this);
//    QRect rect = this->rect();

//    QDockWidget *dw = qobject_cast<QDockWidget*>(parentWidget());
//    Q_ASSERT(dw != 0);

//    if (dw->features() & QDockWidget::DockWidgetVerticalTitleBar) {
//        QSize s = rect.size();
//        s.transpose();
//        rect.setSize(s);

//        painter.translate(rect.left(), rect.top() + rect.width());
//        painter.rotate(-90);
//        painter.translate(-rect.left(), -rect.top());
//    }


////    QPixmap * pLeft   = &_styles[_currentStyle].left;
////    QPixmap * pRight  = &_styles[_currentStyle].right;
////    QPixmap * pCenter = &_styles[_currentStyle].center;


////    painter.drawPixmap(rect.topLeft(), *pLeft);
////    painter.drawPixmap(rect.topRight() - QPoint(pRight->width() - 1, 0), *pRight);
////    painter.fillRect(rect.left() + pLeft->width(), rect.top(),
////                        rect.width() - pLeft->width() - pRight->width(),
////                        pCenter->height(), *pCenter);

//    QPixmap * pCenter = &_styles[*_pCurrentStyle].center;
//    painter.fillRect(rect,*pCenter);

//    painter.drawPixmap(rect.topRight() - QPoint(_closeButton.width() + closeButtonRight,
//                                                -( rect.height()/2 - _closeButton.height()/2)),
//                       _closeButton);

//    QRect r = rect;
//    r.adjust(10, -2, _closeButton.width(), 1);
//    painter.translate(4, 4);
//    painter.setPen(Qt::black);
//    painter.drawText(r, Qt::AlignLeft|Qt::AlignTop, _title);
//}


//QSize DocklTitleBar::minimumSizeHint() const
//{
//    QDockWidget *dw = qobject_cast<QDockWidget*>(parentWidget());
//    Q_ASSERT(dw != 0);
//    const QPixmap * pLeft   = &_styles[*_pCurrentStyle].left;
//    const QPixmap * pRight  = &_styles[*_pCurrentStyle].right;
//    QSize result(pLeft->width() + pRight->width(), pLeft->height());
//    if (dw->features() & QDockWidget::DockWidgetVerticalTitleBar)
//        result.transpose();
//    return result;
//}

//void DocklTitleBar::mousePressEvent(QMouseEvent *event)
//{
//    QPoint pos = event->pos();

//    QRect rect = this->rect();


//    QDockWidget *dw = qobject_cast<QDockWidget*>(parentWidget());
//    Q_ASSERT(dw != 0);

//    if (dw->features() & QDockWidget::DockWidgetVerticalTitleBar) {
//        QPoint p = pos;
//        pos.setX(rect.left() + rect.bottom() - p.y());
//        pos.setY(rect.top() + p.x() - rect.left());

//        QSize s = rect.size();
//        s.transpose();
//        rect.setSize(s);
//    }
//    const int buttonRight = closeButtonRight;
//    const int buttonWidth = _closeButton.width();
//    int right = rect.right() - pos.x();
//    int button = (right - buttonRight)/buttonWidth;
//    switch (button) {
//        case 0:
//            event->accept();
//            dw->close();
//            break;
////        case 1:
////            event->accept();
////            dw->setFloating(!dw->isFloating());
////            break;
////        case 2: {
////            event->accept();
////            QDockWidget::DockWidgetFeatures features = dw->features();
////            if (features & QDockWidget::DockWidgetVerticalTitleBar)
////                features &= ~QDockWidget::DockWidgetVerticalTitleBar;
////            else
////                features |= QDockWidget::DockWidgetVerticalTitleBar;
////            dw->setFeatures(features);
////            break;
////        }
//        default:
//            event->ignore();
//            break;
//    }
//}


////void DocklTitleBar::updateMask()
////{
////    QDockWidget *dw = qobject_cast<QDockWidget*>(parent());
////    Q_ASSERT(dw != 0);

////    QRect rect = dw->rect();
////    QPixmap bitmap(dw->size());

////    {
////        QPainter painter(&bitmap);

//////        ///initialize to transparent
//////        painter.fillRect(rect, Qt::color0);

//////        QRect contents = rect;
//////        contents.setTopLeft(geometry().bottomLeft());
//////        contents.setRight(geometry().right());
//////        contents.setBottom(contents.bottom()-y());
//////        painter.fillRect(contents, Qt::color1);



////        //let's pait the titlebar

////        QRect titleRect = this->geometry();

////        if (dw->features() & QDockWidget::DockWidgetVerticalTitleBar) {
////            QSize s = rect.size();
////            s.transpose();
////            rect.setSize(s);

////            QSize s2 = size();
////            s2.transpose();
////            titleRect.setSize(s2);

////            painter.translate(rect.left(), rect.top() + rect.width());
////            painter.rotate(-90);
////            painter.translate(-rect.left(), -rect.top());
////        }

////        QRect contents = rect; //1
////        contents.setBottom(geometry().bottom());
////        painter.fillRect(contents, Qt::color0); // 2

////        contents = rect;
////        contents.setTopLeft(geometry().bottomLeft()); // 4
////        contents.setRight(geometry().right());
////        contents.setBottom(contents.bottom()-y());
////        //painter.fillRect(contents, Qt::color0);


////        QRect rect = titleRect;

////        QPixmap * pLeft   = &_styles[*_pCurrentStyle].left;
////        QPixmap * pRight  = &_styles[*_pCurrentStyle].right;
////        QPixmap * pCenter = &_styles[*_pCurrentStyle].center;

////        painter.drawPixmap(rect.topLeft(), pLeft->mask());
////        painter.fillRect(rect.left() + pLeft->width(), rect.top(),
////                            rect.width() - pLeft->width() - pRight->width(),
////                            pCenter->height(), Qt::color1);
////        painter.drawPixmap(rect.topRight() - QPoint(pRight->width() - 1, 0), pRight->mask());

////        //painter.fillRect(contents, Qt::color1);
////    }

////    dw->setMask(bitmap);
////}

//void DocklTitleBar::emitAttentionTimer()
//{
//    _pCurrentStyle = (*_pCurrentStyle == _activeStyle)? &_attentionStyle:&_activeStyle;
//    repaint();
//}

//void DocklTitleBar::startAlerting()
//{
//    _operAttentionTimer.start(300);
//}

//void DocklTitleBar::stopAlerting()
//{
//    _operAttentionTimer.stop();
//    _pCurrentStyle = &_activeStyle;
//    repaint();
//}



