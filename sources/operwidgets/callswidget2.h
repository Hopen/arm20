#ifndef CALLSWIDGET2_H
#define CALLSWIDGET2_H

#include <QWidget>
#include <QPointer>
#include "appcore/lateboundobject.h"
//#include "appcore/callslistmodel.h"
#include "appcore/filteredmodel.h"
#include "appcore/callscontroller.h"
#include "appcontext.h"

using namespace ccappcore;

class QTreeView;
class OperSettings;
//class TabsWindow;

class CallsModel
    : public QAbstractListModel
    , public FilteredModel<ccappcore::Call>
{
    Q_OBJECT;
public:

    enum ColDef
    {
        AbonPhoneColumn,
        CallStateColumn,
        GroupNameColumn,
        CallTypeColumn,
        BeginTimeColumn,
        StateTimeColumn,
        PriorityColumn,
        ExtraInfoColumn,
        LastOperNameColumn,
        CallLengthElapsedColumn,
        CallLengthColumn,
        CallingPhoneColumn/*,
        CallSelectColumn*/
    };


    CallsModel( QObject* parent = 0);
    virtual ~CallsModel(){}

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;
    int columnCount ( const QModelIndex & parent = QModelIndex() ) const;

    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

    bool dropMimeData(const QMimeData *data, Qt::DropAction action,
                      int row, int column, const QModelIndex &parent);
    QMimeData *mimeData(const QModelIndexList &indexes) const;
    QStringList mimeTypes() const;
    Qt::DropActions supportedDropActions() const;


    CallsController::CallIDCollector* connectingCalls(){return (CallsController::CallIDCollector *)(&_connectingCalls);}

    QString name;

protected: //FilteredModel
    virtual void applyFilter(const PredicateGroup<Call>& filter);
    QIcon getCallStatusIcon(ccappcore::Call) const;

public:
    QModelIndex indexOf(ccappcore::Call) const;
    ccappcore::Call callAt(const QModelIndex& index) const;
    ccappcore::Call callAt(const int& index) const;

private:
    QString columnNameById(const qint32& id)const;
    void setVisibleColumns();
signals:
    void connectingCallsListChanged(int callID);
    void connectCalls();
    void joinCalls(const int& conf_id);
    void unjoinConference(int callId);
    //void dropSelectedCall();
    void incomingCall(const int&);
public slots:
    void reset(ccappcore::Call);
//    void addToConferenceList(const ccappcore::Call&);
//    void deleteFromConferenceList(const ccappcore::Call&);
//    void addToAlertingList(const ccappcore::Call&);
    //void deleteFromAlertingList(const ccappcore::Call&);
    void onCallStateChanged(ccappcore::Call);

private slots:
    void columeTimerExpired();
    //void alertingTimerExpired();
protected:
    LateBoundObject < CallsController > _controller;
    LateBoundObject < AppContext      > _appContext;
    QList<ColDef> _columnsList;
    QList<Call> _calls; //currently displayed calls
    CallsController::CallIDCollector _connectingCalls;
    //CallsController::CallIDCollector _alertingCalls;

    bool s_test;

    QTimer _columeTimer;
    //QTimer _alertingTimer;
    //quint8 _alertingCount;

};

class CallsWidget2 : //public QWidget
         public IAlarmHandler
{
    Q_OBJECT
public:
    explicit CallsWidget2(QWidget *parent = 0/*, bool bIsSessionOwnerCall = false*/);
    virtual ~CallsWidget2(){}
    
    QWidget* emptyWidget() { return _emptyWidget; }
    void setEmptyWidget(QWidget* w);
    QAbstractItemView* view()const{return _mainView.data();}

    virtual void reset()=0;

signals:
    
public slots:
    //void on_callChange(QModelIndex);
    //void on_callSelect(QModelIndex);
    //void onDropSelectedCall();
//    void onIncomingCall(const int& index);
//    void onIncomingCall2();
private slots:
    void toggleEmptyWidget();
protected:
    QPointer<  CallsModel  > _mainModel;
    QPointer<  QTreeView   > _mainView;
    QPointer<  QWidget     > _emptyWidget;
    //QPointer < QSplitter   > _topSplitter;
    QPointer < QVBoxLayout > _topLayout;
    QPointer < QVBoxLayout > _viewLayout;

    LateBoundObject<AppContext  > _appContext;
    LateBoundObject<OperSettings> _operSettings;

};

class OperCallsWidget : public CallsWidget2
{
    Q_OBJECT
public:
    explicit OperCallsWidget(QWidget *parent = 0);
    void reset();

    // IAlarmHandler interface
    void connectToTimer();
    int getTabIndex()const;
    QIcon getTabIcon (bool _default = false) const;


private slots:
    void on_showCallForm(ccappcore::Call c);
    void on_btnConnectCallsCliked();
    void on_btnConferenceCallsCliked();
    void updateWidgetControls();
    void on_joinCalls(const int& conf_id);


public slots:
    void onIncomingCall(const int& index);
    void onIncomingCall2();


private:
    //QPointer<TabsWindow> _callForm;
    LateBoundObject<CallsController> _callsController;
    QPointer<QPushButton> _btnConnectCalls;
    QPointer<QPushButton> _btnConferenceCalls;

    CallsController::CallIDCollector _incomingCallsID;
};

class QueueCallsWidget : public CallsWidget2
{
    Q_OBJECT
public:
    explicit QueueCallsWidget(QWidget *parent = 0);

    void reset();

    // IAlarmHandler interface
    void connectToTimer();
    int getTabIndex()const;
    QIcon getTabIcon (bool _default = false) const;

private:
    FilteredModel<Call>::FilterCriteria   _isQueuedCall;
};


////test123
//class TestModel : public QAbstractListModel/*,
//                      public FilteredModel<ccappcore::Contact>*/
//{
//    Q_OBJECT

//public:

//    enum ColDef
//    {
//        GroupColumn = Contact::Name,
//        NameColumn,
//        TaskIdColumn,
//        StatusColumn
//    };


//        TestModel(QObject *parent = 0);


//        int rowCount(const QModelIndex &parent = QModelIndex()) const;
//        QVariant data(const QModelIndex &index, int role) const;
//        QVariant headerData(int section, Qt::Orientation orientation,
//                            int role = Qt::DisplayRole) const;
//        int columnCount ( const QModelIndex & parent = QModelIndex() ) const;

////        bool insertRows(int position, int rows, const QModelIndex &index = QModelIndex());
////        bool removeRows(int position, int rows, const QModelIndex &index = QModelIndex());

//        //void sort ( int column, Qt::SortOrder order = Qt::AscendingOrder );

//        Qt::ItemFlags flags(const QModelIndex &index) const;
//        //bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
//        //void setOwnGroupsChecked();

//private:
//        QString columnNameById(const qint32& id)const;

////signals:
////public slots:
////        void reset();
////        //void sort();

////protected: //FilteredModel
////    virtual void applyFilter(const PredicateGroup<Contact>& filter);

//    private:
//        LateBoundObject < OperListController    > _controller;
////        LateBoundObject < GroupIndicatorManager > _manager;


////        typedef typedef QList</*ccappcore::Group*/int> GroupCollector;
//        QList<QString> _columnsList;
////        GroupCollector _groups      ; // all displayed groups
////        GroupCollector _activeGroups; // user selected groups
////        QMap<int,IndicatorList>        _indicatorList;

//          QList<int> source;
////        friend class ActiveGroupSortPredicate;
//};

//class TestWidget : public QWidget
//{
//    Q_OBJECT
//public:
//    explicit TestWidget(QWidget *parent = 0);

//signals:

//private:
//    QPointer< TestModel > _mainModel;
//    QPointer< QTreeView     > _mainView;
//};


#endif // CALLSWIDGET2_H
