#include "messagetrayicon.h"
#include "appcore/operlistcontroller.h"

MessageTrayIcon::MessageTrayIcon(const SpamMessage& message, QObject *parent)
    : QSystemTrayIcon( QIcon(":/resources/icons/mail_new_16x16.png"), parent), _associatedMessage(message)
{
    setToolTip(SpamMessage::shortInfo(_associatedMessage));
    connect(this, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(activate(QSystemTrayIcon::ActivationReason)));
    connect(this,SIGNAL(messageClicked()), this, SLOT(on_messageClicked()));

    show();
    showMessage("", SpamMessage::shortInfo(_associatedMessage),
                  QSystemTrayIcon::Information );

}

MessageTrayIcon::~MessageTrayIcon()
{

}

void MessageTrayIcon::on_messageClicked()
{
    onMessageActivate();
}

void MessageTrayIcon::activate(QSystemTrayIcon::ActivationReason reason)
{
    if(reason == QSystemTrayIcon::DoubleClick)
        onMessageActivate();
}

void MessageTrayIcon::onMessageActivate()
{
    LateBoundObject<AppContext> appContext;

    if (!_associatedMessage.isRead())
    {
        ContactId personId = _opersController->createOperId(_associatedMessage.operFromId());
        Contact c = _opersController->findById(personId);
        if (!c.isValid())
            return;

        appContext->showChatForm(c);
    }

    appContext->onActivateForegroundWindow();

    //emit activated();
    QTimer::singleShot(500, this, SLOT(deleteLater()));
}

