#include "calltab.h"
#include "ui_calltab.h"
//#ifndef QT_DEBUG
#if defined(QT_DEBUG) && defined (QT_NODLL)
#else
#include <QtWebKit/QtWebKit> //test123
#include <QtWebKit/QWebFrame>
#endif
#include "appcore/useritemdatarole.h"
#include "transferwidget.h"
#include "appcore/filteredmodel.h"
#include "appcore/domainobjectfilter.h"


using namespace ccappcore;

Brouser::Brouser(QWidget *parent): QWidget(parent), _mainView(NULL)
{
    _btnBack = new QToolButton(this);
    _btnBack->setIcon(QIcon(":/resources/icons/new/navigate_left.png"));
    _btnBack->setAutoRaise(true);
    _btnBack->setEnabled(true);
    _btnBack->setToolTip(tr("Назад"));

    _btnForward = new QToolButton(this);
    _btnForward->setIcon(QIcon(":/resources/icons/new/navigate_right.png"));
    _btnForward->setAutoRaise(true);
    _btnForward->setEnabled(true);
    _btnForward->setToolTip(tr("Вперед"));

    _btnStop = new QToolButton(this);
    _btnStop->setIcon(QIcon(":/resources/icons/new/stop.png"));
    _btnStop->setAutoRaise(true);
    _btnStop->setEnabled(false);
    _btnStop->setToolTip(tr("Остановить"));

    _btnRefresh = new QToolButton(this);
    _btnRefresh->setIcon(QIcon(":/resources/icons/new/refresh.png"));
    _btnRefresh->setAutoRaise(true);
    _btnRefresh->setEnabled(true);
    _btnRefresh->setToolTip(tr("Обновить"));

    QVBoxLayout * btnsLayout = new QVBoxLayout();
    btnsLayout->addWidget(_btnBack);
    btnsLayout->addWidget(_btnForward);
    btnsLayout->addWidget(_btnStop);
    btnsLayout->addWidget(_btnRefresh);

    QWidget* spacer = new QWidget();
    spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    btnsLayout->addWidget(spacer);


    //QWebView* webView = new QWebView(this);
    //setWebView(webView);
    _mainView = new QWebView(this);


    connect(_btnBack.data(),SIGNAL(clicked()),
            _mainView,SLOT(back()));
    connect(_btnForward.data(),SIGNAL(clicked()),
            _mainView,SLOT(forward()));
    connect(_btnStop.data(),SIGNAL(clicked()),
            _mainView,SLOT(stop()));
    connect(_btnRefresh.data(),SIGNAL(clicked()),
            _mainView,SLOT(reload()));

    connect(_mainView,SIGNAL(loadStarted()),
            this,SLOT(onWebViewLoadStarted()));
    connect(_mainView,SIGNAL(loadFinished(bool)),
            this,SLOT(onWebViewLoadFinished(bool)));

    QHBoxLayout * mainLayout = new QHBoxLayout();
    mainLayout->addLayout(btnsLayout);
    mainLayout->addWidget(_mainView);

    setLayout(mainLayout);
}

void Brouser::onWebViewLoadStarted()
{
    _btnStop->setEnabled(true);
}

void Brouser::onWebViewLoadFinished(bool ok)
{
    _btnStop->setEnabled(false);
}

CallTab::CallTab(QWidget *parent) :
    QWidget(parent)
    ,_ui(new Ui::CallTab)
    , _collapsedIcon(":/resources/icons/arrow_collapsed_16x16.png")
    , _expandedIcon(":/resources/icons/arrow_expanded_16x16.png")
    , _process(new QProcess(this))
    , _forcedWebPage(tr(""))
    , _externalCommand(tr(""))
{

    _transferContext = new TransferContext();
    connect(_transferContext.data(),SIGNAL(operPhoneSelected (QVariant)),this,SLOT(onContactSelected(QVariant)));
    connect(_transferContext.data(),SIGNAL(groupPhoneSelected(QVariant)),this,SLOT(onContactSelected(QVariant)));
    connect(_transferContext.data(),SIGNAL(ivrScriptSelected (QString)),this,SLOT(onScriptSelected (QString)));
    connect(_transferContext.data(),SIGNAL(phoneLineSelected (QString)),this,SLOT(onPhoneSelected (QString)));
    connect(_process.data(),SIGNAL(error(QProcess::ProcessError)),this,SLOT(onProcessStartError(QProcess::ProcessError)));

    //connect(this,SIGNAL())

    _ui->setupUi(this);

    _ui->phone->setHintText(tr("<Донабор>"));
    _ui->subject->setHintText(tr("Тема не задана"));

    ServiceProvider::instance().registerService(this); //will be automatically removed when tab is deleted

    _ui->subject->addItems(_operSettings->subjectList());
    _ui->subject->setHintText(tr("Тема не задана"));
    _ui->tabWidget->setAutoHideTabBar(true); //hide tab bar when single tab
    _ui->tabWidget->setDeleteWidgetOnClose(true);
    _ui->tabWidget->removeCloseButton(0);

    connect(_ui->headerHtml, SIGNAL(loadStarted()), this, SLOT(htmlViewLoadStarted()));
    connect(_ui->headerHtml, SIGNAL(loadFinished(bool)), this, SLOT(htmlViewLoadFinished()));
    connect(_ui->headerHtml, SIGNAL(urlChanged(QUrl)), this, SLOT(htmlViewLoadFinished()));
    connect(_ui->footerHtml, SIGNAL(loadStarted()), this, SLOT(htmlViewLoadStarted()));
    connect(_ui->footerHtml, SIGNAL(loadFinished(bool)), this, SLOT(htmlViewLoadFinished()));
    connect(_ui->footerHtml, SIGNAL(urlChanged(QUrl)), this, SLOT(htmlViewLoadFinished()));

    connect(_ui->subject,SIGNAL(textCommitting(QString)),this, SLOT(setSubject(QString)));
    connect(_callsController.instance(), SIGNAL(callStateChanged(ccappcore::Call)),
            this, SLOT(updateCallControls()));
    connect(_callsController.instance(), SIGNAL(callInfoChanged(ccappcore::Call)),
            this, SLOT(updateCallInfo()));
    connect(_callsController.instance(), SIGNAL(callInfoChanged(ccappcore::Call)),
            this, SLOT(executeExtraInfo(ccappcore::Call)));
//    connect(_operListController.instance(), SIGNAL(personInfoChanged(ccappcore::Contact)),
//            this, SLOT(updateClientInfo()));
    connect(_callsController.instance(), SIGNAL(callEnded(ccappcore::Call)),
            this, SLOT(onEndCall(ccappcore::Call)));
    connect(&_callHistoryFutureWatcher, SIGNAL(finished()),
            this, SLOT(onCallHistory()));

    connect(_ui->actionStopCall, SIGNAL(triggered()), _ui->bnCallStart, SLOT(click()));
    connect(_ui->actionStartCall, SIGNAL(triggered()), _ui->bnCallStart, SLOT(click()));

    connect(_ui->actionHoldOnOff, SIGNAL(toggled(bool)), _ui->bnHold, SLOT(setChecked(bool)));
    connect(_ui->actionMuteOnOff, SIGNAL(toggled(bool)), _ui->bnMute, SLOT(setChecked(bool)));
    //connect(_ui->actionRecordOnOff, SIGNAL(toggled(bool)), _ui->bnRecord, SLOT(setChecked(bool)));
    connect(_ui->bnHold, SIGNAL(toggled(bool)), _ui->actionHoldOnOff, SLOT(setChecked(bool)));
    connect(_ui->bnMute, SIGNAL(toggled(bool)), _ui->actionMuteOnOff, SLOT(setChecked(bool)));
    //connect(_ui->bnRecord, SIGNAL(toggled(bool)), _ui->actionRecordOnOff, SLOT(setChecked(bool)));

    connect(_appContext.instance(), SIGNAL(callToTransferChanged(ccappcore::Call)),
            this, SLOT(updateCallControls()));

    connect(_ui->phone, SIGNAL(textChanged(const QString &)), this, SLOT(on_phoneLine_textChanged(QString)));
    connect(_ui->phone, SIGNAL(returnPressed()), this, SLOT(playDTMF()));

//    connect(_ui->bnAddContact   , SIGNAL(clicked()), this, SIGNAL(shutUpISeeYou()));
//    connect(_ui->bnAddExtraInfo , SIGNAL(clicked()), this, SIGNAL(shutUpISeeYou()));
//    connect(_ui->bnCallDetails  , SIGNAL(clicked()), this, SIGNAL(shutUpISeeYou()));
//    connect(_ui->bnCallStart    , SIGNAL(clicked()), this, SIGNAL(shutUpISeeYou()));
//    connect(_ui->bnCallStop     , SIGNAL(clicked()), this, SIGNAL(shutUpISeeYou()));
//    connect(_ui->bnClientDetails, SIGNAL(clicked()), this, SIGNAL(shutUpISeeYou()));
//    connect(_ui->bnConnect      , SIGNAL(clicked()), this, SIGNAL(shutUpISeeYou()));
//    connect(_ui->bnHold         , SIGNAL(clicked()), this, SIGNAL(shutUpISeeYou()));
//    connect(_ui->bnMute         , SIGNAL(clicked()), this, SIGNAL(shutUpISeeYou()));
//    connect(_ui->bnTransfer     , SIGNAL(clicked()), this, SIGNAL(shutUpISeeYou()));
//    connect(_ui->bnClientDetails, SIGNAL(clicked()), this, SIGNAL(shutUpISeeYou()));

    QList<QWidget *> widgets = findChildren<QWidget *>();
    foreach (QWidget *w2, widgets)
        //connect(w2, SIGNAL(clicked()), this, SIGNAL(shutUpISeeYou()));
        connect(w2, SIGNAL(clicked()), this, SLOT(onAnyWidgetCliked()));


    //connect(this, SIGNAL(shutUpISeeYou()), _appContext.instance(), SLOT(onStopAlerting(ccappcore::Call)));

    connect((QTimer*)&_stateTimeTimer, SIGNAL(timeout()), this, SLOT(refreshStateTime()) );
    _stateTimeTimer.start(1000);

    //if(!call.contactLink().isValid())
    //    _callsController->reqBindPerson(call);

    _ui->frameClientDetails->setVisible( false );
    _ui->bnClientDetails->setIcon( _collapsedIcon );

    _ui->callDetails->setVisible( false );
    _ui->bnCallDetails->setIcon( _collapsedIcon  );

    _ui->subject->setFocus();   

    //request bind person to get full person name
    if(!_call.isCompleted())
        _callsController->reqBindPerson(_call);

    _ui->bnTransfer->setMenu(_transferContext->menu());
    connect(_ui->bnTransfer, SIGNAL(clicked()), _transferContext.data(), SLOT(on_defaulTransfer()));

    pauseTimoutTimer.setSingleShot(true);
    //TODO: move timeout to settings?
    pauseTimoutTimer.setInterval(300);

    connect(&pauseTimoutTimer, SIGNAL(timeout()),
            this, SLOT(playDTMF()));


    updateCallControls();
    //requestCallInfo();
}

CallTab::~CallTab()
{
    delete _ui;
}

void CallTab::changeEvent(QEvent *e)
{
    switch (e->type()) {
    case QEvent::LanguageChange:
        _ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void CallTab::showEvent(QShowEvent *evt)
{
    QWidget::showEvent(evt);
    updateCallControls();
}

//bool CallTab::event(QEvent *evt)
//{
//    if (evt->type() == QEvent::MouseButtonPress)
//    {
//        emit shutUpISeeYou();
//    }
//    return QWidget::event(evt);

//}

void CallTab::setContact(Contact c)
{
    _contact = c;

    updateClientInfo();
}

void CallTab::setCall(Call c)
{
    _call = c;
    emit titleChanged(c.controller().callShortInfo(c));
    updateCallInfo();
    //_callsController->reqCallInfo(c);
    requestCallInfo();
    _transferContext->setTransferCall(c);
}

void CallTab::refreshStateTime()
{
    const QDateTime& begin = _call.beginDateTime();
    int seconds = begin.secsTo(QDateTime::currentDateTime());
    _ui->stateTime->setTime(QTime().addSecs(seconds));

    qDebug()<<"refreshStateTime: _call.beginDateTime = "<< begin << ", seconds left = " << seconds;
    update();
}

void CallTab::updateCallControls()
{
    updateClientInfo();

//    int titleColor = TCS_ACTIVE;
//    if(_call.isOnHold() || _call.isMuted())
//    {
//        setWindowIcon(QIcon(":/resources/icons/ball_blue_16x16.png"));
//        titleColor = TCS_HOLD;
//    }
//    else if(_call.isCompleted())
//    {
//        setWindowIcon(QIcon(":/resources/icons/ball_gray_16x16.png"));
//        titleColor = TCS_COMPLETE;
//    }
//    else
//        setWindowIcon(QIcon(":/resources/icons/ball_green_16x16.png"));

    emit iconChanged();
//    emit titleCallStatusChange(titleColor);


    _ui->callStatus->setText(_call.callStateString());

    QString subject = _call.extraInfo(Call::CI_Subject);
    if(!subject.isEmpty())
        _ui->subject->setEditText(subject);

    _ui->bnHold->setChecked(_call.isOnHold() );
    _ui->bnHold->setEnabled( _call.isTalking() || _call.isOnHold() );
    _ui->bnHold->setIcon(_ui->actionHoldOnOff->icon());

    _ui->bnMute->setChecked(_call.isMuted() );
    _ui->bnMute->setEnabled( _call.isTalking() || _call.isMuted() );
    _ui->bnMute->setIcon(_ui->actionMuteOnOff->icon());

    //_ui->bnRecord->setChecked(_call.isRecording() );
    //_ui->bnRecord->setEnabled( _call.isTalking() );
    //_ui->bnRecord->setIcon(_ui->actionRecordOnOff->icon());

    _ui->bnTransfer->setChecked(_appContext->callToTransfer() == _call);
    _ui->bnTransfer->setEnabled( !_call.isCompleted() );
    _ui->bnConnect->setEnabled( !_call.isCompleted() );

    _ui->bnCallStart->setEnabled( _call.isCompleted() || _call.isAssigned() || _call.isQueued() );
    _ui->bnCallStop->setEnabled(!_call.isCompleted());

    if(_call.isCompleted())
        _stateTimeTimer.stop();
    else if (!_stateTimeTimer.isActive())
        _stateTimeTimer.start();

//    if (_call.callState() == Call::CS_Completed)
//        emit shutUpISeeYou();

    _ui->phone->setEnabled( !_call.isCompleted() );

    updateCallInfo();
}

void CallTab::on_bnCallStart_clicked()
{
    if( _call.isAssigned() || _call.isQueued() )
        _callsController->answerCall(_call);
    else if(_call.isCompleted())
        _callsController->makeCall(_call);
    else
        qWarning()<<"CallTab::on_bnCallStart_clicked() - unexpected call state "
                <<_call.callStateString();

    //emit shutUpISeeYou();
}

void CallTab::on_bnCallStop_clicked()
{
    _callsController->endCall(_call);
}

void CallTab::on_bnHold_clicked(bool checked)
{
    _callsController->holdCall(_call, checked);
}

void CallTab::on_bnMute_clicked(bool checked)
{
    _callsController->muteCall(_call, checked);
}

//void CallTab::on_bnRecord_clicked(bool checked)
//{
//    _callsController->recordCall(_call, checked);
//}

void CallTab::on_bnConnect_clicked()
{
    on_bnConnect_customContextMenuRequested(QCursor::pos());
}

void CallTab::connectCallAction_triggered()
{
    QAction* a = qobject_cast<QAction*>(sender());
    Call c = a->data().value<Call>();
    _callsController->connectCalls(_call, c);
}


void CallTab::on_bnConnect_customContextMenuRequested(QPoint pos)
{
    _callAdditionalActions.clear();

    FilteredModel<Call>::FilterCriteria _ownerCall = new IsSessionOwnerCall();
    QList<Call> callsToConnect = _callsController->findCallsToConnect(_call,*_ownerCall.data());

    foreach(Call c, callsToConnect)
    {
        QString name = _callsController->callShortInfo(c);
        QString text = tr("Соединить с \"%1\"").arg(name);
        QAction* a = _callAdditionalActions.addAction(QIcon(),text,
            this, SLOT(connectCallAction_triggered()));
        a->setData(QVariant::fromValue(c));
    }
    _callAdditionalActions.popup(pos);
}

void CallTab::setSubject(QString subj)
{
    _callsController->setSubject(_call,subj);
}

void CallTab::on_bnAddExtraInfo_clicked()
{
    QString note = _ui->lineEditCallNote->text();
    if(!note.isEmpty())
        _callsController->addCallInfo(_call, Call::CI_AdditionalTextInfo, note);

    _ui->lineEditCallNote->clear();
}

/*static */
static bool ci_lessThanByTimeAdded(const CallInfo& c1, const CallInfo& c2)
{
    return c1.timeAdded() < c2.timeAdded() ||
            ( c1.timeAdded() == c2.timeAdded() && c1.infoType()<c2.infoType() );
}

void CallTab::updateHeaderFooter()
{
    QString header = _call.extraInfo(Call::CI_CallFormHeader);
    _ui->headerHtml->setText(header);
    _ui->headerHtml->setVisible(!header.isEmpty());

    QString footer = _call.extraInfo(Call::CI_CallFormFooter);
    _ui->footerHtml->setText(footer);
    _ui->footerHtml->setVisible(!footer.isEmpty());
}

class HtmlTable
{
public:
    HtmlTable()
        : newLine(true)
        , html()
        , out(&html)

    {        
    };

    QString toString()
    {

        if(!newLine)
            endl();
        out.flush();
        return "<table width='100%'>" + html + "</table>";
    }

    HtmlTable& td(int data)
    {
        return td(qvariant_cast<QString>(data));
    }

    HtmlTable& td(const QString& data)
    {
        if(newLine)
        {
            out<<"<tr>";
            newLine = false;
        }
        out<<"<td>"<<data<<"</td>";
        return *this;
    }

    HtmlTable& endl()
    {
        out<<"</tr>";
        newLine = true;
        return *this;
    }
private:
    bool newLine;
    QString html;
    QTextStream out;
};

void CallTab::updateCallInfo()
{
    if (!_call.isValid())
        return;

    updateHeaderFooter();

    _ui->callInfo->clear();
    _ui->callDetails->clear();      

    HtmlTable table;

    QString abonNumber = _callsController->callAbonPhone(_call);
    QString region = _call.extraInfo(Call::CI_RegionName);
    if(region.isEmpty())
        region = tr("Не определен","Карточка звонка, регион не определен");
    table.td(tr("Телефон: ")).td(abonNumber)
         .td(tr("Регион: ")).td(region).endl();

     table.td(tr("Начало: ")).td(_call.beginDateTime().toString(Qt::LocalDate))
          .td(tr("CallID: ")).td(_call.callId()).endl();

    if(!_call.callExtraInfo().isEmpty())
        table.td(tr("Комментарий: ")).td(_call.callExtraInfo());

    _ui->callDetails->setText(table.toString());

    QUrl urlShowHistory("ccapp://"+QString(metaObject()->className()).toLower());
    urlShowHistory.setPath(qvariant_cast<QString>(call().callId()) + "/showcallhistory");

    QString callHistoryLinks;
    urlShowHistory.addQueryItem("days","7");
    callHistoryLinks.append(tr("Показать историю звонков за:&nbsp;&nbsp;"));
    callHistoryLinks.append("<a href='"+urlShowHistory.toString()+"'>")
            .append(tr("Неделя")).append("</a>").append("&nbsp;&nbsp;");

    urlShowHistory.removeQueryItem("days");
    urlShowHistory.addQueryItem("days","31");
    callHistoryLinks.append("<a href='"+urlShowHistory.toString()+"'>")
            .append(tr("Месяц")).append("</a>").append("&nbsp;&nbsp;");

    urlShowHistory.removeQueryItem("days");
    urlShowHistory.addQueryItem("days","365");
    callHistoryLinks.append("<a href='"+urlShowHistory.toString()+"'>")
            .append(tr("Год")).append("</a>").append("&nbsp;&nbsp;");
    callHistoryLinks.append("<br/>");

    QList<Call::CallInfoType> infoTypeToDisplay;
    infoTypeToDisplay
            <<Call::CI_AdditionalTextInfo
            <<Call::CI_IVRMenuItemIn
            <<Call::CI_CallAssigned
            <<Call::CI_CallQueued
            <<Call::CI_CallConnected
            <<Call::CI_CallRecordOn
            <<Call::CI_CallEnded;
    QList<CallInfo> infoToDisplay;
    foreach(const CallInfo& ci, _call.extraInfo().values())
    {
        if( infoTypeToDisplay.contains(ci.infoType()) )
        infoToDisplay.append(ci);
    }

    qSort( infoToDisplay.begin(),infoToDisplay.end(),
           ci_lessThanByTimeAdded );

    QString callInfoHtml;
    foreach(const CallInfo& ci, infoToDisplay)
    {
        QString dateAdded =
                ( ci.timeAdded().date() == QDate::currentDate() )
                ? ci.timeAdded().time().toString()
                : ci.timeAdded().toString(Qt::SystemLocaleShortDate);
        QString line;
        line.append("[").append(dateAdded).append("] ");
        line.append(_callsController->callInfoString(ci));
        callInfoHtml+=line+"<br/>";
    }
    _ui->callInfo->setText(callHistoryLinks+"<br/>"+callInfoHtml);

    qDebug() << tr("CallTab::updateCallInfo()");
}

void CallTab::updateClientInfo()
{
    Contact p = contact();
    if(!p.isValid())
    {
        _ui->clientName->showHint();
        return;
    }
    else
        _ui->clientName->setText(p.name());


    HtmlTable table;

    table.td(tr("Имя: ")).td(p.name().isEmpty()?tr("Нет"):p.name());
    if(!p.name().isEmpty())
        table.td(tr("Полное имя: ")).td(p.name()).endl();

    Contact::ExtraInfo extraInfo = p.contactInfo();
    for(Contact::ExtraInfo::ConstIterator cit = extraInfo.begin();
            cit!=extraInfo.end(); ++cit )
    {
        int key = cit.key();
        QVariant value = cit.value();
        if (value.type() == QVariant::Int)
        {
            table.td(key).td(value.toInt());
            table.endl();
        }
        else if (value.type() == QVariant::String)
        {
            table.td(key).td(value.toString());
            table.endl();
        }

    }



    _ui->clientInfo->setText(table.toString());

    qDebug() << tr("CallTab::updateClientInfo()");
}

void CallTab::on_lineEditCallNote_returnPressed()
{
    on_bnAddExtraInfo_clicked();
}

void CallTab::requestCallInfo()
{
    _callsController->reqCallInfo(_call);
//    if(_call.contactLink().isValid())
//        _operListController->reqPersonInfo(_call.person());
}

void CallTab::onEndCall(ccappcore::Call call)
{
    if (!call.isValid())
        return;
    if( call.isCompleted() &&
        call == _call &&
        _operSettings->closeCallFormOnEndCall() )
    {
        //this->close();
        //if(parentWidget())
        //    parentWidget()->close();
        qDebug() << tr("emit readyToQuit");
        emit readyToQuit();
    }
}

void CallTab::htmlViewLoadStarted()
{
}

void CallTab::htmlViewLoadFinished()
{
#if defined(QT_DEBUG) && defined (QT_NODLL)
#else
    QWebView* source = qobject_cast<QWebView*>(sender()); //test123
    if(!source)
        return;

    if(!source->page() || !source->page()->mainFrame() )
        return;

    QSize viewSize = source->page()->mainFrame()->contentsSize();
    if(viewSize.isValid())
        source->setMaximumHeight(viewSize.height());
#endif
}

void CallTab::on_bnCallDetails_clicked()
{
    bool hide = _ui->callDetails->isVisible();
    _ui->callDetails->setVisible(!hide);
    _ui->bnCallDetails->setIcon( hide ? _collapsedIcon : _expandedIcon );
}

void CallTab::on_bnClientDetails_clicked()
{
    bool hide = _ui->frameClientDetails->isVisible();
    _ui->frameClientDetails->setVisible(!hide);
    _ui->bnClientDetails->setIcon( hide ? _collapsedIcon : _expandedIcon );
}

bool CallTab::openUrl(const QUrl& url)
{
    if (url.path().isEmpty())
        return false;
    QString scheme = url.scheme().toLower();
    if( scheme == "http" )
    {
        openWebPage(url);
        return true;
    }

    QString path = "/" + qvariant_cast<QString>(call().callId());
    if( !url.path().startsWith( path ) )
        return false;

    if( url.path().contains("showcallhistory") )
    {
        int days = url.queryItemValue("days").toInt();
        openCallHistory(days);
        return true;
    }

    return false;
}

void CallTab::openWebPage(const QUrl& url)
{
#if defined(QT_DEBUG) && defined (QT_NODLL)
#else
    Brouser * brouser = NULL;

    //QWebView* webView = NULL; //test123
    //webView = _ui->tabWidget->findChild<QWebView*>(url.toString());
    brouser = _ui->tabWidget->findChild<Brouser*>(url.toString());
    if(!/*webView*/brouser)
    {
        Brouser *brouser = new Brouser(this);

        brouser->setObjectName(url.toString());
        connect(brouser->getWebView(),SIGNAL(iconChanged()),
                this,SLOT(webViewIconChanged()));
        connect(brouser->getWebView(),SIGNAL(titleChanged(QString)),
                this,SLOT(webViewTitleChanged(QString)));

        _ui->tabWidget->addTab(/*webView*/brouser, url.toString());
        /*webView*/brouser->show();
        brouser->getWebView()->setUrl(url);
    }
    int index = _ui->tabWidget->indexOf(/*webView*/brouser);
    _ui->tabWidget->setCurrentIndex(index);
#endif
}

void CallTab::openCallHistory(int days)
{
    QDateTime from = QDateTime::currentDateTime().addDays(-days);
    QDateTime to = QDateTime::currentDateTime();
    _callHistoryFutureWatcher.setFuture(
            _callsController->reqCallHistory(_call,from,to)
            );
}

void CallTab::webViewTitleChanged(const QString&)
{
#if defined(QT_DEBUG) && defined (QT_NODLL)
#else
    QWebView* view = qobject_cast<QWebView*>(sender()); //test123
    if(!view)
        return;
    Brouser *brouser = qobject_cast<Brouser*>(view->parent());
    if (!brouser)
        return;

    int index = _ui->tabWidget->indexOf(brouser);
    _ui->tabWidget->setTabText(index, view->title());
#endif
}

void CallTab::webViewIconChanged()
{
#if defined(QT_DEBUG) && defined (QT_NODLL)
#else
    //QWebView * view = new QWebView(); //test123
    QWebView* view = qobject_cast<QWebView*>(sender());
    if(!view)
        return;
    Brouser *brouser = qobject_cast<Brouser*>(view->parent());
    if (!brouser)
        return;
    int index = _ui->tabWidget->indexOf(brouser);
    _ui->tabWidget->setTabIcon(index, view->icon());
#endif
}

void CallTab::on_footerHtml_linkActivated(QString link)
{
    openUrl(QUrl(link));
}

void CallTab::on_headerHtml_linkActivated(QString link)
{
    openUrl(QUrl(link));
}

void CallTab::onCallHistory()
{    
    QTableView* grid = new QTableView(this);
    CallsListModel* callsListModel = new CallsListModel(grid);
    QList<CallsListModel::ColDef> columns;
    columns<<CallsListModel::BeginTimeColumn
           <<CallsListModel::CallTypeColumn
           <<CallsListModel::CallLengthColumn
           <<CallsListModel::AbonPhoneColumn;
    callsListModel->setVisibleColumns(columns);
    callsListModel->setCalls(_callHistoryFutureWatcher.future().results());

    grid->setModel(callsListModel);
    grid->resizeColumnsToContents();

    connect(grid,SIGNAL(activated(QModelIndex)),
            this, SLOT(onCallHistoryItemClicked(QModelIndex)));

    int index = _ui->tabWidget->addTab(grid,QIcon(),tr("История звонков"));
    _ui->tabWidget->setCurrentIndex(index);
}

void CallTab::onCallHistoryItemClicked(QModelIndex index)
{
    QVariant varCall = index.data(AssociatedObjectRole);
    if(!varCall.isValid())
        return;
    Call call = varCall.value<ccappcore::Call>();
    if(!call.isValid())
        return;
    _appContext->showCallForm(call);
}

//void CallTab::on_bnTransfer_toggled(bool checked)
//{
//    if(checked)
//        _appContext->setCallToTransfer(_call);
//    else
//        _appContext->clearCallToTransfer();
//}

void CallTab::onContactSelected(QVariant varContact)
{
    if(!varContact.isValid())
        return;
    Contact c = varContact.value<ccappcore::Contact>();
    if(!c.isValid())
        return;

    Call callToTransfer = _appContext->callToTransfer();
    if(!callToTransfer.isValid())
        return;

    if( c.contactId().type() == CT_Group )
    {
        _callsController->transferToGroup(callToTransfer, c);
    }
    else
    {
        _callsController->transferToOperator(callToTransfer, c);
    }
}

void CallTab::onScriptSelected(QString scriptName)
{
    Call callToTransfer = _appContext->callToTransfer();
    if(callToTransfer.isValid())
        _callsController->transferToIVR(callToTransfer, scriptName);
}

void CallTab::onPhoneSelected(QString phone)
{
    Call callToTransfer = _appContext->callToTransfer();
    if(callToTransfer.isValid())
        _callsController->transferToPhone(callToTransfer, phone);
}


void CallTab::executeExtraInfo(ccappcore::Call incoming_call)
{
//    disconnect(_callsController.instance(), SIGNAL(callInfoChanged(ccappcore::Call)),
//            this, SLOT(executeExtraInfo()));

    if (_call != incoming_call)
        return;

//    QString testUrl = "http://vz.ru";
//    QUrl testURL2;
//    testURL2.setPath(testUrl);
//    openWebPage((testUrl));

    QString ciForced = _call.extraInfo(Call::CI_ForcedWebPage);
    QString ciExtern = _call.extraInfo(Call::CI_External);

    if (!ciForced.isEmpty() && _forcedWebPage != ciForced)
    {
            _forcedWebPage = ciForced;
            openUrl(QUrl(_forcedWebPage));
    }

    if (!ciExtern.isEmpty() && _externalCommand != ciExtern)
    {
        _externalCommand = ciExtern;
        startProcess(_externalCommand);
    }
}

void CallTab::startProcess(const QString &path)
{
//    QProcess* process= new QProcess(this);
//    process->start("cmd.exe /c \"start http://ya.ru\"");

    qDebug() << tr("Executing extraInfo(CI_External):\"%1\"").arg(path);
    if (path.isEmpty())
        return;

    _process->start(path);
}

void CallTab::onProcessStartError(QProcess::ProcessError error)
{
    qDebug() << tr("Error executing extraInfo:\"%1\"").arg(error);
}

void CallTab::onAnyWidgetCliked()
{
    QList<QWidget *> widgets = findChildren<QWidget *>();
    foreach (QWidget *w2, widgets)
        disconnect(w2, SIGNAL(clicked()), this, SLOT(onAnyWidgetCliked()));

    _appContext->onStopAlerting(_call);
}

//void CallTab::close()
//{
//    QWidget::close();
//}
void CallTab::on_phoneLine_textChanged(QString text)
{
    Q_UNUSED(text)

//    startTimeout();
    pauseTimoutTimer.start();
}

void CallTab::playDTMF()
{
    if (_ui->phone->isHintVisible())
        return;

    QString text = _ui->phone->text();

    if(!_call.isValid())
        return;

    if (!text.isEmpty() && _lastDTMF!=text)
    {
        QString newDTMF = text;
        if (!_lastDTMF.isEmpty() && 0 == newDTMF.indexOf(_lastDTMF))
        {
            newDTMF = newDTMF.right(newDTMF.length() - _lastDTMF.length());
        }

        if( _call.callState() == Call::CS_Connected)
            _callsController->playDTMF(_call, newDTMF);

    }

    _lastDTMF = text;

}

