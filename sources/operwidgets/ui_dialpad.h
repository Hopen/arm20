/********************************************************************************
** Form generated from reading UI file 'dialpad.ui'
**
** Created: Tue 26. Mar 13:54:37 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALPAD_H
#define UI_DIALPAD_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DialPad
{
public:
    QGridLayout *gridLayout;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QPushButton *pushButton_6;
    QPushButton *pushButton_5;
    QPushButton *pushButton_7;
    QPushButton *pushButton_9;
    QPushButton *pushButton_8;
    QPushButton *pushButton_11;
    QPushButton *pushButton_12;
    QPushButton *pushButton_10;

    void setupUi(QWidget *DialPad)
    {
        if (DialPad->objectName().isEmpty())
            DialPad->setObjectName(QString::fromUtf8("DialPad"));
        DialPad->resize(100, 100);
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(DialPad->sizePolicy().hasHeightForWidth());
        DialPad->setSizePolicy(sizePolicy);
        DialPad->setMinimumSize(QSize(100, 100));
        DialPad->setMaximumSize(QSize(250, 250));
        DialPad->setBaseSize(QSize(100, 100));
        DialPad->setStyleSheet(QString::fromUtf8("QWidget \n"
"{\n"
"	background-color: qlineargradient(spread:pad, x1:0.503, y1:0.374091, x2:0.503, y2:0.0738636, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(93, 93, 93, 255));\n"
"\n"
"}\n"
"\n"
"QPushButton\n"
"{\n"
"	color:white;\n"
"	font: bold 10pt;\n"
"}\n"
"\n"
"QPushButton:pressed, QToolButton:pressed\n"
"{\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color:black;\n"
"}\n"
"\n"
"\n"
"QToolButton\n"
"{\n"
"	color:white;\n"
"	font: 8pt;\n"
"}\n"
"\n"
"QToolButton:checked\n"
"{\n"
"	\n"
"	background-color: qlineargradient(spread:pad, x1:0.503318, y1:0.977, x2:0.519682, y2:0, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(192, 192, 192, 255));\n"
"	border:2px solid white;\n"
"  	margin: 2px;\n"
"}\n"
""));
        gridLayout = new QGridLayout(DialPad);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetMinimumSize);
        pushButton = new QPushButton(DialPad);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(pushButton->sizePolicy().hasHeightForWidth());
        pushButton->setSizePolicy(sizePolicy1);
        pushButton->setMinimumSize(QSize(16, 16));
        pushButton->setFlat(false);

        gridLayout->addWidget(pushButton, 0, 0, 1, 1);

        pushButton_2 = new QPushButton(DialPad);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        sizePolicy1.setHeightForWidth(pushButton_2->sizePolicy().hasHeightForWidth());
        pushButton_2->setSizePolicy(sizePolicy1);
        pushButton_2->setMinimumSize(QSize(16, 16));

        gridLayout->addWidget(pushButton_2, 0, 1, 1, 1);

        pushButton_3 = new QPushButton(DialPad);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        sizePolicy1.setHeightForWidth(pushButton_3->sizePolicy().hasHeightForWidth());
        pushButton_3->setSizePolicy(sizePolicy1);
        pushButton_3->setMinimumSize(QSize(16, 16));

        gridLayout->addWidget(pushButton_3, 0, 2, 1, 1);

        pushButton_4 = new QPushButton(DialPad);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        sizePolicy1.setHeightForWidth(pushButton_4->sizePolicy().hasHeightForWidth());
        pushButton_4->setSizePolicy(sizePolicy1);
        pushButton_4->setMinimumSize(QSize(16, 16));

        gridLayout->addWidget(pushButton_4, 1, 0, 1, 1);

        pushButton_6 = new QPushButton(DialPad);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));
        sizePolicy1.setHeightForWidth(pushButton_6->sizePolicy().hasHeightForWidth());
        pushButton_6->setSizePolicy(sizePolicy1);
        pushButton_6->setMinimumSize(QSize(16, 16));

        gridLayout->addWidget(pushButton_6, 1, 1, 1, 1);

        pushButton_5 = new QPushButton(DialPad);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        sizePolicy1.setHeightForWidth(pushButton_5->sizePolicy().hasHeightForWidth());
        pushButton_5->setSizePolicy(sizePolicy1);
        pushButton_5->setMinimumSize(QSize(16, 16));

        gridLayout->addWidget(pushButton_5, 1, 2, 1, 1);

        pushButton_7 = new QPushButton(DialPad);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));
        sizePolicy1.setHeightForWidth(pushButton_7->sizePolicy().hasHeightForWidth());
        pushButton_7->setSizePolicy(sizePolicy1);
        pushButton_7->setMinimumSize(QSize(16, 16));

        gridLayout->addWidget(pushButton_7, 2, 0, 1, 1);

        pushButton_9 = new QPushButton(DialPad);
        pushButton_9->setObjectName(QString::fromUtf8("pushButton_9"));
        sizePolicy1.setHeightForWidth(pushButton_9->sizePolicy().hasHeightForWidth());
        pushButton_9->setSizePolicy(sizePolicy1);
        pushButton_9->setMinimumSize(QSize(16, 16));

        gridLayout->addWidget(pushButton_9, 2, 1, 1, 1);

        pushButton_8 = new QPushButton(DialPad);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));
        sizePolicy1.setHeightForWidth(pushButton_8->sizePolicy().hasHeightForWidth());
        pushButton_8->setSizePolicy(sizePolicy1);
        pushButton_8->setMinimumSize(QSize(16, 16));

        gridLayout->addWidget(pushButton_8, 2, 2, 1, 1);

        pushButton_11 = new QPushButton(DialPad);
        pushButton_11->setObjectName(QString::fromUtf8("pushButton_11"));
        sizePolicy1.setHeightForWidth(pushButton_11->sizePolicy().hasHeightForWidth());
        pushButton_11->setSizePolicy(sizePolicy1);
        pushButton_11->setMinimumSize(QSize(16, 16));

        gridLayout->addWidget(pushButton_11, 3, 2, 1, 1);

        pushButton_12 = new QPushButton(DialPad);
        pushButton_12->setObjectName(QString::fromUtf8("pushButton_12"));
        sizePolicy1.setHeightForWidth(pushButton_12->sizePolicy().hasHeightForWidth());
        pushButton_12->setSizePolicy(sizePolicy1);
        pushButton_12->setMinimumSize(QSize(16, 16));

        gridLayout->addWidget(pushButton_12, 3, 1, 1, 1);

        pushButton_10 = new QPushButton(DialPad);
        pushButton_10->setObjectName(QString::fromUtf8("pushButton_10"));
        sizePolicy1.setHeightForWidth(pushButton_10->sizePolicy().hasHeightForWidth());
        pushButton_10->setSizePolicy(sizePolicy1);
        pushButton_10->setMinimumSize(QSize(16, 16));

        gridLayout->addWidget(pushButton_10, 3, 0, 1, 1);


        retranslateUi(DialPad);

        QMetaObject::connectSlotsByName(DialPad);
    } // setupUi

    void retranslateUi(QWidget *DialPad)
    {
        DialPad->setWindowTitle(QApplication::translate("DialPad", "Form", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("DialPad", "1", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("DialPad", "2", 0, QApplication::UnicodeUTF8));
        pushButton_3->setText(QApplication::translate("DialPad", "3", 0, QApplication::UnicodeUTF8));
        pushButton_4->setText(QApplication::translate("DialPad", "4", 0, QApplication::UnicodeUTF8));
        pushButton_6->setText(QApplication::translate("DialPad", "5", 0, QApplication::UnicodeUTF8));
        pushButton_5->setText(QApplication::translate("DialPad", "6", 0, QApplication::UnicodeUTF8));
        pushButton_7->setText(QApplication::translate("DialPad", "7", 0, QApplication::UnicodeUTF8));
        pushButton_9->setText(QApplication::translate("DialPad", "8", 0, QApplication::UnicodeUTF8));
        pushButton_8->setText(QApplication::translate("DialPad", "9", 0, QApplication::UnicodeUTF8));
        pushButton_11->setText(QApplication::translate("DialPad", "#", 0, QApplication::UnicodeUTF8));
        pushButton_12->setText(QApplication::translate("DialPad", "0", 0, QApplication::UnicodeUTF8));
        pushButton_10->setText(QApplication::translate("DialPad", "*", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class DialPad: public Ui_DialPad {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALPAD_H
