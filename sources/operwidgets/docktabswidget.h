#ifndef DOCKTABSWIDGET_H
#define DOCKTABSWIDGET_H
#include "docktabs.h"
#include "opersettings.h"
#include "appcore/spamlistcontroller.h"
#include "appcore/JabberController.h"

//class DockTabsWidget;
class DockTabsController : public QObject
{
    Q_OBJECT
public:
    explicit DockTabsController(QObject *parent = 0);

signals:
    void addDockTab   (DockTab* tab, DockTab* after, const int& tabQuantity);
    void raiseDockTab (DockTab* tab);
    void tabsClear    (            );
public slots:
    void on_showCallForm(ccappcore::Call c/*, bool alerting = false*/);
    void callStarted    (ccappcore::Call c);
    void chatMessageIn  (SpamMessage message);
    void on_showChatForm(ccappcore::Contact c);
    void on_childTabClosed();
    void activateIcon(const SpamMessage& message);
    void on_toggleMinMaxWindow(bool);

    void on_raiseOrAdd(DockTab* tab);
    void on_raise(bool);

public:
    typedef QList<DockTab*> TabsCollector;
    DockTab* findDockByCall   (ccappcore::Call   );
    DockTab* findDockByContact(ccappcore::Contact);
private:
    //TabsCollector _tabs;

    LateBoundObject < AppContext         > _appContext;
    LateBoundObject < CallsController    > _callsController;
    LateBoundObject < OperSettings       > _operSettings;
    LateBoundObject < SpamListController > _spamController;
    LateBoundObject < OperListController > _opersController;
    LateBoundObject < JabberController   > _jabberController;

    TabsCollector _tabs;
    bool          _toggleMinMaxWindow;

    //friend class DockTabsWidget;

};

//class SupportWindow: public QMainWindow
//{
//    Q_OBJECT
//public:
//    explicit SupportWindow(QWidget *parent = 0, Qt::WindowFlags flags = 0):
//        QMainWindow(parent,flags)
//    {

//    }

//protected:
//    virtual bool event(QEvent *);
//};


class DockTabsWidget : public QDockWidgetEx
{
    Q_OBJECT
public:
    //explicit DockTabsWidget(QWidget *parent = 0);
    explicit DockTabsWidget(const QString &title, QWidget *parent = 0, Qt::WindowFlags flags = 0);

    explicit DockTabsWidget(QWidget *parent = 0, Qt::WindowFlags flags = 0);

    ~DockTabsWidget();


signals:

public slots:
    void addDockTab(DockTab* tab, DockTab* after, const int& tabQuantity);
    void raiseDockTab(DockTab* tab);
    //void deleteDockTab();
    //void on_childTabAdded();
    void on_tabsClear();

private:
    QMainWindow*  _window;
    QPointer<DockTabsController> _tabsController;
};

#endif // DOCKTABSWIDGET_H
