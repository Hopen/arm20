#include <QKeyEvent>
#include <QApplication>
#include <QStyle>

#include "lineeditwithhint.h"

LineEditWithHint::LineEditWithHint(QWidget *parent)
    : QLineEdit(parent)
    , _hintMode(true)
{
    connect(this, SIGNAL(textChanged(QString)), this, SLOT(on_textChanged(QString)));
}

LineEditWithHint::~LineEditWithHint()
{
}

void LineEditWithHint::showHint()
{
    _hintMode = true;
    setText(_hintText);
    clearFocus();
}

void LineEditWithHint::setHintText(const QString& s)
{
    _hintText = s;
    showHint();
}

bool LineEditWithHint::isHintVisible() const
{
    return _hintMode;
}

void LineEditWithHint::focusInEvent(QFocusEvent *event)
{
    QLineEdit::focusInEvent(event);
    if(_hintMode && !isReadOnly())
    {
        _hintMode = false;
        setText("");
    }
}

void LineEditWithHint::focusOutEvent(QFocusEvent *event)
{
    QLineEdit::focusOutEvent(event);
    if(text().isEmpty())
        showHint();
}

void LineEditWithHint::keyPressEvent(QKeyEvent *event)
{
    if(event && event->key() == Qt::Key_Escape)
    {
        showHint();
    }

    QLineEdit::keyPressEvent(event);

    if(event && event->key() == Qt::Key_Escape)
        emit cancelPressed();
}

void LineEditWithHint::on_textChanged(const QString& text)
{
    if(_hintText.isNull())
    {
        _hintText = text;
        _hintMode = true;
    }

    if(!hasFocus() && !text.isEmpty() && text!=_hintText && !_hintText.isNull())
        _hintMode = false;

    style()->unpolish(this);
    ensurePolished();
}



