#ifndef CONTACTSITEMDELEGATE_H
#define CONTACTSITEMDELEGATE_H

#include "operwidgets_global.h"
#include <QStyledItemDelegate>
#include "appcore/contact.h"
#include "appcore/call.h"

class StateTimeDelegate : public QStyledItemDelegate
{
Q_OBJECT
public:

    enum StateTimeFormat
    {
        FormatSeconds, //ex: 03:07, 01:03:07
        FormatMinutes, //ex: 3 мин., 1 ч. 3 мин.
    };

    explicit StateTimeDelegate(StateTimeFormat format = FormatSeconds, QObject *parent = 0);

    // painting
    void paint(QPainter *painter,
               const QStyleOptionViewItem &option, const QModelIndex &index) const;

    QSize sizeHint(const QStyleOptionViewItem &option,
                   const QModelIndex &index) const;

private:
    StateTimeFormat _stateTimeFormat;
};

#endif // CONTACTSITEMDELEGATE_H
