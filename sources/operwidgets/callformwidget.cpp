#include "CallFormWidget.h"
#include "calltab.h"
#include "opersettings.h"

//const int closeButtonRight = 5;

//QSize BlueTitleBar::minimumSizeHint() const
//{
//    QDockWidget *dw = qobject_cast<QDockWidget*>(parentWidget());
//    Q_ASSERT(dw != 0);
//    QSize result(leftPm.width() + rightPm.width(), centerPm.height());
//    if (dw->features() & QDockWidget::DockWidgetVerticalTitleBar)
//        result.transpose();
//    return result;
//}

//BlueTitleBar::BlueTitleBar(QWidget *parent)
//    : QWidget(parent)
//{
//    leftPm = QPixmap(":/resources/icons/test/titlebarLeft.png");
//    centerPm = QPixmap(":/resources/icons/test/titlebarCenter.png");
//    rightPm = QPixmap(":/resources/icons/test/titlebarRight.png");
//}

//void BlueTitleBar::paintEvent(QPaintEvent*)
//{
//    QPainter painter(this);
//    QRect rect = this->rect();

//    QDockWidget *dw = qobject_cast<QDockWidget*>(parentWidget());
//    Q_ASSERT(dw != 0);

//    if (dw->features() & QDockWidget::DockWidgetVerticalTitleBar) {
//        QSize s = rect.size();
//        s.transpose();
//        rect.setSize(s);

//        painter.translate(rect.left(), rect.top() + rect.width());
//        painter.rotate(-90);
//        painter.translate(-rect.left(), -rect.top());
//    }

//    painter.drawPixmap(rect.topLeft(), leftPm);
//    painter.drawPixmap(rect.topRight() - QPoint(rightPm.width() - 1, 0), rightPm);
//    QBrush brush(centerPm);
//    painter.fillRect(rect.left() + leftPm.width(), rect.top(),
//                        rect.width() - leftPm.width() - rightPm.width(),
//                        centerPm.height(), centerPm);
//}

//void BlueTitleBar::mousePressEvent(QMouseEvent *event)
//{
//    QPoint pos = event->pos();

//    QRect rect = this->rect();

//    QDockWidget *dw = qobject_cast<QDockWidget*>(parentWidget());
//    Q_ASSERT(dw != 0);

//    if (dw->features() & QDockWidget::DockWidgetVerticalTitleBar) {
//        QPoint p = pos;
//        pos.setX(rect.left() + rect.bottom() - p.y());
//        pos.setY(rect.top() + p.x() - rect.left());

//        QSize s = rect.size();
//        s.transpose();
//        rect.setSize(s);
//    }

//    const int buttonRight = 7;
//    const int buttonWidth = 20;
//    int right = rect.right() - pos.x();
//    int button = (right - buttonRight)/buttonWidth;
//    switch (button) {
//        case 0:
//            event->accept();
//            dw->close();
//            break;
//        case 1:
//            event->accept();
//            dw->setFloating(!dw->isFloating());
//            break;
//        case 2: {
//            event->accept();
//            QDockWidget::DockWidgetFeatures features = dw->features();
//            if (features & QDockWidget::DockWidgetVerticalTitleBar)
//                features &= ~QDockWidget::DockWidgetVerticalTitleBar;
//            else
//                features |= QDockWidget::DockWidgetVerticalTitleBar;
//            dw->setFeatures(features);
//            break;
//        }
//        default:
//            event->ignore();
//            break;
//    }
//}

//void BlueTitleBar::updateMask()
//{
//    QDockWidget *dw = qobject_cast<QDockWidget*>(parent());
//    Q_ASSERT(dw != 0);

//    QRect rect = dw->rect();
//    QPixmap bitmap(dw->size());

//    {
//        QPainter painter(&bitmap);

//        ///initialize to transparent
//        painter.fillRect(rect, Qt::color0);

//        QRect contents = rect;
//        contents.setTopLeft(geometry().bottomLeft());
//        contents.setRight(geometry().right());
//        contents.setBottom(contents.bottom()-y());
//        painter.fillRect(contents, Qt::color1);



//        //let's pait the titlebar

//        QRect titleRect = this->geometry();

//        if (dw->features() & QDockWidget::DockWidgetVerticalTitleBar) {
//            QSize s = rect.size();
//            s.transpose();
//            rect.setSize(s);

//            QSize s2 = size();
//            s2.transpose();
//            titleRect.setSize(s2);

//            painter.translate(rect.left(), rect.top() + rect.width());
//            painter.rotate(-90);
//            painter.translate(-rect.left(), -rect.top());
//        }

//        contents.setTopLeft(titleRect.bottomLeft());
//        contents.setRight(titleRect.right());
//        contents.setBottom(rect.bottom()-y());

//        QRect rect = titleRect;


//        painter.drawPixmap(rect.topLeft(), leftPm.mask());
//        painter.fillRect(rect.left() + leftPm.width(), rect.top(),
//            rect.width() - leftPm.width() - rightPm.width(),
//            centerPm.height(), Qt::color1);
//        painter.drawPixmap(rect.topRight() - QPoint(rightPm.width() - 1, 0), rightPm.mask());

//        painter.fillRect(contents, Qt::color1);
//    }

//    dw->setMask(bitmap);
//}




/****************************************************************************************************
 ****************************************************************************************************
 ***************************************************************************************************/

CallFormWidget::CallFormWidget(ccappcore::Call c, const QString &title, /*bool alerting,*/ QWidget *parent, Qt::WindowFlags flags)
    :DockTab(title,/*parent*/0,flags),_call(c)
{
    //CallTab* callWidget = new CallTab( this );
    _callTab = QSharedPointer<CallTab>(new CallTab());
    setWidget(_callTab.data());
    _callTab->setCall(_call);

    setAttribute(Qt::WA_QuitOnClose, false);
//    //setWindowIcon(QIcon(":/resources/icons/ball_blue_16x16.png"));
//    DocklTitleBar* titleBar = new DocklTitleBar(title,this);
//    //BlueTitleBar* titleBar = new BlueTitleBar(this);
//    //titleBar->setActiveStyle(TCS_COMPLETE);
//    setTitleBarWidget(titleBar);
//    if (alerting)
//    {
//        connect(_callTab.data(),SIGNAL(shutUpISeeYou()),titleBar,SLOT(stopAlerting()));
//        titleBar->startAlerting();
//    }


//    connect(callWidget, SIGNAL(destroyed()), this, SLOT(onTabDestroyed()));
    //connect(callWidget, SIGNAL(iconChanged()), this, SLOT(tabIconChanged()));
    connect(_callTab.data(), SIGNAL(titleChanged(QString)), this, SLOT(tabTitleChanged(QString)));
    //connect(_callTab.data(),SIGNAL(titleCallStatusChange(int)),this,SLOT(tabTitleColorChanged(int)));
    connect(_callTab.data(),SIGNAL(readyToQuit()),this,SLOT(close()));

    connect(_globalAddressController.instance(),
            SIGNAL(onSearchCompleted(ccappcore::Contact, QString)),
            this,
            SLOT(onExtraInfoLoaded(ccappcore::Contact, QString)));

    _globalAddressController->search( _callsController->callAbonPhone(c)/*c.aNumber()*/, false);


}

CallFormWidget::~CallFormWidget()
{
    int test = 0;
    qDebug () << tr("CallFormWidget::~CallFormWidget()");
}

QString CallFormWidget::title()const
{
    if (QWidget* titleBar = this->titleBarWidget())
        return titleBar->windowTitle();
    return QString();
}

void CallFormWidget::tabTitleChanged(QString title)
{
    this->titleBarWidget()->setWindowTitle(title);
}

//void CallFormWidget::tabTitleColorChanged(int status)
//{
////    if (DocklTitleBar *titleBar = qobject_cast<DocklTitleBar*>(titleBarWidget()))
////        titleBar->setActiveStyle(status);
//}

//void CallFormWidget::tabIconChanged()
//{
////    QWidget* tab = qobject_cast<QWidget*>(sender());
////    QIcon icon  = tab->windowIcon();
////    setWindowIcon(icon);
//}

void CallFormWidget::resizeEvent(QResizeEvent *e)
{
//    if (BlueTitleBar *btb = qobject_cast<BlueTitleBar*>(titleBarWidget()))
//        btb->updateMask();

//    if (DocklTitleBar *titleBar = qobject_cast<DocklTitleBar*>(titleBarWidget()))
//        titleBar->updateMask();

    QDockWidget::resizeEvent(e);
}

//bool CallFormWidget::event(QEvent *evt)
//{
//    QEvent::Type type = evt->type();
////    if ((type != QEvent::Paint) &&
////        (type != QEvent::Enter) &&
////            (type != QEvent::Leave) &&
////            (type != QEvent::MouseMove) &&
////            (type != QEvent::MouseButtonPress) &&
////            (type != QEvent::MouseButtonRelease) &&
////            (type != QEvent::WindowActivate) &&
////            (type != QEvent::ActivationChange) &&
////            (type != QEvent::UpdateRequest) &&
////            (type != QEvent::WindowDeactivate)
////            )
////    {
////        int test = 0;
////    }

//    if (type == QEvent::ChildAdded)
//    {
//        emit childAdded();
//    }

//    DockTab::event(evt);
//}

void CallFormWidget::close()
{
    //bool quitOnClose = this->testAttribute(Qt::WA_QuitOnClose);
    this->QDockWidget::close();
}

void CallFormWidget::onExtraInfoLoaded(const ccappcore::Contact &c, const QString& lookingForValue)
{
    if (_call.aNumber() == lookingForValue)
        _callTab->setContact(c);
}
