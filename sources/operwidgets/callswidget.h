#ifndef CALLSWIDGET_H
#define CALLSWIDGET_H

#include <QWidget>
#include <QPointer>
#include "appcore/lateboundobject.h"
#include "contactsmanager.h"


class QTabWidget;
class ContactsView;
class CallsView;

class CallsWidget : public QWidget
{
    Q_OBJECT
public:
    explicit CallsWidget(QWidget *parent = 0);

signals:

public slots:
    void activeTabChanged(int index);

private:
    LateBoundObject<ContactsManager>    _contactsManager;

    QPointer <QTabWidget>   _tabWidget;
    QPointer <ContactsView> _callsView;
    QPointer <CallsView>    _callsView2;

};

#endif // CALLSWIDGET_H
