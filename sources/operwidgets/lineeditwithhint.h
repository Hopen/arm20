#ifndef SEARCHLINE_H
#define SEARCHLINE_H

#include "operwidgets_global.h"

class LineEditWithHint : public QLineEdit {
    Q_OBJECT

    Q_PROPERTY(bool isHintVisible READ isHintVisible);
public:
    LineEditWithHint(QWidget *parent = 0);
    ~LineEditWithHint();

    void showHint();
    bool isHintVisible() const;
    const QString& hintText() { return _hintText; }
    void setHintText(const QString& s);


signals:
    void cancelPressed();
protected:
    void focusInEvent(QFocusEvent *);
    void focusOutEvent(QFocusEvent *);
    void keyPressEvent(QKeyEvent *);

private:
    bool _hintMode;
    QString _hintText;

private slots:
    void on_textChanged(const QString&);
};

#endif // SEARCHLINE_H
