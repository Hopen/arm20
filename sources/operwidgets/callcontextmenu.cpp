#include "callcontextmenu.h"

using namespace ccappcore;

CallContextMenu::CallContextMenu(QWidget *parent)
    : QMenu(parent)
{
    _showCallFormAction = addAction(QIcon(":/resources/icons/call_form_16x16.png"),
                                    tr("Открыть карточку звонка"),
                                    this, SLOT(showCallForm_triggered()));

    _dropCallAction = addAction(QIcon(":/resources/icons/call_end_32x32.png"),
                                tr("Завершить звонок"),
                                this, SLOT(dropCallAction_triggered()));

    _redialAction = addAction(QIcon(":/resources/icons/call_start_32x32.png"),
                                tr("Перезвонить абоненту"),
                                this, SLOT(redialCallAction_trigered()));

    _connectCallAction = addAction(QIcon(":/resources/icons/call_transfer_32x32.png"),
                                tr("Соединить"),
                                this, SLOT(connectCallAction_triggered()));

    _joinCallToConfAction = addAction(QIcon(":/resources/icons/new/conference03.png"),
                                tr("Добавить в конференцию"),
                                this, SLOT(joinCallAction_triggered()));

    _unjoinCallToConfAction = addAction(QIcon(":/resources/icons/new/conference04.png"),
                                tr("Удалить из конференции"),
                                this, SLOT(unjoinCallAction_triggered()));

    _removeConfAction = addAction(QIcon(":/resources/icons/new/conference05.png"),
                                tr("Удалить конференцию"),
                                this, SLOT(removeConfAction_triggered()));

}

void CallContextMenu::popup(ccappcore::Call c)
{
    _associatedCall = c;
    if(!c.isValid())
        return;

    _dropCallAction->setEnabled(!c.isCompleted());
    _redialAction->setVisible(_callsController->canMakeCall(_associatedCall));

    bool canConnectCalls = _callsController->canConnectCalls(
            _associatedCall, _callsController->lastCall() );
    _connectCallAction->setVisible( canConnectCalls );

    //_joinCallToConfAction   ->setEnabled( _callsController->canJoinCallToConf     (_associatedCall));
    _joinCallToConfAction   ->setEnabled( false);
    _unjoinCallToConfAction ->setEnabled( _callsController->canUnjoinCallFromConf (_associatedCall));
    _removeConfAction       ->setEnabled( _callsController->canRemoveConf         (_associatedCall));

    QMenu::popup(QCursor::pos());
}

void CallContextMenu::showCallForm_triggered()
{
    _appContext->showCallForm(_associatedCall);
}

void CallContextMenu::dropCallAction_triggered()
{
    _callsController->endCall(_associatedCall);
}

void CallContextMenu::redialCallAction_trigered()
{
    _callsController->makeCall(_associatedCall);
}

void CallContextMenu::connectCallAction_triggered()
{
    _callsController->connectCalls(_associatedCall, _callsController->lastCall());
}

void CallContextMenu::joinCallAction_triggered()
{

}

void CallContextMenu::unjoinCallAction_triggered()
{
    _callsController->unjoinCallFormConference(_associatedCall.confId(),_associatedCall.callId());
}

void CallContextMenu::removeConfAction_triggered()
{
    _callsController->removeConference(_associatedCall.confId());
}
