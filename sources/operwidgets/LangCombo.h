#ifndef LANGCOMBO_H
#define LANGCOMBO_H

#include <QStyledItemDelegate>
#include <QComboBox>
#include <QPointer>
#include <QPushButton>

class LangComboDelegate : public QStyledItemDelegate
{
Q_OBJECT
public:

    explicit LangComboDelegate(QObject *parent = 0);

    // painting
    void paint(QPainter *painter,
               const QStyleOptionViewItem &option, const QModelIndex &index) const;

    QSize sizeHint(const QStyleOptionViewItem &option,
                   const QModelIndex &index) const;
};

class ComboBoxEx;

class LangComboBoxWidget: public QWidget
{
Q_OBJECT
public:
    explicit LangComboBoxWidget(QWidget *parent = NULL);

    void setText(QString text);
    QString text()const;


signals:
    void changeLanguage(QString);

private:
    QPointer < ComboBoxEx > _cmbLanguage;


};

class ComboBoxEx : public QComboBox
{
    Q_OBJECT
public:
    explicit ComboBoxEx(QString text, QWidget *parent = NULL);
    QString text()const{return _text;}

public slots:
    void setText(QString newText);

protected:
    void paintEvent(QPaintEvent *event);

private:
    QScopedPointer<QPushButton> button;

    QString _text;
};


#endif // LANGCOMBO_H
