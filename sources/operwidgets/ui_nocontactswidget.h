/********************************************************************************
** Form generated from reading UI file 'nocontactswidget.ui'
**
** Created: Tue 26. Mar 13:54:37 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NOCONTACTSWIDGET_H
#define UI_NOCONTACTSWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QTextBrowser>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_NoContactsWidget
{
public:
    QVBoxLayout *verticalLayout;
    QTextBrowser *textBrowser;

    void setupUi(QWidget *NoContactsWidget)
    {
        if (NoContactsWidget->objectName().isEmpty())
            NoContactsWidget->setObjectName(QString::fromUtf8("NoContactsWidget"));
        NoContactsWidget->resize(159, 76);
        verticalLayout = new QVBoxLayout(NoContactsWidget);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        textBrowser = new QTextBrowser(NoContactsWidget);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setFrameShape(QFrame::NoFrame);
        textBrowser->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        textBrowser->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        textBrowser->setOpenExternalLinks(false);

        verticalLayout->addWidget(textBrowser);


        retranslateUi(NoContactsWidget);

        QMetaObject::connectSlotsByName(NoContactsWidget);
    } // setupUi

    void retranslateUi(QWidget *NoContactsWidget)
    {
        NoContactsWidget->setWindowTitle(QApplication::translate("NoContactsWidget", "Form", 0, QApplication::UnicodeUTF8));
        textBrowser->setHtml(QApplication::translate("NoContactsWidget", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<table border=\"0\" style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Ubuntu'; font-size:11pt; color:#7f7f7f;\">\320\232\320\276\320\275\321\202\320\260\320\272\321\202\321\213 \320\275\320\265 \320\275\320\260\320\271\320\264\320\265\320\275\321\213</span></p></td></tr></table></body></html>", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class NoContactsWidget: public Ui_NoContactsWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NOCONTACTSWIDGET_H
