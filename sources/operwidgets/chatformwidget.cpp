#include "chatformwidget.h"
#include "chattab.h"

ChatFormWidget::ChatFormWidget(ccappcore::Contact c, const QString &title, QWidget *parent, Qt::WindowFlags flags)
    :DockTab(title,parent,flags), _title(title),_contact(c)
{
    ChatTab* chatWidget = new ChatTab(_contact, this );
    setWidget(chatWidget);

    //DocklTitleBar* titleBar = new DocklTitleBar(title,this);
    //setTitleBarWidget(titleBar);

    //connect(chatWidget,SIGNAL(titleStatusChange(int)),this,SLOT(tabTitleColorChanged(int)));
    //chatWidget->setContact(_contact);
    connect(chatWidget,SIGNAL(readyToQuit()),this,SLOT(close()));

}

ChatFormWidget::~ChatFormWidget()
{
    int test = 0;
}

QString ChatFormWidget::title()const
{
    return _title;
}

//void ChatFormWidget::tabTitleColorChanged(int /*status*/)
//{
//    if (DocklTitleBar *titleBar = qobject_cast<DocklTitleBar*>(titleBarWidget()))
//        titleBar->setActiveStyle(status);
//}

void ChatFormWidget::close()
{
    //bool quitOnClose = this->testAttribute(Qt::WA_QuitOnClose);
    this->QDockWidget::close();
}

