#ifndef OPERSETTINGS_H
#define OPERSETTINGS_H

#include "operwidgets_global.h"
#include "appcore/appsettings.h"
#include "appcore/lateboundobject.h"

using namespace ccappcore;

class OperSettings
    : public QObject
    , public IAppSettingsHandler
{
    Q_OBJECT
    Q_INTERFACES(ccappcore::IAppSettingsHandler)
public:
    void registerMetaTypes()const;

    explicit OperSettings(QObject *parent = 0);

    bool showPrivateGroupsOnly() const { return _showPrivateGroupsOnly; }
    void setShowPrivateGroupsOnly(bool v) { _showPrivateGroupsOnly = v; }

    bool showCallFormImmediately() const { return _showCallFormImmediately; }
    void setShowCallFormImmediately(bool b) { _showCallFormImmediately = b; }

    bool closeCallFormOnEndCall() const { return _closeCallFormOnEndCall; }
    void setCloseCallFormOnEndCall(bool b) { _closeCallFormOnEndCall = b; }

    const QStringList& subjectList() const { return _subjectList; }
    void setSubjectList(QStringList v) { _subjectList = v; }

    const QVariantList& operationList()const {return _operationList;}
    void setOperationList(QVariantList v){_operationList = v;}

    const QVariantList& mainOperationList()const {return _mainOperationList;}
    void setMainOperationList(QVariantList v){_mainOperationList = v;}

    //const bool isDefaultOperTools()const {return _bDefaultOperTools;}

private:
    void _add(const QString &icon, const QString& text, const quint8& opernum);
    QString OperationString(const quint8& operNum)const;
public: //IAppSettingsHandler

    //Apply service configuration.
    virtual void applySettings(const AppSettings&);

    //Flush service configuration
    virtual void flushSettings(AppSettings&);

public:
signals:
    void OperOperationChanged();

//public slots:
//    void on_OperOperationChanged();
private:
    bool _showPrivateGroupsOnly;
    bool _showCallFormImmediately;
    bool _closeCallFormOnEndCall;
    QStringList _subjectList;
    QVariantList _operationList;
    QVariantList _mainOperationList;

    //bool _bDefaultOperTools;

};

#endif // OPERSETTINGS_H
