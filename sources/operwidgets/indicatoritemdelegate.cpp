#include "indicatoritemdelegate.h"

#include "appcore/quantativeindicator.h"
#include "appcore/useritemdatarole.h"
#include <QPixmap>

using namespace ccappcore;

IndicatorItemDelegate::IndicatorItemDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{
}

// painting
void IndicatorItemDelegate::paint(
        QPainter *painter,
        const QStyleOptionViewItem &option,
        const QModelIndex &index
        ) const
{
    QVariant varValue = index.data(AssociatedObjectRole);
    if(!varValue.isValid() || !varValue.canConvert<QPointer<QuantativeIndicator> >())
    {
        QStyledItemDelegate::paint(painter,option,index);
        return;
    }

    QPointer<QuantativeIndicator> indicator = varValue.value<QPointer<QuantativeIndicator> >();
    if(!indicator)
    {
        QStyledItemDelegate::paint(painter,option,index);
        return;
    }

    painter->save();
    QString value = indicator->toString();
    QString title = indicator->name();
    QIcon trendIcon = index.data(Qt::DecorationRole).value<QIcon>();
    QColor valueColor = index.data(Qt::ForegroundRole).value<QColor>();

    QRect borderRect = option.rect.adjusted( 1, 1, -1, -1 );
    QRect rect = borderRect.adjusted( 1, 1, -1, -1 );

    QRect titleRect = rect.adjusted(0,0,0,-rect.height()*2/3);
    QRect valueRect = rect.adjusted(0,rect.height()*1/3,-rect.width()*1/3,0);
    QRect trendRect = rect.adjusted(rect.width()*2/3,rect.height()*1/3,0,0);

    QFont font = painter->font();

    font.setPixelSize(titleRect.height());
    painter->setFont( font );
    painter->setPen(QPen(QColor(Qt::gray)));
    painter->drawRect(borderRect);

    painter->setPen(QPen(valueColor));

    painter->drawText( titleRect, Qt::AlignVCenter | Qt::AlignCenter, title );

    if(!trendIcon.isNull())
    {
        trendIcon.paint( painter,
                         trendRect,
                         Qt::AlignVCenter | Qt::AlignCenter
                       );
    }

    font.setPixelSize( valueRect.height()* 0.8 );
    painter->setFont( font );
    painter->drawText( valueRect, Qt::AlignVCenter | Qt::AlignCenter, value );
    painter->restore();
}

QSize IndicatorItemDelegate::sizeHint(
        const QStyleOptionViewItem &option,
        const QModelIndex &index
        ) const
{

    QVariant varValue = index.data(AssociatedObjectRole);
    if(!varValue.isValid() || !varValue.canConvert<QPointer<QuantativeIndicator> >())
        return QStyledItemDelegate::sizeHint(option,index);
    QPointer<QuantativeIndicator> indicator = varValue.value<QPointer<QuantativeIndicator> >();
    if(!indicator)
        return QStyledItemDelegate::sizeHint(option,index);

    QStyleOptionViewItemV4 vo = option;
    QStyle* style = vo.widget->style()?vo.widget->style():QApplication::style();
    QRect rect = style->itemTextRect(vo.fontMetrics, vo.rect, 0, true, indicator->name());

    int width = rect.width()+vo.decorationSize.width();
    width = width > 80 ? width : 80;
    return QSize(width, 20);
}

