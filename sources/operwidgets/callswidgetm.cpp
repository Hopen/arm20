#include "callswidgetm.h"

#include "appcore/useritemdatarole.h"
#include "applicationwidget.h"
#include "opertoolswidget.h"

QDockMinimizedWidget::QDockMinimizedWidget(const QString &title, QWidget *parent, Qt::WindowFlags flags)
    :QDockWidget(title,parent,flags)
{
    //_operMinCalls = new CallsWidgetM(this);

    setObjectName( "minimizedCallWidget" );
    setSizePolicy( QSizePolicy::Ignored, QSizePolicy::Ignored );

    setAllowedAreas( Qt::AllDockWidgetAreas );
    setFeatures( QDockWidget::DockWidgetClosable
                    |QDockWidget::DockWidgetMovable
                    |QDockWidget::DockWidgetFloatable );

    QPalette pal = palette();
    QPixmap pxm(":/style/resources/style/background5.png");
    pal.setBrush(QPalette::Background, QBrush(pxm));
    setPalette(pal);

}

QDockMinimizedWidget::QDockMinimizedWidget(QWidget *parent, Qt::WindowFlags flags)
    :QDockWidget(parent,flags)
{
    QDockMinimizedWidget(tr(""),parent,flags);
}

//QDockWidgetEx::QDockWidgetEx(const QString &title, QWidget *parent, Qt::WindowFlags flags)
//    :QDockWidget(title,parent,flags)
//{
//}

//QDockWidgetEx::QDockWidgetEx(QWidget *parent, Qt::WindowFlags flags)
//    :QDockWidget(parent,flags)
//{
//    QDockWidgetEx(tr(""),parent,flags);
//}

//QDockWidgetEx::~QDockWidgetEx()
//{
//    //int test = 0;
//}

//void QDockWidgetEx::closeEvent(QCloseEvent *event)
//{
//    emit closed();
//}


void QDockMinimizedWidget::setGeometry(const QRect &rect)
{
    _realSize = rect.width();
    QDockWidget::setGeometry(rect);
}

CallPropertyWidget::CallPropertyWidget(QWidget *parent):QWidget(parent)
{
    QLabel* a = new QLabel(tr("A"));
    _a = new QLineEdit(this);
    _a->setReadOnly(true);
    //_a->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    a->setBuddy(_a);

    QLabel* b = new QLabel(tr("B"));
    _b = new QLineEdit(this);
    _b->setReadOnly(true);
    b->setBuddy(_b);

    QLabel* priority = new QLabel(tr("Приоритет"));
    _priority = new QLineEdit(this);
    _priority->setReadOnly(true);
    priority->setBuddy(_priority);

    QLabel* extra = new QLabel(tr("Инфо"));
    _extra = new QLineEdit(this);
    _extra->setReadOnly(true);
    extra->setBuddy(_extra);

    QHBoxLayout *topLayout = new QHBoxLayout();
    topLayout->setMargin(0);

    QWidget* spacer = new QWidget();
    spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    topLayout->addWidget(spacer);
    topLayout->addWidget(a);
    topLayout->addWidget(_a);
    topLayout->addWidget(b);
    topLayout->addWidget(_b);
    topLayout->addWidget(priority);
    topLayout->addWidget(_priority);
    topLayout->addWidget(extra);
    topLayout->addWidget(_extra);
    topLayout->addWidget(spacer);

    setLayout(topLayout);

    _newCallStateCount = 0;
    connect(&_newCallStateTimer, SIGNAL(timeout()),
            this, SLOT(emitCallStateTimeChanged()));
    _backgroundSaveColor = QColor(0,0,0,0);
}

QSize CallPropertyWidget::minimumSizeHint()const
{
    QSize minSizeHint = QLineEdit().minimumSizeHint();
    minSizeHint = QSize(0,minSizeHint.height());
    return minSizeHint;
}

void CallPropertyWidget::setCall(ccappcore::Call c)
{
    _a       ->setText(tr("%1").arg(_controller->callAbonPhone(c)));
    _b       ->setText(tr("%1").arg(_controller->callOperPhone(c)));
    _priority->setText(tr("%1").arg(_controller->callPriority (c)));
    _extra   ->setText(tr("%1").arg(c.callExtraInfo()));

    //    _a       ->setText(tr("+79067837625"));
    //    _b       ->setText(tr("+79055505661"));
    //    _priority->setText(tr("1"));

//    _a       ->setText(tr(""));
//    _b       ->setText(tr(""));
//    _priority->setText(tr(""));
//    _extra   ->setText(tr(""));

    _a       ->setMaximumWidth(fontMetrics().width(_a       ->text()) + 10);
    _b       ->setMaximumWidth(fontMetrics().width(_b       ->text()) + 10);
    _priority->setMaximumWidth(fontMetrics().width(_priority->text()) + 10);
    _extra   ->setMaximumWidth(fontMetrics().width(_extra   ->text()) + 10);

    emit setCallComplited(realSize());

    _newCallStateCount = 0;
    emitCallStateTimeChanged();
    _newCallStateTimer.start(100);

}

void CallPropertyWidget::resetCall()
{
    _a       ->setText(tr(""));
    _b       ->setText(tr(""));
    _priority->setText(tr(""));
    _extra   ->setText(tr(""));

    emit setCallComplited(0);
}

void CallPropertyWidget::emitCallStateTimeChanged()
{
    if (_newCallStateCount >= BLINK_COUNT*2)
        _newCallStateTimer.stop();
    ++_newCallStateCount;
     repaint();
}

qint32 CallPropertyWidget::realSize()const
{
    qint32 fixed_size = 160; // "A" + "B" + "Приоритет" + "Инфо" + spacers
    qint32 size = fixed_size;
    //size+=fontMetrics().width(_a->text()) + 10;
    //size+=fontMetrics().width(_b->text()) + 10;
    //size+=fontMetrics().width(_priority->text()) + 10;
    size += _a        ->maximumWidth();
    size += _b        ->maximumWidth();
    size += _priority ->maximumWidth();
    size += _extra    ->maximumWidth();

    return size;
}

void CallPropertyWidget::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);

    QColor backgroundCurrentColor = _backgroundSaveColor;;
    if (_newCallStateCount && _newCallStateCount%2 == 0)
    {
        backgroundCurrentColor = QColor("#F1D8D8");//QColor(0,0,255,100);
        //QPixmap pxm(":/resources/icons/new/pawn_glass_red.png");
        //backgroundCurrentColor = QBrush(pxm).color();
    }

    p.fillRect(rect(), backgroundCurrentColor);
    //QPixmap pxm(":/resources/style/background2.png");
    //p.drawPixmap(rect(), pxm);
    p.save();

    p.restore();
}

CallsWidgetM::CallsWidgetM(bool isOnTop, QWidget *parent) :
    QWidget(parent),
    _stayOnTop(isOnTop),
    _minimized(false)
{
    connect(_appContext.instance(),SIGNAL(windowMinimized(bool)),this,SLOT(on_mainWindowMinimized(bool)));

    //QString text(tr("A: %1  B: %2  Pr:%3").arg(tr("8(906)783-76-25")).arg(tr("8(905)550-56-61")).arg(tr("Main")));
    //_text = new QLineEditEx(text);
    //_text->setReadOnly(true);
    _call = new CallPropertyWidget(this);
    connect(_call.data(),SIGNAL(setCallComplited(qint32)),this,SLOT(on_actionSetCallCompleted(qint32)));

    _appltools = new ApplicationToolBar(true, _stayOnTop);
    _appltools->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

    //connect(_appltools.data(),SIGNAL(minimizeWindow(bool)),this,SIGNAL(minimizeWindow(bool)));
    //connect(_appltools.data(),SIGNAL(staysOnTopWindow(bool)),this,SIGNAL(staysOnTopWindow(bool)));
    //connect(_appltools.data(),SIGNAL(staysOnTopWindow(bool)),this,SLOT(on_stayOnTop(bool)));
    connect(_appContext.instance(),SIGNAL(staysOnTop     (bool)),this,SLOT(on_stayOnTop(bool)));
    connect(_appContext.instance(),SIGNAL(windowMinimized(bool)),this,SLOT(on_minimized(bool)));

    _opertools = new OperToolBar(true);
    _opertools->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

//    _hideCalls = new QToolButton(this);
//    _hideCalls->setIcon(QIcon(":/resources/icons/new/forward 32.png"));
//    _hideCalls->setAutoRaise(true);
//    //_hideCalls->setCheckable(true);
//    //connect(_hideCalls.data(),SIGNAL(clicked()),this/*_callsView.data()*/,SIGNAL(testExpand()));
//    connect(_hideCalls.data(),SIGNAL(clicked()),this/*_callsView.data()*/,SLOT(test()));

//    _hideCalls->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

    QHBoxLayout *topLayout = new QHBoxLayout();
    //topLayout->setSpacing(0);
    topLayout->setMargin(0);

    // application toolbar
    QHBoxLayout *applLayout = new QHBoxLayout();
    applLayout->addWidget(_appltools);
    topLayout->addLayout(applLayout);

    // input/output call widget
    QHBoxLayout *textLayout = new QHBoxLayout();
    textLayout->addWidget(_call/*_text*/);
    topLayout->addLayout(textLayout, 1);
    //topLayout->addWidget(_text);

//    // test button
//    QHBoxLayout *buttonLayout = new QHBoxLayout();
//    buttonLayout->addWidget(_hideCalls);
//    topLayout->addLayout(buttonLayout);
//    //topLayout->addWidget(_hideCalls);

    // operation toolbar
    QHBoxLayout *operLayout = new QHBoxLayout();
    operLayout->addWidget(_opertools);
    topLayout->addLayout(operLayout);
    //topLayout->addWidget(_commontools);

    setLayout(topLayout);
}

CallsWidgetM::~CallsWidgetM()
{
    //int test = 0;
}

bool CallsWidgetM::isOwnerCall(Call call)
{
//    Call::CallState callState = call.callState();
//    if( callState<Call::CS_Assigned /*|| callState >= Call::CS_CallFailed*/ )
//        return        return true;

//    Call linkedCall = _controller->findByCallId(call.operCallId());
//    if(linkedCall.operId() == _controller->sessionOwner().contactId())
//        return true;

//    return false;
// false;

//    if(call.operId() == _controller->sessionOwner().contactId())
    Call::CallState callState = call.callState();
    if( callState<Call::CS_Assigned /*|| callState >= Call::CS_CallFailed*/ )
        return false;

    if(call.operId() == _controller->sessionOwner().contactId())
        return true;

    Call linkedCall = _controller->findByCallId(call.operCallId());
    if(linkedCall.operId() == _controller->sessionOwner().contactId())
        return true;

    return false;

}

void CallsWidgetM::on_callStateChanged(Call c)
{
    if (!c.isValid())
        return;

    if (isOwnerCall(c))
    {
        Call::CallState callState = c.callState();
        switch (callState)
        {
        case Call::CS_Assigned:
        case Call::CS_Calling:
        case Call::CS_RingStarted:
        {
            _associatedCall = c;
            _call->setCall(_associatedCall);
            _appContext->onActiveCallChanged(_associatedCall);
            break;
        }

        case Call::CS_Completed:
        case Call::CS_CallFailed:
        {
            if (_associatedCall == c)
            {
                _call->resetCall();
                _associatedCall = Call::invalid();
            }
            qDebug() << tr("CallsWidgetM::on_callStateChanged - Call::CS_CallFailed");
            break;
        }

        default:
        {

        }
        }
    }
}

void CallsWidgetM::on_actionSetCallCompleted(qint32 size)
{
    emit setWindowSize(size);
}

void CallsWidgetM::on_mainWindowMinimized(bool minimized)
{
    if (minimized)
    {
        connect(_controller.instance(),
                SIGNAL(callStateChanged(ccappcore::Call)),
                this,
                SLOT(on_callStateChanged(ccappcore::Call)),
                Qt::QueuedConnection);
//        connect (_controller.instance(),
//                 SIGNAL(callAssigned(ccappcore::Call)),
//                 this,
//                 SLOT(on_callStateChanged(ccappcore::Call)),
//                 Qt::QueuedConnection);
//        connect (_controller.instance(),
//                 SIGNAL(callStarted(ccappcore::Call)),
//                 this,
//                 SLOT(on_callStateChanged(ccappcore::Call)),
//                 Qt::QueuedConnection);
//        connect (_controller.instance(),
//                 SIGNAL(callEnd(ccappcore::Call)),
//                 this,
//                 SLOT(on_callStateChanged(ccappcore::Call)),
//                 Qt::QueuedConnection);
//        connect (_controller.instance(),
//                 SIGNAL(callFailed(ccappcore::Call,ccappcore::Call::CallResult,QString)),
//                 this,
//                 SLOT(on_callStateChanged(ccappcore::Call)),
//                 Qt::QueuedConnection);

    }
    else
    {
        disconnect(_controller.instance(),
                SIGNAL(callStateChanged(ccappcore::Call)),
                this,
                SLOT(on_callStateChanged(ccappcore::Call)));
//        disconnect (_controller.instance(),
//                    SIGNAL(callAssigned(ccappcore::Call)),
//                    this,
//                    SLOT(on_callStateChanged(ccappcore::Call)));
//        disconnect (_controller.instance(),
//                    SIGNAL(callStarted(ccappcore::Call)),
//                    this,
//                    SLOT(on_callStateChanged(ccappcore::Call)));
//        disconnect (_controller.instance(),
//                    SIGNAL(callEnd(ccappcore::Call)),
//                    this,
//                    SLOT(on_callStateChanged(ccappcore::Call)));
//        disconnect (_controller.instance(),
//                    SIGNAL(callFailed(ccappcore::Call,ccappcore::Call::CallResult,QString)),
//                    this,
//                    SLOT(on_callStateChanged(ccappcore::Call)));
    }
}

void CallsWidgetM::on_stayOnTop(bool checked)
{
    _stayOnTop = checked;
    //emit staysOnTopWindow(checked);
}

void CallsWidgetM::on_minimized(bool checked)
{
    _minimized = checked;
}

static int count=0;
void CallsWidgetM::test()
{
    if(count%2 == 0)
    {
         _call->setCall(Call::invalid());
//    Message m(tr("C2O_QUEUE_CALL"));
//    m["call_id"] = 76;
//    m["call_type"] = tr("OP");
//    m["begin_time_t"] = QDateTime::currentDateTime();
//    m["begin_queue_time_t"] = QDateTime::currentDateTime();
//    m["priority"] = 1;
//    m["group_id"] = -1;
//    m["oper_id"] = 62;
//    m["a_num"] = tr("+79067837625");
//    m["b_num"] = tr("+79055505661");
//    _controller->on_c2oQueueCall(m);
    }
    else
    {
        _call->resetCall();
    }
    count++;
}
