#ifndef GLOBALADDRESSBOOKWIDGET_H
#define GLOBALADDRESSBOOKWIDGET_H

#include <QWidget>
#include "appcore/lateboundobject.h"
#include "appcore/filteredmodel.h"
#include "appcore/globaladdressbookcontroller.h"
#include "appcore/contactstreemodel.h"

#include "appcontext.h"

using namespace ccappcore;

QT_BEGIN_NAMESPACE
class LineEditWithHint;
QT_END_NAMESPACE

//class GlobalAddressModel
//        : public QAbstractListModel
//        , public FilteredModel<ccappcore::GlobalAddress>
//{
//    Q_OBJECT
//public:

//    enum ColDef
//    {
//        IdColumn,
//        NameColumn,
//        FullNameColumn,
//        ParentIdColumn,
//        TypeColumn,
//        BookIdColumn,
//        BookNameColumn
//    };


//    GlobalAddressModel( QObject* parent = 0);
//    virtual ~GlobalAddressModel(){}

//    int rowCount(const QModelIndex &parent = QModelIndex()) const;
//    QVariant data(const QModelIndex &index, int role) const;
//    QVariant headerData(int section, Qt::Orientation orientation,
//                        int role = Qt::DisplayRole) const;
//    int columnCount ( const QModelIndex & parent = QModelIndex() ) const;

////    Qt::ItemFlags flags(const QModelIndex &index) const;
////    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

////    bool dropMimeData(const QMimeData *data, Qt::DropAction action,
////                      int row, int column, const QModelIndex &parent);
////    QMimeData *mimeData(const QModelIndexList &indexes) const;
////    QStringList mimeTypes() const;
////    Qt::DropActions supportedDropActions() const;


//protected: //FilteredModel
//    virtual void applyFilter(const PredicateGroup<GlobalAddress>& filter);

//private:
//    QString columnNameById(const qint32& id)const;
//signals:
//public slots:
//        void reset();
//        void sort();
//protected:
//    LateBoundObject < GlobalAddressBookController > _controller;
//    LateBoundObject < AppContext      > _appContext;
//    QList<ColDef> _columnsList;
//    GlobalAddressBookController::GlobalAddressContainer _persons; //currently displayed persons
//};


class AddressExtraInfoModel
        : public QAbstractListModel
{
    Q_OBJECT
public:

    enum ColDef
    {
        TypeColumn,
        ValueColumn
    };


    AddressExtraInfoModel( QObject* parent = 0);
    virtual ~AddressExtraInfoModel(){}

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;
    int columnCount ( const QModelIndex & parent = QModelIndex() ) const;

private:
    QString columnNameById(const qint32& id)const;
    QString typeNameById(const qint32& id)const;
signals:
public slots:
        void reset();
        void onExtraInfoLoaded(const ccappcore::Contact& c);
protected:
    LateBoundObject < GlobalAddressBookController > _controller;
    //LateBoundObject < AppContext      > _appContext;
    QList<ColDef> _columnsList;
    GlobalAddressBookController::AddressExtraInfoContainer _info;
    GlobalAddress _assignedAddress;
};


class GlobalAddressBookWidget: public IAlarmHandler
{
    Q_OBJECT
public:
    explicit GlobalAddressBookWidget(QWidget *parent = 0);
     QAbstractItemView* view()const{return _mainView.data();}
     QAbstractItemView* infoView()const{return _infoView.data();}

    // IAlarmHandler interface
    int getTabIndex()const;
    QIcon getTabIcon (bool _default = false) const;
    void connectToTimer();

public slots:
    void searching();
    void startTimeout();
    void on_searchLine_textChanged(QString text);

    void onGlobalAddressChanged(ccappcore::Contact c);
    void onGlobalAddressSelected(ccappcore::Contact c);

    void onLoadPersonInfoCompleted(const ccappcore::Contact& c);
    void onResetModel();
protected:
    QPointer< ContactsTreeModel   > _mainModel;
    QPointer< QTreeView           > _mainView;
    QPointer< LineEditWithHint    > _searchLine;
    QPointer< QTreeView           > _infoView;


    QTimer pauseTimoutTimer;
    QString _lastSearchString;

    LateBoundObject < GlobalAddressBookController > _controller;
    LateBoundObject < AppContext                  > _appContext;
};

#endif // GLOBALADDRESSBOOKWIDGET_H
