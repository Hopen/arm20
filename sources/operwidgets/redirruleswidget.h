#ifndef REDIRECTRULESWIDGET_H
#define REDIRECTRULESWIDGET_H

#include <QWidget>
#include <QDialog>
#include "appcore/appsettings.h"
#include "appcore/lateboundobject.h"
#include "appcore/redirectlistcontroller.h"

using namespace ccappcore;

QT_BEGIN_NAMESPACE
class QAbstractListModel;
class QTreeView;
class QPushButton;
class QLineEdit;
QT_END_NAMESPACE

class RedirectCallsWidget : public QDialog
{
    Q_OBJECT
public:
    explicit RedirectCallsWidget(QWidget *parent = 0);
    QString inputPhoneNumber()const{return _inputPhoneNumber;}
signals:

public slots:

    void on_btnOkCliked();
    void on_btnCancelCliked();
private:
     QLineEdit* _phoneNum;
     QString _inputPhoneNumber;
};

class RuleListModel : public QAbstractListModel
{
    Q_OBJECT

    enum ColDef
    {
        IdColumn = 0,
        NameColumn,
        IfTypeColumn,
        IfValueColumn,
        ActTypeColumn,
        ActValueColumn,
        MaxCountColumn

    };

    public:
        RuleListModel(/*const QPointer<RedirectListController> controller,*/ QObject *parent = 0);


        int rowCount(const QModelIndex &parent = QModelIndex()) const;
        QVariant data(const QModelIndex &index, int role) const;
        QVariant headerData(int section, Qt::Orientation orientation,
                            int role = Qt::DisplayRole) const;
        int columnCount ( const QModelIndex & parent = QModelIndex() ) const;

        bool insertRows(int position, int rows, const QModelIndex &index = QModelIndex());
        bool removeRows(int position, int rows, const QModelIndex &index = QModelIndex());

        bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

public slots:
        void on_dataChanged(const QModelIndex &index);

    private:
        LateBoundObject<RedirectListController> _controller;
        QStringList _columnsList;
};

class RedirectRulesWidget : public QWidget
{
//    enum ePredefineRules
//    {
//        PR_CALL2NUM = 0,
//        PR_CALL2VM
//    };

    Q_OBJECT
public:
    explicit RedirectRulesWidget(QWidget *parent = 0);

signals:

public slots:
    void on_ruleSelect(QModelIndex);

    void on_btnEdit_clicked  ();
    void on_btnNew_clicked   ();
    void on_btnDelete_clicked();

    void on_call2NumClicked       ();
    void on_call2VoiceMailClicked ();
    //void on_ruleAdded             ();

private:
    QPointer<  QAbstractListModel  > _mainModel;
    QPointer<  QTreeView           > _mainView;
    QPushButton *                    _editButton;
    QPushButton *                    _deleteButton;

    LateBoundObject<RedirectListController> _controller;

    //qint32                           _curRule;
};

class ProfileSystemMaster: public QWidget
{
    Q_OBJECT
public:
    ProfileSystemMaster(QWidget *parent = 0);
    virtual ~ProfileSystemMaster();
    void activateCall2NumProfile      ();
    void activateCall2VoicemailProfile();
    void deactivateCall2NumProfile      ();
    void deactivateCall2VoicemailProfile();
    bool IsAnySystemProfileActive()const;

private:
    void activateSystemProfile(const QString& sysName);
    void deactivateSystemProfile(const QString& sysName);
private:
    LateBoundObject<RedirectListController> _controller;
    QString                                 _name;
    RedirectRule::ePredefineRules           _ruleId;
    qint32                                  _taskId;
//    bool                                    _call2NumActive;
//    bool                                    _call2VoicemailActive;
signals:
    void profileActivated(qint32);
    void profileActivateError(QString error);
public slots:
    void on_profileAdded();
    void on_profileEdited(qint32 position);
    void on_ruleAdded();
    void on_ruleEdited(qint32 position);
};

#endif // REDIRECTRULESWIDGET_H
