#ifndef OPERACTIONSMENU_H
#define OPERACTIONSMENU_H

#include "operwidgets_global.h"
#include "appcore/operlistcontroller.h"
#include "appcontext.h"
#include "contactstatusmenu.h"

using namespace ccappcore;

class ContactContextMenu : public QMenu
{
    Q_OBJECT
public:
    explicit ContactContextMenu(QWidget *parent = 0);

    void popup(ccappcore::Operator op);

signals:

public slots:
    void doMakeCall();
    void doTransfer();
    void doNewMail();

private:
    LateBoundObject<OperListController> _operListController;
    LateBoundObject<CallsController> _callsController;
    LateBoundObject<AppContext> _appContext;
    ccappcore::Operator _op; //assoicuated operator

    ContactStatusMenu _statusMenu;
    QAction* _statusMenuItem;
    QPointer<QAction> _makeCallAction;
    QPointer<QAction> _transferCallAction;
    QPointer<QAction> _mailToAction;

    //add QAction for each infoId in listInfoId.
    //Returns True if there are >1 info found.
    bool addContactInfo(const QList<int>& listInfoId, QAction* target);
};

#endif // OPERACTIONSMENU_H
