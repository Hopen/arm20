#ifndef TOOLBUTTONOPERSTATUS_H
#define TOOLBUTTONOPERSTATUS_H

#include "operwidgets_global.h"

#include "appcore/sessioncontroller.h"
#include "appcore/operlistcontroller.h"
#include "appcore/lateboundobject.h"
#include "contactstatusmenu.h"

using namespace ccappcore;

class ToolButtonOperStatus : public QToolButton {
    Q_OBJECT
public:
    ToolButtonOperStatus(QWidget *parent = 0);
    ~ToolButtonOperStatus();

    ContactStatusMenu& menu() { return _operStatusMenu; }

    void showEvent(QShowEvent *);

signals:
    void changed(bool Pause = false);
    void updateControlsStatus();

private slots:
    void onClicked();
    void onCheckedActionChanged(const QAction* action);
    void onControlsStatusChanged();

private:
    LateBoundObject<SessionController> _sessionController;
    LateBoundObject<OperListController> _operListController;
    ContactStatusMenu _operStatusMenu;
};

class ToolButtonOperStatusEx: public QWidget
{
    Q_OBJECT
public:
    ToolButtonOperStatusEx(QWidget *parent = 0);
private:
    QPointer<ToolButtonOperStatus>  _toolButtonStatus;
    QPointer<QToolButton> _quickPause;
signals:
    void quickBtnClicked(bool isPause);
private slots:
    void onToolButtonOperStatusChanged(const bool& isPause);

};

#endif // TOOLBUTTONOPERSTATUS_H
