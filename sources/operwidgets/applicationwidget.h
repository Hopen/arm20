#ifndef APPLICATIONWIDGET_H
#define APPLICATIONWIDGET_H

#include <QWidget>
#include <QPointer>
#include "appcore/lateboundobject.h"
#include "appcore/callscontroller.h"
#include "toolbuttonoperstatus.h"
#include "appcontext.h"
//#include "callswidgetm.h"

class QToolBar;
class QLabel;
//class ToolButtonOperStatus;
class QToolButton;
//class SessionController;
//class ccappcore::CallsController;
using namespace ccappcore;


//class ApplicationWidget : public QWidget
//{
//    Q_OBJECT
//public:
//    explicit ApplicationWidget(QWidget *parent = 0);

//signals:

//public slots:

//private:
//    LateBoundObject<SessionController>  _sessionController;
//    // GUI
//    QPointer<QLabel> _queueStatus;
//    QPointer<QLabel> _opername;
//    QPointer<ToolButtonOperStatusEx>  _toolButtonStatus;
//    //QPointer<QToolButton> _quickPause;
//    QPointer<QToolButton> _mailButton;
//    QPointer<QToolButton> _helpButton;
//    QPointer<QToolButton> _miniButton;
//    QPointer<QToolButton> _soundButton;
//    QPointer<QToolButton> _settingsButton;
//};

class QToolBar;
class ApplicationToolBar: public QToolBar
{
    Q_OBJECT
public:
    explicit ApplicationToolBar(bool minimize, bool stayOnTop,  QWidget *parent = 0);
    //void minimize(bool enable);
signals:
//    void minimizeWindow(bool);
//    void staysOnTopWindow(bool);
    //void testExpand();
    void displaySettings();
    void showVolumeWidget();

public slots:
    void on_callListChanged();
    void on_minimizedWindow(bool);
    //void onBtnSoundClicked();

private:
    LateBoundObject<SessionController>  _sessionController;
    LateBoundObject<CallsController>    _callsController;
    LateBoundObject<AppContext>         _appContext;
    // GUI
    QPointer<QLabel> _queueStatus;
    QPointer<QLabel> _opername;
    QPointer<ToolButtonOperStatusEx>  _toolButtonStatus;
    //QPointer<QToolButton> _quickPause;
    QPointer<QToolButton> _mailButton;
    QPointer<QToolButton> _helpButton;
    QPointer<QToolButton> _miniButton;
    QPointer<QToolButton> _soundButton;
    QPointer<QToolButton> _settingsButton;

    //QPointer<CallsWidgetM> _callsView;
    //QPointer<QToolButton>  _hideCalls;
    QPointer<QToolButton> _staysOnTop;

    //bool _minimized;

};

#endif // APPLICATIONWIDGET_H
