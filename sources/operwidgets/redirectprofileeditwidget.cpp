#include "QtGui"
#include "redirectprofileeditwidget.h"

RedirectProfileEditWidget::RedirectProfileEditWidget(QAbstractItemModel* profileModel, QAbstractItemModel* taskModel, const QModelIndex & index, QWidget *parent) :
    QDialog(parent)
{
    QLabel* labelProfile = new QLabel(tr("Имя профайла"));
    _nameProfile = new QLineEdit(this);
    labelProfile->setBuddy(_nameProfile);

    _currentTask = new QComboBox(this);
    QLabel* labelTask = new QLabel(tr("Доб. правило"));
    labelTask->setBuddy(_currentTask);
    _currentTask->setModel(taskModel);

    QPushButton* btnOK = new QPushButton();
    btnOK->setText(tr("OK"));
    QPushButton* btnCancel = new QPushButton();
    btnCancel->setText(tr("Cancel"));

//    connect(_currentTask, SIGNAL(currentIndexChanged(int)),
//                this, SLOT(on_taskChanged()));
    connect(btnOK    ,SIGNAL(clicked()),this,SLOT(on_btnOkCliked    ()));
    connect(btnCancel,SIGNAL(clicked()),this,SLOT(on_btnCancelCliked()));


    _mapper = new QDataWidgetMapper(this);
    _mapper->setModel(profileModel);
    //_mapper->addMapping(nameEdit, 0);
    _mapper->addMapping(_nameProfile, 1);
    _mapper->addMapping(_currentTask, 2, "currentIndex");
    //_mapper->toFirst();
    //_mapper->setCurrentIndex(1);
    _mapper->setSubmitPolicy(QDataWidgetMapper::ManualSubmit);
    _mapper->setCurrentModelIndex(index);

    QGridLayout *layout = new QGridLayout();
    layout->addWidget(labelProfile, 0, 0, 1, 1);
    layout->addWidget(_nameProfile, 0, 1, 1, 1);
    layout->addWidget(labelTask   , 1, 0, 1, 1);
    layout->addWidget(_currentTask, 1, 1, 1, 1);

    QHBoxLayout *bottomLayout = new QHBoxLayout();
    bottomLayout->addStretch();
    bottomLayout->addWidget(btnOK);
    bottomLayout->addWidget(btnCancel);
    bottomLayout->addStretch();

    //layout->addWidget(btnOK       , 1, 0, 1, 1);
    //layout->addWidget(btnCancel   , 1, 1, 1, 1);

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addLayout(layout);
    mainLayout->addLayout(bottomLayout);

    setLayout(mainLayout);

    setWindowTitle(tr("Создать профиль"));

}

void RedirectProfileEditWidget::on_btnOkCliked()
{
    _mapper->submit();
    done(QDialog::Accepted);
    //accept();
}

void RedirectProfileEditWidget::on_btnCancelCliked()
{
    done(QDialog::Rejected);
    //reject();
}

//void RedirectProfileEditWidget::on_taskChanged()
//{
//    qint32 taskId =  _currentTask->itemData(_currentTask->currentIndex()).toInt();
//}
