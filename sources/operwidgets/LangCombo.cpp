#include "LangCombo.h"
#include <QStringListModel>
#include <QPainter>
#include <QItemDelegate>
#include <QApplication>
#include <QStylePainter>

LangComboDelegate::LangComboDelegate(QObject *parent)
    : QStyledItemDelegate(parent)
{


}

void LangComboDelegate::paint(QPainter *painter,
           const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if(!index.isValid())
    {
        return;
    }
    QStyleOptionViewItemV4 opt(option);
    opt.showDecorationSelected = true;

    painter->save();
    QStyle *style = opt.widget ? opt.widget->style() : QApplication::style();

    style->drawPrimitive(QStyle::PE_PanelItemViewItem, &opt, painter, opt.widget);

    QRect textRect = opt.rect;

    QString displayText = index.data(Qt::DisplayRole).toString();

    QColor textColor = Qt::blue;

    painter->setPen(QPen(textColor));

    painter->drawText(textRect,Qt::AlignCenter, displayText);

    painter->restore();


}

QSize LangComboDelegate::sizeHint(const QStyleOptionViewItem &option,
               const QModelIndex &index) const
{
    //return QStyledItemDelegate::sizeHint(option, index);
    return QSize(25, 25);
}


LangComboBoxWidget::LangComboBoxWidget(QWidget *parent)
    :QWidget(parent)
{
    //create widgets
    _cmbLanguage = new ComboBoxEx(tr("RU"),this);

    _cmbLanguage->setMinimumHeight(25);

    _cmbLanguage->setMinimumWidth(25);
    _cmbLanguage->setMaximumWidth(25);

    //create model
    QStringListModel *model = new QStringListModel();
    QStringList list;
    list << "RU" << "EN";
    model->setStringList(list);

    _cmbLanguage->setModel(model);

    _cmbLanguage->setItemDelegate(new LangComboDelegate(_cmbLanguage.data()));

    connect(_cmbLanguage, SIGNAL(currentIndexChanged(QString)),
            _cmbLanguage, SLOT(setText(QString)));

    connect(_cmbLanguage, SIGNAL(currentIndexChanged(QString)),
            this, SIGNAL(changeLanguage(QString)));




}

void LangComboBoxWidget::setText(QString text)
{
    //setCurrentIndex
    QStringListModel *model = qobject_cast<QStringListModel*>(_cmbLanguage->model());
    if (!model)
    {
        _cmbLanguage->setText(text); // by default
        return;
    }

    QStringList list = model->stringList();
    for (int i=0;i<list.size();++i)
    {
        if (list[i] == text)
        {
            _cmbLanguage->setCurrentIndex(i);
            break;
        }
    }

    //
}

QString LangComboBoxWidget::text()const
{
    return _cmbLanguage->text();
}



ComboBoxEx::ComboBoxEx(QString text, QWidget *parent)
    :QComboBox(parent), button(new QPushButton), _text(text)
{
    button->setStyleSheet("background-color: CornflowerBlue");
}


void ComboBoxEx::paintEvent( QPaintEvent * )
{
    QStylePainter p( this );
    QStyleOptionButton b;
    b.initFrom( this );
    b.state = QStyle::State_Raised | QStyle::State_Enabled;
    b.rect = QRect( 0, 0, 25, 25 );
    b.text = _text;
    button->style()->drawControl(QStyle::CE_PushButton, &b, &p,
                                         button.data());
}

void ComboBoxEx::setText(QString newText)
{
    _text = newText;
}

























