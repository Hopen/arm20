#include <QtCore/QCoreApplication>
#include <QtDBus/QDBusInterface>
#include <QtCore/QStringList>
#include <QtCore/QDebug>
#include <QtDBus/QDBusReply>

enum eScriptError
{
    SE_NOT_ENOUGH_PARAMS = -101,
    SE_COULD_NOT_CONNECT,
    SE_COULD_NOT_GET_INTERFACE,
    SE_SCRIPT_EXEC_FAILED
};

//#include <iostream>
using namespace std;

const QString PARAM_WAIT    = "/wait"   ;
//const QString PARAM_REGISTR = "/registr";
//const QString PARAM_URI     = "/uri";

class CWaiter
{
public:
    CWaiter(const bool& param):_haveToWait(param)
    {}
    virtual ~CWaiter()
    {
        if (_haveToWait)
        {
            fprintf(stderr,"press <Enter> to exit...");
            getchar();
        }
    }
private:
    bool _haveToWait;
};

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    bool bWait = false;
    QString scriptName,
            scriptParams;

    // a.arguments().at(0) - it's the name of application - "runarmscript.exe"

    fprintf(stderr, "%s",a.arguments().at(0).toStdString().c_str());
    for (int i=1;i<a.arguments().count();++i)
    {
        if (!QString (a.arguments().at(i)).compare(PARAM_WAIT))
            bWait = true;
        else if (scriptName.isEmpty())
        {
            scriptName = a.arguments().at(i);
        }
        else
        {
            scriptParams+=QObject::tr(" %1").arg(a.arguments().at(i));
        }
        fprintf(stderr, " %s",a.arguments().at(i).toStdString().c_str());
    }
    fprintf(stderr, "\n\n");


    CWaiter waiter(bWait);

    if( (a.arguments().count() == 1) || scriptName.isEmpty())
    {
        QString m = QString("Not enough params\n"
                   "runarmscript <script name> [<params>] [%2]\n")
                .arg(PARAM_WAIT);
        fprintf(stderr,m.toStdString().c_str());
        return SE_NOT_ENOUGH_PARAMS;
    }

//    QString message = QString (QObject::tr("Executing script: \"%1 %2\"\n")).arg(scriptName).arg(scriptParams);
//    fprintf(stderr,message.toStdString().c_str());

    if (!QDBusConnection::sessionBus().isConnected()) {
        fprintf(stderr, "Cannot connect to the D-Bus session bus.\n"
                "To start it, run:\n"
                "\teval `dbus-launch --auto-syntax`\n");
        return SE_COULD_NOT_CONNECT;
    }

    QDBusInterface iface("DBus.ARM.QScript", "/", "", QDBusConnection::sessionBus());
    if (!iface.isValid())
    {
        fprintf(stderr, "%s\n",
                qPrintable(QDBusConnection::sessionBus().lastError().message()));
        return SE_COULD_NOT_GET_INTERFACE;
    }

    QDBusReply<QString> reply = iface.call("exec",scriptName, scriptParams);
    if (!reply.isValid())
    {
        fprintf(stderr, "Call failed: %s\n", qPrintable(reply.error().message()));
        return SE_SCRIPT_EXEC_FAILED;
    }

    printf("Reply was: %s\n", qPrintable(reply.value()));
    return 0;
}
