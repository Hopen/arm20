#-------------------------------------------------
#
# Project created by QtCreator 2012-11-13T16:56:54
#
#-------------------------------------------------

QT       += dbus

QT       -= gui

TARGET = runarmscript
CONFIG   += console
CONFIG   -= app_bundle
#CONFIG += link_prl

DESTDIR = $$OUT_PWD/../bin

TEMPLATE = app


SOURCES += \
    runarmscript.cpp

contains( CONFIG, static) {
  CONFIG  += staticlib
  DEFINES += QT_NODLL
  DEFINES += QCA_STATIC
}


##INCLUDEPATH+= d:/Qt_source_4.8.3/lib
windows {
CONFIG(debug,debug|release): LIBS+=-lQtDBusd4 -Ld:/Qt_source_4.8.3/lib
CONFIG(release,debug|release): LIBS+=-lQtDBus4 -Ld:/Qt_source_4.8.3/lib
}

#windows {
#CONFIG(debug,debug|release): LIBS+=-lQtCored -Ld:/Qt_source_4.8.3/lib
#CONFIG(release,debug|release): LIBS+=-lQtCore -Ld:/Qt_source_4.8.3/lib
#}



