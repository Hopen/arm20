#include "JabberController.h"

#include "QXmppLogger.h"
#include "QXmppMessage.h"
#include "QXmppRosterManager.h"
#include "spamlistcontroller.h"
#include "appsettings.h"

namespace ccappcore
{

//***************** JABBER CLIENT ****************
jabberClient::jabberClient(QObject *parent)
    : QXmppClient(parent)
{
}

jabberClient::~jabberClient()
{

}

//*************** JABBER CONTROLLER **************

JabberController::JabberController(QObject *parent)
    : ContactsController (parent)
{
    _xmppClient = new jabberClient(this);

    bool check = connect(_xmppClient.data(), SIGNAL(messageReceived(QXmppMessage)),
        SLOT(messageReceived(QXmppMessage)));

    check *= connect(_xmppClient.data(), SIGNAL(error(QXmppClient::Error)),
                    this, SLOT(slotError(QXmppClient::Error)));

    check *= connect(_xmppClient.data(), SIGNAL(connected()),
                     this, SLOT(onConnected()));

    QXmppRosterManager& mng = _xmppClient->rosterManager();
    check *= connect(&mng, SIGNAL(rosterReceived()),
                    this, SLOT(onRosterReceived()));


    /// Then QXmppRoster::presenceChanged() is emitted whenever presence of someone
    /// in roster changes
    check = connect(&_xmppClient->rosterManager(), SIGNAL(presenceChanged(QString,QString)),
                    this, SLOT(onPresenceChanged(QString,QString)));


    Q_ASSERT(check);
    Q_UNUSED(check);

}

JabberController::~JabberController()
{
    stopController();
}

QFuture<Contact> JabberController::loadContactsAsync()
{
    qWarning("JabberController::loadContactsAsync() is not implemented.");
}

const QList<Contact>& JabberController::contacts() const
{
    return _clientList;
}

QUrl JabberController::buildContactUrl(const Contact& c) const
{
    qWarning("JabberController::buildContactUrl() is not implemented.");

    return QString();
}


bool JabberController::isStarted() const
{
    return _xmppClient->isConnected();
}

void JabberController::startController()
{
    stopController();

    _xmppClient->logger()->setLoggingType(QXmppLogger::SignalLogging);
    //_xmppClient->connectToServer("gubarev-forte-it-ru@jivosite.com", "YtjDby22");
    _xmppClient->connectToServer(_settings->getOption("jabber/xmppAddress").toString(),
                                 _settings->getOption("jabber/xmppPassword").toString());
}

void JabberController::stopController()
{
    if(!isStarted())
        return;

    _xmppClient->disconnectFromServer();
}


void JabberController::messageReceived(const QXmppMessage& message)
{
    QString from = message.from();
    QString msg = message.body();
    if(msg.isEmpty()) //skip empty messages
        return;

    int id = getId(from);
    if (id > 0)
        _spamController->rosterIN(id,/*from,*/msg);

}

void JabberController::messageSend(const QString &jid, const QString &text) const
{
    _xmppClient->sendPacket(QXmppMessage("", jid, text));
}

void JabberController::slotError(QXmppClient::Error error)
{
    qDebug() << "Jabber connection failed:" << error;
}

void JabberController::onConnected()
{
    qDebug() << "Jabber connection successful:";
}

//void JabberController::onStateChanged(QXmppClient::State state)
//{

//}
static int RosterState2ContactState(const int& rosterState)
{
/*
        Online = 0,      ///< The entity or resource is online.
        Away,           ///< The entity or resource is temporarily away.
        XA,             ///< The entity or resource is away for an extended period.
        DND,            ///< The entity or resource is busy ("Do Not Disturb").
        Chat,           ///< The entity or resource is actively interested in chatting.
        Invisible       ///< obsolete XEP-0018: Invisible Presence

*/

/*
        UnknownStatus = -1,
        Free = 0,
        Busy = 1,
        Paused = 2,
        Offline = 3//,

*/
    switch (QXmppPresence::AvailableStatusType(rosterState))
    {
    case QXmppPresence::Chat:
    case QXmppPresence::Online:
        return Contact::Free;
    case QXmppPresence::Away:
        return Contact::Busy;
    case QXmppPresence::XA:
    case QXmppPresence::DND:
        return Contact::Paused;
    case QXmppPresence::Invisible:
        return Contact::Offline;
    }

    return Contact::Offline;
}

Roster JabberController::setStatus(const QString &bareJid)
{
    int jid = getId(bareJid);
    if (jid>0)
    {
        RosterId rosterId = createRosterId(jid);
        Roster op = findById(rosterId);

        if(!op.isValid())
        {
            op = Contact(rosterId);
            //_clientList.push_back(op);
            QString name = _xmppClient->rosterManager().getRosterEntry(bareJid).name();
            if(name.isEmpty())
                name = "-";
            name+= " [" +bareJid +"]";

            op.setName(name);
            op.addData((Contact::ContactInfo)Contact::BareJid, bareJid);
            op.addData((Contact::ContactInfo)Contact::Phone, QString("-"));
        }

        QMap<QString, QXmppPresence> presences = _xmppClient->rosterManager().
                                                 getAllPresencesForBareJid(bareJid);
        if (!presences.isEmpty())
        {
            QXmppPresence presence = *presences.begin();
            QXmppPresence::AvailableStatusType state = presence.availableStatusType();

            op.setStatus((Contact::ContactStatus)RosterState2ContactState(state));
        }
        else
        {
            op.setStatus(Contact::Offline);
        }

        op.setStatusTime(QDateTime::currentDateTime());

        return op;
    }

    return Contact::invalid();
}

void JabberController::onRosterReceived()
{
//    foreach (const QString &bareJid, _xmppClient->rosterManager().getRosterBareJids())
//    {
//        QString name = _xmppClient->rosterManager().getRosterEntry(bareJid).name();
//        if(name.isEmpty())
//            name = "-";
//        name+= " [" +bareJid +"]";

//        int jid = getId(bareJid);
//        if (jid>0)
//        {
//            RosterId rosterId = createRosterId(jid);
//            Roster op = findById(rosterId);

//            if(!op.isValid())
//            {
//                op = Contact(rosterId);
//                _clientList.push_back(op);
//            }

//            QMap<QString, QXmppPresence> presences = _xmppClient->rosterManager().
//                                                     getAllPresencesForBareJid(bareJid);
//            if (!presences.isEmpty())
//            {
//                QXmppPresence presence = *presences.begin();
//                QXmppPresence::AvailableStatusType state = presence.availableStatusType();

//                op.setStatus((Contact::ContactStatus)RosterState2ContactState(state));
//            }
//            else
//            {
//                op.setStatus(Contact::Offline);
//            }

//            op.setName(name);
//            op.addData((Contact::ContactInfo)Contact::BareJid, bareJid);

//            //Contact::ContactStatus newStatus = /*Contact::ContactStatus)(int)m["status"]*/ Contact::Free;

//            op.addData((Contact::ContactInfo)Contact::Phone, QString("-"));
//            //op.setStatus( newStatus );
//            //op.setStatusReason( m["reason"].value().toString() );

//            op.setStatusTime(QDateTime::currentDateTime());

//        }
//        qDebug() << "Jabber roster received: " << bareJid << " [" << name << "]";
//    }

//    emit dataChanged();


    foreach (const QString &bareJid, _xmppClient->rosterManager().getRosterBareJids())
    {
        QString name = _xmppClient->rosterManager().getRosterEntry(bareJid).name();
        if(name.isEmpty())
            name = "-";
        name+= " [" +bareJid +"]";

//        Roster op = setStatus(bareJid);
//        if (op.isValid())
//        {

//            _clientList.push_back(op);

////            op.setName(name);
////            op.addData((Contact::ContactInfo)Contact::BareJid, bareJid);

////            //Contact::ContactStatus newStatus = /*Contact::ContactStatus)(int)m["status"]*/ Contact::Free;

////            op.addData((Contact::ContactInfo)Contact::Phone, QString("-"));
////            //op.setStatus( newStatus );
////            //op.setStatusReason( m["reason"].value().toString() );

//            //op.setStatusTime(QDateTime::currentDateTime());

//        }
        qDebug() << "Jabber roster received: " << bareJid << " [" << name << "]";
    }

    //emit dataChanged();
}



void JabberController::onPresenceChanged(const QString& bareJid,
                                 const QString& resource)
{
//    int id = getId(bareJid);
//    if (id > 0)
//    {
//        RosterId rosterId = createRosterId(id);
//        Roster op = findById(rosterId);
//        if(!op.isValid())
//        {
//            op = Contact(rosterId);
//            _clientList.push_back(op);
//        }


//        QMap<QString, QXmppPresence> presences = _xmppClient->rosterManager().
//                                                 getAllPresencesForBareJid(bareJid);
//        if (!presences.isEmpty())
//        {
//            QXmppPresence presence = *presences.begin();
//            QXmppPresence::AvailableStatusType state = presence.availableStatusType();

//            op.setStatus((Contact::ContactStatus)RosterState2ContactState(state));
//        }
//        else
//        {
//            op.setStatus(Contact::Offline);
//        }
//        //op.setStatusReason( m["reason"].value().toString() );
//        op.setStatusTime( QDateTime::currentDateTime() );
//        emit contactStatusChanged(op);
//    }


//    qDebug("Jabber presence changed %s/%s", qPrintable(bareJid), qPrintable(resource));


    Roster op = setStatus(bareJid);
    if (op.isValid())
    {
        if (!findById(op.contactId()).isValid())
        {
            _clientList.push_back(op);
            emit dataChanged();
        }
        else
        //op.setStatusReason( m["reason"].value().toString() );
        //op.setStatusTime( QDateTime::currentDateTime() );
            emit contactStatusChanged(op);

        QString name = _xmppClient->rosterManager().getRosterEntry(bareJid).name();
        if(name.isEmpty())
            name = "-";
        name+= " [" +bareJid +"]";

        if (op.name()!=name)
        {
            op.setName(name);
            emit dataChanged();
            emit contactStatusChanged(op);
        }
    }


    qDebug("Jabber presence changed %s/%s", qPrintable(bareJid), qPrintable(resource));
}

RosterId JabberController::createRosterId(int id) const
{
    return ContactId(const_cast<JabberController*>(this), id, CT_Roster);
}

int JabberController::getId(const QString &jid) const
{
    QString numbers = "0123456789";
    QString id = jid.left(jid.indexOf("@"));
    QString retValue;
    for (int i=0;i<id.length();++i)
    {
        if (numbers.contains(id[i]))
            retValue+=id[i];
    }

    if (retValue.isEmpty())
        return -1;

    return retValue.toInt();
}

Contact JabberController::findById(const ContactId& id) const
{
    foreach(const Contact& c, _clientList)
        if(c.contactId() == id)
            return c;
    return Contact::invalid();
}


} //namespace ccappcore
