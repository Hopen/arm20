#include "presetmanager.h"
#include <QMainWindow>
#include <QDockWidget>
#include <QDomDocument>
#include "presetwidget.h"
#include <QApplication>
#include <QDesktopWidget>

namespace ccappcore {

PresetManager::PresetManager(QObject *parent)
    : QObject(parent)
{
    registerMetaObject<DockWidget>();
}

void PresetManager::setMainWindow(QMainWindow* mainWindow)
{
    Q_ASSERT(!_mainWindow);
    _mainWindow = mainWindow;
}

const QMetaObject* PresetManager::findMetaObject(const QString& className)
{
    if(_metaObjects.contains(className.toAscii()))
        return _metaObjects[className.toAscii()];
    return NULL;
}


void PresetManager::addWidget(PresetWidget* widget)
{
    Q_ASSERT(!_widgets.contains(widget));
    if(!_widgets.contains(widget))
    {
        _widgets.append(widget);
        connect(widget, SIGNAL(destroyed(QObject*)), SLOT(presetWidgetDestroyed(QObject*)));
    }
}

void PresetManager::removeWidget(QObject* widget)
{
    Q_ASSERT(_widgets.contains(widget));
    if(_widgets.contains(widget))
    {
        widget->disconnect(this);
        _widgets.removeOne(widget);
    }
}

void PresetManager::presetWidgetDestroyed(QObject* o)
{
    removeWidget(o);
}

void PresetManager::addDockedWidget(PresetWidget* widget, const QString& title)
{
    DockWidget* dw = new DockWidget(this);
    addWidget(dw);
    dw->create(widget);
    dw->dockWidget()->setWindowTitle(title);
    dw->dockWidget()->setFloating(true);
    dw->dockWidget()->setVisible(true);

    int screen = qApp->desktop()->screenNumber(mainWindow());
    QRect screenRect = qApp->desktop()->screenGeometry(screen<0?0:screen);
    QRect widgetRect = dw->dockWidget()->rect();
    widgetRect.moveCenter(screenRect.center());
    dw->dockWidget()->move(widgetRect.topLeft());
}

bool PresetManager::load(QIODevice* ioDevice)
{
    QDomDocument doc;
    QString error;
    int line = 0, column = 0;
    if(!doc.setContent(ioDevice, &error, &line, &column))
    {
        qWarning()<<"PresetManager::load() - "<< error
                <<" at line "<<line<<", column"<<column;
        return false;
    }

    if(doc.documentElement().nodeName()!="preset")
    {
        qWarning()<<"PresetManager::load() - document is not a valid preset.";
        return false;
    }

    QDomElement widgetsElement = doc.documentElement().firstChildElement("widgets");
    foreach(QDomElement el, DomElementList(widgetsElement.childNodes()))
    {

        PresetWidget* widget = NULL;
        if(PresetWidget::readElement(this, el, &widget))
            addWidget(widget);
    }

    _layoutSettings.clear();
    LateBoundObject<LayoutManager> layoutManager;
    QDomElement layoutElement = doc.documentElement().firstChildElement("layoutSettings");
    if(layoutManager.isValid() && !layoutElement.isNull())
    {
        foreach(QDomElement el, DomElementList(layoutElement.childNodes()))
        {
            QString key = el.attribute("key");
            QVariant value = el.attribute("value");
            _layoutSettings[key] = value;
        }
    }

    return true;
}

bool PresetManager::save(QIODevice* ioDevice)
{
    QDomDocument doc;
    QDomProcessingInstruction pi = doc.createProcessingInstruction(
            "xml", "version=\"1.0\" encoding=\"UTF-8\""
            );
    doc.appendChild(pi);
    QDomElement root = doc.createElement("preset");
    doc.appendChild(root);

    QDomElement widgetsElement = doc.createElement("widgets");
    root.appendChild(widgetsElement);
    foreach(QObject* o, _widgets)
    {
        PresetWidget* w = qobject_cast<PresetWidget*>(o);
        if(w)
            PresetWidget::appendElement(this, widgetsElement, w);
    }

    QDomElement layoutElement = doc.createElement("layoutSettings");
    root.appendChild(layoutElement);
    LateBoundObject<LayoutManager> layoutManager;
    if(layoutManager.isValid())
    {
        _layoutSettings.clear();
        layoutManager->saveLayout(_layoutSettings);
        QMapIterator<QString, QVariant> it(_layoutSettings);
        while(it.hasNext())
        {
            it.next();
            QDomElement el = doc.createElement("add");
            el.setAttribute("key",it.key());
            el.setAttribute("value",it.value().toString());
            layoutElement.appendChild(el);
        }
    }

    QTextStream stream(ioDevice);
    stream.setCodec("UTF-8");
    doc.save(stream, 4);
    stream.flush();
    return true;
}

void PresetManager::load()
{
    QFile file("calloper.preset");
    file.open(QIODevice::ReadOnly);
    load(&file);
}

void PresetManager::save()
{
    QFile file("calloper.preset");
    file.open(QIODevice::WriteOnly);
    save(&file);
}

void PresetManager::restoreWidgets()
{
    foreach(QObject* o, _widgets)
    {
        PresetWidget* w = qobject_cast<PresetWidget*>(o);
        if(w)
            w->restoreWidget();
    }
    LateBoundObject<LayoutManager> layoutManager;
    if(layoutManager.isValid())
        layoutManager->restoreLayout(_layoutSettings);
}

void PresetManager::destroyWidgets()
{
    foreach(QObject* o, _widgets)
    {
        PresetWidget* w = qobject_cast<PresetWidget*>(o);
        if(w)
            w->destroyWidget();
    }
}


} // namespace ccappcore
