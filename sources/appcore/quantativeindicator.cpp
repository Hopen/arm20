#include "quantativeindicator.h"
#include <functional>
#include <numeric>
#include <algorithm>
#include "timeutils.h"

namespace ccappcore
{

QuantativeIndicator::QuantativeIndicator(QObject* parent)
    : QObject(parent)
    , _value(0), _min(0), _max(0)
    , _warningValue(0), _criticalValue(0)
    , _inverted(false)
    , _format(Format_Value)
    , _trend(UndefinedTrend)
    , _severity(UndefinedSeverity)
{
}

void QuantativeIndicator::setMaxValue(int max)
{
    if(_max!=max)
    {
        _max = max;
        emit maxChanged(max);
    }
}

void QuantativeIndicator::setMinValue(int min)
{
    if(_min!=min)
    {
        _min = min;
        emit minChanged(min);
    }
}

void QuantativeIndicator::setValue(QDateTime valueTime, int newValue)
{
    int oldValue = value();

    _history[valueTime] = newValue;

    std::map<QDateTime,int>::const_iterator i = _history.end();
    --i;
    _value = i->second;

    if( _value > _max )
        setMaxValue(_value);

    updateTrend();
    updateSeverity();

    if( _value != oldValue )
    {
        emit valueChanged(_value);
        emit formattedValueChanged(toString());
    }
}

void QuantativeIndicator::setFormat(Format f)
{
    if(_format==f)
        return;

    _format = f;
    emit formatChanged(f);
    emit valueChanged(_value);
    emit formattedValueChanged(toString());
}


QString QuantativeIndicator::toString() const
{
    switch( _format )
    {
        case Format_Percents:
        {
            return qvariant_cast<QString>(value()) + "%";
        }
        case Format_ShortTime:
        {
            return TimeUtils::SecsToShortHMS(value());
        }
        case Format_Value:
        default:
        {
            return qvariant_cast<QString>(value());
        }
    }
}

void QuantativeIndicator::clear()
{
    _history.clear();
    _trend = UndefinedTrend;
    _severity = UndefinedSeverity;
}

void QuantativeIndicator::shrinkHistory(QDateTime dateFrom, QDateTime dateTo)
{
    if(_history.size() == 0)
        return;

    std::map<QDateTime,int>::iterator pos = _history.begin();
    std::map<QDateTime,int>::iterator last = --_history.end();
    while( pos != last )
    {
        if(pos->first < dateFrom || pos->first > dateTo)
            _history.erase(pos++);
        else
            ++pos;
    }
    updateTrend();
}

void QuantativeIndicator::shrinkHistory(int maxItems)
{
    while( _history.size() > (size_t)maxItems )
        _history.erase( _history.begin() );

    updateTrend();
}

void QuantativeIndicator::updateTrend()
{
    Trend oldTrend = _trend;
    int size = _history.size();
    if( size < 2 )
    {
        _trend = UndefinedTrend;
        return;
    }

    std::map<QDateTime,int>::const_iterator last = --_history.end();
    std::map<QDateTime,int>::const_iterator i;
    int sum = 0;
    for( i = _history.begin(); i!=_history.end(); ++i )
        sum+=i->second;

    double avg1 = double(sum - last->second)/double(size-1);
    double avg2 = double(sum) / (double) size;
    double diff = avg2 - avg1; //diff indicates how average has changed

    if( fabs(diff) < 0.01)
        _trend = UndefinedTrend;
    else
        _trend = diff > 0.0 ? PositiveTrend : NegativeTrend;
    if(oldTrend!=_trend)
        emit trendChanged(_trend);
}

void QuantativeIndicator::updateSeverity()
{
    Severity newSeverity = NormalValue;
    if(warningValue() ==0 && criticalValue() == 0)
    {
        newSeverity = UndefinedSeverity;
    }
    else if(!isInverted())
    {
        if(value()>criticalValue()) //average not more than
            newSeverity = CriticallyHighValue;
        else if(value()>warningValue()) //normal not more than
            newSeverity = AboveNormalValue;
    }
    else
    {
        if(value()<warningValue())//average not less than
            newSeverity = CriticallyLowValue;
        else if(value()<criticalValue()) //normal not less than
            newSeverity = BelowNormalValue;
    }
    if(newSeverity!=severity())
    {
        _severity = newSeverity;
        emit severityChanged(newSeverity);
    }
}


void QuantativeIndicator::setWarningValue(int value)
{
    if(_warningValue!=value)
    {
        _warningValue = value;
        emit warningValueChanged(value);
        updateSeverity();
    }
}

void QuantativeIndicator::setCriticalValue(int value)
{
    if(_criticalValue!=value)
    {
        _criticalValue = value;
        emit criticalValueChanged(value);
        updateSeverity();
    }
}

void QuantativeIndicator::setInverted(bool v)
{
    if(v!=_inverted)
    {
        _inverted = v;
        isInvertedChanged(_inverted);
        updateSeverity();
    }
}



}
