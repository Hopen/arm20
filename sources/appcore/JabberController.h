#ifndef JABBERCONTROLLER_H
#define JABBERCONTROLLER_H

#include <QObject>
#include "QXmppClient.h"
#include "appcore.h"
#include "contactscontroller.h"

namespace ccappcore
{

typedef ContactId RosterId;
typedef Contact   Roster;

enum ContactType2
{
    CT_Roster = 3
};


//***************** JABBER CLIENT ****************
class jabberClient : public QXmppClient
{
    Q_OBJECT

public:
    jabberClient(QObject *parent = 0);
    ~jabberClient();
};

//*************** JABBER CONTROLLER **************
class SpamListController;
class AppSettings;
class APPCORE_EXPORT JabberController : public ContactsController//public QObject
{
    Q_OBJECT

public:
    explicit JabberController(QObject *parent = 0);
    virtual ~JabberController();

    // interface implementation
    virtual QFuture<Contact> loadContactsAsync();
    virtual const QList<Contact>& contacts() const;
    virtual QUrl buildContactUrl(const Contact&) const;

    RosterId createRosterId(int id) const;

    bool isStarted() const;
    void startController();
    void stopController();

    int getId(const QString& jid)const;
    virtual Contact findById(const ContactId&) const;

public slots:
    void messageReceived(const QXmppMessage&);
    void messageSend(const QString& jid, const QString& text)const;
    void slotError(QXmppClient::Error error);
    void onConnected();
    void onRosterReceived();
    void onPresenceChanged(const QString& bareJid, const QString& resource);
    //void onStateChanged(QXmppClient::State state);

private:
    Roster setStatus(const QString& bareJid);


private:
    QPointer<jabberClient> _xmppClient;
    LateBoundObject < SpamListController > _spamController;
    LateBoundObject < AppSettings        > _settings;

    QList<Roster> _clientList;
};
} //namespace ccappcore

#endif //JABBERCONTROLLER_H
