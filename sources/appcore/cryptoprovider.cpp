#include "cryptoprovider.h"
#include "QtCrypto"

#include "irouterclient.h"
#include "lateboundobject.h"
#include "requestreplymessage.h"

namespace ccappcore
{

class CryptoProvider::Private
{
public:
    QCA::RSAPublicKey publicKey;
    QCA::RSAPrivateKey privateKey;
};

CryptoProvider::CryptoProvider(QObject *parent)
    : QObject(parent)
    , d(new Private())
{
    QCA::init();
}

CryptoProvider::~CryptoProvider()
{
    delete d;
    QCA::deinit();
}

void CryptoProvider::reqPublicKey()
{
    RequestReplyMessage::send( Message("O2C_GET_PUBLICKEY"),
                               Message("C2O_GET_PUBLICKEY"),
                               this, SLOT(on_c2oGetPublickKey(ccappcore::Message)));
}

bool CryptoProvider::canEncryptRSA() const
{
    return !d->publicKey.isNull();
}

QByteArray CryptoProvider::encryptRSA(const QByteArray& source) const
{
    if(!canEncryptRSA())
    {
        qWarning()<<"CryptoProvider::decryptRSA() failed.";
        return QByteArray();
    }
    return d->publicKey.encrypt(source, QCA::EME_PKCS1_OAEP).toByteArray();
}

bool CryptoProvider::canDecryptRSA() const
{
    return !d->privateKey.isNull();
}

QByteArray CryptoProvider::decryptRSA(const QByteArray& source) const
{
    QCA::SecureArray secureArray;
    if(!canDecryptRSA() || !d->privateKey.decrypt(source,&secureArray,QCA::EME_PKCS1_OAEP))
    {
        qWarning()<<"CryptoProvider::decryptRSA() failed.";
        return QByteArray();
    }

    return QByteArray();
}

void CryptoProvider::on_c2oGetPublickKey(const Message& m)
{
    QCA::SecureArray n = m["n"].value().toByteArray();
    QCA::SecureArray e = m["e"].value().toByteArray();
    d->publicKey = QCA::RSAPublicKey(n,e);
    emit publicKeyReady();
}

}
