/****************************************************************************
** Meta object code from reading C++ file 'operlistcontroller.h'
**
** Created: Mon 25. Mar 16:55:41 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../operlistcontroller.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'operlistcontroller.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ccappcore__OperListController[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      33,   31,   30,   30, 0x08,
      68,   31,   30,   30, 0x08,
     104,   31,   30,   30, 0x08,
     142,   31,   30,   30, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ccappcore__OperListController[] = {
    "ccappcore::OperListController\0\0m\0"
    "on_c2oOperList(ccappcore::Message)\0"
    "on_c2oGroupList(ccappcore::Message)\0"
    "on_c2oOperInGroup(ccappcore::Message)\0"
    "on_c2oUpdateOperStatus(ccappcore::Message)\0"
};

void ccappcore::OperListController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        OperListController *_t = static_cast<OperListController *>(_o);
        switch (_id) {
        case 0: _t->on_c2oOperList((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 1: _t->on_c2oGroupList((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 2: _t->on_c2oOperInGroup((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 3: _t->on_c2oUpdateOperStatus((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ccappcore::OperListController::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ccappcore::OperListController::staticMetaObject = {
    { &ContactsController::staticMetaObject, qt_meta_stringdata_ccappcore__OperListController,
      qt_meta_data_ccappcore__OperListController, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ccappcore::OperListController::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ccappcore::OperListController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ccappcore::OperListController::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ccappcore__OperListController))
        return static_cast<void*>(const_cast< OperListController*>(this));
    return ContactsController::qt_metacast(_clname);
}

int ccappcore::OperListController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ContactsController::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
