/****************************************************************************
** Meta object code from reading C++ file 'proxymodellist.h'
**
** Created: Mon 25. Mar 16:55:13 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../proxymodellist.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'proxymodellist.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ccappcore__ProxyModelList[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      27,   26,   26,   26, 0x08,
      63,   26,   26,   26, 0x08,
     110,   90,   26,   26, 0x08,
     158,   26,   26,   26, 0x08,
     195,  177,   26,   26, 0x08,
     249,  177,   26,   26, 0x08,
     294,  177,   26,   26, 0x08,
     347,  177,   26,   26, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ccappcore__ProxyModelList[] = {
    "ccappcore::ProxyModelList\0\0"
    "sourceModelLayoutAboutToBeChanged()\0"
    "sourceModelLayoutChanged()\0"
    "topLeft,bottomRight\0"
    "sourceModelDataChanged(QModelIndex,QModelIndex)\0"
    "sourceModelReset()\0parent,first,last\0"
    "sourceModelRowsAboutToBeInserted(QModelIndex,int,int)\0"
    "sourceModelRowsInserted(QModelIndex,int,int)\0"
    "sourceModelRowsAboutToBeRemoved(QModelIndex,int,int)\0"
    "sourceModelRowsRemoved(QModelIndex,int,int)\0"
};

void ccappcore::ProxyModelList::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ProxyModelList *_t = static_cast<ProxyModelList *>(_o);
        switch (_id) {
        case 0: _t->sourceModelLayoutAboutToBeChanged(); break;
        case 1: _t->sourceModelLayoutChanged(); break;
        case 2: _t->sourceModelDataChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        case 3: _t->sourceModelReset(); break;
        case 4: _t->sourceModelRowsAboutToBeInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 5: _t->sourceModelRowsInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 6: _t->sourceModelRowsAboutToBeRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 7: _t->sourceModelRowsRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ccappcore::ProxyModelList::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ccappcore::ProxyModelList::staticMetaObject = {
    { &QAbstractItemModel::staticMetaObject, qt_meta_stringdata_ccappcore__ProxyModelList,
      qt_meta_data_ccappcore__ProxyModelList, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ccappcore::ProxyModelList::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ccappcore::ProxyModelList::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ccappcore::ProxyModelList::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ccappcore__ProxyModelList))
        return static_cast<void*>(const_cast< ProxyModelList*>(this));
    return QAbstractItemModel::qt_metacast(_clname);
}

int ccappcore::ProxyModelList::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractItemModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
