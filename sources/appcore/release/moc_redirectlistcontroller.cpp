/****************************************************************************
** Meta object code from reading C++ file 'redirectlistcontroller.h'
**
** Created: Mon 25. Mar 16:55:15 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../redirectlistcontroller.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'redirectlistcontroller.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ccappcore__RedirectListController[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      35,   34,   34,   34, 0x05,
      54,   50,   34,   34, 0x05,
      76,   34,   34,   34, 0x05,
      88,   50,   34,   34, 0x05,

 // slots: signature, parameters, type, tag, flags
     109,  107,   34,   34, 0x08,
     149,  107,   34,   34, 0x08,
     194,  107,   34,   34, 0x08,
     236,  107,   34,   34, 0x08,
     278,  107,   34,   34, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ccappcore__RedirectListController[] = {
    "ccappcore::RedirectListController\0\0"
    "profileAdded()\0pos\0profileEdited(qint32)\0"
    "ruleAdded()\0ruleEdited(qint32)\0m\0"
    "on_c2oRedirProfiles(ccappcore::Message)\0"
    "on_c2oCreateRedirProfile(ccappcore::Message)\0"
    "on_c2oGetRedirections(ccappcore::Message)\0"
    "on_c2oCreateRedirRule(ccappcore::Message)\0"
    "on_c2oScriptEvent(ccappcore::Message)\0"
};

void ccappcore::RedirectListController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        RedirectListController *_t = static_cast<RedirectListController *>(_o);
        switch (_id) {
        case 0: _t->profileAdded(); break;
        case 1: _t->profileEdited((*reinterpret_cast< qint32(*)>(_a[1]))); break;
        case 2: _t->ruleAdded(); break;
        case 3: _t->ruleEdited((*reinterpret_cast< qint32(*)>(_a[1]))); break;
        case 4: _t->on_c2oRedirProfiles((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 5: _t->on_c2oCreateRedirProfile((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 6: _t->on_c2oGetRedirections((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 7: _t->on_c2oCreateRedirRule((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 8: _t->on_c2oScriptEvent((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ccappcore::RedirectListController::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ccappcore::RedirectListController::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ccappcore__RedirectListController,
      qt_meta_data_ccappcore__RedirectListController, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ccappcore::RedirectListController::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ccappcore::RedirectListController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ccappcore::RedirectListController::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ccappcore__RedirectListController))
        return static_cast<void*>(const_cast< RedirectListController*>(this));
    return QObject::qt_metacast(_clname);
}

int ccappcore::RedirectListController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void ccappcore::RedirectListController::profileAdded()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void ccappcore::RedirectListController::profileEdited(qint32 _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ccappcore::RedirectListController::ruleAdded()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void ccappcore::RedirectListController::ruleEdited(qint32 _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE
