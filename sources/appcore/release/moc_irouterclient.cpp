/****************************************************************************
** Meta object code from reading C++ file 'irouterclient.h'
**
** Created: Mon 25. Mar 16:55:20 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../irouterclient.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'irouterclient.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ccappcore__IRouterClient[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      10,       // signalCount

 // signals: signature, parameters, type, tag, flags
      26,   25,   25,   25, 0x05,
      45,   38,   25,   25, 0x05,
      91,   25,   25,   25, 0x05,
     122,  106,   25,   25, 0x05,
     185,  173,   25,   25, 0x05,
     271,  173,   25,   25, 0x05,
     356,  173,   25,   25, 0x05,
     442,  438,   25,   25, 0x05,
     484,  438,   25,   25, 0x05,
     554,  515,   25,   25, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_ccappcore__IRouterClient[] = {
    "ccappcore::IRouterClient\0\0connected()\0"
    "reason\0disconnected(IRouterClient::DisconnectReason)\0"
    "error(QString)\0clientId,status\0"
    "statusChanged(quint32,IRouterClient::ClientStatus)\0"
    "msg,from,to\0"
    "messageReceived(ccappcore::Message,ccappcore::ClientAddress,ccappcore:"
    ":ClientAddress)\0"
    "messageSending(ccappcore::Message,ccappcore::ClientAddress,ccappcore::"
    "ClientAddress)\0"
    "messageSent(ccappcore::Message,ccappcore::ClientAddress,ccappcore::Cli"
    "entAddress)\0"
    "msg\0monitorNoticeReceived(ccappcore::Message)\0"
    "routeError(ccappcore::Message)\0"
    "eventHash,status,consumers,subscribers\0"
    "sibscribeNoticeReceived(quint32,IRouterClient::EventSubscriptionStatus"
    ",quint32,QVector<quint32>)\0"
};

void ccappcore::IRouterClient::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        IRouterClient *_t = static_cast<IRouterClient *>(_o);
        switch (_id) {
        case 0: _t->connected(); break;
        case 1: _t->disconnected((*reinterpret_cast< IRouterClient::DisconnectReason(*)>(_a[1]))); break;
        case 2: _t->error((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->statusChanged((*reinterpret_cast< quint32(*)>(_a[1])),(*reinterpret_cast< IRouterClient::ClientStatus(*)>(_a[2]))); break;
        case 4: _t->messageReceived((*reinterpret_cast< const ccappcore::Message(*)>(_a[1])),(*reinterpret_cast< const ccappcore::ClientAddress(*)>(_a[2])),(*reinterpret_cast< const ccappcore::ClientAddress(*)>(_a[3]))); break;
        case 5: _t->messageSending((*reinterpret_cast< const ccappcore::Message(*)>(_a[1])),(*reinterpret_cast< const ccappcore::ClientAddress(*)>(_a[2])),(*reinterpret_cast< const ccappcore::ClientAddress(*)>(_a[3]))); break;
        case 6: _t->messageSent((*reinterpret_cast< const ccappcore::Message(*)>(_a[1])),(*reinterpret_cast< const ccappcore::ClientAddress(*)>(_a[2])),(*reinterpret_cast< const ccappcore::ClientAddress(*)>(_a[3]))); break;
        case 7: _t->monitorNoticeReceived((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 8: _t->routeError((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 9: _t->sibscribeNoticeReceived((*reinterpret_cast< quint32(*)>(_a[1])),(*reinterpret_cast< IRouterClient::EventSubscriptionStatus(*)>(_a[2])),(*reinterpret_cast< quint32(*)>(_a[3])),(*reinterpret_cast< const QVector<quint32>(*)>(_a[4]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ccappcore::IRouterClient::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ccappcore::IRouterClient::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ccappcore__IRouterClient,
      qt_meta_data_ccappcore__IRouterClient, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ccappcore::IRouterClient::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ccappcore::IRouterClient::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ccappcore::IRouterClient::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ccappcore__IRouterClient))
        return static_cast<void*>(const_cast< IRouterClient*>(this));
    return QObject::qt_metacast(_clname);
}

int ccappcore::IRouterClient::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void ccappcore::IRouterClient::connected()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void ccappcore::IRouterClient::disconnected(IRouterClient::DisconnectReason _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ccappcore::IRouterClient::error(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void ccappcore::IRouterClient::statusChanged(quint32 _t1, IRouterClient::ClientStatus _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void ccappcore::IRouterClient::messageReceived(const ccappcore::Message & _t1, const ccappcore::ClientAddress & _t2, const ccappcore::ClientAddress & _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void ccappcore::IRouterClient::messageSending(const ccappcore::Message & _t1, const ccappcore::ClientAddress & _t2, const ccappcore::ClientAddress & _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void ccappcore::IRouterClient::messageSent(const ccappcore::Message & _t1, const ccappcore::ClientAddress & _t2, const ccappcore::ClientAddress & _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void ccappcore::IRouterClient::monitorNoticeReceived(const ccappcore::Message & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void ccappcore::IRouterClient::routeError(const ccappcore::Message & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void ccappcore::IRouterClient::sibscribeNoticeReceived(quint32 _t1, IRouterClient::EventSubscriptionStatus _t2, quint32 _t3, const QVector<quint32> & _t4)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}
QT_END_MOC_NAMESPACE
