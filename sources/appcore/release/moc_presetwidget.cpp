/****************************************************************************
** Meta object code from reading C++ file 'presetwidget.h'
**
** Created: Mon 25. Mar 16:55:36 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../presetwidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'presetwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ccappcore__PresetWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_ccappcore__PresetWidget[] = {
    "ccappcore::PresetWidget\0"
};

void ccappcore::PresetWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData ccappcore::PresetWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ccappcore::PresetWidget::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ccappcore__PresetWidget,
      qt_meta_data_ccappcore__PresetWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ccappcore::PresetWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ccappcore::PresetWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ccappcore::PresetWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ccappcore__PresetWidget))
        return static_cast<void*>(const_cast< PresetWidget*>(this));
    return QObject::qt_metacast(_clname);
}

int ccappcore::PresetWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_ccappcore__DockWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       1,   14, // constructors
       0,       // flags
       0,       // signalCount

 // constructors: signature, parameters, type, tag, flags
      23,   22,   22,   22, 0x0e,

       0        // eod
};

static const char qt_meta_stringdata_ccappcore__DockWidget[] = {
    "ccappcore::DockWidget\0\0DockWidget(QObject*)\0"
};

void ccappcore::DockWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::CreateInstance) {
        switch (_id) {
        case 0: { DockWidget *_r = new DockWidget((*reinterpret_cast< QObject*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast<QObject**>(_a[0]) = _r; } break;
        }
    }
    Q_UNUSED(_o);
}

const QMetaObjectExtraData ccappcore::DockWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ccappcore::DockWidget::staticMetaObject = {
    { &PresetWidget::staticMetaObject, qt_meta_stringdata_ccappcore__DockWidget,
      qt_meta_data_ccappcore__DockWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ccappcore::DockWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ccappcore::DockWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ccappcore::DockWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ccappcore__DockWidget))
        return static_cast<void*>(const_cast< DockWidget*>(this));
    return PresetWidget::qt_metacast(_clname);
}

int ccappcore::DockWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PresetWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
