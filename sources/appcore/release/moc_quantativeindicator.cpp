/****************************************************************************
** Meta object code from reading C++ file 'quantativeindicator.h'
**
** Created: Mon 25. Mar 16:55:37 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../quantativeindicator.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'quantativeindicator.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ccappcore__QuantativeIndicator[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
      11,   69, // properties
       3,  113, // enums/sets
       0,    0, // constructors
       0,       // flags
      11,       // signalCount

 // signals: signature, parameters, type, tag, flags
      41,   32,   31,   31, 0x05,
      59,   32,   31,   31, 0x05,
      97,   90,   31,   31, 0x05,
     120,  113,   31,   31, 0x05,
     136,   31,   31,   31, 0x05,
     188,   31,   31,   31, 0x05,
     246,   31,   31,   31, 0x05,
     300,   31,   31,   31, 0x05,
     321,   31,   31,   31, 0x05,
     347,   31,   31,   31, 0x05,
     372,   31,   31,   31, 0x05,

 // properties: name, type, flags
     404,  396, 0x0a495001,
     423,  419, 0x02495001,
     429,  419, 0x02495001,
     438,  419, 0x02495001,
     447,  419, 0x02495001,
     461,  419, 0x02495001,
     479,  474, 0x01495001,
     531,  490, 0x00495009,
     579,  540, 0x00495009,
     624,  586, 0x00495009,
     630,  396, 0x0a495103,

 // properties: notify_signal_id
       1,
       0,
       3,
       2,
       8,
       9,
      10,
       5,
       6,
       4,
       7,

 // enums: name, flags, count, data
     635, 0x0,    6,  125,
     644, 0x0,    3,  137,
     650, 0x0,    3,  143,

 // enum data: key, value
     657, uint(ccappcore::QuantativeIndicator::UndefinedSeverity),
     675, uint(ccappcore::QuantativeIndicator::CriticallyLowValue),
     694, uint(ccappcore::QuantativeIndicator::BelowNormalValue),
     711, uint(ccappcore::QuantativeIndicator::NormalValue),
     723, uint(ccappcore::QuantativeIndicator::AboveNormalValue),
     740, uint(ccappcore::QuantativeIndicator::CriticallyHighValue),
     760, uint(ccappcore::QuantativeIndicator::UndefinedTrend),
     775, uint(ccappcore::QuantativeIndicator::PositiveTrend),
     789, uint(ccappcore::QuantativeIndicator::NegativeTrend),
     803, uint(ccappcore::QuantativeIndicator::Format_Value),
     816, uint(ccappcore::QuantativeIndicator::Format_Percents),
     832, uint(ccappcore::QuantativeIndicator::Format_ShortTime),

       0        // eod
};

static const char qt_meta_stringdata_ccappcore__QuantativeIndicator[] = {
    "ccappcore::QuantativeIndicator\0\0"
    "newValue\0valueChanged(int)\0"
    "formattedValueChanged(QString)\0newMax\0"
    "maxChanged(int)\0newMin\0minChanged(int)\0"
    "trendChanged(ccappcore::QuantativeIndicator::Trend)\0"
    "severityChanged(ccappcore::QuantativeIndicator::Severity)\0"
    "formatChanged(ccappcore::QuantativeIndicator::Format)\0"
    "nameChanged(QString)\0criticalValueChanged(int)\0"
    "warningValueChanged(int)\0"
    "isInvertedChanged(bool)\0QString\0"
    "formattedValue\0int\0value\0minValue\0"
    "maxValue\0criticalValue\0warningValue\0"
    "bool\0isInverted\0"
    "ccappcore::QuantativeIndicator::Severity\0"
    "severity\0ccappcore::QuantativeIndicator::Format\0"
    "format\0ccappcore::QuantativeIndicator::Trend\0"
    "trend\0name\0Severity\0Trend\0Format\0"
    "UndefinedSeverity\0CriticallyLowValue\0"
    "BelowNormalValue\0NormalValue\0"
    "AboveNormalValue\0CriticallyHighValue\0"
    "UndefinedTrend\0PositiveTrend\0NegativeTrend\0"
    "Format_Value\0Format_Percents\0"
    "Format_ShortTime\0"
};

void ccappcore::QuantativeIndicator::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QuantativeIndicator *_t = static_cast<QuantativeIndicator *>(_o);
        switch (_id) {
        case 0: _t->valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->formattedValueChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->maxChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->minChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->trendChanged((*reinterpret_cast< ccappcore::QuantativeIndicator::Trend(*)>(_a[1]))); break;
        case 5: _t->severityChanged((*reinterpret_cast< ccappcore::QuantativeIndicator::Severity(*)>(_a[1]))); break;
        case 6: _t->formatChanged((*reinterpret_cast< ccappcore::QuantativeIndicator::Format(*)>(_a[1]))); break;
        case 7: _t->nameChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 8: _t->criticalValueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->warningValueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->isInvertedChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

#ifdef Q_NO_DATA_RELOCATION
static const QMetaObjectAccessor qt_meta_extradata_ccappcore__QuantativeIndicator[] = {
        ccappcore::QuantativeIndicator::getStaticMetaObject,
#else
static const QMetaObject *qt_meta_extradata_ccappcore__QuantativeIndicator[] = {
        &ccappcore::QuantativeIndicator::staticMetaObject,
#endif //Q_NO_DATA_RELOCATION
    0
};

const QMetaObjectExtraData ccappcore::QuantativeIndicator::staticMetaObjectExtraData = {
    qt_meta_extradata_ccappcore__QuantativeIndicator,  qt_static_metacall 
};

const QMetaObject ccappcore::QuantativeIndicator::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ccappcore__QuantativeIndicator,
      qt_meta_data_ccappcore__QuantativeIndicator, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ccappcore::QuantativeIndicator::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ccappcore::QuantativeIndicator::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ccappcore::QuantativeIndicator::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ccappcore__QuantativeIndicator))
        return static_cast<void*>(const_cast< QuantativeIndicator*>(this));
    return QObject::qt_metacast(_clname);
}

int ccappcore::QuantativeIndicator::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = toString(); break;
        case 1: *reinterpret_cast< int*>(_v) = value(); break;
        case 2: *reinterpret_cast< int*>(_v) = minValue(); break;
        case 3: *reinterpret_cast< int*>(_v) = maxValue(); break;
        case 4: *reinterpret_cast< int*>(_v) = criticalValue(); break;
        case 5: *reinterpret_cast< int*>(_v) = warningValue(); break;
        case 6: *reinterpret_cast< bool*>(_v) = isInverted(); break;
        case 7: *reinterpret_cast< ccappcore::QuantativeIndicator::Severity*>(_v) = severity(); break;
        case 8: *reinterpret_cast< ccappcore::QuantativeIndicator::Format*>(_v) = format(); break;
        case 9: *reinterpret_cast< ccappcore::QuantativeIndicator::Trend*>(_v) = trend(); break;
        case 10: *reinterpret_cast< QString*>(_v) = name(); break;
        }
        _id -= 11;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 10: setName(*reinterpret_cast< QString*>(_v)); break;
        }
        _id -= 11;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 11;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void ccappcore::QuantativeIndicator::valueChanged(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ccappcore::QuantativeIndicator::formattedValueChanged(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ccappcore::QuantativeIndicator::maxChanged(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void ccappcore::QuantativeIndicator::minChanged(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void ccappcore::QuantativeIndicator::trendChanged(ccappcore::QuantativeIndicator::Trend _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void ccappcore::QuantativeIndicator::severityChanged(ccappcore::QuantativeIndicator::Severity _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void ccappcore::QuantativeIndicator::formatChanged(ccappcore::QuantativeIndicator::Format _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void ccappcore::QuantativeIndicator::nameChanged(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void ccappcore::QuantativeIndicator::criticalValueChanged(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void ccappcore::QuantativeIndicator::warningValueChanged(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void ccappcore::QuantativeIndicator::isInvertedChanged(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}
QT_END_MOC_NAMESPACE
