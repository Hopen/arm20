/****************************************************************************
** Meta object code from reading C++ file 'presetmanager.h'
**
** Created: Mon 25. Mar 16:55:35 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../presetmanager.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'presetmanager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ccappcore__PresetManager[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      26,   25,   25,   25, 0x0a,
      43,   25,   25,   25, 0x0a,
      60,   25,   25,   25, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ccappcore__PresetManager[] = {
    "ccappcore::PresetManager\0\0restoreWidgets()\0"
    "destroyWidgets()\0presetWidgetDestroyed(QObject*)\0"
};

void ccappcore::PresetManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PresetManager *_t = static_cast<PresetManager *>(_o);
        switch (_id) {
        case 0: _t->restoreWidgets(); break;
        case 1: _t->destroyWidgets(); break;
        case 2: _t->presetWidgetDestroyed((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ccappcore::PresetManager::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ccappcore::PresetManager::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ccappcore__PresetManager,
      qt_meta_data_ccappcore__PresetManager, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ccappcore::PresetManager::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ccappcore::PresetManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ccappcore::PresetManager::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ccappcore__PresetManager))
        return static_cast<void*>(const_cast< PresetManager*>(this));
    return QObject::qt_metacast(_clname);
}

int ccappcore::PresetManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
