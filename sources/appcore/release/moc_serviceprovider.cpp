/****************************************************************************
** Meta object code from reading C++ file 'serviceprovider.h'
**
** Created: Mon 25. Mar 16:55:18 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../serviceprovider.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'serviceprovider.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ccappcore__ServiceProvider[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      32,   28,   27,   27, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ccappcore__ServiceProvider[] = {
    "ccappcore::ServiceProvider\0\0obj\0"
    "serviceDestroyed(QObject*)\0"
};

void ccappcore::ServiceProvider::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ServiceProvider *_t = static_cast<ServiceProvider *>(_o);
        switch (_id) {
        case 0: _t->serviceDestroyed((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ccappcore::ServiceProvider::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ccappcore::ServiceProvider::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ccappcore__ServiceProvider,
      qt_meta_data_ccappcore__ServiceProvider, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ccappcore::ServiceProvider::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ccappcore::ServiceProvider::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ccappcore::ServiceProvider::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ccappcore__ServiceProvider))
        return static_cast<void*>(const_cast< ServiceProvider*>(this));
    return QObject::qt_metacast(_clname);
}

int ccappcore::ServiceProvider::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
