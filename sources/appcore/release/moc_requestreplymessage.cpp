/****************************************************************************
** Meta object code from reading C++ file 'requestreplymessage.h'
**
** Created: Mon 25. Mar 16:55:22 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../requestreplymessage.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'requestreplymessage.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ccappcore__RequestReplyMessage[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      32,   31,   31,   31, 0x05,
      69,   31,   31,   31, 0x05,

 // slots: signature, parameters, type, tag, flags
      98,   95,   31,   31, 0x08,
     184,   31,   31,   31, 0x08,
     215,   31,   31,   31, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ccappcore__RequestReplyMessage[] = {
    "ccappcore::RequestReplyMessage\0\0"
    "responseReceived(ccappcore::Message)\0"
    "error(ccappcore::Message)\0,,\0"
    "messageReceived(ccappcore::Message,ccappcore::ClientAddress,ccappcore:"
    ":ClientAddress)\0"
    "routeError(ccappcore::Message)\0"
    "responseTimeout()\0"
};

void ccappcore::RequestReplyMessage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        RequestReplyMessage *_t = static_cast<RequestReplyMessage *>(_o);
        switch (_id) {
        case 0: _t->responseReceived((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 1: _t->error((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 2: _t->messageReceived((*reinterpret_cast< const ccappcore::Message(*)>(_a[1])),(*reinterpret_cast< const ccappcore::ClientAddress(*)>(_a[2])),(*reinterpret_cast< const ccappcore::ClientAddress(*)>(_a[3]))); break;
        case 3: _t->routeError((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 4: _t->responseTimeout(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ccappcore::RequestReplyMessage::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ccappcore::RequestReplyMessage::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ccappcore__RequestReplyMessage,
      qt_meta_data_ccappcore__RequestReplyMessage, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ccappcore::RequestReplyMessage::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ccappcore::RequestReplyMessage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ccappcore::RequestReplyMessage::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ccappcore__RequestReplyMessage))
        return static_cast<void*>(const_cast< RequestReplyMessage*>(this));
    return QObject::qt_metacast(_clname);
}

int ccappcore::RequestReplyMessage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void ccappcore::RequestReplyMessage::responseReceived(const ccappcore::Message & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ccappcore::RequestReplyMessage::error(const ccappcore::Message & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
