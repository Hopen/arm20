/****************************************************************************
** Meta object code from reading C++ file 'sessioncontroller.h'
**
** Created: Mon 25. Mar 16:55:43 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../sessioncontroller.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'sessioncontroller.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ccappcore__SessionController[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      28,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      30,   29,   29,   29, 0x05,
      47,   29,   29,   29, 0x05,
      77,   64,   29,   29, 0x05,

 // slots: signature, parameters, type, tag, flags
     141,  135,   29,   29, 0x0a,
     168,  159,   29,   29, 0x0a,
     189,   29,   29,   29, 0x0a,
     214,  207,   29,   29, 0x0a,
     233,  207,   29,   29, 0x0a,
     265,  258,   29,   29, 0x0a,
     303,  293,   29,   29, 0x0a,
     337,  329,   29,   29, 0x0a,
     363,   29,   29,   29, 0x0a,
     378,   29,   29,   29, 0x0a,
     392,   29,   29,   29, 0x09,
     410,   29,   29,   29, 0x09,
     433,   29,   29,   29, 0x09,
     455,   29,   29,   29, 0x09,
     474,   29,   29,   29, 0x09,
     493,   29,   29,   29, 0x09,
     506,   29,   29,   29, 0x09,
     528,   29,   29,   29, 0x09,
     545,   29,   29,   29, 0x09,
     558,   29,   29,   29, 0x09,
     579,  577,   29,   29, 0x09,
     619,  577,   29,   29, 0x09,
     657,   29,   29,   29, 0x09,
     683,  577,   29,   29, 0x09,
     726,   29,   29,   29, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_ccappcore__SessionController[] = {
    "ccappcore::SessionController\0\0"
    "sessionStarted()\0sessionStopped()\0"
    "reason,error\0"
    "sessionError(SessionController::SessionErrorCode,QString)\0"
    "login\0setLogin(QString)\0password\0"
    "setPassword(QString)\0encryptPassword()\0"
    "device\0setDeviceType(int)\0"
    "setDeviceNumber(QString)\0forced\0"
    "setForcedLoginEnabled(bool)\0autoLogin\0"
    "setAutoLoginEnabled(bool)\0useNtlm\0"
    "setNtlmLoginEnabled(bool)\0startSession()\0"
    "stopSession()\0connectToRouter()\0"
    "onRouterError(QString)\0onRouteMessageError()\0"
    "onSessionStarted()\0onSessionStopped()\0"
    "onLoadData()\0onLoadDataCompleted()\0"
    "onLoadProfiles()\0onLoadSpam()\0"
    "sendLoginRequest()\0m\0"
    "on_c2oLoginSucceded(ccappcore::Message)\0"
    "on_c2oLoginFailed(ccappcore::Message)\0"
    "setFreeStatusAfterLogon()\0"
    "on_c2oUpdateOperStatus(ccappcore::Message)\0"
    "setFreeStatus()\0"
};

void ccappcore::SessionController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SessionController *_t = static_cast<SessionController *>(_o);
        switch (_id) {
        case 0: _t->sessionStarted(); break;
        case 1: _t->sessionStopped(); break;
        case 2: _t->sessionError((*reinterpret_cast< SessionController::SessionErrorCode(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 3: _t->setLogin((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->setPassword((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->encryptPassword(); break;
        case 6: _t->setDeviceType((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->setDeviceNumber((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 8: _t->setForcedLoginEnabled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->setAutoLoginEnabled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->setNtlmLoginEnabled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: _t->startSession(); break;
        case 12: _t->stopSession(); break;
        case 13: _t->connectToRouter(); break;
        case 14: _t->onRouterError((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 15: _t->onRouteMessageError(); break;
        case 16: _t->onSessionStarted(); break;
        case 17: _t->onSessionStopped(); break;
        case 18: _t->onLoadData(); break;
        case 19: _t->onLoadDataCompleted(); break;
        case 20: _t->onLoadProfiles(); break;
        case 21: _t->onLoadSpam(); break;
        case 22: _t->sendLoginRequest(); break;
        case 23: _t->on_c2oLoginSucceded((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 24: _t->on_c2oLoginFailed((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 25: _t->setFreeStatusAfterLogon(); break;
        case 26: _t->on_c2oUpdateOperStatus((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 27: _t->setFreeStatus(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ccappcore::SessionController::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ccappcore::SessionController::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ccappcore__SessionController,
      qt_meta_data_ccappcore__SessionController, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ccappcore::SessionController::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ccappcore::SessionController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ccappcore::SessionController::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ccappcore__SessionController))
        return static_cast<void*>(const_cast< SessionController*>(this));
    if (!strcmp(_clname, "IAppSettingsHandler"))
        return static_cast< IAppSettingsHandler*>(const_cast< SessionController*>(this));
    if (!strcmp(_clname, "ru.forte-it.ccappcore.iappsettingshandler/1.0"))
        return static_cast< ccappcore::IAppSettingsHandler*>(const_cast< SessionController*>(this));
    return QObject::qt_metacast(_clname);
}

int ccappcore::SessionController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 28)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 28;
    }
    return _id;
}

// SIGNAL 0
void ccappcore::SessionController::sessionStarted()const
{
    QMetaObject::activate(const_cast< ccappcore::SessionController *>(this), &staticMetaObject, 0, 0);
}

// SIGNAL 1
void ccappcore::SessionController::sessionStopped()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void ccappcore::SessionController::sessionError(SessionController::SessionErrorCode _t1, const QString & _t2)const
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(const_cast< ccappcore::SessionController *>(this), &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
