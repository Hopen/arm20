/****************************************************************************
** Meta object code from reading C++ file 'spamlistcontroller.h'
**
** Created: Mon 25. Mar 16:55:16 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../spamlistcontroller.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'spamlistcontroller.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ccappcore__SpamListController[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      31,   30,   30,   30, 0x05,
      54,   46,   30,   30, 0x05,
      78,   46,   30,   30, 0x05,
     111,  104,   30,   30, 0x05,

 // slots: signature, parameters, type, tag, flags
     138,  136,   30,   30, 0x0a,
     171,  136,   30,   30, 0x0a,
     216,  136,   30,   30, 0x0a,
     250,   30,   30,   30, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ccappcore__SpamListController[] = {
    "ccappcore::SpamListController\0\0"
    "messageAdded()\0message\0newMessage(SpamMessage)\0"
    "chatMsgAdded(SpamMessage)\0operID\0"
    "updateContactStatus(int)\0m\0"
    "on_c2oBBList(ccappcore::Message)\0"
    "on_c2oNeed_o2c_BBListReq(ccappcore::Message)\0"
    "on_c2oSendMsg(ccappcore::Message)\0"
    "on_reloadSpamFinished()\0"
};

void ccappcore::SpamListController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SpamListController *_t = static_cast<SpamListController *>(_o);
        switch (_id) {
        case 0: _t->messageAdded(); break;
        case 1: _t->newMessage((*reinterpret_cast< SpamMessage(*)>(_a[1]))); break;
        case 2: _t->chatMsgAdded((*reinterpret_cast< SpamMessage(*)>(_a[1]))); break;
        case 3: _t->updateContactStatus((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 4: _t->on_c2oBBList((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 5: _t->on_c2oNeed_o2c_BBListReq((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 6: _t->on_c2oSendMsg((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 7: _t->on_reloadSpamFinished(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ccappcore::SpamListController::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ccappcore::SpamListController::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ccappcore__SpamListController,
      qt_meta_data_ccappcore__SpamListController, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ccappcore::SpamListController::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ccappcore::SpamListController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ccappcore::SpamListController::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ccappcore__SpamListController))
        return static_cast<void*>(const_cast< SpamListController*>(this));
    return QObject::qt_metacast(_clname);
}

int ccappcore::SpamListController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void ccappcore::SpamListController::messageAdded()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void ccappcore::SpamListController::newMessage(SpamMessage _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ccappcore::SpamListController::chatMsgAdded(SpamMessage _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void ccappcore::SpamListController::updateContactStatus(const int & _t1)const
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(const_cast< ccappcore::SpamListController *>(this), &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE
