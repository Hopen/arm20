/****************************************************************************
** Meta object code from reading C++ file 'operstatuslistmodel.h'
**
** Created: Mon 25. Mar 16:55:47 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../operstatuslistmodel.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'operstatuslistmodel.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ccappcore__OperStatusListModel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      32,   31,   31,   31, 0x0a,
      47,   40,   31,   31, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ccappcore__OperStatusListModel[] = {
    "ccappcore::OperStatusListModel\0\0reset()\0"
    "filter\0applyFilter(PredicateGroup<Contact>)\0"
};

void ccappcore::OperStatusListModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        OperStatusListModel *_t = static_cast<OperStatusListModel *>(_o);
        switch (_id) {
        case 0: _t->reset(); break;
        case 1: _t->applyFilter((*reinterpret_cast< const PredicateGroup<Contact>(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ccappcore::OperStatusListModel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ccappcore::OperStatusListModel::staticMetaObject = {
    { &QAbstractListModel::staticMetaObject, qt_meta_stringdata_ccappcore__OperStatusListModel,
      qt_meta_data_ccappcore__OperStatusListModel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ccappcore::OperStatusListModel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ccappcore::OperStatusListModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ccappcore::OperStatusListModel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ccappcore__OperStatusListModel))
        return static_cast<void*>(const_cast< OperStatusListModel*>(this));
    if (!strcmp(_clname, "FilteredModel<ccappcore::Contact>"))
        return static_cast< FilteredModel<ccappcore::Contact>*>(const_cast< OperStatusListModel*>(this));
    return QAbstractListModel::qt_metacast(_clname);
}

int ccappcore::OperStatusListModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractListModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
