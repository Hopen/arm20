/****************************************************************************
** Meta object code from reading C++ file 'ntlmauthentication.h'
**
** Created: Mon 25. Mar 16:55:34 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../ntlmauthentication.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ntlmauthentication.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ccappcore__INtlmLogonProvider[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      31,   30,   30,   30, 0x05,
      55,   30,   30,   30, 0x05,

 // slots: signature, parameters, type, tag, flags
      81,   30,   30,   30, 0x0a,
     111,   30,  103,   30, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ccappcore__INtlmLogonProvider[] = {
    "ccappcore::INtlmLogonProvider\0\0"
    "authenticationStarted()\0"
    "authenticationCompleted()\0"
    "startAuthentication()\0QString\0"
    "queryUserName()\0"
};

void ccappcore::INtlmLogonProvider::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        INtlmLogonProvider *_t = static_cast<INtlmLogonProvider *>(_o);
        switch (_id) {
        case 0: _t->authenticationStarted(); break;
        case 1: _t->authenticationCompleted(); break;
        case 2: _t->startAuthentication(); break;
        case 3: { QString _r = _t->queryUserName();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ccappcore::INtlmLogonProvider::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ccappcore::INtlmLogonProvider::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ccappcore__INtlmLogonProvider,
      qt_meta_data_ccappcore__INtlmLogonProvider, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ccappcore::INtlmLogonProvider::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ccappcore::INtlmLogonProvider::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ccappcore::INtlmLogonProvider::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ccappcore__INtlmLogonProvider))
        return static_cast<void*>(const_cast< INtlmLogonProvider*>(this));
    return QObject::qt_metacast(_clname);
}

int ccappcore::INtlmLogonProvider::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void ccappcore::INtlmLogonProvider::authenticationStarted()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void ccappcore::INtlmLogonProvider::authenticationCompleted()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
static const uint qt_meta_data_ccappcore__NTLMAuthentication[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      31,   30,   30,   30, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ccappcore__NTLMAuthentication[] = {
    "ccappcore::NTLMAuthentication\0\0"
    "on_c2oNtlmChallengeRequest(ccappcore::Message)\0"
};

void ccappcore::NTLMAuthentication::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        NTLMAuthentication *_t = static_cast<NTLMAuthentication *>(_o);
        switch (_id) {
        case 0: _t->on_c2oNtlmChallengeRequest((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ccappcore::NTLMAuthentication::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ccappcore::NTLMAuthentication::staticMetaObject = {
    { &INtlmLogonProvider::staticMetaObject, qt_meta_stringdata_ccappcore__NTLMAuthentication,
      qt_meta_data_ccappcore__NTLMAuthentication, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ccappcore::NTLMAuthentication::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ccappcore::NTLMAuthentication::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ccappcore::NTLMAuthentication::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ccappcore__NTLMAuthentication))
        return static_cast<void*>(const_cast< NTLMAuthentication*>(this));
    return INtlmLogonProvider::qt_metacast(_clname);
}

int ccappcore::NTLMAuthentication::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = INtlmLogonProvider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
