/****************************************************************************
** Meta object code from reading C++ file 'callslistmodel.h'
**
** Created: Mon 25. Mar 16:55:46 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../callslistmodel.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'callslistmodel.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ccappcore__CallsListModel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      32,   27,   26,   26, 0x08,
      57,   27,   26,   26, 0x08,
      85,   26,   26,   26, 0x08,
     119,   26,   26,   26, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ccappcore__CallsListModel[] = {
    "ccappcore::CallsListModel\0\0call\0"
    "addCall(ccappcore::Call)\0"
    "removeCall(ccappcore::Call)\0"
    "callStateChanged(ccappcore::Call)\0"
    "emitStateTimeChanged()\0"
};

void ccappcore::CallsListModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CallsListModel *_t = static_cast<CallsListModel *>(_o);
        switch (_id) {
        case 0: _t->addCall((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 1: _t->removeCall((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 2: _t->callStateChanged((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 3: _t->emitStateTimeChanged(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ccappcore::CallsListModel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ccappcore::CallsListModel::staticMetaObject = {
    { &QAbstractListModel::staticMetaObject, qt_meta_stringdata_ccappcore__CallsListModel,
      qt_meta_data_ccappcore__CallsListModel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ccappcore::CallsListModel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ccappcore::CallsListModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ccappcore::CallsListModel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ccappcore__CallsListModel))
        return static_cast<void*>(const_cast< CallsListModel*>(this));
    if (!strcmp(_clname, "FilteredModel<ccappcore::Call>"))
        return static_cast< FilteredModel<ccappcore::Call>*>(const_cast< CallsListModel*>(this));
    return QAbstractListModel::qt_metacast(_clname);
}

int ccappcore::CallsListModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractListModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
