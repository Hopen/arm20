/****************************************************************************
** Meta object code from reading C++ file 'soundeventsmanager.h'
**
** Created: Mon 25. Mar 16:55:29 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../soundeventsmanager.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'soundeventsmanager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ccappcore__SoundEvent[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      23,   22,   22,   22, 0x0a,
      30,   22,   22,   22, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ccappcore__SoundEvent[] = {
    "ccappcore::SoundEvent\0\0play()\0stop()\0"
};

void ccappcore::SoundEvent::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SoundEvent *_t = static_cast<SoundEvent *>(_o);
        switch (_id) {
        case 0: _t->play(); break;
        case 1: _t->stop(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData ccappcore::SoundEvent::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ccappcore::SoundEvent::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ccappcore__SoundEvent,
      qt_meta_data_ccappcore__SoundEvent, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ccappcore::SoundEvent::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ccappcore::SoundEvent::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ccappcore::SoundEvent::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ccappcore__SoundEvent))
        return static_cast<void*>(const_cast< SoundEvent*>(this));
    return QObject::qt_metacast(_clname);
}

int ccappcore::SoundEvent::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
static const uint qt_meta_data_ccappcore__SoundEventsManager[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_ccappcore__SoundEventsManager[] = {
    "ccappcore::SoundEventsManager\0"
};

void ccappcore::SoundEventsManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData ccappcore::SoundEventsManager::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ccappcore::SoundEventsManager::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ccappcore__SoundEventsManager,
      qt_meta_data_ccappcore__SoundEventsManager, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ccappcore::SoundEventsManager::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ccappcore::SoundEventsManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ccappcore::SoundEventsManager::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ccappcore__SoundEventsManager))
        return static_cast<void*>(const_cast< SoundEventsManager*>(this));
    if (!strcmp(_clname, "IAppSettingsHandler"))
        return static_cast< IAppSettingsHandler*>(const_cast< SoundEventsManager*>(this));
    if (!strcmp(_clname, "ru.forte-it.ccappcore.iappsettingshandler/1.0"))
        return static_cast< ccappcore::IAppSettingsHandler*>(const_cast< SoundEventsManager*>(this));
    return QObject::qt_metacast(_clname);
}

int ccappcore::SoundEventsManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
