/****************************************************************************
** Meta object code from reading C++ file 'callscontroller.h'
**
** Created: Mon 25. Mar 16:55:44 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../callscontroller.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'callscontroller.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ccappcore__CallsController[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      60,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      18,       // signalCount

 // signals: signature, parameters, type, tag, flags
      28,   27,   27,   27, 0x05,
      55,   27,   27,   27, 0x05,
      89,   27,   27,   27, 0x05,
     122,   27,   27,   27, 0x05,
     151,   27,   27,   27, 0x05,
     183,   27,   27,   27, 0x05,
     217,   27,   27,   27, 0x05,
     256,   27,   27,   27, 0x05,
     286,   27,   27,   27, 0x05,
     315,   27,   27,   27, 0x05,
     336,   27,   27,   27, 0x05,
     356,   27,   27,   27, 0x05,
     396,  387,   27,   27, 0x05,
     460,   27,   27,   27, 0x05,
     500,  487,   27,   27, 0x05,
     530,   27,   27,   27, 0x05,
     537,   27,   27,   27, 0x05,
     588,  570,   27,   27, 0x05,

 // slots: signature, parameters, type, tag, flags
     631,  626,   27,   27, 0x0a,
     665,  659,   27,   27, 0x0a,
     694,  683,   27,   27, 0x0a,
     731,  720,   27,   27, 0x0a,
     762,  720,   27,   27, 0x0a,
     793,  720,   27,   27, 0x0a,
     826,  626,   27,   27, 0x0a,
     858,  851,   27,   27, 0x0a,
     903,  899,   27,   27, 0x0a,
     965,  958,   27,   27, 0x0a,
    1029, 1017,   27,   27, 0x0a,
    1091, 1075,   27,   27, 0x0a,
    1123, 1113,   27,   27, 0x0a,
    1165, 1162,   27,   27, 0x0a,
    1223, 1221,   27,   27, 0x0a,
    1263, 1259,   27,   27, 0x0a,
    1293, 1278,   27,   27, 0x0a,
    1351, 1328,   27,   27, 0x0a,
    1403, 1395,   27,   27, 0x0a,
    1446, 1430,   27,   27, 0x0a,
    1476, 1430,   27,   27, 0x0a,
    1510, 1395,   27,   27, 0x0a,
    1532,   27,   27,   27, 0x08,
    1568,   27,   27,   27, 0x0a,
    1604,   27,   27,   27, 0x08,
    1646,   27,   27,   27, 0x08,
    1681,   27,   27,   27, 0x08,
    1720,   27,   27,   27, 0x08,
    1759,   27,   27,   27, 0x08,
    1796,   27,   27,   27, 0x08,
    1830,   27,   27,   27, 0x08,
    1868,   27,   27,   27, 0x08,
    1899,   27,   27,   27, 0x08,
    1930,   27,   27,   27, 0x08,
    1961,   27,   27,   27, 0x08,
    1994,   27,   27,   27, 0x08,
    2029,   27,   27,   27, 0x08,
    2067,   27,   27,   27, 0x08,
    2104,   27,   27,   27, 0x08,
    2141,   27,   27,   27, 0x08,
    2178,   27,   27,   27, 0x08,
    2219,   27,   27,   27, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ccappcore__CallsController[] = {
    "ccappcore::CallsController\0\0"
    "callAdded(ccappcore::Call)\0"
    "callStateChanged(ccappcore::Call)\0"
    "callInfoChanged(ccappcore::Call)\0"
    "callRemoved(ccappcore::Call)\0"
    "queueCallAdded(ccappcore::Call)\0"
    "queueCallRemoved(ccappcore::Call)\0"
    "privateQueueCallAdded(ccappcore::Call)\0"
    "callAssigned(ccappcore::Call)\0"
    "callStarted(ccappcore::Call)\0"
    "callOutRingStarted()\0callInRingStarted()\0"
    "callConnected(ccappcore::Call)\0,,reason\0"
    "callFailed(ccappcore::Call,ccappcore::Call::CallResult,QString)\0"
    "callEnded(ccappcore::Call)\0callid,error\0"
    "joinCallToConfFailed(int,int)\0hung()\0"
    "lastCallChanged(ccappcore::Call)\0"
    "call,onOff,forced\0"
    "recordCall(ccappcore::Call,bool,bool)\0"
    "call\0answerCall(ccappcore::Call)\0phone\0"
    "makeCall(QString)\0recentCall\0"
    "makeCall(ccappcore::Call)\0call,onOff\0"
    "holdCall(ccappcore::Call,bool)\0"
    "muteCall(ccappcore::Call,bool)\0"
    "recordCall(ccappcore::Call,bool)\0"
    "endCall(ccappcore::Call)\0,phone\0"
    "transferToPhone(ccappcore::Call,QString)\0"
    ",op\0transferToOperator(ccappcore::Call,ccappcore::Contact)\0"
    ",group\0transferToGroup(ccappcore::Call,ccappcore::Contact)\0"
    "call1,call2\0connectCalls(ccappcore::Call,ccappcore::Call)\0"
    "callId1,callId2\0connectCalls(int,int)\0"
    ",funcName\0transferToIVR(ccappcore::Call,QString)\0"
    ",,\0addCallInfo(ccappcore::Call,Call::CallInfoType,QString)\0"
    ",\0setSubject(ccappcore::Call,QString)\0"
    "url\0makeCall(QUrl)\0callsToConnect\0"
    "createConference(CallIDCollector*)\0"
    "callsToConnect,conf_id\0"
    "joinCallsToConference(CallIDCollector*,int)\0"
    "conf_id\0joinCallsToConference(int)\0"
    "conf_id,call_id\0joinCallToConference(int,int)\0"
    "unjoinCallFormConference(int,int)\0"
    "removeConference(int)\0"
    "on_c2oQueueList(ccappcore::Message)\0"
    "on_c2oQueueCall(ccappcore::Message)\0"
    "on_c2oRemoveQueueCall(ccappcore::Message)\0"
    "on_c2oInitCall(ccappcore::Message)\0"
    "on_c2oCallAssigned(ccappcore::Message)\0"
    "on_c2oIncomingCall(ccappcore::Message)\0"
    "on_c2oCallResult(ccappcore::Message)\0"
    "on_c2oEndCall(ccappcore::Message)\0"
    "on_c2oRingStarted(ccappcore::Message)\0"
    "on_c2oHung(ccappcore::Message)\0"
    "on_c2oHold(ccappcore::Message)\0"
    "on_c2oMute(ccappcore::Message)\0"
    "on_c2oRecord(ccappcore::Message)\0"
    "on_c2oCallInfo(ccappcore::Message)\0"
    "on_c2oCallHistory(ccappcore::Message)\0"
    "on_c2oBindPerson(ccappcore::Message)\0"
    "on_c2oCreateConf(ccappcore::Message)\0"
    "on_c2oJoinToConf(ccappcore::Message)\0"
    "on_c2oRemoveFromConf(ccappcore::Message)\0"
    "on_c2oRemoveConf(ccappcore::Message)\0"
};

void ccappcore::CallsController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CallsController *_t = static_cast<CallsController *>(_o);
        switch (_id) {
        case 0: _t->callAdded((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 1: _t->callStateChanged((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 2: _t->callInfoChanged((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 3: _t->callRemoved((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 4: _t->queueCallAdded((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 5: _t->queueCallRemoved((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 6: _t->privateQueueCallAdded((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 7: _t->callAssigned((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 8: _t->callStarted((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 9: _t->callOutRingStarted(); break;
        case 10: _t->callInRingStarted(); break;
        case 11: _t->callConnected((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 12: _t->callFailed((*reinterpret_cast< ccappcore::Call(*)>(_a[1])),(*reinterpret_cast< ccappcore::Call::CallResult(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 13: _t->callEnded((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 14: _t->joinCallToConfFailed((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 15: _t->hung(); break;
        case 16: _t->lastCallChanged((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 17: _t->recordCall((*reinterpret_cast< ccappcore::Call(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        case 18: _t->answerCall((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 19: _t->makeCall((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 20: _t->makeCall((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 21: _t->holdCall((*reinterpret_cast< ccappcore::Call(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 22: _t->muteCall((*reinterpret_cast< ccappcore::Call(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 23: _t->recordCall((*reinterpret_cast< ccappcore::Call(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 24: _t->endCall((*reinterpret_cast< ccappcore::Call(*)>(_a[1]))); break;
        case 25: _t->transferToPhone((*reinterpret_cast< ccappcore::Call(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 26: _t->transferToOperator((*reinterpret_cast< ccappcore::Call(*)>(_a[1])),(*reinterpret_cast< ccappcore::Contact(*)>(_a[2]))); break;
        case 27: _t->transferToGroup((*reinterpret_cast< ccappcore::Call(*)>(_a[1])),(*reinterpret_cast< ccappcore::Contact(*)>(_a[2]))); break;
        case 28: _t->connectCalls((*reinterpret_cast< ccappcore::Call(*)>(_a[1])),(*reinterpret_cast< ccappcore::Call(*)>(_a[2]))); break;
        case 29: _t->connectCalls((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 30: _t->transferToIVR((*reinterpret_cast< ccappcore::Call(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 31: _t->addCallInfo((*reinterpret_cast< ccappcore::Call(*)>(_a[1])),(*reinterpret_cast< Call::CallInfoType(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 32: _t->setSubject((*reinterpret_cast< ccappcore::Call(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 33: _t->makeCall((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 34: _t->createConference((*reinterpret_cast< CallIDCollector*(*)>(_a[1]))); break;
        case 35: _t->joinCallsToConference((*reinterpret_cast< CallIDCollector*(*)>(_a[1])),(*reinterpret_cast< const int(*)>(_a[2]))); break;
        case 36: _t->joinCallsToConference((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 37: _t->joinCallToConference((*reinterpret_cast< const int(*)>(_a[1])),(*reinterpret_cast< const int(*)>(_a[2]))); break;
        case 38: _t->unjoinCallFormConference((*reinterpret_cast< const int(*)>(_a[1])),(*reinterpret_cast< const int(*)>(_a[2]))); break;
        case 39: _t->removeConference((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 40: _t->on_c2oQueueList((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 41: _t->on_c2oQueueCall((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 42: _t->on_c2oRemoveQueueCall((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 43: _t->on_c2oInitCall((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 44: _t->on_c2oCallAssigned((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 45: _t->on_c2oIncomingCall((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 46: _t->on_c2oCallResult((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 47: _t->on_c2oEndCall((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 48: _t->on_c2oRingStarted((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 49: _t->on_c2oHung((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 50: _t->on_c2oHold((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 51: _t->on_c2oMute((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 52: _t->on_c2oRecord((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 53: _t->on_c2oCallInfo((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 54: _t->on_c2oCallHistory((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 55: _t->on_c2oBindPerson((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 56: _t->on_c2oCreateConf((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 57: _t->on_c2oJoinToConf((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 58: _t->on_c2oRemoveFromConf((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        case 59: _t->on_c2oRemoveConf((*reinterpret_cast< const ccappcore::Message(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ccappcore::CallsController::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ccappcore::CallsController::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ccappcore__CallsController,
      qt_meta_data_ccappcore__CallsController, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ccappcore::CallsController::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ccappcore::CallsController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ccappcore::CallsController::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ccappcore__CallsController))
        return static_cast<void*>(const_cast< CallsController*>(this));
    return QObject::qt_metacast(_clname);
}

int ccappcore::CallsController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 60)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 60;
    }
    return _id;
}

// SIGNAL 0
void ccappcore::CallsController::callAdded(ccappcore::Call _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ccappcore::CallsController::callStateChanged(ccappcore::Call _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ccappcore::CallsController::callInfoChanged(ccappcore::Call _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void ccappcore::CallsController::callRemoved(ccappcore::Call _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void ccappcore::CallsController::queueCallAdded(ccappcore::Call _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void ccappcore::CallsController::queueCallRemoved(ccappcore::Call _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void ccappcore::CallsController::privateQueueCallAdded(ccappcore::Call _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void ccappcore::CallsController::callAssigned(ccappcore::Call _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void ccappcore::CallsController::callStarted(ccappcore::Call _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void ccappcore::CallsController::callOutRingStarted()
{
    QMetaObject::activate(this, &staticMetaObject, 9, 0);
}

// SIGNAL 10
void ccappcore::CallsController::callInRingStarted()
{
    QMetaObject::activate(this, &staticMetaObject, 10, 0);
}

// SIGNAL 11
void ccappcore::CallsController::callConnected(ccappcore::Call _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void ccappcore::CallsController::callFailed(ccappcore::Call _t1, ccappcore::Call::CallResult _t2, const QString & _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}

// SIGNAL 13
void ccappcore::CallsController::callEnded(ccappcore::Call _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}

// SIGNAL 14
void ccappcore::CallsController::joinCallToConfFailed(int _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 14, _a);
}

// SIGNAL 15
void ccappcore::CallsController::hung()
{
    QMetaObject::activate(this, &staticMetaObject, 15, 0);
}

// SIGNAL 16
void ccappcore::CallsController::lastCallChanged(ccappcore::Call _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 16, _a);
}

// SIGNAL 17
void ccappcore::CallsController::recordCall(ccappcore::Call _t1, bool _t2, bool _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 17, _a);
}
QT_END_MOC_NAMESPACE
