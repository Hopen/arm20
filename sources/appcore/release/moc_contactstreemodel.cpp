/****************************************************************************
** Meta object code from reading C++ file 'contactstreemodel.h'
**
** Created: Mon 25. Mar 16:55:40 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../contactstreemodel.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'contactstreemodel.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ccappcore__ContactsTreeModel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      30,   29,   29,   29, 0x0a,
      49,   38,   29,   29, 0x0a,
      70,   29,   29,   29, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ccappcore__ContactsTreeModel[] = {
    "ccappcore::ContactsTreeModel\0\0reset()\0"
    "searchText\0searchAsync(QString)\0"
    "searchCompleted()\0"
};

void ccappcore::ContactsTreeModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ContactsTreeModel *_t = static_cast<ContactsTreeModel *>(_o);
        switch (_id) {
        case 0: _t->reset(); break;
        case 1: _t->searchAsync((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->searchCompleted(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ccappcore::ContactsTreeModel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ccappcore::ContactsTreeModel::staticMetaObject = {
    { &QAbstractItemModel::staticMetaObject, qt_meta_stringdata_ccappcore__ContactsTreeModel,
      qt_meta_data_ccappcore__ContactsTreeModel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ccappcore::ContactsTreeModel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ccappcore::ContactsTreeModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ccappcore::ContactsTreeModel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ccappcore__ContactsTreeModel))
        return static_cast<void*>(const_cast< ContactsTreeModel*>(this));
    if (!strcmp(_clname, "FilteredModel<Contact>"))
        return static_cast< FilteredModel<Contact>*>(const_cast< ContactsTreeModel*>(this));
    return QAbstractItemModel::qt_metacast(_clname);
}

int ccappcore::ContactsTreeModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractItemModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
