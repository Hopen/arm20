#pragma once

#include "sspiwrapper_defs.h"

using std::tr1::shared_ptr;
using std::tr1::enable_shared_from_this;

namespace SSPIWrapper
{

typedef std::vector<BYTE> ByteArray;

class SSPIException : public std::exception
{
public:
	int errorCode() const { return _errorCode; }

	SSPIException(const char*, int errorCode);

private:
	const char* _what;
	int			_errorCode;
};

class SecurityPackage
{
public:

	static const wchar_t* szNegotiatePackageName;
	static const wchar_t* szKerberosPackageName;
	static const wchar_t* szNTLMPackageName;
	enum Package {Negotiate, Kerberos, NTLM}; 

	const wchar_t* packageName() const { return _packageName.c_str(); }
	PSecurityFunctionTableW functionTable() { return _secFunctionTable; }


	// constructor/destructor
	SecurityPackage(const wchar_t* packageName);

	// methods
	std::list<std::wstring> queryAllSecurityPackages();

private: 
	std::wstring			_packageName;
	PSecurityFunctionTableW _secFunctionTable;
};

typedef shared_ptr<SecurityPackage> SecurityPackagePtr;

class Credential
{
public:
	enum CredentialType {Client, Server};
	virtual ~Credential();
	
	SecurityPackagePtr securityPackage() const { return _securityPackage; }
	CredHandle* handle() { return &*_credentialHandle; }
	std::wstring queryUserNameAttribute();;

protected:
	Credential(SecurityPackagePtr package, CredentialType credentialType);

protected:
	// member variables
        SecurityPackagePtr	_securityPackage;
        PSecurityFunctionTableW _securityFunctionTable;
	shared_ptr<CredHandle>  _credentialHandle;	
};

typedef shared_ptr<Credential> CredentialPtr;

class ClientCredential : public Credential
{
public:

	// constructor/destructor
	ClientCredential(SecurityPackagePtr package) : Credential(package, Credential::Client)
	{
	}
};

typedef shared_ptr<ClientCredential> ClientCredentialPtr;

class ServerCredential : public Credential
{
public:

	// constructor/destructor
	ServerCredential(SecurityPackagePtr package) : Credential(package, Credential::Server)
	{
	}
};

typedef shared_ptr<ServerCredential> ServerCredentialPtr;

class Context 
{
protected:
	Context(shared_ptr<Credential> credential);
	virtual ~Context();

public:
	ByteArray signMessage(const ByteArray& msg);
	ByteArray verifyMessage(const ByteArray& signedMsg);
	ByteArray encryptMessage(const ByteArray& msg);
	ByteArray decryptMessage(const ByteArray& encryptedMsg);

protected:
	ByteArray secBufferToByteArray(SecBuffer &outSecBuff);

public:
	virtual std::list<std::wstring> contextAttributes();
	std::wstring queryUserNameAttribute();;
	std::wstring queryPackageNameAttribute();;

	Credential* credential() { return &*_credential;}
	const ByteArray& token() const { return _token; }
	bool continueProcessing() const { return _continueProcessing; }

protected:
	shared_ptr<SecurityPackage> _securityPackage;
	shared_ptr<Credential>  _credential;
	shared_ptr<CtxtHandle>	_contextHandle;
	ByteArray				_token;
	bool					_continueProcessing;
	ULONG					_contextAttributes;
};

class ClientContext : public Context
{
public:
	enum ContextAttributeFlags
	{
		None						= 0,
		Delegate					= 1,
		Identify					= 2,
		MutualAuthentication		= 4
	};

	ClientContext(ClientCredentialPtr credential, const std::wstring& serverPrincipal, ContextAttributeFlags contextAttributeFlags);

	void initialize(const ByteArray& inToken);
	virtual std::list<std::wstring> contextAttributes();

private:

	// data members
	std::wstring	_serverPrincipalName;
};

typedef shared_ptr<ClientContext> ClientContextPtr;

class ServerContext : public Context
{
public:

	// constructor/destructor
	ServerContext(ServerCredentialPtr credential, const ByteArray& inToken);

	std::list<std::wstring> contextAttributes();;

	void accept(const ByteArray& inToken);
	void impersonateClient(void);
	void revertImpersonation(void);

	HANDLE queryClientSecurityToken();;
};

typedef shared_ptr<ServerContext> ServerContextPtr;

} //namespace SSPIWrapper
