#include "contactstreemodel.h"
#include "useritemdatarole.h"
#include <QIcon>
#include <QtAlgorithms>

namespace ccappcore
{

ContactsTreeModel::ContactsTreeModel(QObject *parent)
    : QAbstractItemModel(parent)
    , _forceListMode(false)
    , _internalId(0)
{
    _columns<<Contact::Name;
    _columns<<Contact::Status;
    _columns<<Contact::Phone;
    _columns<<Contact::StatusReason;
    _columns<<Contact::StatusTime;

    connect(&_searchResultWatcher, SIGNAL(finished()), this, SLOT(searchCompleted()));
}

void ContactsTreeModel::searchAsync(const QString& searchText)
{
    if(_searchText.isEmpty() && !searchText.isEmpty())
        _contacts.clear();
    _searchText = searchText;
    if(_searchText.isEmpty())
    {
        reset();
        return;
    }

    QFuture<Contact> searchResult = controller()->searchContactsAsync(searchText);
        _searchResultWatcher.setFuture(searchResult);
}

void ContactsTreeModel::searchCompleted()
{
    applyFilter(_filter);
}

void ContactsTreeModel::setController(QPointer<ContactsController> controller)
{
    disconnect(this);
    _controller = controller;

    connect(_controller.data(),
            SIGNAL(dataChanged()),
            this,
            SLOT(reset()),
            Qt::QueuedConnection);

    connect(_controller.data(),
            SIGNAL(contactStatusChanged(ccappcore::Contact)),
            this,
            SIGNAL(layoutChanged()),
            Qt::QueuedConnection);

    applyFilter(_filter);
}

bool ContactsTreeModel::isListMode() const
{
    return _forceListMode || !_searchText.isEmpty();
}

void ContactsTreeModel::setListMode(bool val)
{
    _forceListMode = val;
    applyFilter(_filter);
}

void ContactsTreeModel::setColumns(const QList<int>& cols)
{
    emit layoutAboutToBeChanged();
    _columns = cols;
    emit layoutChanged();
}


QModelIndex ContactsTreeModel::index ( int row, int column, const QModelIndex & parent ) const
{
    QReadLocker lock(controller()->syncRoot());
    Q_UNUSED(lock);

    if(parent.isValid() && _forceListMode)
        return QModelIndex();

    if( row<0 || column<0 || (parent.model() && parent.model() != this) )
        return QModelIndex();
    QModelIndex index;
    if(!parent.isValid())
    {
        if( row >= _contacts.count() )
            return QModelIndex();

        index = createIndex(row, column, 0);
    }
    else
    {
        const Contact& parentContact = contactByIndex(parent);
        if( !parentContact.isValid() )
            return QModelIndex();

        if( row>=parentContact.children().count() )
            return QModelIndex();

        int parentId = _indexMap.key(parent,0);
        Q_ASSERT(parentId);
        index = createIndex(row, column, parentId);
    }

    int indexId = _indexMap.key(index,0);
    if(!indexId)
        _indexMap[++_internalId] = index;
    return index;
}

QModelIndex ContactsTreeModel::parent ( const QModelIndex & index ) const
{
    if(_forceListMode)
        return QModelIndex();

    if(!index.isValid())
        return QModelIndex();

    if(!index.internalId())
        return QModelIndex();

    Q_ASSERT(_indexMap.contains(index.internalId()));
    return _indexMap[index.internalId()];
}

int ContactsTreeModel::rowCount(const QModelIndex& parent) const
{
    QReadLocker lock(controller()->syncRoot());
    Q_UNUSED(lock);
    if(_forceListMode && parent.isValid())
        return 0;

    if(!parent.isValid())
        return _contacts.count();

    const Contact& parentContact = contactByIndex(parent);
    if( !parentContact.isValid() )
        return 0;

    if(parentContact.isContainer() && !parentContact.childrenLoaded() && _searchText.isEmpty())
        parentContact.controller()->loadChildrenAsync(parentContact);

    const QList<Contact>& children = parentContact.children();
    return filtered(children).count();
}

int ContactsTreeModel::columnCount(const QModelIndex&) const
{
    return _columns.count();
}

QVariant ContactsTreeModel::data(const QModelIndex& index, int role) const
{
    QReadLocker lock(controller()->syncRoot());
    Q_UNUSED(lock);

    Contact c = contactByIndex(index);
    if(!c.isValid() || index.column() <0 || index.column() >= columns().count() )
        return QVariant();

    if(role == AssociatedObjectRole)
        return QVariant::fromValue(c);

    if(role == StateTimeRole && c.statusTime().isValid() )
        return c.statusTime();

    if(role == StateReasonRole )
        return c.statusReason();

    Contact::ContactInfo dt = (Contact::ContactInfo)_columns[index.column()];
    if(role == Qt::DisplayRole)
        return c.data(dt);

    if(!c.isValid())
        return QVariant();

    return c.controller()->modelData(index, c, role);
}

//QVariant ContactsTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
QVariant ContactsTreeModel::headerData(int section, Qt::Orientation orientation,
                            int role ) const
{
    if(orientation!=Qt::Horizontal)
        return QVariant();

    if(section<0 || section >= columns().count() )
        return QVariant();

    if(role != Qt::DisplayRole)
        return QVariant();

    return _controller->infoDisplayName(_columns.at(section));
}

void ContactsTreeModel::reset()
{    
    applyFilter(_filter);
}

void ContactsTreeModel::applyFilter(const PredicateGroup<Contact>& filter)
{
    if(!controller())
        return;

    emit layoutAboutToBeChanged();

    QReadLocker lock(controller()->syncRoot());
    Q_UNUSED(lock);

    _filter = filter;
    _contacts.clear();

    //display search results if search text is not empty
    if(!_searchText.isEmpty())
    {
        QList<Contact> searchResult;
        foreach(Contact contact, _searchResultWatcher.future())
            searchResult.append(contact);
        _contacts = filtered(searchResult);
    }
    //display all contacts as tree
    else if(!isListMode())
    {
        _contacts = filtered(controller()->contacts());
    }
    //display all contacts as list
    else
    {
        //generate unique contacts list
        QList<Contact> uniqueList;
        foreach(Contact rootContact, controller()->contacts())
        {
            QQueue<Contact> stack;
            stack.enqueue(rootContact);
            while(stack.count()>0)
            {
                Contact current = stack.dequeue();
                if( !uniqueList.contains(current) )
                    uniqueList.append(current);

                foreach(Contact c, current.children())
                    stack.enqueue(c);
            }
        }
        _contacts = filtered(uniqueList);
    }

    //sort by: container -> name
    qSort(_contacts);
    emit layoutChanged();
}

QList<Contact> ContactsTreeModel::filtered(const QList<Contact>& source) const
{    
    QList<Contact> result;
    foreach(const Contact& c, source)
        if(_filter.matches(c))
            result.append(c);
    return result;
}

Contact ContactsTreeModel::contactByIndex(const QModelIndex& index) const
{
    if(!index.isValid())
        return Contact::invalid();

    QList<Contact> contacts;
    if(!index.parent().isValid())
    {
        contacts = _contacts;
    }
    else
    {
        const Contact& parent = contactByIndex(index.parent());
        Q_ASSERT( parent.isValid() );
        //Q_ASSERT( index.row() < parent.children().count() );
        contacts = filtered(parent.children());
    }
    if(index.row()<0 || index.row() >= contacts.count())
        return Contact::invalid();
    const Contact& result = contacts.at(index.row());
    return result;
}



}
