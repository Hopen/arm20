#ifndef CALL_H
#define CALL_H

#include "appcore.h"
#include "contact.h"
#include "lateboundobject.h"

namespace ccappcore
{
class CallsController;
class CallInfo;

class APPCORE_EXPORT Call
{
public:
    enum CallInfoType
    {
        CI_Unknown                      = 0,   //invalid call info type
        CI_QueueCode                    = 1,   // GC:GroupCode or EXT:EXTENSIONS OR LQ:
        CI_External                     = 2,   //
        CI_QueueDescription             = 3,   // queue description
        CI_AdditionalTextInfo           = 4,   // additional text info (auto generated + manual by operator)
        CI_CallbackNumber               = 5,   // callback number entered by abonent
        CI_AbonName                     = 6,   // abonent name if it was automatically detected
        CI_CallQueued                   = 11,  // call queued
        CI_CallAssigned                 = 12,  // assigned to operator
        CI_CallConnected                = 13,  // connected with operator
        CI_CallEnded                    = 27,  // connected with operator
        CI_CallRecordOn                 = 20,  // recording started
        CI_IVRMenuItemIn                = 64,  // name of the IVR menu item (when abonent enters into it)
        CI_IVRMenuItemOut               = 65,  // name of the IVR menu item (when abonent exits from it)
        CI_QuestCode                    = 79,  // code of the quest form, associated with the call
        CI_ScriptFuncName               = 87,  // name of the script function, the call can be transferred
        CI_RegionName                   = 88,  // name of the abonent region
        CI_CallExtraType                = 90,  // type of call
        CI_IsVirtualCall                = 106, // virtual incoming
        CI_Subject                      = 107, // subject        CI_
        CI_CallFormHeader               = 700, // call form header html
        CI_CallFormFooter               = 701, // call form footer html
        CI_ForcedWebPage                = 702  // open webpage immediately
    };

    enum CallState
    {
        CS_Unknown,
        CS_Ivr,
        CS_Queued,
        CS_Assigned,
        CS_Calling,
        CS_RingStarted,
        CS_Connected,
        CS_Muted,
        CS_OnHold,
        CS_Conferencing,
        CS_CallFailed,
        CS_Completed
    };

    enum CallType
    {
        CT_Unknown,
        CT_Incoming,
        CT_Outgoing,
        CT_Operator,
    };

    enum CallResult
    {
        CR_Succeded = 1,
        CR_Busy = -1,
        CR_NoAnswer = -2,
        CR_NotExists = -3,
    };

    typedef QMultiMap<CallInfoType,CallInfo> ExtraInfo;
    typedef QList    <CallInfo             > MultiInfo;
public:

    Call();
    ~Call();

    int callId() const { return d->callId; }
    ccappcore::CallsController& controller() const { return *d->controller; }

    static Call invalid() { return Call(); }
    bool isValid() const { return callId()>=0 && d->controller!=NULL; }

    const QString& aNumber() const { return d->anum; }
    const QString& bNumber() const { return d->bnum; }

    CallType callType() const { return d->callType; }
    QString callTypeString() const { return callTypeString(callType()); }
    static QString callTypeString(CallType);

    CallState callState() const { return d->callState; }
    QString callStateString() const;
    static QString callResultString(CallResult);
    const QDateTime& stateTime() const { return d->stateTime; }
    const QString& stateReason() const { return d->stateReason; }

    bool isIncoming() const { return callType() == CT_Incoming; }
    bool isOutgoing() const { return callType() == CT_Outgoing; }
    bool isOperator() const { return callType() == CT_Operator; }

    bool isAssigned() const { return callState() == CS_Assigned || callState() == Call::CS_RingStarted; }
    bool isQueued() const { return callState() == CS_Queued; }
    bool isTalking() const { return callState() == CS_Connected; }

    bool isOnHold() const { return callState() == CS_OnHold; }
    bool isMuted() const { return callState() == CS_Muted; }
    bool isRecording() const { return d->isRecording; }
    bool isForcedRecording()const {return d->isForcedRecoding;}

    bool isCompleted() const { return callState() >= CS_CallFailed; }

    int priority() const { return d->priority; }
    ContactId groupId() const { return d->groupId; }
    
    ContactId operId() const { return d->operId; }
    int operCallId() const { return d->operCallId; }
    
    const QDateTime& beginDateTime() const { return d->begin; }
    const QDateTime& localBeginDateTime()const { return d->local_begin;}
    const QString& callExtraInfo() const { return d->callNotes; }
    
    bool canReject() const { return d->canReject; }

    bool operator == (const Call& other) const { return callId() == other.callId(); }
    bool operator != (const Call& other) const { return callId() != other.callId(); }
    bool operator < (const Call& other) const;
    const Call::ExtraInfo& extraInfo() const { return d->callInfo; }
    QString extraInfo(CallInfoType) const;
    bool extraInfo (const CallInfoType&, MultiInfo& info)const;

    const ccappcore::Contact& contact() const { return d->contactLink; }

    int confId() const { return d->confId; }

private:

    friend class CallsController;

    Call(CallsController* controller, int callId, CallType);
    void setCallId(CallsController* controller, int callId);
    void setANumber(const QString& anum) { d->anum = anum; }
    void setBNumber(const QString& bnum) { d->bnum = bnum; }
    void setCallType(CallType callType) { d->callType = callType; }
    void setCallState(CallState callState);

    void setStateTime(const QDateTime& dt) { d->stateTime = dt; }
    void setRecording(bool yesNo) { d->isRecording = yesNo; }
    void setForcedRecording(bool yesNo){d->isForcedRecoding = yesNo;}
    void setStateReason(const QString& stateReason) { d->stateReason = stateReason; }
    void setPriority(int value) { d->priority = value; }
    void setGroupId(ContactId groupId);
    void setOperId(ContactId id);// { d->operId = id; }
    void setOperCallId(int operCallId) { d->operCallId = operCallId; }
    void setBegin(const QDateTime& value) { d->begin = value; }
    void setComments(const QString& value) { d->callNotes = value; }
    void canReject(bool arg) { d->canReject = arg; }

    void clearExtraInfo() { d->callInfo.clear(); }
    ccappcore::CallInfo addExtraInfo(CallInfoType ci, QDateTime timeAdded, const QString& value, Contact whoAdded);
    void removeExtraInfo(CallInfoType ci);

    void setContact(ccappcore::Contact c) { d->contactLink = c; }

    void setConfId(const int& id) {d->confId = id;}

    struct Private {
    Private(CallsController* controller, int callId, CallType callType)
            : controller(controller)
            , callId(callId)
            , callType(callType)
            , callState(CS_Unknown)
            , priority(0)
            , operCallId(-1)
            , begin(QDateTime::currentDateTime())
            , local_begin(QDateTime::currentDateTime())
            , canReject(false)
            , isRecording(false)
            , isForcedRecoding(false)
            , confId(-1)
    {}
    CallsController* controller;
    int callId;
    QString anum;
    QString bnum;
    CallType callType;
    CallState callState;
    QString stateReason;
    int priority;
    ContactId groupId;
    ContactId operId;
    int operCallId;
    QDateTime begin;
    QDateTime local_begin;
    QString callNotes;
    bool canReject;
    bool isRecording;
    bool isForcedRecoding;
    QDateTime stateTime;
    ExtraInfo callInfo;
    ccappcore::Contact contactLink;
    int confId;
    };

    QSharedPointer<class Call::Private> d;
};

class APPCORE_EXPORT CallInfo
{
public:

    CallInfo();
    CallInfo(Call::CallInfoType ciType,
             const QString info,
             Contact whoAdded,
             const QDateTime& dtAdded);

    bool isValid() const { return _type>=0; }

    Call::CallInfoType infoType() const { return _type; }
    const QString& value() const { return _value; }
    const QDateTime& timeAdded() const { return _timeAdded; }
    Contact contactLink() const { return _whoAdded; }

private:
    Call::CallInfoType _type;
    QString _value;
    QDateTime _timeAdded;
    Contact _whoAdded;
};

}


Q_DECLARE_METATYPE(ccappcore::Call);

#endif // CALL_H
