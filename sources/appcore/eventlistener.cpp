#include "eventlistener.h"

namespace ccappcore
{

EventListener::EventListener()
        : _handler(NULL), _router(NULL), _subscribe(false), _started(false)
{
}

void EventListener::startListening(IRouterClient* routerClient,
                                   const Message& prototype,
                                   QObject* handler, const char* slot)
{
    _router = routerClient;
    _prototype = prototype;
    _handler = handler;

    stopListening();
    if(_subscribe)
        _router->subscribeEvent(_prototype.name());
    connect(this,SIGNAL(onMessage(ccappcore::Message)),_handler,slot);
    connect(routerClient,
            SIGNAL(messageReceived(ccappcore::Message,ccappcore::ClientAddress,ccappcore::ClientAddress)),
            this,
            SLOT(messageReceived(ccappcore::Message,ccappcore::ClientAddress,ccappcore::ClientAddress)));
    _started = true;
}

void EventListener::stopListening()
{
    if(_subscribe)
        _router->unsubscribeEvent(_prototype.name());
    _router->disconnect(this);
    this->disconnect(_handler);
    this->disconnect(this);
    _started = false;
}

void EventListener::messageReceived(const Message& m,
                         const ClientAddress& /*from*/, const ClientAddress& /*to*/)
{
    if(m.like(_prototype))
        emit onMessage(m);
}


}
