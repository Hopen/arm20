#include "globaladdressbookcontroller.h"
#include "requestreplymessage.h"
#include "callscontroller.h"
#include <QtAlgorithms>

namespace ccappcore
{

const int BOOK_HASH = 123456;


GlobalAddressBookController::GlobalAddressBookController(QObject *parent)
    :ContactsController(parent), _resetModelRequest(true)
{
}

GlobalAddressId GlobalAddressBookController::createGlobalAddressId(int id) const
{
    return ContactId(const_cast<GlobalAddressBookController*>(this), id, CT_GlobalAddress);
}

GlobalAddressId GlobalAddressBookController::createBookAddressId(int id) const
{
    return ContactId(const_cast<GlobalAddressBookController*>(this), id, CT_GlobalBook);
}


QFuture < GlobalAddress >  GlobalAddressBookController::loadContactsAsync()
{
     _loadGlobalAddressBookResult = QFutureInterface<GlobalAddress>( (QFutureInterfaceBase::State)
            (QFutureInterfaceBase::Started | QFutureInterfaceBase::Running)
            );

    _globalAddressList.clear();

    Message request, replyPrototype;
    request = Message("O2C_PERSON_LIST_REQ");
    request["oper_id"] = _callsController->sessionOwner().contactId().toInt();
    replyPrototype = Message("C2O_PERSON_LIST");
    RequestReplyMessage::send(
                request, replyPrototype, this, SLOT(on_c2oPersonList(ccappcore::Message)));

    return _loadGlobalAddressBookResult.future();
}

const QList<Contact>& GlobalAddressBookController::contacts() const
{
    return _globalAddressList;
}

QUrl GlobalAddressBookController::buildContactUrl(const Contact& c) const
{
    qWarning("GlobalAddressBookController::buildContactUrl() is not implemented.");

    return QString();
}

void GlobalAddressBookController::on_c2oPersonList(const ccappcore::Message &m)
{
    _loadGlobalAddressBookResult.reportStarted();
    QSet <int> bookList;
    if( 0==(quint32)m["sql_result"] )
    {
        QByteArray listData = m["data"];
        QList<Message> list = _messageSerializer->loadList(listData);
        foreach(const Message& m, list)
        {
            qDebug()<<m;
            qint32  id        = m["id"];
            QString name      = m["name"];
            QString full_name = m["full_name"];
            qint32  parent_id = m["parent_id"];
            qint32  type      = m["type"];
            qint32  book_id   = m["book_id"];
            QString book_name = m["book_name"];

            Q_UNUSED(type)

            bookList.insert(book_id);

            GlobalAddressId globalAddressId = createGlobalAddressId(id);
            GlobalAddress globalAddress = findById(globalAddressId);

            bool isNew = !globalAddress.isValid();
            if(isNew)
            {
                globalAddress = GlobalAddress(globalAddressId);
                _globalAddressList.push_back(globalAddress);
            }

            globalAddress.setName(name);
            globalAddress.addData((Contact::ContactInfo)Contact::LongName, full_name);
            globalAddress.addData((Contact::ContactInfo)Contact::UserData, parent_id);
            globalAddress.addData((Contact::ContactInfo)Contact::UserData+1, book_id);
            globalAddress.addData((Contact::ContactInfo)Contact::UserData+2, book_name);
            globalAddress.addData((Contact::ContactInfo)Contact::Status, Contact::Free);

//            if (parent_id > -1)
//            {
//                GlobalAddressId parentId = createGlobalAddressId(parent_id);
//                GlobalAddress parent = findById(parentId);

//                if(parent.isValid())
//                    parent.addChild(globalAddress);
//                else
//                    qWarning()<<"GlobalAddressBookController::on_c2oPersonList() - "<<
//                            "invalid data: globalAddressId"<<globalAddressId<<", parentId="<<parentId;
//            }

            //GlobalAddress globalAddress(id,name,full_name,parent_id,type,book_id,book_name);
            //_globalAddressList.push_back(globalAddress);
        }
    }
    // create tree
    GlobalAddressContainer tmpList;
    foreach(const GlobalAddress &globAddress, _globalAddressList)
    {
        int parent_id = globAddress.data(Contact::UserData).toInt();
        if (parent_id > 0)
        {
            GlobalAddressId parentId = createGlobalAddressId(parent_id);
            GlobalAddress parent = findById(parentId);

            if(parent.isValid())
                parent.addChild(globAddress);
            else
                qWarning()<<"GlobalAddressBookController::on_c2oPersonList() - "<<
                        "invalid data:"<<globAddress.name()<<", parentId="<<parentId;
        }
        else
        {
            tmpList.push_back(globAddress);
        }
    }

    _globalAddressList = tmpList;

    // create book list (map)
    QMap<int, GlobalAddress> bookMap;
    foreach(const int &index, bookList)
    {
        GlobalAddressId bookId = createBookAddressId(index + BOOK_HASH);
        bookMap[index] = GlobalAddress(bookId);
    }

    // insert global address in book
    foreach(const GlobalAddress &globAddress, _globalAddressList)
    {
        int book_id       = globAddress.data(Contact::UserData+1).toInt();
        QString book_name = globAddress.data(Contact::UserData+2).toString();
        GlobalAddress book = bookMap.value(book_id,GlobalAddress());
        if (book.isValid())
        {
            book.addChild(globAddress);
            book.setName(book_name);
        }
        else
        {
            qWarning()<<"GlobalAddressBookController::on_c2oPersonList() - "<<
                    "invalid book_id:"<<book_id<<" from contact="<<globAddress.name();
        }
    }

    _globalAddressList = bookMap.values();
    //_saveGlobalAddressList = _globalAddressList;
    _saveGlobalAddressList.resize(_globalAddressList.size());
    qCopy(_globalAddressList.begin(),_globalAddressList.end(),_saveGlobalAddressList.begin());
//    foreach(const GlobalAddress &globAddress, _globalAddressList)
//    {
//        GlobalAddress saveAddress = globAddress;
//        _saveGlobalAddressList.push_back(saveAddress);
//    }

    if (_globalAddressList.size() > 0)
    {
        _loadGlobalAddressBookResult.reportResults(_globalAddressList.toVector(), 0, _globalAddressList.count() );

    }
    _loadGlobalAddressBookResult.reportFinished();



    emit onLoadFinished();

}

QList<GlobalAddress> GlobalAddressBookController::findGlobalAddresses(const Predicate<GlobalAddress>& predicate) const
{
    QList<GlobalAddress> r;
    foreach(GlobalAddress c, _globalAddressList)
        if(c.isValid() && predicate(c))
            r.append(c);
    return r;
}

void GlobalAddressBookController::getPersonInfo(const ccappcore::ContactId &id)
{
    Message m("O2C_PERSON_INFO_REQ");
    m["person_id"] = id.toInt();
    RequestReplyMessage::send( m,
                               Message("C2O_PERSON_INFO"),
                               this, SLOT(on_c2oPersonInfo(ccappcore::Message)));

}

void GlobalAddressBookController::on_c2oPersonInfo(const ccappcore::Message &m)
{
    if( 0==(quint32)m["sql_result"] )
    {
        qDebug()<<m;
        qint32  person_id = m["person_id"];

        GlobalAddressId globalAddressId = createGlobalAddressId(person_id);
        GlobalAddress globalAddress = findById(globalAddressId);

        if(!globalAddress.isValid())
        {
            qWarning()<<"GlobalAddressBookController::on_c2oPersonInfo() - "<<
                    "invalid person_id:"<<person_id;

            return;
        }


        if (_personExtraInfo.contains(person_id))
            _personExtraInfo.remove(person_id);

        QByteArray listData = m["data"];
        QList<Message> list = _messageSerializer->loadList(listData);
        foreach(const Message& m2, list)
        {
            qDebug()<<m2;
            QString type_name = m2["type_name"];
            qint32  type_id   = m2["type_id"];

            QString info      = m2["info"];
            qint32  info_id   = m2["info_id"];

            Q_UNUSED(type_name)
            Q_UNUSED(info_id)

            globalAddress.addData((ccappcore::Contact::ContactInfo)(getContactTypeByPersonType(type_id)), info);

            _personExtraInfo.insertMulti(person_id, AddressExtraInfo(type_id, info));
        }

        emit dataChanged();
        emit onLoadInfoCompleted(globalAddress);
    }
}

int GlobalAddressBookController::getContactTypeByPersonType(const int& type_id)const
{
    switch (type_id)
    {
    case ETI_NAME:
        return ccappcore::Contact::Name;
    case ETI_EMAIL:
        return ccappcore::Contact::Email;
    case ETI_PHONE:
        return ccappcore::Contact::Phone;
    case EIT_ADDRESS:
        return ccappcore::Contact::Address;
    case EIT_ORGANIZATION:
        return ccappcore::Contact::Organization;

    default:
        return -1;
    }
}


void GlobalAddressBookController::search(const QString &text, bool bResetModel)
{
    _resetModelRequest = bResetModel;

    Message m("O2C_SEARCH_CONTACTS");
    m["type"] = -2;
    m["book"] = tr("По всем");
    m["value"] = text;
    RequestReplyMessage::send( m,
                               Message("C2O_SEARCH_CONTACTS"),
                               this, SLOT(on_c2oSearchContacts(ccappcore::Message)));

}

void GlobalAddressBookController::on_c2oSearchContacts(const ccappcore::Message &m)
{
    QMap <int, QString> idList;
    if( 0==(quint32)m["sql_result"] )
    {
        QByteArray listData = m["data"];
        QList<Message> list = _messageSerializer->loadList(listData);
        foreach(const Message& m, list)
        {
            qDebug()<<m;
            qint32  id        = m["id"];
            QString name      = m["name"];
            QString full_name = m["full_name"];
            //qint32  parent_id = m["parent_id"];
            //qint32  type      = m["type"];
            //qint32  book_id   = m["book_id"];
            //QString book_name = m["book"];
            QString value     = m["value"];

            //idList.push_back(id);
            idList[id] = value;

            Q_UNUSED(name)
            Q_UNUSED(full_name)

//            bookList.insert(book_id);

//            GlobalAddressId globalAddressId = createGlobalAddressId(id);
//            GlobalAddress globalAddress = findById(globalAddressId);

//            bool isNew = !globalAddress.isValid();
//            if(isNew)
//            {
//                globalAddress = GlobalAddress(globalAddressId);
//                _globalAddressList.push_back(globalAddress);
//            }

//            globalAddress.setName(name);
//            globalAddress.addData((Contact::ContactInfo)Contact::LongName, full_name);
//            globalAddress.addData((Contact::ContactInfo)Contact::UserData, parent_id);
//            globalAddress.addData((Contact::ContactInfo)Contact::UserData+1, book_id);
//            globalAddress.addData((Contact::ContactInfo)Contact::UserData+2, book_name);
        }
    }

//    disableAddressList();

//    if (!idList.isEmpty())
//    {
//        GlobalAddressContainer tmpList;
//        enableBranch(idList,_globalAddressList, tmpList);
//        //_globalAddressList = tmpList;
//        qSwap (_globalAddressList, tmpList);
//    }

    if (_resetModelRequest)
    {
        GlobalAddressContainer tmpList;
        //_globalAddressList = _saveGlobalAddressList;
        _globalAddressList.clear();
        QVector <GlobalAddress> tmpVector(_saveGlobalAddressList.size());
        qCopy(_saveGlobalAddressList.begin(),_saveGlobalAddressList.end(),tmpVector.begin());
        _globalAddressList = tmpVector.toList();

//        foreach(const GlobalAddress &globAddress, _saveGlobalAddressList)
//        {
//            GlobalAddress saveAddress = globAddress;
//             _globalAddressList.push_back(saveAddress);
//        }

        changeListStatus(idList.keys(),_globalAddressList, tmpList);
        //qSwap (_globalAddressList, tmpList);
        _globalAddressList = tmpList;

        emit dataChanged();
        //emit resetModel();

    }

    if(idList.isEmpty())
    {
        emit onLoadInfoCompleted(Contact());
        emit onSearchCompleted(Contact(), QString());
    }
    else
    {
        ContactId firstContact = createGlobalAddressId(idList.begin().key());
        Contact c = findById(firstContact);
        emit onLoadInfoCompleted(c);
        emit onSearchCompleted(c, idList.begin().value());
    }



//    // create tree
//    GlobalAddressContainer tmpList;
//    foreach(const GlobalAddress &globAddress, _globalAddressList)
//    {
//        int parent_id = globAddress.data(Contact::UserData).toInt();
//        if (parent_id > 0)
//        {
//            GlobalAddressId parentId = createGlobalAddressId(parent_id);
//            GlobalAddress parent = findById(parentId);

//            if(parent.isValid())
//                parent.addChild(globAddress);
//            else
//                qWarning()<<"GlobalAddressBookController::on_c2oPersonList() - "<<
//                        "invalid data:"<<globAddress.name()<<", parentId="<<parentId;
//        }
//        else
//        {
//            tmpList.push_back(globAddress);
//        }
//    }

//    _globalAddressList = tmpList;

//    // create book list (map)
//    QMap<int, GlobalAddress> bookMap;
//    foreach(const int &index, bookList)
//    {
//        GlobalAddressId bookId = createBookAddressId(index + BOOK_HASH);
//        bookMap[index] = GlobalAddress(bookId);
//    }

//    // insert global address in book
//    foreach(const GlobalAddress &globAddress, _globalAddressList)
//    {
//        int book_id       = globAddress.data(Contact::UserData+1).toInt();
//        QString book_name = globAddress.data(Contact::UserData+2).toString();
//        GlobalAddress book = bookMap.value(book_id,GlobalAddress());
//        if (book.isValid())
//        {
//            book.addChild(globAddress);
//            book.setName(book_name);
//        }
//        else
//        {
//            qWarning()<<"GlobalAddressBookController::on_c2oPersonList() - "<<
//                    "invalid book_id:"<<book_id<<" from contact="<<globAddress.name();
//        }
//    }

//    _globalAddressList = bookMap.values();
}


//void GlobalAddressBookController::enableAddressList()
//{
//    GlobalAddressContainer tmpList;
//    changeListStatus(_globalAddressList, tmpList, Contact::Free);
//    qSwap (_globalAddressList, tmpList);
//}

//void GlobalAddressBookController::disableAddressList()
//{
//    GlobalAddressContainer tmpList;
//    changeListStatus(_globalAddressList, tmpList, Contact::UnknownStatus);
//    qSwap (_globalAddressList, tmpList);
//}

//void GlobalAddressBookController::changeListStatus(const QList<Contact>& list, QList<Contact>& out, int status)
//{
//    foreach (const Contact& c, list)
//    {
//        Contact c1 = c;
//        c1.addData((Contact::ContactInfo)Contact::Status, status/*bEnable? Contact::Free : Contact::UnknownStatus*/);
//        out.push_back(c1);

//        if (!c.children().isEmpty())
//        {
//            QList<Contact> children;
//            changeListStatus(c.children(),children,status);
//            c1.setChildren(children);
//        }

//    }
//}

bool GlobalAddressBookController::changeListStatus(const QList<int> &idList, const QList<Contact> &list, QList<Contact> &out)
{
    bool bEnable = false;
    foreach (const Contact& c, list)
    {
        bool bEnable2 = false;
        Contact c1(c.contactId());
        c1.setName(c.name());
        c1.addData((Contact::ContactInfo)Contact::LongName, c.data(Contact::LongName).toString());
        //c1.addData((Contact::ContactInfo)Contact::UserData, parent_id);
        //c1.addData((Contact::ContactInfo)Contact::UserData+1, book_id);
        //c1.addData((Contact::ContactInfo)Contact::UserData+2, book_name);
        //c1.addData((Contact::ContactInfo)Contact::Status, Contact::Free);

        if (!c.children().isEmpty())
        {
            QList<Contact> children;
            if (changeListStatus(idList,c.children(),children))
                bEnable2 = true;
            //if (!children.isEmpty())
                c1.setChildren(children);
        }

        if (bEnable2 || idList.contains(c.contactId().toInt()) || (c.contactId().type() == CT_GlobalBook))
        {
            out.push_back(c1);
            bEnable = true;
        }
    }
    return bEnable;
}

//bool GlobalAddressBookController::enableBranch(const QList<int> &idList, const QList<Contact> &list, QList<Contact> &out)
//{
//    bool bEnable = false;
//    foreach (const Contact& c, list)
//    {
//        Contact c1 = c;
//        if (idList.contains(c.contactId().toInt()))
//        {
//            c1.addData((Contact::ContactInfo)Contact::Status, Contact::Free);
//            bEnable = true;
//        }
//        out.push_back(c1);

//        if (!c.children().isEmpty())
//        {
//            QList<Contact> children;
//            if (enableBranch(idList,c.children(),children))
//                bEnable = true;
//            c1.setChildren(children);
//        }
//    }

//    return bEnable;
//}

void GlobalAddressBookController::resetList()
{
//    //enableAddressList();
    //_globalAddressList = _saveGlobalAddressList;
    //qCopy(_saveGlobalAddressList.begin(),_saveGlobalAddressList.end(),_globalAddressList.begin());
    _globalAddressList.clear();
    QVector <GlobalAddress> tmpVector(_saveGlobalAddressList.size());
    qCopy(_saveGlobalAddressList.begin(),_saveGlobalAddressList.end(),tmpVector.begin());
//    foreach(const GlobalAddress &globAddress, _saveGlobalAddressList)
//    {
//        GlobalAddress saveAddress = globAddress;
//         _globalAddressList.push_back(saveAddress);
//    }
    _globalAddressList = tmpVector.toList();

    emit dataChanged();
    //emit resetModel();
    emit onLoadInfoCompleted(Contact());
}

} //namespace ccappcore
