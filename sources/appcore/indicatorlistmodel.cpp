#include "indicatorlistmodel.h"
#include "useritemdatarole.h"

#include <QColor>
#include <QApplication>
#include <QStyle>

namespace ccappcore
{

IndicatorListModel::IndicatorListModel(QObject *parent)
    : QAbstractListModel(parent)
    , _colCount(0)
{
}

int IndicatorListModel::addRow(const QString& headerDisplayText,
                                const QVariant& associatedObject,
                                const IndicatorList& rowData )
{
    _rows.append(rowData);
    HeaderData headerData;
    headerData[Qt::DisplayRole] = headerDisplayText;
    headerData[AssociatedObjectRole] = associatedObject;
    _verticalHeaderData.append(headerData);
    _colCount = rowData.count() > _colCount ? rowData.count() : _colCount;

    foreach(QuantativeIndicator* i, rowData)
        connect(i,SIGNAL(valueChanged(int)), SIGNAL(layoutChanged()));

    return _verticalHeaderData.indexOf(headerData);
}

int IndicatorListModel::rowCount(const QModelIndex &parent) const
{
    if(!parent.isValid())
        return _rows.count();

    return 0;
}

int IndicatorListModel::columnCount(const QModelIndex &parent) const
{
    if(!parent.isValid())
        return _colCount;

    return 0;
}

QVariant IndicatorListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(orientation == Qt::Vertical)
    {
        if( section < 0 || section >= _verticalHeaderData.count() )
            return QVariant();
        const HeaderData& header = _verticalHeaderData[section];
        if( !header.keys().contains(role) )
            return QVariant();

        return header[role];
    }
    else
    {
        if(role == Qt::DisplayRole)
        {
            if( section < 0 || section >= _rows[0].count() )
                return QVariant();

            return _rows[0][section]->name();
        }
    }

    return QVariant();
}

QPointer<QuantativeIndicator> IndicatorListModel::indicatorByIndex(const QModelIndex& index) const
{
    if( index.row() < 0 || index.row() >= _rows.count() )
        return QPointer<QuantativeIndicator>();

    if( index.column() < 0 || index.column() >= _rows[index.row()].count() )
        return QPointer<QuantativeIndicator>();

    return _rows.at( index.row() ).at( index.column() );
}


Qt::ItemFlags IndicatorListModel::flags(const QModelIndex &index) const
{
    QPointer<QuantativeIndicator> indicator = indicatorByIndex(index);
    if(indicator.isNull())
        return Qt::NoItemFlags;

    return QAbstractListModel::flags(index) | Qt::ItemIsUserCheckable;
}

QVariant IndicatorListModel::data(const QModelIndex &index, int role) const
{
    QPointer<QuantativeIndicator> indicator = indicatorByIndex(index);
    if(indicator.isNull())
        return QVariant();

    if( role == AssociatedObjectRole )
        return QVariant::fromValue( indicator  );

    if( role == Qt::DisplayRole )
    {
        return indicator->value();
    }

    if( role == Qt::ForegroundRole )
    {
        static QColor defaultColor = QColor(Qt::gray).darker();
        static QColor warningColor = QColor(0xff9900); //orange
        static QColor criticalColor = QColor(Qt::red);
        static QColor normalColor = QColor(Qt::green).darker();
        switch(indicator->severity())
        {
            case QuantativeIndicator::CriticallyLowValue:
            case QuantativeIndicator::CriticallyHighValue:
                return criticalColor;
            case QuantativeIndicator::AboveNormalValue:
            case QuantativeIndicator::BelowNormalValue:
                return warningColor;
            case QuantativeIndicator::NormalValue:
                return normalColor;
            default:
                return defaultColor;
        }
    }

    if(role == Qt::DecorationRole)
    {
        static QIcon negativeTrendIcon(":/resources/icons/trend_negative_24x24.png");
        static QIcon positiveTrendIcon(":/resources/icons/trend_positive_24x24.png");
        static QIcon negativeInvertedTrendIcon(":/resources/icons/trend_negative_inverted_24x24.png");
        static QIcon positiveInvertedTrendIcon(":/resources/icons/trend_positive_inverted_24x24.png");

        const QIcon& positiveIcon = indicator->isInverted()
                                    ? positiveInvertedTrendIcon : positiveTrendIcon;
        const QIcon& negativeIcon = indicator->isInverted()
                                    ? negativeInvertedTrendIcon : negativeTrendIcon;

        QIcon trendIcon;
        if( indicator->trend() == QuantativeIndicator::PositiveTrend )
            trendIcon = positiveIcon;
        else if( indicator->trend() == QuantativeIndicator::NegativeTrend )
            trendIcon = negativeIcon;

        return trendIcon;
    }

    return QVariant();
}


}
