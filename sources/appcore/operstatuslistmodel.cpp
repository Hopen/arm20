#include "operstatuslistmodel.h"
#include <QColor>
#include <QBrush>

namespace ccappcore {

OperStatusListModel::OperStatusListModel(QObject *parent)
    : QAbstractListModel(parent)
{
    connect(_operListController.instance(),
            SIGNAL(contactStatusChanged(ccappcore::Contact)),
            this,
            SLOT(reset()));
}

void OperStatusListModel::reset()
{
        applyFilter(_filter);
}

void OperStatusListModel::applyFilter(const PredicateGroup<Contact>& filter)
{
    emit layoutAboutToBeChanged();

    _filter = filter;
    QMap<Contact::ContactStatus, int> data;
    data[Contact::Free] = 0;
    data[Contact::Paused] = 0;
    data[Contact::Busy] = 0;
    data[Contact::Offline] = 0;

    foreach(const Contact& o, _operListController->operList() )
    {
        if(_filter.matches(o))
            data[o.status()]++;
    }
    _data = data;
    emit layoutChanged();
}


int OperStatusListModel::columnCount(const QModelIndex &parent) const
{
    return parent.isValid()? 0: _data.count();
}

int OperStatusListModel::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : 1;
}


QVariant OperStatusListModel::data(const QModelIndex &index, int role ) const
{
    if(index.column()<0||index.column()>_data.count())
        return QVariant();

    if(index.row()!=0)
        return QVariant();

    Contact::ContactStatus status = (Contact::ContactStatus)index.column();
    int statusCount = _data.value(status);

    if(role == Qt::DisplayRole)
    {
        return statusCount;
    }

    if(role == Qt::DecorationRole && index.column() == 0)
    {
        return _operListController->statusIcon(status);
    }
    if(role == Qt::TextColorRole)
    {
        return _operListController->statusColor(status);
    }

    return QVariant();
}

QVariant OperStatusListModel::headerData(int section,
                                         Qt::Orientation orientation,
                                         int role) const
{
    if(orientation!=Qt::Horizontal)
        return QVariant();

    if(section<0||section>_data.count())
        return QVariant();

    if(role == Qt::TextAlignmentRole)
        return Qt::AlignLeft;

    Contact::ContactStatus status = _data.keys().at(section);
    if(role == Qt::DisplayRole)
        return _operListController->statusAsString(status);

    return QVariant();
}


//QString OperStatusListModel::statusAsString(Contact::ContactStatus status) const
//{
//    switch(status)
//    {
//    case Contact::Free:
//        return tr("Свободно");
//    case Contact::Busy:
//        return tr("Занято");
//    case Contact::Paused:
//        return tr("На паузе");
//    case Contact::Offline:
//        return tr("Оффлайн");
//    default:
//        return tr("Неизвестно");
//    }
//}



} // namespace ccappcore
