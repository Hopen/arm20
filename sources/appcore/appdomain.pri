HEADERS += call.h \
    contact.h \
    contactscontroller.h \
    contactstreemodel.h \
    operlistcontroller.h \
    sessioncontroller.h \
    callscontroller.h \
    callslistmodel.h \
    domainobjectfilter.h \
    operstatuslistmodel.h \
    useritemdatarole.h \
    statusoptions.h
SOURCES += contact.cpp \
    contactscontroller.cpp \
    contactstreemodel.cpp \
    call.cpp \
    operlistcontroller.cpp \
    callscontroller.cpp \
    sessioncontroller.cpp \
    callslistmodel.cpp \
    operstatuslistmodel.cpp \
    statusoptions.cpp

#win32 {
#    !static {
#      DEFINES+=QT_MAKEDLL
#    }
#    else {
#      DEFINES+=QT_NODLL
#    }
#}
