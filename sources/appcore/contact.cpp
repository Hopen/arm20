#include "contact.h"
#include "contactscontroller.h"
#include "callscontroller.h"

namespace ccappcore {

ContactId::ContactId(ContactsController* controller, const QVariant& id, int type/* = 0*/)
    : QVariant(id), _controller(controller), _type(type)
{}

ContactsController* ContactId::controller() const
{
    return _controller;
}

Contact::Private::Private(const ContactId& id)
    : contactId(id)
    , isContainer(false)
    , childrenLoaded(false)
{
}

Contact::Private::Private(ContactsController* c, const QVariant& id)
    : contactId(c, id)
    , isContainer(false)
    , childrenLoaded(false)
{}


Contact::Contact()
    : d(new Contact::Private(NULL, QVariant()))
{
}

Contact::Contact( const ContactsController* controller, const QVariant& personId)
    : d(new Contact::Private(const_cast<ContactsController*>(controller), personId))
{

}

Contact::Contact( const ContactId& id )
    : d(new Contact::Private(id))
{

}

bool Contact::isValid() const
{
    return contactId().isValid();
}

const ContactId& Contact::contactId() const
{
    return d->contactId;
}

QString Contact::name() const
{
    return data(Name).toString();
}

QUrl Contact::buildUrl() const
{
    return controller()->buildContactUrl(*this);
}

Contact::ExtraInfo Contact::contactInfo() const
{
    return d->extraInfo;
}

QList<QVariant> Contact::dataAsList(int type) const
{
    return d->extraInfo.values((ContactInfo)type);
}

QVariant Contact::data(int type) const
{
    return d->extraInfo.value((ContactInfo)type);
}

QString Contact::infoName(int infoId) const
{
    return controller()->infoDisplayName(infoId);
}

Contact::ContactStatus Contact::status() const
{
    return (Contact::ContactStatus) data (Status).toInt();
}

void Contact::setStatus(ContactStatus status)
{
    setData(Status, status);
}

QDateTime Contact::statusTime() const
{
    return data(StatusTime).toDateTime();
}

void Contact::setStatusTime(const QDateTime& dt)
{
    setData(StatusTime, dt);
}

QString Contact::statusReason() const
{
    return data(StatusReason).toString();
}

void Contact::setStatusReason(const QString& reason)
{
    setData(StatusReason, reason);
}

void Contact::setIsContainer(bool val)
{
    d->isContainer = val;
}

void Contact::setName(const QString& name)
{
    //name as separate field is to see the name in watch window when debugging
    d->name = name;
    addData(Name,name);
}

void Contact::addData(int type, const QVariant& value)
{
    d->extraInfo.insertMulti((ContactInfo)type, value);
}

void Contact::setData(int type, const QVariant& value)
{
    d->extraInfo.replace((ContactInfo)type, value);
}

bool Contact::childrenLoaded() const
{
    return d->childrenLoaded;
}

const QList<Contact>& Contact::children() const
{
    return d->children;
}

bool Contact::isContainer() const
{
    return d->isContainer;
}

void Contact::setChildrenLoaded(bool b)
{
    d->childrenLoaded = b;
}

void Contact::setChildren(const QList<Contact>& children)
{
    d->children =children;
}

void Contact::addChild(const Contact& contact)
{
    d->children.append(contact);
}

const Contact& Contact::findChild(const ContactId& id) const
{
    if(contactId() == id)
        return *this;

    foreach(const Contact& c, d->children)
    {
        const Contact& child = c.findChild(id);
        if(child.isValid())
            return child;
    }
    return Contact::invalid();
}

bool Contact::operator==(const Contact& other) const
{
    return isValid() &&
           controller() == other.controller() &&
           contactId() == other.contactId();
}

bool Contact::operator!=(const Contact& other) const
{
    return !(*this==other);
}

bool Contact::operator < (const Contact& other) const
{
    if(status()<other.status())
        return true;
    if(status()>other.status())
        return false;
    if( isContainer() && !other.isContainer() )
        return true;
    if( !isContainer() && other.isContainer() )
        return false;

    return name() < other.name();
}

bool Contact::canMakeCall() const
{
    return controller()->canMakeCall(*this);
}

bool Contact::canTransferCall(const ccappcore::Call& call) const
{
    return controller()->canTransferCall(call, *this);
}

bool Contact::canSendMessage() const
{
    return controller()->canSendMessage(*this);
}

} // namespace ccappcore
