﻿#include "requestreplymessage.h"
#include "appsettings.h"
#include "contact.h"
#include "sessioncontroller.h"
#include "operlistcontroller.h"
#include "callscontroller.h"
#include "statusoptions.h"
#include "scriptadapter.h"

#include <QNetworkInterface>
#include <QStateMachine>

#ifdef Q_WS_WIN
#   define _WIN32_WINDOWS 0x0500
#   include <windows.h>

    static DWORD s_dwTimeout = 0;
#endif


namespace ccappcore
{

SessionController::SessionController(QObject* parent)
        : QObject(parent)
        , _sessionType(OperatorSession)
        , _deviceType(0)
        , _forced(false)
        , _autoLogin(false)
        , _useNtlm(false)
        , _langId(LANG_RUS)
{    
}

SessionController::~SessionController()
{
}

void SessionController::initialize()
{
    connect(_router.instance(),SIGNAL(error(QString)),this,SLOT(onRouterError(QString)));
    connect(_router.instance(),SIGNAL(routeError(ccappcore::Message)),this,SLOT(onRouteMessageError()));

    Q_ASSERT(!_sessionStateMachine);
    _sessionStateMachine = new QStateMachine(this);
    _stateConnecting    = new QState();
    _stateConnected     = new QState();
    _stateDisconnected  = new QFinalState();

    _sessionStateMachine->addState(_stateConnecting);
    _sessionStateMachine->addState(_stateConnected);
    _sessionStateMachine->addState(_stateDisconnected);
    _sessionStateMachine->setInitialState(_stateConnecting);

    //connecting state
    connect(_stateConnecting, SIGNAL(entered()), this, SLOT(connectToRouter()));
    _stateConnecting->addTransition( _router.instance(),
                                     SIGNAL(connected()),
                                     _stateConnected );

    _stateConnecting->addTransition( _router.instance(),
                                     SIGNAL(disconnected(IRouterClient::DisconnectReason)),
                                     _stateDisconnected );
    _stateConnecting->addTransition( this,
                                     SIGNAL(sessionError(SessionController::SessionErrorCode,QString)),
                                     _stateDisconnected );
    //connected state
    _stateStarting      = new QState(_stateConnected);
    _stateStarted       = new QState(_stateConnected);
    _stateConnected->setInitialState(_stateStarting);
    _stateConnected->addTransition( this,
                                     SIGNAL(sessionError(SessionController::SessionErrorCode,QString)),
                                     _stateDisconnected );
    _stateConnected->addTransition( _router.instance(),
                                    SIGNAL(disconnected(IRouterClient::DisconnectReason)),
                                    _stateDisconnected );


    //starting state
#if (_CDSVER >= 0x0400)
    _stateReqPublicKey          = new QState(_stateStarting);
#endif
    _stateTryLogin              = new QState(_stateStarting);
    _stateLoadData              = new QState(QState::ParallelStates, _stateStarting);

#if (_CDSVER >= 0x0400)
    _stateStarting->setInitialState(_stateReqPublicKey);
#else
    _stateStarting->setInitialState(_stateTryLogin);
#endif
    _stateStarting->addTransition( &_loadDataFuture, SIGNAL(finished()), _stateLoadData);
    _stateStarting->addTransition( _stateLoadData, SIGNAL(finished()), _stateStarted);

    addLoadDataAgent(&_loadProfilesFuture   , SLOT(onLoadProfiles   ()));
    addLoadDataAgent(&_loadSpamFuture       , SLOT(onLoadSpam       ()));
    addLoadDataAgent(&_loadAddressBookFuture, SLOT(onLoadAddressBook()));
    addLoadDataAgent(&_loadSettingsFuture   , SLOT(onLoadSetiings   ()));
//    _stateLoadProfiles          = new QState(_stateLoadData);
//    _stateLoadSpam              = new QState(_stateLoadData);

//    QFinalState *LoadDataFinalState = new QFinalState(_stateLoadData);


//    _stateLoadProfiles->addTransition( &_loadProfilesFuture, SIGNAL(finished()), _stateLoadSpam);
//    _stateLoadSpam    ->addTransition( &_loadSpamFuture, SIGNAL(finished()), LoadDataFinalState);

    //state request public key
#if (_CDSVER >= 0x0400)
    connect(_stateReqPublicKey, SIGNAL(entered()), _cryptoProvider.instance(), SLOT(reqPublicKey()));
    connect(_stateReqPublicKey, SIGNAL(exited()), this, SLOT(encryptPassword()));
    _stateReqPublicKey->addTransition( _cryptoProvider.instance(), SIGNAL(publicKeyReady()),_stateTryLogin);
#endif    
    //state try auto login
    connect(_stateTryLogin,SIGNAL(entered()), this, SLOT(sendLoginRequest()));

    //state started
    connect(_stateStarted, SIGNAL(entered()), this, SLOT(onSessionStarted()));

    //state disconnected
    connect(_stateDisconnected, SIGNAL(entered()), this, SLOT(onSessionStopped()) );

    //state load data
    connect(_stateLoadData, SIGNAL(entered()), this, SLOT(onLoadData()) );
    connect(_stateLoadData, SIGNAL(exited()), this, SLOT(onLoadDataCompleted()) );

//    //state load profile list
//    connect(_stateLoadProfiles, SIGNAL(entered()), this, SLOT(onLoadProfiles()) );

//    //state load spam list
//    connect(_stateLoadSpam, SIGNAL(entered()), this, SLOT(onLoadSpam()) );
}

void SessionController::applySettings(const AppSettings& settings)
{
    _deviceType = settings.getOption("device/route").toInt();
    _deviceNumber =  settings.getOption("device/number").toString();

    if (settings.getOption("connecting/autoIP").toBool()) // redmine #370
    {
        QString ip = detectValidIpAddress();
        _deviceNumber = "TA:" + ip;
    }

    _login = settings.getOption( "session/login" ).toString();
    _autoLogin = settings.getOption( "session/autoLogin" ).toBool();
    if (isAutoLoginEnabled())
        _password = QByteArray::fromHex(settings.getOption( "session/password" ).toString().toAscii());
    _useNtlm = settings.getOption( "session/useNtlm" ).toBool();

    _langId = settings.getOption( "session/language" ).toString();
    if (!_langId.length())
        _langId = tr("RU");
}

void SessionController::flushSettings(AppSettings& settings)
{
    settings.setAppOption( "device/route", _deviceType );
    settings.setAppOption( "device/number", _deviceNumber );

    settings.setUserOption( "session/login",_login );
    settings.setUserOption( "session/autoLogin", _autoLogin );
    settings.setUserOption( "session/useNtlm", _useNtlm );
    settings.setUserOption( "session/password", isAutoLoginEnabled()
                            ? _password.toByteArray().toHex() : QByteArray() );
    settings.setUserOption( "session/language", _langId );
}

void SessionController::setPassword(const QString& password)
{
    _password = password;
    encryptPassword();
}

void SessionController::encryptPassword()
{
    if(!_password.isValid())
        return;

    if(_password.type() == QVariant::ByteArray)
        return; //already crypted
    Q_ASSERT(_password.type() == QVariant::String);

    if(!_crypto->canEncryptRSA())
        return;

    QString pwd = _password.toString();
    QByteArray src = QByteArray::fromRawData( (const char*) pwd.data(), (pwd.size() + 1) * sizeof(QChar) );
    _password = _crypto->encryptRSA(src);
}

Contact SessionController::sessionOwner() const
{
    return  _sessionOwner;
}

void SessionController::startSession()
{
#ifdef Q_WS_WIN
    SystemParametersInfo(SPI_GETFOREGROUNDLOCKTIMEOUT, 0, &s_dwTimeout, 0);
    if (!SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, 0, 0))
    {
        qDebug() << tr("Error: cannot set ForegroundLockTimeout param: %1").arg(GetLastError());
    }

#endif

    Q_ASSERT(_sessionStateMachine);
    if(!_sessionStateMachine->isRunning())
        _sessionStateMachine->start();
    else
        sendLoginRequest();
}

void SessionController::stopSession()
{
#ifdef Q_WS_WIN
    if (s_dwTimeout > 0)
        SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, (LPVOID)s_dwTimeout, 0);
#endif

    if(!isSessionStarted())
        return;

    _opStatusListener.stopListening();
    _router->send(Message("O2C_CLIENT_DISCONNECTED"), ClientAddress::Any());
    _router->disconnectFromServer();
    _sessionOwner = ContactId();

    bool stat = true;
    if (stat && !(stat = QDBusConnection::sessionBus().isConnected()))
    {
        qDebug() << "Cannot connect to the D-Bus session bus.\n"
                    << "To start it, run:\n"
                       << "\teval `dbus-launch --auto-syntax`\n";
    }
    if (stat && !(stat = QDBusConnection::sessionBus().unregisterService("DBus.ARM.QScript")))
    {
        qDebug() << QDBusConnection::sessionBus().lastError().message();
    }

    if (stat)
    {
        QDBusConnection::sessionBus().unregisterObject("/");
    }

}

QString SessionController::sessionTypeString()
{
    QString result = (_sessionType == SupervisorSession)
                                ? "11000000"
                                : "10000000";
    return result;
}

void SessionController::sendLoginRequest()
{
    _status->postInfo(tr("Регистрация пользователя..."));

    if(isNtlmLoginEnabled())
    {
        _ntlm->startAuthentication();
        return;
    }

    if(_login.isEmpty() || _password.toByteArray().isEmpty() )
    {
        emit sessionError(LoginCanceled,tr("Требуется авторизация"));
        return;
    }

    Message o2cLogin("O2C_LOGIN_REQ");
    o2cLogin["cmd"] = 1000;
    o2cLogin["app_id"] = sessionTypeString();
#if (_CDSVER >= 0x0400)
    o2cLogin["sender_oper_id"] = -1;
#else
    o2cLogin["oper_id"] = -1;
#endif
    o2cLogin["hw_type"] = _deviceType;
    o2cLogin["phone"] = _deviceNumber;
    o2cLogin["login"] = _login;
#if (_CDSVER >= 0x0400)
    o2cLogin["pass"] = _password.toByteArray();
#else
    o2cLogin["pass"] = _password.toString();
#endif
    o2cLogin["forced"] = _forced;

    Message c2oLogin("C2O_LOGIN");
    c2oLogin["sql_result"] = 0;

    RequestReplyMessage::send(o2cLogin, c2oLogin, //request and response prototype
                              this,
                              SLOT(on_c2oLoginSucceded(ccappcore::Message)),
                              SLOT(on_c2oLoginFailed(ccappcore::Message)));
}

bool SessionController::isSessionStarted() const
{
    return _sessionStateMachine && _sessionStateMachine->configuration().contains(_stateStarted);
}

bool SessionController::isConnected() const
{
    return _router->isConnected();
}

void SessionController::connectToRouter()
{
    _status->postInfo(tr("Подключение к серверу..."));
    _router->connectToServer();
}

void SessionController::onRouterError(const QString& error)
{
    emit sessionError(ConnectError, error);
}

void SessionController::onRouteMessageError()
{
    emit sessionError(ConnectError, tr("Ошибка доставки сообщения"));
}

void SessionController::on_c2oLoginSucceded(const Message& m)
{
    _sessionOwner = ccappcore::Contact(
            _operListController->createOperId( m["oper_id"] )
            );

    _operListController->setSessionOwner(_sessionOwner);

    Message statusPrototype("C2O_UPDATE_OPER_STATUS");
    statusPrototype["oper_id"] = _sessionOwner.contactId().toInt();
    _opStatusListener.startListening(
            &_router, statusPrototype,
            this, SLOT(on_c2oUpdateOperStatus(ccappcore::Message)));

    _operListController->startEventsListening();
    _status->postInfo(tr("Загрузка списка контактов..."));
    _loadDataFuture.setFuture(_operListController->loadContactsAsync());

    _callsController->startController();
}

void SessionController::on_c2oLoginFailed(const Message& m)
{
    _sessionOwner = ContactId();

    QString error = tr("Ошибка входа в систему.");
    SessionErrorCode reason = LoginFailed;

    if(m.contains("error"))
        error = m["error"].value().toString();
    else if(m.contains("sql_result"))
    {
        int sqlResult = m["sql_result"];
        switch(sqlResult)
        {
            case -1:
                error = tr("Неправильный логин или пароль.");
                break;
            case -2:
                reason = AlreadyLoggedIn;
                error = tr("Логин или телефонный номер уже используется.");
                break;
            case -3:
                error = tr("Недостаточно прав для входя в систему.");
                break;
            default:
                break;
        }
    }
    emit sessionError(reason, error);
}

void SessionController::on_c2oUpdateOperStatus(const Message& m)
{
    int operId = m["oper_id"];
    if( operId != _sessionOwner.contactId().toInt() )
        return;

    _pauseTimer.stop();

    Contact::ContactStatus operStatus = (Contact::ContactStatus) (int) m["status"];
    if(operStatus != Contact::Paused)
        return;

    int pauseTimeInSeconds = m["pause_time"];
    if(pauseTimeInSeconds<=0)
        return;

    _pauseTimer.setSingleShot(true);
    connect(&_pauseTimer, SIGNAL(timeout()),
            this, SLOT(setFreeStatus()),
            Qt::UniqueConnection);

    _pauseTimer.setInterval(pauseTimeInSeconds * 1000);
    _pauseTimer.start();
}

void SessionController::setFreeStatus()
{
    qDebug()<<"Session owner pause state has timed out.";
    _operListController->reqChangeStatus(_sessionOwner,Contact::Free);
}

void SessionController::setFreeStatusAfterLogon()
{
    LateBoundObject<StatusOptions> statusOptions;
    if(statusOptions.isValid() && !statusOptions->pauseAfterLogon())
    {
        _operListController->reqChangeStatus(_sessionOwner, Contact::Free);
    }
}

void SessionController::addLoadDataAgent(QFutureWatcher<void> *agent, const char *amember)
{
    QState * state_machine = new QState(_stateLoadData);
    // create child ctates
    QState      * state1 = new QState     (state_machine);
    QFinalState * state2 = new QFinalState(state_machine);
    state_machine->setInitialState(state1);

    state1->addTransition(agent, SIGNAL(finished()), state2);
    connect(state1, SIGNAL(entered()), this, amember);
}

void SessionController::onSessionStarted()
{
    qDebug()<<"Session started.";
    _router->disconnect(this,SLOT(onRouteMessageError()));
    setFreeStatusAfterLogon();
    emit sessionStarted();
}

void SessionController::onSessionStopped()
{
    qDebug()<<"Session stopped.";
    _router->disconnectFromServer();
    _sessionOwner = ContactId();
    emit sessionStopped();
}

void SessionController::onLoadData()
{
    _status->postInfo(tr("Загрузка данных..."));
    //_loadProfilesFuture.setFuture(_redirectListController->loadRedirectionsAsync());
    //_globalAddressController->loadAddressBookAsync(); // dont wait

}

void SessionController::onLoadProfiles()
{
    //_status->postInfo(tr("Загрузка списка переадресации..."));
    _loadProfilesFuture.setFuture(_redirectListController->loadRedirectionsAsync());

}

void SessionController::onLoadSpam()
{
    //_status->postInfo(tr("Загрузка списка объявлений..."));
    _loadSpamFuture.setFuture(_spamListController->loadSpamListAsync());

}

void SessionController::onLoadAddressBook()
{
    _loadAddressBookFuture.setFuture(_globalAddressController->loadContactsAsync());
}

void SessionController::onLoadSetiings()
{
    // sync data with input types (it is needed, cause system auto sync to conf file)
    _settings->flushSettings();
    _loadSettingsFuture.setFuture(_settings->loadServerSettings(sessionOwner()));
}

void SessionController::onLoadDataCompleted()
{
    _status->postInfo(tr("Загрузка данных завершена..."));
    _status->postInfo(tr("Подключение dBus демона..."));

    bool stat = true;
    if (stat && !(stat = QDBusConnection::sessionBus().isConnected()))
    {
        qDebug() << "Cannot connect to the D-Bus session bus.\n"
                    << "To start it, run:\n"
                       << "\teval `dbus-launch --auto-syntax`\n";
    }
    if (stat && !(stat = QDBusConnection::sessionBus().registerService("DBus.ARM.QScript")))
    {
        qDebug() << QDBusConnection::sessionBus().lastError().message();
    }

    _adapter = new ScriptAdapter(this);

    if (stat && !(stat = QDBusConnection::sessionBus().registerObject("/", _adapter, QDBusConnection::ExportAllSlots)))
    {
        qDebug() << QDBusConnection::sessionBus().lastError().message();
    }

    _status->postInfo(stat? tr("Демон dBus подключен успешно..."): tr("Невозможно подключить демон dBus..."));

    _spamListController->startController();

    // -------------------------------------------------------------
    _status->postInfo(tr("Подключение к Jabber-серверу..."));

//    QXmppClient client;
//    client.logger()->setLoggingType(QXmppLogger::StdoutLogging);
//    client.connectToServer("gubarev-forte-it-ru@jivosite.com", "YtjDby22");

    LateBoundObject<JabberController> jabberController;
    jabberController->startController();

    // -------------------------------------------------------------
    _status->postInfo(tr("Применение глобальных настроек..."));
    _settings->applySetting("PauseList");
    _settings->applySetting("OtherPauseEnabled");
    _settings->applySettings();
}

static bool sortByIndex(const QNetworkInterface& i1, const QNetworkInterface& i2)
{
    return i1.index()<i2.index();
}
static bool sortByMask(const QNetworkAddressEntry& i1, const QNetworkAddressEntry& i2)
{
    return i1.netmask().toIPv4Address()<i2.netmask().toIPv4Address();
}


QString SessionController::detectValidIpAddress()
{
    QList<QHostAddress>	allAddresses = QNetworkInterface::allAddresses();
//    if( !_ipAddress.isEmpty() && allAddresses.contains(QHostAddress(_ipAddress)))
//        return _ipAddress;

//    _ipAddress = QString();
    QString ipAddress;

    QList<QNetworkInterface> allInterfaces = QNetworkInterface::allInterfaces();
    qSort(allInterfaces.begin(),allInterfaces.end(),&sortByIndex);
    QNetworkInterface netInterface;
    qDebug()<<"VoIP: Detecting IP address. Network interface list:";
    foreach( QNetworkInterface curr, allInterfaces )
    {
        qDebug()<<curr;
        bool isUp= curr.flags().testFlag(QNetworkInterface::IsUp);
        bool isLoopback = curr.flags().testFlag(QNetworkInterface::IsLoopBack);
        bool hasEntries = curr.addressEntries().count()>0;
        if( isUp && !isLoopback && hasEntries)
        {
            if(!netInterface.isValid())
                netInterface = curr;
        }
    }
    qDebug()<<"VoIP: Selected network interface:"<<netInterface;

    QList<QNetworkAddressEntry> entryList = netInterface.addressEntries();
    qSort(entryList.begin(),entryList.end(),&sortByMask);
    QList<QString> addrList;
    foreach(QNetworkAddressEntry addr, entryList)
    {
        if( addr.ip().protocol() == QAbstractSocket::IPv4Protocol )
        {
            addrList.append(addr.ip().toString());
        }
    }

    if(addrList.count()>0)
        ipAddress = addrList[0];

    if(ipAddress.isEmpty() && !allAddresses.isEmpty())
        ipAddress = allAddresses[0].toString();
    return ipAddress;
}



}
