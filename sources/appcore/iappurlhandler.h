#ifndef IAPPACTIONHANDLER_H
#define IAPPACTIONHANDLER_H

#include "appcore.h"

namespace ccappcore
{
    class APPCORE_EXPORT IAppUrlHandler
    {
    public:
        virtual ~IAppUrlHandler() {}
        virtual bool openUrl(const QUrl& url) = 0;
    };
}

Q_DECLARE_INTERFACE(ccappcore::IAppUrlHandler, "ru.forte-it.ccappcore.iappurlhandler/1.0")

#endif // IAPPACTIONHANDLER_H
