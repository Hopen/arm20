#ifndef DOMNODELISTITERATOR_H
#define DOMNODELISTITERATOR_H

#include <QDomNodeList>
#include <QDomElement>
#include <QDomNode>


template<typename NodeType>
class DomNodeListIterator
{
public:

    template<typename TRet> NodeType convertTo(QDomNode node);

    DomNodeListIterator() : index(0) {}
    explicit DomNodeListIterator(QDomNodeList list) : list(list), index(0)  {}
    DomNodeListIterator(const DomNodeListIterator& other) : list(other.list), index(other.index) {}

    NodeType operator*()
    {
        return isValid() ? convertTo<NodeType>(list.item(index)) : NodeType();
    }

    bool isValid() const { return index >= 0 && index < list.count(); }

    DomNodeListIterator& operator++()
    {
        ++index;
        return *this;
    }

    DomNodeListIterator operator++(int)
    {
        DomNodeListIterator tmp = *this;
        operator ++();
        return tmp;
    }

    bool operator==(const DomNodeListIterator& r) const
    {
        if(!isValid() && !r.isValid())
            return true;

        return list == r.list;
    }

    bool operator!=(const DomNodeListIterator& r) const
    {
        return !operator ==(r);
    }

private:
    QDomNodeList list;
    int index;
};

class DomElementList
{
public:

    QDomNodeList list;

    DomElementList(QDomNodeList list) : list(list) {}

    typedef DomNodeListIterator<QDomElement> const_iterator;

    const_iterator begin() const { return const_iterator(list); }
    const_iterator end() const { return const_iterator(); }
};

template<>
template<> inline QDomElement DomNodeListIterator<QDomElement>::convertTo<QDomElement>(QDomNode node)
{
    return node.toElement();
}


#endif // DOMNODELISTITERATOR_H
