# -------------------------------------------------
# Project created by QtCreator 2009-03-30T13:26:05
# -------------------------------------------------
QT += network xml script
TARGET = appcore
TEMPLATE = lib
DESTDIR = $$OUT_PWD/../bin

#!static: QT += dbus

contains( CONFIG, static) {
  CONFIG  += staticlib
} else {
  CONFIG  += dll
}

contains( CONFIG, static) {
  CONFIG  += staticlib
  DEFINES += QT_NODLL
  DEFINES += QCA_STATIC
  DEFINES += QXMPP_STATIC
}

DEFINES -= QABSTRACTSOCKET_DEBUG

DEFINES += APPCORE_PACKAGE \
    OPTIONSTREE_EXPORT
RESOURCES +=

LIBS += -L$$DESTDIR
LIBS += -lthirdparty

INCLUDEPATH += ../thirdparty/log4qt/src \
    ../thirdparty/psi/src/tools \
    ../thirdparty/cds3 \
    ../thirdparty/qxmpp-0.7.6/src/base \
    ../thirdparty/qxmpp-0.7.6/src/client

PRECOMPILED_HEADER = appcore.h
HEADERS += functor.h \
    predicate.h \
    filteredmodel.h \
    qobjectutils.h \
    timeutils.h \
    proxymodeltree.h \
    proxymodellist.h \
    proxymodelshared.h \
    focuseventtranslator.h \
    domnodelistiterator.h \
    operationcontroller.h \
    redirectlistcontroller.h \
    spamlistcontroller.h \
    scriptadapter.h \
    ScriptEngine.h \
    JabberController.h \
    globaladdressbookcontroller.h

SOURCES += timeutils.cpp \
    proxymodeltree.cpp \
    proxymodellist.cpp \
    focuseventtranslator.cpp \
    operationcontroller.cpp \
    ../thirdparty/cds3/consts.cpp \
    redirectlistcontroller.cpp \
    spamlistcontroller.cpp \
    scriptadapter.cpp \
    JabberController.cpp \
    globaladdressbookcontroller.cpp

windows {
    #sspi support (for NTLM authentication)
    HEADERS += sspiwrapper.h sspiwrapper_defs.h
    SOURCES += sspiwrapper.cpp
    LIBS += -lsecur32
}

include("appcontract.pri")
include("appdomain.pri")

#DEFINES+=QT_NODLL
#QtCrypto
INCLUDEPATH+=../thirdparty/qca/include/QtCrypto
windows {
CONFIG(debug,debug|release): LIBS+=-lqcad
CONFIG(release,debug|release): LIBS+=-lqca
}
else {
LIBS+=-L$$DESTDIR -lqca
}

TRANSLATIONS += appcore_en.ts

XMPP_PATH = ../../sources/thirdparty/qxmpp-0.7.6

##INCLUDEPATH += XMPP_PATH

contains( CONFIG, static) {
  windows {
    CONFIG(debug,debug|release): LIBS+=-lqxmpp_d -L$$XMPP_PATH/src
    CONFIG(release,debug|release): LIBS+=-lqxmpp -L$$XMPP_PATH/src
  }
} else {
  windows {
    CONFIG(debug,debug|release): LIBS+=-lqxmpp_d4 -L$$XMPP_PATH/src
    CONFIG(release,debug|release): LIBS+=-lqxmpp4 -L$$XMPP_PATH/src
  }

  windows {
    INCLUDEPATH+= d:/Qt_source_4.8.3/lib
    CONFIG(debug,debug|release): LIBS+=-lQtDBusd4 -Ld:/Qt_source_4.8.3/lib
    CONFIG(release,debug|release): LIBS+=-lQtDBus4 -Ld:/Qt_source_4.8.3/lib
  }
}

#unix {
#  CONFIG(debug,debug|release): LIBS+=../../sources/thirdparty/qxmpp-0.7.6/src/libqxmpp_d.so
#  CONFIG(release,debug|release): LIBS+=../../sources/thirdparty/qxmpp-0.7.6/src/libqxmpp.so
#}














