#include "apploader.h"

#include <QtCrypto>
#include <QMessageBox>

namespace ccappcore
{

//QT_BEGIN_NAMESPACE
//Q_IMPORT_PLUGIN(pnp_operwidgets)
//QT_END_NAMESPACE


    AppLoader::AppLoader()
    {
    }

    bool AppLoader::load(QString appRootPath) throw()
    {
        return loadPlugins(appRootPath);
    }

    bool AppLoader::loadPlugins(QString appRootPath) throw()
    {
        QDir dir(appRootPath);
        QStringList filters;
        filters << "ccplugin*.dll" << "libccplugin*.so"; // dll - win32, so - nix
        dir.setNameFilters(filters);

        QStringList fileNames = dir.entryList(QDir::Files, QDir::Name);
        qDebug()<<"Loading plugins. Probing "<<fileNames;
        foreach (QString fileName, fileNames)
        {
            if(fileName.toLower().startsWith("qt")) //ignore qt dlls
                continue;

            QPluginLoader loader(dir.absoluteFilePath(fileName));
            qDebug().nospace()<<"Loading "<<fileName;
            loader.load();
            if(!loader.isLoaded())
            {
                qDebug()<< "Load failed: "<<loader.errorString();
                QMessageBox::critical(NULL,
                                      QObject::tr("Ошибка загрузки приложения"),
                                      QObject::tr("Не удалось загрузить плагин: \n%1, \nошибка: %2").arg(fileName).arg(loader.errorString()));

                return false;
            }

            QObject *plugin = loader.instance();
            if (plugin)
            {
                qDebug()<<"Plugin loaded:"<<fileName;
                loadedModules.append(fileName);
                loadedPlugins.append(plugin);
            }
        }

        foreach (QObject* plugin, getPluginsList())
        {
            IAppModule* coreModule = qobject_cast<IAppModule*>(plugin);
            if(coreModule)
                coreModule->initialize();
            QCAPlugin* qcaPlugin  = qobject_cast<QCAPlugin*>(plugin);
            if(qcaPlugin)
                QCA::insertProvider(qcaPlugin->createProvider());
        }

        return true;
    }
}
