#include "appurlhandler.h"
#include "iappurlhandler.h"
#include <QDesktopServices>

namespace ccappcore
{

AppUrlHandler::AppUrlHandler(QObject *parent)
    : QObject(parent)
{
    QDesktopServices::setUrlHandler("ccapp",this,"openUrl");
}

void AppUrlHandler::openUrl(const QUrl& url)
{
    qDebug()<<"AppUrlHandler::openUrl( '"<<url<<"' )";
    QList<IAppUrlHandler*> listHandlers = ServiceProvider::instance().findServices<IAppUrlHandler>();
    foreach(IAppUrlHandler* handler, listHandlers)
    {
        QObject* obj = dynamic_cast<QObject*>(handler);
        if(!obj)
            continue;
        QString host = url.host();
        QString className = obj->metaObject()->className();
        if( host.toLower()!=className.toLower() )
            continue;

        if(handler->openUrl(url))
            return;
    }
}

}
