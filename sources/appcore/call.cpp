#include "call.h"
#include "callscontroller.h"
#include "operlistcontroller.h"

namespace ccappcore
{
Call::Call(CallsController* controller, int callId, CallType callType)
        : d(new Call::Private(controller, callId, callType))
{    
}

Call::Call()
        : d(new Call::Private(NULL, -1, CT_Unknown))
{
}

Call::~Call()
{
}

void Call::setCallId(CallsController* controller, int callId)
{
    d->controller = controller;
    d->callId = callId;
}

void Call::setOperId(ContactId id)
{
    d->operId = id;
}

bool Call::operator < (const Call& other) const
{
    //by default, sort calls by: state asc -> priority desc -> state time desc

    if(callState()<other.callState())
        return true;
    if(callState()>other.callState())
        return false;

    if(priority()<other.priority())
        return true;
    if(priority()>other.priority())
        return false;

    if(stateTime()<other.stateTime())
        return true;
    if(stateTime()>other.stateTime())
        return false;

    return callId()<other.callId();
}

//The method mostly for debugging purposes
QString Call::callStateString() const
{
    CallState cs = callState();
    switch(cs)
    {
        case CS_Ivr:
            return QObject::tr("В IVR");
        case CS_Queued:
            return QObject::tr("В очереди");
        case CS_Assigned:
            return QObject::tr("Распределен");
        case CS_Calling:
        case CS_RingStarted:
            return QObject::tr("Распределен");
        case CS_Connected:
            return QObject::tr("Соединен");
        case CS_Muted:
            return QObject::tr("На паузе");
        case CS_OnHold:
            return QObject::tr("На паузе");
        case CS_CallFailed:
            return QObject::tr("Ошибка дозвона");
        case CS_Completed:
            return QObject::tr("Завершен");
        case CS_Conferencing:
            return QObject::tr("Конференция");
        default:
            return QObject::tr("Неизвестно");
    }
}

QString Call::callResultString(CallResult cr)
{
    switch(cr)
    {
    case CR_Busy:
        return QObject::tr("Номер занят");
    case CR_NotExists:
        return QObject::tr("Номер не существует");
    case CR_NoAnswer:
        return QObject::tr("Номер не отвечает");
    default:
        return QObject::tr("Ошибка дозвона (%1)").arg(cr);
    }
}

QString Call::callTypeString(CallType ct)
{
    switch(ct)
    {
    case CT_Incoming:
        return QObject::tr("Входящий");
    case CT_Outgoing:
        return QObject::tr("Исходящий");
    case CT_Operator:
        return QObject::tr("Оператор");
    default:
        return QObject::tr("Неизвестно");
    }
}

CallInfo Call::addExtraInfo(CallInfoType ci, QDateTime timeAdded, const QString& value, Contact whoAdded)
{
    CallInfo v(ci,value,whoAdded,timeAdded);
    d->callInfo.insertMulti(ci,v);
    return v;
}

QString Call::extraInfo(CallInfoType infoType) const
{
    if( !d->callInfo.contains(infoType) )
        return QString();

    return d->controller->callInfoString(d->callInfo.value(infoType));
}

bool Call::extraInfo(const CallInfoType& infoType, MultiInfo& info) const
{
    if( !d->callInfo.contains(infoType) )
        return false;

    info = d->callInfo.values(infoType);

    return true;
}

void Call::removeExtraInfo(CallInfoType ci)
{
    d->callInfo.remove(ci);
}

void Call::setCallState(CallState callState)
{
    d->callState = callState;
}

void Call::setGroupId(ContactId groupId)
{
    if(groupId == d->groupId)
        return;
    d->groupId = groupId;
}

CallInfo::CallInfo()
    : _type(Call::CI_Unknown)
{

}

CallInfo::CallInfo(Call::CallInfoType ciType,
         const QString info,
         Contact whoAdded,
         const QDateTime& dtAdded)
     : _type(ciType)
     , _value(info)
     , _timeAdded(dtAdded)
     , _whoAdded(whoAdded)
{

}

}
