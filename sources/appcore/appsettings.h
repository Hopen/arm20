#ifndef APPSETTINGS_H
#define APPSETTINGS_H

#include <QObject>
#include <QString>

#include "appcore.h"

#include "serviceprovider.h"
#include "message.h"
#include <memory>
#include "lateboundobject.h"

namespace ccappcore
{


enum eConnectionType
{
    CT_TEL = 0,
    CT_VOIP
};

const QString LANG_RUS = "RU";
const QString LANG_ENG = "EN";

class AppSettings;


class APPCORE_EXPORT IAppSettingsHandler
{
    public:

    //Apply service configuration.
    virtual void applySettings(const AppSettings&) {}

    //Flush service configuration
    virtual void flushSettings(AppSettings&) {}
};
}

Q_DECLARE_INTERFACE(ccappcore::IAppSettingsHandler, "ru.forte-it.ccappcore.iappsettingshandler/1.0")

using ccappcore::ServiceProvider;

namespace ccappcore
{

class Contact;
class IRouterClient;

class APPCORE_EXPORT AppSettings: public QObject
{
    Q_OBJECT;
public:

    AppSettings( QObject *parent = 0 );

    QVariant getOption(const QString& name, const QVariant& defaultValue = QVariant()) const;
    void setAppOption(const QString& name, const QVariant& value );
    void setUserOption(const QString& name, const QVariant& value );

    void loadSettings(); //Load settings from storage
    void applySettings(); //Apply configuration to services
    void flushSettings(); //Flush services configuration
    void saveSettings(); //Save settings to the storage

    void loadAppSettings();
    void saveAppSettings();

    void loadUserSettings();
    void saveUserSettings();

    QFuture<void> loadServerSettings(/*ccappcore::*/Contact loadForOper);
    QFuture<void> saveServerSettings(/*ccappcore::*/Contact saveForOper);

    void applyCDSSettings();
    void applySetting(const QString& name);

public:
    struct TOption
    {
        TOption(const QString& name, const QVariant& value):
            _name(name),
            _value(value)
        {

        }

        QString  _name;
        QVariant _value;
    };

    typedef QList <TOption> OptionsContainer;
    //void test ();

signals:
    void switchSessionLanguage(QString id);

public slots:
    void onLoadServerSettings(const ccappcore::Message& m);
    //void onUpdateLanguage(QString text);
private:
    void applySettings(IAppSettingsHandler* service);
    void flushSettings(IAppSettingsHandler* service);

    class Private;
    std::auto_ptr<Private> d;

    QFutureInterface< void > _loadSettingsResult;
    LateBoundObject<IRouterClient> _router;

    OptionsContainer _serverOptions;

};

}

#endif // APPSETTINGS_H
