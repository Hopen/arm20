#ifndef CORECONTRACT_H
#define CORECONTRACT_H

#include "appcore.h"
#include "serviceprovider.h"

namespace ccappcore
{
    class APPCORE_EXPORT IAppModule
    {
    public:
        virtual ~IAppModule() {}

        virtual void initialize() throw() = 0;
    };

}

Q_DECLARE_INTERFACE(ccappcore::IAppModule, "ru.forte-it.ccappcore.iappmodule/1.0")

#endif // CORECONTRACT_H
