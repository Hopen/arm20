#include "callslistmodel.h"
#include "timeutils.h"
#include "useritemdatarole.h"

namespace ccappcore
{

CallsListModel::CallsListModel(QObject* parent)
    : QAbstractListModel(parent)
    , _fixedCallsList(false)
{
    //default visible columns
    QList<ColDef> columns;
    columns
            <<AbonPhoneColumn
            <<CallTypeColumn
            <<CallStateColumn
            <<GroupNameColumn
            <<BeginTimeColumn
            <<StateTimeColumn
            <<PriorityColumn
            <<ExtraInfoColumn
            <<LastOperNameColumn
            <<CallLengthColumn;
    setVisibleColumns(columns);


    connect(_callsController.instance(), SIGNAL(callStateChanged(ccappcore::Call)),
            this, SLOT(callStateChanged(ccappcore::Call)));
    connect(_callsController.instance(), SIGNAL(callAdded(ccappcore::Call)),
            this, SLOT(addCall(ccappcore::Call)));
    connect(&_stateTimeTimer, SIGNAL(timeout()),
            this, SLOT(emitStateTimeChanged()));

    applyFilter(_filter);
    emit layoutChanged();
}

void CallsListModel::setCalls(const QList<Call>& callsList)
{
    emit layoutAboutToBeChanged();
    _fixedCallsList = true;
    _callsController->disconnect(this);
    _calls = callsList;
    qSort(_calls);
    emit layoutChanged();
}

void CallsListModel::setVisibleColumns(const QList<ColDef>& columns)
{
   emit layoutAboutToBeChanged();
    _visibleColumns = columns;
   emit layoutChanged();

   if(_visibleColumns.contains(StateTimeColumn))
       _stateTimeTimer.start(5000);
   else
       _stateTimeTimer.stop();
}

void CallsListModel::applyFilter(const PredicateGroup<Call>& filter)
{
    emit layoutAboutToBeChanged();
    _filter = filter;

    if(_fixedCallsList)
        return;

    _calls = _callsController->findCalls(filter);
    qSort(_calls);
    emit layoutChanged();
}

void CallsListModel::addCall(ccappcore::Call call)
{
    if(!call.isValid())
        return;

    if(!_filter.matches(call))
        return;

    if(_calls.contains(call))
        return;

    emit layoutAboutToBeChanged();
    _calls.insert(0, call);
    qSort(_calls);
    emit layoutChanged();
}

void CallsListModel::removeCall(ccappcore::Call call)
{
    QModelIndex existingIndex = indexOf(call);
    if(existingIndex.isValid())
    {
        QAbstractItemModel::beginRemoveRows( QModelIndex(), existingIndex.row(), existingIndex.row() );
        //emit layoutAboutToBeChanged();
        _calls.removeAt(existingIndex.row());
        //emit layoutChanged();
        QAbstractItemModel::endRemoveRows();
    }
}

QVariant CallsListModel::headerData(int section,
                                    Qt::Orientation orientation,
                                    int role ) const
{
    if(orientation!=Qt::Horizontal)
        return QAbstractListModel::headerData(section,orientation,role);

    if(section<0 || section>=_visibleColumns.count())
        return QVariant();

    if(role == Qt::TextAlignmentRole)
        return Qt::AlignLeft;

    ColDef column = _visibleColumns[section];
    if(role == Qt::DisplayRole)
    {
        switch(column)
        {
        case AbonPhoneColumn:
                return tr("Номер абонента");
        case CallStateColumn:
                return tr("Состояние");
        case GroupNameColumn:
                return tr("Группа");
        case CallTypeColumn:
                return tr("Тип");
        case BeginTimeColumn:
                return tr("Время начала");
        case StateTimeColumn:
                return tr("Время состояния");
        case PriorityColumn:
                return tr("Приоритет");
        case ExtraInfoColumn:
                return tr("Доп. информация");
        case LastOperNameColumn:
                return tr("Последний оператор");
        case CallLengthColumn:
                return tr("Длительность звонка");
        }
    }
    return QAbstractListModel::headerData(section,orientation,role);
}

int CallsListModel::columnCount(const QModelIndex &parent) const
{    
    return parent.isValid() ? 0 : _visibleColumns.count();
}

int CallsListModel::rowCount(const QModelIndex& parent) const
{
    if(parent.isValid())
        return 0; //no childs

    return _calls.count();
}

QVariant CallsListModel::data(const QModelIndex& index, int role) const
{
    Call call = callAt(index);
    if(!call.isValid())
        return QVariant();

    if(index.column()<0 || index.column()>=_visibleColumns.count())
        return QVariant();

    if(role == ccappcore::AssociatedObjectRole)
        return QVariant::fromValue(call);

    if(role == ccappcore::StateTimeRole)
    {
        if( call.isCompleted() )
            return QVariant(); //do not display state time for completed calls

        return call.stateTime();
    }

    if(role == ccappcore::StateReasonRole)
    {
        return call.stateReason();
    }

    ColDef column = _visibleColumns[index.column()];

    if(role == Qt::DisplayRole)
    {
        switch(column)
        {
        case AbonPhoneColumn:
            return _callsController->callAbonPhone(call);
        case CallStateColumn:
            return call.callStateString();
        case GroupNameColumn:
            return _callsController->callGroup(call).name();
        case CallTypeColumn:
            return call.callTypeString();
        case BeginTimeColumn:
                return call.beginDateTime();
        case StateTimeColumn:               
                return TimeUtils::timeElapsed(call.stateTime(), QDateTime::currentDateTime()).toString("HH:mm:ss");
        case PriorityColumn:
                return call.priority();
        case ExtraInfoColumn:
                return call.callExtraInfo();
        case LastOperNameColumn:
                return _callsController->callOper(call).name();
        case CallLengthColumn:
                return TimeUtils::timeElapsed(call.beginDateTime(), call.stateTime());
        }
    }   

    if(role == Qt::DecorationRole && index.column() == 0)
        return getCallStatusIcon(call);

    if(role == Qt::TextColorRole)
    {
        //TBD
    }
    return QVariant();
}

void CallsListModel::emitStateTimeChanged()
{
    int columnIndex = _visibleColumns.indexOf(StateTimeColumn);
    if(columnIndex<0)
        return;

    int count = rowCount(QModelIndex());
    if(count <= 0)
        return;

    QModelIndex topLeft = index(0,columnIndex,QModelIndex());
    QModelIndex bottomRight = index(count-1,columnIndex,QModelIndex());
    emit dataChanged(topLeft, bottomRight);
}


QIcon CallsListModel::getCallStatusIcon(ccappcore::Call call) const
{

    if(call.callType() == Call::CT_Incoming )
    {
        if(call.isCompleted())
            return QIcon(":/resources/icons/call_in_offline_16x16.png");
        else
            return QIcon(":/resources/icons/call_in_online_16x16.png");
    }

    //TODO: Deal with op call: Is OP call incoming or outgoing?
    if(call.isCompleted())
        return QIcon(":/resources/icons/call_out_offline_16x16.png");
    else
        return QIcon(":/resources/icons/call_out_online_16x16.png");
}


Call CallsListModel::callAt(const QModelIndex& index) const
{
    if( !index.isValid() )
        return Call::invalid();

    //simple list mode
    if( index.row() < 0 || index.row() >= _calls.count() )
        return Call::invalid();

    Call call = _calls.at( index.row() );
    return call;
}

void CallsListModel::callStateChanged(ccappcore::Call call)
{
    QModelIndex first = indexOf(call);
    QModelIndex last = index(first.row(), columnCount() -1, first.parent());
    if(!first.isValid())
    {
        addCall(call);
        return;
    }

    if(!_filter.matches(call))
    {
        removeCall(call);
        return;
    }

    emit layoutAboutToBeChanged();
    qSort(_calls);
    emit layoutChanged();
}

QModelIndex CallsListModel::indexOf(ccappcore::Call call) const
{
    int row = _calls.indexOf(call);
    QModelIndex itemIndex = index( row, 0, QModelIndex() );
    return itemIndex;
}

}
