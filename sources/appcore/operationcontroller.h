#ifndef OPERATIONCONTROLLER_H
#define OPERATIONCONTROLLER_H

//#include "operoperation.h"
#include "qobject.h"
#include "appcore.h"

namespace ccappcore
{

class OperOperation//: public QObject
{
    //Q_OBJECT
public:
    OperOperation(){};
    explicit OperOperation(const QString& text, const QString& iconpath, const quint8& num)
        :_text(text),_iconpath(iconpath),_opernum(num)
    {

    }

private:
    QString _text;
    QString _iconpath;
    quint8  _opernum;

public:
    void setText     (const QString& text     ){_text = text;}
    void setIconPath (const QString& iconpath ){_iconpath = iconpath;}
    void setOperNum  (const quint8 & opernum  ){_opernum = opernum;}

    QString text     ()const {return _text;}
    QString iconPath ()const {return _iconpath;}
    quint8  operNum  ()const {return _opernum;}

    friend QDataStream &operator<<(QDataStream &out, const OperOperation &rhs);
    friend QDataStream &operator>>(QDataStream &in, OperOperation &rhs);

    //friend void operator <<( QVariant& data, const OperOperation& target );
    //friend void operator >>( const QVariant& data, OperOperation& target );

};

inline QDataStream &operator<<(QDataStream &out, const OperOperation &rhs)
{
    out << rhs._text
           << rhs._iconpath
              << rhs._opernum;
    return out;
}

inline QDataStream &operator>>(QDataStream &in, OperOperation &rhs)
{
    in >> rhs._text
           >> rhs._iconpath
              >> rhs._opernum;
    return in;
}

//template<typename T>
//void operator <<( QVariant& data, const T& target )
//{
//    data = QVariant::fromValue<T>( target );
//}

//template<typename T>
//void operator >>( const QVariant& data, T& target )
//{
//    target = data.value<T>();
//}

//template<typename T>
//void operator <<( QVariant& data, const QList<T>& target )
//{
//    QVariantList list;
//    list.reserve( target.count() );
//    for ( int i = 0; i < target.count(); i++ ) {
//        QVariant item;
//        item << target[ i ];
//        list.append( item );
//    }
//    data = list;
//}

//template<typename T>
//void operator >>( const QVariant& data, QList<T>& target )
//{
//    QVariantList list = data.toList();
//    target.reserve( list.count() );
//    for ( int i = 0; i < list.count(); i++ ) {
//        T item;
//        list[ i ] >> item;
//        target.append( item );
//    }
//}

//void operator <<( QVariant& data, const OperOperation& target )
//{
//    QVariantMap map;
//    map[ "text"     ] << target._text;
//    map[ "iconpath" ] << target._iconpath;
//    map[ "opernum"  ] << target._opernum;
//    data << map;
//}

//void operator >>( const QVariant& data, OperOperation& target )
//{
//    QVariantMap map;
//    data >> map;
//    map[ "text"     ] >> target._text;
//    map[ "iconpath" ] >> target._iconpath;
//    map[ "opernum"  ] >> target._opernum;
//}


enum E_OperOperation
{
    OO_CALL = 1,
    OO_CALLEND,
    OO_CALLBACK,
    OO_WAITING,
    OO_IVR,
    OO_TRANSFER,
    OO_VOICEMAIL,
    OO_LOGIN,
    OO_REC,
    OO_TRANSFER_FORWARD,
    OO_TRANSFER_GROUP,
    OO_TRANSFER_PHONE,
    // 9... 99 - in reserve
    OO_USER = 100
};

////const QString OPERCALL       = QObject::tr( "Сделать звонок"   );
////const QString OPERCALLEND    = QObject::tr( "Повесить трубку"  );
////const QString OPERCALLBACK   = QObject::tr( "Перезвонить"      );
////const QString OPERWAITING    = QObject::tr( "Ожидание"         );
////const QString OPERIVR        = QObject::tr( "Перевести на IVR" );
////const QString OPERTRANSFER   = QObject::tr( "Перевести звонок" );
////const QString OPERVOICEMAIL  = QObject::tr( "Голосовая почта"  );
////const QString OPERLOGIN      = QObject::tr( "Перезайти"        );

//    class APPCORE_EXPORT OperationController: public QObject
//    {
//        Q_OBJECT
//    public:
//        typedef QVariantList OperationList;

//    public:
//        OperationController(QObject* parent = 0);
//        virtual ~OperationController(){}

//        OperationList* GetApplOperationList(){return &_applOper;}
//        OperationList* GetUserOperationList(){return &_userOper;}

//        QString OperationString(const quint8& operNum)const;
//        void initialize();
//    private:
//        OperationList _applOper;
//        OperationList _userOper;


//    private:
//        void _add(const QString &icon, const QString& text, const quint8& opernum, OperationList& list);

//    public:
//        void addApplOperation(const QString &icon, QString text, const quint8& opernum);
//        void addUserOperation(const QString &icon, const QString& text, const quint8& opernum);
//    };


} //namespace ccappcore
Q_DECLARE_METATYPE(ccappcore::OperOperation)

//class Test2: public QObject
//{
//    Q_OBJECT
//public:
//    Test2(QObject* parent = 0);//: QObject(parent) {}
//    //virtual ~Test2(){}

//};
#endif // OPERATIONCONTROLLER_H
