#include "styleloader.h"
#include <QWidget>
#include <QApplication>
#include <QIcon>
#include <QTextDocument>

namespace ccappcore
{

StyleLoader::StyleLoader(QObject* parent)
    : QObject(parent)
{
    if(QApplication::instance())
    {
        QString qss = loadStyleSheet(qApp);
        if(!qss.isEmpty())
            qApp->setStyleSheet(qss);
    }
}

void StyleLoader::load(QWidget* widget)
{
    QString qss;
    QTextStream stream(&qss);
    stream << loadStyleSheet("appglobal");
    stream << endl;
    stream << loadStyleSheet(widget);
    stream.flush();
    if(!qss.isEmpty())
        widget->setStyleSheet(qss);
}

QString StyleLoader::loadStyleSheet(QObject* obj)
{
    if(!obj)
        return QString();

    QString objName = obj->objectName();
    if(objName.isEmpty())
        objName = obj->metaObject()->className();
    return loadStyleSheet(objName);
}

QString StyleLoader::loadStyleSheet(const QString& name)
{
    QString fileName = QString("qss/%1.qss").arg(name.toLower());
    if (QFile::exists(fileName))
    {
        QFile f(fileName);
        if(!f.open(QFile::ReadOnly))
             return QString();
        return f.readAll();
    }

    QString resName = QString(":/qss/%1.qss").arg(name.toLower());
    if(QFile::exists(resName))
    {
        QFile f(resName);
        if(!f.open(QFile::ReadOnly))
             return QString();
        return f.readAll();
    }

    return QString();
}


}
