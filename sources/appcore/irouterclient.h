#ifndef IROUTERCLIENT_H
#define IROUTERCLIENT_H

#include "appcore.h"
#include "message.h"
#include "clientaddress.h"
#include "serviceprovider.h"

namespace ccappcore {

class APPCORE_EXPORT IRouterClient: public QObject
{
    Q_OBJECT;
    public:

        enum DisconnectReason
        {
                RouterDisconnected,
                ClientDisconnected,
                DisconnectedAfterError
        };

        enum ClientStatus
        {
                Connected = 1,
                Disconnected = 2,
        };

        enum EventSubscriptionStatus
        {
                Active,
                Subscribed,
                NotSubscribed
        };

    public:
        virtual ~IRouterClient() {}

        virtual bool isConnected() const = 0;

        virtual const ClientAddress& clientAddress() const = 0;

        virtual void connectToServer() = 0;
        virtual void connectToServer(const QString& host, int port) = 0;
        virtual void disconnectFromServer() = 0;

        virtual void waitForConnected(int msecs = 30000) = 0;
        virtual void waitForDisconnected(int msecs = 30000) = 0;

        virtual void send(const Message& msg, const ClientAddress& from, const ClientAddress& to) = 0;
        virtual void send(const Message& msg, const ClientAddress& to = ClientAddress::Any()) = 0;
        virtual void subscribeEvent(const QString& eventName) = 0;
        virtual void unsubscribeEvent(const QString& eventName) = 0;

        //router response timout, in milliseconds
        virtual int getResponseTimeout() = 0;
        virtual QString getError() const = 0;
    signals:
        void connected();
        void disconnected(IRouterClient::DisconnectReason reason);
        void error(const QString&);
        void statusChanged(quint32 clientId, IRouterClient::ClientStatus status);
        void messageReceived(const ccappcore::Message& msg, const ccappcore::ClientAddress& from, const ccappcore::ClientAddress& to);
        void messageSending(const ccappcore::Message& msg, const ccappcore::ClientAddress& from, const ccappcore::ClientAddress& to);
        void messageSent(const ccappcore::Message& msg, const ccappcore::ClientAddress& from, const ccappcore::ClientAddress& to);
        void monitorNoticeReceived(const ccappcore::Message& msg);
        void routeError(const ccappcore::Message& msg);
        void sibscribeNoticeReceived(quint32 eventHash, IRouterClient::EventSubscriptionStatus status, quint32 consumers, const QVector<quint32>& subscribers);
};

}

Q_DECLARE_INTERFACE(ccappcore::IRouterClient, "ru.forte-it.ccappcore.irouterclient/1.0")

#endif // IROUTERCLIENT_H
