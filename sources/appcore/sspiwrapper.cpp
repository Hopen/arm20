#include "sspiwrapper.h"
#include <assert.h>

namespace SSPIWrapper
{

#define TOKENBUFSIZE					12288

const wchar_t* SecurityPackage::szNegotiatePackageName	= L"Negotiate";
const wchar_t* SecurityPackage::szKerberosPackageName	= L"Kerberos";
const wchar_t* SecurityPackage::szNTLMPackageName		= L"NTLM";

#define STANDARD_CONTEXT_ATTRIBUTES		ISC_REQ_CONFIDENTIALITY | ISC_REQ_REPLAY_DETECT | ISC_REQ_SEQUENCE_DETECT | ISC_REQ_CONNECTION

SSPIException::SSPIException( const char* message, int errorCode ) 
    : _what(message)
	, _errorCode(errorCode)
{

}

Credential::Credential( SecurityPackagePtr package, CredentialType credentialType )
	: _securityPackage( package )
	, _credentialHandle( new CredHandle() )
{
	// create a new credential handle
	// note: we're allocating the CredHandle on the C++ heap.  This needs to be
	// explicitly deleted upon Disposal
	_credentialHandle->dwLower = 0; 
	_credentialHandle->dwUpper = 0;
	
	// determine credential use
	ULONG fCredentialUse = 0;
	switch (credentialType)
	{
	case /*CredentialType::*/Client:
		fCredentialUse = SECPKG_CRED_OUTBOUND;
		break;

	case /*CredentialType::*/Server:
		fCredentialUse = SECPKG_CRED_INBOUND;
		break;
	}

	// acquire credentials handle
	TimeStamp tsExpiry = { 0, 0 };
	SECURITY_STATUS sResult = _securityPackage->functionTable()->AcquireCredentialsHandleW(
		NULL,											// [in] name of principal. NULL = principal of current security context
		(LPWSTR)_securityPackage->packageName(),									// [in] name of package
		fCredentialUse,									// [in] flags indicating use.
		NULL,											// [in] pointer to logon identifier.  NULL = we're not specifying the id of another logon session
		NULL,											// [in] package-specific data.  NULL = default credentials for security package
		NULL,											// [in] pointer to GetKey function.  NULL = we're not using a callback to retrieve the credentials
		NULL,											// [in] value to pass to GetKey
		&*_credentialHandle,							// [out] credential handle (this must be already allocated)
		&tsExpiry										// [out] lifetime of the returned credentials
		);


	// check for errors
	if (sResult != SEC_E_OK)
		throw SSPIException("AcquireCredentialsHandle failed", sResult);
}

Credential::~Credential()
{
	SECURITY_STATUS sResult = _securityPackage->functionTable()->FreeCredentialsHandle(
		&*_credentialHandle							// [in] handle to free
		);
	// check for errors
	assert(sResult == SEC_E_OK);
}

std::wstring Credential::queryUserNameAttribute()
{
	// get the name associated with this credential
	SecPkgCredentials_NamesW secPkgCredentials_Names = { 0 };
	try
	{
		SECURITY_STATUS sResult = _securityPackage->functionTable()->QueryCredentialsAttributesW( &*_credentialHandle, 
			SECPKG_CRED_ATTR_NAMES, 
			&secPkgCredentials_Names);


		// check for errors
		if (sResult != SEC_E_OK)
			throw SSPIException("QueryCredentialsAttributes failed", sResult);


		// copy the name for the caller
		return secPkgCredentials_Names.sUserName;
	}
	catch(...)
	{
		// free the buffer
		if (secPkgCredentials_Names.sUserName != NULL)
		{
			_securityPackage->functionTable()->FreeContextBuffer(secPkgCredentials_Names.sUserName);
			secPkgCredentials_Names.sUserName = NULL;
		}
		throw;
	}
}

std::wstring Context::queryPackageNameAttribute()
{
	// get the SSP package associated with this context
	SecPkgContext_PackageInfoW secPkgContext_PackageInfo = { 0 };

	try
	{
		SECURITY_STATUS sResult = _securityPackage->functionTable()->QueryContextAttributesW(&*_contextHandle, 
			SECPKG_ATTR_PACKAGE_INFO, 
			&secPkgContext_PackageInfo);


		// check for errors
		if (sResult != SEC_E_OK)
			throw SSPIException("QueryContextAttributes failed", sResult);

		// copy the name for the caller
		std::wstring securityPackageName = secPkgContext_PackageInfo.PackageInfo->Name;
		return securityPackageName;
	}
	catch(...)
	{
		// free the buffer
		if (secPkgContext_PackageInfo.PackageInfo != NULL)
		{
			if (secPkgContext_PackageInfo.PackageInfo->Name != NULL)
			{
				_securityPackage->functionTable()->FreeContextBuffer(secPkgContext_PackageInfo.PackageInfo->Name);
				secPkgContext_PackageInfo.PackageInfo->Name = NULL;
			}

			if (secPkgContext_PackageInfo.PackageInfo->Comment != NULL)
			{
				_securityPackage->functionTable()->FreeContextBuffer(secPkgContext_PackageInfo.PackageInfo->Comment);
				secPkgContext_PackageInfo.PackageInfo->Comment = NULL;
			}

			_securityPackage->functionTable()->FreeContextBuffer(secPkgContext_PackageInfo.PackageInfo);
			secPkgContext_PackageInfo.PackageInfo = NULL;
		}
		throw;
	}
}

std::wstring Context::queryUserNameAttribute()
{
	// get the name associated with this context
	SecPkgContext_NamesW secPkgContext_Names = { 0 };


	try
	{
		SECURITY_STATUS sResult = _securityPackage->functionTable()->QueryContextAttributesW(&*_contextHandle,
			SECPKG_ATTR_NAMES, 
			&secPkgContext_Names);


		// check for errors
		if (sResult != SEC_E_OK)
			throw SSPIException("QueryContextAttributes failed", sResult);


		// copy the name for the caller
		return secPkgContext_Names.sUserName;
	}
	catch(...)
	{
		// free the buffer
		if (secPkgContext_Names.sUserName != NULL)
		{
			_securityPackage->functionTable()->FreeContextBuffer(secPkgContext_Names.sUserName);
			secPkgContext_Names.sUserName = NULL;
		}
		throw;
	}
}

ByteArray Context::signMessage( const ByteArray& msg )
{
	// this takes the given byte array, signs it, and returns another
	// byte array containing the original message and signature.
	// the format of the returned byte array is as follows:
	// |MESSAGE_LENGTH|MESSAGE|SIGNATURE|

	// get the size of the message
	DWORD cbMsg = msg.size();


	// get the size of the message length
	DWORD cbMsgLength = sizeof(cbMsg);


	// get the maximum size of a signature
	SecPkgContext_Sizes sizes;
	_securityPackage->functionTable()->QueryContextAttributesW(&*_contextHandle, SECPKG_ATTR_SIZES, &sizes);


	// allocate space for the signed message
	ByteArray signedMsg(cbMsgLength + cbMsg + sizes.cbMaxSignature);
	BYTE* pSignedMsg = &signedMsg[0]; 


	// copy the message length and message into the signed message buffer
	const BYTE* pMsg = &msg[0];
	CopyMemory(pSignedMsg, &cbMsg, cbMsgLength);
	CopyMemory(pSignedMsg+cbMsgLength, pMsg, cbMsg);
	pMsg = NULL;


	// prepare the message buffer
	SecBuffer rgsb[] = {
		{cbMsg,					SECBUFFER_DATA,  pSignedMsg + cbMsgLength},
		{sizes.cbMaxSignature,	SECBUFFER_TOKEN, pSignedMsg + cbMsg + cbMsgLength},
	};
	SecBufferDesc sbd = {SECBUFFER_VERSION, sizeof rgsb / sizeof *rgsb, rgsb};


	// sign the message
	SECURITY_STATUS sResult = _securityPackage->functionTable()->MakeSignature(
		&*_contextHandle,					// context to use
		0,										// quality of protection
		&sbd,									// message to sign
		0										// message sequence number
		);


	// release pinned objects
	pSignedMsg = NULL;

	// check for errors
	if (sResult != SEC_E_OK)
		throw SSPIException("MakeSignature failed", sResult);

	// return signed message to caller
	return signedMsg;
}

std::list<std::wstring> Context::contextAttributes()
{
	// only derived classes should have a valid ContextAttributes property
	return std::list<std::wstring>();
}

Context::Context( shared_ptr<Credential> credential ) 
	: _securityPackage(credential->securityPackage())
	, _credential(credential)
	, _contextHandle(new CtxtHandle)
    , _continueProcessing(true)
    , _contextAttributes(0)
{
	// create a new context handle
	_contextHandle->dwLower = 0; 
	_contextHandle->dwUpper = 0;
}

Context::~Context()
{
	// clean up
	SECURITY_STATUS sResult = _securityPackage->functionTable()->DeleteSecurityContext(&*_contextHandle);

	// check for errors
	assert(sResult == SEC_E_OK);
}

ByteArray Context::verifyMessage( const ByteArray& signedMsg )
{
	// this takes the given byte array and verifies it using the SSPI
	// VerifySignature method.  the given byte array is assumed to be of the format:
	// |MESSAGE_LENGTH|MESSAGE|SIGNATURE|

	// get a pinned reference to parameters appropriate to call unmanaged methods
	const BYTE* pSignedMsg = &signedMsg[0]; 


	// get the size of the message length
	DWORD cbMsgLength = sizeof(DWORD);


	// get the size of the original message
	DWORD cbMsg = 0;
	CopyMemory(&cbMsg, pSignedMsg, cbMsgLength);


	// get the size of the signature
	const DWORD cbSignature = signedMsg.size() - cbMsg - cbMsgLength;


	// prepare the message buffer
	SecBuffer rgsb[] = {
		{cbMsg,			SECBUFFER_DATA,		(void*)(pSignedMsg + cbMsgLength)},
		{cbSignature,	SECBUFFER_TOKEN,	(void*)(pSignedMsg + cbMsg + cbMsgLength)},
	};
	SecBufferDesc sbd = {SECBUFFER_VERSION, sizeof rgsb / sizeof *rgsb, rgsb};

	// sign the message
	ULONG qop = 0;
	SECURITY_STATUS sResult = _securityPackage->functionTable()->VerifySignature(
		&*_contextHandle,					// context to use
		&sbd,									// message to verify
		0,										// message sequence number
		&qop									// quality of protection
		);


	// retrieve the verified message (which is the original message passed to SignMessage)
	ByteArray verifiedMsg(cbMsg);
	BYTE* pVerifiedMsg = &verifiedMsg[0]; 
	CopyMemory(pVerifiedMsg, pSignedMsg + cbMsgLength, cbMsg);
	pVerifiedMsg = NULL;

	// release pinned objects
	pSignedMsg = NULL;

	// check for errors
	if (sResult != SEC_E_OK)
		throw SSPIException("VerifySignature failed", sResult);

	// return the verified message to the caller
	return verifiedMsg;
}

ByteArray Context::encryptMessage( const ByteArray& msg )
{
	// this takes the given byte array, encrypts it, and returns
	// another byte array containing the encrypted message.
	// the format of the returned byte array is as follows:
	// |MESSAGE_LENGTH|ENCRYPTED_MESSAGE|

	// get the size of the message
	DWORD cbMsg = msg.size();

	// get the size of the message length
	DWORD cbMsgLength = sizeof(cbMsg);

	// get the maximum size of a signature
	SecPkgContext_Sizes sizes;
	_securityPackage->functionTable()->QueryContextAttributesW(&*_contextHandle, SECPKG_ATTR_SIZES, &sizes);

	// allocate space for the encrypted message
	ByteArray encryptedMsg(cbMsgLength + cbMsg + sizes.cbSecurityTrailer);
	BYTE* pEncryptedMsg = &encryptedMsg[0]; 

	// copy the message into the signed message buffer
	const BYTE* pMsg = &msg[0];
	CopyMemory(pEncryptedMsg, &cbMsg, cbMsgLength);
	CopyMemory(pEncryptedMsg+cbMsgLength, pMsg, cbMsg);
	pMsg = NULL;

	// prepare the message buffer
	SecBuffer rgsb[] = {
		{cbMsg,						SECBUFFER_DATA,  pEncryptedMsg + cbMsgLength},
		{sizes.cbSecurityTrailer,	SECBUFFER_TOKEN, pEncryptedMsg + cbMsgLength + cbMsg},
	};
	SecBufferDesc sbd = {SECBUFFER_VERSION, sizeof rgsb / sizeof *rgsb, rgsb};

	// sign the message
	SECURITY_STATUS sResult = _securityPackage->functionTable()->EncryptMessage(
		&*_contextHandle,					// context to use
		0,										// quality of protection
		&sbd,									// message to encrypt
		0										// message sequence number
		);

	// The encrypted message may have taken less space than was allocated.
	// It's important we return a correctly sized message to the caller.
	ByteArray sealedMsg(cbMsgLength + cbMsg + rgsb[1].cbBuffer);
	BYTE* pSealedMsg = &sealedMsg[0]; 
	CopyMemory(pSealedMsg, pEncryptedMsg, cbMsgLength + cbMsg + rgsb[1].cbBuffer);
	pSealedMsg = NULL;

	// release pinned objects
	pEncryptedMsg = NULL;

	// check for errors
	if (sResult != SEC_E_OK)
                throw SSPIException("Context::EncryptMessage: EncryptMessage failed",0);

	// return signed message to caller
	return sealedMsg;
}

ByteArray Context::decryptMessage( const ByteArray& encryptedMsg )
{
	// this takes the given byte array, decrypts it, and returns
	// the original, unencrypted byte array.
	// the given byte array is assumed to be of the format:
	// |MESSAGE_LENGTH|ENCRYPTED_MESSAGE|

	// get a pinned reference to parameters to the unmanaged API
	const BYTE* pEncryptedMsg = &encryptedMsg[0]; 


	// get the size of the message length
	DWORD cbMsgLength = sizeof(DWORD);


	// get the size of the original, unencrypted message
	DWORD cbMsg = 0;
	CopyMemory(&cbMsg, pEncryptedMsg, cbMsgLength);


	// get the size of the trailer
	const DWORD cbTrailer = encryptedMsg.size() - cbMsg - cbMsgLength;


	// prepare the message buffer
	SecBuffer rgsb[] = {
		{cbMsg,						SECBUFFER_DATA,  (void*)(pEncryptedMsg + cbMsgLength)},
		{cbTrailer,					SECBUFFER_TOKEN, (void*)(pEncryptedMsg + cbMsgLength + cbMsg)},
	};
	SecBufferDesc sbd = {SECBUFFER_VERSION, sizeof rgsb / sizeof *rgsb, rgsb};


	// sign the message
	ULONG qop = 0;
	SECURITY_STATUS sResult = _securityPackage->functionTable()->DecryptMessage(
		&*_contextHandle,							// context to use
		&sbd,									// message to decrypt
		0,										// expected sequence number
		&qop									// quality of protection
		);


	// retrieve the decrypted message
	std::vector<BYTE> decryptedMsg(cbMsg);
	BYTE* pDecryptedMsg = &decryptedMsg[0]; 
	CopyMemory(pDecryptedMsg, pEncryptedMsg + cbMsgLength, cbMsg);
	pDecryptedMsg = NULL;


	// release pinned objects
	pEncryptedMsg = NULL;


	// check for errors
	if (sResult != SEC_E_OK)
                throw SSPIException("Context::DecryptMessage: DecryptMessage failed",0);


	// return the decrypted message to the caller
	return decryptedMsg;
}

ByteArray Context::secBufferToByteArray( SecBuffer &outSecBuff )
{
	// helper function to copy SecBuffer buffers into byte arrays.  This is typically
	// used to populate this->_token.

	ByteArray outBuff;
	if (outSecBuff.cbBuffer > 0)
	{
		outBuff.resize(outSecBuff.cbBuffer);
		for (unsigned long i = 0;(i < outSecBuff.cbBuffer);i++)
			outBuff[i] = *((BYTE *)outSecBuff.pvBuffer + i);
	}
	return outBuff;
}

std::list<std::wstring> ClientContext::contextAttributes()
{
	// create a new collection
	std::list<std::wstring> attrList;

	// add all of the elements to the collection
	if (_contextAttributes & ISC_RET_DELEGATE)
		attrList.push_back(L"Delegate");
	if (_contextAttributes & ISC_RET_MUTUAL_AUTH)
		attrList.push_back(L"Mutual Authentication");
	if (_contextAttributes & ISC_RET_REPLAY_DETECT )
		attrList.push_back(L"Replay Detection");
	if (_contextAttributes & ISC_RET_SEQUENCE_DETECT)
		attrList.push_back(L"Sequence Detection");
	if (_contextAttributes & ISC_RET_CONFIDENTIALITY)
		attrList.push_back(L"Confidentiality");
	if (_contextAttributes & ISC_RET_USE_SESSION_KEY)
		attrList.push_back(L"Use Session Key");
	if (_contextAttributes & ISC_RET_USED_COLLECTED_CREDS)
		attrList.push_back(L"Used collected credential");
	if (_contextAttributes & ISC_RET_USED_SUPPLIED_CREDS)
		attrList.push_back(L"Used Supplied Credential");
	if (_contextAttributes & ISC_RET_ALLOCATED_MEMORY)
		attrList.push_back(L"Allocated Memory");
	if (_contextAttributes & ISC_RET_USED_DCE_STYLE)
		attrList.push_back(L"Used DCE Style");
	if (_contextAttributes & ISC_RET_DATAGRAM)
		attrList.push_back(L"Datagram");
	if (_contextAttributes & ISC_RET_CONNECTION)
		attrList.push_back(L"Connection");
	if (_contextAttributes & ISC_RET_INTERMEDIATE_RETURN)
		attrList.push_back(L"Intermediate Return");
	if (_contextAttributes & ISC_RET_CALL_LEVEL)
		attrList.push_back(L"Call Level");
	if (_contextAttributes & ISC_RET_EXTENDED_ERROR)
		attrList.push_back(L"Extended Error");
	if (_contextAttributes & ISC_RET_STREAM)
		attrList.push_back(L"Stream");
	if (_contextAttributes & ISC_RET_INTEGRITY)
		attrList.push_back(L"Integrity");
	if (_contextAttributes & ISC_RET_IDENTIFY)
		attrList.push_back(L"Identify");
	if (_contextAttributes & ISC_RET_NULL_SESSION)
		attrList.push_back(L"NULL Session");
	if (_contextAttributes & ISC_RET_MANUAL_CRED_VALIDATION)
		attrList.push_back(L"Manual Cred Validation");
	if (_contextAttributes & ISC_RET_RESERVED1)
		attrList.push_back(L"Reserved1");
	if (_contextAttributes & ISC_RET_FRAGMENT_ONLY)
		attrList.push_back(L"Fragment Only");

	// return the collection to the caller
	return attrList;
}

void ClientContext::initialize( const ByteArray& inToken )
{
	// prepare the input buffer
	SecBufferDesc inBuffDesc;
	SecBuffer inSecBuff;
	const BYTE* pInToken = &inToken[0];

	inBuffDesc.ulVersion = SECBUFFER_VERSION;
	inBuffDesc.cBuffers = 1;
	inBuffDesc.pBuffers = &inSecBuff;

	inSecBuff.cbBuffer = inToken.size();
	inSecBuff.BufferType = SECBUFFER_TOKEN;
	inSecBuff.pvBuffer = (void*)pInToken;

	// prepare the output buffer
	SecBufferDesc outBuffDesc;
	SecBuffer outSecBuff;
	BYTE outBuff[TOKENBUFSIZE];

	outBuffDesc.ulVersion = SECBUFFER_VERSION;
	outBuffDesc.cBuffers = 1;
	outBuffDesc.pBuffers = &outSecBuff;

	outSecBuff.cbBuffer = TOKENBUFSIZE;
	outSecBuff.BufferType = SECBUFFER_TOKEN;
	outSecBuff.pvBuffer = outBuff;


	// output parameters
	ULONG reqContextAttributes = STANDARD_CONTEXT_ATTRIBUTES;
	TimeStamp tsLifeSpan = { 0, 0 };


	// get a reference to the credential
	CredHandle *phCredential = _credential->handle();

	// get a pinned reference to the context
	ULONG* pulContextAttributes = &_contextAttributes;
	const wchar_t* pwszServerPrincipalName = NULL; 
	if ( _credential->securityPackage()->packageName() == SecurityPackage::szKerberosPackageName 
		|| _credential->securityPackage()->packageName() == SecurityPackage::szKerberosPackageName )
		pwszServerPrincipalName = _serverPrincipalName.c_str(); 


	// initialize the context
	SECURITY_STATUS sResult = _securityPackage->functionTable()->InitializeSecurityContextW(
		phCredential,										// [in] handle to the credentials
		&*_contextHandle,								// [in/out] handle of partially formed context. Always NULL the first time through
		(SEC_WCHAR*)pwszServerPrincipalName,							// [in] name of the target of the context. Not needed by NTLM
		reqContextAttributes,								// [in] required context attributes
		0,													// [reserved] reserved; must be zero
		SECURITY_NATIVE_DREP,								// [in] data representation on the target
		&inBuffDesc,										// [in/out] pointer to the input buffers.  Always NULL the first time through
		0,													// [reserved] reserved; must be zero
		&*_contextHandle,								// [in/out] receives the new context handle
		&outBuffDesc,										// [out] pointer to the output buffers
		pulContextAttributes,								// [out] receives the context attributes
		&tsLifeSpan											// [out] receives the life span of the security context
		);


	// we're finished with the credential pointer
	phCredential = NULL;


	// release pinned objects
	pInToken = NULL;
	pulContextAttributes = NULL;
	pwszServerPrincipalName = NULL;


	// check for errors
	if (sResult == SEC_E_OK)
		_continueProcessing = false;
	else if (sResult == SEC_I_CONTINUE_NEEDED)
		_continueProcessing = true;
	else
		throw SSPIException("InitializeSecurityContext failed", sResult);

	// Set the token property
	_token = secBufferToByteArray(outSecBuff);
}

ClientContext::ClientContext( ClientCredentialPtr credential, const std::wstring& serverPrincipal, ContextAttributeFlags contextAttributeFlags ) 
	: Context(credential)
{
	// if the caller sets the DELEGATE flag where the security package is NTLM,
	// throw an exception
	if ( (contextAttributeFlags & Delegate) && 
		(_credential->securityPackage()->packageName() == SecurityPackage::szNTLMPackageName) )
	{
                throw SSPIException("Delegate is not supported by NTLM",0);
	}


	// capture the server principal name
	_serverPrincipalName = serverPrincipal;

	// prepare the output buffer
	SecBufferDesc outBuffDesc;
	SecBuffer outSecBuff;
	BYTE outBuff[TOKENBUFSIZE];

	outBuffDesc.ulVersion = SECBUFFER_VERSION;
	outBuffDesc.cBuffers = 1;
	outBuffDesc.pBuffers = &outSecBuff;

	outSecBuff.cbBuffer = TOKENBUFSIZE;
	outSecBuff.BufferType = SECBUFFER_TOKEN;
	outSecBuff.pvBuffer = outBuff;


	// output parameters
	ULONG reqContextAttributes = STANDARD_CONTEXT_ATTRIBUTES;
	TimeStamp tsLifeSpan = { 0, 0 };


	// add requested context attributes
	if (contextAttributeFlags & Delegate)
		reqContextAttributes = reqContextAttributes | ISC_REQ_DELEGATE | ISC_REQ_MUTUAL_AUTH;
	if (contextAttributeFlags & MutualAuthentication)
		reqContextAttributes = reqContextAttributes | ISC_REQ_MUTUAL_AUTH;
	if (contextAttributeFlags & Identify)
		reqContextAttributes = reqContextAttributes | ISC_REQ_IDENTIFY;


	// get a reference to the credential
	CredHandle *phCredential = _credential->handle();

	// get a pinned reference to types appropriate for calling unmanaged methods
	ULONG* pulContextAttributes = &_contextAttributes;
	const wchar_t* pwszServerPrincipalName = NULL;
	if ( _credential->securityPackage()->packageName() == SecurityPackage::szKerberosPackageName
		 || _credential->securityPackage()->packageName() == SecurityPackage::szNegotiatePackageName )
		pwszServerPrincipalName = _serverPrincipalName.c_str(); 

	// initialize the context
	SECURITY_STATUS sResult = _securityPackage->functionTable()->InitializeSecurityContextW(
		phCredential,										// [in] handle to the credentials
		NULL,												// [in/out] handle of partially formed context. Always NULL the first time through
		(SEC_WCHAR*)pwszServerPrincipalName,							// [in] name of the target of the context. Not needed by NTLM
		reqContextAttributes,								// [in] required context attributes
		0,													// [reserved] reserved; must be zero
		SECURITY_NATIVE_DREP,								// [in] data representation on the target
		NULL,												// [in/out] pointer to the input buffers.  Always NULL the first time through
		0,													// [reserved] reserved; must be zero
		&*_contextHandle,									// [in/out] receives the new context handle (must be pre-allocated)
		&outBuffDesc,										// [out] pointer to the output buffers
		pulContextAttributes,								// [out] receives the context attributes
		&tsLifeSpan											// [out] receives the life span of the security context
		);


	// we're finished with the credential pointer
	phCredential = NULL;

	// release pinned objects
	pulContextAttributes = NULL;
	pwszServerPrincipalName = NULL;

	// check for errors
	if (sResult == SEC_E_OK)
		_continueProcessing = false;
	else if (sResult == SEC_I_CONTINUE_NEEDED)
		_continueProcessing = true;
	else
		throw SSPIException("InitializeSecurityContext failed.", sResult);

	// Set the token property
	_token = secBufferToByteArray(outSecBuff);
}

ServerContext::ServerContext( ServerCredentialPtr credential, const ByteArray& inToken ) 
	: Context(credential)
{
	// prepare the input buffer
	SecBufferDesc inBuffDesc;
	SecBuffer inSecBuff;
	const BYTE* pInToken = &inToken[0];

	inBuffDesc.ulVersion = SECBUFFER_VERSION;
	inBuffDesc.cBuffers = 1;
	inBuffDesc.pBuffers = &inSecBuff;

	inSecBuff.cbBuffer = inToken.size();
	inSecBuff.BufferType = SECBUFFER_TOKEN;
	inSecBuff.pvBuffer = (void*)pInToken;		

	// prepare the output buffer
	SecBufferDesc outBuffDesc;
	SecBuffer outSecBuff;
	BYTE outBuff[TOKENBUFSIZE];

	outBuffDesc.ulVersion = SECBUFFER_VERSION;
	outBuffDesc.cBuffers = 1;
	outBuffDesc.pBuffers = &outSecBuff;

	outSecBuff.cbBuffer = TOKENBUFSIZE;
	outSecBuff.BufferType = SECBUFFER_TOKEN;
	outSecBuff.pvBuffer = outBuff;


	// output parameters
	ULONG reqContextAttributes = STANDARD_CONTEXT_ATTRIBUTES;
	TimeStamp tsLifeSpan = { 0, 0 };

	// get a reference to the credential
	CredHandle *phCredential = _credential->handle();

	// get a pinned reference to the context attributes
	ULONG *pulContextAttributes = &_contextAttributes;

	// initialize the context
	SECURITY_STATUS sResult = _securityPackage->functionTable()->AcceptSecurityContext(
		phCredential,									// [in] handle to the credentials
		NULL,											// [in/out] handle of partially formed context.  Always NULL the first time through
		&inBuffDesc,									// [in] pointer to the input buffers
		reqContextAttributes,							// [in] required context attributes
		SECURITY_NATIVE_DREP,							// [in] data representation on the target
		&*_contextHandle,							// [in/out] receives the new context handle	
		&outBuffDesc,									// [in/out] pointer to the output buffers
		pulContextAttributes,							// [out] receives the context attributes
		&tsLifeSpan										// [out] receives the life span of the security context
		);


	// release pinned objects
	phCredential = NULL;
	pInToken = NULL;
	pulContextAttributes = NULL;


	// check for errors
	if (sResult == SEC_E_OK)
		_continueProcessing = false;
	else if (sResult == SEC_I_CONTINUE_NEEDED)
		_continueProcessing = true;
	else
		throw SSPIException("AcceptSecurityContext failed", sResult);

	// Set the token property
	_token = secBufferToByteArray(outSecBuff);
}

void ServerContext::accept( const ByteArray& inToken )
{
	// prepare the input buffer
	SecBufferDesc inBuffDesc;
	SecBuffer inSecBuff;
	const BYTE* pInToken = &inToken[0];

	inBuffDesc.ulVersion = SECBUFFER_VERSION;
	inBuffDesc.cBuffers = 1;
	inBuffDesc.pBuffers = &inSecBuff;

	inSecBuff.cbBuffer = inToken.size();
	inSecBuff.BufferType = SECBUFFER_TOKEN;
	inSecBuff.pvBuffer = (void*)pInToken;


	// prepare the output buffer
	SecBufferDesc outBuffDesc;
	SecBuffer outSecBuff;
	BYTE outBuff[TOKENBUFSIZE];

	outBuffDesc.ulVersion = SECBUFFER_VERSION;
	outBuffDesc.cBuffers = 1;
	outBuffDesc.pBuffers = &outSecBuff;

	outSecBuff.cbBuffer = TOKENBUFSIZE;
	outSecBuff.BufferType = SECBUFFER_TOKEN;
	outSecBuff.pvBuffer = outBuff;


	// output parameters
	ULONG reqContextAttributes = STANDARD_CONTEXT_ATTRIBUTES;
	TimeStamp tsLifeSpan = { 0, 0 };

	// get a reference to the credential
	CredHandle *phCredential = _credential->handle();

	// get a pinned reference to the context
	ULONG* pulContextAttributes = &_contextAttributes;

	// initialize the context
	SECURITY_STATUS sResult =_securityPackage->functionTable()->AcceptSecurityContext(
		phCredential,									// [in] handle to the credentials
		&*_contextHandle,								// [in/out] handle of partially formed context.  Always NULL the first time through
		&inBuffDesc,									// [in] pointer to the input buffers
		reqContextAttributes,							// [in] required context attributes
		SECURITY_NATIVE_DREP,							// [in] data representation on the target
		&*_contextHandle,								// [in/out] receives the new context handle
		&outBuffDesc,									// [in/out] pointer to the output buffers
		pulContextAttributes,							// [out] receives the context attributes
		&tsLifeSpan										// [out] receives the life span of the security context
		);


	// we're finished with the credential pointer
	phCredential = NULL;


	// release pinned objects
	pInToken = NULL;
	pulContextAttributes = NULL;


	// check for errors
	if (sResult == SEC_E_OK)
		_continueProcessing = false;
	else if (sResult == SEC_I_CONTINUE_NEEDED)
		_continueProcessing = true;
	else
		throw SSPIException("AcceptSecurityContext failed", sResult);


	// Set the token property
	_token = secBufferToByteArray(outSecBuff);
}

void ServerContext::impersonateClient( void )
{
	// impersonate the client
	SECURITY_STATUS sResult = _securityPackage->functionTable()->ImpersonateSecurityContext(
		&*_contextHandle										// [in] handle of context to impersonate
		);

	// check for errors
	if (sResult != SEC_E_OK)
		throw SSPIException("ImpersonateSecurityContext failed", sResult);
}

void ServerContext::revertImpersonation( void )
{
	// revert the impersonation
	SECURITY_STATUS sResult = _securityPackage->functionTable()->RevertSecurityContext(
		&*_contextHandle										// [in] handle of context to revert
		);


	// check for errors
	if (sResult != SEC_E_OK)
		throw SSPIException("RevertSecurityContext failed", sResult);
}

std::list<std::wstring> ServerContext::contextAttributes()
{
	// create a new collection
	std::list<std::wstring> attrList;

	// add all of the elements to the collection
	if (_contextAttributes & ASC_RET_DELEGATE)
		attrList.push_back(L"Delegate");
	if (_contextAttributes & ASC_RET_MUTUAL_AUTH)
		attrList.push_back(L"Mutual Authentication");
	if (_contextAttributes & ASC_RET_REPLAY_DETECT )
		attrList.push_back(L"Replay Detection");
	if (_contextAttributes & ASC_RET_SEQUENCE_DETECT)
		attrList.push_back(L"Sequence Detection");
	if (_contextAttributes & ASC_RET_CONFIDENTIALITY)
		attrList.push_back(L"Confidentiality");
	if (_contextAttributes & ASC_RET_USE_SESSION_KEY)
		attrList.push_back(L"Use Session Key");
	if (_contextAttributes & ASC_RET_ALLOCATED_MEMORY)
		attrList.push_back(L"Allocated Memory");
	if (_contextAttributes & ASC_RET_USED_DCE_STYLE)
		attrList.push_back(L"Used DCE Style");
	if (_contextAttributes & ASC_RET_DATAGRAM)
		attrList.push_back(L"Datagram");
	if (_contextAttributes & ASC_RET_CONNECTION)
		attrList.push_back(L"Connection");
	if (_contextAttributes & ASC_RET_CALL_LEVEL)
		attrList.push_back(L"Call Level");
	if (_contextAttributes & ASC_RET_THIRD_LEG_FAILED)
		attrList.push_back(L"Third Leg Failed");
	if (_contextAttributes & ASC_RET_EXTENDED_ERROR)
		attrList.push_back(L"Extended Error");
	if (_contextAttributes & ASC_RET_STREAM)
		attrList.push_back(L"Stream");
	if (_contextAttributes & ASC_RET_INTEGRITY)
		attrList.push_back(L"Integrity");
	if (_contextAttributes & ASC_RET_LICENSING)
		attrList.push_back(L"Licensing");
	if (_contextAttributes & ASC_RET_IDENTIFY)
		attrList.push_back(L"Identify");
	if (_contextAttributes & ASC_RET_NULL_SESSION)
		attrList.push_back(L"NULL Session");
	if (_contextAttributes & ASC_RET_ALLOW_NON_USER_LOGONS)
		attrList.push_back(L"Allow Non User Logons");
	if (_contextAttributes & ASC_RET_ALLOW_CONTEXT_REPLAY)
		attrList.push_back(L"Allow Context Replay");
	if (_contextAttributes & ASC_RET_FRAGMENT_ONLY)
		attrList.push_back(L"Fragment Only");

	// return the collection to the caller
	return attrList;
}

HANDLE ServerContext::queryClientSecurityToken()
{

	// retrieve the token
	HANDLE hToken = NULL;
        throw SSPIException("ServerContext::queryClientSecurityToken() - not implemented",0);
        //requires using InitSecurityInterface function

//	SECURITY_STATUS sResult = QuerySecurityContextToken(&*_contextHandle, &hToken);


//	// check for errors
//	if (sResult != SEC_E_OK)
//		throw SSPIException("QuerySecurityContextToken failed", sResult);

	// return the handle to the caller
	return hToken;
}

std::list<std::wstring> SecurityPackage::queryAllSecurityPackages()
{
	// create a new collection
	std::list<std::wstring> packageNames;

	// list of packages
	SecPkgInfoW *ppSecPkgInfo = NULL;

	SECURITY_STATUS sRes = 0;
	try
	{
		// get the list of packages
		ULONG cPackages = 0;
		SECURITY_STATUS sResult = functionTable()->EnumerateSecurityPackagesW(&cPackages, &ppSecPkgInfo);


		// check for errors
		if (sResult != SEC_E_OK)
			throw SSPIException("EnumerateSecurityPackages failed", sResult);


		// add the security package names to the collection
		for (ULONG i = 0;(i < cPackages);i++)
		{
			packageNames.push_back(ppSecPkgInfo[i].Name);		
		}
		// return the collection to the caller
		return packageNames;
	}
	catch(...)
	{
		// free memory
		if (ppSecPkgInfo != NULL)
		{
			sRes = FreeContextBuffer(ppSecPkgInfo);
			ppSecPkgInfo = NULL;
		}
		throw;
	}
}

SecurityPackage::SecurityPackage( const wchar_t* packageName ) : _packageName(packageName)
	, _secFunctionTable(InitSecurityInterfaceW())
{

}

} //namespace SSPIWrapper
