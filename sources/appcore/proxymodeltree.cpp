#include <QStringList>
#include <QMimeData>
#include "proxymodeltree.h"

namespace ccappcore
{

ProxyModelTree::ProxyModelTree(QObject *parent)
        : QAbstractItemModel(parent)
        , _nextIndexId(0)
{
}

void ProxyModelTree::setSourceModelList(const ModelList& sourceList)
{
    beginResetModel();
    _sourceToMapped.clear();
    _mappedToSource.clear();
    _sourceModels = sourceList;
    _visibleModels.clear();
    foreach( ModelPtr model, _sourceModels )
    {
        connectModel(model);
        if(model->rowCount(QModelIndex()) > 0)
            _visibleModels.append(model);
    }
    endResetModel();
}

void ProxyModelTree::removeModel(ModelPtr model)
{
    QModelIndex mappedModelIndex = index(model.data());
    if(!mappedModelIndex.isValid())
        return;

    int row = mappedModelIndex.row();
    beginRemoveRows(QModelIndex(),row, row);
    model->disconnect(this);
    _sourceModels.removeOne(model);
    _visibleModels.removeOne(model);
    _mappedToSource.remove(mappedModelIndex);
    endRemoveRows();
}

void ProxyModelTree::insertModel(ModelPtr model, int pos)
{
    if(_sourceModels.contains(model))
        return;

    _sourceModels.append(model);
    connectModel(model);

    if(model->rowCount(QModelIndex())==0)
        return;

    if(pos<0) pos = 0;
    if(pos>_visibleModels.count()) pos = _visibleModels.count();

    beginInsertRows(QModelIndex(),pos,pos);
    _visibleModels.insert(pos,model);
    endInsertRows();
}

void ProxyModelTree::connectModel(ModelPtr model)
{
    connect(model.data(), SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(sourceModelDataChanged(QModelIndex,QModelIndex)));
    connect(model.data(), SIGNAL(modelReset()), this, SLOT(sourceModelReset()));
    connect(model.data(), SIGNAL(rowsAboutToBeInserted(QModelIndex,int, int)), this, SLOT(sourceModelRowsAboutToBeInserted(QModelIndex,int,int)));
    connect(model.data(), SIGNAL(rowsInserted(QModelIndex,int, int)), this, SLOT(sourceModelRowsInserted(QModelIndex,int,int)));
    connect(model.data(), SIGNAL(rowsAboutToBeRemoved(QModelIndex,int, int)), this, SLOT(sourceModelRowsAboutToBeRemoved(QModelIndex,int,int)));
    connect(model.data(), SIGNAL(rowsRemoved(QModelIndex,int, int)), this, SLOT(sourceModelRowsRemoved(QModelIndex,int,int)));
    connect(model.data(), SIGNAL(layoutAboutToBeChanged()), this, SLOT(sourceModelLayoutAboutToBeChanged()));
    connect(model.data(), SIGNAL(layoutChanged()), this, SLOT(sourceModelLayoutChanged()));
}

void ProxyModelTree::hideModel(ModelPtr model)
{
    int row = _visibleModels.indexOf(model,0);
    if(row<0)
        return;
    beginRemoveRows(QModelIndex(),row,row);
    _visibleModels.removeAt(row);
    endRemoveRows();
}

void ProxyModelTree::showModel(ModelPtr model)
{
    if(_visibleModels.contains(model))
        return;

    //get position to insert
    int row = _sourceModels.indexOf(model,0);
    if(row<0)
        return;

    if(row > _visibleModels.count())
        row = _visibleModels.count();

    beginInsertRows(QModelIndex(),row,row);
    _visibleModels.insert(row,model);
    endInsertRows();
}

int ProxyModelTree::rowCount(const QModelIndex& parent) const
{
    if(!parent.isValid())
        return _visibleModels.count();

    if( parent.internalPointer() == NULL )
    {
        if( parent.row()<0 || parent.row()>=_visibleModels.count() )
            return 0;

        const ModelPtr model = _visibleModels[parent.row()];
        return model->rowCount(QModelIndex());
    }

    QModelIndex sourceParent = _mappedToSource[parent];
    if(!sourceParent.isValid())
        return 0;

    return sourceParent.model()->rowCount(sourceParent);
}

void ProxyModelTree::setRootData(const ModelPtr m, const QVariant& data, int role)
{
    _rootData[m.data()][role] = data;
}

QVariant ProxyModelTree::data(const QModelIndex& index, int role) const
{
    if( !index.isValid() )
        return QVariant();

    if( index.internalPointer() == NULL )
    {
        if(index.column()!=0)
            return QVariant();

        ModelPtr model =modelAt(index);
        return getRootData(model,role);
    }

    QModelIndex sourceIndex = sourceIndexFromMappedIndex(index);
    return sourceIndex.model()->data(sourceIndex, role);

}

QVariant ProxyModelTree::getRootData(const ModelPtr model, int role) const
{
    QAbstractItemModel* key = model.data();
    if(_rootData.contains(key) && _rootData[key].contains(role))
        return _rootData[key][role];

    return QVariant();
}

Qt::ItemFlags ProxyModelTree::flags(const QModelIndex &index) const
{
    QModelIndex sourceIndex = sourceIndexFromMappedIndex(index);
    if(!sourceIndex.isValid())
        return QAbstractItemModel::flags(index);

    return sourceIndex.model()->flags(sourceIndex);
}

QModelIndex ProxyModelTree::index(int row, int column,
                              const QModelIndex &parent /*= QModelIndex()*/) const
{
    if( !parent.isValid() )
        return createIndex(row, column, (void*) NULL);

    QModelIndex sourceParent;
    const QAbstractItemModel* sourceModel;
    if( parent.internalPointer() == NULL )
    {
        sourceParent = QModelIndex();
        sourceModel = _visibleModels.at(parent.row()).data();
    }
    else
    {
        sourceParent = sourceIndexFromMappedIndex(parent);
        sourceModel = sourceParent.model();
    }

    QModelIndex sourceIndex = sourceModel->index(row, column, sourceParent);
    QModelIndex mappedIndex = mappedIndexFromSourceIndex(sourceIndex);
    return mappedIndex;
}

QModelIndex ProxyModelTree::index(const QAbstractItemModel* model) const
{
    int row = -1;
    for(int i=0; i<_visibleModels.count(); i++)
    {
        if(_visibleModels.at(i).data() == model)
        {
            row = i;
            break;
        }
    }
    if( row < 0 || row >= _visibleModels.count() )
        return QModelIndex();

    return createIndex(row, 0);
}

bool ProxyModelTree::isRootIndex(const QModelIndex& mappedIndex) const
{
    return mappedIndex.isValid() && !mappedIndex.parent().isValid();
}

QModelIndex ProxyModelTree::mappedIndexFromSourceIndex(const QModelIndex& index) const
{
    if(!index.isValid())
        return QModelIndex();

    QModelIndex mapppedIndex;
    if(_sourceToMapped.contains(index))
        mapppedIndex = _sourceToMapped[index];
    else
        mapppedIndex = createIndex(index.row(), index.column(), getNextIndexId());

    _mappedToSource[mapppedIndex] = index;
    _sourceToMapped[index] = mapppedIndex;
    return mapppedIndex;
}

QModelIndex ProxyModelTree::sourceIndexFromMappedIndex(const QModelIndex& index) const
{
    if( !index.isValid() || index.internalPointer() == NULL )
        return QModelIndex();

    QModelIndex sourceIndex;
    if(_mappedToSource.contains(index))
        sourceIndex = _mappedToSource[index];

    return sourceIndex;
}

QModelIndex ProxyModelTree::parent(const QModelIndex &child) const
{
    if( !child.isValid() )
        return QModelIndex();

    if( child.internalPointer() == NULL )
        return QModelIndex();

    QModelIndex sourceChild = sourceIndexFromMappedIndex(child);
    if(!sourceChild.isValid())
        return QModelIndex();
    QModelIndex sourceParent = sourceChild.model()->parent(sourceChild);
    QModelIndex mappedParent = sourceParent.isValid()
                              ? mappedIndexFromSourceIndex(sourceParent)
                              : index(sourceChild.model());
    return mappedParent;
}

int ProxyModelTree::columnCount(const QModelIndex &parent /*= QModelIndex()*/) const
{
    int columnCount = 0;
    if(!parent.isValid())
    {
       foreach(ModelPtr m, _sourceModels)
           columnCount = std::max(columnCount, m->columnCount(QModelIndex()));
    }
    else if(parent.internalPointer() == NULL)
    {
        ModelPtr model = modelAt(parent);
        columnCount = model->columnCount(QModelIndex());
    }
    else
    {
    QModelIndex sourceParent = sourceIndexFromMappedIndex(parent);
    columnCount = sourceParent.model()->columnCount(sourceParent);
    }

    return columnCount;
}

void ProxyModelTree::sourceModelDataChanged(const QModelIndex &topLeft,
                                               const QModelIndex &bottomRight)
{
    QModelIndex topLeftMapped = mappedIndexFromSourceIndex(topLeft);
    QModelIndex bottomRightMapped = mappedIndexFromSourceIndex(bottomRight);
    emit dataChanged(topLeftMapped, bottomRightMapped);
}

ModelPtr ProxyModelTree::modelPtr(QAbstractItemModel* model) const
{
    foreach(ModelPtr m, _sourceModels)
        if(m.data() == model)
            return m;

    return ModelPtr();
}

void ProxyModelTree::sourceModelReset()
{        
    emit layoutAboutToBeChanged();

    showModelIfNotEmpty(qobject_cast<QAbstractItemModel*>(sender()));

    _nextIndexId = 0;
    _mappedToSource.clear();
    _sourceToMapped.clear();

    emit layoutChanged();
}

void ProxyModelTree::sourceModelRowsAboutToBeInserted(const QModelIndex &parent, int first, int last)
{
    ModelPtr sourceModel = modelPtr(qobject_cast<QAbstractItemModel*>(sender()));
    if(sourceModel.isNull() || !_visibleModels.contains(sourceModel))
        return;

    QModelIndex mappedParent = parent.isValid()
                              ? mappedIndexFromSourceIndex(parent)
                              : index(qobject_cast<QAbstractItemModel*>(sender()));
    beginInsertRows(mappedParent, first, last);
}

void ProxyModelTree::sourceModelRowsInserted(const QModelIndex &parent, int first, int last)
{
    Q_UNUSED(parent);
    Q_UNUSED(first);
    Q_UNUSED(last);
    ModelPtr sourceModel = modelPtr(qobject_cast<QAbstractItemModel*>(sender()));

    if(sourceModel.isNull())
        return;

    if(_visibleModels.contains(sourceModel))
        endInsertRows();

    if( !sourceModel.isNull()
        && !_visibleModels.contains(sourceModel)
        && sourceModel->rowCount(QModelIndex())>0 )
    {
        showModel(sourceModel);
    }
}

void ProxyModelTree::sourceModelRowsAboutToBeRemoved(const QModelIndex &parent, int first, int last)
{
    QModelIndex mappedParent = parent.isValid()
                              ? mappedIndexFromSourceIndex(parent)
                              : index(qobject_cast<QAbstractItemModel*>(sender()));

    beginRemoveRows(mappedParent, first, last);
}

void ProxyModelTree::sourceModelRowsRemoved(const QModelIndex &parent, int first, int last)
{
    QModelIndex mappedParent = parent.isValid()
                              ? mappedIndexFromSourceIndex(parent)
                              : index(qobject_cast<QAbstractItemModel*>(sender()));

    for(int row=first;row <=last; row++)
        for( int col=0; col<columnCount(QModelIndex()); col++ )
        {
            QModelIndex mappedIndex = index(row,col, mappedParent);
            QModelIndex sourceIndex = sourceIndexFromMappedIndex(mappedIndex);
            _mappedToSource.remove(mappedIndex);
            _sourceToMapped.remove(sourceIndex);
        }
    endRemoveRows();
    hideModelIfEmpty(qobject_cast<QAbstractItemModel*>(sender()));
}

void ProxyModelTree::hideModelIfEmpty(QAbstractItemModel* model)
{
    ModelPtr sourceModel = modelPtr(model);
    if(!sourceModel.isNull()
        && _visibleModels.contains(sourceModel)
        && sourceModel->rowCount(QModelIndex())==0 )
    {
        hideModel(sourceModel);
    }
}

void ProxyModelTree::showModelIfNotEmpty(QAbstractItemModel* model)
{
    ModelPtr sourceModel = modelPtr(model);
    if( !sourceModel.isNull()
        && !_visibleModels.contains(sourceModel)
        && sourceModel->rowCount(QModelIndex())>0 )
    {
        showModel(sourceModel);
    }
}


ModelPtr ProxyModelTree::modelAt(const QModelIndex& mappedIndex)  const
{
    if(isRootIndex(mappedIndex))
        return _visibleModels.at(mappedIndex.row());

    QModelIndex sourceIndex = sourceIndexFromMappedIndex(mappedIndex);
    if(!sourceIndex.isValid())
        return ModelPtr(0);

    QModelIndex mappedModelIndex = index(sourceIndex.model());
    if( !mappedModelIndex.isValid() )
        return ModelPtr(0);

    ModelPtr model = _visibleModels[mappedModelIndex.row()];
    return model;
}

QStringList ProxyModelTree::mimeTypes() const
{
    QStringList types;
    foreach(const ModelPtr& model, _visibleModels)
    {
        types.append(model->mimeTypes());
    }
    return types;
}

Qt::DropActions ProxyModelTree::supportedDropActions() const
{
    return Qt::LinkAction | Qt::CopyAction | Qt::MoveAction;
}

QMimeData* ProxyModelTree::mimeData(const QModelIndexList &indexes) const
{
    if(indexes.count() == 0)
        return new QMimeData();

    QModelIndex firstmappedIndex = indexes[0];
    QModelIndex firstSourceIndex = sourceIndexFromMappedIndex(firstmappedIndex);
    if(!firstSourceIndex.isValid())
        return new QMimeData();

    QModelIndex modelIndex = index(firstSourceIndex.model());
    if( !modelIndex.isValid() )
        return new QMimeData();

    QModelIndexList sourceModelIndexList;
    foreach(QModelIndex i, indexes)
    {
        QModelIndex sourceIndex = sourceIndexFromMappedIndex(i);
        if(!sourceIndex.isValid() || sourceIndex.model()!=firstSourceIndex.model())
            return new QMimeData();
        sourceModelIndexList.append(sourceIndex);
    }

    return firstSourceIndex.model()->mimeData(sourceModelIndexList);
}

bool ProxyModelTree::dropMimeData(const QMimeData *data, Qt::DropAction action,
                          int row, int column, const QModelIndex &parent)
{
    if(action == Qt::IgnoreAction)
        return false;

    QModelIndex mappedIndex = index(row, column, parent);
    QModelIndex sourceIndex = sourceIndexFromMappedIndex(mappedIndex);
    if(!sourceIndex.isValid())
        return false;

    ModelPtr model = modelAt(mappedIndex);
    if(model.isNull())
        return false;

    return model->dropMimeData(data,
                                      action,
                                      sourceIndex.row(),
                                      sourceIndex.column(),
                                      sourceIndex.parent());
}

void ProxyModelTree::sourceModelLayoutAboutToBeChanged()
{
    emit layoutAboutToBeChanged();
}

void ProxyModelTree::sourceModelLayoutChanged()
{
    emit layoutAboutToBeChanged();
    QAbstractItemModel* model = qobject_cast<QAbstractItemModel*>(sender());
    hideModelIfEmpty(model);
    showModelIfNotEmpty(model);
    emit layoutChanged();
}

}
