#ifndef SCRIPTADAPTER_H
#define SCRIPTADAPTER_H

#include "appcore.h"
#include <QObject>
#include <QApplication>
#include <QtCore>
#include <QtDBus/QDBusAbstractAdaptor>
#include <QtDBus/QDBusConnection>
#include <QtScript/QScriptEngine>
#include <QtDBus/QDBusError>

class ScriptParamList : public QObject
{
    Q_OBJECT
public:
    explicit ScriptParamList(QObject* parent = 0)
        :QObject(parent)
    {

    }

    void addParamList(const QString& list);
    void clear();

    Q_INVOKABLE const QString operator[](int i) const;
    Q_INVOKABLE QString operator[](int i);
    Q_INVOKABLE QString at(int i);


    typedef QVector<QString> paramsContainer;
private:
    paramsContainer _params;
};


class ScriptAdapter : public QDBusAbstractAdaptor
{
    Q_OBJECT

public:
    explicit ScriptAdapter(QObject *parent = 0);

    //Q_CLASSINFO("Script", "DBus.ARM.QScript")

public slots:
    Q_SCRIPTABLE QString exec(const QString& scriptName, const QString& scriptParams);

private:
    QSharedPointer < QScriptEngine   > _engine;
    QSharedPointer < ScriptParamList > _params;

};
#endif // SCRIPTADAPTER_H
