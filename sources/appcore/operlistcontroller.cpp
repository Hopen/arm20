#include "operlistcontroller.h"
#include "requestreplymessage.h"

namespace ccappcore
{

OperListController::OperListController(QObject* parent)
    : ContactsController(parent)
{

}

ContactId OperListController::createOperId(int id) const
{
    return ContactId(const_cast<OperListController*>(this), id, CT_Operator);
}

ContactId OperListController::createGroupId(int id) const
{
    return ContactId(const_cast<OperListController*>(this), id, CT_Group);
}

QUrl OperListController::buildContactUrl(const Contact& c) const
{
    if(c.isContainer())
        return QString("ccapp://operlistcontroller/group?id=%1").arg(c.contactId().toInt());

    return QString("ccapp://operlistcontroller/operator?id=%1").arg(c.contactId().toInt());
}

void OperListController::setSessionOwner(const ccappcore::Contact& c)
{
    if(!_operList.contains(c))
        _operList.append(c);
    _sessionOwner = c;
}

QFuture<Contact> OperListController::loadContactsAsync()
{
    _loadContactsResult = QFutureInterface<Contact>( (QFutureInterfaceBase::State)
            (QFutureInterfaceBase::Started | QFutureInterfaceBase::Running)
            );

    _operList.clear();
    _groupList.clear();

    Message request, replyPrototype;
    request = Message("O2C_OPER_LIST_REQ");
    replyPrototype = Message("C2O_OPER_LIST");
    RequestReplyMessage::send(
        request, replyPrototype, this, SLOT(on_c2oOperList(ccappcore::Message)));

    request = Message("O2C_GROUP_LIST_REQ");
    replyPrototype = Message("C2O_GROUP_LIST"); //TODO: zero istead of O char - fixed 19.04.2012
    RequestReplyMessage::send(
        request, replyPrototype, this, SLOT(on_c2oGroupList(ccappcore::Message)));

    request = Message("O2C_GET_OPER_IN_GROUPS");
    replyPrototype = Message("C2O_GET_OPER_IN_GROUPS");
    RequestReplyMessage::send(
        request, replyPrototype, this, SLOT(on_c2oOperInGroup(ccappcore::Message)));

//    request = Message("O2C_GET_REDIR_PROFILES");
//    replyPrototype = Message("C2O_GET_REDIR_PROFILES");
//    RequestReplyMessage::send(
//        request, replyPrototype, this, SLOT(on_c2oRedirProfiles(ccappcore::Message)));


    return _loadContactsResult.future();
}

void OperListController::startEventsListening()
{
#if (_CDSVER >= 0x0400)
    _router->subscribeEvent("C2O_UPDATE_OPER_STATUS");
#endif
    _opStatusListener.setSubscribeRequired(true);
    _opStatusListener.startListening(
            &_router, Message("C2O_UPDATE_OPER_STATUS"),
            this, SLOT(on_c2oUpdateOperStatus(ccappcore::Message)));
}

void OperListController::stopEventsListening()
{
#if (_CDSVER >= 0x0400)
    _router->unsubscribeEvent("C2O_UPDATE_OPER_STATUS");
#endif
    _opStatusListener.stopListening();
}

Group OperListController::findGroupByCode(const QString& groupCode)
{
    foreach(const Contact& group, groupList())
    {
        if(group.data(GroupCode).toString() == groupCode)
            return group;
    }

    return Contact::invalid();
}

void OperListController::on_c2oOperList(const Message& listMessage)
{    
    if(sessionOwner().isValid())
        _operList.push_back(sessionOwner());

    if( 0==(quint32)listMessage["sql_result"] )
    {
        qWarning()<<"C2O_OPER_LIST should include 'state_time' parameter";
        QByteArray listData = listMessage["data"];
        QList<Message> list = _messageSerializer->loadList(listData);
        foreach(const Message& m, list)
        {
            qDebug()<<m;
            ContactId operId = createOperId(m["oper_id"]);
            Contact op = findById(operId);

            bool isNew = !op.isValid();
            if(isNew)
            {
                op = Contact(this, operId);
                _operList.push_back(op);
            }

            Contact::ContactStatus newStatus = (Contact::ContactStatus)(int)m["status"];

            op.setName(m["name"]);
            qDebug() << tr("Oper \"%1\" new status: %2").arg(op.name()).arg(newStatus);
            op.addData((Contact::ContactInfo)Contact::Phone, (const QString&)m["phone"]);
            op.setStatus( newStatus );
            op.setStatusReason( m["reason"].value().toString() );

            op.setStatusTime(QDateTime::currentDateTime());
        }
    }
}

void OperListController::on_c2oGroupList(const Message& listMessage)
{
    if( 0==(quint32)listMessage["sql_result"] )
    {
        QByteArray listData = listMessage["data"];
        QList<Message> list = _messageSerializer->loadList(listData);
        foreach(const Message& m, list)
        {
            qDebug()<<m;
            ContactId groupId = createGroupId(m["group_id"]);
            Contact g = findById(groupId);
            int edge = m["edge"];
            if(!g.isValid() && edge >= 0)
            {
                g = Contact(groupId);
                _groupList.push_back(g);
                g.setData(GroupCode, m["script_code"].value().toString());
                g.setName(m["name"]);
                g.setIsContainer(true);
            }
        }
    }
}

void OperListController::on_c2oOperInGroup(const Message& m)
{
    if( 0==(quint32)m["sql_result"] )
    {
        QByteArray listData = m["data"];
        QList<Message> list = _messageSerializer->loadList(listData);
        foreach(const Message& row, list)
        {
            qDebug()<<row;
            ContactId groupId = createGroupId( row["group_id"] );
            ContactId operId = createOperId( row["oper_id"] );
            bool inGroup = row["status"];
            if(inGroup)
            {
                Contact g = findById(groupId);
                Contact op = findById(operId);
                if(g.isValid() && op.isValid())
                    g.addChild(op);
                else
                    qWarning()<<"OperListController::on_c2oOperInGroup() - "<<
                            "invalid data: operId"<<operId<<", groupId="<<groupId;
            }
        }
        updateOpStateCount();
        _loadContactsResult.reportResults(_groupList.toVector(), 0, _groupList.count() );
        _loadContactsResult.reportFinished();
        emit dataChanged();
    }
}

void OperListController::updateOpStateCount()
{
    foreach(Contact group, _groupList)
    {
        QMap<Contact::ContactStatus,int> stateCount;
        stateCount[Contact::Free]     = 0;
        stateCount[Contact::Busy]     = 0;
        stateCount[Contact::Paused]   = 0;
        stateCount[Contact::Offline]  = 0;
        foreach(Contact op, group.children())
           stateCount[op.status()]++;
        group.setData(GroupFreeOpCount,stateCount[Contact::Free]);
        group.setData(GroupBusyOpCount,stateCount[Contact::Busy]);
        group.setData(GroupPausedOpCount,stateCount[Contact::Paused]);
        group.setData(GroupOfflineOpCount,stateCount[Contact::Offline]);
        group.setData(GroupOnlineOpCount,group.children().count() - stateCount[Contact::Offline]);

        if(stateCount[Contact::Free]>0)
        {
            group.setStatus(Contact::Free);
        }
        else if(stateCount[Contact::Free]==0 &&
                stateCount[Contact::Busy]>0 )
        {
            group.setStatus(Contact::Busy);
        }
        else if(stateCount[Contact::Free]==0 &&
                stateCount[Contact::Busy]==0 &&
                stateCount[Contact::Paused]>0 )
        {
            group.setStatus(Contact::Paused);
        }
        else
            group.setStatus(Contact::Offline);
    }
}

void OperListController::updateOpStateCount(Contact op,
                                            Contact::ContactStatus oldStatus,
                                            bool isNew)
{
    if(oldStatus == op.status() && !isNew )
        return;

    foreach(Contact group, _groupList)
    {
        if( !group.isParentOf(op) )
            continue;

        QMap<Contact::ContactStatus, int> stateCount;
        stateCount[Contact::Free]     = group.data(GroupFreeOpCount).toInt();
        stateCount[Contact::Busy]     = group.data(GroupBusyOpCount).toInt();
        stateCount[Contact::Paused]   = group.data(GroupPausedOpCount).toInt();
        stateCount[Contact::Offline]  = group.data(GroupOfflineOpCount).toInt();
        if( !isNew && stateCount[oldStatus]>0)
            stateCount[oldStatus]--;
        stateCount[op.status()]++;
        group.setData(GroupFreeOpCount,stateCount[Contact::Free]);
        group.setData(GroupBusyOpCount,stateCount[Contact::Busy]);
        group.setData(GroupPausedOpCount,stateCount[Contact::Paused]);
        group.setData(GroupOfflineOpCount,stateCount[Contact::Offline]);
        group.setData(GroupOnlineOpCount,group.children().count() - stateCount[Contact::Offline]);

        if(stateCount[Contact::Free]>0)
        {
            group.setStatus(Contact::Free);
        }
        else if(stateCount[Contact::Free]==0 &&
                stateCount[Contact::Busy]>0 )
        {
            group.setStatus(Contact::Busy);
        }
        else if(stateCount[Contact::Free]==0 &&
                stateCount[Contact::Busy]==0 &&
                stateCount[Contact::Paused]>0 )
        {
            group.setStatus(Contact::Paused);
        }
        else
            group.setStatus(Contact::Offline);
    }
}

void OperListController::on_c2oUpdateOperStatus(const Message& m)
{
    ContactId operId( this, (int)m["oper_id"] );
    Contact::ContactStatus operStatus = (Contact::ContactStatus)(int) m["status"];
    Contact op = findById(operId);
    Contact::ContactStatus oldStatus = op.status();
    if(!op.isValid())
    {
        qWarning()<<"Operator not found. ID: ="<<operId;
        return;
    }

    //qDebug() << tr("Oper \"%1\" new status update: %2").arg(op.name()).arg(operStatus);

    if ((operStatus != Contact::Offline && oldStatus == Contact::Offline) ||  //get in
        (operStatus == Contact::Offline && oldStatus != Contact::Offline)   ) //get out
        emit contactGetInOut(op);

    op.setStatus(operStatus);
    op.setStatusReason( m["reason"].value().toString() );
    op.setStatusTime( QDateTime::currentDateTime() );

    updateOpStateCount(op, oldStatus, false);

    emit contactStatusChanged(op);
}

void OperListController::reqChangeStatus(const Contact& contact, Contact::ContactStatus newStatus, const QString &reason)
{
    reqChangeStatus(contact.contactId(),newStatus,reason);
}

void OperListController::reqChangeStatus(const ccappcore::ContactId& cid, ccappcore::Contact::ContactStatus newStatus, const QString &reason)
{
    Message m("O2C_SET_OPERATOR_STATUS");
    m["sender_oper_id"] = cid.toInt();
    m["status"] = (int) newStatus;
    m["reason"] = reason;
    _router->send(m);
}

const QList<Contact>& OperListController::contacts() const
{
    return _groupList;
}

Contact OperListController::findById(const ContactId& id) const
{
    if(id == _sessionOwner.contactId())
        return _sessionOwner;

    const QList<Contact>& source = id.type() == CT_Group ? _groupList : _operList;
    foreach(const Contact& c, source)
        if(c.contactId() == id)
            return c;
    return Contact::invalid();
}

QColor OperListController::statusColor(Contact::ContactStatus status) const
{
    switch(status)
    {
        case Contact::Free:
            return QColor("green");
        case Contact::Busy:
            return QColor("red");
        case Contact::Paused:
            return QColor("blue");
        case Contact::Offline:
            return QColor("gray");
        default:
            return QColor();
    }

}

const QIcon& OperListController::statusIcon(Contact::ContactStatus status, bool roster) const
{
    static QIcon freeIcon(":/resources/icons/new/status online 16 01.png" /*":/resources/icons/oper_free_16x16.png"*/);
    static QIcon rosterIcon(":/resources/icons/new/message green.png" );
    static QIcon busyIcon(":/resources/icons/new/status busy 16.png" /*":/resources/icons/oper_busy_16x16.png"*/);
    static QIcon pausedIcon(":/resources/icons/new/status pause on 16 02.png" /*":/resources/icons/oper_paused_16x16.png"*/);
    //static QIcon byNumberIcon(":/resources/icons/new/status phone 32.png");
    //static QIcon byVoicemailIcon(":/resources/icons/new/status message 32.png");
    static QIcon offlineIcon(":/resources/icons/new/help off.png");

    switch(status)
    {
    case Contact::Free:
    {
        if (!roster)
            return freeIcon;
        else
            return rosterIcon;
    }
        break;
    case Contact::Busy:
        return busyIcon;
        break;
    case Contact::Paused:
        return pausedIcon;
        break;
    case Contact::Offline:
        return offlineIcon;
        break;
//    case Contact::ByNumber:
//        return byNumberIcon;
//        break;
//    case Contact::ByVoicemail:
//        return byVoicemailIcon;
//        break;

    case Contact::UnknownStatus:
    default:
        break;
    }

    static QIcon other;
    return other;
}

QString OperListController::infoDisplayName(int infoId) const
{
    switch(infoId)
    {
    case GroupCode:
        return tr("Имя группы");
    case GroupFreeOpCount:
        return tr("Свободно");
    case GroupOfflineOpCount:
        return tr("Оффлайн");
    case GroupPausedOpCount:
        return tr("На паузе");
    case GroupBusyOpCount:
        return tr("Занято");
    default:
        return ContactsController::infoDisplayName(infoId);
    }
}

QVariant OperListController::modelData(const QModelIndex& index, const Contact& c, int role) const
{
    if(role == Qt::DecorationRole && index.column()==0)
    {
        static QIcon groupIcon(":/resources/icons/group_16x16.png");
        if( c.contactId().type() == CT_Group )
            return groupIcon;

        return statusIcon(c.status());
    }
    if(role == Qt::TextColorRole)
        return statusColor(c.status());

    return ContactsController::modelData(index, c,role);
}

QString OperListController::statusAsString(Contact::ContactStatus status) const
{
    switch(status)
    {
    case Contact::Free:
        return tr("Онлайн");
    case Contact::Busy:
        return tr("Занято");
    case Contact::Paused:
        return tr("На паузе");
    case Contact::Offline:
        return tr("Оффлайн");
    default:
        return tr("Неизвестно");
    }
}

QList<Contact> OperListController::findOpers(const Predicate<Contact>& predicate) const
{
    QList<Contact> r;
    foreach(Contact c, _operList)
        if(c.isValid() && predicate(c))
            r.append(c);
    return r;
}

QList<Contact> OperListController::findGroups(const Predicate<Contact>& predicate) const
{
    QList<Contact> r;
    foreach(Contact c, _groupList)
        if(c.isValid() && predicate(c))
            r.append(c);
    return r;
}


}
