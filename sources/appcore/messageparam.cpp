#include "messageparam.h"

namespace ccappcore
{
        MessageParam MessageParam::invalid("Invalid parameter");

        MessageParam::MessageParam()
        {
            m_flag = 0;
        }

        MessageParam::MessageParam(const char* name)
            : m_name(name), m_flag(0)
	{
	}

        MessageParam::~MessageParam()
	{
	}

        bool MessageParam::isValid() const
        {
            return !m_name.isEmpty() && m_value.isValid();
        }

        bool MessageParam::operator == (const MessageParam& param) const
	{
		if( 0 != name().compare(param.name()) )
			return false;

		return value() == param.value();
	}

        bool MessageParam::operator != (const MessageParam& param) const
	{
		return ! operator == (param);
	}

        void MessageParam::clear ()
	{
		m_value.clear();
	}

        void MessageParam::operator = (quint32 value)
	{
		m_value.setValue(value);
	}

        void MessageParam::operator = (qint32 value)
	{
		m_value.setValue(value);
	}

        void MessageParam::operator = (quint64 value)
	{
		m_value.setValue(value);
	}

        void MessageParam::operator = (qint64 value)
	{
		m_value.setValue(value);
	}

        void MessageParam::operator = (float value)
	{
		m_value.setValue((double)value);
	}

        void MessageParam::operator = (double value)
	{
		m_value.setValue(value);
	}

        void MessageParam::operator = (const char* value)
	{
		m_value.setValue(QString(value));
	}

        void MessageParam::operator = (const QString& value)
	{
		m_value.setValue(value);
	}

        void MessageParam::operator = (const QDateTime& value)
        {
            if(!m_name.endsWith("_time_t"))
                m_name.append("_time_t");
            m_value.setValue(value.toTime_t());
        }

        void MessageParam::operator = (QByteArray value)
	{
		m_value.setValue(value);
	}

        MessageParam::operator quint32() const throw()
	{
		return m_value.toUInt();
	}

        MessageParam::operator qint32() const throw()
	{
		return m_value.toInt();
	}

        MessageParam::operator bool() const throw()
        {
                return m_value.toInt()!=0;
        }

        MessageParam::operator quint64() const throw()
	{
		return m_value.toULongLong();
	}

        MessageParam::operator qint64() const throw()
	{
		return m_value.toLongLong();
	}

        MessageParam::operator float() const throw()
	{
		return (float)m_value.toDouble();
	}

        MessageParam::operator double() const throw()
	{
		return m_value.toDouble();
	}

        MessageParam::operator QString() const  throw()
	{
		return m_value.toString();
	}

        MessageParam::operator const char*() const throw()
        {
            return m_value.toString().toLocal8Bit();
        }

        MessageParam::operator QDateTime() const  throw()
        {

            if (m_value.type() == QVariant::Double)
            {
                // QDateTime starts form 1 Jan 1970
                //   incoming time starts from 1 Jan 1900
                QDateTime dt(QDate(1900, 1, 1),  QTime(0,0,0));
                QString stime = dt.toString();
                qreal days, time; // 41122
                time = modf(m_value.toDouble(), &days);

                // There is an error in QT: QDate() desides that 1900 is not leap-year,
                //  but it's not true actionaly
                //  that's why minus 1 day in colculation
                // Secondly, database returns +1 day by unknown reason
                //  that's why we have another minus ;)
                dt = dt.addDays(days - 1 - 1);
                dt = dt.addSecs(time*24*60*60);
                dt = dt.toLocalTime();

                stime = dt.toString();

                return dt;
            }
            if( m_value.type() != QVariant::LongLong &&
                m_value.type() != QVariant::ULongLong &&
                m_value.type() != QVariant::Int &&
                m_value.type() != QVariant::UInt
                )
                return QDateTime();

            quint64 time_t_value = m_value.toULongLong();
            QDateTime dt;
            dt.setTime_t(time_t_value);
            return dt;
        }

        MessageParam::operator QByteArray() const throw()
	{
		return m_value.toByteArray();
	}

        quint16 MessageParam::size()const
        {
            quint16 size = 0;
            size += sizeof (qint8);// size of CDS_DATA_TYPE
            switch (m_value.type())
            {
            case QVariant::UInt:
            case QVariant::Int:
                size += sizeof (qint32);
                break;
            case QVariant::ULongLong:
            case QVariant::LongLong:
                size += sizeof (qint64);
                break;
            case QVariant::Double:
                size += sizeof (qreal);
                break;
            case QVariant::String:
            {
                size += sizeof (quint32); // size of size
                size += m_value.toString().length()/**2+2*/;
                break;
            }
            case QVariant::ByteArray:
            {
                size += sizeof (quint32); // size of size
                size += m_value.toByteArray().length();
                break;
            }
            default:
                    {
                        qWarning()<<"MessageParam::size"
                                <<" - unsupported variant type:"<< m_value;
                        //stream << char(MessageParam::cds_unknown);
                        break;
                    }
            }
            size += sizeof (quint32); //size of flags -> see cds_proto.h
            return size;
        }
}
