#ifndef SHORTCUTMANAGER_H
#define SHORTCUTMANAGER_H

#include "appcore.h"
#include "serviceprovider.h"
#include "appsettings.h"
#include <QKeySequence>

namespace ccappcore
{

class APPCORE_EXPORT ShortcutManager
    : public QObject
    , public IAppSettingsHandler
{
    Q_OBJECT;
    Q_INTERFACES(ccappcore::IAppSettingsHandler)
public:
        ShortcutManager(QObject* parent = 0);

        void registerShortcut(const QString& name, const QString& descr);
        void connect(const QString& path, QObject *parent, const char* slot);

        QStringList allNames();
        QString description(const QString& name) const;
        QKeySequence key(const QString& name);
        void setKey(const QString& name, const QKeySequence& key);

public: //IAppSettingsHandler
    virtual void applySettings(const AppSettings &);
    virtual void flushSettings(AppSettings &);

private:
        QMap<QString,QString> _mapNameDescr;
        QMap<QString,QKeySequence> _mapNameKey;
};

}

#endif
