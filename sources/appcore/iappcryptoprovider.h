#ifndef IAPPCRYPTOPROVIDER_H
#define IAPPCRYPTOPROVIDER_H

#include "appcore.h"
namespace ccappcore {

class APPCORE_EXPORT IAppCryptoProvider
{
    public:
    virtual ~IAppCryptoProvider() {}
    virtual bool canEncryptRSA() const = 0;
    virtual QByteArray encryptRSA(const QByteArray&) const = 0;

    virtual bool canDecryptRSA() const = 0;
    virtual QByteArray decryptRSA(const QByteArray&) const = 0;
};

}

Q_DECLARE_INTERFACE(ccappcore::IAppCryptoProvider, "ru.forte-it.ccappcore.iappcryptoprovider/1.0")

#endif // IAPPCRYPTOPROVIDER_H
