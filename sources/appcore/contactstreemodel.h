#ifndef CONTACTSTREEMODEL_H
#define CONTACTSTREEMODEL_H

#include "appcore.h"
#include "contact.h"
#include "filteredmodel.h"
#include "contactscontroller.h"

namespace ccappcore
{

class APPCORE_EXPORT ContactsTreeModel
    : public QAbstractItemModel
    , public FilteredModel<Contact>
{
Q_OBJECT;
public:
    explicit ContactsTreeModel(QObject *parent = 0);

    QPointer<ContactsController> controller() const { return _controller; }
    void setController(QPointer<ContactsController> controllr);

    bool isListMode() const;
    void setListMode(bool val);

    const QList<int>& columns() const { return _columns; }
    void setColumns(const QList<int>& data);

    virtual QModelIndex index ( int row, int column = 0, const QModelIndex & parent = QModelIndex() ) const;
    virtual QModelIndex parent ( const QModelIndex & index ) const;
    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
    //virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation,
                                int role = Qt::DisplayRole) const;

signals:

public slots:
    void reset();
    void searchAsync(const QString& searchText);

private slots:
    void searchCompleted();

protected: //FilteredModel
    virtual void applyFilter(const PredicateGroup<Contact>& filter);

private:

    QList<Contact> _contacts; //currently displayed contacts

    QList<int> _columns;
    QPointer<ContactsController> _controller;

    QString _searchText; //user search string
    bool _forceListMode; //True to force display as list of unique contacts, False - normal tree

    Contact contactByIndex(const QModelIndex& index) const;
    mutable quint32 _internalId;
    mutable QHash<int,QModelIndex> _indexMap;

    QFutureWatcher<Contact> _searchResultWatcher;

private:
    QList<Contact> filtered(const QList<Contact>& source) const;
};

}

#endif // CONTACTSTREEMODEL_H
