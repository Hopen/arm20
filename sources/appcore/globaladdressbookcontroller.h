#ifndef GLOBALADDRESSBOOKCONTROLLER_H
#define GLOBALADDRESSBOOKCONTROLLER_H

#include <QObject>
#include "appcore.h"
#include "imessageserializer.h"
#include "lateboundobject.h"
#include "irouterclient.h"
#include "predicate.h"
#include "contactscontroller.h"

namespace ccappcore
{

typedef ContactId GlobalAddressId;
typedef Contact   GlobalAddress;

enum E_TYPE_ID
{
    ETI_NAME         = -1,
    ETI_EMAIL        = 10,
    ETI_PHONE        = 12,
    EIT_ADDRESS      = 30,
    EIT_ORGANIZATION = 31
};


enum ContactType3
{
    CT_GlobalAddress = 4,
    CT_GlobalBook
};

class CallsController;

//class APPCORE_EXPORT GlobalAddress
//{
//public:
//    GlobalAddress():
//        _id(-1),
//        _parent_id(0),
//        _type(0),
//        _book_id(0)
//    {

//    }
//    GlobalAddress(qint32 id, QString name, QString full_name, qint32 parent_id, qint32 type, qint32 book_id, QString book_name):
//        _id(id), _name(name), _full_name(full_name), _parent_id(parent_id), _type(type), _book_id(book_id), _book_name(book_name)
//    {

//    }
//    bool isValid()const{return !(_id==-1);}

//    qint32 id        () const { return _id        ;}
//    qint32 parent_id () const { return _parent_id ;}
//    qint32 type      () const { return _type      ;}
//    qint32 book_id   () const { return _book_id   ;}

//    QString name      () const { return _name      ;}
//    QString full_name () const { return _full_name ;}
//    QString book_name () const { return _book_name ;}

//private:
//    qint32 _id;
//    QString _name;
//    QString _full_name;
//    qint32 _parent_id;
//    qint32 _type;
//    qint32 _book_id;
//    QString _book_name;

//};


class APPCORE_EXPORT GlobalAddressBookController : public ContactsController //public QObject
{
    Q_OBJECT
public:
    explicit GlobalAddressBookController(QObject *parent = 0);


    // interface implementation
    virtual QFuture<Contact> loadContactsAsync();
    virtual const QList<Contact>& contacts() const;
    virtual QUrl buildContactUrl(const Contact&) const;

    //QFuture < GlobalAddress > loadAddressBookAsync();

    struct AddressExtraInfo
    {
        int type_id;
        QString info;

        AddressExtraInfo():type_id(0){}
        AddressExtraInfo(const int& t, const QString& i):
            type_id(t), info(i)
        {

        }
    };
    typedef QList     < GlobalAddress         > GlobalAddressContainer;
    typedef QList     < AddressExtraInfo      > AddressExtraInfoContainer;
    typedef QMultiMap < int, AddressExtraInfo > AddressExtraInfoMap;


    GlobalAddressId createGlobalAddressId(int id) const;
    GlobalAddressId createBookAddressId(int id) const;

    GlobalAddressContainer * globalAddressList() const
    {
        GlobalAddressContainer * p = (GlobalAddressContainer *)(&_globalAddressList);
        //return &_profilesList;
        return p;
    }

    AddressExtraInfoContainer getExtraInfoList(const int& person_id)const
    {
        return _personExtraInfo.values(person_id);
    }

    int getContactTypeByPersonType(const int& type_id)const;

    qint32 globalAddressListCount()const {return _globalAddressList.count();}

    QList<GlobalAddress> findGlobalAddresses(const Predicate<GlobalAddress>& predicate) const;

    void getPersonInfo(const ccappcore::ContactId& id);
    void search(const QString& text, bool bResetModel);

    void resetList();
private:
    //void changeListStatus(const QList<Contact>& list, QList<Contact>& out, int status);
    bool changeListStatus(const QList<int> &idList, const QList<Contact> &list, QList<Contact> &out);
    //void disableAddressList();
    //void enableAddressList();

    //bool enableBranch(const QList<int>& idList, const QList<Contact>& list, QList<Contact>& out);
private slots:
    void on_c2oPersonList    (const ccappcore::Message& m);
    void on_c2oPersonInfo    (const ccappcore::Message& m);
    void on_c2oSearchContacts(const ccappcore::Message& m);

signals:
    void onLoadFinished();
    void onLoadInfoCompleted(const ccappcore::Contact&);
    void onSearchCompleted(const ccappcore::Contact&, const QString& value);
    void resetModel();

private:

    LateBoundObject<IMessageSerializer> _messageSerializer;
    LateBoundObject<CallsController> _callsController;
    LateBoundObject<IRouterClient> _router;

    QFutureInterface< GlobalAddress > _loadGlobalAddressBookResult;

    GlobalAddressContainer _globalAddressList;
    QVector<GlobalAddress> _saveGlobalAddressList;
    AddressExtraInfoMap  _personExtraInfo;
    //QMap <int,int> _type_id_converter;
    bool                 _resetModelRequest;
};

} //namespace ccappcore

//Q_DECLARE_METATYPE(ccappcore::GlobalAddress);

#endif // GLOBALADDRESSBOOKCONTROLLER_H
