#ifndef APPLOADER_H
#define APPLOADER_H

#include "appcore.h"
#include "iappmodule.h"

namespace ccappcore
{
    class APPCORE_EXPORT AppLoader
    {
        QList<QObject*> loadedPlugins;
        QStringList loadedModules;
        
    public:
        AppLoader();

        bool load(QString appRootPath = ".\\") throw();

        const QList<QObject*>& getPluginsList() { return loadedPlugins; }
        const QStringList& getLoadedModulesList() { return loadedModules; }

    private:
        bool loadPlugins(QString appRootPath) throw();
    };
}

#endif // APPLOADER_H
