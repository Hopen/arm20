#ifndef MessagePARAM_H_
#define MessagePARAM_H_

#include "appcore.h"

namespace ccappcore
{
        class APPCORE_EXPORT MessageParam {
#if (_CDSVER < 0x0400)
        public:
        enum CDS_DATA_TYPE {
              cds_unknown,
              cds_int,
              cds_double,
              cds_datetime = cds_double,
              cds_binary,
              cds_string,
              cds_memo,
              cds_int64
            };
        enum flags {
            F_DATE_TIME = 0x01,
            F_HIDDEN = 0x02,
            F_CDS_TABLE = 0x04,
            F_MEMO = 0x08,
        };

#endif

        public:
                static MessageParam invalid;

                MessageParam();
                explicit MessageParam(const char* name);
                virtual ~MessageParam();

                bool isValid() const;

		inline const QString& name() const { return m_name; }
                inline void setName(const QString& name) { m_name = name; }

                inline const QVariant& value() const { return m_value; }
                inline void setValue(const QVariant& value) { m_value = value; }
#if (_CDSVER < 0x0400)
                quint32 flag()const {return m_flag;}
                void setFlag(const quint32& flag){ m_flag = flag; }
#endif
                bool operator == (const MessageParam& param) const;
                bool operator != (const MessageParam& param) const;

		void clear ();

		void operator = (quint32 value);
		void operator = (qint32 value);
		void operator = (quint64 value);
		void operator = (qint64 value);
		void operator = (float value);
		void operator = (double value);
		void operator = (const char* value);
		void operator = (const QString& value);
                void operator = (const QDateTime& value);
                void operator = (QByteArray value);

                quint16 size()const;

		operator quint32() const throw();
                operator qint32() const throw();
		operator quint64() const  throw();
		operator qint64() const throw();
                operator bool() const throw();
                operator float() const throw();
		operator double() const throw();
                operator const char*() const throw();
                operator QString() const throw();
                operator QDateTime() const throw();
                operator QByteArray() const throw();
        private:
                QVariant m_value;
                QString m_name;
#if (_CDSVER < 0x0400)
                quint32 m_flag;
#endif
        };

        inline QDebug operator<<(QDebug out, const MessageParam& m)
        {
            if(m.value().type() == QVariant::ByteArray)
            {
                out.nospace()<<m.name();
                out<<" ByteArray["<<m.value().toByteArray().length()<<"]";
                return out;
            }
            if( QString(m.value().typeName()).contains("int") )
                return out.nospace()<<m.name()<<"="<<m.value().toInt();

            return out.nospace()<<m.name()<<"="<<m.value().toString();
        }

}

#endif /* MessagePARAM_H_ */
