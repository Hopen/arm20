#include "scriptadapter.h"
#include "lateboundobject.h"
#include "callscontroller.h"

using namespace ccappcore;

void ScriptParamList::addParamList(const QString &list)
{
    QStringList l = list.split(" ");
    for (QStringList::const_iterator cit=l.begin();cit!=l.end();++cit)
    {
        QString param = *cit;
        if (!param.trimmed().isEmpty())
            _params.push_back(param);
    }
}

const QString ScriptParamList::operator [](int i)const
{
    return _params[i];
}

QString ScriptParamList::operator [](int i)
{
    return _params[i];
}

QString ScriptParamList::at(int i)
{
    if (i<_params.size())
        return _params[i];
    return "index out of range";
}

void ScriptParamList::clear()
{
    _params.clear();
}

ScriptAdapter::ScriptAdapter(QObject *application)
    : QDBusAbstractAdaptor(application)
{
    _engine = QSharedPointer<QScriptEngine>(new QScriptEngine());
    LateBoundObject<CallsController>    _callsController;
    QScriptValue objectWnd = _engine->newQObject(_callsController.instance());
    _engine->globalObject().setProperty("CallsController", objectWnd);

    _params = QSharedPointer<ScriptParamList>(new ScriptParamList());
    objectWnd = _engine->newQObject(_params.data());
    _engine->globalObject().setProperty("Params", objectWnd);
}

QString ScriptAdapter::exec(const QString& scriptName, const QString& scriptParams)
{
    qDebug() << tr("Run script: \"%1 %2\"").arg(scriptName).arg(scriptParams);
    QFile scriptFile(scriptName);
    QString script;
    if (!scriptFile.exists())
    {
        QString error (tr("Run script error: file \"%1\" does not exist").arg(scriptName));
        qDebug() << error;
        //return SE_FILE_NOT_EXIST;
        return error;
    }

    scriptFile.open(QIODevice::ReadOnly);
    script.append(scriptFile.readAll());
    scriptFile.close();

    _params->clear();
    _params->addParamList(scriptParams);

    QScriptValue result = _engine->evaluate(script);
    if (result.isError())
    {
        QString error(tr("Run script error: %1").arg(result.toString()));
        qDebug() << error;
        //return SE_SCRIPT_EVALUATE;
        return error;
    }

    return QString(tr("Script \"%1\" has successfully completed").arg(scriptName));
}
