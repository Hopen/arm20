#ifndef CLIENTADDRESS_H
#define CLIENTADDRESS_H

#include "appcore.h"

namespace ccappcore
{
    class APPCORE_EXPORT ClientAddress
    {
        quint32	_clientId;
        quint32	_childId;

        typedef enum
        {
            AllClients = 0,
            AllClientsAndSelf = 1,
            AnyClients = 2,
            AnyClientsAndSelf = 3
        } BroadcastClientAddress;

    public:

        bool isValid() const { return _clientId!=0; }
        quint32 clientId() const { return _clientId; }
        quint32 childId() const { return _childId; }

        static	ClientAddress	All(quint32 childID = 0) { return ClientAddress(AllClients,childID);}
        static	ClientAddress	AllAndSelf(quint32 childID = 0) { return ClientAddress(AllClientsAndSelf,childID);}
        static	ClientAddress	Any(quint32 childID = 0) { return ClientAddress(AnyClients,childID); }
        static	ClientAddress	AnyAndSelf(quint32 childID = 0) { return ClientAddress(AnyClientsAndSelf,childID); }

        ClientAddress()
                : _clientId(AllClients), _childId(0)
        {}

        ClientAddress(const ClientAddress& other)
                : _clientId(other._clientId), _childId(other._childId)
        {}

        ClientAddress(quint32 client, quint32 child)
                : _clientId(client), _childId(child)
        {}

        ClientAddress(quint32 client)
                : _clientId(client), _childId(0)
        {}

        const ClientAddress & operator = (const ClientAddress& other)
        {
                if (this != &other)
                {
                        _clientId = other._clientId;
                        _childId = other._childId;
                }
                return *this;
        }

        ClientAddress& operator = (const quint32 clientId)
        {
            this->_clientId = clientId;
            return *this;
        }

        bool operator == (const ClientAddress &other) const { return this->_clientId == other._clientId && this->_childId==other._childId; }
        bool operator != (const ClientAddress &other) const { return !operator==(other); }
    };
}

#endif // CLIENTADDRESS_H
