#ifndef CONTACTSCONTROLLER_H
#define CONTACTSCONTROLLER_H

#include "appcore.h"
#include "lateboundobject.h"
#include "contact.h"

#include <QtConcurrentRun>
#include <QReadWriteLock>
#include <QUrl>

namespace ccappcore
{
    class CallsController;
    class APPCORE_EXPORT ContactsController : public QObject
    {
        Q_OBJECT;
        public:
            ContactsController(QObject* parent = 0);
            virtual ~ContactsController();

            //Load contacts to display in contacts window
            virtual QFuture<Contact> loadContactsAsync() = 0;
            virtual QFuture<Contact> loadChildrenAsync(Contact parent);

            //Returns Contacts mathing user provided search criteria
            //(when user types something in contacts window search box)
            virtual QFuture<Contact> searchContactsAsync(const QString& filter);

            QReadWriteLock* syncRoot();
            virtual const QList<Contact>& contacts() const = 0;
            virtual Contact findById(const ContactId&) const;

            virtual QUrl buildContactUrl(const Contact&) const = 0;

            virtual QString  infoDisplayName(int infoId) const;
            virtual QVariant modelData(const QModelIndex& index, const Contact&, int role) const;

            virtual bool canChangeStatus(const Contact&) const;
            virtual void reqChangeStatus(const Contact&, Contact::ContactStatus, const QString& reason = "");

            virtual bool canMakeCall(const Contact&) const;
            virtual bool canSendMessage(const Contact&)const;
            virtual bool canTransferCall(const ccappcore::Call&, const Contact&) const;

        signals:
            void dataChanged();
            void contactStatusChanged(const ccappcore::Contact&);
            void contactInfoChanged(const ccappcore::Contact&);
            void contactGetInOut(const ccappcore::Contact&);

        protected:
            class Private;
            Private* d;
    };
}

#endif // CONTACTSCONTROLLER_H
