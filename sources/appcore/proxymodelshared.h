#ifndef PROXYMODELSHARED_H
#define PROXYMODELSHARED_H

#include "appcore.h"

namespace ccappcore
{
    typedef QSharedPointer<QAbstractItemModel> ModelPtr;
    typedef QList<ModelPtr> ModelList;
}

#endif // PROXYMODELSHARED_H
