#ifndef SPAMLISTCONTROLLER_H
#define SPAMLISTCONTROLLER_H

#include <QObject>
#include "QtGui/qcolor.h"
#include "appcore.h"
#include "appsettings.h"
#include "imessageserializer.h"
#include "lateboundobject.h"
#include "irouterclient.h"
#include "eventlistener.h"


namespace ccappcore
{

class CallsController;
class OperListController;
//class QSystemTrayIcon;

enum eMessageType
{
    MT_SYSTEM = 0,
    MT_IN,
    MT_OUT,
    MT_CHAT
};

const int TO_ALL               = -1;

class APPCORE_EXPORT SpamMessage
{
public:
    SpamMessage()
    {
        _msgId = -1;
    }
    SpamMessage(qint32 groupId,
                qint32 msgId,
                QDateTime end,
                qint32 operFromId,
                qint32 operToId,
                QDateTime begin,
                QString text,
                qint32 msgType):
        _groupId(groupId),
        _msgId(msgId),
        _end(end),
        _operFromId(operFromId),
        _operToId(operToId),
        _begin(begin),
        _text(text),
        _msgType(msgType)
    {
        _isRead = false;
    }

    virtual ~SpamMessage(){}

    void setGroupId    (const qint32   & groupId){_groupId     = groupId;}
    void setMsgId      (const qint32   & msgId  ){_msgId       = msgId  ;}
    void setEnd        (const QDateTime& end    ){_end         = end    ;}
    void setOperFromId (const qint32   & operId ){_operFromId  = operId ;}
    void setOperToId   (const qint32   & operId ){_operToId    = operId ;}
    void setBegin      (const QDateTime& begin  ){_begin       = begin  ;}
    void setText       (const QString  & text   ){_text        = text   ;}

    void setRead       (const bool& isRead      ){_isRead      = isRead ;}

    qint32    groupId    ()const{return _groupId    ;}
    qint32    msgId      ()const{return _msgId      ;}
    QDateTime end        ()const{return _end        ;}
    qint32    operFromId ()const{return _operFromId ;}
    qint32    operToId   ()const{return _operToId ;}
    QDateTime begin      ()const{return _begin      ;}
    QString   text       ()const{return _text       ;}
    qint32    msgType    ()const{return _msgType    ;}

    bool      isRead     ()const{return _isRead ;}
    bool      isValid    ()const{return _msgId!=-1;}

    static QString shortInfo(const SpamMessage& message);

    //void activateIcon();

private:
    qint32     _groupId;
    qint32     _msgId;
    QDateTime  _end;
    qint32     _operFromId;
    qint32     _operToId;
    QDateTime  _begin;
    QString    _text;
    qint32     _msgType;

private:
    bool       _isRead;
//private:
//    QSharedPointer <QSystemTrayIcon> _trayIcon;

};

typedef QList<SpamMessage> SpamContainer;
class JabberController;
class APPCORE_EXPORT SpamListController : public QObject, IAppSettingsHandler
{
    Q_OBJECT
    Q_INTERFACES(ccappcore::IAppSettingsHandler)

public:
    explicit SpamListController(QObject *parent = 0);
    virtual ~SpamListController(){}

    bool isStarted() const;
    void startController();
    void stopController();
    
//protected:
    QFuture < SpamMessage > loadSpamListAsync();
    QFuture < SpamMessage > loadHistoryListAsync(const int& idWho, const QDateTime& from, const QDateTime& to);
public:

    typedef QMap<qint32, SpamContainer> ChatMap;

//    SpamContainer * messageList() const
//    {
//        return (SpamContainer *)(&_messageList);
//    }

    SpamContainer messageList() const
    {
        return _messageList;
    }

    SpamContainer messageChatIn(int contactId)const
    {
        return _chats_in[contactId];
    }

    SpamContainer messageChatOut(int contactId)const
    {
        return _chats_out[contactId];
    }

    //QString printMessages(int contactId, int count = -1);
    //QString printChat(int count = -1);
    bool isUnreadChatMessage(const int& operId) const;

    //qint32 messageListCount()const{return _messageList.count();}
    QString statusColor(const bool& isRead) const;

    qint32 findMessageById  (const qint32 & id  )const;
    void setAllMessagesRead (const int& contactId);

    QString getContactName(const int& contactId)const;
    void loadHistory(const int& idWho, const QDateTime& from, const QDateTime& to);

    // chats
    void sendMsg(const int& operID, const QString& text);
    void sendMsg2(const int& operID, const QString& text);
    void rosterIN(const int& operID, /*const QString& jid,*/ const QString& text);
    void rosterOUT(const int& operID, const QString& jid, const QString& text);
    //SpamMessage addChatMsg(const ccappcore::Message& m, const int& type);

    void setSelected(const int& index);

    int spamMessageTimeout()const{return _spamMessageTimeout;}

protected: //IAppSettingsHandler
    virtual void applySettings(const AppSettings& settings);
    virtual void flushSettings(AppSettings& settings);

signals:
    void messageAdded();
    void newMessage(SpamMessage message);
    void chatMsgAdded(SpamMessage message);
    void chatMsgAdded2(/*SpamMessage message*/);
    void updateContactStatus(const int& operID)const;
    void loadHistoryCompleted();
public slots:
    void on_c2oBBList(const ccappcore::Message& m);
    void on_c2oNeed_o2c_BBListReq(const ccappcore::Message& m);
    void on_c2oSendMsg(const ccappcore::Message& m);
    void on_c2oSendMsg2(const ccappcore::Message& m);
    void on_reloadSpamFinished();
    void on_loadHistoryFinished();
    void on_c2oMSGHistory(const ccappcore::Message& m);

    void showBalloon(SpamMessage);
private:
    LateBoundObject < IMessageSerializer > _messageSerializer;
    LateBoundObject < CallsController    > _callsController;
    LateBoundObject < IRouterClient      > _router;
    LateBoundObject < OperListController > _operListController;
    LateBoundObject < JabberController   > _jabberController;

    QFutureInterface< SpamMessage > _loadSpamResult;
    QFutureInterface< SpamMessage > _loadHistoryResult;

    SpamContainer        _messageList;
    SpamContainer        _messageListOld;
    SpamContainer        _historyList;
    int                  _operIdWho;
    bool                 _isStarted;
    QFutureWatcher<void> _loadSpamFuture;
    QFutureWatcher<void> _loadHistoryFuture;
    ChatMap              _chats_in;
    ChatMap              _chats_out;
    ChatMap              _chat;
private:
    EventListener _c2oBBListListener;
    EventListener _c2oSendMsgListener;
    EventListener _c2oSendMsgListener2;

    int                  _spamMessageTimeout;
};

} // namespace ccappcore

Q_DECLARE_METATYPE(ccappcore::SpamMessage);

#endif // SPAMLISTCONTROLLER_H
