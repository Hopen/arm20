#ifndef REQUESTREPLYMESSAGE_H
#define REQUESTREPLYMESSAGE_H

#include <QtCore>

#include "serviceprovider.h"
#include "message.h"
#include <QFutureInterface>


namespace ccappcore
{
class ClientAddress;

class RequestReplyMessage: public QObject
{
    Q_OBJECT;
public:

    static RequestReplyMessage* send (
            const Message& request,
            const Message& responsePrototype,
            const QObject* handler = 0,
            const char* replySlot = 0,
            const char* errorSlot = 0,
            const Message& errorPrototype = Message()
            ) throw();

    static RequestReplyMessage* create (
            const Message& request,
            const Message& responsePrototype,
            const QObject* handler = 0,
            const char* replySlot = 0,
            const char* errorSlot = 0,
            const Message& errorPrototype = Message()
            ) throw();

    void send();

    int requestQueryId() const { return _requestQueryId; }

    const QVariant& data() const { return _data; }
    void setData(const QVariant& data) { _data = data; }

    template<class T>
    QFutureInterface<T> futureInterface()
    {
        QFutureInterface<T>& pFuture = static_cast<QFutureInterface<T>&>(_futureInterface);
        return pFuture;
    }

    template<class T> QFuture<T> future()
    {
        return futureInterface<T>().future();
    }

    template<class T>
    void setFutureInterface(const QFutureInterface<T>& iFace)
    {
        _futureInterface = iFace;
    }



private:
    RequestReplyMessage(const Message& request,
                        const Message& responsePrototype,
                        const Message& errorPrototype);
    ~RequestReplyMessage();

signals:
    void responseReceived(const ccappcore::Message&);
    void error(const ccappcore::Message&);

private slots:
    void messageReceived(const ccappcore::Message& ,
                         const ccappcore::ClientAddress& ,
                         const ccappcore::ClientAddress&);
    void routeError(const ccappcore::Message&);
    void responseTimeout();

private:
    void stopMessageProcessing();

private:
    Message _request;
    Message _responsePrototype;
    Message _errorPrototype;
    QVariant _data;

    QTimer _timeoutTimer;
    QString _responseHandler;
    int _requestQueryId;
    volatile static int _lastQueryId;
    QFutureInterfaceBase _futureInterface;
};

typedef QSharedPointer<RequestReplyMessage> RequestReplyMessagePtr;

}


#endif // REQUESTREPLYMESSAGE_H
