#ifndef FILTEREDMODEL_H
#define FILTEREDMODEL_H

#include "predicate.h"

namespace ccappcore
{

template<class T>
class FilteredModel
{
public:
    FilteredModel() {}

    typedef typename Predicate<T>::SharedPtr FilterCriteria;

    void addFilter(FilterCriteria criteria);
    void removeFilter(FilterCriteria criteria);
    void clearFilter();

protected:
    virtual void applyFilter(const PredicateGroup<T>& filter) = 0;
    PredicateGroup<T> _filter;
};

template<class T>
void FilteredModel<T>::addFilter(/*FilteredModel::*/FilterCriteria criteria)
{
    PredicateGroup<T> copy(_filter);
    copy.add(criteria);
    applyFilter(copy);
}

template<class T>
void FilteredModel<T>::removeFilter(/*FilteredModel::*/FilterCriteria criteria)
{
    PredicateGroup<T> copy(_filter);
    copy.remove(criteria);
    applyFilter(copy);
}

template<class T>
void FilteredModel<T>::clearFilter()
{
    applyFilter(PredicateGroup<T>());
}

}

#endif // FILTEREDMODEL_H
