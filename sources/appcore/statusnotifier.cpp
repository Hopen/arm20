#include "statusnotifier.h"

namespace ccappcore
{

StatusNotifier::StatusNotifier(QObject *parent) :
    QObject(parent)
{
}

void StatusNotifier::postInfo(const QString& m)
{
    qDebug()<<"StatusNotifier Info:"<<m;
    emit info(m);
}



}
