#ifndef EVENTLISTENER_H
#define EVENTLISTENER_H

#include "appcore.h"
#include "irouterclient.h"
#include "serviceprovider.h"

namespace ccappcore
{

class APPCORE_EXPORT EventListener : public QObject
{
    Q_OBJECT;
public:
    EventListener();

    void startListening(IRouterClient* routerClient, const Message& prototype,
                        QObject* handler, const char* slot);
    void stopListening();

    bool isStarted() const { return _started; }

    bool subscribeRequired() const { return _subscribe; }
    void setSubscribeRequired(bool subscribe) { _subscribe = subscribe; }
    
signals:
    void onMessage(const ccappcore::Message& m);

private slots:
    void messageReceived(const ccappcore::Message& ,
                         const ccappcore::ClientAddress& ,
                         const ccappcore::ClientAddress&);

private:

    QObject* _handler;
    IRouterClient* _router;
    Message _prototype;
    bool _subscribe;
    bool _started;
};

}

#endif // EVENTLISTENER_H
