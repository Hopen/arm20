#ifndef LAYOUTMANAGER_H
#define LAYOUTMANAGER_H

#include "appcore.h"
#include "lateboundobject.h"
#include "appsettings.h"
#include <QDockWidget>

class QWidget;
class QMainWindow;
class QDockWidget;

namespace ccappcore
{

class APPCORE_EXPORT LayoutManager
    : public QObject
{
    Q_OBJECT
public:

    class LayoutSettings: public QMap<QString, QVariant>
    {
    public:
        QVariant getOption(const QString& key, const QVariant def = QVariant()) const
        {
            if(!this->contains(key))
                return def;
            return this->operator [](key);
        }
        void setUserOption(const QString& key, const QVariant& value) { this->operator [](key) = value; }
    };

    explicit LayoutManager(QObject *parent = 0);

    void addWatchWidget(QWidget* w);

    bool isLayoutLocked() const { return _lockLayout; }

public slots:
    void lockLayout(bool lock);

public:
    void saveLayout(LayoutSettings& settings);
    void restoreLayout(const LayoutSettings& settings);

private:
    QList<QWidget*> _watchWidgets;
    bool _lockLayout;

    void saveLayout(QWidget*, LayoutSettings&);
    void restoreLayout(QWidget*, const LayoutSettings&);

    QList<QDockWidget*> findVisibleDockedWidgets(QMainWindow* mainWindow);
    void updateLastDockedWidgetFeatures(QMainWindow* mainWindow);
    void updateFeatures(QDockWidget* dw);
private slots:
    void widgetDestroyed(QObject*);
    void updateLastDockedWidgetFeatures();

};

}

#endif // LAYOUTMANAGER_H
