#ifndef REDIRECTLISTCONTROLLER_H
#define REDIRECTLISTCONTROLLER_H

#include <QObject>
#include "appcore.h"
#include "imessageserializer.h"
#include "lateboundobject.h"
#include "irouterclient.h"

namespace ccappcore
{

const qint32 TEMPORARY_PROFILE_ID = 0x1000000;
const qint32 TEMPORARY_RULE_ID = 0x1000001;

const QString SYSTEM_CALL2NUM_RULENAME = QObject::tr("sys_call2num");
const QString SYSTEM_CALL2VM_RULENAME  = QObject::tr("sys_call2vm");


class CallsController;

class APPCORE_EXPORT RedirectProfile
{
public:
    RedirectProfile()
    {
        _id = TEMPORARY_PROFILE_ID;
        _taskId = -1;
        _status = QObject::tr("OFF");
    }
    RedirectProfile(const qint32& id, const QString& name, const qint32& taskId, const QString& status):
        _id(id), _name(name), _taskId(taskId), _status(status)
    {

    }
    bool IsValid()const
    {
        return !(_id==TEMPORARY_PROFILE_ID);
    }

    qint32  id     ()const{ return _id     ;}
    QString name   ()const{ return _name   ;}
    qint32  taskId ()const{ return _taskId ;}
    QString status ()const{ return _status ;}

    void setId     (const qint32 & id     ){ _id     = id     ;}
    void setName   (const QString& name   ){ _name   = name   ;}
    void setTaskId (const qint32 & taskId ){ _taskId = taskId ;}
    void setStatus (const QString& status ){ _status = status ;}

    QString taskId2Str()const{return (_taskId==-1)?QObject::tr(""):QObject::tr("%1").arg(_taskId);}

private:
    qint32   _id;
    QString  _name;
    qint32   _taskId;
    QString  _status;
};

class APPCORE_EXPORT RedirectRule
{
public:
    enum ePredefineRules
    {
        PR_CALL2NUM = 100,
        PR_CALL2VM
    };

    // values are the same as in database
    enum ERedirCondition
        {
        RC_Unknown    = 0,
        RC_ANumber    = 50,
        RC_Person     = 63,
        RC_OperState  = 53,
        RC_CallCount  = 54,
        RC_Time       = 96,
        RC_Date       = 98,
        RC_DatePeriod = 97,
        RC_DaysOfWeek = 99
        };

    // values are the same as in database
    enum ERedirAction
        {
        RA_Unknown = 0,
        RA_Phone   = 62,
        RA_Oper    = 1, // actions of this type should be translated to RA_Group (with private group)
        RA_Group   = 61,
        RA_Script  = 60
        };


    RedirectRule()
    {
        _id        = TEMPORARY_RULE_ID;
        _if_type   = RC_Unknown;
        _act_type  = RA_Unknown;
        _max_count = -1;
    }
    RedirectRule(const qint32 & id,
                 const QString& name,
                 const qint32 & if_type,
                 const QString& if_value,
                 const qint32 & act_type,
                 const QString& act_value,
                 const qint32 & max_count)
    {
        _id        = id;
        _name      = name;
        _if_type   = if_type;
        _if_value  = if_value;
        _act_type  = act_type;
        _act_value = act_value;
        _max_count = max_count;
    }
    qint32  id        ()const{return _id        ;}
    QString name      ()const{return _name      ;}
    qint32  if_type   ()const{return _if_type   ;}
    QString if_value  ()const{return _if_value  ;}
    qint32  act_type  ()const{return _act_type  ;}
    QString act_value ()const{return _act_value ;}
    qint32  max_count ()const{return _max_count ;}

    void setId      (const qint32 & id       ){_id        = id       ;}
    void setName    (const QString& name     ){_name      = name     ;}
    void setIfType  (const qint32 & if_type  ){_if_type   = if_type  ;}
    void setIfValue (const QString& if_value ){_if_value  = if_value ;}
    void setActType (const qint32 & act_type ){_act_type  = act_type ;}
    void setActValue(const QString& act_value){_act_value = act_value;}
    void setMaxCount(const qint32 & max_count){_max_count = max_count;}

    bool IsValid()const{return !(_id==TEMPORARY_RULE_ID);}

private:
    qint32  _id;
    QString _name;
    qint32  _if_type;
    QString _if_value;
    qint32  _act_type;
    QString _act_value;
    qint32  _max_count;
};

class APPCORE_EXPORT ScriptEvent
{
public:
    ScriptEvent()
    {
        _id = -1;
    }
    ScriptEvent(const int& id, const QString& name, const QString& desc):
        _id(id),
        _name(name),
        _desc(desc)
    {

    }

    qint32 id    ()const { return _id   ;}
    QString name ()const { return _name ;}
    QString desc ()const { return _desc ;}

    void setId   (const int    & id   ){_id   = id   ;}
    void setName (const QString& name ){_name = name ;}
    void setDesc (const QString& desc ){_desc = desc ;}

private:
    qint32  _id;
    QString _name;
    QString _desc;
};

class APPCORE_EXPORT RedirectListController : public QObject
{
    Q_OBJECT
public:
    explicit RedirectListController(QObject *parent = 0);
    const QIcon& statusIcon(int status) const;
    bool removeProfile    (const qint32& position);
    bool removeProfile    (const QString& name);
    bool createProfile    (const qint32& position);
    bool editProfile      (const qint32& position);
    bool setActiveProfile (const qint32& position, bool turnOFF = false);

    bool createRule   (const qint32& position);
    bool removeRule   (const qint32& position);
    bool editRule     (const qint32& position);
    bool removeRule   (const QString& name);

    qint32 findProfileByName(const QString& name)const;
    qint32 findProfileById  (const qint32 & id  )const;
    qint32 findRuleByName   (const QString& name)const;
    qint32 findRuleById     (const qint32 & id  )const;
    qint32 findScriptByName (const QString& name)const;

    QFuture < RedirectProfile > loadRedirectionsAsync();
    //QFuture < RedirectRule    > loadRedirRulesAsync   ();

    typedef QList<RedirectProfile> RedirProfileContainer;
    typedef QList<RedirectRule   > RedirRuleContainer;
    typedef QList<ScriptEvent    > ScriptEventContainer;

    RedirProfileContainer * profilesList() const
    {
        RedirProfileContainer * p = (RedirProfileContainer *)(&_profilesList);
        //return &_profilesList;
        return p;
    }
    RedirRuleContainer * rulesList() const
    {
        return (RedirRuleContainer *)(&_rulesList);
    }
    ScriptEventContainer *scriptsList()const
    {
        return (ScriptEventContainer*)(&_scriptsList);
    }

    qint32 profilesListCount()const{return _profilesList.count();}
    qint32 rulesListCount   ()const{return _rulesList   .count();}
    qint32 scriptsListCount ()const{return _scriptsList .count();}

    //void activateCall2NumProfile();
    //void activateSystemProfile(const qint32& ruleId);
    //void activateCall2VoicemailProfile();
//    void deactivateSystemProfiles();
    //void createSystemProfiles();
signals:
    void profileAdded  ();
    void profileEdited (qint32 pos);
    void ruleAdded     ();
    void ruleEdited    (qint32 pos);
    //void createCall2NumRule();
    //void createCall2VoicemailRule();
    //void createSysProfile(QString name);
public slots:

private slots:
    void on_c2oRedirProfiles     (const ccappcore::Message& m);
    void on_c2oCreateRedirProfile(const ccappcore::Message& m);
    void on_c2oGetRedirections   (const ccappcore::Message& m);
    void on_c2oCreateRedirRule   (const ccappcore::Message& m);
    void on_c2oScriptEvent       (const ccappcore::Message& m);

    //void on_sysRuleCreated    (int position);
    //void on_sysProfileCreated (int position);
private:
    LateBoundObject<IMessageSerializer> _messageSerializer;
    LateBoundObject<CallsController> _callsController;
    LateBoundObject<IRouterClient> _router;

    QFutureInterface< RedirectProfile > _loadRedirectionsResult;
    //QFutureInterface< RedirectRule    > _loadRulesResult;

    RedirProfileContainer _profilesList;
    RedirRuleContainer    _rulesList;
    ScriptEventContainer  _scriptsList;

};

} // namespace ccappcore

Q_DECLARE_METATYPE(ccappcore::RedirectProfile);

#endif // REDIRECTLISTCONTROLLER_H
