#ifndef OPERATORSCONTROLLER_H
#define OPERATORSCONTROLLER_H

#include "appcore.h"
#include "contactscontroller.h"
#include "sessioncontroller.h"
#include "imessageserializer.h"
#include "eventlistener.h"
#include "predicate.h"
#include "irouterclient.h"
#include <QIcon>
//#include "redirectlistcontroller.h"

namespace ccappcore
{

enum ContactType
{
    CT_Operator = 0,
    CT_Group = 1
};


typedef Contact Operator;
typedef Contact Group;
typedef ContactId GroupId;
typedef ContactId OperId;

enum GroupInfo
{
    GroupCode            = Contact::UserData,
    GroupFreeOpCount     = Contact::UserData + 1,
    GroupBusyOpCount     = Contact::UserData + 2,
    GroupOfflineOpCount  = Contact::UserData + 3,
    GroupOnlineOpCount   = Contact::UserData + 4,
    GroupPausedOpCount   = Contact::UserData + 5,
};

class APPCORE_EXPORT OperListController
    : public ContactsController
{
    Q_OBJECT;
public:
    OperListController(QObject* parent = 0);

    OperId createOperId(int id) const;
    GroupId createGroupId(int id) const;

    ccappcore::Contact sessionOwner() const { return _sessionOwner; }
    void setSessionOwner(const ccappcore::Operator& c);

    void startEventsListening();
    void stopEventsListening();

    const QList<Contact>& operList() const { return _operList; }
    const QList<Contact>& groupList() const { return _groupList; }

    QList<Contact> findOpers (const Predicate<Contact>& predicate) const;
    QList<Contact> findGroups(const Predicate<Contact>& predicate) const;

    Group findGroupByCode(const QString& groupCode);

public: //ContactsController impl
    virtual QFuture<Contact> loadContactsAsync();
    virtual QUrl buildContactUrl(const Contact&) const;

    virtual const QList<Contact>& contacts() const;
    virtual Contact findById(const ContactId&) const;

    virtual QString  infoDisplayName(int infoId) const;
    virtual QVariant modelData(const QModelIndex& index, const Contact&, int role) const;

    virtual void reqChangeStatus(const ccappcore::Contact&, ccappcore::Contact::ContactStatus, const QString &reason = "");
    virtual void reqChangeStatus(const ccappcore::ContactId&, ccappcore::Contact::ContactStatus, const QString &reason = "");
    QString statusAsString(Contact::ContactStatus status) const;

public:
    const QIcon& statusIcon(Contact::ContactStatus, bool roster = false) const;
    QColor statusColor(Contact::ContactStatus) const;

private slots:

    void on_c2oOperList(const ccappcore::Message& m);
    void on_c2oGroupList(const ccappcore::Message& m);
    void on_c2oOperInGroup(const ccappcore::Message& m);
    void on_c2oUpdateOperStatus(const ccappcore::Message& m);
    //void on_c2oRedirProfiles(const ccappcore::Message& m);

private:
    void updateOpStateCount();
    void updateOpStateCount(Contact, Contact::ContactStatus oldStatus, bool isNew);

    Operator _sessionOwner;
    QList<Operator> _operList;
    QList<Group> _groupList;
    EventListener _opStatusListener;

    QIcon _iconOperator;
    QIcon _iconGroup;

    QFutureInterface<Contact> _loadContactsResult;

    LateBoundObject<IRouterClient> _router;
    LateBoundObject<IMessageSerializer> _messageSerializer;
    //LateBoundObject<RedirectListController> _redirListController;
};

}

#endif // OPERATORSCONTROLLER_H
