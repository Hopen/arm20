#include "shortcutmanager.h"
#include <QAction>

#include "globalshortcut/globalshortcutmanager.h"
#include "lateboundobject.h"
#include "appsettings.h"

namespace ccappcore
{

ShortcutManager::ShortcutManager(QObject* parent)
    : QObject(parent)
{
}

void ShortcutManager::registerShortcut(const QString& path, const QString& descr)
{
    _mapNameDescr[path] = descr;
}

QString ShortcutManager::description(const QString& name) const
{
    if(_mapNameDescr.contains(name))
        return _mapNameDescr[name];

    return QString();
}

QStringList ShortcutManager::allNames()
{
    return _mapNameDescr.keys();
}

QKeySequence ShortcutManager::key(const QString& name)
{
    return _mapNameKey.value(name);
}

void ShortcutManager::setKey(const QString& name, const QKeySequence& key)
{
    _mapNameKey[name] = key;;
}

void ShortcutManager::connect(const QString& path, QObject* obj, const char* slot)
{
        if (obj == NULL || slot == NULL)
		return;

        QKeySequence shortcut = this->key(path);
        if(shortcut.isEmpty())
            return;

        if ( !path.startsWith("global.") )
        {
            bool appWide = path.startsWith("appwide.");
            QAction* act = new QAction(obj);
            act->setShortcut(shortcut);
            act->setShortcutContext(appWide ?
                                    Qt::ApplicationShortcut : Qt::WindowShortcut);
            if (obj->isWidgetType())
                    ((QWidget*) obj)->addAction(act);
            obj->connect(act, SIGNAL(activated()), slot);
	}
        else
        {
            GlobalShortcutManager::connect(shortcut, obj, slot);
	}
}

void ShortcutManager::applySettings(const AppSettings &settings)
{
    _mapNameKey.clear();
    foreach( const QString& name, _mapNameDescr.keys() )
    {
        QString path = QString("shortcuts/%1").arg(name);
        QVariant variant = settings.getOption(path);
        QKeySequence keySeq = variant.value<QKeySequence>();
        setKey(name, keySeq);
    }
}

void ShortcutManager::flushSettings(AppSettings &settings)
{
    foreach( const QString& name, _mapNameDescr.keys() )
    {
        QString path = QString("shortcuts/%1").arg(name);
        QKeySequence keySeq = key(name);
        if(keySeq.isEmpty())
            continue;
        settings.setUserOption(path,keySeq);
    }
}

}
