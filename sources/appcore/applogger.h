#ifndef APPLOGGER_H
#define APPLOGGER_H

#include "appcore.h"
#include "serviceprovider.h"
#include "ilogger.h"
#include "appsettings.h"

namespace Log4Qt
{
    class RollingFileAppender;
    class TTCCLayout;
}

namespace ccappcore
{

class APPCORE_EXPORT AppLogger : public ILogger, public IAppSettingsHandler
{
    Q_OBJECT;
    Q_INTERFACES(ccappcore::IAppSettingsHandler)
public:
    AppLogger(QObject* parent=0);

    virtual LogLevel::Value effectiveLevel() const;
    virtual void write(const QString& logger, LogLevel::Value level, const QString& );

    virtual void mdcSet(const QString& key, const QVariant& value);
    virtual const QVariant& mdcGet(const QString& key) const;
    virtual void mdcRemove(const QString& key);

    virtual void ndcPush(const QString& value);
    virtual void ndcPop();
    virtual void ndcClear();

public: //IAppSettingsVisitor
    virtual void applySettings(const AppSettings& settings);

private:
    Log4Qt::TTCCLayout* layout;
    Log4Qt::RollingFileAppender* fileAppender;
};

}

#endif // LOGGER_H
