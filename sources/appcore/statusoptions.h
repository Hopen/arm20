#ifndef STATUSREASONLIST_H
#define STATUSREASONLIST_H

#include "appcore.h"
#include "contact.h"
#include "appsettings.h"
#include <QIcon>

namespace ccappcore
{

class APPCORE_EXPORT StatusOptions
    : public QObject
    , public ccappcore::IAppSettingsHandler
{
    Q_OBJECT
    Q_INTERFACES(ccappcore::IAppSettingsHandler)
public:
    explicit StatusOptions(QObject *parent = 0);

    const QStringList& pauseReasons() const { return _pauseReasons; }
    const QStringList& pauseGlobalReasons() const { return _pauseGlobalReasons; }
    void setPauseReasons(const QStringList& data);

    bool pauseAfterLogon() const { return _pauseAfterLogon; }
    void setPauseAfterLogon(bool pause) { _pauseAfterLogon = pause; }

    bool pauseWhenDialing() const { return _pauseWhenDialing; }
    void setPauseWhenDialing(bool pause) { _pauseWhenDialing = pause; }

    bool isEnableOtherPauseReasons()const {return !!_otherPauseEnabled;}

signals:
    void dataChanged();

public: //ccappcore::IAppSettingsHandler
    virtual void applySettings(const AppSettings&);
    virtual void flushSettings(AppSettings&);
private:
    QStringList _pauseReasons;
    QStringList _pauseGlobalReasons;
    QString _pauseList;
    int     _otherPauseEnabled;

    //true to stay paused after logon, false to set free status.
    bool _pauseAfterLogon;
    //true to set pause when entering phone number
    bool _pauseWhenDialing;
};

}

#endif // STATUSREASONLIST_H
