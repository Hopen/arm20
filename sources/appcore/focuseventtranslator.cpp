#include "focuseventtranslator.h"
#include <QWidget>
namespace ccappcore
{

FocusEventTranslator::FocusEventTranslator(QWidget *observedWidget) :
    QObject(observedWidget)
{
    _observerdWidget = observedWidget;
    connect(qApp, SIGNAL(focusChanged(QWidget*,QWidget*)),
            this, SLOT(focusChanged(QWidget*,QWidget*)) );

}

void FocusEventTranslator::focusChanged(QWidget* oldWidget,QWidget* newWidget)
{
    if( oldWidget == _observerdWidget && newWidget!=_observerdWidget )
        emit focusOut();
    else if( newWidget== _observerdWidget && oldWidget != _observerdWidget )
        emit focusIn();
}


}
