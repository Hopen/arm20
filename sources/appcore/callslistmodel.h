#ifndef CALLSLISTMODEL_H
#define CALLSLISTMODEL_H

#include "appcore.h"
#include <QItemSelectionModel>
#include <QActionGroup>

#include "callscontroller.h"
#include "sessioncontroller.h"
#include "serviceprovider.h"
#include "lateboundobject.h"
#include "filteredmodel.h"

namespace ccappcore
{

class APPCORE_EXPORT CallsListModel
    : public QAbstractListModel
    , public FilteredModel<ccappcore::Call>
{
    Q_OBJECT;
public:

    enum ColDef
    {
        AbonPhoneColumn,
        CallStateColumn,
        GroupNameColumn,
        CallTypeColumn,
        BeginTimeColumn,
        StateTimeColumn,
        PriorityColumn,
        ExtraInfoColumn,
        LastOperNameColumn,
        CallLengthColumn,
    };

    CallsListModel( QObject* parent = 0);
    const QList<ccappcore::Call>& getCalls() const { return _calls; }
    void setCalls(const QList<Call>&);

    QVariant headerData(int section, Qt::Orientation orientation,
                                int role = Qt::DisplayRole) const;

    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    int rowCount(const QModelIndex& parent) const;

    QVariant data(const QModelIndex& index, int role) const;

    QModelIndex indexOf(ccappcore::Call) const;
    ccappcore::Call callAt(const QModelIndex& index) const;
    QIcon getCallStatusIcon(ccappcore::Call) const;

    const QList<ColDef> visibleColumns() const { return _visibleColumns; }
    void setVisibleColumns(const QList<ColDef>& columns);

protected: //FilteredModel
    virtual void applyFilter(const PredicateGroup<Call>& filter);

private:
    typedef QList<ccappcore::Call> CallsContainer;
    CallsContainer _calls;

    LateBoundObject<CallsController> _callsController;
    LateBoundObject<OperListController> _operListController;
    LateBoundObject<SessionController> _sessionController;

    QList<ColDef> _visibleColumns;
    QTimer _stateTimeTimer;
    bool _fixedCallsList;
private slots:
    void addCall(ccappcore::Call call);
    void removeCall(ccappcore::Call call);
    void callStateChanged(ccappcore::Call);
    void emitStateTimeChanged();
};

}
#endif // CALLSLISTMODEL_H
