#ifndef FUNCTOR_H
#define FUNCTOR_H

#include <QSharedPointer>
#include <QPointer>

namespace ccappcore
{

template <class TArg> class TFunctor
{
public:
    typedef QSharedPointer<TFunctor<TArg> > Ptr;
    virtual ~TFunctor() {}
    virtual void operator()(TArg arg) = 0;
};

template <class TClass, class TArg> class TSpecificFunctor : public TFunctor<TArg>
{
private:
    QPointer<TClass> objectPtr;
  void ( TClass::*funcPtr )( TArg );

public:
 TSpecificFunctor(TClass* objectPtr, void(TClass::*funcPtr)( TArg ))
    : objectPtr(objectPtr)
    , funcPtr(funcPtr)
 { }

  virtual void operator()(TArg arg)
   { (*objectPtr.*funcPtr)(arg);}
};

}
#endif // FUNCTOR_H
