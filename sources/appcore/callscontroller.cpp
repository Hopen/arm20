﻿#include "callscontroller.h"
#include "operlistcontroller.h"
#include "sessioncontroller.h"
#include "requestreplymessage.h"

#include <QDesktopServices>
//#include "scriptview.h"

namespace ccappcore {

const int DefaultResponseTimeoutInSeconds = 30;
const int DefaultAnswerTimeoutInSeconds = 20;

CallsController::CallsController(QObject* parent)
    : QObject(parent)
    , _isStarted(false)
    , _responseTimeoutInSeconds(DefaultResponseTimeoutInSeconds)
    , _noAnswerTimeoutInSeconds(DefaultAnswerTimeoutInSeconds)
{
    QDesktopServices::setUrlHandler("callto",this,"makeCall");

    //connect(this,SIGNAL(callAssigned(ccappcore::Call)),this,SLOT(force_answer(ccappcore::Call)));
}

ccappcore::Contact CallsController::sessionOwner() const
{
    if(!_sessionController.isValid())
        return Contact::invalid();

    return _sessionController->sessionOwner();
}

bool CallsController::isStarted() const
{
    return _isStarted;
}

void CallsController::startController()
{
    stopController();

    _c2oQueueCallListener.setSubscribeRequired(true);
    _c2oQueueCallListener.startListening(
            &_router, Message("C2O_QUEUE_CALL"), this, SLOT(on_c2oQueueCall(ccappcore::Message)));

    _c2oRemoveQueueCallListener.setSubscribeRequired(true);
    _c2oRemoveQueueCallListener.startListening(
            &_router, Message("C2O_REMOVE_QUEUE_CALL"), this, SLOT(on_c2oRemoveQueueCall(ccappcore::Message)));

    _c2oInitCall.startListening(
            &_router, Message("C2O_INIT_CALL"), this, SLOT(on_c2oInitCall(ccappcore::Message)));

    //_c2oCallAssignedListener.setSubscribeRequired(true);
    _c2oCallAssignedListener.startListening(
            &_router, Message("C2O_CALL_ASSIGNED"), this, SLOT(on_c2oCallAssigned(ccappcore::Message)));

    //_c2oIncomingCallListener.setSubscribeRequired(true);
    _c2oIncomingCallListener.startListening(
            &_router, Message("C2O_INCOMING_CALL"), this, SLOT(on_c2oIncomingCall(ccappcore::Message)));

    //_c2oCallResultListener.setSubscribeRequired(true);
    _c2oCallResultListener.startListening(
            &_router, Message("C2O_CALL_RESULT"), this, SLOT(on_c2oCallResult(ccappcore::Message)));

    _c2oRingStartedListener.startListening(
            &_router, Message("C2O_RING_STARTED"), this, SLOT(on_c2oRingStarted(ccappcore::Message)));

    //_c2oEndCallListener.setSubscribeRequired(true);
    _c2oEndCallListener.startListening(
            &_router, Message("C2O_END_CALL"), this, SLOT(on_c2oEndCall(ccappcore::Message)));

    //_c2oCompleteCallListener.setSubscribeRequired(true);
    _c2oCompleteCallListener.startListening(
            &_router, Message("C2O_COMPLETE_CALL"), this, SLOT(on_c2oEndCall(ccappcore::Message)));

    _c2oHungListener.startListening(
            &_router, Message("C2O_HUNG"), this, SLOT(on_c2oHung(ccappcore::Message)));

    _c2oHoldListener.startListening(
            &_router, Message("C2O_HOLD"), this, SLOT(on_c2oHold(ccappcore::Message)));

    _c2oMuteListener.startListening(
            &_router, Message("C2O_MUTE"), this, SLOT(on_c2oMute(ccappcore::Message)));

    _c2oRecordListener.startListening(
            &_router, Message("C2O_RECORD"), this, SLOT(on_c2oRecord(ccappcore::Message)));

    _c2oCreateConfListener.startListening(
            &_router, Message("C2O_CREATE_CONF"), this, SLOT(on_c2oCreateConf(ccappcore::Message)));

    _c2oJoinToConfListener.startListening(
            &_router, Message("C2O_JOIN_TO_CONF"), this, SLOT(on_c2oJoinToConf(ccappcore::Message)));

    _c2oRemoveFromConfListener.startListening(
            &_router, Message("C2O_REMOVE_FROM_CONF"), this, SLOT(on_c2oRemoveFromConf(ccappcore::Message)));

    _c2oRemoveConfListener.startListening(
            &_router, Message("C2O_REMOVE_CONF"), this, SLOT(on_c2oRemoveConf(ccappcore::Message)));

    reqQueueCallList();

    _isStarted = true;
}

void CallsController::setSubscribeAllCallEvents(bool subscribe)
{
    //_c2oInitCall.setSubscribeRequired(subscribe);
}

void CallsController::stopController()
{    
    if(!_isStarted)
        return;

    _c2oQueueCallListener.stopListening();
    _c2oRemoveQueueCallListener.stopListening();
    _c2oInitCall.stopListening();
    _c2oIncomingCallListener.stopListening();
    _c2oCallAssignedListener.stopListening();
    _c2oRingStartedListener.stopListening();
    _c2oCallResultListener.stopListening();
    _c2oEndCallListener.stopListening();
    _c2oHungListener.stopListening();

    _calls.clear();
    _isStarted = false;
}

void CallsController::answerCall(ccappcore::Call call)
{
#if (_CDSVER >= 0x0400)
    qWarning()<<"CallsController::answerCall() not implemented.";
    Message m("O2C_ANSWER_CALL");
    m["sender_oper_id"] = call.operId().toInt();
    m["call_id"] = call.callId();
    _router->send(m, ClientAddress::Any());
#else
    Message m("O2C_ROUTE_REQ");
    m["sender_oper_id"] = call.operId().toInt();
    m["call_id"] = call.callId();
    _router->send(m);
#endif
}

void CallsController::reqQueueCallList()
{
    Message m("O2C_QUEUE_CALL_LIST_REQ");\
    m["sender_oper_id"] = sessionOwner().contactId().toInt();
    m["oper_id"] = sessionOwner().contactId().toInt();
    RequestReplyMessage::send( m,
                               Message("C2O_QUEUE_CALL_LIST"),
                               this, SLOT(on_c2oQueueList(ccappcore::Message)));
}

void CallsController::on_c2oQueueList(const Message& m)
{
    if( 0==(quint32)m["sql_result"] )
    {
        QByteArray listData = m["data"];
        QList<Message> list = _messageSerializer->loadList(listData);
        foreach(const Message& row, list)
        {
            qDebug()<<row;
            on_c2oQueueCall(row);
        }
    }
}

void CallsController::on_c2oQueueCall(const Message& m)
{
    int callId = m["call_id"];
    Call c = findByCallId(callId);

    bool isNewCall = !c.isValid();

    c.setCallId(this, callId);

    if(!isNewCall)
        on_c2oRemoveQueueCall(m);

    if(m.contains("call_type"))
        c.setCallType(parseCallType(m["call_type"]));
    else if(isNewCall)
    {
        qWarning()<<"BUG: C2O_QUEUE_CALL && C2O_QUEUE_CALL_LIST must include call_type";
        c.setCallType(Call::CT_Incoming);
    }
    c.setCallState(Call::CS_Queued);
#if (_CDSVER < 0x0400)
    c.setBegin(m["begin"]);
#else
    c.setBegin(m["begin_time_t"]);
#endif
//    if(m.contains("begin_time_t"))
//        c.setStateTime(m["begin_time_t"]);
//    else
//        c.setStateTime(QDateTime::currentDateTime());

    if(m.contains("begin_queue_time_t"))
        c.setStateTime(m["begin_queue_time_t"]);
    else
        c.setStateTime(QDateTime::currentDateTime());

    c.setPriority(m["priority"]);
    c.setGroupId( _operListController->createGroupId(m["group_id"]) );
    c.setOperId( _operListController->createOperId(m["oper_id"]) );
    c.setANumber(m["a_num"]);
    c.setBNumber(m["b_num"]);
    c.setComments(m["extra_info"]);

    if(isNewCall)
    {
        _calls[callId] = c;
        emit callAdded(c);
    }


    emit queueCallAdded(c);
    emit callStateChanged(c);

    if( isPrivateGroupCall(c) )
        emit privateQueueCallAdded(c);
}

void CallsController::on_c2oRemoveQueueCall(const Message& m)
{
    int callId = m["call_id"];
    Call c = findByCallId(callId);
    if(!c.isValid())
        return;

    if( c.isQueued() )
    {
        //C2O_REMOVE_QUEUE comes when remove from the queue receibed from the script
        c.setCallState(Call::CS_Ivr);

        _calls.remove(callId); // fixed bug !!! the call is not removed from call list, when incoming call has ended
        //c.setCallState(Call::CS_Completed);
        emit callStateChanged(c);
        emit queueCallRemoved(c);

    }
}

bool CallsController::isPrivateGroupCall(ccappcore::Call c) const
{
    if(!sessionOwner().isValid())
        return false;

    if( callGroup(c).data(GroupCode).toString() == "private_"+sessionOwner().contactId().toString() )
        return true;
    return false;
}

bool CallsController::isAnotherPrivateGroupCall(ccappcore::Call c) const
{
    if(!sessionOwner().isValid())
        return false;

    QString callGroupCode = callGroup(c).data(GroupCode).toString();

    if ( (callGroupCode.startsWith("private_")                             ) &&
         (callGroupCode != "private_"+sessionOwner().contactId().toString())   )
        return true;

    return false;
}


bool CallsController::isSessionOwnerCall(ccappcore::Call call) const
{
    return call.operId() == sessionOwner().contactId();
}

bool CallsController::isOwnGroupCall(ccappcore::Call call) const
{
    if(!sessionOwner().isValid())
        return false;

    Contact g = callGroup(call);
    if(!g.isValid())
        return false;

    bool sessionOperInGroup = g.isParentOf(sessionOwner());
    return sessionOperInGroup;
}

bool CallsController::canMakeCall(const QString& phone)
{
    return !phone.isEmpty();
}

bool CallsController::canMakeCall(ccappcore::Call recentCall)
{
    return recentCall.isCompleted();
}

//void CallsController::execAbonentNumbersFromScriptParam(const QString &scriptParam, QString &a_num, QString &b_num)
//{
//    if (scriptParam.contains("callto:") || scriptParam.contains("xferto:"))
//    {
//        QString url = scriptParam.left(scriptParam.indexOf("?"));

//        if(url.contains("callto:"))
//            b_num = url.remove("callto:");
//        if(url.contains("xferto:"))
//            b_num = url.remove("xferto:");


//        url = scriptParam.right(scriptParam.size() - scriptParam.indexOf("?"));
//        QStringList params = url.split("&");
//        foreach(QString param,params)
//        {
//            if (param.contains("a_num="))
//                a_num = param.remove("a_num=");
//        }

//    }
//}

void CallsController::extractParamsFromScriptParam(const QString &incoming, ScriptParams & out_params)
{
    if (incoming.contains("callto:") || incoming.contains("xferto:"))
    {
        QString url = incoming.left(incoming.indexOf("?"));

        if(url.contains("callto:"))
            out_params["b_num"] = url.remove("callto:");
        if(url.contains("xferto:"))
            out_params["b_num"] = url.remove("xferto:");


        url = incoming.right(incoming.size() - incoming.indexOf("?") - 1);
        QStringList params = url.split("&");
        foreach(QString param,params)
        {
            QString param_name = param.left(param.indexOf("="));
            out_params[param_name] = param.right(param.size() - param.indexOf("=") - 1);
        }

    }
}

//void CallsController::execCallIDFromScriptParam(const QString &scriptParam, QString &callid)
//{
//    if (scriptParam.contains("callto:") || scriptParam.contains("xferto:"))
//    {
//        QString url = scriptParam.left(scriptParam.indexOf("?"));

//        QStringList params = url.split("&");
//        foreach(QString param,params)
//        {
//            if (param.contains("callid="))
//                callid = param.remove("callid=");
//        }

//    }
//}

void CallsController::makeCall(const QString& phone)
{
    if(!isStarted())
    {
        qWarning()<<"CallsController::makeCall('"<<phone<<"') has failed. "
                <<"Reason: CallsController is not started.";
        return;
    }

    if(phone.trimmed().isEmpty())
    {
        qWarning()<<"CallsController::makeCall('"<<phone<<"') has failed. "
                <<"Reason: Phone is empty.";
        return;
    }

    if (_lastCall.isValid() &&
        (_lastCall.callState() != Call::CS_OnHold) &&
        (_lastCall.callState() != Call::CS_Ivr   ) &&
        (_lastCall.callState()!= Call::CS_Completed)  )
    {
        holdCall(_lastCall,true);
    }


    QString b_num = phone,
            a_num;
    a_num.clear();

//    if (b_num.contains("callto:"))
//    {
//        QString url = b_num.left(b_num.indexOf("?"));

//        QStringList params = url.split("&");
//        foreach(QString param,params)
//        {
//            if (param.contains("callto:"))
//                b_num = param.remove("callto:");
//            if (param.contains("a_num="))
//                a_num = param.remove("a_num=");
//        }

//    }

    ScriptParams params;
    extractParamsFromScriptParam(phone,params);

    b_num = params.value("b_num", phone);
    a_num = params.value("b_num", "");

    Message m("O2C_MAKE_CALL_REQ");
    m["sender_oper_id"] = sessionOwner().contactId().toInt();
    m["phone"] = b_num;//phone;
    if (!a_num.isEmpty())
        m["a_num"] = a_num;
    m["query_id"] = 1234;

    _router->send(m, ClientAddress::Any());
}

void CallsController::makeCall(const QUrl& url)
{
    QString phone = url.toString(QUrl::RemoveScheme);
    phone.remove("/");
    makeCall(phone);
}

void CallsController::makeCall(ccappcore::Call recentCall)
{
    switch(recentCall.callType())
    {
        case Call::CT_Operator:
        {
            //Operator op = _operListController->findOperById(recentCall.operId());
            //makeCall(op);
            qWarning()<<"CallsController::makeCall(ccappcore::Call recentCall) not implemented";
            break;
        }
        case Call::CT_Incoming:
        {
            makeCall(recentCall.aNumber());
            break;
        }
        case Call::CT_Outgoing:
        {
            makeCall(recentCall.bNumber());
            break;
        }
        default:
        {
            qWarning()<<"CallsController::makeCall(recentCall) failed. "
                    <<"Unexpected call type: "<<recentCall.callType();
            break;
        }
    }
}

QString CallsController::callAbonPhone(ccappcore::Call call) const
{
    switch(call.callType())
    {
    case Call::CT_Incoming:
        {
            return call.aNumber();
        }
    case Call::CT_Outgoing:
        {
            return call.bNumber();
        }
    case Call::CT_Operator:
            {
                return call.aNumber();
            }
    default:
        qWarning()<<"CallsController::callAbonPhone("<<call.callId()<<") - invalid call type";
        return call.aNumber();
    }
}

Call CallsController::findCallByPhone(const QString &phone) const
{
    foreach(Call c, _calls)
        if(callAbonPhone(c) == phone)
            return c;

    return Call::invalid();
}

QString CallsController::callOperPhone(ccappcore::Call call) const
{
    switch(call.callType())
    {
    case Call::CT_Incoming:
        {
            return call.bNumber();
        }
    case Call::CT_Outgoing:
        {
            return call.aNumber();
        }
    case Call::CT_Operator:
            {
                return call.bNumber();
            }
    default:
        qWarning()<<"CallsController::callOperPhone("<<call.callId()<<") - invalid call type";
        return call.bNumber();
    }

}

ccappcore::Contact CallsController::callGroup(ccappcore::Call call) const
{
    Contact group = _operListController->findById(call.groupId());
    return group;
}

ccappcore::Contact CallsController::callOper(ccappcore::Call call) const
{
    ccappcore::Contact op = _operListController->findById(call.operId());
    return op;
}

QString CallsController::callShortInfo(ccappcore::Call call) const
{
    switch(call.callType())
    {
    case Call::CT_Incoming:
        {
            return call.aNumber() + tr(" - Входящий");
        }
    case Call::CT_Outgoing:
        {
            return call.bNumber() + tr(" - Исходящий");
        }
    case Call::CT_Operator:
            {
                Contact op = _operListController->findById(call.operId());
                if(isIncomingCall(call))
                    return op.name() + tr(" - Входящий");
                return op.name() + tr(" - Исходящий");
            }
    default:
        qWarning()<<"CallsController::getCallShortInfo("<<call.callId()<<") - invalid call type";
        return "?";
    }
}

QString CallsController::callContactId(ccappcore::Call call) const
{
    QString phone = qvariant_cast<QString>(callAbonPhone(call));
    QUrl url;
    url.setScheme("tel");
    url.setHost(
            phone.length() == 0 ? qvariant_cast<QString>(call.callId()) :phone
            );
    return url.toString();
}

bool CallsController::canTransferCall(ccappcore::Call call) const
{
    if(!call.isValid())
        return false;

    if(call.isCompleted())
        return false;

    return true;
}

void CallsController::transferLastCallToPhone(QString scriptParams)
{
    ScriptParams params;
    extractParamsFromScriptParam(scriptParams,params);

    QString phone = params.value("b_num", scriptParams);

    if(_lastCall.isValid())
        transferToPhone(_lastCall,phone);
}

void CallsController::transferToPhone(ccappcore::Call c, const QString& phone)
{
    if(phone.isEmpty())
    {
        qWarning()<<"CallsController::transferToPhone() - phone is empty.";
        return;
    }

    if(!canTransferCall(c))
    {
        qWarning()<<"CallsController::transferToPhone() - can't transfer call.";
        return;
    }

    Message m("O2C_TRANSFER_PHONE");
    m["phone"] = phone;
    m["sender_oper_id"] = sessionOwner().contactId().toInt();
    m["call_id"] = c.callId();

    setLastCall(Call::invalid());
    _router->send(m, ClientAddress::Any());
}

void CallsController::transferToIVR(ccappcore::Call c, const QString& funcName )
{
    if(!canTransferCall(c))
    {
        qWarning()<<"CallsController::transferToIVR() - can't transfer call.";
        return;
    }

    Message m("O2C_TRANSFER_SCRIPT");
    m["call_id"] = c.callId();
    m["event"] = funcName;
    m["end_call"] = 0; // do not end call for operator

    setLastCall(Call::invalid());
    _router->send(m, ClientAddress::Any());
}

//
void CallsController::transferToOperator(ccappcore::Call c, ccappcore::Contact contact)
{
    ccappcore::Operator op = static_cast<ccappcore::Operator>(contact);
    if(!op.isValid())
    {
        qWarning()<<"CallsController::transferToOperator() - operator is invalid.";
        return;
    }

    if(!canTransferCall(c))
    {
        qWarning()<<"CallsController::transferToOperator() - can't transfer call.";
        return;
    }

    Message m("O2C_TRANSFER_PHONE");
    m["phone"] = op.data(Contact::Phone).toString();
    m["sender_oper_id"] = sessionOwner().contactId().toInt();
    m["call_id"] = c.callId();

//    Message m("O2C_TRANSFER_GROUP");
//    m["script_code"] = QString("privat_")+qvariant_cast<QString>(op.operId());
//    m["sender_oper_id"] = sessionOwner().contactId();
//    m["call_id"] = c.callId();

    setLastCall(Call::invalid());
    _router->send(m, ClientAddress::Any());
}

void CallsController::transferToGroup(ccappcore::Call c,
                                      ccappcore::Contact contact)
{
    ccappcore::Group group = static_cast<ccappcore::Group>(contact);
    if(!group.isValid())
    {
        qWarning()<<"CallsController::transferToGroup() - invalid group.";
        return;
    }

    if(!canTransferCall(c))
    {
        qWarning()<<"CallsController::transferToGroup() - can't transfer call.";
        return;
    }

    Message m("O2C_TRANSFER_GROUP");
    m["script_code"] = group.data(GroupCode).toString();
    m["sender_oper_id"] = sessionOwner().contactId().toInt();
    m["call_id"] = c.callId();
    m["priority"] = c.priority();

    setLastCall(Call::invalid());
    _router->send(m, ClientAddress::Any());
}

bool CallsController::canConnectCalls(ccappcore::Call call1, ccappcore::Call call2)
{
    return call1.isValid() && !call1.isCompleted() &&
    call2.isValid() && !call2.isCompleted()
    && call1!= call2;
}

bool CallsController::canJoinCallToConf(ccappcore::Call call)
{
    // conference empty
    if (_conferenceInfo.isEmpty())
        return false;

    // assigned call should be on hold
    if (call.callState() != Call::CS_OnHold)
        return false;

    // call already has joined to conference
    for (ConferenceCollector::const_iterator cit = _conferenceInfo.begin();cit!= _conferenceInfo.end();++cit)
    {
        if (cit.value() == call.callId())
            return false;
    }

    return true;
}

bool CallsController::canUnjoinCallFromConf(ccappcore::Call call)
{
    // conference empty
    if (_conferenceInfo.isEmpty())
        return false;

    // assigned call conferencing
    if (call.callState() != Call::CS_Conferencing)
        return false;

    // call already has joined to conference
    for (ConferenceCollector::const_iterator cit = _conferenceInfo.begin();cit!= _conferenceInfo.end();++cit)
    {
        if (cit.value() == call.callId())
            return true;
    }

    return false;
}

bool CallsController::canRemoveConf(ccappcore::Call call)
{
    // conference empty
    if (_conferenceInfo.isEmpty())
        return false;

    // assigned call conferencing
    if (call.callState() != Call::CS_Conferencing)
        return false;

    return true;
}

void CallsController::connectCalls(QString scriptParam)
{
    QString callid;

    ScriptParams params;
    extractParamsFromScriptParam(scriptParam,params);

    callid = params.value("callid", "");

    if (callid.isEmpty())
    {
        qWarning()<<"CallsController::execCallIDFromScriptParam("<<scriptParam<<") - ccannot execute \"callid\".";
        return;
    }


//    Call callA = findCallByPhone(a_num);
//    Call callB = findCallByPhone(b_num);

    int CallID1 = lastCall().callId();
    int CallID2 = callid.toInt();

//    if (!callA.isValid())
//    {
//        qWarning()<<"CallsController::findCallByPhone("<<a_num<<") - call not found.";
//        return;
//    }

//    if (!callB.isValid())
//    {
//        qWarning()<<"CallsController::findCallByPhone("<<b_num<<") - call not found.";
//        return;
//    }

//    connectCalls(callA,callB);
    connectCalls(CallID1,CallID2);
}

void CallsController::connectCalls(ccappcore::Call call1,ccappcore::Call call2)
{
    connectCalls(call1.callId(),call2.callId());
}

void CallsController::connectCalls(int callId1, int callId2)
{
    if(!_calls.contains(callId1))
        qWarning()<<"CallsController::connectCalls("<<callId1
        <<","<<callId2<<" - callid1 not found";

    if(!_calls.contains(callId2))
        qWarning()<<"CallsController::connectCalls("<<callId1
        <<","<<callId2<<" - callid2 not found";

    Message m("O2C_CONNECT");
    m["sender_oper_id"] = sessionOwner().contactId().toInt();
    m["call_id1"] = callId1;
    m["call_id2"] = callId2;

    setLastCall(Call::invalid());
    _router->send(m, ClientAddress::Any());
}

void CallsController::on_c2oInitCall(const ccappcore::Message& m)
{
    int callId = m["call_id"];

    if (findByCallId(callId).isValid()) //1
        return;

    Call::CallType callType = parseCallType( m["call_type"] );
    Call c(this,callId,callType);
    c.setCallState(Call::CS_Ivr);
    c.setANumber(m["a_num"]);
    c.setBNumber(m["b_num"]);
    c.setStateTime(QDateTime::currentDateTime());
    _calls[callId] = c;

    emit callAdded(c);
}

void CallsController::on_c2oCallAssigned(const Message& m)
{
    Message m2 = m;
    m2["call_state"] = (int)Call::CS_Assigned;
    on_c2oIncomingCall(m2);
}

void CallsController::on_c2oIncomingCall(const Message& m)
{
    int callId = m["call_id"];
    Call c = findByCallId(callId);

    bool isNewCall = !c.isValid();

    Call::CallType callType = parseCallType( m["call_type"] );

    Call::CallState callState = ((bool)m["on_hold"])
                                ? Call::CS_OnHold : Call::CS_Calling;
    if(m.contains("call_state"))
        callState = (Call::CallState)m["call_state"].value().toInt();

    c.setCallId(this, callId);
    c.setCallType(callType);
    c.setCallState(callState);
    c.canReject(m["can_reject"]);
#if (_CDSVER < 0x0400)
    c.setBegin(m["begin"]);
#else
    c.setBegin(m["begin_time_t"]);
#endif
    c.setPriority(m["priority"]);
    c.setGroupId( _operListController->createGroupId(m["group_id"]) );
    c.setOperId( _operListController->createOperId(m["oper_id"]) );

//    int m_group_id = 516;//m["group_id"];
//    GroupId group_id = _operListController->createGroupId(m_group_id);

//    const QList<Contact>& source = _operListController->contacts();

//    int test = 0;


//    foreach(const Contact& c, source)
//    {
//        qDebug() << "GroupName: " << c.name() << ", GroupID = " << c.contactId().toInt();
//            if(c.contactId().toInt() == m_group_id)
//                test = 11111;
//    }

//    Contact group1 = _operListController->findById(group_id);

//    Contact group2 = callGroup(c);


//    if (group1.isValid())
//    {
//        test = 1;
//    }
//    if (group2.isValid())
//    {
//        test = 2;
//    }

//    if (group1 == group2)
//    {
//        test = 3;
//    }


    c.setANumber(m["a_num"]);
    c.setBNumber(m["b_num"]);
    c.setComments(m["extra_info"]);
    c.setOperCallId(m["oper_call_id"]);
    c.setStateTime(QDateTime::currentDateTime());

    if(isSessionOwnerCall(c))
        setLastCall(c);

    _calls[callId] = c;

    if(isNewCall)
        emit callAdded(c);

    if(c.isAssigned())
        emit callAssigned(c);
    else
        emit callStarted(c);

    emit callStateChanged(c);

    QTimer::singleShot(_responseTimeoutInSeconds * 1000, this, SLOT(avtoTransfer()));

    qDebug()<<"Auto transfer timeout started, callid = \""<<callId<<"\", expiration timeout =\"" << (_responseTimeoutInSeconds * 1000) << "\" ms";
}

void CallsController::on_c2oCallResult(const Message& m)
{
    int callId = m["call_id"];
    Call c = findByCallId(callId);
    if(!c.isValid())
    {
        qWarning()<<"CallsController::on_c2oCallResult(callId="<<callId<<")"
                <<" - Unexpected state: incoming call missed.";
//#if (_CDSVER >= 0x0400)
//#else
        Call linkedCall = findLinkedCall(callId);
        if( linkedCall.isValid() )
        {
            c = linkedCall; //assume ring started is for linked call
        }
        else
//#endif
        {
            Message c2o_incomingCall(m);
            c2o_incomingCall["can_reject"] = false;
            on_c2oIncomingCall(c2o_incomingCall);
            c = findByCallId(callId);
        }


    }

    Call::CallResult cr = (Call::CallResult) m["result"].value().toInt();

    if( cr == Call::CR_Succeded)
    {
        c.setCallState( Call::CS_Connected );
        int otherCallId = m["oper_call_id"];
        Call otherCall = findByCallId(otherCallId);
        if(!otherCall.isValid())
            qWarning()<<"CallsController::on_c2oCallResult() - call with "<<
                    "oper_call_id="<<otherCallId<<" not found";
        else if(otherCall.callState()!=Call::CS_Connected)
        {
            otherCall.setCallState( Call::CS_Connected );
            emit callStateChanged( otherCall );
            emit callConnected(otherCall);
        }

        if(isSessionOwnerCall(c))
            setLastCall(c);
        else if(isSessionOwnerCall(otherCall))
            setLastCall(otherCall);

        emit callStateChanged(c);
        emit callConnected(c);
    }
    else
    {
        c.setCallState( Call::CS_CallFailed );
        c.setStateReason( Call::callResultString(cr) );
        emit callFailed(c, cr, Call::callResultString(cr));
    }
}

void CallsController::on_c2oEndCall(const Message& m)
{
    int callId = m["call_id"];
    if(callId==-1)
    {
        qWarning()<<"CallsController::on_c2oEndCall(callId==-1)"
                <<". Invalid argument.";
        return;
    }

    Call c = findByCallId(callId);
    if(!c.isValid())
    {
        qWarning()<<"CallsController::on_c2oEndCall(callId="<<callId<<")"
                <<" - Unexpected state: call is not registered.";
        return;
    }

    QString stateReason = m["reason"];
    c.setCallState(Call::CS_Completed);
    c.setStateReason(stateReason);
    c.setStateTime(QDateTime::currentDateTime());
    _calls.remove(callId);

    emit callStateChanged(c);
    emit callEnded(c);
    emit callRemoved(c);
}

//void CallsController::on_c2oCompleteCall(const Message &m)
//{

//}

bool CallsController::isIncomingCall(ccappcore::Call c) const
{    
    return c.isIncoming() ||
            ( ( c.operId() == sessionOwner().contactId() ) && c.isOperator() );
}

bool CallsController::isOutgoingCall(ccappcore::Call c) const
{
    return !isIncomingCall(c);
}

void CallsController::on_c2oRingStarted(const Message& m)
{
    int callId = m["call_id"];
    Call c = findByCallId(callId);
    if( !c.isValid())
    {
        qWarning()<<"CallsController::on_c2oRingStarted() - "<<
                "Call "<<callId<<" is not found. ";

        Call linkedCall = findLinkedCall(callId);
        if( !linkedCall.isValid() )
            return;

        c = linkedCall; //assume ring started is for linked call
    }

    c.setCallState(Call::CS_RingStarted);
    emit callStateChanged(c);

    if( isIncomingCall(c) )
        emit callInRingStarted();
    else
        emit callOutRingStarted();
}

void CallsController::on_c2oHung(const Message& /*m*/)
{
    setLastCall(Call::invalid());
    emit hung();
}

Call CallsController::findByCallId(int callId) const
{
    if(!_calls.contains(callId))
        return Call();

    return _calls[callId];
}

Call CallsController::findLinkedCall(int callId) const
{
    foreach(Call c, _calls)
        if(c.operCallId() == callId)
            return c;

    return Call::invalid();
}

QList<Call> CallsController::allCalls() const
{
    return _calls.values();
}

QList<Call> CallsController::findCallsToConnect(Call callToConnect) const
{
    QList<Call> calls = allCalls();
    calls.removeOne(callToConnect);
    return calls;
}

QList<Call> CallsController::findCallsToConnect(Call callToConnect, const Predicate<Call> &predicate) const
{
    QList<Call> calls = findCalls(predicate);
    calls.removeOne(callToConnect);
    return calls;
}

void CallsController::endCall(ccappcore::Call call)
{
    Message m("O2C_END_CALL_REQ");
    m["sender_oper_id"] = call.operId().toInt();
    m["call_id"] = call.callId();

    _router->send(m);
}

void CallsController::holdCall(ccappcore::Call call, bool onOff)
{
    Message request("O2C_SET_HOLD");
    request["on_off"] = (int) onOff;
    request["call_id"] = call.callId();
    request["sender_oper_id"] = call.operId().toInt();
    _router->send(request, ClientAddress::Any());
}

void CallsController::on_c2oHold(const Message& m)
{
    int callId = m["call_id"];
    bool onHold = m["on_off"];
    Call c = findByCallId(callId);
    if(!c.isValid())
    {
        qWarning()<<"CallsController::on_c2oHold("<<callId<<") - call not found.";
        return;
    }

    c.setCallState( onHold ? Call::CS_OnHold : Call::CS_Connected );

    if( !onHold && isSessionOwnerCall(c) )
         setLastCall(c);

    emit callStateChanged(c);
}

void CallsController::muteCall(ccappcore::Call call, bool onOff)
{
    Message request("O2C_SET_MUTE");
    request["on_off"] = (int) onOff;
    request["call_id"] = call.callId();
    request["sender_oper_id"] = call.operId().toInt();

    _router->send(request, ClientAddress::Any());

}

void CallsController::on_c2oMute(const Message& m)
{
    int callId = m["call_id"];
    bool onOff = m["on_off"];
    Call c = findByCallId(callId);
    if(!c.isValid())
    {
        qWarning()<<"CallsController::on_c2oMute("<<callId<<") - call not found.";
        return;
    }

    c.setCallState( onOff ? Call::CS_Muted: Call::CS_Connected );
    emit callStateChanged(c);
}

void CallsController::recordCall(ccappcore::Call call, bool onOff)
{
    if(!_calls.contains(call.callId()))
        qWarning()<<"CallsController::recordCall("<<call.callId()<<") - call not found.";

    Message request("O2C_SET_RECORD");
    request["on_off"] = (int) onOff;
    request["call_id"] = call.callId();
    request["sender_oper_id"] = call.operId().toInt();

    _router->send(request, ClientAddress::Any());
}

void CallsController::on_c2oRecord(const Message& m)
{
    int callId  = m["call_id"];
    bool onOff  = m["on_off"];
    bool forced = m["forced"];
    Call c = findByCallId(callId);
    if(!c.isValid())
    {
        qWarning()<<"CallsController::on_c2oRecord("<<callId<<") - call not found.";
        return;
    }

    c.setRecording(onOff);
    c.setForcedRecording(forced);
    emit callStateChanged(c);
    emit recordCall(c, onOff, forced);
}

QList<Call> CallsController::findCalls(const Predicate<Call>& predicate) const
{
    QList<Call> r;
    foreach(Call c, _calls)
        if(c.isValid() && predicate(c))
            r.append(c);
    return r;
}

QFuture<void> CallsController::reqCallInfo(ccappcore::Call call)
{
    Message request("O2C_EXTRA_INFO_REQ");
    request["sender_oper_id"] = sessionOwner().contactId().toInt();
    request["call_id"] = call.callId();

    RequestReplyMessage* requestReply =
            RequestReplyMessage::create(request,Message("C2O_CALL_INFO"),
                                        this, SLOT(on_c2oCallInfo(ccappcore::Message)));
    requestReply->setData(QVariant::fromValue(call));
    requestReply->send();
    return requestReply->future<void>();
}

void CallsController::on_c2oCallInfo(const Message& m)
{
    RequestReplyMessage* reply = qobject_cast<RequestReplyMessage*>(sender());
    if(!reply)
        return;

    if( 0==(quint32)m["sql_result"] )
    {
        Call call = reply->data().value<Call>();
        int callId = m["call_id"];
        if(!call.isValid() || callId!=call.callId())
        {
            qWarning()<<"CallsController::on_c2oCallInfo() - call is invalid.";
            return;
        }

        call.clearExtraInfo();
        QByteArray listData = m["data"];
        QList<Message> list = _messageSerializer->loadList(listData);
        foreach(const Message& row, list)
        {
            qDebug()<<row;
            Call::CallInfoType type = (Call::CallInfoType)(int)row["info_id"];
            QString info = row["info"];
#if (_CDSVER < 0x0400)
            QDateTime timeAdded = row["begin"];
#else
            QDateTime timeAdded = row["begin_time_t"];
#endif

            ContactId personId = _operListController->createOperId(row["person_id"]);
            Contact whoAdded = _operListController->findById(personId);
            call.addExtraInfo(type,timeAdded,info, whoAdded);
        }
        emit callInfoChanged(call);
    }
}

void CallsController::addCallInfo(ccappcore::Call call, Call::CallInfoType type,
                                   const QString& info)
{
    call.addExtraInfo(
            type,
            QDateTime::currentDateTime(),
            info,
            sessionOwner()
            );

    Message m("O2C_ADD_EXTRA_INFO");
    m["sender_oper_id"] = sessionOwner().contactId().toInt();
    m["person_id"] = sessionOwner().contactId().toInt();
    m["call_id"] = call.callId();
    m["info_id"] = (int)type;
    m["info"] = info;
    _router->send(m);

    emit callInfoChanged(call);
}

void CallsController::setSubject(ccappcore::Call call, const QString& subj)
{
    QString subjCurrent = call.extraInfo(Call::CI_Subject);
    if(subjCurrent == subj)
        return;

    call.removeExtraInfo(Call::CI_Subject);
    addCallInfo(call, Call::CI_Subject, subj);
}

QFuture<void> CallsController::reqBindPerson(ccappcore::Call call)
{
    Message request("O2C_BIND_PERSON_INFO_REQ");
    request["sender_oper_id"] = sessionOwner().contactId().toInt();
    request["call_id"] = call.callId();
    RequestReplyMessage* r = RequestReplyMessage::send(request,
                              Message("C2O_BIND_PERSON_INFO")
                              , this, SLOT(on_c2oBindPerson(ccappcore::Message)));
    return r->future<void>();
}

QFuture<Call> CallsController::reqCallHistory(ccappcore::Call call, const QDateTime& from, const QDateTime& to)
{
    QVariant personId = 0;
    QString phone;
    if(call.contact().isValid())
        personId = call.contact().contactId();
    else
        phone = callAbonPhone(call);

    Message request("O2C_GET_CALL_HISTORY");
    request["sender_oper_id"] = sessionOwner().contactId().toInt();
    request["from"] = from;
    request["to"] = to;
    request["person_id"] = 0;
    request["abon_number"] = phone;
    request["group_id"] = 0;
    request["oper_id"] = 0;


    RequestReplyMessage* requestReply = RequestReplyMessage::create(request,
                              Message("C2O_CALL_HISTORY")
                              , this, SLOT(on_c2oCallHistory(ccappcore::Message)));

    if(call.contact().isValid())
        requestReply->setData(QVariant::fromValue(call.contact()));
    requestReply->send();
    return requestReply->future<Call>();
}

void CallsController::on_c2oCallHistory(const Message& m)
{
    RequestReplyMessage* reply = qobject_cast<RequestReplyMessage*>(sender());
    if(!reply)
        return;

    Contact contactLink = reply->data().canConvert<Contact>()
                    ? reply->data().value<Contact>()
                    : Contact();

    QFutureInterface<Call> futureInterface = reply->futureInterface<Call>();
    if( 0==(quint32)m["sql_result"] )
    {
        QByteArray listData = m["data"];
        QList<Message> list = _messageSerializer->loadList(listData);
        QVector<Call> results;
        foreach(const Message& row, list)
        {
            Call call(this,row["call_id"],parseCallType(row["call_type"]));
            call.setANumber(row["a_num"]);
            call.setBNumber(row["b_num"]);
            call.setCallState(Call::CS_Completed);
            call.setContact(contactLink);

#if (_CDSVER < 0x0400)
            QDateTime begin = row["begin"];
#else
            QDateTime begin = row["begin_time_t"];
#endif

            QDateTime end = begin.addSecs(row["call_time"]);
            call.setBegin(begin);
            call.setStateTime(end);

            results.append(call);
            qDebug()<<row;
        }        
        if(results.count()>0)
            futureInterface.reportResults(results,0,results.count());
    }
    futureInterface.reportFinished();
}

Call::CallType CallsController::parseCallType(const QString& strCallType)
{
    Call::CallType callType = Call::CT_Unknown;
    if( strCallType == "OP" )
        callType = Call::CT_Operator;
    else if( strCallType == "OUT" )
        callType = Call::CT_Outgoing;
    else if( strCallType == "IN" || strCallType == "NTF")
        callType = Call::CT_Incoming;

    return callType;
}

QString CallsController::callInfoString(const ccappcore::CallInfo& ci) const
{
    switch(ci.infoType())
    {
    case Call::CI_CallConnected:
        {
            Contact op = ci.contactLink();
            return QObject::tr("Соединен с оператором ")
                + ( op.isValid() ? "'" + op.name() + "'" : "" );
        }
    case Call::CI_CallAssigned:
        {
            Contact op = ci.contactLink();
            return QObject::tr("Распределен на оператора ")
                + ( op.isValid() ? "'" + op.name() + "'" : "" );
        }
    case Call::CI_CallQueued:
        {
        Contact group = ci.contactLink();
        return QObject::tr("Поставлен в очередь ")
                + ( group.isValid() ? "'" + group.name() + "'" : "" );
        }
    case Call::CI_CallEnded:
        {
            return QObject::tr("Завершен");
        }
    case Call::CI_IVRMenuItemIn:
        {
        return QObject::tr("Вход в IVR меню ")
                + ( ci.value().isEmpty() ? "" : "'" + ci.value() + "'" );
        }
    case Call::CI_AdditionalTextInfo:
        {
            Contact op = ci.contactLink();
            return QString( op.isValid() ? op.name() + ": " : "" )
                    + ci.value();
        }
    case Call::CI_CallRecordOn:
        {
            return QObject::tr("Включена запись")+"<a href='"+ci.value()+"'>"+
                      "<img src=':/resources/icons/download_16x16.png'/>Скачать файл</a>";
        }
    default:
        return ci.value();
    }
}

void CallsController::setLastCall(ccappcore::Call call)
{
    _lastCall = call;
    emit lastCallChanged(call);
}

int CallsController::countCalls()const
{
    return _calls.count();
}

QString CallsController::callPriority(ccappcore::Call c) const
{
    switch (c.priority())
    {
    default:
    {
        return QString(tr("%1")).arg(c.priority());
    }
    }
}


void CallsController::createConference(CallIDCollector *callsToConnect)
{
    QMutexLocker lock(&sendingLock);
    Q_UNUSED(lock);

    _joinedCalls = *callsToConnect;

    Message m("O2C_CREATE_CONF_REQ");
    m["query_id"] = 5678;

    _router->send(m, ClientAddress::Any());
}

void CallsController::joinCallsToConference(CallIDCollector *callsToConnect, const int& conf_id)
{
    QMutexLocker lock(&sendingLock);
    Q_UNUSED(lock);

    _joinedCalls = *callsToConnect;

    joinCallsToConference(conf_id);

}

void CallsController::joinCallsToConference(const int& conf_id)
{
    foreach(int call_id, _joinedCalls)
    {
        joinCallToConference(conf_id,call_id);
    }

}

void CallsController::joinCallToConference(const int &conf_id, const int &call_id)
{
    Message m("O2C_JOIN_TO_CONF");
    m["conf_id"] = conf_id;
    m["call_id"] = call_id;
    m["status" ] = 0;
    _router->send(m, ClientAddress::Any());
}

void CallsController::unjoinCallFormConference(const int& conf_id, const int &call_id)
{
    CallIDCollector calls = _conferenceInfo.values(conf_id);
    if (calls.isEmpty() || (calls.indexOf(call_id) == -1))
    {
        qWarning()<<"CallsController::unjoinCallFormConference('"<<call_id<<"') has failed. "
                 <<"Reason: callid" << call_id << "hasn't joined to conference with conf_id "
                   << conf_id;
        return;
    }

    Message m("O2C_REMOVE_FROM_CONF");
    m["conf_id"] = conf_id;
    m["call_id"] = call_id;

    _router->send(m, ClientAddress::Any());

//    if (calls.size() == 1) // remove conference
//    {
//        //m.setName("O2C_REMOVE_CONF");
//        //_router->send(m, ClientAddress::Any());
//        removeConference(conf_id);
//    }
}

void CallsController::removeConference(const int &conf_id)
{
    Message m("O2C_REMOVE_CONF");
    m["conf_id"] = conf_id;

    _router->send(m, ClientAddress::Any());
}

void CallsController::on_c2oBindPerson(const Message &)
{

}

void CallsController::on_c2oCreateConf(const ccappcore::Message &m)
{
    QMutexLocker lock(&sendingLock);
    Q_UNUSED(lock);

    int conf_id = m["conf_id"];
    int call_id = m["call_id"]; // opercall

    Call linkedCall = findLinkedCall(call_id);
    if (linkedCall.isValid())
    {
        _conferenceInfo.insertMulti(conf_id,call_id);
        linkedCall.setCallState(Call::CS_Conferencing);
        linkedCall.setConfId(conf_id);
    }

    joinCallsToConference(conf_id);
}

void CallsController::on_c2oJoinToConf(const ccappcore::Message &m)
{
    QMutexLocker lock(&sendingLock);
    Q_UNUSED(lock);

    int callId = m["call_id"];
    int confId = m["conf_id"];

    if (confId < 0)
    {
        emit joinCallToConfFailed(callId,confId);
        return;
    }

    Call joinedCall = findByCallId(callId);
    if (joinedCall.isValid())
    {
        _conferenceInfo.insertMulti(confId,callId);
        joinedCall.setCallState(Call::CS_Conferencing);
        joinedCall.setConfId(confId);
        //emit callJoined(joinedCall);
        emit callStateChanged(joinedCall);
    }
}

void CallsController::on_c2oRemoveFromConf(const ccappcore::Message &m)
{
    QMutexLocker lock(&sendingLock);
    Q_UNUSED(lock);

    int conf_id = m["conf_id"];
    int call_id = m["call_id"];

    CallIDCollector calls = _conferenceInfo.values(conf_id);

    if (calls.isEmpty() || (calls.indexOf(call_id) == -1))
    {
        qWarning()<<"CallsController::on_c2oRemoveFromConf('"<<call_id<<"') has failed. "
                 <<"Reason: callid" << call_id << "hasn't joined to conference with conf_id "
                   << conf_id;
        return;

    }

    Call call = findByCallId(call_id);
    if (call.isValid())
    {
        call.setCallState(Call::CS_OnHold);
        call.setConfId(-1);
        emit callStateChanged(call);
    }

    _conferenceInfo.remove(conf_id, call_id);
}

void CallsController::on_c2oRemoveConf(const ccappcore::Message &m)
{
    QMutexLocker lock(&sendingLock);
    Q_UNUSED(lock);

//    int conf_id = m["conf_id"];

//    CallIDCollector calls = _conferenceInfo.values(conf_id);

//    foreach (int call_id, calls)
//    {
//        Call call = findByCallId(call_id);
//        if (call.isValid())
//        {
//            call.setCallState(Call::CS_OnHold);
//            call.setConfId(-1);
//            emit callStateChanged(call);
//        }

//        _conferenceInfo.remove(conf_id, call_id);

//    }

}

void CallsController::playDTMF(ccappcore::Call call, const QString &dtmf)
{
    if(!isStarted())
    {
        qWarning()<<"CallsController::playDTMF('"<<dtmf<<"') has failed. "
                <<"Reason: CallsController is not started.";
        return;
    }


    if (call.isValid() && (call.callState() == Call::CS_Connected))
    {
        Message m("O2C_PLAY_DTMF");
        m["call_id"] = call.callId();
        m["dtmf"] = dtmf;

        _router->send(m, ClientAddress::Any());
    }


}

void CallsController::avtoTransfer()
{
    Call c = lastCall();
    int callId = c.callId();
    Call::CallState callState = c.callState();
    if ( callState == Call::CS_Assigned ||
         callState == Call::CS_Calling  ||
         callState == Call::CS_RingStarted )
    {
        qDebug()<<"Auto transfer: call (callid=\"" <<callId<<"\") has waited enough";
        c.setPriority(c.priority() + 10);
        transferToGroup(c,callGroup(c));
    }

}

void CallsController::applySettings(const AppSettings& settings)
{
    _responseTimeoutInSeconds = settings.getOption("calls/responseTimeout").toInt();
    if (0 == _responseTimeoutInSeconds)
        _responseTimeoutInSeconds = DefaultResponseTimeoutInSeconds;

    _noAnswerTimeoutInSeconds = settings.getOption("calls/noAnswerTimeout").toInt();
    if (0 == _noAnswerTimeoutInSeconds)
        _noAnswerTimeoutInSeconds = DefaultAnswerTimeoutInSeconds;
}

void CallsController::flushSettings(AppSettings& settings)
{
    settings.setAppOption( "calls/responseTimeout", _responseTimeoutInSeconds );

    settings.setAppOption( "calls/noAnswerTimeout", _noAnswerTimeoutInSeconds );
}

int CallsController::noAnswerTimeoutSeconds() const
{
    return _noAnswerTimeoutInSeconds;
}

// TESTING
//#include <QWaitCondition>
//void Sleep(int ms)
//{
//    QWaitCondition sleep;
//    QMutex mutex;
//    sleep.wait(&mutex, ms);
//}

//void CallsController::force_answer(ccappcore::Call c)
//{
//    answerCall(c);
//    int milliseconds = 5 * 1000;
//    Sleep(milliseconds);
//    endCall(c);
//    //QTimer::singleShot(1000, this, SLOT(test_hungup()));
//}

////void CallsController::force_hungup()
////{
////    qDebug << tr("test force hungup\n");
////    //endCall(_associatedCall);
////}


} // namespace ccappcore


