#ifndef NTLMAUTHENTICATION_H
#define NTLMAUTHENTICATION_H

#include <QObject>
#include "message.h"
#include "eventlistener.h"
#include "irouterclient.h"
#include "lateboundobject.h"

namespace ccappcore
{
class SessionController;

class APPCORE_EXPORT INtlmLogonProvider: public QObject
{
    Q_OBJECT
public:
    INtlmLogonProvider(QObject* parent = 0) : QObject(parent) {}
    virtual ~INtlmLogonProvider() {}
public slots:
    virtual void startAuthentication() = 0;
    virtual QString queryUserName() = 0;

signals:
    void authenticationStarted();
    void authenticationCompleted();
};

#ifdef Q_WS_WIN

class APPCORE_EXPORT NTLMAuthentication : public INtlmLogonProvider
{
    Q_OBJECT
public:
    explicit NTLMAuthentication(QObject* parent = 0);
    ~NTLMAuthentication();

    virtual void startAuthentication();
    virtual QString queryUserName();

private slots:
    void on_c2oNtlmChallengeRequest(const ccappcore::Message&);

private:
    class Private;
    Private* p;
    LateBoundObject<SessionController> sessionController;
    EventListener messageFilter;
    LateBoundObject<IRouterClient> router;
};

#endif

}

#endif // NTLMAUTHENTICATION_H
