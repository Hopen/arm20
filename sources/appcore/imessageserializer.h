#ifndef IMESSAGESERIALIZER_H
#define IMESSAGESERIALIZER_H

#include "appcore.h"
#include "message.h"

namespace ccappcore
{
    class APPCORE_EXPORT IMessageSerializer: public QObject
    {
        Q_OBJECT;
        public:
        virtual ~IMessageSerializer() {}

        virtual void save(QDataStream& ds, const Message& msg) = 0;
        virtual Message load(QDataStream& ds) = 0;

        virtual void saveParam(QDataStream& ds, const MessageParam& msg)= 0;
        virtual MessageParam loadParam(QDataStream& ds) = 0;

        virtual void saveList(QByteArray& data, const QList<Message>& msg)= 0;
        virtual QList<Message> loadList(const QByteArray& data) = 0;
    };
}

Q_DECLARE_INTERFACE(ccappcore::IMessageSerializer, "ru.forte-it.ccappcore.imessageserializer/1.0")

#endif // IMESSAGESERIALIZER_H
