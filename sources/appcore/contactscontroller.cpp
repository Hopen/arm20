#include "contactscontroller.h"
#include "callscontroller.h"

#include "useritemdatarole.h"
#include <QIcon>

namespace ccappcore
{    

class ContactsController::Private
{
public:
    Private()
        : lockObject(QReadWriteLock::Recursive)
    {}

    LateBoundObject<CallsController> callsController;
    QFutureInterface<Contact> searchResult;
    QReadWriteLock lockObject;
};

ContactsController::ContactsController(QObject* parent)
    : QObject(parent)
    , d(new ContactsController::Private())
{
}

ContactsController::~ContactsController()
{
    delete d;
}

QReadWriteLock* ContactsController::syncRoot()
{
    return &d->lockObject;
}

QFuture<Contact> ContactsController::loadChildrenAsync(Contact /*parent*/)
{
    //override method if contacts can be loaded asynchronously
    return QFuture<Contact>();
}

QFuture<Contact> ContactsController::searchContactsAsync(const QString &filter)
{
    d->searchResult = QFutureInterface<Contact>( (QFutureInterfaceBase::State)
             (QFutureInterfaceBase::Running | QFutureInterfaceBase::Started) );

    QReadLocker lock(syncRoot());
    Q_UNUSED(lock);
    QVector<Contact> result;
    foreach(Contact rootContact, contacts() )
    {
        QQueue<Contact> stack;
        stack.append(rootContact);
        while(stack.count()>0)
        {
            Contact current = stack.dequeue();
            if(current.name().contains(filter, Qt::CaseInsensitive))
                result.append(current);

            foreach(Contact c, current.children())
                stack.enqueue(c);
        }
    }

    if(result.count()>0)
    {
        d->searchResult.reportResults(result, 0, result.count());
    }
    d->searchResult.reportFinished();
    return d->searchResult.future();
}

Contact ContactsController::findById(const ContactId& id) const
{
    foreach(Contact rootContact, contacts() )
    {
        QQueue<Contact> stack;
        stack.append(rootContact);
        while(stack.count()>0)
        {
            Contact current = stack.dequeue();
            if(current.contactId() == id)
                return current;

            foreach(Contact c, current.children())
                stack.enqueue(c);
        }
    }
    return Contact::invalid();
}

bool ContactsController::canChangeStatus(const Contact&) const
{
    return false;
}

void ContactsController::reqChangeStatus(const Contact&, Contact::ContactStatus, const QString& /*reason*/)
{
    qWarning("ContactsController::reqChangeStatus() is not supported.");
}

bool ContactsController::canMakeCall(const Contact& c) const
{  
    if(!c.data(Contact::Phone).isValid())
        return false;

    if(c.contactId() == d->callsController->sessionOwner().contactId())
        return false;

    return true;
}

bool ContactsController::canSendMessage(const Contact& c) const
{
    if(c.contactId() == d->callsController->sessionOwner().contactId())
        return false;

    return true;
}


bool ContactsController::canTransferCall(const ccappcore::Call& call, const Contact& contact) const
{
    Q_UNUSED(call);
    return canMakeCall(contact);
}

QString ContactsController::infoDisplayName(int infoId) const
{
    switch( (Contact::ContactInfo)infoId )
    {
        case Contact::Name:
            return tr("Имя");
        case Contact::LongName:
            return tr("Полное имя");
        case Contact::Status:
            return tr("Статус");
        case Contact::StatusTime:
            return tr("Время изменения статуса");
        case Contact::StatusReason:
            return tr("Доп. статус");
        case Contact::Email:
            return tr("E-mail");
        case Contact::Phone:
            return tr("Телефон");
        case Contact::OfficePhone:
            return tr("Раб. телефон");
        case Contact::MobilePhone:
            return tr("Моб. телефон");
        case Contact::Fax:
            return tr("Факс");
        case Contact::Address:
            return tr("Адрес");
        case Contact::Organization:
            return tr("Организация");
        case Contact::JobPosition:
            return tr("Должность");
        case Contact::Location:
            return tr("Регион");
//        case Contact::BookName:
//            return tr("Книга");
        default:
            return tr("Доп. информация");
    }   
}

QVariant ContactsController::modelData(const QModelIndex& index, const Contact& c, int role) const
{
    if(role == Qt::DecorationRole && index.column()==0)
    {
        static QIcon contactIcon(":/resources/icons/contact_16x16.png");
        static QIcon contactsGroupIcon(":/resources/icons/contacts_group_16x16.png");

        if( c.isContainer() )
            return contactsGroupIcon;

        return contactIcon;
    }

    return QVariant();
}


}
