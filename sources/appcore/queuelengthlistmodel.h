#ifndef QUEUELENGTHLISTMODEL_H
#define QUEUELENGTHLISTMODEL_H

#include "appcore.h"
#include "filteredmodel.h"
#include "callscontroller.h"
#include "operlistcontroller.h"

namespace ccappcore {

class APPCORE_EXPORT QueueLengthListModel :
        public FilteredModel<ccappcore::Contact>
{
Q_OBJECT
public:
    explicit QueueLengthListModel(QObject *parent = 0);

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;

    virtual int columnCount(const QModelIndex &parent) const;

    virtual QVariant data(const QModelIndex &index,
                          int role = Qt::DisplayRole) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation,
                                int role = Qt::DisplayRole) const;

    void applyFilter();
signals:

private slots:

    void notifyQueueLengthChanged();
private:    
    LateBoundObject<OperListController> _operListController;
    LateBoundObject<CallsController> _callsController;
    QList<Group> _groups;
};

} // namespace ccappcore

#endif // QUEUELENGTHLISTMODEL_H
