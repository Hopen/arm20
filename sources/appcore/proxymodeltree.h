#ifndef PROXYMODELTREE_H
#define PROXYMODELTREE_H

#include "appcore.h"
#include "proxymodelshared.h"

namespace ccappcore
{

class APPCORE_EXPORT ProxyModelTree : public QAbstractItemModel
{
    Q_OBJECT
public:
    ProxyModelTree(QObject *parent = 0);

public:
    QModelIndex index(int row, int column,
                              const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &child) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex& index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    QStringList mimeTypes() const;
    QMimeData *mimeData(const QModelIndexList &indexes) const;
    bool dropMimeData(const QMimeData *data, Qt::DropAction action,
                              int row, int column, const QModelIndex &parent);
    Qt::DropActions supportedDropActions() const;

    const ModelList& sourceModelList() const { return _visibleModels; }
    void setSourceModelList(const ModelList& source);
    void removeModel(ModelPtr model);
    void insertModel(ModelPtr model, int pos);
    void hideModel(ModelPtr model);
    void showModel(ModelPtr model);
    ModelPtr modelAt(const QModelIndex& index) const;
    ModelPtr modelPtr(QAbstractItemModel*) const;

    bool isRootIndex(const QModelIndex&) const;
    virtual void setRootData(const ModelPtr, const QVariant& data, int role);
    virtual QVariant getRootData(const ModelPtr, int role) const;

public:
    QModelIndex mappedIndexFromSourceIndex(const QModelIndex& index) const;
    QModelIndex sourceIndexFromMappedIndex(const QModelIndex& index) const;
    QModelIndex index(const QAbstractItemModel* model) const;

private:
    void connectModel(ModelPtr model);

protected:
    ModelList _visibleModels;
    ModelList _sourceModels;
    QMap<QAbstractItemModel*, QMap<int, QVariant> > _rootData;

    mutable QMap<QModelIndex, QModelIndex> _sourceToMapped;
    mutable QMap<QModelIndex, QModelIndex> _mappedToSource;
    mutable int _nextIndexId;
    void* getNextIndexId() const { return (void*)++_nextIndexId; }

    void hideModelIfEmpty(QAbstractItemModel* model);
    void showModelIfNotEmpty(QAbstractItemModel* model);
private slots:

    void sourceModelLayoutAboutToBeChanged();
    void sourceModelLayoutChanged();

    void sourceModelDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);
    void sourceModelReset();

    void sourceModelRowsAboutToBeInserted(const QModelIndex &parent, int first, int last);
    void sourceModelRowsInserted(const QModelIndex &parent, int first, int last);

    void sourceModelRowsAboutToBeRemoved(const QModelIndex &parent, int first, int last);
    void sourceModelRowsRemoved(const QModelIndex &parent, int first, int last);
};

}
#endif // PROXYMODELTREE_H
