#ifndef STATUSNOTIFIER_H
#define STATUSNOTIFIER_H

#include "appcore.h"

namespace ccappcore
{

class APPCORE_EXPORT StatusNotifier : public QObject
{
Q_OBJECT
public:
    explicit StatusNotifier(QObject *parent = 0);

signals:
    void info(const QString&);
public slots:
    void postInfo(const QString&);
};

}

#endif // STATUSNOTIFIER_H
