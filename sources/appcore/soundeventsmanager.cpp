#include "soundeventsmanager.h"

namespace ccappcore {

SoundEvent::SoundEvent(const QString& name,
                       const QString& descr,
                       SoundEventsManager* owner )
    : QObject(owner)
    , _name(name)
    , _description(descr)
    , _loops(1)
{

}

void SoundEvent::play()
{
    if(!owner()->soundsEnabled())
        return;

    if(!_sound.isNull())
        qDebug() << tr("play sound: %1").arg(_name);
    _sound = QSharedPointer<QSound>(new QSound(_soundFile));
    _sound->setLoops(_loops);
    _sound->play();
}

void SoundEvent::stop()
{
    if(!_sound.isNull())
        qDebug() << tr("stop sound: %1").arg(_name);
    if(!_sound.isNull())
        _sound->stop();
}

SoundEventsManager::SoundEventsManager(QObject *parent)
    : QObject(parent)
    , _enabled(true)
{

}

bool SoundEventsManager::registerEvent(const QString& name, const QString& descr)
{
    if(_events.contains(name))
    {
        qWarning()<<"SoundEvents::registerEvent(...) "
                <<"- name '"<<name<<"' already registered";
        return false;
    }

    _events[name] = new SoundEvent(name,descr, this);
    return true;
}

bool SoundEventsManager::connectPlayEvent(const QString& name, QObject* sender, const char* signal)
{
    if(!_events.contains(name) || _events[name].isNull())
    {
        qWarning()<<"SoundEvents::connectPlayEvent(...) "
                <<"- event '"<<name<<"' is not registered";
        return false;
    }

    QPointer<SoundEvent> ev = _events[name];
    return connect(sender,signal,ev.data(),SLOT(play()));
}

bool SoundEventsManager::connectStopEvent(const QString& name, QObject* sender, const char* signal)
{
    if(!_events.contains(name) || _events[name].isNull())
    {
        qWarning()<<"SoundEvents::connectStopEvent(...) "
                <<"- event '"<<name<<"' is not registered";
        return false;
    }

    QPointer<SoundEvent> ev = _events[name];
    ev->setLoops(-1); //infinite loops as we have explicitly defined stop signal
    return connect(sender,signal,ev.data(), SLOT(stop()));
}


#define OptionsPrefix QString("notifications/sounds.")

void SoundEventsManager::applySettings(const AppSettings& settings)
{
    setSoundsEnabled( settings.getOption( OptionsPrefix+"enabled" ).toBool() );
    foreach(QPointer<SoundEvent> event, _events )
    {
        QString option = OptionsPrefix + event->name();
        event->setFile( settings.getOption( option ).toString());
    }
}

void SoundEventsManager::flushSettings(AppSettings& settings)
{
    settings.setUserOption( OptionsPrefix+"enabled", soundsEnabled() );
    foreach(QPointer<SoundEvent> event, _events )
    {
        QString option = OptionsPrefix + event->name();
        QString fileName = event->file();
        settings.setUserOption(option, fileName);
    }
}

void SoundEventsManager::playDtmf(const QString& dtmf)
{
    if(!soundsEnabled())
        return;

    QString dtmfCode = dtmf;
    dtmfCode.replace("*","star");
    dtmfCode.replace("#","pound");
    QString wav = "sound/dtmf/dtmf-"+dtmfCode+".wav";
    QSound::play(wav);
}

} // namespace appcore
