#include "layoutmanager.h"
#include <QWidget>
#include <QMainWindow>
#include <QLayout>
#include <QDockWidget>

namespace ccappcore
{

LayoutManager::LayoutManager(QObject *parent)
    : QObject(parent)
    , _lockLayout(false)
{

}

void LayoutManager::saveLayout(LayoutSettings& settings)
{
    settings.setUserOption("ui/lock_layout", _lockLayout);
    foreach(QWidget* w, _watchWidgets)
        saveLayout(w,settings);
}

void LayoutManager::restoreLayout(const LayoutSettings& settings)
{
    _lockLayout = settings.getOption("ui/lock_layout",_lockLayout).toBool();
    foreach(QWidget* w, _watchWidgets)
        restoreLayout(w,settings);
    lockLayout(_lockLayout);
}

void LayoutManager::saveLayout(QWidget* w, LayoutSettings& settings)
{
    QString name = w->objectName().isEmpty()
                   ? w->metaObject()->className() : w->objectName();
    QString path;
    QByteArray data;

    path = "ui/layout."+name.toLower();
    data = w->saveGeometry();

    settings.setUserOption(path,QString(data.toBase64()));

    QMainWindow* mainWindow = qobject_cast<QMainWindow*>(w);
    if(mainWindow)
    {
        path = "ui/state."+name.toLower();
        data = mainWindow->saveState();
        settings.setUserOption(path,QString(data.toBase64()));
    }
}

void LayoutManager::restoreLayout(QWidget* w, const LayoutSettings& settings)
{
    QString name = w->objectName().isEmpty()
                   ? w->metaObject()->className() : w->objectName();
    QString path;
    QByteArray data;

    path = "ui/layout."+name.toLower();
    data = QByteArray::fromBase64(settings.getOption(path).toByteArray());

    if(data.isEmpty())
        return;

    w->restoreGeometry(data);
    QMainWindow* mainWindow = qobject_cast<QMainWindow*>(w);
    if(mainWindow)
    {
        path = "ui/state."+name.toLower();
        data = QByteArray::fromBase64(settings.getOption(path).toByteArray());
        if(!data.isEmpty())
            mainWindow->restoreState(data);

        foreach(QObject* o, mainWindow->children())
        {
            QDockWidget* dw = qobject_cast<QDockWidget*>(o);
            if(dw)
            {
                connect(dw,SIGNAL(dockLocationChanged(Qt::DockWidgetArea)),
                        this, SLOT(updateLastDockedWidgetFeatures()));
                connect(dw,SIGNAL(visibilityChanged(bool)),
                        this, SLOT(updateLastDockedWidgetFeatures()));
            }
        }
    }

    lockLayout(isLayoutLocked());
}

void LayoutManager::addWatchWidget(QWidget *w)
{
    connect( w, SIGNAL(destroyed(QObject*)),
             this, SLOT(widgetDestroyed(QObject*)) );

    w->installEventFilter(this);

    if(!_watchWidgets.contains(w))
    {
        _watchWidgets.append(w);
    }
}

void LayoutManager::widgetDestroyed(QObject* obj)
{
    QWidget* w = qobject_cast<QWidget*>(obj);
    if(!w)
        return;

    if(_watchWidgets.contains(w))
    {
        _watchWidgets.removeOne(w);
    }
}

QList<QDockWidget*> LayoutManager::findVisibleDockedWidgets(QMainWindow* mainWindow)
{
    Q_ASSERT(mainWindow);
    QList<QDockWidget*> result;
    foreach(QObject* o, mainWindow->children())
    {
        QDockWidget* dw = qobject_cast<QDockWidget*>(o);
        if(!dw || dw->isFloating() || !dw->isVisible())
            continue;
        result.append(dw);
    }
    return result;
}

void LayoutManager::updateLastDockedWidgetFeatures()
{
    QDockWidget* dockWidget = qobject_cast<QDockWidget*>(sender());
    Q_ASSERT(dockWidget);
    QMainWindow* mainWindow = qobject_cast<QMainWindow*>(dockWidget->parentWidget());
    updateLastDockedWidgetFeatures(mainWindow);
}

void LayoutManager::lockLayout(bool lock)
{
    _lockLayout = lock;
    foreach(QWidget* w, _watchWidgets)
    {
        QMainWindow* mainWindow = qobject_cast<QMainWindow*>(w);
        if( !mainWindow )
            continue;

        foreach(QObject* o, mainWindow->children())
        {
            QDockWidget* dw = qobject_cast<QDockWidget*>(o);
            if(dw)
                updateFeatures(dw);
        }

        updateLastDockedWidgetFeatures(mainWindow);
    }
}

void LayoutManager::updateLastDockedWidgetFeatures(QMainWindow* mainWindow)
{
    Q_ASSERT(mainWindow);
    QList<QDockWidget*> dockedWidgets = findVisibleDockedWidgets(mainWindow);
    if(dockedWidgets.count()==1)
    {
        dockedWidgets[0]->setFeatures(QDockWidget::NoDockWidgetFeatures);
    }
    else
    {
        foreach(QDockWidget* dw, dockedWidgets)
            updateFeatures(dw);
    }
}

void LayoutManager::updateFeatures(QDockWidget* dw)
{
    QDockWidget::DockWidgetFeatures lockedFeatures = dw->isFloating()
                                                     ? QDockWidget::DockWidgetFloatable | QDockWidget::DockWidgetClosable
                                            : QDockWidget::NoDockWidgetFeatures;
    QDockWidget::DockWidgetFeatures unlockedFeatures =
                                              QDockWidget::DockWidgetFloatable
                                            | QDockWidget::DockWidgetClosable
                                            | QDockWidget::DockWidgetMovable;
    dw->setFeatures(_lockLayout ? lockedFeatures: unlockedFeatures );

    Qt::DockWidgetAreas lockedAreas = dw->isFloating()
                                            ? Qt::NoDockWidgetArea
                                            : Qt::AllDockWidgetAreas;
    Qt::DockWidgetAreas unlockedAreas = Qt::AllDockWidgetAreas;
    dw->setAllowedAreas( _lockLayout ? lockedAreas : unlockedAreas );
}

}
