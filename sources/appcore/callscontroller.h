﻿#ifndef CALLSCONTROLLER_H
#define CALLSCONTROLLER_H

#include "appcore.h"
#include "appsettings.h"
#include "irouterclient.h"
#include "imessageserializer.h"
#include "call.h"
#include "eventlistener.h"
#include "lateboundobject.h"
#include "predicate.h"

namespace ccappcore {

class OperListController;
class SessionController;
class JabberController;

class APPCORE_EXPORT CallsController
    : public QObject
    , IAppSettingsHandler
{
    Q_OBJECT
    Q_INTERFACES(ccappcore::IAppSettingsHandler)
public:

    CallsController(QObject* parent = 0);

    bool isStarted() const;
    void startController();
    void stopController();

    void setSubscribeAllCallEvents(bool subscribe);

    ccappcore::Contact sessionOwner() const;

    QString callAbonPhone(ccappcore::Call) const;
    QString callOperPhone(ccappcore::Call) const;
    QString callShortInfo(ccappcore::Call) const;
    QString callContactId(ccappcore::Call) const;
    QString callInfoString(const ccappcore::CallInfo&) const;
    QString callPriority (ccappcore::Call) const;
    ccappcore::Contact callGroup(ccappcore::Call) const;
    ccappcore::Contact callOper(ccappcore::Call) const;

    //returns last call assigned or connected with session owner
    //see also lastCallChanged Fsignal
    ccappcore::Call lastCall() const { return _lastCall; }
    //ccappcore::Call lastCallPhone() const { return callAbonPhone(_lastCall); }

    Call findByCallId(int callId) const;
    Call findCallByPhone(const QString& phone)const;
    Call findLinkedCall(int callId) const;
    QList<Call> allCalls() const;
    QList<Call> findCallsToConnect(Call callToConnect) const;
    QList<Call> findCallsToConnect(Call callToConnect,const Predicate<Call>& predicate) const;
    QList<Call> findCalls(const Predicate<Call>& predicate) const;

    bool isPrivateGroupCall(ccappcore::Call) const;
    bool isAnotherPrivateGroupCall(ccappcore::Call) const;
    bool isOwnGroupCall(ccappcore::Call) const;
    bool isSessionOwnerCall(ccappcore::Call) const;

    bool isIncomingCall(ccappcore::Call) const;
    bool isOutgoingCall(ccappcore::Call) const;

    int noAnswerTimeoutSeconds() const;// { return 20; }

    void reqQueueCallList();
    QFuture<void> reqCallInfo(ccappcore::Call);
    QFuture<void> reqBindPerson(ccappcore::Call);
    QFuture<Call> reqCallHistory(ccappcore::Call, const QDateTime& from, const QDateTime& to);


    bool canMakeCall(const QString& phone);
    bool canMakeCall(ccappcore::Call recentCall);

    bool canTransferCall(ccappcore::Call) const;
    bool canConnectCalls(ccappcore::Call call1, ccappcore::Call call2);
    bool canJoinCallToConf(ccappcore::Call call);
    bool canUnjoinCallFromConf(ccappcore::Call call);
    bool canRemoveConf(ccappcore::Call call);

    int countCalls()const;

    typedef QMap<QString, QString> ScriptParams;
    //void execAbonentNumbersFromScriptParam(const QString& param, QString& a_num, QString& b_num);
    void extractParamsFromScriptParam(const QString& param, ScriptParams & params);
    //void execCallIDFromScriptParam(const QString& param, QString& callid);

public:
    typedef QMap<int, Call> CallsContainer;
    typedef QList<int> CallIDCollector;
    typedef QMultiMap<int, int> ConferenceCollector; // <conf_id, call_id>

protected: //IAppSettingsHandler
    virtual void applySettings(const AppSettings& settings);
    virtual void flushSettings(AppSettings& settings);

public slots:

    void answerCall(ccappcore::Call call);

    void makeCall(const QString& phone);
    void makeCall(ccappcore::Call recentCall); //recent call

    void holdCall(ccappcore::Call call, bool onOff);
    void muteCall(ccappcore::Call call, bool onOff);

    void recordCall(ccappcore::Call call, bool onOff);
    void endCall(ccappcore::Call call);

    void transferLastCallToPhone(QString scriptParams);
    void transferToPhone(ccappcore::Call, const QString& phone);
    void transferToOperator(ccappcore::Call, ccappcore::Contact op);
    void transferToGroup(ccappcore::Call, ccappcore::Contact group);


    void connectCalls(ccappcore::Call call1, ccappcore::Call call2);
    void connectCalls(int callId1, int callId2);
    void connectCalls(QString scriptParam);
    void transferToIVR(ccappcore::Call, const QString& funcName );

    void addCallInfo(ccappcore::Call, Call::CallInfoType, const QString&);
    void setSubject(ccappcore::Call, const QString&);

    void makeCall(const QUrl& url);

    void createConference(CallIDCollector *callsToConnect);
    void joinCallsToConference(CallIDCollector *callsToConnect, const int& conf_id);
    void joinCallsToConference(const int& conf_id);
    void joinCallToConference(const int& conf_id, const int& call_id);
    void unjoinCallFormConference(const int& conf_id, const int& call_id);
    void removeConference(const int& conf_id);

    void playDTMF(ccappcore::Call, const QString& dtmf);
    //void force_answer(ccappcore::Call c);
    void avtoTransfer();
signals:

    //common events
    void callAdded(ccappcore::Call);
    void callStateChanged(ccappcore::Call);
    void callInfoChanged(ccappcore::Call);
    void callRemoved(ccappcore::Call);

    //emits when new call added into the current operator queue
    void queueCallAdded(ccappcore::Call);
    void queueCallRemoved(ccappcore::Call);
    void privateQueueCallAdded(ccappcore::Call);

    //emits when current oprators begins call processing
    void callAssigned(ccappcore::Call);
    void callStarted(ccappcore::Call);

    //emits when ring starts
    void callOutRingStarted();
    void callInRingStarted();

    //emits when current call is connected, failed or ended
    void callConnected(ccappcore::Call);
    void callFailed(ccappcore::Call, ccappcore::Call::CallResult, const QString& reason);
    void callEnded(ccappcore::Call);
    void joinCallToConfFailed(int callid, int error);

    //emits when current operators drops the phone
    void hung();

    //emits when last connected or assigned call is changed
    //see also lastCall() method
    void lastCallChanged(ccappcore::Call);
    void recordCall(ccappcore::Call call, bool onOff, bool forced);

    //void callJoined(ccappcore::Call call);

private:


    CallsContainer _calls;
    CallIDCollector _joinedCalls;
    ConferenceCollector _conferenceInfo;
    ccappcore::Call _lastCall; //last call assigned or connected with session owner
    LateBoundObject<OperListController> _operListController;
    LateBoundObject<SessionController> _sessionController;
    LateBoundObject<IRouterClient> _router;
    LateBoundObject<IMessageSerializer> _messageSerializer;

    bool _isStarted;
    QMutex sendingLock;
    int _responseTimeoutInSeconds;
    int _noAnswerTimeoutInSeconds;

    void setLastCall(ccappcore::Call);
private:
    EventListener _c2oInitCall;
    EventListener _c2oIncomingCallListener;
    EventListener _c2oQueueCallListener;
    EventListener _c2oRemoveQueueCallListener;
    EventListener _c2oCallAssignedListener;
    EventListener _c2oRingStartedListener;
    EventListener _c2oCallResultListener;
    EventListener _c2oEndCallListener;
    EventListener _c2oHungListener;
    EventListener _c2oHoldListener;
    EventListener _c2oMuteListener;
    EventListener _c2oRecordListener;
    EventListener _c2oCompleteCallListener;
    EventListener _c2oCreateConfListener;
    EventListener _c2oJoinToConfListener;
    EventListener _c2oRemoveFromConfListener;
    EventListener _c2oRemoveConfListener;

    Call::CallType parseCallType(const QString&);

private slots:

    void on_c2oQueueList(const ccappcore::Message&);
public slots:
    void on_c2oQueueCall(const ccappcore::Message&);
private slots:
    void on_c2oRemoveQueueCall(const ccappcore::Message&);

    void on_c2oInitCall(const ccappcore::Message&);
    void on_c2oCallAssigned(const ccappcore::Message&);
    void on_c2oIncomingCall(const ccappcore::Message&);
    void on_c2oCallResult(const ccappcore::Message&);
    void on_c2oEndCall(const ccappcore::Message&);
    //void on_c2oCompleteCall(const ccappcore::Message&);

    void on_c2oRingStarted(const ccappcore::Message&);
    void on_c2oHung(const ccappcore::Message&);

    void on_c2oHold(const ccappcore::Message&);
    void on_c2oMute(const ccappcore::Message&);
    void on_c2oRecord(const ccappcore::Message&);

    void on_c2oCallInfo(const ccappcore::Message&);
    void on_c2oCallHistory(const ccappcore::Message&);
    void on_c2oBindPerson(const ccappcore::Message&);

    void on_c2oCreateConf(const ccappcore::Message&);
    void on_c2oJoinToConf(const ccappcore::Message&);
    void on_c2oRemoveFromConf(const ccappcore::Message&);
    void on_c2oRemoveConf(const ccappcore::Message&);
};

} // namespace ccappcore

#endif // CALLSCONTROLLER_H
