#include "spamlistcontroller.h"
#include "requestreplymessage.h"
#include "callscontroller.h"
#include "operlistcontroller.h"
//#include "messagetrayicon.h"
#include "JabberController.h"

//#include <ole2.h>
//#include <comutil.h>
//#include <oleauto.h>
//#include <qwt_date.h>

namespace ccappcore
{

const int DefaultSpamMessageTimeoutInSeconds = 20;

//typedef qint64 QwtJulianDay;
//static const QwtJulianDay minJulianDayD = Q_INT64_C( -784350574879 );
//static const QwtJulianDay maxJulianDayD = Q_INT64_C( 784354017364 );

//enum
//{
//    JulianDayForEpoch = 2440588
//};

//static QDateTime toDateTime( double value, Qt::TimeSpec timeSpec )
//{
//    const int msecsPerDay = 86400000;

//    const double days = static_cast<qint64>( floor( value / msecsPerDay ) );

//    const double jd = JulianDayForEpoch + days;
//    if ( ( jd > maxJulianDayD ) || ( jd < minJulianDayD ) )
//    {
//        qWarning() << "toDateTime: overflow";
//        return QDateTime();
//    }

//    const QDate d = QDate::fromJulianDay( jd );

//    const int msecs = static_cast<int>( value - days * msecsPerDay );

//    static const QTime timeNull( 0, 0, 0, 0 );

//    QDateTime dt( d, timeNull.addMSecs( msecs ), Qt::UTC );

//    if ( timeSpec == Qt::LocalTime )
//        //dt = qwtToTimeSpec( dt, timeSpec );
//        dt = dt.toTimeSpec( timeSpec );

//    return dt;
//}

//static double toDouble( const QDateTime &dateTime )
//{
//    const int msecsPerDay = 86400000;

//    //const QDateTime dt = qwtToTimeSpec( dateTime, Qt::UTC );
//    const QDateTime dt = dateTime.toTimeSpec( Qt::UTC );

//    const double days = dt.date().toJulianDay() - JulianDayForEpoch;

//    const QTime time = dt.time();
//    const double secs = 3600.0 * time.hour() +
//        60.0 * time.minute() + time.second();

//    return days * msecsPerDay + time.msec() + 1000.0 * secs;
//}

static double toDouble(const QDateTime &dateTime)
{
    QDateTime dt(QDate(1900, 1, 1),  QTime(0,0,0));
    const double days = dateTime.date().toJulianDay() - dt.date().toJulianDay() + 1 + 1;
    const double time = ( static_cast<double>(dateTime.time().hour())*3600
                          + static_cast<double>(dateTime.time().minute())*60
                          + static_cast<double>(dateTime.time().second()));
    return days+time/(24*60*60);
}

static QDateTime toDateTime( double value )
{
    QDateTime dt(QDate(1900, 1, 1),  QTime(0,0,0));
    qreal days, time; // 41122
    time = modf(value, &days);

    // There is an error in QT: QDate() desides that 1900 is not leap-year,
    //  but it's not true actionaly
    //  that's why minus 1 day in colculation
    // Secondly, database returns +1 day by unknown reason
    //  that's why we have another minus ;)
    dt = dt.addDays(days - 1 - 1);
    dt = dt.addSecs(time*24*60*60);
    dt = dt.toLocalTime();

    return dt;
}

//QDateTime mDdate2Qdtr( DATE dt)
//{
//    COleDateTime odt;
//    SYSTEMTIME st;
//    odt = dt;
//    odt.GetAsSystemTime(st);
//    QDateTime qdt (QDate(st.wYear, st.wMonth,st.wDay ),QTime( st.wHour, st.wMinute,st.wSecond ) );
//    return qdt;
//}

int makeVariantTimeToDosDateTime()
{

}

//const QString s_messageTemplateSystem (QObject::tr("<font size=\"4\" color=\"black\"  face=\"Arial\">%1 (%2): </font> "
//                                               "<font size=\"4\" color=\"black\" face=\"Arial\">%3</font><p>"));

//const QString s_messageTemplateIn (QObject::tr("<font size=\"4\" color=\"red\"  face=\"Arial\">%1 (%2): </font> "
//                                               "<font size=\"4\" color=\"black\" face=\"Arial\">%3</font><p>"));
//const QString s_messageTemplateOut(QObject::tr("<font size=\"4\" color=\"blue\" face=\"Arial\">%1 (%2): </font> "
//                                               "<font size=\"4\" color=\"black\" face=\"Arial\">%3</font><p>"));

//const QString s_messageTemplatePrivateIn (QObject::tr("<font size=\"4\" color=\"red\"  face=\"Arial\">%1 (%2) Private message: </font> "
//                                               "<font size=\"4\" color=\"black\" face=\"Arial\">%3</font><p>"));
//const QString s_messageTemplatePrivateOut(QObject::tr("<font size=\"4\" color=\"blue\" face=\"Arial\">%1 (%2) Private message: </font> "
//                                               "<font size=\"4\" color=\"black\" face=\"Arial\">%3</font><p>"));


QString SpamMessage::shortInfo(const SpamMessage& m)
{
    LateBoundObject<OperListController> operListController;

    ContactId personId = operListController->createOperId(m.operFromId());
    Contact   whoAdded = operListController->findById(personId);

    if (!whoAdded.isValid())
    {
        LateBoundObject<JabberController> jabberController;

        RosterId rosterId = jabberController->createRosterId(m.operFromId());
        whoAdded = jabberController->findById(rosterId);
    }
    return QString("%1: %2").arg(whoAdded.name()).arg(m.text());
}

//void SpamMessage::activateIcon()
//{
//    _trayIcon = new MessageTrayIcon();
//    //connect(qApp, SIGNAL(lastWindowClosed()), trayIcon, SLOT(deleteLater()));

//}

SpamListController::SpamListController(QObject *parent) :
    QObject(parent), _operIdWho(-1), _spamMessageTimeout(DefaultSpamMessageTimeoutInSeconds)
{
    connect(&_loadSpamFuture   , SIGNAL(finished()), this, SLOT(on_reloadSpamFinished()));
    connect(&_loadHistoryFuture, SIGNAL(finished()), this, SLOT(on_loadHistoryFinished()));
}

qint32 SpamListController::findMessageById(const qint32& id)const
{
    for(qint32 i=0;i<_messageList.size();++i)
    {
        if (id == _messageList[i].msgId())
            return i;
    }
    return _messageList.size(); // out of range
}


bool SpamListController::isStarted() const
{
    return _isStarted;
}

void SpamListController::startController()
{
    stopController();

    _c2oBBListListener.setSubscribeRequired(true);
    _c2oBBListListener.startListening(
            &_router, Message("C2O_NEW_REQUEST_2"), this, SLOT(on_c2oNeed_o2c_BBListReq(ccappcore::Message)));

    //_c2oSendMsgListener.setSubscribeRequired(true);
    _c2oSendMsgListener.startListening(
            &_router, Message("C2O_SEND_MSG"), this, SLOT(on_c2oSendMsg(ccappcore::Message)));


//    _c2oSendMsgListener2.setSubscribeRequired(true);
//    _c2oSendMsgListener2.startListening(
//            &_router, Message("C2O_SEND_MSG"), this, SLOT(on_c2oSendMsg2(ccappcore::Message)));


    //reqQueueCallList();

    _isStarted = true;
}

void SpamListController::stopController()
{
    if(!_isStarted)
        return;

    _c2oBBListListener.stopListening();
    _c2oSendMsgListener.stopListening();

    _messageList.clear();
    _isStarted = false;
}

void SpamListController::on_c2oBBList(const Message &m)
{
    _loadSpamResult.reportStarted();

    if( 0==(quint32)m["sql_result"] )
    {
        QByteArray listData = m["data"];
        QList<Message> list = _messageSerializer->loadList(listData);
        foreach(const Message& m, list)
        {
            qDebug()<<m;
            qint32     groupId = m[ "group_id" ];
            qint32     msgId   = m[ "msg_id"   ];
            QDateTime  end     = m[ "end"      ];
            qint32     operId  = m[ "oper_id"  ];
            QDateTime  begin   = m[ "begin"    ];
            QString    text    = m[ "text"     ];


            SpamMessage message (groupId,msgId,end,operId,TO_ALL,begin,text,MT_SYSTEM);
            //_messageList.push_back(message);
            _messageList.push_front(message);
        }
    }
    if (_messageList.size() > 0)
    {
        _loadSpamResult.reportResults(_messageList.toVector(), 0, _messageList.count() );

    }
    _loadSpamResult.reportFinished();
}

QFuture < SpamMessage > SpamListController::loadSpamListAsync()
{
    _loadSpamResult = QFutureInterface<SpamMessage>( (QFutureInterfaceBase::State)
            (QFutureInterfaceBase::Started | QFutureInterfaceBase::Running)
            );

    _messageList.clear();

    Message request, replyPrototype;
    request = Message("O2C_BB_LIST_REQ");
    request["oper_id"] = _callsController->sessionOwner().contactId().toInt();
    replyPrototype = Message("C2O_BB_LIST");
    RequestReplyMessage::send(
        request, replyPrototype, this, SLOT(on_c2oBBList(ccappcore::Message)));

    return _loadSpamResult.future();
}

QFuture < SpamMessage > SpamListController::loadHistoryListAsync(const int& idWho, const QDateTime& from, const QDateTime& to)
{
    _loadHistoryResult = QFutureInterface<SpamMessage>( (QFutureInterfaceBase::State)
            (QFutureInterfaceBase::Started | QFutureInterfaceBase::Running)
            );

    _historyList.clear();
    _operIdWho = idWho;

    Message request, replyPrototype;
    request = Message("O2C_MSG_HISTORY_REQ");
    request[ "oper_id" ] = _callsController->sessionOwner().contactId().toInt();
    request[ "who"     ] = idWho;
    request[ "begin"   ] = toDouble(from);
    request[ "end"     ] = toDouble(to);
    replyPrototype = Message("C2O_MSG_HISTORY");
    RequestReplyMessage::send(
        request, replyPrototype, this, SLOT(on_c2oMSGHistory(ccappcore::Message)));

    return _loadHistoryResult.future();
}


void SpamListController::on_c2oNeed_o2c_BBListReq(const Message &m)
{
    _messageListOld = _messageList;
    _messageList.clear();
    _loadSpamFuture.setFuture(loadSpamListAsync());
}

void SpamListController::loadHistory(const int &idWho, const QDateTime &from, const QDateTime &to)
{
    _loadHistoryFuture.setFuture(loadHistoryListAsync(idWho, from, to));
}

//SpamMessage SpamListController::addChatMsg(const Message &m, const int& type)
//{
//    qDebug()<<m;
//    qint32     groupId = -1;
//    qint32     msgId   = -1;
//    qint32     toId    = m[ "to_id"  ];
//    qint32     fromId  = m[ "from_id"  ];
//    QString    text    = m[ "text"     ];

//    qint32 operId = (type == MT_IN)?fromId:_operListController->sessionOwner().contactId().toInt();

//    SpamMessage message (groupId,msgId,QDateTime::currentDateTime(),operId,toId,QDateTime::currentDateTime(),text,type);

//    _chats[type == MT_IN?fromId:toId].push_back(message);


//    return message;
//}

void SpamListController::on_c2oSendMsg(const Message &m)
{
    if( 0==(quint32)m["sql_result"] )
    {

        qDebug()<<m;
        qint32     groupId = 0;
        qint32     msgId   = 0;
        qint32     toId    = m[ "to_id"  ];
        qint32     fromId  = m[ "from_id"  ];
        QString    text    = m[ "text"     ];

        SpamMessage message (groupId,msgId,QDateTime::currentDateTime(),fromId,toId,QDateTime::currentDateTime(),text, MT_IN);

        _chats_in[fromId].push_back(message);

        emit chatMsgAdded(message);
        emit updateContactStatus(fromId);
    }

}

void SpamListController::on_c2oSendMsg2(const Message &m)
{
    if( 0==(quint32)m["sql_result"] )
    {
        qDebug()<<m;
        qint32     groupId = 0;
        qint32     msgId   = 0;
        qint32     toId    = m[ "to_id"  ];
        qint32     fromId  = m[ "from_id"  ];
        QString    text    = m[ "text"     ];

        SpamMessage message (groupId,msgId,QDateTime::currentDateTime(),fromId,toId,QDateTime::currentDateTime(),text, MT_CHAT);

        _chat[fromId].push_back(message);

        emit chatMsgAdded2();
    }

}


QString SpamListController::statusColor(const bool& isRead) const
{
    if (!isRead)
        return QString(tr("red"));

    return QString(tr("black"));
}

void SpamListController::on_reloadSpamFinished()
{
    for(qint32 i=0;i<_messageListOld.size();++i)
    {
        qint32 row = findMessageById(_messageListOld[i].msgId());
        if (row!=_messageList.size())
            _messageList[row].setRead(_messageListOld[i].isRead());
    }

    _messageListOld.clear();

    for(qint32 i=0;i<_messageList.size();++i)
    {
        if (!_messageList[i].isRead())
        {
            emit newMessage(_messageList[i]);
            break;
        }
    }

    emit messageAdded();
}

void SpamListController::on_loadHistoryFinished()
{
    if (!_historyList.count())
        return;

    SpamContainer in_chat, out_chat;
    foreach (SpamMessage m, _historyList)
    {
        if (_operIdWho == m.operFromId())
            out_chat.push_back(m);
        else
            in_chat.push_back(m);
    }

    //if (in_chat.size())
        _chats_in[_operIdWho] = in_chat;
    //if (out_chat.size())
        _chats_out[_operIdWho] = out_chat;

    _historyList.clear();
    _operIdWho = -1;
//    SpamMessage &m = _historyList.first();
//    int operID = m.operFromId();
//    ChatMap cur_chat = (operID != _callsController->sessionOwner().contactId().toInt())?_chats_in:_chats_out;

//    cur_chat.remove(operID);
//    cur_chat[operID] = _historyList;

    emit loadHistoryCompleted();

}

void SpamListController::sendMsg(const int& toId, const QString& text)
{
    int fromId = _callsController->sessionOwner().contactId().toInt();

    Message m("O2C_SEND_MSG_REQ");
    m[ "to_id"   ] = toId;
    m[ "from_id" ] = fromId;
    m[ "text"    ] = text;
    m[ "type"    ] = int(1);
    _router->send(m);

    SpamMessage message (0,0,QDateTime::currentDateTime(),fromId,toId,QDateTime::currentDateTime(),text, MT_OUT);

    _chats_out[toId].push_back(message);

    emit chatMsgAdded(message);
    emit updateContactStatus(toId);
}

void SpamListController::sendMsg2(const int& operID, const QString& text)
{
    Message m("O2C_SEND_MSG_REQ");
    m[ "to_id" ] = operID;
    m[ "text"  ] = text;
    m[ "type"  ] = int(1);
    _router->send(m);
}

void SpamListController::rosterIN(const int& operID, /*const QString &jid,*/ const QString &text)
{
    qint32     groupId = 0;
    qint32     msgId   = 0;

    int toId = _callsController->sessionOwner().contactId().toInt();
    SpamMessage message (groupId,msgId,QDateTime::currentDateTime(),operID,toId,QDateTime::currentDateTime(),text, MT_IN);

    _chats_in[operID].push_back(message);

    emit chatMsgAdded(message);
    emit updateContactStatus(operID);
}

void SpamListController::rosterOUT(const int& toId, const QString &jid, const QString &text)
{
    _jabberController->messageSend(jid,text);

    int fromId = _callsController->sessionOwner().contactId().toInt();
    SpamMessage message (0,0,QDateTime::currentDateTime(),fromId,toId,QDateTime::currentDateTime(),text, MT_OUT);

    _chats_out[toId].push_back(message);

    emit chatMsgAdded(message);
    //emit updateContactStatus(toId);
}

//QString SpamListController::printMessages(int contactId, int count)
//{
//    QString doc;
//    SpamContainer &listMsg = _chats[contactId];
//    quint8 i = 0; // count == -1 -> all messages
//    for (SpamContainer::iterator it = listMsg.begin();it!=listMsg.end()&&i<count;++it,++i)
//    {
//        //SpamMessage * msg = (SpamMessage *)(*it);
//        it->setRead(true);

//        QString msgTemplate = (it->msgType()==MT_IN)?s_messageTemplateIn:s_messageTemplateOut;
//        msgTemplate = msgTemplate.arg(getContactName(it->operFromId()))
//                                 .arg(it->begin().toString( "HH:mm:ss dd/MM/yy"))
//                                 .arg(it->text());

//        doc+=msgTemplate;
//    }
//    emit updateContactStatus(contactId);
//    return doc;

//}

//QString SpamListController::printChat(int count)
//{
//    QString doc;
//    qint32 ownerId = _operListController->sessionOwner().contactId().toInt();

//    // collect messages
//    SpamContainer listMsg;
//    for (ChatMap::const_iterator cit = _chat.begin();cit!=_chat.end();++cit)
//    {
//        listMsg+=cit.value();
//    }

//    // sort by date
//    qSort(listMsg.begin(),listMsg.end(),MessageSortByDatePredicate());

//    // and print they now
//    quint8 i = 0; // count == -1 -> all messages
//    for (SpamContainer::iterator it = listMsg.begin();it!=listMsg.end()&&i<count;++it,++i)
//    {
//        it->setRead(true);

//        QString msgTemplate = s_messageTemplateSystem; // default

//        if(it->operToId() == ownerId)
//        {
//            if (it->operFromId() == ownerId)
//                msgTemplate = s_messageTemplatePrivateOut;
//            else
//                msgTemplate = s_messageTemplatePrivateIn;
//        }
//        else
//        {
//            if (it->operFromId() == ownerId)
//                msgTemplate = s_messageTemplateOut;
//            else
//                msgTemplate = s_messageTemplateIn;
//        }

//        msgTemplate = msgTemplate.arg(getContactName(it->operFromId()))
//                                 .arg(it->begin().toString( "HH:mm:ss dd/MM/yy"))
//                                 .arg(it->text());

//        doc+=msgTemplate;
//    }

//    //emit updateContactStatus(contactId);
//    return doc;

//}

QString SpamListController::getContactName(const int &contactId) const
{
    ContactId personId = _operListController->createOperId(contactId);
    Contact person = _operListController->findById(personId);
    if (person.isValid())
        return person.name();

    ContactId rosterId = _jabberController->createRosterId(contactId);
    person = _jabberController->findById(rosterId);
    return person.name();

}

bool SpamListController::isUnreadChatMessage(const int &contactId)const
{
    SpamContainer listMsg = _chats_in[contactId];
    for (SpamContainer::const_iterator cit = listMsg.begin();cit!=listMsg.end();++cit)
    {
        if (!cit->isRead() && cit->msgType() == MT_IN)
            return true;
    }
    return false;
}

void SpamListController::showBalloon(SpamMessage m)
{
    emit newMessage(m);
    //m.activateIcon();
}

void SpamListController::on_c2oMSGHistory(const ccappcore::Message &m)
{
    _loadHistoryResult.reportStarted();

    qint32 sessionOwnerId = _callsController->sessionOwner().contactId().toInt();
    if( 0==(quint32)m["sql_result"] )
    {
        QByteArray listData = m["data"];
        QList<Message> list = _messageSerializer->loadList(listData);
        foreach(const Message& m, list)
        {
            qDebug()<<m;
            qint32     groupId = 0;//m[ "group_id" ];
            qint32     msgId   = m[ "id"   ];
            QDateTime  end     = m[ "time" ];
            QDateTime  begin   = end;
            qint32     operId  = m[ "from" ];
            QString    text    = m[ "text" ];

            //qint32 operIdWho = (sessionOwnerId == operId)?_operIdWho:sessionOwnerId;
            qint32 operIdWho = _operIdWho;
            qint32 msgType = MT_OUT;
            if (sessionOwnerId != operId)
            {
                operIdWho = sessionOwnerId;
                msgType   = MT_IN;
            }


            SpamMessage message (groupId,msgId,end,operId,operIdWho,begin,text,msgType);
            _historyList.push_back(message);
        }
    }
    if (_messageList.size() > 0)
    {
        _loadHistoryResult.reportResults(_historyList.toVector(), 0, _historyList.count() );

    }
    _loadHistoryResult.reportFinished();

}

void SpamListController::setAllMessagesRead(const int &contactId)
{
    SpamContainer &listMsg = _chats_in[contactId];
    for (SpamContainer::iterator it = listMsg.begin();it!=listMsg.end();++it)
    {
        it->setRead(true);
    }

}

void SpamListController::setSelected(const int &index)
{
    _messageList[index].setRead(true);

}

void SpamListController::applySettings(const AppSettings& settings)
{
    _spamMessageTimeout = settings.getOption("messages/spamMessageTimeout").toInt();
    if (0 == _spamMessageTimeout)
        _spamMessageTimeout = DefaultSpamMessageTimeoutInSeconds;
}

void SpamListController::flushSettings(AppSettings& settings)
{
    settings.setAppOption( "messages/spamMessageTimeout", _spamMessageTimeout );
}

} // namespace ccappcore
