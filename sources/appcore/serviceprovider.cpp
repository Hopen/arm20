#include "serviceprovider.h"
//#include <QThreadStorage>

namespace ccappcore
{    
    ServiceProvider::ServiceProvider(QObject* parent )
        : QObject(parent)
    {
    }

    void ServiceProvider::serviceDestroyed(QObject* obj)
    {
        _services.removeOne(obj);
    }

    ServiceProvider& ServiceProvider::instance()
    {
        static ServiceProvider staticInstance;
        //QThreadStorage<ServiceProvider*> threadStaticInstance;
        //if(!threadStaticInstance.hasLocalData())
            return staticInstance;
        
       //return *threadStaticInstance.localData();
    }
}
