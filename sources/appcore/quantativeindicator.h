#ifndef QUANTATIVEINDICATOR_H
#define QUANTATIVEINDICATOR_H

#include "appcore.h"
#include <set>

namespace ccappcore
{

class APPCORE_EXPORT QuantativeIndicator: public QObject
{
    Q_OBJECT;
    Q_ENUMS(Severity Trend Format)
    Q_PROPERTY(QString formattedValue READ toString NOTIFY formattedValueChanged)
    Q_PROPERTY(int value READ value NOTIFY valueChanged)
    Q_PROPERTY(int minValue READ minValue NOTIFY minChanged)
    Q_PROPERTY(int maxValue READ maxValue NOTIFY maxChanged)
    Q_PROPERTY(int criticalValue READ criticalValue NOTIFY criticalValueChanged)
    Q_PROPERTY(int warningValue READ warningValue NOTIFY warningValueChanged)
    Q_PROPERTY(bool isInverted READ isInverted NOTIFY isInvertedChanged)
    Q_PROPERTY(ccappcore::QuantativeIndicator::Severity severity READ severity NOTIFY severityChanged)
    Q_PROPERTY(ccappcore::QuantativeIndicator::Format format READ format NOTIFY formatChanged)
    Q_PROPERTY(ccappcore::QuantativeIndicator::Trend trend READ trend NOTIFY trendChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
public:

    enum Severity
    {
        UndefinedSeverity    = 0,
        CriticallyLowValue   = 1,
        BelowNormalValue     = 2,
        NormalValue          = 3,
        AboveNormalValue     = 4,
        CriticallyHighValue  = 5
    };

    typedef std::map<int,Severity> ThresholdSet;

    enum Trend
    {
        UndefinedTrend,
        PositiveTrend,
        NegativeTrend
    };

    enum Format
    {
        Format_Value,
        Format_Percents,
        Format_ShortTime //value is tim in seconds
    };

    explicit QuantativeIndicator(QObject* parent = 0);

    void setValue(QDateTime valueTime, int value);
    void increment() { setValue( QDateTime::currentDateTime(), value() + 1 ); }

    const QList<Format>& allowedFormats() const          { return _allowedFormats;    }
    void setAllowedFormats(const QList<Format>& formats) { _allowedFormats = formats; }

    Format format() const                { return _format; }
    void setFormat(Format f);
    QString toString() const;

    int value() const           { return _value;    }
    Severity severity() const   { return _severity; }
    Trend trend() const         { return _trend;    }

    int minValue() const        { return _min;    }
    void setMinValue(int min);

    int maxValue() const        { return _max;    }
    void setMaxValue(int max);

    int warningValue() const    { return _warningValue;  }
    int criticalValue() const   { return _criticalValue; }
    void setWarningValue(int);
    void setCriticalValue(int);

    const std::map<QDateTime, int>& history() const { return _history; }
    void shrinkHistory(QDateTime dateFrom, QDateTime dateTo);
    void shrinkHistory(int maxItems);

    void clear();

    const QString& name() const        { return _name;  }
    void setName(const QString& name ) { _name = name;  }

    bool isInverted() const            { return _inverted; }
    void setInverted(bool);
signals:
    void valueChanged(int newValue);
    void formattedValueChanged(const QString& newValue);
    void maxChanged(int newMax);
    void minChanged(int newMin);
    void trendChanged(ccappcore::QuantativeIndicator::Trend);
    void severityChanged(ccappcore::QuantativeIndicator::Severity);
    void formatChanged(ccappcore::QuantativeIndicator::Format);
    void nameChanged(QString);
    void criticalValueChanged(int);
    void warningValueChanged(int);
    void isInvertedChanged(bool);
private:

    int _value;
    int _min;
    int _max;
    int _warningValue;
    int _criticalValue;
    bool _inverted;

    Format          _format;
    QList<Format>   _allowedFormats;
    QString         _name;

    Trend           _trend;
    Severity        _severity;

    std::map<QDateTime, int>    _history;

    void updateTrend();
    void updateSeverity();
};

}
Q_DECLARE_METATYPE(QPointer<ccappcore::QuantativeIndicator>);


#endif // QUANTATIVEINDICATOR_H
