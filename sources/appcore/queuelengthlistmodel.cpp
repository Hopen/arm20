#include "queuelengthlistmodel.h"

namespace ccappcore {

QueueLengthListModel::QueueLengthListModel(QObject *parent)
    : FilteredModel<ccappcore::Group, QAbstractListModel>(parent)
{

    connect(_callsController.instance(),SIGNAL(callStateChanged(ccappcore::Call)),
            this, SLOT(notifyQueueLengthChanged()));

    applyFilter();
}

void QueueLengthListModel::applyFilter()
{
    _groups = _operListController->find(filter());
}

int QueueLengthListModel::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : 1;
}

int QueueLengthListModel::columnCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : _groups.count();
}

QVariant QueueLengthListModel::data(const QModelIndex &index,
                      int role ) const
{
    if(index.parent().isValid())
        return QVariant();

    if(index.row()!=0)
        return QVariant();

    if(index.column()<0||index.column()>=_groups.count())
        return QVariant();

    if(role== Qt::DisplayRole)
        return _groups[index.column()].queueLength();

    return QVariant();
}

QVariant QueueLengthListModel::headerData(int section, Qt::Orientation orientation,
                            int role ) const
{
    if(orientation!=Qt::Horizontal)
        return QVariant();

    if(section<0||section>=_groups.count())
        return QVariant();

    if(role== Qt::DisplayRole)
        return _groups[section].name();

    return QVariant();
}

void QueueLengthListModel::notifyQueueLengthChanged()
{
    QModelIndex topLeft = index(0,0,QModelIndex());
    QModelIndex bottomRight = index(0,columnCount(QModelIndex())-1,QModelIndex());
    emit dataChanged(topLeft,bottomRight);
}


} // namespace ccappcore
