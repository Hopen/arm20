<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/apploader.cpp" line="44"/>
        <source>Ошибка загрузки приложения</source>
        <translation>Error application loading</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/apploader.cpp" line="45"/>
        <source>Не удалось загрузить плагин: 
%1, 
ошибка: %2</source>
        <translation>Could not load plugin:
%1,
Error:%2</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/call.cpp" line="61"/>
        <source>В IVR</source>
        <translation>In IVR</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/call.cpp" line="63"/>
        <source>В очереди</source>
        <translation>In queue</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/call.cpp" line="65"/>
        <location filename="../../Projects/new/trunk/sources/appcore/call.cpp" line="68"/>
        <source>Распределен</source>
        <translation>Distribute</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/call.cpp" line="70"/>
        <source>Соединен</source>
        <translation>Connected</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/call.cpp" line="72"/>
        <location filename="../../Projects/new/trunk/sources/appcore/call.cpp" line="74"/>
        <source>На паузе</source>
        <translation>On pause</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/call.cpp" line="76"/>
        <source>Ошибка дозвона</source>
        <translation>Error calling</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/call.cpp" line="78"/>
        <location filename="../../Projects/new/trunk/sources/appcore/callscontroller.cpp" line="1390"/>
        <source>Завершен</source>
        <translation>Done</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/call.cpp" line="80"/>
        <source>Конференция</source>
        <translation>Conference</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/call.cpp" line="82"/>
        <location filename="../../Projects/new/trunk/sources/appcore/call.cpp" line="112"/>
        <source>Неизвестно</source>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/call.cpp" line="91"/>
        <source>Номер занят</source>
        <translation>Busy</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/call.cpp" line="93"/>
        <source>Номер не существует</source>
        <translation>Number does not exist</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/call.cpp" line="95"/>
        <source>Номер не отвечает</source>
        <translation>Not answered</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/call.cpp" line="97"/>
        <source>Ошибка дозвона (%1)</source>
        <translation>Error dial (%1)</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/call.cpp" line="106"/>
        <source>Входящий</source>
        <translation>Incoming</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/call.cpp" line="108"/>
        <source>Исходящий</source>
        <translation>Outcoming</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/call.cpp" line="110"/>
        <source>Оператор</source>
        <translation>Operator</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/callscontroller.cpp" line="1373"/>
        <source>Соединен с оператором </source>
        <translation>Connected to operator</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/callscontroller.cpp" line="1379"/>
        <source>Распределен на оператора </source>
        <translation>Distribute to operator</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/callscontroller.cpp" line="1385"/>
        <source>Поставлен в очередь </source>
        <translation>Queued</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/callscontroller.cpp" line="1394"/>
        <source>Вход в IVR меню </source>
        <translation>Enter to IVR</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/callscontroller.cpp" line="1405"/>
        <source>Включена запись</source>
        <translation>Recording</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/redirectlistcontroller.h" line="16"/>
        <source>sys_call2num</source>
        <translation>sys_call2num</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/redirectlistcontroller.h" line="17"/>
        <source>sys_call2vm</source>
        <translation>sys_call2vm</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/redirectlistcontroller.h" line="29"/>
        <source>OFF</source>
        <translation>OFF</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/timeutils.cpp" line="22"/>
        <source>%1 ч. %2 мин.</source>
        <translation>%1 h. %2 min.</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/timeutils.cpp" line="27"/>
        <source>%1 мин.</source>
        <translation>%1 min.</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/timeutils.cpp" line="42"/>
        <source>менее минуты</source>
        <translation>less minute</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/timeutils.cpp" line="45"/>
        <source>1 минута</source>
        <translation>1 minute</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/timeutils.cpp" line="48"/>
        <location filename="../../Projects/new/trunk/sources/appcore/timeutils.cpp" line="60"/>
        <source>%1 минуты</source>
        <translation>%1 min</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/timeutils.cpp" line="51"/>
        <location filename="../../Projects/new/trunk/sources/appcore/timeutils.cpp" line="54"/>
        <location filename="../../Projects/new/trunk/sources/appcore/timeutils.cpp" line="62"/>
        <source>%1 минут</source>
        <translation>%1 min</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/timeutils.cpp" line="57"/>
        <source>%1 минута</source>
        <translation>%1 min</translation>
    </message>
</context>
<context>
    <name>QObject::QObject</name>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/redirectlistcontroller.h" line="51"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
</context>
<context>
    <name>ScriptAdapter</name>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/scriptadapter.cpp" line="55"/>
        <source>Run script: &quot;%1 %2&quot;</source>
        <translation>Run script: &quot;%1 %2&quot;</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/scriptadapter.cpp" line="60"/>
        <source>Run script error: file &quot;%1&quot; does not exist</source>
        <translation>Run script error: file &quot;%1&quot; does not exist</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/scriptadapter.cpp" line="76"/>
        <source>Run script error: %1</source>
        <translation>Run script error: %1</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/scriptadapter.cpp" line="82"/>
        <source>Script &quot;%1&quot; has successfully completed</source>
        <translation>Script &quot;%1&quot; has successfully completed</translation>
    </message>
</context>
<context>
    <name>ccappcore::CallsController</name>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/callscontroller.cpp" line="529"/>
        <location filename="../../Projects/new/trunk/sources/appcore/callscontroller.cpp" line="539"/>
        <source> - Входящий</source>
        <translation> - Incoming</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/callscontroller.cpp" line="533"/>
        <location filename="../../Projects/new/trunk/sources/appcore/callscontroller.cpp" line="540"/>
        <source> - Исходящий</source>
        <translation> - Outcoming</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/callscontroller.cpp" line="1430"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
</context>
<context>
    <name>ccappcore::CallsListModel</name>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/callslistmodel.cpp" line="123"/>
        <source>Номер абонента</source>
        <translation>Abonent number</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/callslistmodel.cpp" line="125"/>
        <source>Состояние</source>
        <translation>State</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/callslistmodel.cpp" line="127"/>
        <source>Группа</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/callslistmodel.cpp" line="129"/>
        <source>Тип</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/callslistmodel.cpp" line="131"/>
        <source>Время начала</source>
        <translation>Begin time</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/callslistmodel.cpp" line="133"/>
        <source>Время состояния</source>
        <translation>Begin state</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/callslistmodel.cpp" line="135"/>
        <source>Приоритет</source>
        <translation>Priority</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/callslistmodel.cpp" line="137"/>
        <source>Доп. информация</source>
        <translation>Additional info</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/callslistmodel.cpp" line="139"/>
        <source>Последний оператор</source>
        <translation>Last operator</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/callslistmodel.cpp" line="141"/>
        <source>Длительность звонка</source>
        <translation>Duration</translation>
    </message>
</context>
<context>
    <name>ccappcore::ContactsController</name>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/contactscontroller.cpp" line="135"/>
        <source>Имя</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/contactscontroller.cpp" line="137"/>
        <source>Полное имя</source>
        <translation>Full name</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/contactscontroller.cpp" line="139"/>
        <source>Статус</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/contactscontroller.cpp" line="141"/>
        <source>Время изменения статуса</source>
        <translation>Time Feed</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/contactscontroller.cpp" line="143"/>
        <source>Доп. статус</source>
        <translation>Extra status</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/contactscontroller.cpp" line="145"/>
        <source>E-mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/contactscontroller.cpp" line="147"/>
        <source>Телефон</source>
        <translation>Phone</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/contactscontroller.cpp" line="149"/>
        <source>Раб. телефон</source>
        <translation>Work phone</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/contactscontroller.cpp" line="151"/>
        <source>Моб. телефон</source>
        <translation>Mobile phone</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/contactscontroller.cpp" line="153"/>
        <source>Факс</source>
        <translation>Fax</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/contactscontroller.cpp" line="155"/>
        <source>Адрес</source>
        <translation>Address</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/contactscontroller.cpp" line="157"/>
        <source>Организация</source>
        <translation>Organization</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/contactscontroller.cpp" line="159"/>
        <source>Должность</source>
        <translation>Position</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/contactscontroller.cpp" line="161"/>
        <source>Регион</source>
        <translation>Region</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/contactscontroller.cpp" line="165"/>
        <source>Доп. информация</source>
        <translation>Extra info</translation>
    </message>
</context>
<context>
    <name>ccappcore::GlobalAddressBookController</name>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/globaladdressbookcontroller.cpp" line="279"/>
        <source>По всем</source>
        <translation>For all</translation>
    </message>
</context>
<context>
    <name>ccappcore::OperListController</name>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/operlistcontroller.cpp" line="128"/>
        <source>Oper &quot;%1&quot; new status: %2</source>
        <translation>Oper &quot;%1&quot; new status: %2</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/operlistcontroller.cpp" line="401"/>
        <source>Имя группы</source>
        <translation>Group name</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/operlistcontroller.cpp" line="403"/>
        <source>Свободно</source>
        <translation>Free</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/operlistcontroller.cpp" line="405"/>
        <location filename="../../Projects/new/trunk/sources/appcore/operlistcontroller.cpp" line="442"/>
        <source>Оффлайн</source>
        <translation>Offline</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/operlistcontroller.cpp" line="407"/>
        <location filename="../../Projects/new/trunk/sources/appcore/operlistcontroller.cpp" line="440"/>
        <source>На паузе</source>
        <translation>On pause</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/operlistcontroller.cpp" line="409"/>
        <location filename="../../Projects/new/trunk/sources/appcore/operlistcontroller.cpp" line="438"/>
        <source>Занято</source>
        <translation>Busy</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/operlistcontroller.cpp" line="436"/>
        <source>Онлайн</source>
        <translation>Online</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/operlistcontroller.cpp" line="444"/>
        <source>Неизвестно</source>
        <translation>Unknown</translation>
    </message>
</context>
<context>
    <name>ccappcore::PresetWidget</name>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/presetwidget.cpp" line="73"/>
        <source>PresetWidget::destroyWidget()</source>
        <translation>PresetWidget::destroyWidget()</translation>
    </message>
</context>
<context>
    <name>ccappcore::RedirectListController</name>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/redirectlistcontroller.cpp" line="104"/>
        <location filename="../../Projects/new/trunk/sources/appcore/redirectlistcontroller.cpp" line="112"/>
        <source>OFF</source>
        <translation>OFF</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/redirectlistcontroller.cpp" line="107"/>
        <source>ON</source>
        <translation>ON</translation>
    </message>
</context>
<context>
    <name>ccappcore::RequestReplyMessage</name>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/requestreplymessage.cpp" line="139"/>
        <source>Ошибка доставки сообщения. Убедитесь в правильности настроек соединения.</source>
        <translation>Error message delivery. Make sure that the connection settings.</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/requestreplymessage.cpp" line="149"/>
        <source>Истекло время ожидания ответа от сервера.</source>
        <translation>Timeout response from the server.</translation>
    </message>
</context>
<context>
    <name>ccappcore::SessionController</name>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/sessioncontroller.cpp" line="152"/>
        <source>RU</source>
        <translation>RU</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/sessioncontroller.cpp" line="202"/>
        <source>Error: cannot set ForegroundLockTimeout param: %1</source>
        <translation>Error: cannot set ForegroundLockTimeout param: %1</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/sessioncontroller.cpp" line="258"/>
        <source>Регистрация пользователя...</source>
        <translation>User registration...</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/sessioncontroller.cpp" line="268"/>
        <source>Требуется авторизация</source>
        <translation>Authorization required</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/sessioncontroller.cpp" line="311"/>
        <source>Подключение к серверу...</source>
        <translation>Connecting to server...</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/sessioncontroller.cpp" line="322"/>
        <source>Ошибка доставки сообщения</source>
        <translation>Error message delivery</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/sessioncontroller.cpp" line="340"/>
        <source>Загрузка списка контактов...</source>
        <translation>Loading contact list ...</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/sessioncontroller.cpp" line="350"/>
        <source>Ошибка входа в систему.</source>
        <translation>Logon failure.</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/sessioncontroller.cpp" line="361"/>
        <source>Неправильный логин или пароль.</source>
        <translation>Invalid login or password.</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/sessioncontroller.cpp" line="365"/>
        <source>Логин или телефонный номер уже используется.</source>
        <translation>Login or phone number is already in use.</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/sessioncontroller.cpp" line="368"/>
        <source>Недостаточно прав для входя в систему.</source>
        <translation>Have sufficient rights to log on.</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/sessioncontroller.cpp" line="447"/>
        <source>Загрузка данных...</source>
        <translation>Loading data ...</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/sessioncontroller.cpp" line="481"/>
        <source>Загрузка данных завершена...</source>
        <translation>Loading data is completed ...</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/sessioncontroller.cpp" line="482"/>
        <source>Подключение dBus демона...</source>
        <translation>Connecting dBus demon ...</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/sessioncontroller.cpp" line="503"/>
        <source>Демон dBus подключен успешно...</source>
        <translation>Demon dBus connected successfully ...</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/sessioncontroller.cpp" line="503"/>
        <source>Невозможно подключить демон dBus...</source>
        <translation>Cannot connect dBus demon ...</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/sessioncontroller.cpp" line="508"/>
        <source>Подключение к Jabber-серверу...</source>
        <translation>Conneting to Jabber-server...</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/sessioncontroller.cpp" line="518"/>
        <source>Применение глобальных настроек...</source>
        <translation>Application global settings ...</translation>
    </message>
</context>
<context>
    <name>ccappcore::SoundEvent</name>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/soundeventsmanager.cpp" line="22"/>
        <source>play sound: %1</source>
        <translation>play sound: %1</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/soundeventsmanager.cpp" line="31"/>
        <source>stop sound: %1</source>
        <translation>stop sound: %1</translation>
    </message>
</context>
<context>
    <name>ccappcore::SpamListController</name>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/spamlistcontroller.cpp" line="360"/>
        <source>red</source>
        <translation>red</translation>
    </message>
    <message>
        <location filename="../../Projects/new/trunk/sources/appcore/spamlistcontroller.cpp" line="362"/>
        <source>black</source>
        <translation>black</translation>
    </message>
</context>
</TS>
