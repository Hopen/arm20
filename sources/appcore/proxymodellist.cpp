#include <QStringList>
#include <QMimeData>
#include "proxymodellist.h"

namespace ccappcore
{

ProxyModelList::ProxyModelList(QObject *parent)
        : QAbstractItemModel(parent)
        , _nextIndexId(0)
{
}

void ProxyModelList::setSourceModelList(const ModelList& sourceList)
{
    beginResetModel();

    _sourceIndexMap.clear();
    _mappedIndexMap.clear();
    _nextIndexId = 0;

    _sourceModels = sourceList;

    foreach( ModelPtr model, _sourceModels )
        connectModel(model);
    endResetModel();
}

void ProxyModelList::removeModel(ModelPtr model)
{
    int begin = beginRow(model.data());
    int end = endRow(model.data());

    if( begin<0 || end < 0 )
        return;

    beginRemoveRows(QModelIndex(),begin, end);
    model->disconnect(this);
    _sourceModels.removeOne(model);
    endRemoveRows();
}

void ProxyModelList::insertModel(ModelPtr model, int pos)
{
    if(_sourceModels.contains(model))
        return;

    int begin = rowCount(QModelIndex());
    int end = model->rowCount(QModelIndex());

    beginInsertRows(QModelIndex(),begin,end);

    _sourceModels.insert( pos, model );
    connectModel(model);

    endInsertRows();
}

void ProxyModelList::connectModel(ModelPtr model)
{
    connect(model.data(), SIGNAL(dataChanged(QModelIndex,QModelIndex)),
            this, SLOT(sourceModelDataChanged(QModelIndex,QModelIndex)),
            Qt::DirectConnection);

    connect(model.data(), SIGNAL(modelReset()),
            this, SLOT(sourceModelReset()),
            Qt::DirectConnection);

    connect(model.data(), SIGNAL(rowsAboutToBeInserted(QModelIndex,int, int)),
            this, SLOT(sourceModelRowsAboutToBeInserted(QModelIndex,int,int)),
            Qt::DirectConnection);

    connect(model.data(), SIGNAL(rowsInserted(QModelIndex,int, int)),
            this, SLOT(sourceModelRowsInserted(QModelIndex,int,int)),
            Qt::DirectConnection);

    connect(model.data(), SIGNAL(rowsAboutToBeRemoved(QModelIndex,int, int)),
            this, SLOT(sourceModelRowsAboutToBeRemoved(QModelIndex,int,int)),
            Qt::DirectConnection);

    connect(model.data(), SIGNAL(rowsRemoved(QModelIndex,int, int)),
            this, SLOT(sourceModelRowsRemoved(QModelIndex,int,int)),
            Qt::DirectConnection);

    connect(model.data(), SIGNAL(layoutAboutToBeChanged()),
            this, SLOT(sourceModelLayoutAboutToBeChanged()),
            Qt::DirectConnection);

    connect(model.data(), SIGNAL(layoutChanged()),
            this, SLOT(sourceModelLayoutChanged()),
            Qt::DirectConnection);
}

int ProxyModelList::columnCount(const QModelIndex &parent /*= QModelIndex()*/) const
{
    if(!parent.isValid())
    {
        int columnCount = 0;
        foreach(ModelPtr model, _sourceModels)
            columnCount = std::max(columnCount, model->columnCount());

        return columnCount;
    }

    QModelIndex sourceParent = sourceIndexFromMappedIndex(parent);
    return sourceParent.model()->columnCount(sourceParent);
}

int ProxyModelList::rowCount(const QModelIndex& parent) const
{
    int count = 0;
    if(!parent.isValid())
    {
        foreach(ModelPtr model, _sourceModels)
            count += model->rowCount(QModelIndex());

        return count;
    }
    QModelIndex sourceParent = sourceIndexFromMappedIndex(parent);
    if(!sourceParent.isValid())
        return 0;

    count = sourceParent.model()->rowCount(sourceParent);
    return count;
}

QVariant ProxyModelList::data(const QModelIndex& index, int role) const
{
    if( !index.isValid() )
        return QVariant();

    QModelIndex sourceIndex = sourceIndexFromMappedIndex(index);
    if(!sourceIndex.isValid())
        return QVariant();

    return sourceIndex.data(role);
}

Qt::ItemFlags ProxyModelList::flags(const QModelIndex &index) const
{
    QModelIndex sourceIndex = sourceIndexFromMappedIndex(index);
    if(!sourceIndex.isValid())
        return QAbstractItemModel::flags(index);

    return sourceIndex.model()->flags(sourceIndex);
}

QModelIndex ProxyModelList::index(int row, int column,
                              const QModelIndex &parent /*= QModelIndex()*/) const
{
    if(_sourceModels.isEmpty())
        return QModelIndex();

    ModelPtr sourceModel;
    QModelIndex mappedIndex;
    QModelIndex sourceIndex;
    if( !parent.isValid() )
    {
        sourceModel = modelAt(row);
        if(sourceModel.isNull())
            return QModelIndex();
        int shift = beginRow(sourceModel.data());
        mappedIndex = createIndex(row, column, (void*) NULL);
        sourceIndex = sourceModel->index(row-shift,column,QModelIndex());
    }
    else
    {
        QModelIndex sourceParent = sourceIndexFromMappedIndex(parent);
        sourceModel = modelPtr(sourceParent.model());
        if(sourceModel.isNull())
            return QModelIndex();

        sourceIndex = sourceModel->index(row, column, sourceParent);
        mappedIndex = mappedIndexFromSourceIndex(sourceIndex);
        if(!mappedIndex.isValid())
            mappedIndex = createIndex(row, column, getNextIndexId());
    }

    _mappedIndexMap[mappedIndex] = sourceIndex;
    _sourceIndexMap[sourceIndex] = mappedIndex;
    return mappedIndex;
}

ModelPtr ProxyModelList::modelAt(int row) const
{
    int shift = 0;
    foreach(ModelPtr model, _sourceModels)
    {
        int begin = shift;
        int count = model->rowCount(QModelIndex());
        if( row >= begin && row < begin + count )
            return model;
        shift = begin + count;
    }

    return ModelPtr();
}

int ProxyModelList::beginRow(const QAbstractItemModel* model) const
{
    int shift = 0;
    foreach(ModelPtr modelPtr, _sourceModels)
    {
        int begin = shift;
        int count = modelPtr->rowCount(QModelIndex());
        if(model == modelPtr.data())
            return begin;
        shift = begin + count;
    }
    return -1;
}

int ProxyModelList::endRow(const QAbstractItemModel* model) const
{
    int begin = beginRow(model);
    int count = model->rowCount(QModelIndex());
    return begin+count;
}

QModelIndex ProxyModelList::mappedIndexFromSourceIndex(const QModelIndex& index) const
{
    if(!index.isValid())
        return QModelIndex();

    QModelIndex mapppedIndex;
    if(_sourceIndexMap.contains(index))
        mapppedIndex = _sourceIndexMap[index];

    return mapppedIndex;
}

QModelIndex ProxyModelList::sourceIndexFromMappedIndex(const QModelIndex& index) const
{
   if( !index.isValid())
       return QModelIndex();

   QModelIndex sourceIndex;
   if(_mappedIndexMap.contains(index))
       sourceIndex = _mappedIndexMap[index];

   return sourceIndex;
}

QModelIndex ProxyModelList::parent(const QModelIndex &child) const
{
    if( !child.isValid() )
        return QModelIndex();

    QModelIndex sourceChild = sourceIndexFromMappedIndex(child);
    if(!sourceChild.isValid())
        return QModelIndex();
    QModelIndex sourceParent = sourceChild.model()->parent(sourceChild);
    QModelIndex mappedParent = mappedIndexFromSourceIndex(sourceParent);
    return mappedParent;
}

void ProxyModelList::sourceModelDataChanged(const QModelIndex &topLeft,
                                               const QModelIndex &bottomRight)
{
    QModelIndex topLeftMapped = mappedIndexFromSourceIndex(topLeft);
    QModelIndex bottomRightMapped = mappedIndexFromSourceIndex(bottomRight);
    emit dataChanged(topLeftMapped, bottomRightMapped);
}

ModelPtr ProxyModelList::modelPtr(const QAbstractItemModel* model) const
{
    foreach(ModelPtr m, _sourceModels)
        if(m.data() == model)
            return m;

    return ModelPtr();
}

void ProxyModelList::sourceModelReset()
{
    emit layoutAboutToBeChanged();
    _sourceIndexMap.clear();
    _mappedIndexMap.clear();
    _nextIndexId = 0;
    emit layoutChanged();
}

void ProxyModelList::sourceModelRowsAboutToBeInserted(const QModelIndex &parent, int first, int last)
{
    ModelPtr sourceModel = modelPtr(qobject_cast<QAbstractItemModel*>(sender()));
    if(sourceModel.isNull() || !_sourceModels.contains(sourceModel))
        return;

    if(parent.isValid())
    {
        QModelIndex mappedParent = mappedIndexFromSourceIndex(parent);
        beginInsertRows(mappedParent, first, last);
        return;
    }

    int offset = beginRow(sourceModel.data());
    beginInsertRows(QModelIndex(), first+offset, last+offset);
}

void ProxyModelList::sourceModelRowsInserted(const QModelIndex &parent, int first, int last)
{
    Q_UNUSED(parent);
    Q_UNUSED(first);
    Q_UNUSED(last);
    endInsertRows();
}

void ProxyModelList::sourceModelRowsAboutToBeRemoved(const QModelIndex &parent, int first, int last)
{
    ModelPtr sourceModel = modelPtr(qobject_cast<QAbstractItemModel*>(sender()));
    if(sourceModel.isNull() || !_sourceModels.contains(sourceModel))
        return;

    if(parent.isValid())
    {
        QModelIndex mappedParent = mappedIndexFromSourceIndex(parent);
        beginRemoveRows(mappedParent, first, last);
        return;
    }

    int offset = beginRow(sourceModel.data());
    beginRemoveRows(QModelIndex(), first+offset, last+offset);
}

void ProxyModelList::sourceModelRowsRemoved(const QModelIndex &parent, int first, int last)
{
    Q_UNUSED(parent);
    Q_UNUSED(first);
    Q_UNUSED(last);

    endRemoveRows();
}

QStringList ProxyModelList::mimeTypes() const
{
    QStringList types;
    foreach(const ModelPtr& model, _sourceModels)
    {
        types.append(model->mimeTypes());
    }
    return types;
}

Qt::DropActions ProxyModelList::supportedDropActions() const
{
    return Qt::LinkAction | Qt::CopyAction | Qt::MoveAction;
}

QMimeData* ProxyModelList::mimeData(const QModelIndexList &indexes) const
{
    if(indexes.count() == 0)
        return new QMimeData();

    QModelIndex firstmappedIndex = indexes[0];
    QModelIndex firstSourceIndex = sourceIndexFromMappedIndex(firstmappedIndex);
    if(!firstSourceIndex.isValid())
        return new QMimeData();

    QModelIndexList sourceModelIndexList;
    foreach(QModelIndex i, indexes)
    {
        QModelIndex sourceIndex = sourceIndexFromMappedIndex(i);
        if(!sourceIndex.isValid() || sourceIndex.model()!=firstSourceIndex.model())
            return new QMimeData();
        sourceModelIndexList.append(sourceIndex);
    }

    return firstSourceIndex.model()->mimeData(sourceModelIndexList);
}

bool ProxyModelList::dropMimeData(const QMimeData *data, Qt::DropAction action,
                          int row, int column, const QModelIndex &parent)
{
    if(action == Qt::IgnoreAction)
        return false;

    QModelIndex mappedIndex = index(row, column, parent);
    QModelIndex sourceIndex = sourceIndexFromMappedIndex(mappedIndex);
    if(!sourceIndex.isValid())
        return false;

    ModelPtr model = modelAt(mappedIndex.row());
    if(model.isNull())
        return false;

    return model->dropMimeData(data,
                                      action,
                                      sourceIndex.row(),
                                      sourceIndex.column(),
                                      sourceIndex.parent());
}

void ProxyModelList::sourceModelLayoutAboutToBeChanged()
{
    emit layoutAboutToBeChanged();
}

void ProxyModelList::sourceModelLayoutChanged()
{
    emit layoutChanged();
}

}
