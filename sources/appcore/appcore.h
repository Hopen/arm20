#ifndef APPCORE_H
#define APPCORE_H

/*
#ifndef QT_DBUS
#   define QT_DBUS
#   define QT_NO_DBUS
#endif
*/

#include <QtCore>


#ifndef QT_NODLL
#  ifdef APPCORE_PACKAGE
#    define APPCORE_EXPORT Q_DECL_EXPORT
#  else
#    define APPCORE_EXPORT Q_DECL_IMPORT
#  endif
#else
#  define APPCORE_EXPORT
#endif


#ifndef _CDSVER
//#   define _CDSVER 0x0400
#   define _CDSVER 0x0300
#endif



#endif // APPCORE_H
