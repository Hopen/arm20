#include "statusoptions.h"

namespace ccappcore
{

StatusOptions::StatusOptions(QObject *parent)
    : QObject(parent)
    , _pauseAfterLogon(false)
    , _pauseWhenDialing(true)
    , _otherPauseEnabled(1)
{
}

void StatusOptions::setPauseReasons(const QStringList& data)
{
    _pauseReasons = data; emit dataChanged();
}

void StatusOptions::applySettings(const AppSettings& settings)
{    
    bool bDataChanged = false;

    _pauseReasons.clear();
    _pauseGlobalReasons.clear();
    _otherPauseEnabled = settings.getOption("OtherPauseEnabled").toInt();

    _pauseList = settings.getOption("PauseList").toString();
    _pauseGlobalReasons = _pauseList.split("\r\n");

    while(_pauseGlobalReasons.removeOne(""));

    if (!_pauseGlobalReasons.isEmpty())
        bDataChanged = true;

    QVariant varPauseReasons = settings.getOption("status/pause_reasons");
    if(varPauseReasons.canConvert<QStringList>())
    {
        _pauseReasons = varPauseReasons.value<QStringList>();
        bDataChanged = true;
    }

    _pauseAfterLogon = settings.getOption("status/pauseAfterLogon").toBool();
    _pauseWhenDialing = settings.getOption("status/pauseWhenDialing").toBool();

    if (bDataChanged)
        emit dataChanged();
}

void StatusOptions::flushSettings(AppSettings& settings)
{
    settings.setUserOption("status/pause_reasons",
                           QVariant::fromValue(_pauseReasons));
    settings.setUserOption("status/pauseAfterLogon", _pauseAfterLogon);
    settings.setUserOption("status/pauseWhenDialing", _pauseWhenDialing);
    settings.setUserOption("PauseList", _pauseList);
    settings.setUserOption("OtherPauseEnabled", _otherPauseEnabled);
}

}
