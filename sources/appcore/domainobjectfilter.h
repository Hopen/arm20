#ifndef DOMAINOBJECTFILTER_H
#define DOMAINOBJECTFILTER_H

#include "appcore.h"
#include "predicate.h"
#include "callscontroller.h"
#include "operlistcontroller.h"

namespace ccappcore
{
    class IsOperInGroup: public Predicate<Operator>
    {
        Group _group;
        LateBoundObject<OperListController> _operListController;
    public:
        IsOperInGroup(const Group& group)
            : _group(group)
        {}

        bool operator()(const Operator& op) const
        {
            return _group.isParentOf(op);
        }
    };

    class IsGroupWithOper: public Predicate<Group>
    {
        LateBoundObject<OperListController> _operListController;
    public:
        IsGroupWithOper(bool privateGroupOnly)
            : _privateGroupOnly(privateGroupOnly)
        {}
        //void setPrivate(bool privateGroupOnly){_privateGroupOnly = privateGroupOnly;}

        bool operator()(const Group& group) const
        {
            return ((!_privateGroupOnly) || (group.isParentOf(_operListController->sessionOwner())));
        }

    private:
        bool _privateGroupOnly;
    };

    class IsContactOnline: public Predicate<Contact>
    {
    public:
        bool operator()(const Operator& op) const
        {
            return op.isOnline();
        }
    };

    class IsContactOffline: public Predicate<Operator>
    {
    public:
        bool operator()(const Operator& op) const
        {
            return op.isOffline();
        }
    };

    class IsNonEmptyContainer: public Predicate<Contact>
    {
    public:
        bool operator()(const Contact& c) const
        {
            if(!c.isContainer())
                return false;
            return !c.childrenLoaded() || c.children().count()>0;
        }
    };

    class IsContactInSessionOwnerGroup: public Predicate<Contact>
    {
        LateBoundObject<OperListController> _operListController;
        QList<Group> _privateGroups;
    public:

        IsContactInSessionOwnerGroup()
        {
            LateBoundObject<SessionController> session;
            Operator sessionOwner = session->sessionOwner();
            foreach(const Group g, _operListController->groupList())
            {
                if(g.isParentOf(sessionOwner))
                    _privateGroups.append(g);
            }
        }

        bool operator()(const Contact& contact) const
        {
            foreach(Group group, _privateGroups)
                if(group.isParentOf(contact) || group==contact)
                    return true;

            return false;
        }
    };

    class IsSessionOwnerCall: public Predicate<Call>
    {
        LateBoundObject<CallsController> _callsController;
    public:

        IsSessionOwnerCall()
        {
        }

        bool operator()(const Call& call) const
        {
            Call::CallState callState = call.callState();
            if( callState<Call::CS_Assigned || callState >= Call::CS_CallFailed )
                return false;

            if(call.operId() == _callsController->sessionOwner().contactId())
                return true;

            Call linkedCall = _callsController->findByCallId(call.operCallId());
            if(linkedCall.operId() == _callsController->sessionOwner().contactId())
                return true;

            return false;
        }
    };


//    class IsSessionOwnerIncomingOneCallOnly: public Predicate<Call>
//    {
//        LateBoundObject<CallsController> _callsController;
//    public:

//        IsSessionOwnerIncomingOneCallOnly()
//        {
//        }

//        bool operator()(const Call& call) const
//        {
//            Call::CallState callState = call.callState();
//            if( callState<Call::CS_Assigned || callState >= Call::CS_CallFailed )
//                return false;

//            if(call.operId() == _callsController->sessionOwner().contactId())
//                return true;

////            Call linkedCall = _callsController->findByCallId(call.operCallId());
////            if(linkedCall.operId() == _callsController->sessionOwner().contactId())
////                return true;

//            return false;
//        }
//    };


    class IsQueuedCall: public Predicate<Call>
    {
        LateBoundObject<CallsController> _callsController;
        bool _privateGroupsOnly;
    public:

        IsQueuedCall(bool privateGroupsOnly)
            : _privateGroupsOnly(privateGroupsOnly)
        {
        }

        bool operator()(const Call& call) const
        {
            if( !call.isQueued() )
                return false;

//            if (_callsController->isPrivateGroupCall(call))
//                return true;

//            if (_callsController->isAnotherPrivateGroupCall(call))
//                return false;

            if(!_privateGroupsOnly)
                return true;

            bool result = _callsController->isOwnGroupCall(call);
            return result;
        }
    };

    class IsOperator: public Predicate<Contact>
    {
    public:
        bool operator()(const Operator& op) const
        {
            return (op.contactId().type() == CT_Operator);
        }
    };

    class IsGroup: public Predicate<Contact>
    {
    public:
        bool operator()(const Group& op) const
        {
            return (op.contactId().type() == CT_Group);
        }
    };

}

#endif // DOMAINOBJECTFILTER_H
