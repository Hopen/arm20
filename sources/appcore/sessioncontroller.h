#ifndef SessionController_H
#define SessionController_H

#include <QObject>

#include "lateboundobject.h"
#include "appsettings.h"
#include "eventlistener.h"
#include "iappcryptoprovider.h"
#include "statusnotifier.h"
#include "contact.h"
#include "cryptoprovider.h"
#include "ntlmauthentication.h"
#include "redirectlistcontroller.h"
#include "spamlistcontroller.h"
#include "JabberController.h"
#include "globaladdressbookcontroller.h"

class QStateMachine;
class QState;
class QFinalState;
class ScriptAdapter;

namespace ccappcore
{
class Message;
class OperListController;
class CallsController;

//SessionController allows logging into the call center. It stores login info
//in user settings, and hardware info in application settings.
//By default it logs in user as Operator. To change this use setSessionType().
class APPCORE_EXPORT SessionController
        : public QObject
        , IAppSettingsHandler
{
    Q_OBJECT
    Q_INTERFACES(ccappcore::IAppSettingsHandler)
public:

    enum SessionErrorCode
    {
        LoginCanceled,
        ConnectError,
        AlreadyLoggedIn,
        LoginFailed
    };

    enum SessionType
    {
        OperatorSession = 0x01,
        SupervisorSession = 0x02
    };
    
    SessionController(QObject* parent = 0);
    ~SessionController();
public:
    void setSessionType(ccappcore::SessionController::SessionType type) { _sessionType = type; }
    ccappcore::Contact sessionOwner() const;

    //initialize session controller. (plugins, including router client, should be ready)
    void initialize();
public:
    const QString& login() const { return _login; }
#if (_CDSVER >= 0x0400)
    QByteArray password() const { return _password.toByteArray(); }
#else
    QString password() const { return _password.toString(); }
#endif

    bool isAutoLoginEnabled() const { return _autoLogin; }
    bool isForcedLoginEnabled() const { return _forced; }
    bool isSessionStarted() const;
    bool isNtlmLoginEnabled() const { return _useNtlm; }
    bool isConnected() const;
    QString getLanguage()const{return _langId;}

    int deviceType() const { return _deviceType; }
    const QString& deviceNumber() const { return _deviceNumber; }
    QString sessionTypeString();

    static QString detectValidIpAddress();

protected:
    void addLoadDataAgent(QFutureWatcher<void> *agent, const char *amember);

public slots:
    void setLogin(const QString& login) { _login = login; }
    void setPassword(const QString& password);
    void encryptPassword();
    void setDeviceType(int device) { _deviceType = device; }
    void setDeviceNumber(const QString& device) { _deviceNumber = device; }
    void setForcedLoginEnabled(bool forced) { _forced = forced; }
    void setAutoLoginEnabled(bool autoLogin) { _autoLogin = autoLogin; }
    void setNtlmLoginEnabled(bool useNtlm) { _useNtlm = useNtlm; }
    void setLanguage(QString id) {_langId = id;}

public slots:
    void startSession();
    void stopSession();

signals:
    void sessionStarted() const;
    void sessionStopped();
    void sessionError(SessionController::SessionErrorCode reason,
                               const QString& error) const;

    //void switchSessionLanguage(int id);

protected slots:

    void connectToRouter();
    void onRouterError(const QString&);
    void onRouteMessageError();

    void onSessionStarted();
    void onSessionStopped();
    void onLoadData();
    void onLoadDataCompleted();

    void onLoadProfiles();
    void onLoadSpam();
    void onLoadAddressBook();
    void onLoadSetiings();

    void sendLoginRequest();
    void on_c2oLoginSucceded(const ccappcore::Message& m);
    void on_c2oLoginFailed(const ccappcore::Message& m);

    void setFreeStatusAfterLogon();
    void on_c2oUpdateOperStatus(const ccappcore::Message& m);
    void setFreeStatus();

protected: //IAppSettingsHandler
    virtual void applySettings(const AppSettings& settings);
    virtual void flushSettings(AppSettings& settings);

protected:
    LateBoundObject<AppSettings> _settings;
    LateBoundObject<OperListController> _operListController;
    LateBoundObject<CallsController> _callsController;
    LateBoundObject<IAppCryptoProvider> _crypto;
    LateBoundObject<StatusNotifier> _status;
    LateBoundObject<IRouterClient> _router;
    LateBoundObject<CryptoProvider> _cryptoProvider;
    LateBoundObject<INtlmLogonProvider> _ntlm;
    LateBoundObject<RedirectListController> _redirectListController;
    LateBoundObject<SpamListController> _spamListController;
    //LateBoundObject<JabberController> _jabberController;
    LateBoundObject<GlobalAddressBookController> _globalAddressController;

    EventListener _c2oLoginLister;
    EventListener _opStatusListener;

    QString         _login;
    QVariant        _password; //password encrypted by RSA public key or as clear text
    SessionType     _sessionType;
    int             _deviceType; //trunk or route
    QString         _deviceNumber; //phone, ip, etc
    bool            _forced;
    bool            _autoLogin;
    bool            _useNtlm;
    QString         _langId;

    Contact         _sessionOwner;

    QTimer          _pauseTimer;

    QPointer<ScriptAdapter> _adapter;

    QPointer<QStateMachine>   _sessionStateMachine;

    //session states
    QPointer<QState>        _stateConnecting;
    QPointer<QState>        _stateConnected;
    QPointer<QFinalState>   _stateDisconnected;

    //children of connected state
    QPointer<QState>    _stateStarting;
    QPointer<QState>    _stateStarted;

    //children of starting state
#if (_CDSVER >= 0x0400)
    QPointer<QState>    _stateReqPublicKey;
#endif
    QPointer<QState>    _stateTryLogin;
    QPointer<QState>    _stateLoadData;

    //QPointer<QState>    _stateLoadProfiles;
    //QPointer<QState>    _stateLoadSpam;

    QFutureWatcher<void> _loadDataFuture;
    QFutureWatcher<void> _loadProfilesFuture;
    QFutureWatcher<void> _loadSpamFuture;
    QFutureWatcher<void> _loadAddressBookFuture;
    QFutureWatcher<void> _loadSettingsFuture;

};

}

#endif // SessionController_H
