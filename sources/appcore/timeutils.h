#ifndef TIMEUTILS_H
#define TIMEUTILS_H

#include "appcore.h"

namespace ccappcore {

class APPCORE_EXPORT TimeUtils
{
public:
    TimeUtils();

    static QTime timeElapsed(const QDateTime& since, const QDateTime& to);

    static QString SecsToLongHMS(int totalSeconds);
    static QString SecsToShortHMS(int totalSeconds);
    static QString minutesString(int minutes);
};

} // namespace ccappcore

#endif // TIMEUTILS_H
