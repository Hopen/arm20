SOURCES += messageparam.cpp \
    message.cpp \
    requestreplymessage.cpp \
    eventlistener.cpp \
    apploader.cpp \
    appsettings.cpp \
    applogger.cpp \
    styleloader.cpp \
    layoutmanager.cpp \
    shortcutmanager.cpp \
    soundeventsmanager.cpp \
    statusnotifier.cpp \
    appurlhandler.cpp \
    ccwebservices.cpp \
    serviceprovider.cpp \
    cryptoprovider.cpp \
    presetmanager.cpp \
    presetwidget.cpp \
    quantativeindicator.cpp \
    indicatorlistmodel.cpp

HEADERS += serviceprovider.h \
    iappmodule.h \
    ilogger.h \
    irouterclient.h \
    messageparam.h \
    message.h \
    clientaddress.h \
    imessageserializer.h \
    requestreplymessage.h \
    eventlistener.h \
    apploader.h \
    appsettings.h \
    applogger.h \
    lateboundobject.h \
    iappcryptoprovider.h \
    styleloader.h \
    layoutmanager.h \
    shortcutmanager.h \
    soundeventsmanager.h \
    statusnotifier.h \
    iappurlhandler.h \
    appurlhandler.h \
    ccwebservices.h \
    cryptoprovider.h \
    ntlmauthentication.h \
    presetmanager.h \
    presetwidget.h \
    quantativeindicator.h \
    indicatorlistmodel.h

windows {
    SOURCES += ntlmauthentication.cpp
}

#win32 {
#    !static {
#      DEFINES+=QT_MAKEDLL
#    }
#    else {
#      DEFINES+=QT_NODLL
#    }
#}

