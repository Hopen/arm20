#ifndef FOCUSEVENTTRANSLATOR_H
#define FOCUSEVENTTRANSLATOR_H

#include "appcore.h"
class QWidget;

namespace ccappcore
{

class APPCORE_EXPORT FocusEventTranslator : public QObject
{
Q_OBJECT
public:
    explicit FocusEventTranslator(QWidget *observedWidget);

signals:
    void focusIn();
    void focusOut();
private slots:
    void focusChanged(QWidget* oldWidget,QWidget* newWidget);
private:
    QPointer<QWidget> _observerdWidget;
};

}
#endif // FOCUSEVENTTRANSLATOR_H
