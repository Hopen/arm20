#include "QtGui"
#include "redirectlistcontroller.h"
#include "requestreplymessage.h"
#include "callscontroller.h"

namespace ccappcore
{

RedirectListController::RedirectListController(QObject *parent) :
    QObject(parent)
{
}

//void RedirectListController::addInvalidProfile(const qint32& position)
//{
//    _profilesList.insert(position,RedirectProfile());
//}

bool RedirectListController::removeProfile(const qint32 &position)
{
    if (position >= _profilesList.size())
        return false;

    if (!_profilesList.at(position).IsValid())
        return false;

    //{
        // delete redir profile task
        Message m("O2C_DELETE_REDIR_PROFILE");
        m["prof_id"] = _profilesList.at(position).id();

        _router->send(m, ClientAddress::Any());
    //}

    _profilesList.removeAt(position);
    return true;
}

bool RedirectListController::removeProfile(const QString &name)
{
    return removeProfile(findProfileByName(name));
}

bool RedirectListController::createProfile(const qint32 &position)
{
//    if (position >= _profilesList.size())
//        return false;

//    RedirectProfile& r = _profilesList[position];
//    if (!r.IsValid() && !r.name().isEmpty())
//    {
        // create redir profile task
        RedirectProfile r;
        r.setId(TEMPORARY_PROFILE_ID);
        _profilesList.insert(position,r);

        Message request, replyPrototype;
        request = Message("O2C_CREATE_REDIR_PROFILE");
        request["oper_id"] = _callsController->sessionOwner().contactId().toInt();
        request["name"   ] = tr("");//r.name();
        request["task_id"] = tr(""); // NFS - need for string! empty profile tasks
        replyPrototype = Message("C2O_CREATE_REDIR_PROFILE");
        RequestReplyMessage::send(
            request, replyPrototype, this, SLOT(on_c2oCreateRedirProfile(ccappcore::Message)));
//    }

    return true;
}

//bool RedirectListController::createProfile()
//{
//    addInvalidProfile(_profilesList.size()-1);
//    return createProfile(_profilesList.size()-1);
//}

bool RedirectListController::editProfile(const qint32 &position)
{
    if (position >= _profilesList.size())
        return false;

    RedirectProfile& r = _profilesList[position];
    //if (r.IsValid() /*&& !r.name().isEmpty()*/)
    //{
        Message m;
        m = Message("O2C_EDIT_REDIR_PROFILE");
        m["oper_id"] = _callsController->sessionOwner().contactId().toInt();
        m["prof_id"] = r.id();
        m["name"   ] = r.name();
        m["task_id"] = r.taskId2Str();
        _router->send(m, ClientAddress::Any());
    //}
    emit profileEdited(position);
    return true;

}

bool RedirectListController::setActiveProfile(const qint32 &position, bool turnOFF)
{
    if (position >= _profilesList.size())
        return false;

    RedirectProfile& r = _profilesList[position];
    if (turnOFF)
        r.setStatus(tr("OFF"));
    else
    {
        r.setStatus(tr("ON"));
        for (int i=0;i<_profilesList.size();++i)
        {
            if (i==position)
                continue;
            _profilesList[i].setStatus(tr("OFF"));
        }
    }
    Message m;
    m = Message("O2C_SET_REDIR_PROFILE");
    m["prof_id"] = r.id();
    m["status"   ] = r.status();//turnOFF?tr("OFF"):tr("ON");
    _router->send(m, ClientAddress::Any());

    return true;

}


QFuture <RedirectProfile> RedirectListController::loadRedirectionsAsync()
{
    _loadRedirectionsResult = QFutureInterface<RedirectProfile>( (QFutureInterfaceBase::State)
            (QFutureInterfaceBase::Started | QFutureInterfaceBase::Running)
            );

    _profilesList.clear();
    _rulesList   .clear();
    _scriptsList .clear();

    Message request, replyPrototype;
    request = Message("O2C_GET_REDIR_PROFILES");
    request["oper_id"] = _callsController->sessionOwner().contactId().toInt();
    replyPrototype = Message("C2O_GET_REDIR_PROFILES");
    RequestReplyMessage::send(
        request, replyPrototype, this, SLOT(on_c2oRedirProfiles(ccappcore::Message)));

    request = Message("O2C_GET_REDIRECTIONS");
    request["oper_id"] = _callsController->sessionOwner().contactId().toInt();
    replyPrototype = Message("C2O_GET_REDIRECTIONS");
    RequestReplyMessage::send(
        request, replyPrototype, this, SLOT(on_c2oGetRedirections(ccappcore::Message)));


    request = Message("O2C_SCRIPT_EVENT_REQ");
    replyPrototype = Message("C2O_SCRIPT_EVENT");
    RequestReplyMessage::send(
        request, replyPrototype, this, SLOT(on_c2oScriptEvent(ccappcore::Message)));

    return _loadRedirectionsResult.future();
}

//QFuture <RedirectRule> RedirectListController::loadRedirRulesAsync()
//{
//    _loadRulesResult = QFutureInterface<RedirectRule>( (QFutureInterfaceBase::State)
//            (QFutureInterfaceBase::Started | QFutureInterfaceBase::Running)
//            );

//    Message request, replyPrototype;
//    request = Message("O2C_GET_REDIRECTIONS");
//    request["oper_id"] = _callsController->sessionOwner().contactId().toInt();
//    replyPrototype = Message("C2O_GET_REDIRECTIONS");
//    RequestReplyMessage::send(
//        request, replyPrototype, this, SLOT(on_c2oGetRedirections(ccappcore::Message)));

//    return _loadRulesResult.future();
//}

void RedirectListController::on_c2oRedirProfiles(const ccappcore::Message &m)
{
    _loadRedirectionsResult.reportStarted();
    if( 0==(quint32)m["sql_result"] )
    {
        QByteArray listData = m["data"];
        QList<Message> list = _messageSerializer->loadList(listData);
        foreach(const Message& m, list)
        {
            qDebug()<<m;
            qint32  id     = m["prof_id"];
            QString name   = m["name"];
            qint32  taskId = m["task_id"];
            QString status = m["status"];

            RedirectProfile profile (id,name,taskId,status);
            _profilesList.push_back(profile);
        }
    }
//    if (_profilesList.size() > 0)
//    {
//        _loadRedirectionsResult.reportResults(_profilesList.toVector(), 0, _profilesList.count() );

//    }
//    _loadRedirectionsResult.reportFinished();
}

void RedirectListController::on_c2oGetRedirections(const ccappcore::Message &m)
{
//    _loadRulesResult.reportStarted();
    if( 0==(quint32)m["sql_result"] )
    {
        QByteArray listData = m["data"];
        QList<Message> list = _messageSerializer->loadList(listData);
        foreach(const Message& m, list)
        {
            qDebug()<<m;
            qint32  id        = m["task_id"];
            QString name      = m["name"];
            qint32  if_type   = m["if_type"];
            QString if_value  = m["if_value"];
            qint32  act_type  = m["act_type"];
            QString act_value = m["act_value"];
            qint32  max_count = m["max_count"];

            _rulesList.push_back(RedirectRule(id,name,if_type,if_value,act_type,act_value,max_count));
        }
    }

//    if (_rulesList.size() > 0)
//    {
//        _loadRulesResult.reportResults(_rulesList.toVector(), 0, _rulesList.count() );

//    }
//    _loadRulesResult.reportFinished();
//    if (_profilesList.size() > 0)
//    {
//        _loadRedirectionsResult.reportResults(_profilesList.toVector(), 0, _profilesList.count() );

//    }
//    _loadRedirectionsResult.reportFinished();
}

void RedirectListController::on_c2oScriptEvent(const Message &m)
{

    if( 0==(quint32)m["sql_result"] )
    {
        QByteArray listData = m["data"];
        QList<Message> list = _messageSerializer->loadList(listData);
        foreach(const Message& m, list)
        {
            qDebug()<<m;
            qint32  id   = m["id"];
            QString name = m["event"];
            QString desc = m["description"];

            _scriptsList.push_back(ScriptEvent(id,name,desc));
        }
    }

    if (_profilesList.size() > 0)
    {
        _loadRedirectionsResult.reportResults(_profilesList.toVector(), 0, _profilesList.count() );

    }
    _loadRedirectionsResult.reportFinished();
}

void RedirectListController::on_c2oCreateRedirProfile(const ccappcore::Message &m)
{
    if(0 == (quint32)m["sql_result"])
    {
        qint32 prof_id = (quint32)m["prof_id"];
        // that's crap, qt does't have any functions to find by predicate
//        for (RedirProfileContainer::iterator it=_profilesList.begin();it!=_profilesList.end();++it)
//        {
//            if(it->id() == TEMPORARY_PROFILE_ID)
//            {
//                it->setId(prof_id);
//            }
//        }
        qint32 pos = findProfileById(TEMPORARY_PROFILE_ID);
        if (pos < _profilesList.size())
            _profilesList[pos].setId(prof_id);

        emit profileAdded();
    }
//    if(0 == (quint32)m["sql_result"])
//    {
//        qint32 prof_id = (quint32)m["prof_id"];
//        _profilesList.push_back(RedirectProfile(prof_id,tr(""),0,tr("OFF")));
//        emit profileAdded();
//        //_profilesList.insert(position,RedirectProfile());
//    }
}

qint32 RedirectListController::findProfileByName(const QString& name)const
{
    for(qint32 i=0;i<_profilesList.size();++i)
    {
        if (!name.compare(_profilesList[i].name()))
            return i;
    }
    return _profilesList.size(); // out of range
}

qint32 RedirectListController::findProfileById(const qint32& id)const
{
    for(qint32 i=0;i<_profilesList.size();++i)
    {
        if (id == _profilesList[i].id())
            return i;
    }
    return _profilesList.size(); // out of range
}

bool RedirectListController::createRule(const qint32 &position)
{
    RedirectRule r;
    r.setId(TEMPORARY_RULE_ID);
    _rulesList.insert(position,r);

    Message request, replyPrototype;
    request = Message("O2C_CREATE_REDIR_TASK");
    request["oper_id"] = _callsController->sessionOwner().contactId().toInt();

    request[ "name"       ] = tr("");
    request[ "a_num"      ] = tr("");
    request[ "client_id"  ] = tr("");
    request[ "status"     ] = tr("");
    request[ "call_count" ] = 0;
    request[ "time"       ] = tr("");
    request[ "time_type"  ] = tr("");
    request[ "date"       ] = tr("");
    request[ "act_type"   ] = RedirectRule::RA_Unknown;
    request[ "act_value"  ] = tr("");

    replyPrototype = Message("C2O_CREATE_REDIR_TASK");
    RequestReplyMessage::send(
        request, replyPrototype, this, SLOT(on_c2oCreateRedirRule(ccappcore::Message)));

    return true;
}

bool RedirectListController::removeRule(const QString &name)
{
    return removeRule(findRuleByName(name));
}

bool RedirectListController::removeRule(const qint32 &position)
{
    if (position >= _rulesList.size())
        return false;

    if (!_rulesList.at(position).IsValid())
        return false;

    Message m("O2C_DELETE_REDIR_TASK");
    m["task_id"] = _rulesList.at(position).id();
    _router->send(m, ClientAddress::Any());

    _rulesList.removeAt(position);
    return true;
}

bool RedirectListController::editRule(const qint32 &position)
{
    if (position >= _rulesList.size())
        return false;

    RedirectRule& r = _rulesList[position];
    Message m("O2C_EDIT_REDIR_TASK");
    m[ "task_id"    ] = r.id();
    m[ "name"       ] = r.name();
    m[ "a_num"      ] = (r.if_type() == RedirectRule::RC_ANumber  )?r.if_value():tr("");
    m[ "client_id"  ] = (r.if_type() == RedirectRule::RC_Person   )?r.if_value():tr("");
    m[ "status"     ] = (r.if_type() == RedirectRule::RC_OperState)?r.if_value():tr("");
    m[ "call_count" ] = (r.if_type() == RedirectRule::RC_CallCount)?r.if_value().toInt():0;
    m[ "time"       ] = (r.if_type() == RedirectRule::RC_Time     )?r.if_value():tr("");
    m[ "time_type"  ] = tr(""); // ToDo:
    m[ "date"       ] = tr(""); // ToDo:
    m[ "act_type"   ] = r.act_type ();
    m[ "act_value"  ] = r.act_value();

    _router->send(m, ClientAddress::Any());

    emit ruleEdited(position);
    return true;
}

void RedirectListController::on_c2oCreateRedirRule(const ccappcore::Message &m)
{
    if(0 == (quint32)m["sql_result"])
    {
        qint32 task_id = (quint32)m["task_id"];
        qint32 pos = findRuleById(TEMPORARY_RULE_ID);
        if (pos < _rulesList.size())
            _rulesList[pos].setId(task_id);

        emit ruleAdded();
    }
}

qint32 RedirectListController::findRuleByName(const QString& name)const
{
    for(qint32 i=0;i<_rulesList.size();++i)
    {
        if (!name.compare(_rulesList[i].name()))
            return i;
    }
    return _rulesList.size(); // out of range
}

qint32 RedirectListController::findRuleById(const qint32& id)const
{
    for(qint32 i=0;i<_rulesList.size();++i)
    {
        if (id == _rulesList[i].id())
            return i;
    }
    return _rulesList.size(); // out of range
}

qint32 RedirectListController::findScriptByName(const QString &name) const
{
    for(qint32 i=0;i<_scriptsList.size();++i)
    {
        if (!name.compare(_scriptsList[i].name()))
            return i;
    }
    return _scriptsList.size(); // out of range

}

const QIcon& RedirectListController::statusIcon(int status) const
{
    static QIcon byNumberIcon(":/resources/icons/new/status phone 32.png");
    static QIcon byVoicemailIcon(":/resources/icons/new/status message 32.png");

//    switch(status)
//    {
//    case RedirectRule::PR_CALL2NUM:
//        return byNumberIcon;
//        break;
//    case RedirectRule::PR_CALL2VM:
//        return byVoicemailIcon;
//        break;

//    default:
//        break;
//    }

    static QIcon other;
    return other;
}


//void RedirectListController::activateCall2NumProfile()
//{
//    removeProfile(SYSTEM_CALL2NUM_RULENAME);
//    removeRule   (SYSTEM_CALL2NUM_RULENAME);
//    connect(this,SIGNAL(ruleEdited(qint32)),this,SLOT(on_sysRuleCreated(int)));

//    emit createCall2NumRule();

//}

//void RedirectListController::on_sysRuleCreated(int position)
//{
//    disconnect(this,SIGNAL(ruleEdited(qint32)),this,SLOT(on_sysRuleCreated(int)));
//    if (position >= _rulesList.size())
//        return;

//    RedirectRule& r = _rulesList[position];
//    emit createSysProfile(r.name());
//}

//void RedirectListController::on_sysProfileCreated(int position)
//{
//    if (position >= _profilesList.size())
//        return;

//    RedirectProfile& r = _profilesList[position];
//}


} //namespace ccappcore
