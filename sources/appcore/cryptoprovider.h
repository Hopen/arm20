#ifndef CRYPTOPROVIDER_H
#define CRYPTOPROVIDER_H

#include "appcore.h"
#include "iappcryptoprovider.h"

namespace ccappcore
{

class Message;

class APPCORE_EXPORT CryptoProvider
    : public QObject
    , public IAppCryptoProvider
{
    Q_OBJECT
    Q_INTERFACES(ccappcore::IAppCryptoProvider)
public:
    explicit CryptoProvider(QObject *parent = 0);
    ~CryptoProvider();

public slots:
    void reqPublicKey();

public: //IAppCryptoProvider

    virtual bool canEncryptRSA() const;
    virtual QByteArray encryptRSA(const QByteArray&) const;

    virtual bool canDecryptRSA() const;
    virtual QByteArray decryptRSA(const QByteArray&) const;

signals:   
    void publicKeyReady();

private:
    class Private;
    Private* d;

private slots:
    void on_c2oGetPublickKey(const ccappcore::Message&);
};

}
#endif // CRYPTOPROVIDER_H
