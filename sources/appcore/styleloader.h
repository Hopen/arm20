#ifndef STYLELOADER_H
#define STYLELOADER_H

#include "appcore.h"

namespace ccappcore
{
    class APPCORE_EXPORT StyleLoader: public QObject
    {
        Q_OBJECT
    public:
        StyleLoader(QObject* parent=0);
        void load(QWidget* widget);

    private:
        QString loadStyleSheet(QObject*);
        QString loadStyleSheet(const QString& name);
    };
}
#endif // STYLELOADER_H
