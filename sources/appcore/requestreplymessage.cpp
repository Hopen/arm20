#include "requestreplymessage.h"
#include "irouterclient.h"

namespace ccappcore
{

volatile int RequestReplyMessage::_lastQueryId = 0;


RequestReplyMessage* RequestReplyMessage::send (
        const Message& request,
        const Message& responsePrototype,
        const QObject* handler /*= 0*/,
        const char* replySlot /*= 0*/,
        const char* errorSlot /*= 0*/,
        const Message& errorPrototype /*= Message()*/
        ) throw()
{
    RequestReplyMessage* requestReply =
            create(request,responsePrototype,handler,replySlot,errorSlot,errorPrototype);
    requestReply->send();
    return requestReply;
}

RequestReplyMessage* RequestReplyMessage::create (
            const Message& request,
            const Message& responsePrototype,
            const QObject* handler /*= 0*/,
            const char* replySlot /*= 0*/,
            const char* errorSlot /*= 0*/,
            const Message& errorPrototype /*= Message()*/
        ) throw()
{
    RequestReplyMessage* messageObserver = new RequestReplyMessage(
            request,
            responsePrototype,
            errorPrototype
            );

    messageObserver->connect(messageObserver,
                             SIGNAL(responseReceived(ccappcore::Message)),
                             SLOT(deleteLater()));

    if(handler)
    messageObserver->connect(messageObserver,
                             SIGNAL(responseReceived(ccappcore::Message)),
                             handler,
                             replySlot);

    if(errorSlot)
    messageObserver->connect(messageObserver,
                             SIGNAL(error(ccappcore::Message)),
                             handler,
                             errorSlot);
    messageObserver->connect(messageObserver,
                             SIGNAL(error(ccappcore::Message)),
                             SLOT(deleteLater()));

    return messageObserver;
}


RequestReplyMessage::RequestReplyMessage(const Message& request,
                                         const Message& responsePrototype,
                                         const Message& errorPrototype
                                         )
    : _request(request)
    , _responsePrototype(responsePrototype)
    , _errorPrototype(errorPrototype)
    , _requestQueryId(0)
{

}

RequestReplyMessage::~RequestReplyMessage()
{

}

void RequestReplyMessage::send()
{
    _requestQueryId = ++_lastQueryId;
    _request["query_id"] = _requestQueryId;
    _responsePrototype["query_id"] = _requestQueryId;

    IRouterClient* routerClient = ServiceProvider::instance().getService<IRouterClient>();
    Q_ASSERT(routerClient!=NULL);
    if(!routerClient)
        return;

    connect(&_timeoutTimer, SIGNAL(timeout()), SLOT(responseTimeout()));
    connect(routerClient,
            SIGNAL(messageReceived(ccappcore::Message,ccappcore::ClientAddress,ccappcore::ClientAddress)),
            SLOT(messageReceived(ccappcore::Message,ccappcore::ClientAddress,ccappcore::ClientAddress)));

    connect(routerClient,
            SIGNAL(routeError(ccappcore::Message)),
            SLOT(routeError(ccappcore::Message)));

    _futureInterface = QFutureInterface<void>( (QFutureInterfaceBase::State)
             (QFutureInterfaceBase::Running | QFutureInterfaceBase::Started) );

    _timeoutTimer.start( routerClient->getResponseTimeout() );
    routerClient->send(_request);
}

void RequestReplyMessage::messageReceived(const Message& m,
                                          const ClientAddress& /*from*/,
                                          const ClientAddress& /*to*/)
{
    if(m.name()!=_responsePrototype.name())
        return;

    int responseQueryId = m["query_id"].value().toInt();
    if( m.like(_responsePrototype) && ( responseQueryId == _requestQueryId) )
    {
        emit responseReceived(m);
        stopMessageProcessing();
        return;
    }

    if( (responseQueryId == _requestQueryId && _errorPrototype.name().isEmpty())
        || m.like(_errorPrototype))
    {
        stopMessageProcessing();
        emit error(m);
    }
}

void RequestReplyMessage::routeError(const Message& m)
{
    if(m.name()!=_request.name())
        return;

    int query_id = m["query_id"].value().toInt();
    if( query_id == _requestQueryId )
    {
        Message errorMsg = _errorPrototype;
        errorMsg["error"] = tr("Ошибка доставки сообщения. Убедитесь в правильности настроек соединения.");
        stopMessageProcessing();
        emit error(errorMsg);
    }
}

void RequestReplyMessage::responseTimeout()
{
    qWarning()<<"Response timeout, query_id="<<_requestQueryId;
    Message errorMsg = _errorPrototype;
    errorMsg["error"] = tr("Истекло время ожидания ответа от сервера.");
    stopMessageProcessing();
    emit error(errorMsg);
}

void RequestReplyMessage::stopMessageProcessing()
{
    _timeoutTimer.stop();
    disconnect(this, SLOT(messageReceived(ccappcore::Message,
                                          ccappcore::ClientAddress,
                                          ccappcore::ClientAddress)));
}


}
