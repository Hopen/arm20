#ifndef INDICATORLISTMODEL_H
#define INDICATORLISTMODEL_H

#include "appcore.h"
#include "quantativeindicator.h"


namespace ccappcore
{

typedef QList<QPointer<QuantativeIndicator> > IndicatorList;

class APPCORE_EXPORT IndicatorListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit IndicatorListModel(QObject *parent = 0);

    int addRow(const QString& headerDisplayText,
                const QVariant& associatedObject,
                const IndicatorList& rowData );
    virtual int rowCount(const QModelIndex &parent) const;
    virtual int columnCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;
    QPointer<QuantativeIndicator> indicatorByIndex(const QModelIndex&) const;

signals:

public slots:

private:

    typedef QMap<int, QVariant> HeaderData;
    QList<IndicatorList> _rows;
    QList<HeaderData>    _verticalHeaderData;
    int                  _colCount;
};

}

#endif // INDICATORLISTMODEL_H
