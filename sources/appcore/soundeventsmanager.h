#ifndef SOUNDEVENTS_H
#define SOUNDEVENTS_H

#include "appcore.h"
#include <QSound>
#include "appsettings.h"

namespace ccappcore {

class SoundEventsManager;
class APPCORE_EXPORT SoundEvent: public QObject
{
    Q_OBJECT
public:
    explicit SoundEvent(const QString& name,
                        const QString& descr,
                        SoundEventsManager* owner);

    const QString& name() const { return _name; }
    const QString& description() const { return _description; }
    const QString& file() const { return _soundFile; }

    void setFile(const QString& value) { _soundFile = value; }

    int loops() const { return _loops; }
    void setLoops(int loops) { _loops = loops; }

    SoundEventsManager* owner() { return qobject_cast<SoundEventsManager*>(parent()); }

public slots:
    void play();
    void stop();

private:
    friend class SoundEvents;

    QString _name;
    QString _description;
    QString _soundFile;
    QSharedPointer<QSound> _sound;    
    int _loops;
};

class APPCORE_EXPORT SoundEventsManager : public QObject
        , public IAppSettingsHandler
{
    Q_OBJECT
    Q_INTERFACES(ccappcore::IAppSettingsHandler)
public:
    explicit SoundEventsManager(QObject *parent = 0);

    bool registerEvent(const QString& name, const QString& descr);
    bool connectPlayEvent(const QString& name, QObject* sender, const char* signal);
    bool connectStopEvent(const QString& name, QObject* sender, const char* signal);
    void playDtmf(const QString& dtmf);

    QList<QPointer<SoundEvent> > allEvents() const { return _events.values(); }
    QPointer<SoundEvent> operator[](const QString& name) const { return _events[name]; }

    bool soundsEnabled() const { return _enabled; }
    void setSoundsEnabled(bool enabled) { _enabled = enabled; }

public://IAppSettingsHandler

    virtual void applySettings(const AppSettings&);
    virtual void flushSettings(AppSettings&);

signals:

private:
    QMap<QString, QPointer<SoundEvent> > _events;
    bool _enabled;

};

} // namespace appcore

#endif // SOUNDEVENTS_H
