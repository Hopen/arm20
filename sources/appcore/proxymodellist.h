#ifndef PROXYMODELLIST_H
#define PROXYMODELLIST_H

#include "appcore.h"
#include "proxymodelshared.h"

namespace ccappcore
{

class APPCORE_EXPORT ProxyModelList : public QAbstractItemModel
{
    Q_OBJECT
public:
    ProxyModelList(QObject *parent = 0);

public:
    QModelIndex index(int row, int column,
                              const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &child) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    QVariant data(const QModelIndex& index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    QStringList mimeTypes() const;
    QMimeData *mimeData(const QModelIndexList &indexes) const;
    bool dropMimeData(const QMimeData *data, Qt::DropAction action,
                              int row, int column, const QModelIndex &parent);
    Qt::DropActions supportedDropActions() const;

    const ModelList& sourceModelList() const { return _sourceModels; }
    void setSourceModelList(const ModelList& source);

    void removeModel(ModelPtr model);
    void insertModel(ModelPtr model, int pos);

    ModelPtr modelAt(int row) const;
    ModelPtr modelPtr(const QAbstractItemModel*) const;
public:
    QModelIndex mappedIndexFromSourceIndex(const QModelIndex& index) const;
    QModelIndex sourceIndexFromMappedIndex(const QModelIndex& index) const;

private:
    void connectModel(ModelPtr model);

    ModelList _sourceModels;
    mutable QMap<QModelIndex, QModelIndex> _sourceIndexMap;
    mutable QMap<QModelIndex, QModelIndex> _mappedIndexMap;
    mutable int _nextIndexId;
    void* getNextIndexId() const { return (void*)++_nextIndexId; }

    int beginRow(const QAbstractItemModel* model) const;
    int endRow(const QAbstractItemModel* model) const;

private slots:

    void sourceModelLayoutAboutToBeChanged();
    void sourceModelLayoutChanged();

    void sourceModelDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);
    void sourceModelReset();

    void sourceModelRowsAboutToBeInserted(const QModelIndex &parent, int first, int last);
    void sourceModelRowsInserted(const QModelIndex &parent, int first, int last);

    void sourceModelRowsAboutToBeRemoved(const QModelIndex &parent, int first, int last);
    void sourceModelRowsRemoved(const QModelIndex &parent, int first, int last);
};

}
#endif // PROXYMODELLIST_H
