#include "presetwidget.h"
#include <QDomDocument>
#include <QDockWidget>
#include <QMainWindow>

#include "presetmanager.h"

namespace ccappcore {

PresetWidget::PresetWidget(QObject* m)
    : _manager(static_cast<PresetManager*>(m))
{
    setObjectName(QUuid::createUuid().toString());
}

PresetWidget::~PresetWidget()
{
}

PresetManager* PresetWidget::presetManager()
{
    return _manager;
}

bool PresetWidget::readElement(PresetManager* manager, const QDomElement& el, PresetWidget** result)
{
    if(el.nodeName() != "widget")
    {
        qWarning()<<"PresetWidget::readElement() - unexpected element "
                <<" at line "<<el.lineNumber()<<", column"<<el.columnNumber();
        return false;
    }

    QString className = el.attribute("className");
    const QMetaObject* metaObject = manager->findMetaObject(className);
    if(!metaObject)
    {
        qWarning()<<"PresetManager::load() - unknown className '"<< className <<"'"
                <<" at line "<<el.lineNumber()<<", column"<<el.columnNumber();
        return false;
    }
    PresetWidget* widget = qobject_cast<PresetWidget*>(metaObject->newInstance(Q_ARG(QObject*,manager)));
    if(!widget)
    {
        qWarning()<<"PresetManager::load() - unable to create PresetWidget instance"
                <<" at line "<<el.lineNumber()<<", column"<<el.columnNumber();
        return false;
    }

    QString objectName = el.attribute("objectName");
    widget->setObjectName(objectName);
    if(!widget->readElement(el))
    {
        delete widget;
        return false;
    }
    *result = widget;
    return true;
}

bool PresetWidget::appendElement(PresetManager*, QDomElement& parent, PresetWidget* widget)
{
    QDomElement el = parent.ownerDocument().createElement("widget");
    el.setAttribute("className", widget->metaObject()->className());
    el.setAttribute("objectName", widget->objectName());
    widget->writeElement(el);
    parent.appendChild(el);
    return true;
}

void PresetWidget::destroyWidget()
{
    qDebug() << tr("PresetWidget::destroyWidget()");
    widget()->setAttribute(Qt::WA_DeleteOnClose);
    widget()->close();
}

DockWidget::DockWidget(QObject* m)
    : PresetWidget(m)
{
}

QWidget* DockWidget::widget()
{
    return _dockWidget;
}

bool DockWidget::writeElement(QDomElement& el)
{    
    Q_ASSERT(_dockWidget);
    if(!_dockWidget)
        return false;

    el.setAttribute("windowTitle", _dockWidget->windowTitle());
    el.setAttribute("floating", QVariant(_dockWidget->isFloating()).toString());
    el.setAttribute("visible", QVariant(_dockWidget->isVisible()).toString());

    return PresetWidget::appendElement(_manager, el, _presetWidget);
}

bool DockWidget::readElement(const QDomElement& el)
{
    _data["visible"] = el.attribute("visible");
    _data["floating"] = el.attribute("floating");
    _data["windowTitle"] = el.attribute("windowTitle");

    PresetWidget* result = NULL;
    bool bOk = PresetWidget::readElement(_manager, el.firstChild().toElement(), &result);
    _presetWidget = result;
    return bOk;
}

void DockWidget::create(PresetWidget* docketWidget)
{
    Q_ASSERT(!_dockWidget);
    Q_ASSERT(_manager->mainWindow());
    _presetWidget = docketWidget;

    _dockWidget = new QDockWidget(_manager->mainWindow());
    _dockWidget->setObjectName(this->objectName());
    _dockWidget->setAttribute(Qt::WA_DeleteOnClose);
    this->setParent(_dockWidget);

    _dockWidget->setSizePolicy( QSizePolicy::Ignored, QSizePolicy::Ignored );
    _dockWidget->setAllowedAreas( Qt::AllDockWidgetAreas );
    _dockWidget->setFeatures( QDockWidget::DockWidgetClosable
                    |QDockWidget::DockWidgetMovable
                    |QDockWidget::DockWidgetFloatable );

    _dockWidget->setWidget(_presetWidget->widget());
}

QDockWidget* DockWidget::dockWidget()
{
    return _dockWidget;
}

void DockWidget::restoreWidget()
{
    _presetWidget->restoreWidget();
    create(_presetWidget);
    _dockWidget->setVisible(_data["visible"].toBool());
    _dockWidget->setFloating(_data["floating"].toBool());
    _dockWidget->setWindowTitle(_data["windowTitle"].toString());
}

} // namespace ccappcore
