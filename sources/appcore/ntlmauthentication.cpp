#include "ntlmauthentication.h"
#include "requestreplymessage.h"
#include "sspiwrapper.h"
#include "sessioncontroller.h"

using namespace SSPIWrapper;

namespace ccappcore
{

class NTLMAuthentication::Private
{
public:
    Private()
        : package( new SecurityPackage(SecurityPackage::szNTLMPackageName) )
        , clientCredential( new ClientCredential(package) )
        , clientContext( new ClientContext(clientCredential, L"",ClientContext::None) )
    {

    }

    SecurityPackagePtr  package;
    ClientCredentialPtr clientCredential;
    ClientContextPtr    clientContext;
};

NTLMAuthentication::NTLMAuthentication(QObject* parent)
    : INtlmLogonProvider(parent)
    , p(NULL)
{
}

NTLMAuthentication::~NTLMAuthentication()
{
    if(p)
        delete p;
}

QString NTLMAuthentication::queryUserName()
{
    if(p)
        delete p;
    p = new Private();
    QString userName = QString::fromStdWString(
            p->clientCredential->queryUserNameAttribute()
            );
    return userName;
}

void NTLMAuthentication::startAuthentication()
{
    try
    {
        if(p)
            delete p;
        p = new Private();
        QString userName = QString::fromStdWString(
                p->clientCredential->queryUserNameAttribute()
                );

        if(!p->clientContext->continueProcessing())
        {
            qWarning()<<"NTLMAuthentication::startAuthentication()"
                    " - error while starting challenge-response";
            emit authenticationCompleted();
            return;
        }

        QByteArray token1( (const char*) &(p->clientContext->token()[0]), p->clientContext->token().size() );

        messageFilter.startListening(router.instance(),
                                     Message("C2O_NTLM_CHALLENGE_REQUEST"),
                                     this,
                                     SLOT(on_c2oNtlmChallengeRequest(ccappcore::Message)));

        Message o2cLogin("O2C_LOGIN_REQ");
        o2cLogin["cmd"] = 1000;
        o2cLogin["app_id"] = sessionController->sessionTypeString();
        o2cLogin["sender_oper_id"] = -1;
        o2cLogin["hw_type"] = sessionController->deviceType();
        o2cLogin["phone"] = sessionController->deviceNumber();
        o2cLogin["login"] = userName;
        o2cLogin["pass"] = "";
        o2cLogin["forced"] = false;
        o2cLogin["ntlm_token"] = token1;

        Message c2oLogin("C2O_LOGIN");
        c2oLogin["sql_result"] = 0;

        RequestReplyMessage::send(o2cLogin, c2oLogin, //request and response prototype
                                  sessionController.instance(),
                                  SLOT(on_c2oLoginSucceded(ccappcore::Message)),
                                  SLOT(on_c2oLoginFailed(ccappcore::Message)));
    }
    catch(SSPIException& e)
    {
        qWarning()<<"NTLMAuthentication::on_c2o_authResponse() - SSPIException thrown."
                <<"error_code="<<e.errorCode()<<", what = "<<e.what();

        emit authenticationCompleted();
        return;
    }
}

void NTLMAuthentication::on_c2oNtlmChallengeRequest(const ccappcore::Message& response)
{
    try
    {
        Q_ASSERT(p && p->clientContext);
        if(response.contains("error"))
        {
            messageFilter.stopListening();
            emit authenticationCompleted();
            return;
        }

        QByteArray serverToken = response["token"];
        SSPIWrapper::ByteArray serverTokenConverted(serverToken.constBegin(), serverToken.constEnd());
        p->clientContext->initialize(serverTokenConverted);
        QByteArray token( (const char*) &(p->clientContext->token()[0]), p->clientContext->token().size() );
        Message challengeResponse("O2C_NTLM_CHALLENGE_RESPONSE");
        challengeResponse.add("token",token);
        router->send(challengeResponse);
    }
    catch(SSPIException& e)
    {
        qWarning()<<"NTLMAuthentication::on_c2o_authResponse() - SSPIException thrown."
                <<"error_code="<<e.errorCode()<<", what = "<<e.what();

        emit authenticationCompleted();
        return;
    }
}


}
