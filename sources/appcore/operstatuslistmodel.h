#ifndef OPERSTATUSLISTMODEL_H
#define OPERSTATUSLISTMODEL_H

#include "appcore.h"
#include "operlistcontroller.h"
#include "filteredmodel.h"

namespace ccappcore {

class APPCORE_EXPORT OperStatusListModel
    : public QAbstractListModel
    , public FilteredModel<ccappcore::Contact>
{
Q_OBJECT
public:

    explicit OperStatusListModel(QObject *parent = 0);

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;

    virtual int columnCount(const QModelIndex &parent) const;

    virtual QVariant data(const QModelIndex &index,
                          int role = Qt::DisplayRole) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation,
                                int role = Qt::DisplayRole) const;

    //QString statusAsString(Contact::ContactStatus status) const;

public slots:
    void reset();
    virtual void applyFilter(const PredicateGroup<Contact>& filter);
private:
    LateBoundObject<OperListController> _operListController;
    QMap<Contact::ContactStatus, int> _data;
};

} // namespace ccappcore

#endif // OPERSTATUSLISTMODEL_H
