#ifndef APPURLHANDLER_H
#define APPURLHANDLER_H

#include "appcore.h"
#include "serviceprovider.h"

namespace ccappcore
{

class APPCORE_EXPORT AppUrlHandler : public QObject
{
Q_OBJECT
public:
    explicit AppUrlHandler( QObject *parent = 0 );

public slots:
    void openUrl(const QUrl& url);
};

}

#endif // APPURLHANDLER_H
