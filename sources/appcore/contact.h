#ifndef CCAPPCORE_CONTACT_H
#define CCAPPCORE_CONTACT_H

#include "appcore.h"

namespace ccappcore {

class Call;
class ContactsController;

class APPCORE_EXPORT ContactId: public QVariant
{
    ContactsController* _controller;
    int _type;
public:

    int type() const { return _type; }
    ContactsController* controller() const;
    ContactId()
    {}

    ContactId(ContactsController* controller, const QVariant& id, int type = 0);

    inline bool operator==(const ContactId& other) const
    {
        return controller() != NULL &&
               controller() == other.controller() &&
               type() == other.type() &&
               QVariant::operator == (other);
    }
};

class APPCORE_EXPORT Contact
{
public:

    enum ContactInfo
    {
        Name             =1,
        LongName         =2,
        Status           =3,
        StatusTime       =4,
        StatusReason     =5,
        Email            =6,
        Phone            =7,
        OfficePhone      =8,
        MobilePhone      =9,
        Fax              =10,
        Address          =11,
        Organization     =12,
        JobPosition      =13,
        Location         =14,
        BareJid          =15,
        //BookName         =16,
        UserData         =1000,
    };

    enum ContactStatus
    {
        UnknownStatus = -1,
        Free = 0,
        Busy = 1,
        Paused = 2,
        Offline = 3//,
        //ByNumber,
        //ByVoicemail
    };

    typedef QMultiMap<ContactInfo,QVariant> ExtraInfo;

    Contact(); //constructs invalid contact
    Contact( const ContactsController* controller, const QVariant& id );
    Contact( const ContactId& id );

    static const Contact& invalid() { static Contact invalidContact; return invalidContact; }
    bool isValid() const;
    const ContactId& contactId() const;
    ContactsController* controller() const { return contactId().controller(); }

    QUrl buildUrl() const;
    QString name() const;
    ContactStatus status() const;
    QDateTime statusTime() const;
    QString statusReason() const;
    bool childrenLoaded() const;
    const QList<Contact>& children() const;
    bool isContainer() const;
    bool isParentOf(const Contact& c) const { return children().contains(c); }
    const ccappcore::Contact& findChild(const ContactId& id) const;

    bool isFree        () const { return status() == Free; }
    bool isBusy        () const { return status() == Busy; }
    bool isPaused      () const { return status() == Paused; }
    bool isOffline     () const { return status() == Offline; }
    bool isOnline      () const { return status() != Offline; }
    //bool isOnNumber    () const { return status() == ByNumber; }
    //bool isOnVoicevail () const { return status() == ByVoicemail;}

    Contact::ExtraInfo contactInfo() const;
    QList<QVariant> dataAsList(int infoId) const;
    QVariant data(int infoId) const;
    QString infoName(int infoId) const;

    bool canMakeCall() const;
    bool canTransferCall(const ccappcore::Call&) const;
    bool canSendMessage()const;

    virtual bool operator == (const Contact& other) const;
    virtual bool operator != (const Contact& other) const;
    virtual bool operator <  (const Contact& other) const;

public:
    void setIsContainer(bool val);
    void setName(const QString& name);
    void setStatus(ContactStatus status);
    void setStatusTime(const QDateTime&);
    void setStatusReason(const QString& reason);

    void addData(int type, const QVariant& value);
    void setData(int type, const QVariant& value);

    void setChildren(const QList<Contact>&);
    void addChild(const Contact&);
    void setChildrenLoaded(bool b);

private:
    struct Private
    {
        Private(const ContactId& id);
        Private(ContactsController* c, const QVariant& id);
        ContactId               contactId;
        bool                    isContainer;
        bool                    childrenLoaded;
        QString                 name;
        ExtraInfo               extraInfo;
        QList<Contact>          children;
    };

    QSharedPointer<Contact::Private> d;
};

} // namespace ccappcore

Q_DECLARE_METATYPE(ccappcore::Contact);

#endif // CCAPPCORE_CONTACT_H
