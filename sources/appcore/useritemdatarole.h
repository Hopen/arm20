#ifndef USERITEMDATAROLE_H
#define USERITEMDATAROLE_H

#include "appcore.h"

namespace ccappcore
{

enum UserItemDataRole
{
    AssociatedObjectRole    = Qt::UserRole +1,
    StateTimeRole           = Qt::UserRole +2,
    StateReasonRole         = Qt::UserRole +3,
    AddressTypeRole         = Qt::UserRole +4,
    AddressValueRole        = Qt::UserRole +5,
};

}



#endif // USERITEMDATAROLE_H
