#include "domainobjectfilter.h"

namespace ccappcore {

bool IsSessionOwnerCall::operator ()(const Call& call)const
{
    Call::CallState callState = call.callState();
    if( callState<Call::CS_Assigned || callState >= Call::CS_CallFailed )
        return false;

    if(call.operId() == _callsController->sessionOwner().contactId())
        return true;

    Call linkedCall = _callsController->findByCallId(call.operCallId());
    if(linkedCall.operId() == _callsController->sessionOwner().contactId())
        return true;

    return false;
}

bool IsQueuedCall::operator ()(const Call& call)const
{
    if( !call.isQueued() )
        return false;

    if(!_privateGroupsOnly)
        return true;

    bool result = _callsController->isOwnGroupCall(call);
    return result;
}

//test::test()
//{

//}

} //namespace ccappcore
