#include "appsettings.h"

#include <QSettings>
#include <QDebug>

#include "requestreplymessage.h"
#include "imessageserializer.h"
#include "lateboundobject.h"
#include "operlistcontroller.h"
#include "irouterclient.h"
#include "operationcontroller.h"

namespace ccappcore
{

//static QByteArray emptydata("invalid data");
static QByteArray emptydata("");

    class AppSettings::Private
    {
    public:
        QScopedPointer<QSettings> appSettings;
        QScopedPointer<QSettings> userSettings;

    };

    AppSettings::AppSettings( QObject *parent /*= 0 */)
        : QObject(parent)
        , d(new Private())
    {
        QString appName = QCoreApplication::applicationName();
        QString orgName = QCoreApplication::organizationName();

        if(appName.isEmpty())
            appName = QFileInfo(qApp->applicationFilePath()).baseName();

        if(orgName.isEmpty())
            orgName = "forte-it";

        d->appSettings.reset(new QSettings(appName+".ini",  QSettings::IniFormat ) );

        d->userSettings.reset(new QSettings(QSettings::IniFormat,
                                            QSettings::UserScope,
                                            orgName,
                                            appName));

        d->appSettings->setIniCodec(QTextCodec::codecForLocale());
        d->userSettings->setIniCodec(QTextCodec::codecForLocale());
    }

    void AppSettings::flushSettings()
    {
        TSpecificFunctor<AppSettings, IAppSettingsHandler*> flushFunc(this, &AppSettings::flushSettings);
        ServiceProvider::instance().forEach(flushFunc);
    }

    void AppSettings::applySettings()
    {
        TSpecificFunctor<AppSettings, IAppSettingsHandler*> flushFunc(this, &AppSettings::applySettings);
        ServiceProvider::instance().forEach(flushFunc);
    }

    void AppSettings::applySettings(IAppSettingsHandler* service)
    {
        service->applySettings(*this);
    }

    void AppSettings::flushSettings(IAppSettingsHandler* service)
    {
        service->flushSettings(*this);
    }

    void AppSettings::saveSettings()
    {
        flushSettings();
        saveAppSettings();
        saveUserSettings();
    }

    void AppSettings::loadSettings()
    {
        loadAppSettings();
        loadUserSettings();
    }

    void AppSettings::loadAppSettings()
    {
        qDebug()<<"AppSettings::loadAppSettings() from "<<(*d->appSettings).fileName();
        d->appSettings->sync();
    }

    void AppSettings::saveAppSettings()
    {
        qDebug()<<"AppSettings::saveAppSettings() to "<<(*d->appSettings).fileName();
        d->appSettings->sync();
    }

    void AppSettings::loadUserSettings()
    {
        qDebug()<<"AppSettings::loadUserSettings() from "<<(*d->userSettings).fileName();
        d->userSettings->sync();
    }

    void AppSettings::saveUserSettings()
    {
        qDebug()<<"AppSettings::saveUserSettings() to "<<(*d->userSettings).fileName();
        d->userSettings->sync();
    }

    QVariant AppSettings::getOption(const QString& name, const QVariant& defaultValue) const
    {        
        if(d->appSettings->contains(name))
            return d->appSettings->value(name);

        if(d->userSettings->contains(name))
            return d->userSettings->value(name);

        return defaultValue;
    }

    void AppSettings::setAppOption(const QString& name,
                   const QVariant& value )
    {
        d->appSettings->setValue(name, value);
    }

    void AppSettings::setUserOption(const QString& name,
                   const QVariant& value  )
    {

        if( d->appSettings->contains(name) )
        {
            //overridden on application settings level
            setAppOption(name, value);
            return;
        }
        d->userSettings->setValue(name, value);
    }


    QFuture<void> AppSettings::loadServerSettings(ccappcore::Contact loadForOper)
    {
        _loadSettingsResult = QFutureInterface<void>( (QFutureInterfaceBase::State)
                (QFutureInterfaceBase::Started | QFutureInterfaceBase::Running)
                );

        _serverOptions.clear();

        Message request("O2C_GET_SETTINGS");
        request["oper_id"] = loadForOper.contactId().toInt();
        request["object_type"] = "SETTINGS";
        request["object"] = "OPERATOR";
        request["setting_type"] = "Value";
        //request["setting"] = ""; //all settings

        /*RequestReplyMessage* r = */RequestReplyMessage::send(request,
                                  Message("C2O_GET_SETTINGS")
                                  , this, SLOT(onLoadServerSettings(ccappcore::Message)));
        //return r->future<void>();
        return _loadSettingsResult.future();
    }

    QFuture<void> AppSettings::saveServerSettings(ccappcore::Contact saveForOper)
    {
        _loadSettingsResult = QFutureInterface<void>( (QFutureInterfaceBase::State)
                (QFutureInterfaceBase::Started | QFutureInterfaceBase::Running)
                );

//        QFutureInterfaceBase d;
//        QFuture<void> r(&d);
//        return r;
        Message request("O2C_SET_SETTINGS");
        request["oper_id"] = saveForOper.contactId().toInt();
        QStringList allkeys = d->userSettings->allKeys();
        QList<Message> params_list;
        foreach (const QString& key, allkeys)
        {
            Message msg;
            msg["oper_id"] = saveForOper.contactId().toInt();
            msg["object_type"] = "SETTINGS";
            msg["object"] = "OPERATOR";
            msg["setting_type"] = "Value";
            msg["setting"]      =/* tr("opertoolsform/operations");*/key;
            QVariant value = getOption(key/*tr("opertoolsform/operations")*/,QVariant(QVariant::Invalid));
            if (value.isValid())
            {
                switch(value.type())
                {
                case QVariant::Bool:
                case QVariant::Int:
                {
                    msg["int_val"] = value.toInt();
                    msg["string_val"] = tr("");
                    msg["float_val"] = 0.0;
                    msg["binary_val"] = emptydata;
                    break;
                }
                case QVariant::String:
                {
                    msg["int_val"] = 0;
                    msg["string_val"] = /*tr("\"") + */value.toString() /*+ tr("\"")*/;
                    msg["float_val"] = 0.0;
                    msg["binary_val"] = emptydata;
                    break;
                }
                case QVariant::ByteArray:
                {
                    // ToDo: do not support this type
                    continue;
                }
                case QVariant::StringList:
                case QVariant::List: // QVariant -> QByteArray
                {
                    QByteArray data;
                    QBuffer buffer(&data);
                    buffer.open(QIODevice::ReadWrite);

                    QDataStream stream(&buffer);
                    stream.setByteOrder(QDataStream::LittleEndian);
                    stream << value;
                    buffer.close();
                    stream.unsetDevice();

                    msg["int_val"] = 0;
                    msg["string_val"] = tr("");
                    msg["float_val"] = 0.0;
                    msg["binary_val"] = data;
                    break;
                }
                case QVariant::Double:
                {
                    msg["float_val"] = value.toDouble();
                    msg["int_val"] = 0;
                    msg["string_val"] = tr("");
                    msg["binary_val"] = emptydata;
                    break;
                }
                default:
                {
                    continue;
                }
                }
            }
            else
            {
                continue; // that's OK; for example an empty QStringList saved like @invalid; just skip it
            }
            params_list.push_back(msg);
        }
        LateBoundObject< IMessageSerializer > serilizer;
        QByteArray data;
        serilizer->saveList(data, params_list);
        request["data"] = data;
        request["data"].setFlag(MessageParam::F_CDS_TABLE);

        _router->send(request,ClientAddress::Any());
        return _loadSettingsResult.future();

    }

    void AppSettings::onLoadServerSettings(const ccappcore::Message& m)
    {
        _loadSettingsResult.reportStarted();

        LateBoundObject< IMessageSerializer > serilizer;
        QList<Message> list = serilizer->loadList(m["data"]);
        foreach(const Message& m, list)
        {
            qDebug()<<m;
            QString name = m["setting"].value().toString();
            QVariant old_value = getOption(name,QVariant(QVariant::Invalid));
            if (old_value.isValid())
            {
                QVariant value;
                switch(old_value.type())
                {
                case QVariant::Int:
                {
                    value = m[ "int_val"    ].value().toInt();
                    break;
                }
                case QVariant::String:
                {
                    value = m[ "string_val" ].value().toString();
                    break;
                }
                //case QVariant::ByteArray:
                case QVariant::List: //opertools
                case QVariant::StringList: // status/pause_reason
                {
                    QByteArray byteValue = m[ "binary_val" ].value().toByteArray();
                    QBuffer buffer(&byteValue);
                    buffer.open(QIODevice::ReadOnly);

                    QDataStream stream(&buffer);
                    stream.setByteOrder(QDataStream::LittleEndian);
                    //QVariant variantValue;
                    //stream >> variantValue;
                    //QVariantList valuelist = variantValue.toList();
                    //value = variantValue;
                    stream >> value;

                    buffer.close();
                    stream.unsetDevice();
                    break;
                }
                case QVariant::Double:
                {
                    value = m[ "float_val"  ].value().toDouble();
                    break;
                }
                case QVariant::Bool:
                {
                    value = m[ "string_val"].value().toBool();
                    break;
                }
                default:
                {
                    continue;
                }
                }

                //setUserOption(name,value);
                _serverOptions.push_back(TOption(name, value));
            }
        }

        _loadSettingsResult.reportFinished();
    }

    void AppSettings::applyCDSSettings()
    {
        foreach (const TOption& option, _serverOptions)
        {
            setUserOption(option._name,option._value);
        }
    }

    void AppSettings::applySetting(const QString &name)
    {
        foreach (const TOption& option, _serverOptions)
        {
            if (option._name == name)
            {
                setUserOption(option._name,option._value);
                break;
            }
        }
    }

//    void AppSettings::onUpdateLanguage(QString text)
//    {
//        emit switchSessionLanguage(text);
//    }


//    void AppSettings::test()
//    {
//        QVariant value = getOption(tr("opertoolsform/operations"),QVariant(QVariant::Invalid));
//        QByteArray data;

//        if (1)
//        {
//            QBuffer buffer(&data);
//            buffer.open(QIODevice::ReadWrite);

//            QDataStream stream(&buffer);
//            stream.setByteOrder(QDataStream::LittleEndian);

//            stream << value;
//            buffer.close();
//            stream.unsetDevice();

//        }

//        if (1)
//        {
//            QBuffer buffer(&data);
//            buffer.open(QIODevice::ReadOnly);

//            QDataStream stream(&buffer);
//            stream.setByteOrder(QDataStream::LittleEndian);

//            QVariant var1;
//            stream >> var1;
//            QVariantList valuelist = var1.toList();

//            buffer.close();
//            stream.unsetDevice();
//        }
//    }
}
