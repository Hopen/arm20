#include "message.h"
#include "../thirdparty/cds3/consts.h"

namespace ccappcore
{
        Message::Message()
//#if (_CDSVER >= 0x0400)
//#else
//            :_cmd(-1)
//#endif
	{
	}

        Message::Message(const QString& name)
		: m_name(name)
	{
//#if (_CDSVER >= 0x0400)
//#else
//            _cmd = CmdConverter::GetInstance()->GetCmdByString(name.toStdString());
//            addCmd();

//#endif
	}
        Message::~Message()
        {
        }

//#if (_CDSVER >= 0x0400)
//#else
//        Message::Message(const Message& rhs) // copy ctor
//        {
//            m_name   = rhs.m_name;
//            m_params = rhs.m_params;
//            _cmd     = rhs._cmd;
//            addCmd();
//        }
//#endif
        bool Message::isValid() const
        {
            return !m_name.isEmpty();
        }

        const MessageParam* Message::find(const QString& paramName) const
	{
                QList<MessageParam>::const_iterator i = m_params.begin();
		for(;i!=m_params.end();i++)
		{
			if( 0 == (*i).name().compare(QString(paramName),Qt::CaseInsensitive) )
			{
				return &(*i);
			}
		}

		return NULL;
	}

        MessageParam& Message::operator[] (const char* paramName)
	{
                const MessageParam* pParam = find(paramName);
		if(pParam!=NULL)
                        return (MessageParam&)*pParam;

                m_params.append(MessageParam(paramName));
		return m_params.last();
	}

        const MessageParam& Message::at(int paramIndex) const
        {
                if( m_params.count() <= paramIndex)
                {
                    qWarning()<<"Message parameters index "<<paramIndex
                            <<" is out of valid range "<<m_params.count();

                    return MessageParam::invalid;
                }

                return m_params[paramIndex];
        }

        const MessageParam& Message::operator[](int paramIndex) const
        {
            return at(paramIndex);
        }

        const MessageParam& Message::operator[](const char* paramName) const
	{
                const MessageParam* pParam = find(paramName);
			if(pParam!=NULL)
                                return (MessageParam&)*pParam;

                qWarning()<<"Parameter "<<paramName<<" not found";

                return MessageParam::invalid;
        }

        bool Message::operator == (const Message& other) const
	{
		if(this == &other)
			return true;

		if(name()!=other.name())
			return false;

		if(paramsCount()!=other.paramsCount())
			return false;

                for(quint32 i =0;i<paramsCount();i++)
			if(m_params[i]!=other[i])
				return false;

		return true;
	}


        quint32 Message::getHashCode() const
	{
		quint32 hash = 0;
		QByteArray nameData = m_name.toAscii();

		for(int i = 0; i < m_name.length(); i++)
		{
			hash = ((hash << 5) + hash + nameData[i]);
		}

		return hash;
	}

        bool Message::like(const Message& prototype) const
        {
            if(name()!=prototype.name())
                return false;

            QList<MessageParam>::const_iterator i = prototype.params().begin();
            for(;i!=prototype.params().end();i++)
            {
                const MessageParam* thisParam = this->find(i->name());
                if( i->value().isValid() && *thisParam != *i )
                    return false;
            }
            return true;
        }

        Message& Message::add(const char* paramName, const QVariant& paramValue)
        {
            (this->operator [](paramName)).setValue(paramValue);
            return *this;
        }

        quint16 Message::size()const
        {
            quint16 size = 0;
            size += sizeof(quint32); //size of keys quantity
            for(uint i=0; i < paramsCount(); i++)
            {
                size += sizeof (quint32); // size of name size
                size += m_params[i].name().length(); // size of name
                size += m_params[i].size(); // size of value
            }

            return size;

        }

        quint16 Message::addCmd()const
        {
            if (!find("cmd"))
            {
                qint32 cmd = CmdConverter::GetInstance()->GetCmdByString(name().toStdString());
                MessageParam cmd_param("cmd");
                cmd_param.setValue(cmd);
                m_params.push_back(cmd_param);
                return cmd;
            }
            return 0;
//            //add("cmd",cmd);
//            (this->operator []("cmd")).setValue(cmd);
//            //m_params.push_back();
        }


}
