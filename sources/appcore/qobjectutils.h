#ifndef QOBJECTUTILS_H
#define QOBJECTUTILS_H

#include "appcore.h"
#include <QAction>

template<class TObject, class TContainer>
TObject* qObjectFind(const TContainer& c, const QString& name)
{
    foreach(TObject* o, c)
    {
        if(o->objectName() == name)
            return o;
    }
    return NULL;
}

template<class TContainer>
QAction* qActionFind(const TContainer& actions, const QString& name)
{
    foreach(QAction* a, actions)
    {
        if(a->objectName() == name)
            return a;
        
        if(a->menu() && a->menu()->objectName()==name)
            return a;        
    }
    
    return NULL;
}

#endif // QOBJECTUTILS_H
