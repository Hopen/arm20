#ifndef CONTROLLERBOUNDMODEL_H
#define CONTROLLERBOUNDMODEL_H

#include "appcore.h"
#include "serviceprovider.h"

namespace ccappcore
{
template< class TObject > class LateBoundObject
{
public:
    LateBoundObject()
        : _instance(NULL)
    {
    }

    TObject* instance()
    {
        if(_instance==NULL)
            _instance = ServiceProvider::instance().getService<TObject>();

        Q_ASSERT(_instance!=NULL);
        return _instance;
    }

    const TObject* instance() const
    {
        return const_cast<LateBoundObject<TObject>* >(this)->instance();
    }

    bool isValid() const
    {
        return _instance != NULL ||
                ServiceProvider::instance().getService<TObject>()!=NULL;
    }

    inline TObject* operator&() { return instance(); }
    inline TObject* operator ->() { return instance(); }
    inline TObject& operator*() { return *instance(); }
    inline const TObject* operator&() const { return instance(); }
    inline const TObject* operator ->() const { return instance(); }
    inline const TObject& operator*() const { return *instance(); }

private:
    TObject* _instance;
};

}
#endif // CONTROLLERBOUNDMODEL_H
