#ifndef PRESETMANAGER_H
#define PRESETMANAGER_H

#include "appcore.h"
#include "appsettings.h"
#include <QMainWindow>
#include "layoutmanager.h"
#include "lateboundobject.h"

class QMainWindow;

namespace ccappcore {

class PresetWidget;

class APPCORE_EXPORT PresetManager
    : public QObject
{
    Q_OBJECT
public:
    explicit PresetManager(QObject *parent = 0);

    template<class T> void registerMetaObject()
    {
        _metaObjects.insert( T::staticMetaObject.className(), &(T::staticMetaObject) );
    }

    const QMetaObject* findMetaObject(const QString& className);

    QMainWindow* mainWindow() { return _mainWindow; }
    void setMainWindow(QMainWindow* w);

    void addWidget(PresetWidget* widget);
    void removeWidget(QObject* widget);

    void addDockedWidget(PresetWidget* widget, const QString& title);

    bool load(QIODevice*);
    bool save(QIODevice*);

public slots:
    void restoreWidgets();
    void destroyWidgets();

public: //IAppSettingsHandler
    virtual void load();
    virtual void save();

private slots:
    void presetWidgetDestroyed(QObject*);

private:
    QPointer<QMainWindow> _mainWindow;
    QList<QObject*>        _widgets;
    LayoutManager::LayoutSettings _layoutSettings;
    QHash<QByteArray,const QMetaObject*> _metaObjects;
};

} // namespace ccappcore

#endif // PRESETMANAGER_H
