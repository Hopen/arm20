#include "timeutils.h"
namespace ccappcore {

TimeUtils::TimeUtils()
{

}

QTime TimeUtils::timeElapsed(const QDateTime& from, const QDateTime& to)
{
    int secs = from.secsTo(to);
    QTime result = QTime(0,0,0,0).addSecs(secs);
    return result;
}

QString TimeUtils::SecsToLongHMS(int totalSeconds)
{
    int stateMinutes = totalSeconds / 60;
    int stateHours = totalSeconds / ( 60 * 60 );

    if( stateHours > 0 )
        return QObject::tr("%1 ч. %2 мин.").arg(stateHours).arg(stateMinutes);

    if(stateMinutes == 0)
        return QString();

    return QObject::tr("%1 мин.").arg(stateMinutes);
}

QString TimeUtils::SecsToShortHMS(int totalSeconds)
{
    QTime time = QTime(0,0,0,0).addSecs(totalSeconds);
    if(time.hour()>0)
        return time.toString("HH:mm:ss");

    return time.toString("mm:ss");
}

QString TimeUtils::minutesString(int minutes)
{
    if(minutes<1)
        return QObject::tr("менее минуты");

    if(minutes==1)
        return QObject::tr("1 минута");

    if( minutes < 5 )
        return QString(QObject::tr("%1 минуты")).arg(minutes);

    if( minutes <= 20 )
        return QString(QObject::tr("%1 минут")).arg(minutes);

    if( minutes % 10 == 0 )
        return QString(QObject::tr("%1 минут")).arg(minutes);

    if( minutes % 10 == 1 )
        return QString(QObject::tr("%1 минута")).arg(minutes);

    if( minutes % 10 < 5 )
        return QString(QObject::tr("%1 минуты")).arg(minutes);

    return QString(QObject::tr("%1 минут")).arg(minutes);
}


} // namespace ccappcore
