#ifndef SERVICEPROVIDER_H
#define SERVICEPROVIDER_H

#include "appcore.h"
#include "functor.h"

namespace ccappcore
{
    class APPCORE_EXPORT ServiceProvider: public QObject
    {
        Q_OBJECT;
    public:
        ServiceProvider(QObject* parent = 0);
        virtual ~ServiceProvider() {}

        static ServiceProvider& instance();

        template<class T> inline T* getService() const throw();
        template<class T> inline QList<T*> findServices() const throw();
        template<class T> inline void registerService(T* pService) throw();
        template<class T> inline void unregisterService();
        template<class TService> inline void forEach(TFunctor<TService>& func) const;
        template<class TService, class TFunc> inline void forEach(TFunc func) const;

        const QList<QObject*>& getServices() { return _services; }

    private slots:
        void serviceDestroyed(QObject* obj);

    private:
        QList<QObject*> _services;
    };

    template<class T>
    T* ServiceProvider::getService() const throw()
    {
        for(int i=0; i<_services.count(); i++)
        {
            QObject* pObj = _services.at(i);
            T* pService = qobject_cast<T*>(pObj);
            if(pService!=NULL)
                return pService;
        }
        return NULL;
    }

    template<class T>
    QList<T*> ServiceProvider::findServices() const throw()
    {
        QList<T*> list;
        for(int i=0; i<_services.count(); i++)
        {
            QObject* pObj = _services.at(i);
            T* pService = qobject_cast<T*>(pObj);
            if(pService!=NULL)
                list.push_back(pService);
        }
        return list;
    }

    template<class TService>
    void ServiceProvider::forEach(TFunctor<TService>& func) const
    {
        for(int i=0; i<_services.count(); i++)
        {
            QObject* pObj = _services.at(i);
            TService pService = qobject_cast<TService>(pObj);
            if(pService!=NULL)
                func(pService);
        }
    }

    template<class TService, class TFunc>
    void ServiceProvider::forEach(TFunc func) const
    {
        for(int i=0; i<_services.count(); i++)
        {
            QObject* pObj = _services.at(i);
            TService* pService = qobject_cast<TService*>(pObj);
            if(pService!=NULL)
                func(pService);
        }
    }


    template<class T>
    void ServiceProvider::registerService(T* pService) throw()
    {
        if(pService==NULL)
        {
            qWarning()<<"ServiceProvider::registerService() failed -null argument.";
            return;
        }

        connect(pService, SIGNAL(destroyed(QObject*)), this, SLOT(serviceDestroyed(QObject*)));
        _services.push_back(pService);
    }

    template<class T>
    void ServiceProvider::unregisterService()
    {
        T* pService = NULL;
        do
        {
            pService = getService<T>;
            if(pService!=NULL)
                _services.removeOne(pService);
        }
        while( NULL!=pService );
    }
}

#endif // SERVICEPROVIDER_H
