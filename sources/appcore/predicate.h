#ifndef PREDICATE_H
#define PREDICATE_H

#include <QPointer>
#include <QSharedPointer>
#include <QList>

namespace ccappcore
{

template <class TArg> class Predicate
{
public:
  typedef bool result_type;
  class Ptr
  {
        const Predicate<TArg>* _ptr;
  public:
        Ptr(const Predicate<TArg>* ptr) : _ptr(ptr) {}

        virtual bool operator()(const TArg& arg) const { return (*_ptr)(arg); }
        virtual bool operator()(const TArg* arg) const { return (*_ptr)(*arg); }

        const Predicate<TArg>& operator*() const { return *_ptr; }

        friend class SharedPtr;
  };
  
  class SharedPtr: public QSharedPointer<const Predicate<TArg> >
  {
  public:
      SharedPtr(const Predicate<TArg>* ptr)
          : QSharedPointer<const Predicate<TArg> >(ptr) {}

      virtual bool operator()(const TArg& arg) const { return (*this)(arg); }
      virtual bool operator()(const TArg* arg) const { return (*this)(*arg); }
      const Predicate<TArg>::Ptr operator()() const { return Predicate<TArg>::Ptr(&*this); }

  };

  virtual ~Predicate() {}
  virtual bool operator()(const TArg& arg) const = 0;
  virtual bool operator()(const TArg* arg) const { return (*this)(*arg); }
};

template <class TArg> class PredicateGroup: public Predicate<TArg>
{
public:
    bool matches( const TArg* arg ) const { return this->matches(*arg); }
    bool matches( const TArg& arg ) const
    {
        QListIterator<typename Predicate<TArg>::SharedPtr> i(_group);
        while(i.hasNext())
        {
            typename Predicate<TArg>::SharedPtr predicate = i.next();
            if( !predicate.isNull() )
            {
                if(!(*predicate)(arg))
                    return false;
            }
        }
        return true;
    }

    virtual bool operator() (const TArg& arg) const
    {
        return matches(arg);
    }

    void add(typename Predicate<TArg>::SharedPtr predicate) { _group.append(predicate); }
    void remove(typename Predicate<TArg>::SharedPtr predicate) { _group.removeOne(predicate); }
    void clear() { _group.clear(); }
    bool isEmpty() const { return _group.isEmpty(); }

private:
    QList<typename Predicate<TArg>::SharedPtr> _group;
};

template <class TClass, class TArg> class SpecificPredicate
    : public Predicate<TArg>
{
private:
  QPointer<TClass> _instance;
  bool (TClass::*funcPtr)( const TArg* );

public:
 SpecificPredicate(TClass* objectPtr, bool(TClass::*funcPtr)( const TArg* ))
    : _instance(objectPtr)
    , funcPtr(funcPtr)
 {
     Q_CHECK_PTR(_instance);
 }

  virtual bool operator() ( const TArg* arg) const
  {
      return (_instance->*funcPtr)(arg);
  }
};

}

#endif // PREDICATE_H
