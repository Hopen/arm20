/*
 * Message.h
 *
 *  Created on: 17.02.2009
 *      Author: root
 */

#ifndef Message_H_
#define Message_H_

#include "appcore.h"
#include "messageparam.h"

namespace ccappcore
{
        class Message;

        class APPCORE_EXPORT Message {

        private:
		QString m_name;
                mutable QList<MessageParam> m_params;

	public:
                Message();
                explicit Message(const QString& name);
//#if (_CDSVER >= 0x0400)
//#else
//                Message(const Message& rhs);
//#endif

                virtual ~Message();

                bool isValid() const;

                inline const QString& name() const { return m_name; }
                inline void setName(const QString& name) { m_name = name; }

                inline const QList<MessageParam>& params() const { return m_params; }
                inline void setParams(const QList<MessageParam>& params) { m_params = params; }


                const MessageParam* find(const QString& paramName) const;
                quint32 paramsCount() const { return m_params.size(); }

		quint32 getHashCode() const;

                MessageParam& operator[](const char* paramName);
                const MessageParam& operator[](const char* paramName) const ;
                const MessageParam& operator[](int paramIndex) const;
                const MessageParam& at(int paramIndex) const;

                bool operator == (const Message& other) const;

                //returns true if the prototype message is a subset of this message
                bool like(const Message& prototype) const;
                //returns true if the message contains paramenetr with specified name
                bool contains(const QString& paramName) const { return NULL!=find(paramName); }

                Message& add(const char* paramName, const QVariant& paramValue);
#if (_CDSVER >= 0x0400)
#else
                quint16 size()const;
//private:
                //quint32 _cmd;
                quint16 addCmd()const;
#endif
        };

        inline QDebug operator<<(QDebug out, const Message& m)
        {
             return out.nospace()<<"\t"<<m.name()<<"\t"<<m.params();
        }
}

Q_DECLARE_METATYPE(ccappcore::Message);

#endif /* Message_H_ */
