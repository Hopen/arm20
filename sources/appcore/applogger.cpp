#include "applogger.h"

#include "log4qt/rollingfileappender.h"
#include "log4qt/varia/debugappender.h"
#include "log4qt/logger.h"
#include "log4qt/ttcclayout.h"

#include "appsettings.h"

using namespace Log4Qt;

namespace ccappcore
{

void qMessageHandler(QtMsgType type, const char *msg)
{
     QString data = QString(msg).trimmed();
     switch (type) {
     case QtDebugMsg:
         Logger::logger("qDebug")->debug(data);
         break;
     case QtWarningMsg:
         Logger::logger("qWarning")->warn(data);
         break;
     case QtCriticalMsg:
         Logger::logger("qCritical")->error(data);
         break;
     case QtFatalMsg:
         Logger::logger("qFatal")->fatal(data);
         abort();
     }
}

AppLogger::AppLogger(QObject* parent)
        : ILogger(parent)
        , layout(new TTCCLayout())
        , fileAppender( new RollingFileAppender() )
{
    Log4Qt::Logger::rootLogger()->addAppender(fileAppender);

    DebugAppender *p_appender = new DebugAppender(layout);
    p_appender->setName(QLatin1String("debugAppender"));
    p_appender->activateOptions();
    Log4Qt::Logger::rootLogger()->addAppender(p_appender);   
}

void AppLogger::applySettings(const AppSettings& settings)
{
    QString file = settings.getOption("logging/fileName", "").toString();
    QString levelString = settings.getOption("logging/logLevel", "OFF").toString();
    Level logLevel = Level::fromString(levelString);

    layout->setName("defaultLayout");
    layout->setDateFormat(TTCCLayout::ISO8601);
    layout->activateOptions();

    fileAppender->setName("fileAppender");
    fileAppender->setLayout(layout);
    fileAppender->setFile(file);
    fileAppender->setAppendFile(true);
    fileAppender->activateOptions();

    Log4Qt::Logger::rootLogger()->setLevel(logLevel);
    if(Log4Qt::Logger::rootLogger()->effectiveLevel() == Level::OFF_INT)
        qInstallMsgHandler(0);
    else
        qInstallMsgHandler(&qMessageHandler);
}

LogLevel::Value AppLogger::effectiveLevel() const
{
    return static_cast<ccappcore::LogLevel::Value>(Logger::rootLogger()->level().toInt());
}

void AppLogger::write(const QString& logger, LogLevel::Value level, const QString& msg)
{
    Logger::logger(logger)->log((Log4Qt::Level::Value)level, msg);
}

void AppLogger::mdcSet(const QString& /*key*/, const QVariant& /*value*/)
{
}

const QVariant& AppLogger::mdcGet(const QString& /*key*/) const
{
    static QVariant notImpl(QString("Not implemented"));
    return notImpl;
}

void AppLogger::mdcRemove(const QString& /*key*/)
{
}

void AppLogger::ndcPush(const QString& /*value*/)
{
}

void AppLogger::ndcPop()
{
}

void AppLogger::ndcClear()
{

}


}
