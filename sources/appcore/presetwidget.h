#ifndef PRESETWIDGET_H
#define PRESETWIDGET_H

#include "appcore.h"
#include "domnodelistiterator.h"

class QDomElement;
class QWidget;
class QDockWidget;


namespace ccappcore
{

class PresetManager;

class APPCORE_EXPORT PresetWidget: public QObject
{
    Q_OBJECT;
public:
    explicit PresetWidget(QObject*);
    virtual ~PresetWidget();

    PresetManager* presetManager();
public:
    virtual bool writeElement(QDomElement& parent) = 0;
    virtual bool readElement(const QDomElement&) = 0;

    virtual void restoreWidget() = 0;
    virtual void destroyWidget();

    virtual QWidget* widget() = 0;

    static bool readElement(PresetManager* manager, const QDomElement&, PresetWidget**);
    static bool appendElement(PresetManager* manager, QDomElement& el, PresetWidget*);

protected:
    QPointer<PresetManager> _manager;
    QMap<QString, QVariant> _data;
};

class APPCORE_EXPORT DockWidget: public PresetWidget
{
    Q_OBJECT;

public:
    QDockWidget* dockWidget();
    void create(PresetWidget* docketWidget);
public:
    Q_INVOKABLE explicit DockWidget(QObject*);

    virtual bool writeElement(QDomElement&);
    virtual bool readElement(const QDomElement&);

    virtual void restoreWidget();

    virtual QWidget* widget();
protected:
    QPointer<QDockWidget> _dockWidget;
    QPointer<PresetWidget> _presetWidget;
};

} // namespace ccappcore

#endif // PRESETWIDGET_H
