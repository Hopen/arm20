#ifndef ILOGGER_H
#define ILOGGER_H

#include "appcore.h"
#include "serviceprovider.h"

namespace ccappcore
{
    class LogLevel
    {

        private: LogLevel() {}

        public: enum Value
        {
            NotSpecified    = 0,
            Trace           = 64,
            Debug           = 96,
            Info            = 128,
            Warning         = 150,
            Error           = 182,
            Fatal           = 214,
            Disabled        = 0xFF,
        };
    };

    class APPCORE_EXPORT ILogger: public QObject
    {
        Q_OBJECT;
        public:
        ILogger(QObject* parent = 0) : QObject(parent) {}
        virtual ~ILogger() {}

        virtual LogLevel::Value effectiveLevel() const = 0;
        virtual void write(const QString& logger, LogLevel::Value level, const QString& ) = 0;

        virtual void mdcSet(const QString& key, const QVariant& value) = 0;
        virtual const QVariant& mdcGet(const QString& key) const = 0;
        virtual void mdcRemove(const QString& key) = 0;

        virtual void ndcPush(const QString& value) = 0;
        virtual void ndcPop() = 0;
        virtual void ndcClear() = 0;
    };
}

#endif // ILOGGER_H
