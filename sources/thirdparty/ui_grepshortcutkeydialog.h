/********************************************************************************
** Form generated from reading UI file 'grepshortcutkeydialog.ui'
**
** Created: Mon 25. Mar 16:02:44 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GREPSHORTCUTKEYDIALOG_H
#define UI_GREPSHORTCUTKEYDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GrepShortcutKeyDialog
{
public:
    QHBoxLayout *hboxLayout;
    QLineEdit *shortcutPreview;
    QPushButton *pushButton;

    void setupUi(QWidget *GrepShortcutKeyDialog)
    {
        if (GrepShortcutKeyDialog->objectName().isEmpty())
            GrepShortcutKeyDialog->setObjectName(QString::fromUtf8("GrepShortcutKeyDialog"));
        GrepShortcutKeyDialog->setWindowModality(Qt::ApplicationModal);
        GrepShortcutKeyDialog->resize(255, 50);
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(0), static_cast<QSizePolicy::Policy>(0));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(GrepShortcutKeyDialog->sizePolicy().hasHeightForWidth());
        GrepShortcutKeyDialog->setSizePolicy(sizePolicy);
        GrepShortcutKeyDialog->setContextMenuPolicy(Qt::NoContextMenu);
        hboxLayout = new QHBoxLayout(GrepShortcutKeyDialog);
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        shortcutPreview = new QLineEdit(GrepShortcutKeyDialog);
        shortcutPreview->setObjectName(QString::fromUtf8("shortcutPreview"));
        shortcutPreview->setFocusPolicy(Qt::NoFocus);
        shortcutPreview->setAlignment(Qt::AlignHCenter);
        shortcutPreview->setReadOnly(true);

        hboxLayout->addWidget(shortcutPreview);

        pushButton = new QPushButton(GrepShortcutKeyDialog);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setFocusPolicy(Qt::NoFocus);

        hboxLayout->addWidget(pushButton);


        retranslateUi(GrepShortcutKeyDialog);
        QObject::connect(pushButton, SIGNAL(clicked()), GrepShortcutKeyDialog, SLOT(close()));

        QMetaObject::connectSlotsByName(GrepShortcutKeyDialog);
    } // setupUi

    void retranslateUi(QWidget *GrepShortcutKeyDialog)
    {
        pushButton->setText(QApplication::translate("GrepShortcutKeyDialog", "Cancel", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(GrepShortcutKeyDialog);
    } // retranslateUi

};

namespace Ui {
    class GrepShortcutKeyDialog: public Ui_GrepShortcutKeyDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GREPSHORTCUTKEYDIALOG_H
