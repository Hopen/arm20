#include "consts.h"
#include "common_defs.h"

//const char *g_szCallsFields[] = {
//  "call_id",
//  "priority",
//  "b_num",
//  "a_num",
//  "group_id",
//  "begin",
//  "extra_info",
//  "oper_id",
//  "call_type",
//  NULL
//};

//const char *g_szLinksFields[] = {
//  "call_id1",
//  "call_id2",
//  "routed",
//  NULL
//};

//const char *g_szConfFields[] = {
//  "conf_id",
//  "call_id",
//  "oper_id",
//  "status",
//  "owner",
//  NULL,
//};

//const char *g_szCallFailed[] = {
//  "",
//  "������� �����",
//  "�������� �� ��������",
//  "����� �� ����������",
//  "",
//  "",
//  NULL,
//};

//---------------------------------------------------------------------------
char * GetStringByCmd(int dwCmd) {

static char *szC2O[] = {
    "C2O_LOGIN",
    "C2O_OPER_LIST",
    "C2O_PERSON_LIST",
    "C2O_GROUP_LIST",
    "C2O_UPDATE_OPER_STATUS",

    "C2O_QUEUE_CALL",
    "C2O_REMOVE_QUEUE_CALL",
    "C2O_INCOMING_CALL",
    "C2O_CALL_INFO",
    "C2O_CALL_RESULT",
      
    "C2O_END_CALL",
    "C2O_QUEUE_CALL_LIST",
    "C2O_GET_CALLBACK_CALLS",
    "C2O_BIND_CALLBACK",
    "C2O_CALL_ASSIGNED",

    "C2O_SCRIPT_EVENT",
    "C2O_CALL_DENIED",
    "C2O_PERSON_INFO",
    "C2O_BIND_PERSON_INFO",
    "C2O_SEND_MSG",

    "C2O_MSGS_LIST",
    "C2O_MSG_HISTORY",
    "C2O_BB_LIST",
    "C2O_BB_GROUPS",
    "C2O_BB_CHANGE_NOTIFY",

    "C2O_GET_QUESTIONNAIRE",
    "C2O_NEW_CALLBACK",
    "C2O_OPER_CLIENTS",
    "C2O_CREATE_CLIENT",
    "C2O_CREATE_CLIENT_CONTACT",

    "C2O_SET_CALLBACK_STATUS",
    "C2O_QUESTIONNAIRE_LIST",
    "C2O_QUESTIONNAIRE_ANSWERS",
    "C2O_CALL_HISTORY",
    "C2O_GET_REDIR_PROFILES", 

    "C2O_GET_REDIRECTIONS",
    "C2O_CREATE_REDIR_TASK",
    "C2O_CREATE_REDIR_PROFILE",
    "C2O_XZ", 
    "C2O_ZX",

    "C2O_RING_STARTED",
    "C2O_QUICKPHONE_LIST",
    "C2O_QUICKPHONE_INFO",
    "C2O_QUICKPHONE_ADD",
    "C2O_HUNG",

    "C2O_NEXT_QUESTION",
    "C2O_VOICEMAIL_LIST",
    "C2O_NOTIFY_NEW_VOICEMAIL",
    "C2O_CREATE_CONF",
    "C2O_NEW_REQUEST",

    "C2O_GET_REQUEST",
    "C2O_BIND_REQUEST_REQ",
    "C2O_SET_REQUEST_STATUS",
    "C2O_NEW_REQUEST_2",
    "C2O_GET_TEMPLATES", 
      
    "C2O_GET_OPER_IN_GROUPS",
    "C2O_GET_NOTE_BY_NUMBER",
    "C2O_JOIN_TO_CONF",
    "C2O_REMOVE_FROM_CONF",
    "C2O_REMOVE_CONF",

    "C2O_SET_PARTICIPANT_STATUS",
    "C2O_INIT_CALL",
    "C2O_CONF_INFO",
    "C2O_COMPLETE_CALL",
    "C2O_SEARCH_CONTACTS",
    
    "C2O_DELETE_CLIENT",
    "C2O_DELETE_CLIENT_CONTACT",
    "C2O_MUTE",
    "C2O_HOLD",
    "C2O_RECORD",

    "C2O_GET_FILE",      // +70
    "C2O_PUT_FILE",      // +71
    "C2O_GET_FILE_INFO", // +72
    "C2O_ASSIGN_ATTRIBUTE", // +73
    "C2O_GET_ATTRIBUTE", // +74

    "C2O_DELETE_ATTRIBUTE", // +75
    "C2O_GET_SETTINGS",
    "",
    "",
    "",

    "",
    "",
    "",
    "",
    "",
 
    "",
    "",
    "",
    "",
    "",

    "",
    "",
    "",
    "",
    "",

    "",
    "",
    "",
    "",
    "",

    "C2O_CALLSTATE_INFO",
	
	"C2O_NTF_TASK_CHANGED",
	"C2O_GET_NTF_TASKS",
	"C2O_GET_NTF_PHONES",
	"C2O_GET_NTF_PERIODS",
	"C2O_LOGIN_OPERATOR",
	"C2O_DISCONNECT_CLIENT",
	"C2O_SYSTEM_INFO",
	"C2O_ADD_NTF_RETRY",
        "C2O_ALARM",
        0
};


static char *szO2C[] = {
    "O2C_LOGIN_REQ",
    "O2C_SET_OPERATOR_STATUS",
    "O2C_MAKE_CALL_REQ",
    "O2C_MAKE_CALL_OP_REQ",
    "O2C_TRANSFER_GROUP",

    "O2C_TRANSFER_PHONE",
    "O2C_TRANSFER_SCRIPT",
    "O2C_CONNECT",
    "O2C_SET_MUTE",
    "O2C_SET_HOLD",

    "O2C_SET_RECORD",
    "O2C_END_CALL_REQ",
    "O2C_OPER_LIST_REQ",
    "O2C_PERSON_LIST_REQ",
    "O2C_GROUP_LIST_REQ",

    "O2C_ADD_EXTRA_INFO",
    "O2C_SET_PRIORITY",
    "O2C_QUEUE_CALL_LIST_REQ",
    "O2C_EXTRA_INFO_REQ",
    "O2C_ROUTE_REQ",

    "O2C_REJECT_CALL",
    "O2C_GET_CALLBACK_CALLS",
    "O2C_BIND_CALLBACK_REQ",
    "O2C_ANSWER_CALL",
    "O2C_SCRIPT_EVENT_REQ",

    "O2C_PERSON_INFO_REQ",
    "O2C_BIND_PERSON",
    "O2C_BIND_PERSON_INFO_REQ",
    "O2C_SET_CALLBACK_STATUS",
    "O2C_GET_EXTRA_CALL",

    "O2C_SEND_MSG_REQ",
    "O2C_MSGS_LIST_REQ",
    "O2C_MSG_HISTORY_REQ",
    "O2C_BB_CREATE",
    "O2C_BB_EDIT",

    "O2C_BB_DELETE",
    "O2C_BB_LIST_REQ",
    "O2C_BB_GROUPS_REQ",
    "O2C_GET_QUESTIONNAIRE_REQ",
    "O2C_QUESTIONNAIRE_ANSWER",

    "O2C_OPER_CLIENTS_REQ",
    "O2C_CREATE_CLIENT",
    "O2C_EDIT_CLIENT",
    "O2C_CREATE_CLIENT_CONTACT",
    "O2C_EDIT_CLIENT_CONTACT",

    "O2C_AUTODIAL_RESULT",
    "O2C_QUESTIONNAIRE_LIST",
    "O2C_QUESTIONNAIRE_ANSWERS",
    "O2C_CALL_HISTORY",
    "O2C_GET_REDIR_PROFILES",

    "O2C_GET_REDIRECTIONS",
    "O2C_CREATE_REDIR_TASK",
    "O2C_EDIT_REDIR_TASK",
    "O2C_DELETE_REDIR_TASK",
    "O2C_SET_REDIR_PROFILE",

    "O2C_CREATE_REDIR_PROFILE",
    "O2C_EDIT_REDIR_PROFILE",
    "O2C_CREATE_CALLBACK",
    "O2C_QUICKPHONE_LIST",
    "O2C_QUICKPHONE_INFO",

    "O2C_QUICKPHONE_ADD",
    "O2C_QUICKPHONE_EDIT",
    "O2C_QUICKPHONE_DELETE",
    "O2C_QUICKPHONE_ORDERNUM",
    "O2C_CUSTOM_QUERY",

    "O2C_NEXT_QUESTION_REQ",
    "O2C_SET_QUESTIONNAIRE_RESULT",
    "O2C_SUBSCRIBE_CMD",
    "O2C_UNSUBSCRIBE_CMD",
    "O2C_BROADCAST_MSG",

    "O2C_VOICEMAIL_LIST_REQ",
    "O2C_RUN_SCRIPT",
    "O2C_VOICEMAIL_SET_STATUS",
    "O2C_CREATE_CONF_REQ",
    "O2C_JOIN_TO_CONF",

    "O2C_REMOVE_FROM_CONF",
    "O2C_REMOVE_CONF",
    "O2C_SET_PARTICIPANT_STATUS",
    "O2C_NEW_REQUEST",
    "O2C_GET_REQUEST",

    "O2C_BIND_REQUEST_REQ",
    "O2C_SET_REQUEST_STATUS",
    "O2C_DELETE_REDIR_PROFILE",
    "O2C_SET_REQUEST_COMMENT",
    "O2C_GET_TEMPLATES", 
      
    "O2C_GET_OPER_IN_GROUPS",
    "O2C_GET_NOTE_BY_NUMBER",
    "O2C_GET_CONF_INFO",
    "O2C_SEARCH_CONTACTS",
    "O2C_DELETE_CLIENT",

    "O2C_DELETE_CLIENT_CONTACT",
    "O2C_GET_FILE",
    "O2C_PUT_FILE",
    "O2C_GET_FILE_INFO",
    "O2C_ASSIGN_ATTRIBUTE",

    "O2C_GET_ATTRIBUTE",
    "O2C_DELETE_ATTRIBUTE",
    "O2C_GET_SETTINGS",
    "O2C_SET_SETTINGS",
    "",

    "O2C_CALLSTATE_REQ",
    "O2C_PLAY_DTMF",
	
	"O2C_CREATE_NTF_TASK",
	"O2C_CREATE_NTF_PHONE",
	"O2C_CREATE_NTF_PERIOD",
	
	"O2C_EDIT_NTF_TASK",
	"O2C_EDIT_NTF_PHONE",
	"O2C_EDIT_NTF_PERIOD",
	
	"O2C_DELETE_NTF_TASK",
	"O2C_DELETE_NTF_PHONE",
	"O2C_DELETE_NTF_PERIOD",
	
	"O2C_GET_NTF_TASKS",
	"O2C_GET_NTF_PHONES",
	"O2C_GET_NTF_PERIODS",
	
	"O2C_SET_NTF_STATUS",

	"O2C_DISCONNECT_CLIENT",	
	"O2C_LOGIN_OPERATOR",
	"O2C_SYSTEM_INFO", 				
	"O2C_ADD_NTF_RETRY",
        "O2C_SEND_ALARM",
        0

};

static char *szS2C[] = {
    "S2C_INIT_CALL",
    "S2C_SET_EXTRA_INFO",
    "S2C_SET_PRIORITY",
    "S2C_POST_QUEUE",
    "S2C_CALL_RESULT",

    "S2C_COMPLETE_CALL",
    "S2C_GET_CALL_PARAMS",
    "S2C_SET_CALL_PARAMS",
    "S2C_REGISTER_STARTER",
    "S2C_UID_BY_PIN",
      
    "S2C_AVG_QUEUE_DELAY",
    "S2C_AUTODIAL_RESULT",
    "S2C_REMOVE_QUEUE",
    "S2C_RING_STARTED",
    "S2C_HUNG",

    "S2C_SID_BY_EXT",
    "S2C_CHANNEL_STOPPED",
    "S2C_SET_RECORD_FILE",
    "S2C_GET_CUSTOM_PARAM",
    "S2C_NEW_VOICEMAIL",

    "S2C_MAKE_CALL",
    "S2C_CREATE_CONF",
    "S2C_SET_CUSTOM_PARAM",
    "S2C_CONF_JOINED",
    "S2C_CONF_REMOVED",

    "S2C_CONF_STATUS_CHANGED",
    "S2C_CONF_DELETE",
        0
};

static char *szC2S[] = {
    "C2S_INIT_CALL",
    "C2S_SET_MUTE",
    "C2S_SET_RECORD",
    "C2S_GET_CALL",
    "C2S_END_CALL_REQ",

    "C2S_CALL_PARAMS",
    "C2S_HOLD_ON",
    "C2S_ROUTE",
    "C2S_GOTO_SCRIPT",
    "C2S_MUSIC",                     

    "C2S_STOP_CHANNEL",
    "C2S_UID_BY_PIN",
    "C2S_RUN_OUT_IVR",
    "C2S_OPER_READY",
    "C2S_AVG_QUEUE_DELAY",

    "C2S_SID_BY_EXT",
    "C2S_GET_CUSTOM_PARAM",
    "C2S_CREATE_CONF",
    "C2S_JOIN_TO_CONF",
    "C2S_REMOVE_FROM_CONF",

    "C2S_DELETE_CONF",
    "C2S_SET_PARTICIPANT_STATUS",
    "C2S_PLAY_DTMF",
        0
};

static char *szError = "C2O_ERROR";

  if (dwCmd >= C2O_ORIGIN && dwCmd < O2C_ORIGIN) return szC2O[dwCmd - C2O_ORIGIN];
  if (dwCmd >= O2C_ORIGIN && dwCmd < S2C_ORIGIN) return szO2C[dwCmd - O2C_ORIGIN];
  if (dwCmd >= S2C_ORIGIN && dwCmd < C2S_ORIGIN) return szS2C[dwCmd - S2C_ORIGIN];
  if (dwCmd >= C2S_ORIGIN && dwCmd < C2O_ERROR) return szC2S[dwCmd - C2S_ORIGIN];
  if (dwCmd == C2O_ERROR) return szError;
  return "UNKNOWN";
}



CMDConverter::CMDConverter()
{
        RegisterCmdType(C2O_ORIGIN);
        RegisterCmdType(O2C_ORIGIN);
        RegisterCmdType(S2C_ORIGIN);
        RegisterCmdType(C2S_ORIGIN);
        str2cmd[GetStringByCmd(C2O_ERROR)] = C2O_ERROR;
}
CMDConverter::~CMDConverter()
{
}


int CMDConverter::GetCmdByString(const std::string& strCmd) const {
        cmdmap::const_iterator result = str2cmd.find(strCmd);
        if(result != str2cmd.end()){
                return result->second ;
        }
        return NO_COMMAND;
}
std::string CMDConverter::GetStringByCmd(int dwCmd) const
{
        return ::GetStringByCmd(dwCmd);
}

void CMDConverter::RegisterCmdType(int offset)
{
        int i = 0 ;
        for( const char* p = NULL; NULL != (p = ::GetStringByCmd(offset + i)); i++ ){
                str2cmd[std::string(p)] = offset + i;
        }
}


