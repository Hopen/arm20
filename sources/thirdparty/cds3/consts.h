#ifndef constsH
#define constsH

//#include "..\misc\Types.h"
//#include "..\misc\singleton.h"

template <class T>
class static_singleton
{
public:
        static T * GetInstance()
        {
                static T instance;
                return &instance;
        }

        static unsigned long ReleaseInstance()
        {
                return 0;
        }

};


#include <string>
#include <map>

char * GetStringByCmd(int dwCmd);

//extern const char *g_szCallsFields[];
//extern const char *g_szLinksFields[];
//extern const char *g_szConfFields[];
//extern const char *g_szCallFailed[];


class CMDConverter
{
public:
        CMDConverter();
        ~CMDConverter();

        int GetCmdByString(const std::string& strCmd) const;
        std::string GetStringByCmd(int dwCmd) const ;
private:
        void RegisterCmdType(int offset);
private:
        typedef std::map<std::string, int> cmdmap ;
        cmdmap str2cmd;
};

typedef static_singleton<CMDConverter> CmdConverter;


#endif
