TEMPLATE = lib
TARGET = kdchartuitools
CONFIG(debug, debug|release) {
  !unix:TARGET = kdchartuitoolsd
}

include( ../variables.pri )

# can't do that, man:
# KDCHARTDIR = ..
KDCHARTBASE = ..

LIBS += -L$$KDCHARTBASE/lib -l$$KDCHARTLIB


DEFINES += UITOOLS_BUILD_UITOOLS_LIB

QT += xml 
CONFIG += designer

INCLUDEPATH+= $$KDCHARTBASE/extra_include \
              $$KDCHARTBASE/src \
              .
DEPENDPATH += .

UI_DIR = .

KDAB_EVAL{
  HEADERS += ../evaldialog/evaldialog.h
  SOURCES += ../evaldialog/evaldialog.cpp
  DEFINES += KDAB_EVAL
}


FORMS += KDChartLegendPropertiesWidget.ui \
         KDChartHeaderFooterPropertiesWidget.ui \
         KDChartAxisPropertiesWidget.ui \
         KDChartDiagramPropertiesWidget.ui \
         
HEADERS += KDChartLegendPropertiesWidget.h \
           KDChartLegendPropertiesWidget_p.h \
           KDChartAxisPropertiesWidget.h \
           KDChartAxisPropertiesWidget_p.h \
           KDChartHeaderFooterPropertiesWidget.h \
           KDChartHeaderFooterPropertiesWidget_p.h \
           KDChartWidgetSerializer.h \
           KDChartChartSerializer.h \
           KDChartDiagramPropertiesWidget.h \
           KDChartDiagramPropertiesWidget_p.h \ 
           
SOURCES += KDChartLegendPropertiesWidget.cpp \
           KDChartAxisPropertiesWidget.cpp \
           KDChartHeaderFooterPropertiesWidget.cpp \
           KDChartWidgetSerializer.cpp \
           KDChartChartSerializer.cpp \ 
           KDChartDiagramPropertiesWidget.cpp \
