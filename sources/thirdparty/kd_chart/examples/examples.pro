TEMPLATE	= subdirs
CONFIG += ordered
SUBDIRS		= tools \
                  Widget \
                  DrawIntoPainter \
                  HeadersFooters \
                  Legends \
                  Axis \
                  SharedAbscissa \
                  Zoom \
                  Pie \
                  Bars \
                  Stock \
                  Lines \
                  ModelView \
                  EmptyValues \
                  NoValues \
                  Background \
                  Grids \
                  RealTime \
                  Polar \
                  Plane \
                  TernaryCharts \
                  Plotter \
                  LeveyJennings \
                  DataValueTexts \
                  Gantt
