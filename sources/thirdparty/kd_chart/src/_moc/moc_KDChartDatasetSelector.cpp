/****************************************************************************
** Meta object code from reading C++ file 'KDChartDatasetSelector.h'
**
** Created: Mon 17. Mar 15:11:58 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../KDChartDatasetSelector.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KDChartDatasetSelector.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDChart__DatasetSelectorWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      55,   32,   31,   31, 0x05,
     133,   31,   31,   31, 0x05,

 // slots: signature, parameters, type, tag, flags
     160,  151,   31,   31, 0x0a,
     195,  183,   31,   31, 0x0a,
     221,   31,   31,   31, 0x08,
     256,   31,   31,   31, 0x08,
     288,   31,   31,   31, 0x08,
     323,   31,   31,   31, 0x08,
     355,   31,   31,   31, 0x08,
     390,   31,   31,   31, 0x08,
     428,   31,   31,   31, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_KDChart__DatasetSelectorWidget[] = {
    "KDChart::DatasetSelectorWidget\0\0"
    "rowConfig,columnConfig\0"
    "configureDatasetProxyModel(DatasetDescriptionVector,DatasetDescription"
    "Vector)\0"
    "mappingDisabled()\0rowCount\0"
    "setSourceRowCount(int)\0columnCount\0"
    "setSourceColumnCount(int)\0"
    "on_sbStartColumn_valueChanged(int)\0"
    "on_sbStartRow_valueChanged(int)\0"
    "on_sbColumnCount_valueChanged(int)\0"
    "on_sbRowCount_valueChanged(int)\0"
    "on_cbReverseRows_stateChanged(int)\0"
    "on_cbReverseColumns_stateChanged(int)\0"
    "on_groupBox_toggled(bool)\0"
};

void KDChart::DatasetSelectorWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DatasetSelectorWidget *_t = static_cast<DatasetSelectorWidget *>(_o);
        switch (_id) {
        case 0: _t->configureDatasetProxyModel((*reinterpret_cast< const DatasetDescriptionVector(*)>(_a[1])),(*reinterpret_cast< const DatasetDescriptionVector(*)>(_a[2]))); break;
        case 1: _t->mappingDisabled(); break;
        case 2: _t->setSourceRowCount((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 3: _t->setSourceColumnCount((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 4: _t->on_sbStartColumn_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->on_sbStartRow_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->on_sbColumnCount_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->on_sbRowCount_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->on_cbReverseRows_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->on_cbReverseColumns_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->on_groupBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KDChart::DatasetSelectorWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDChart::DatasetSelectorWidget::staticMetaObject = {
    { &QFrame::staticMetaObject, qt_meta_stringdata_KDChart__DatasetSelectorWidget,
      qt_meta_data_KDChart__DatasetSelectorWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDChart::DatasetSelectorWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDChart::DatasetSelectorWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDChart::DatasetSelectorWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDChart__DatasetSelectorWidget))
        return static_cast<void*>(const_cast< DatasetSelectorWidget*>(this));
    return QFrame::qt_metacast(_clname);
}

int KDChart::DatasetSelectorWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QFrame::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void KDChart::DatasetSelectorWidget::configureDatasetProxyModel(const DatasetDescriptionVector & _t1, const DatasetDescriptionVector & _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void KDChart::DatasetSelectorWidget::mappingDisabled()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE
