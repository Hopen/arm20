/****************************************************************************
** Meta object code from reading C++ file 'KDChartModelDataCache_p.h'
**
** Created: Mon 17. Mar 15:12:38 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../KDChartModelDataCache_p.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KDChartModelDataCache_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDChart__ModelDataCachePrivate__ModelSignalMapperConnector[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      60,   59,   59,   59, 0x09,
      76,   73,   59,   59, 0x09,
     113,   73,   59,   59, 0x09,
     151,  149,   59,   59, 0x09,
     188,   59,   59,   59, 0x09,
     204,   59,   59,   59, 0x09,
     217,   73,   59,   59, 0x09,
     251,   73,   59,   59, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_KDChart__ModelDataCachePrivate__ModelSignalMapperConnector[] = {
    "KDChart::ModelDataCachePrivate::ModelSignalMapperConnector\0"
    "\0resetModel()\0,,\0columnsInserted(QModelIndex,int,int)\0"
    "columnsRemoved(QModelIndex,int,int)\0"
    ",\0dataChanged(QModelIndex,QModelIndex)\0"
    "layoutChanged()\0modelReset()\0"
    "rowsInserted(QModelIndex,int,int)\0"
    "rowsRemoved(QModelIndex,int,int)\0"
};

void KDChart::ModelDataCachePrivate::ModelSignalMapperConnector::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ModelSignalMapperConnector *_t = static_cast<ModelSignalMapperConnector *>(_o);
        switch (_id) {
        case 0: _t->resetModel(); break;
        case 1: _t->columnsInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 2: _t->columnsRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 3: _t->dataChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        case 4: _t->layoutChanged(); break;
        case 5: _t->modelReset(); break;
        case 6: _t->rowsInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 7: _t->rowsRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KDChart::ModelDataCachePrivate::ModelSignalMapperConnector::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDChart::ModelDataCachePrivate::ModelSignalMapperConnector::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_KDChart__ModelDataCachePrivate__ModelSignalMapperConnector,
      qt_meta_data_KDChart__ModelDataCachePrivate__ModelSignalMapperConnector, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDChart::ModelDataCachePrivate::ModelSignalMapperConnector::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDChart::ModelDataCachePrivate::ModelSignalMapperConnector::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDChart::ModelDataCachePrivate::ModelSignalMapperConnector::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDChart__ModelDataCachePrivate__ModelSignalMapperConnector))
        return static_cast<void*>(const_cast< ModelSignalMapperConnector*>(this));
    return QObject::qt_metacast(_clname);
}

int KDChart::ModelDataCachePrivate::ModelSignalMapperConnector::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
