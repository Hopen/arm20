/****************************************************************************
** Meta object code from reading C++ file 'KDChartPolarCoordinatePlane.h'
**
** Created: Mon 17. Mar 15:11:20 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../KDChartPolarCoordinatePlane.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KDChartPolarCoordinatePlane.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDChart__PolarCoordinatePlane[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      39,   31,   30,   30, 0x09,
      75,   30,   30,   30, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_KDChart__PolarCoordinatePlane[] = {
    "KDChart::PolarCoordinatePlane\0\0diagram\0"
    "slotLayoutChanged(AbstractDiagram*)\0"
    "adjustZoomAndRepaint()\0"
};

void KDChart::PolarCoordinatePlane::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PolarCoordinatePlane *_t = static_cast<PolarCoordinatePlane *>(_o);
        switch (_id) {
        case 0: _t->slotLayoutChanged((*reinterpret_cast< AbstractDiagram*(*)>(_a[1]))); break;
        case 1: _t->adjustZoomAndRepaint(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KDChart::PolarCoordinatePlane::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDChart::PolarCoordinatePlane::staticMetaObject = {
    { &AbstractCoordinatePlane::staticMetaObject, qt_meta_stringdata_KDChart__PolarCoordinatePlane,
      qt_meta_data_KDChart__PolarCoordinatePlane, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDChart::PolarCoordinatePlane::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDChart::PolarCoordinatePlane::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDChart::PolarCoordinatePlane::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDChart__PolarCoordinatePlane))
        return static_cast<void*>(const_cast< PolarCoordinatePlane*>(this));
    return AbstractCoordinatePlane::qt_metacast(_clname);
}

int KDChart::PolarCoordinatePlane::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = AbstractCoordinatePlane::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
