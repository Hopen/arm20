/****************************************************************************
** Meta object code from reading C++ file 'kdganttconstraintproxy.h'
**
** Created: Mon 17. Mar 15:10:44 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Gantt/kdganttconstraintproxy.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'kdganttconstraintproxy.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDGantt__ConstraintProxy[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      26,   25,   25,   25, 0x08,
      73,   25,   25,   25, 0x08,
     122,   25,   25,   25, 0x08,
     174,   25,   25,   25, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_KDGantt__ConstraintProxy[] = {
    "KDGantt::ConstraintProxy\0\0"
    "slotSourceConstraintAdded(KDGantt::Constraint)\0"
    "slotSourceConstraintRemoved(KDGantt::Constraint)\0"
    "slotDestinationConstraintAdded(KDGantt::Constraint)\0"
    "slotDestinationConstraintRemoved(KDGantt::Constraint)\0"
};

void KDGantt::ConstraintProxy::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ConstraintProxy *_t = static_cast<ConstraintProxy *>(_o);
        switch (_id) {
        case 0: _t->slotSourceConstraintAdded((*reinterpret_cast< const KDGantt::Constraint(*)>(_a[1]))); break;
        case 1: _t->slotSourceConstraintRemoved((*reinterpret_cast< const KDGantt::Constraint(*)>(_a[1]))); break;
        case 2: _t->slotDestinationConstraintAdded((*reinterpret_cast< const KDGantt::Constraint(*)>(_a[1]))); break;
        case 3: _t->slotDestinationConstraintRemoved((*reinterpret_cast< const KDGantt::Constraint(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KDGantt::ConstraintProxy::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDGantt::ConstraintProxy::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_KDGantt__ConstraintProxy,
      qt_meta_data_KDGantt__ConstraintProxy, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDGantt::ConstraintProxy::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDGantt::ConstraintProxy::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDGantt::ConstraintProxy::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDGantt__ConstraintProxy))
        return static_cast<void*>(const_cast< ConstraintProxy*>(this));
    return QObject::qt_metacast(_clname);
}

int KDGantt::ConstraintProxy::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
