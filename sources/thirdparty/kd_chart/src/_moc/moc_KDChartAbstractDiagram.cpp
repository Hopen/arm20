/****************************************************************************
** Meta object code from reading C++ file 'KDChartAbstractDiagram.h'
**
** Created: Mon 17. Mar 15:11:30 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../KDChartAbstractDiagram.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KDChartAbstractDiagram.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDChart__AbstractDiagram[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      26,   25,   25,   25, 0x05,
      58,   25,   25,   25, 0x05,
      74,   25,   25,   25, 0x05,
      87,   25,   25,   25, 0x05,

 // slots: signature, parameters, type, tag, flags
     107,   25,   25,   25, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_KDChart__AbstractDiagram[] = {
    "KDChart::AbstractDiagram\0\0"
    "layoutChanged(AbstractDiagram*)\0"
    "modelsChanged()\0dataHidden()\0"
    "propertiesChanged()\0setDataBoundariesDirty()\0"
};

void KDChart::AbstractDiagram::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AbstractDiagram *_t = static_cast<AbstractDiagram *>(_o);
        switch (_id) {
        case 0: _t->layoutChanged((*reinterpret_cast< AbstractDiagram*(*)>(_a[1]))); break;
        case 1: _t->modelsChanged(); break;
        case 2: _t->dataHidden(); break;
        case 3: _t->propertiesChanged(); break;
        case 4: _t->setDataBoundariesDirty(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KDChart::AbstractDiagram::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDChart::AbstractDiagram::staticMetaObject = {
    { &QAbstractItemView::staticMetaObject, qt_meta_stringdata_KDChart__AbstractDiagram,
      qt_meta_data_KDChart__AbstractDiagram, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDChart::AbstractDiagram::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDChart::AbstractDiagram::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDChart::AbstractDiagram::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDChart__AbstractDiagram))
        return static_cast<void*>(const_cast< AbstractDiagram*>(this));
    return QAbstractItemView::qt_metacast(_clname);
}

int KDChart::AbstractDiagram::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractItemView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void KDChart::AbstractDiagram::layoutChanged(AbstractDiagram * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void KDChart::AbstractDiagram::modelsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void KDChart::AbstractDiagram::dataHidden()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void KDChart::AbstractDiagram::propertiesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}
static const uint qt_meta_data_KDChart__PrivateAttributesModel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_KDChart__PrivateAttributesModel[] = {
    "KDChart::PrivateAttributesModel\0"
};

void KDChart::PrivateAttributesModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData KDChart::PrivateAttributesModel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDChart::PrivateAttributesModel::staticMetaObject = {
    { &AttributesModel::staticMetaObject, qt_meta_stringdata_KDChart__PrivateAttributesModel,
      qt_meta_data_KDChart__PrivateAttributesModel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDChart::PrivateAttributesModel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDChart::PrivateAttributesModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDChart::PrivateAttributesModel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDChart__PrivateAttributesModel))
        return static_cast<void*>(const_cast< PrivateAttributesModel*>(this));
    return AttributesModel::qt_metacast(_clname);
}

int KDChart::PrivateAttributesModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = AttributesModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
