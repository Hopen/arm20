/****************************************************************************
** Meta object code from reading C++ file 'KDChartAbstractCoordinatePlane.h'
**
** Created: Mon 17. Mar 15:11:16 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../KDChartAbstractCoordinatePlane.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KDChartAbstractCoordinatePlane.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDChart__AbstractCoordinatePlane[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: signature, parameters, type, tag, flags
      34,   33,   33,   33, 0x05,
      85,   33,   33,   33, 0x05,
      98,   33,   33,   33, 0x05,
     113,   33,   33,   33, 0x05,
     132,   33,   33,   33, 0x05,
     154,  152,   33,   33, 0x05,
     183,  152,   33,   33, 0x05,

 // slots: signature, parameters, type, tag, flags
     221,   33,   33,   33, 0x0a,
     230,   33,   33,   33, 0x0a,
     241,   33,   33,   33, 0x0a,
     256,   33,   33,   33, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_KDChart__AbstractCoordinatePlane[] = {
    "KDChart::AbstractCoordinatePlane\0\0"
    "destroyedCoordinatePlane(AbstractCoordinatePlane*)\0"
    "needUpdate()\0needRelayout()\0"
    "needLayoutPlanes()\0propertiesChanged()\0"
    ",\0geometryChanged(QRect,QRect)\0"
    "internal_geometryChanged(QRect,QRect)\0"
    "update()\0relayout()\0layoutPlanes()\0"
    "setGridNeedsRecalculate()\0"
};

void KDChart::AbstractCoordinatePlane::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AbstractCoordinatePlane *_t = static_cast<AbstractCoordinatePlane *>(_o);
        switch (_id) {
        case 0: _t->destroyedCoordinatePlane((*reinterpret_cast< AbstractCoordinatePlane*(*)>(_a[1]))); break;
        case 1: _t->needUpdate(); break;
        case 2: _t->needRelayout(); break;
        case 3: _t->needLayoutPlanes(); break;
        case 4: _t->propertiesChanged(); break;
        case 5: _t->geometryChanged((*reinterpret_cast< QRect(*)>(_a[1])),(*reinterpret_cast< QRect(*)>(_a[2]))); break;
        case 6: _t->internal_geometryChanged((*reinterpret_cast< QRect(*)>(_a[1])),(*reinterpret_cast< QRect(*)>(_a[2]))); break;
        case 7: _t->update(); break;
        case 8: _t->relayout(); break;
        case 9: _t->layoutPlanes(); break;
        case 10: _t->setGridNeedsRecalculate(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KDChart::AbstractCoordinatePlane::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDChart::AbstractCoordinatePlane::staticMetaObject = {
    { &AbstractArea::staticMetaObject, qt_meta_stringdata_KDChart__AbstractCoordinatePlane,
      qt_meta_data_KDChart__AbstractCoordinatePlane, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDChart::AbstractCoordinatePlane::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDChart::AbstractCoordinatePlane::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDChart::AbstractCoordinatePlane::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDChart__AbstractCoordinatePlane))
        return static_cast<void*>(const_cast< AbstractCoordinatePlane*>(this));
    return AbstractArea::qt_metacast(_clname);
}

int KDChart::AbstractCoordinatePlane::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = AbstractArea::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void KDChart::AbstractCoordinatePlane::destroyedCoordinatePlane(AbstractCoordinatePlane * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void KDChart::AbstractCoordinatePlane::needUpdate()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void KDChart::AbstractCoordinatePlane::needRelayout()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void KDChart::AbstractCoordinatePlane::needLayoutPlanes()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void KDChart::AbstractCoordinatePlane::propertiesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}

// SIGNAL 5
void KDChart::AbstractCoordinatePlane::geometryChanged(QRect _t1, QRect _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void KDChart::AbstractCoordinatePlane::internal_geometryChanged(QRect _t1, QRect _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}
QT_END_MOC_NAMESPACE
