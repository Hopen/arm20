/****************************************************************************
** Meta object code from reading C++ file 'kdganttview.h'
**
** Created: Mon 17. Mar 15:10:21 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Gantt/kdganttview.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'kdganttview.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDGantt__View[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x08,
      42,   14,   14,   14, 0x08,
      68,   14,   14,   14, 0x08,
     106,  104,   14,   14, 0x08,
     150,  104,   14,   14, 0x08,
     197,  191,   14,   14, 0x0a,
     231,  227,   14,   14, 0x0a,
     264,  257,   14,   14, 0x0a,
     304,   14,   14,   14, 0x0a,
     335,   14,   14,   14, 0x0a,
     372,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_KDGantt__View[] = {
    "KDGantt::View\0\0slotCollapsed(QModelIndex)\0"
    "slotExpanded(QModelIndex)\0"
    "slotVerticalScrollValueChanged(int)\0"
    ",\0slotLeftWidgetVerticalRangeChanged(int,int)\0"
    "slotGfxViewVerticalRangeChanged(int,int)\0"
    "model\0setModel(QAbstractItemModel*)\0"
    "idx\0setRootIndex(QModelIndex)\0smodel\0"
    "setSelectionModel(QItemSelectionModel*)\0"
    "setItemDelegate(ItemDelegate*)\0"
    "setConstraintModel(ConstraintModel*)\0"
    "setGrid(AbstractGrid*)\0"
};

void KDGantt::View::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        View *_t = static_cast<View *>(_o);
        switch (_id) {
        case 0: _t->d->slotCollapsed((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 1: _t->d->slotExpanded((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 2: _t->d->slotVerticalScrollValueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->d->slotLeftWidgetVerticalRangeChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 4: _t->d->slotGfxViewVerticalRangeChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: _t->setModel((*reinterpret_cast< QAbstractItemModel*(*)>(_a[1]))); break;
        case 6: _t->setRootIndex((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 7: _t->setSelectionModel((*reinterpret_cast< QItemSelectionModel*(*)>(_a[1]))); break;
        case 8: _t->setItemDelegate((*reinterpret_cast< ItemDelegate*(*)>(_a[1]))); break;
        case 9: _t->setConstraintModel((*reinterpret_cast< ConstraintModel*(*)>(_a[1]))); break;
        case 10: _t->setGrid((*reinterpret_cast< AbstractGrid*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KDGantt::View::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDGantt::View::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_KDGantt__View,
      qt_meta_data_KDGantt__View, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDGantt::View::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDGantt::View::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDGantt::View::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDGantt__View))
        return static_cast<void*>(const_cast< View*>(this));
    return QWidget::qt_metacast(_clname);
}

int KDGantt::View::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
