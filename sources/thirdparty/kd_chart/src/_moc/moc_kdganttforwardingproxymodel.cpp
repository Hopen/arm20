/****************************************************************************
** Meta object code from reading C++ file 'kdganttforwardingproxymodel.h'
**
** Created: Mon 17. Mar 15:10:51 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Gantt/kdganttforwardingproxymodel.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'kdganttforwardingproxymodel.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDGantt__ForwardingProxyModel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      31,   30,   30,   30, 0x09,
      59,   30,   30,   30, 0x09,
      78,   30,   30,   30, 0x09,
     109,   30,   30,   30, 0x09,
     139,  131,   30,   30, 0x09,
     196,  182,   30,   30, 0x09,
     248,  182,   30,   30, 0x09,
     291,  182,   30,   30, 0x09,
     342,  182,   30,   30, 0x09,
     384,  182,   30,   30, 0x09,
     433,  182,   30,   30, 0x09,
     484,  473,   30,   30, 0x09,
     532,  473,   30,   30, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_KDGantt__ForwardingProxyModel[] = {
    "KDGantt::ForwardingProxyModel\0\0"
    "sourceModelAboutToBeReset()\0"
    "sourceModelReset()\0sourceLayoutAboutToBeChanged()\0"
    "sourceLayoutChanged()\0from,to\0"
    "sourceDataChanged(QModelIndex,QModelIndex)\0"
    "idx,start,end\0"
    "sourceColumnsAboutToBeInserted(QModelIndex,int,int)\0"
    "sourceColumnsInserted(QModelIndex,int,int)\0"
    "sourceColumnsAboutToBeRemoved(QModelIndex,int,int)\0"
    "sourceColumnsRemoved(QModelIndex,int,int)\0"
    "sourceRowsAboutToBeInserted(QModelIndex,int,int)\0"
    "sourceRowsInserted(QModelIndex,int,int)\0"
    ",start,end\0sourceRowsAboutToBeRemoved(QModelIndex,int,int)\0"
    "sourceRowsRemoved(QModelIndex,int,int)\0"
};

void KDGantt::ForwardingProxyModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ForwardingProxyModel *_t = static_cast<ForwardingProxyModel *>(_o);
        switch (_id) {
        case 0: _t->sourceModelAboutToBeReset(); break;
        case 1: _t->sourceModelReset(); break;
        case 2: _t->sourceLayoutAboutToBeChanged(); break;
        case 3: _t->sourceLayoutChanged(); break;
        case 4: _t->sourceDataChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        case 5: _t->sourceColumnsAboutToBeInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 6: _t->sourceColumnsInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 7: _t->sourceColumnsAboutToBeRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 8: _t->sourceColumnsRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 9: _t->sourceRowsAboutToBeInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 10: _t->sourceRowsInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 11: _t->sourceRowsAboutToBeRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 12: _t->sourceRowsRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KDGantt::ForwardingProxyModel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDGantt::ForwardingProxyModel::staticMetaObject = {
    { &QAbstractProxyModel::staticMetaObject, qt_meta_stringdata_KDGantt__ForwardingProxyModel,
      qt_meta_data_KDGantt__ForwardingProxyModel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDGantt::ForwardingProxyModel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDGantt::ForwardingProxyModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDGantt::ForwardingProxyModel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDGantt__ForwardingProxyModel))
        return static_cast<void*>(const_cast< ForwardingProxyModel*>(this));
    return QAbstractProxyModel::qt_metacast(_clname);
}

int KDGantt::ForwardingProxyModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractProxyModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
