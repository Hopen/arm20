/****************************************************************************
** Meta object code from reading C++ file 'KDChartCartesianAxis.h'
**
** Created: Mon 17. Mar 15:11:54 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../KDChartCartesianAxis.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KDChartCartesianAxis.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDChart__CartesianAxis[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      24,   23,   23,   23, 0x0a,
      62,   49,   45,   23, 0x0a,
      79,   23,   45,   23, 0x2a,

       0        // eod
};

static const char qt_meta_stringdata_KDChart__CartesianAxis[] = {
    "KDChart::CartesianAxis\0\0setCachedSizeDirty()\0"
    "int\0subUnitTicks\0tickLength(bool)\0"
    "tickLength()\0"
};

void KDChart::CartesianAxis::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CartesianAxis *_t = static_cast<CartesianAxis *>(_o);
        switch (_id) {
        case 0: _t->setCachedSizeDirty(); break;
        case 1: { int _r = _t->tickLength((*reinterpret_cast< bool(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 2: { int _r = _t->tickLength();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KDChart::CartesianAxis::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDChart::CartesianAxis::staticMetaObject = {
    { &AbstractAxis::staticMetaObject, qt_meta_stringdata_KDChart__CartesianAxis,
      qt_meta_data_KDChart__CartesianAxis, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDChart::CartesianAxis::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDChart::CartesianAxis::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDChart::CartesianAxis::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDChart__CartesianAxis))
        return static_cast<void*>(const_cast< CartesianAxis*>(this));
    return AbstractAxis::qt_metacast(_clname);
}

int KDChart::CartesianAxis::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = AbstractAxis::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
