/****************************************************************************
** Meta object code from reading C++ file 'kdganttgraphicsscene.h'
**
** Created: Mon 17. Mar 15:10:35 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Gantt/kdganttgraphicsscene.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'kdganttgraphicsscene.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDGantt__GraphicsScene[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: signature, parameters, type, tag, flags
      24,   23,   23,   23, 0x05,
      44,   38,   23,   23, 0x05,
      65,   38,   23,   23, 0x05,
      92,   38,   23,   23, 0x05,
     113,   38,   23,   23, 0x05,

 // slots: signature, parameters, type, tag, flags
     134,   23,   23,   23, 0x0a,
     164,   23,   23,   23, 0x0a,
     210,   23,   23,   23, 0x0a,
     251,  247,   23,   23, 0x0a,
     292,  277,   23,   23, 0x0a,
     332,   23,   23,   23, 0x0a,
     350,   23,   23,   23, 0x08,
     391,   23,   23,   23, 0x08,
     434,   23,   23,   23, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_KDGantt__GraphicsScene[] = {
    "KDGantt::GraphicsScene\0\0gridChanged()\0"
    "index\0clicked(QModelIndex)\0"
    "doubleClicked(QModelIndex)\0"
    "entered(QModelIndex)\0pressed(QModelIndex)\0"
    "setModel(QAbstractItemModel*)\0"
    "setSummaryHandlingModel(QAbstractProxyModel*)\0"
    "setConstraintModel(ConstraintModel*)\0"
    "idx\0setRootIndex(QModelIndex)\0"
    "selectionmodel\0setSelectionModel(QItemSelectionModel*)\0"
    "setReadOnly(bool)\0"
    "slotConstraintAdded(KDGantt::Constraint)\0"
    "slotConstraintRemoved(KDGantt::Constraint)\0"
    "slotGridChanged()\0"
};

void KDGantt::GraphicsScene::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        GraphicsScene *_t = static_cast<GraphicsScene *>(_o);
        switch (_id) {
        case 0: _t->gridChanged(); break;
        case 1: _t->clicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 2: _t->doubleClicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 3: _t->entered((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 4: _t->pressed((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 5: _t->setModel((*reinterpret_cast< QAbstractItemModel*(*)>(_a[1]))); break;
        case 6: _t->setSummaryHandlingModel((*reinterpret_cast< QAbstractProxyModel*(*)>(_a[1]))); break;
        case 7: _t->setConstraintModel((*reinterpret_cast< ConstraintModel*(*)>(_a[1]))); break;
        case 8: _t->setRootIndex((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 9: _t->setSelectionModel((*reinterpret_cast< QItemSelectionModel*(*)>(_a[1]))); break;
        case 10: _t->setReadOnly((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: _t->slotConstraintAdded((*reinterpret_cast< const KDGantt::Constraint(*)>(_a[1]))); break;
        case 12: _t->slotConstraintRemoved((*reinterpret_cast< const KDGantt::Constraint(*)>(_a[1]))); break;
        case 13: _t->slotGridChanged(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KDGantt::GraphicsScene::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDGantt::GraphicsScene::staticMetaObject = {
    { &QGraphicsScene::staticMetaObject, qt_meta_stringdata_KDGantt__GraphicsScene,
      qt_meta_data_KDGantt__GraphicsScene, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDGantt::GraphicsScene::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDGantt::GraphicsScene::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDGantt::GraphicsScene::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDGantt__GraphicsScene))
        return static_cast<void*>(const_cast< GraphicsScene*>(this));
    return QGraphicsScene::qt_metacast(_clname);
}

int KDGantt::GraphicsScene::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGraphicsScene::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    }
    return _id;
}

// SIGNAL 0
void KDGantt::GraphicsScene::gridChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void KDGantt::GraphicsScene::clicked(const QModelIndex & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void KDGantt::GraphicsScene::doubleClicked(const QModelIndex & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void KDGantt::GraphicsScene::entered(const QModelIndex & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void KDGantt::GraphicsScene::pressed(const QModelIndex & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_END_MOC_NAMESPACE
