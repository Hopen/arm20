/****************************************************************************
** Meta object code from reading C++ file 'KDChartCartesianCoordinatePlane.h'
**
** Created: Mon 17. Mar 15:11:18 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../KDChartCartesianCoordinatePlane.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KDChartCartesianCoordinatePlane.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDChart__CartesianCoordinatePlane[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      35,   34,   34,   34, 0x0a,
      56,   34,   34,   34, 0x0a,
      86,   34,   34,   34, 0x0a,
     114,   34,   34,   34, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_KDChart__CartesianCoordinatePlane[] = {
    "KDChart::CartesianCoordinatePlane\0\0"
    "adjustRangesToData()\0adjustHorizontalRangeToData()\0"
    "adjustVerticalRangeToData()\0"
    "slotLayoutChanged(AbstractDiagram*)\0"
};

void KDChart::CartesianCoordinatePlane::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CartesianCoordinatePlane *_t = static_cast<CartesianCoordinatePlane *>(_o);
        switch (_id) {
        case 0: _t->adjustRangesToData(); break;
        case 1: _t->adjustHorizontalRangeToData(); break;
        case 2: _t->adjustVerticalRangeToData(); break;
        case 3: _t->slotLayoutChanged((*reinterpret_cast< AbstractDiagram*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KDChart::CartesianCoordinatePlane::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDChart::CartesianCoordinatePlane::staticMetaObject = {
    { &AbstractCoordinatePlane::staticMetaObject, qt_meta_stringdata_KDChart__CartesianCoordinatePlane,
      qt_meta_data_KDChart__CartesianCoordinatePlane, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDChart::CartesianCoordinatePlane::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDChart::CartesianCoordinatePlane::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDChart::CartesianCoordinatePlane::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDChart__CartesianCoordinatePlane))
        return static_cast<void*>(const_cast< CartesianCoordinatePlane*>(this));
    return AbstractCoordinatePlane::qt_metacast(_clname);
}

int KDChart::CartesianCoordinatePlane::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = AbstractCoordinatePlane::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
