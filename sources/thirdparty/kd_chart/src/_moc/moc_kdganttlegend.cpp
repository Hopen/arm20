/****************************************************************************
** Meta object code from reading C++ file 'kdganttlegend.h'
**
** Created: Mon 17. Mar 15:12:47 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Gantt/kdganttlegend.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'kdganttlegend.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDGantt__Legend[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_KDGantt__Legend[] = {
    "KDGantt::Legend\0\0modelDataChanged()\0"
};

void KDGantt::Legend::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Legend *_t = static_cast<Legend *>(_o);
        switch (_id) {
        case 0: _t->modelDataChanged(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData KDGantt::Legend::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDGantt::Legend::staticMetaObject = {
    { &QAbstractItemView::staticMetaObject, qt_meta_stringdata_KDGantt__Legend,
      qt_meta_data_KDGantt__Legend, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDGantt::Legend::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDGantt::Legend::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDGantt::Legend::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDGantt__Legend))
        return static_cast<void*>(const_cast< Legend*>(this));
    return QAbstractItemView::qt_metacast(_clname);
}

int KDGantt::Legend::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractItemView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
