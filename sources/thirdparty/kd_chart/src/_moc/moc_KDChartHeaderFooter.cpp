/****************************************************************************
** Meta object code from reading C++ file 'KDChartHeaderFooter.h'
**
** Created: Mon 17. Mar 15:12:03 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../KDChartHeaderFooter.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KDChartHeaderFooter.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDChart__HeaderFooter[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      23,   22,   22,   22, 0x05,
      60,   22,   22,   22, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_KDChart__HeaderFooter[] = {
    "KDChart::HeaderFooter\0\0"
    "destroyedHeaderFooter(HeaderFooter*)\0"
    "positionChanged(HeaderFooter*)\0"
};

void KDChart::HeaderFooter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        HeaderFooter *_t = static_cast<HeaderFooter *>(_o);
        switch (_id) {
        case 0: _t->destroyedHeaderFooter((*reinterpret_cast< HeaderFooter*(*)>(_a[1]))); break;
        case 1: _t->positionChanged((*reinterpret_cast< HeaderFooter*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KDChart::HeaderFooter::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDChart::HeaderFooter::staticMetaObject = {
    { &TextArea::staticMetaObject, qt_meta_stringdata_KDChart__HeaderFooter,
      qt_meta_data_KDChart__HeaderFooter, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDChart::HeaderFooter::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDChart::HeaderFooter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDChart::HeaderFooter::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDChart__HeaderFooter))
        return static_cast<void*>(const_cast< HeaderFooter*>(this));
    return TextArea::qt_metacast(_clname);
}

int KDChart::HeaderFooter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TextArea::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void KDChart::HeaderFooter::destroyedHeaderFooter(HeaderFooter * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void KDChart::HeaderFooter::positionChanged(HeaderFooter * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
