/****************************************************************************
** Meta object code from reading C++ file 'KDChartDatasetProxyModel.h'
**
** Created: Mon 17. Mar 15:11:56 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../KDChartDatasetProxyModel.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KDChartDatasetProxyModel.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDChart__DatasetProxyModel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      28,   27,   27,   27, 0x0a,
      68,   55,   27,   27, 0x0a,
     138,  128,   27,   27, 0x0a,
     218,  195,   27,   27, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_KDChart__DatasetProxyModel[] = {
    "KDChart::DatasetProxyModel\0\0"
    "resetDatasetDescriptions()\0columnConfig\0"
    "setDatasetColumnDescriptionVector(DatasetDescriptionVector)\0"
    "rowConfig\0"
    "setDatasetRowDescriptionVector(DatasetDescriptionVector)\0"
    "rowConfig,columnConfig\0"
    "setDatasetDescriptionVectors(DatasetDescriptionVector,DatasetDescripti"
    "onVector)\0"
};

void KDChart::DatasetProxyModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DatasetProxyModel *_t = static_cast<DatasetProxyModel *>(_o);
        switch (_id) {
        case 0: _t->resetDatasetDescriptions(); break;
        case 1: _t->setDatasetColumnDescriptionVector((*reinterpret_cast< const DatasetDescriptionVector(*)>(_a[1]))); break;
        case 2: _t->setDatasetRowDescriptionVector((*reinterpret_cast< const DatasetDescriptionVector(*)>(_a[1]))); break;
        case 3: _t->setDatasetDescriptionVectors((*reinterpret_cast< const DatasetDescriptionVector(*)>(_a[1])),(*reinterpret_cast< const DatasetDescriptionVector(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KDChart::DatasetProxyModel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDChart::DatasetProxyModel::staticMetaObject = {
    { &QSortFilterProxyModel::staticMetaObject, qt_meta_stringdata_KDChart__DatasetProxyModel,
      qt_meta_data_KDChart__DatasetProxyModel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDChart::DatasetProxyModel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDChart::DatasetProxyModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDChart::DatasetProxyModel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDChart__DatasetProxyModel))
        return static_cast<void*>(const_cast< DatasetProxyModel*>(this));
    return QSortFilterProxyModel::qt_metacast(_clname);
}

int KDChart::DatasetProxyModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QSortFilterProxyModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
