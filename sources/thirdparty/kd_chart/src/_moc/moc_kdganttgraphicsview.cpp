/****************************************************************************
** Meta object code from reading C++ file 'kdganttgraphicsview.h'
**
** Created: Mon 17. Mar 15:10:26 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Gantt/kdganttgraphicsview.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'kdganttgraphicsview.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDGantt__GraphicsView[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      29,   14, // methods
       1,  159, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: signature, parameters, type, tag, flags
      29,   23,   22,   22, 0x05,
      52,   23,   22,   22, 0x05,
      73,   23,   22,   22, 0x05,
     100,   23,   22,   22, 0x05,
     121,   23,   22,   22, 0x05,
     145,  142,   22,   22, 0x05,

 // slots: signature, parameters, type, tag, flags
     180,   22,   22,   22, 0x08,
     198,   22,   22,   22, 0x08,
     236,   22,   22,   22, 0x08,
     292,  275,   22,   22, 0x08,
     333,  275,   22,   22, 0x08,
     393,  373,   22,   22, 0x08,
     434,   22,   22,   22, 0x08,
     454,   22,   22,   22, 0x08,
     471,  275,   22,   22, 0x08,
     509,  275,   22,   22, 0x08,
     555,  275,   22,   22, 0x08,
     596,  592,   22,   22, 0x08,
     625,  592,   22,   22, 0x08,
     660,   22,   22,   22, 0x0a,
     678,   22,   22,   22, 0x0a,
     714,  708,   22,   22, 0x0a,
     760,   22,   22,   22, 0x0a,
     797,   22,   22,   22, 0x0a,
     823,   22,   22,   22, 0x0a,
     863,   22,   22,   22, 0x0a,
     904,   22,   22,   22, 0x0a,
     936,  927,   22,   22, 0x0a,
     967,   22,   22,   22, 0x0a,

 // properties: name, type, flags
     990,  985, 0x01095103,

       0        // eod
};

static const char qt_meta_stringdata_KDGantt__GraphicsView[] = {
    "KDGantt::GraphicsView\0\0index\0"
    "activated(QModelIndex)\0clicked(QModelIndex)\0"
    "doubleClicked(QModelIndex)\0"
    "entered(QModelIndex)\0pressed(QModelIndex)\0"
    "pt\0headerContextMenuRequested(QPoint)\0"
    "slotGridChanged()\0"
    "slotHorizontalScrollValueChanged(int)\0"
    "slotHeaderContextMenuRequested(QPoint)\0"
    "parent,start,end\0"
    "slotColumnsInserted(QModelIndex,int,int)\0"
    "slotColumnsRemoved(QModelIndex,int,int)\0"
    "topLeft,bottomRight\0"
    "slotDataChanged(QModelIndex,QModelIndex)\0"
    "slotLayoutChanged()\0slotModelReset()\0"
    "slotRowsInserted(QModelIndex,int,int)\0"
    "slotRowsAboutToBeRemoved(QModelIndex,int,int)\0"
    "slotRowsRemoved(QModelIndex,int,int)\0"
    "idx\0slotItemClicked(QModelIndex)\0"
    "slotItemDoubleClicked(QModelIndex)\0"
    "updateSceneRect()\0setModel(QAbstractItemModel*)\0"
    "model\0setSummaryHandlingModel(QAbstractProxyModel*)\0"
    "setConstraintModel(ConstraintModel*)\0"
    "setRootIndex(QModelIndex)\0"
    "setSelectionModel(QItemSelectionModel*)\0"
    "setRowController(AbstractRowController*)\0"
    "setGrid(AbstractGrid*)\0delegate\0"
    "setItemDelegate(ItemDelegate*)\0"
    "setReadOnly(bool)\0bool\0readOnly\0"
};

void KDGantt::GraphicsView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        GraphicsView *_t = static_cast<GraphicsView *>(_o);
        switch (_id) {
        case 0: _t->activated((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 1: _t->clicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 2: _t->doubleClicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 3: _t->entered((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 4: _t->pressed((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 5: _t->headerContextMenuRequested((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 6: _t->d->slotGridChanged(); break;
        case 7: _t->d->slotHorizontalScrollValueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->d->slotHeaderContextMenuRequested((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 9: _t->d->slotColumnsInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 10: _t->d->slotColumnsRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 11: _t->d->slotDataChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        case 12: _t->d->slotLayoutChanged(); break;
        case 13: _t->d->slotModelReset(); break;
        case 14: _t->d->slotRowsInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 15: _t->d->slotRowsAboutToBeRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 16: _t->d->slotRowsRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 17: _t->d->slotItemClicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 18: _t->d->slotItemDoubleClicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 19: _t->updateSceneRect(); break;
        case 20: _t->setModel((*reinterpret_cast< QAbstractItemModel*(*)>(_a[1]))); break;
        case 21: _t->setSummaryHandlingModel((*reinterpret_cast< QAbstractProxyModel*(*)>(_a[1]))); break;
        case 22: _t->setConstraintModel((*reinterpret_cast< ConstraintModel*(*)>(_a[1]))); break;
        case 23: _t->setRootIndex((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 24: _t->setSelectionModel((*reinterpret_cast< QItemSelectionModel*(*)>(_a[1]))); break;
        case 25: _t->setRowController((*reinterpret_cast< AbstractRowController*(*)>(_a[1]))); break;
        case 26: _t->setGrid((*reinterpret_cast< AbstractGrid*(*)>(_a[1]))); break;
        case 27: _t->setItemDelegate((*reinterpret_cast< ItemDelegate*(*)>(_a[1]))); break;
        case 28: _t->setReadOnly((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KDGantt::GraphicsView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDGantt::GraphicsView::staticMetaObject = {
    { &QGraphicsView::staticMetaObject, qt_meta_stringdata_KDGantt__GraphicsView,
      qt_meta_data_KDGantt__GraphicsView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDGantt::GraphicsView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDGantt::GraphicsView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDGantt::GraphicsView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDGantt__GraphicsView))
        return static_cast<void*>(const_cast< GraphicsView*>(this));
    return QGraphicsView::qt_metacast(_clname);
}

int KDGantt::GraphicsView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGraphicsView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 29)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 29;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = isReadOnly(); break;
        }
        _id -= 1;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setReadOnly(*reinterpret_cast< bool*>(_v)); break;
        }
        _id -= 1;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void KDGantt::GraphicsView::activated(const QModelIndex & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void KDGantt::GraphicsView::clicked(const QModelIndex & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void KDGantt::GraphicsView::doubleClicked(const QModelIndex & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void KDGantt::GraphicsView::entered(const QModelIndex & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void KDGantt::GraphicsView::pressed(const QModelIndex & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void KDGantt::GraphicsView::headerContextMenuRequested(const QPoint & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_END_MOC_NAMESPACE
