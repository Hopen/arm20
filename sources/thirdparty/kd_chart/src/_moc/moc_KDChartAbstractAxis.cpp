/****************************************************************************
** Meta object code from reading C++ file 'KDChartAbstractAxis.h'
**
** Created: Mon 17. Mar 15:11:44 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../KDChartAbstractAxis.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KDChartAbstractAxis.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDChart__AbstractAxis[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      23,   22,   22,   22, 0x09,
      37,   22,   22,   22, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_KDChart__AbstractAxis[] = {
    "KDChart::AbstractAxis\0\0delayedInit()\0"
    "update()\0"
};

void KDChart::AbstractAxis::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AbstractAxis *_t = static_cast<AbstractAxis *>(_o);
        switch (_id) {
        case 0: _t->delayedInit(); break;
        case 1: _t->update(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData KDChart::AbstractAxis::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDChart::AbstractAxis::staticMetaObject = {
    { &AbstractArea::staticMetaObject, qt_meta_stringdata_KDChart__AbstractAxis,
      qt_meta_data_KDChart__AbstractAxis, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDChart::AbstractAxis::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDChart::AbstractAxis::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDChart::AbstractAxis::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDChart__AbstractAxis))
        return static_cast<void*>(const_cast< AbstractAxis*>(this));
    return AbstractArea::qt_metacast(_clname);
}

int KDChart::AbstractAxis::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = AbstractArea::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
