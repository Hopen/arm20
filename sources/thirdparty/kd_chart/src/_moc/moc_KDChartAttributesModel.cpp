/****************************************************************************
** Meta object code from reading C++ file 'KDChartAttributesModel.h'
**
** Created: Mon 17. Mar 15:11:47 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../KDChartAttributesModel.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KDChartAttributesModel.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDChart__AttributesModel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      28,   26,   25,   25, 0x05,

 // slots: signature, parameters, type, tag, flags
      88,   71,   25,   25, 0x08,
     135,   71,   25,   25, 0x08,
     185,   71,   25,   25, 0x08,
     223,   71,   25,   25, 0x08,
     264,   71,   25,   25, 0x08,
     310,   71,   25,   25, 0x08,
     359,   71,   25,   25, 0x08,
     396,   71,   25,   25, 0x08,
     456,  436,   25,   25, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_KDChart__AttributesModel[] = {
    "KDChart::AttributesModel\0\0,\0"
    "attributesChanged(QModelIndex,QModelIndex)\0"
    "parent,start,end\0"
    "slotRowsAboutToBeInserted(QModelIndex,int,int)\0"
    "slotColumnsAboutToBeInserted(QModelIndex,int,int)\0"
    "slotRowsInserted(QModelIndex,int,int)\0"
    "slotColumnsInserted(QModelIndex,int,int)\0"
    "slotRowsAboutToBeRemoved(QModelIndex,int,int)\0"
    "slotColumnsAboutToBeRemoved(QModelIndex,int,int)\0"
    "slotRowsRemoved(QModelIndex,int,int)\0"
    "slotColumnsRemoved(QModelIndex,int,int)\0"
    "topLeft,bottomRight\0"
    "slotDataChanged(QModelIndex,QModelIndex)\0"
};

void KDChart::AttributesModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AttributesModel *_t = static_cast<AttributesModel *>(_o);
        switch (_id) {
        case 0: _t->attributesChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        case 1: _t->slotRowsAboutToBeInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 2: _t->slotColumnsAboutToBeInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 3: _t->slotRowsInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 4: _t->slotColumnsInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 5: _t->slotRowsAboutToBeRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 6: _t->slotColumnsAboutToBeRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 7: _t->slotRowsRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 8: _t->slotColumnsRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 9: _t->slotDataChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KDChart::AttributesModel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDChart::AttributesModel::staticMetaObject = {
    { &AbstractProxyModel::staticMetaObject, qt_meta_stringdata_KDChart__AttributesModel,
      qt_meta_data_KDChart__AttributesModel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDChart::AttributesModel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDChart::AttributesModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDChart::AttributesModel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDChart__AttributesModel))
        return static_cast<void*>(const_cast< AttributesModel*>(this));
    return AbstractProxyModel::qt_metacast(_clname);
}

int KDChart::AttributesModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = AbstractProxyModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void KDChart::AttributesModel::attributesChanged(const QModelIndex & _t1, const QModelIndex & _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
