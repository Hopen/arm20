/****************************************************************************
** Meta object code from reading C++ file 'KDChartWidget.h'
**
** Created: Mon 17. Mar 15:11:27 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../KDChartWidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KDChartWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDChart__Widget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      39,   17,   16,   16, 0x0a,
      81,   73,   16,   16, 0x0a,
     107,   73,   16,   16, 0x0a,
     132,   73,   16,   16, 0x0a,
     159,   73,   16,   16, 0x0a,
     205,  187,   16,   16, 0x0a,
     242,  232,   16,   16, 0x2a,
     269,  261,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_KDChart__Widget[] = {
    "KDChart::Widget\0\0left,top,right,bottom\0"
    "setGlobalLeading(int,int,int,int)\0"
    "leading\0setGlobalLeadingLeft(int)\0"
    "setGlobalLeadingTop(int)\0"
    "setGlobalLeadingRight(int)\0"
    "setGlobalLeadingBottom(int)\0"
    "chartType,subType\0setType(ChartType,SubType)\0"
    "chartType\0setType(ChartType)\0subType\0"
    "setSubType(SubType)\0"
};

void KDChart::Widget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Widget *_t = static_cast<Widget *>(_o);
        switch (_id) {
        case 0: _t->setGlobalLeading((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 1: _t->setGlobalLeadingLeft((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->setGlobalLeadingTop((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->setGlobalLeadingRight((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->setGlobalLeadingBottom((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->setType((*reinterpret_cast< ChartType(*)>(_a[1])),(*reinterpret_cast< SubType(*)>(_a[2]))); break;
        case 6: _t->setType((*reinterpret_cast< ChartType(*)>(_a[1]))); break;
        case 7: _t->setSubType((*reinterpret_cast< SubType(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KDChart::Widget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDChart::Widget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_KDChart__Widget,
      qt_meta_data_KDChart__Widget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDChart::Widget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDChart::Widget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDChart::Widget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDChart__Widget))
        return static_cast<void*>(const_cast< Widget*>(this));
    return QWidget::qt_metacast(_clname);
}

int KDChart::Widget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
