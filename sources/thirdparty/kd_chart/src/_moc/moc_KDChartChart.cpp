/****************************************************************************
** Meta object code from reading C++ file 'KDChartChart.h'
**
** Created: Mon 17. Mar 15:11:22 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../KDChartChart.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KDChartChart.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDChart__Chart[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       4,   19, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x05,

 // properties: name, type, flags
      40,   36, 0x02095103,
      57,   36, 0x02095103,
      77,   36, 0x02095103,
      95,   36, 0x02095103,

       0        // eod
};

static const char qt_meta_stringdata_KDChart__Chart[] = {
    "KDChart::Chart\0\0propertiesChanged()\0"
    "int\0globalLeadingTop\0globalLeadingBottom\0"
    "globalLeadingLeft\0globalLeadingRight\0"
};

void KDChart::Chart::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Chart *_t = static_cast<Chart *>(_o);
        switch (_id) {
        case 0: _t->propertiesChanged(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData KDChart::Chart::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDChart::Chart::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_KDChart__Chart,
      qt_meta_data_KDChart__Chart, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDChart::Chart::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDChart::Chart::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDChart::Chart::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDChart__Chart))
        return static_cast<void*>(const_cast< Chart*>(this));
    return QWidget::qt_metacast(_clname);
}

int KDChart::Chart::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = globalLeadingTop(); break;
        case 1: *reinterpret_cast< int*>(_v) = globalLeadingBottom(); break;
        case 2: *reinterpret_cast< int*>(_v) = globalLeadingLeft(); break;
        case 3: *reinterpret_cast< int*>(_v) = globalLeadingRight(); break;
        }
        _id -= 4;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setGlobalLeadingTop(*reinterpret_cast< int*>(_v)); break;
        case 1: setGlobalLeadingBottom(*reinterpret_cast< int*>(_v)); break;
        case 2: setGlobalLeadingLeft(*reinterpret_cast< int*>(_v)); break;
        case 3: setGlobalLeadingRight(*reinterpret_cast< int*>(_v)); break;
        }
        _id -= 4;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void KDChart::Chart::propertiesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
