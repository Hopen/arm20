/****************************************************************************
** Meta object code from reading C++ file 'KDChartEnums.h'
**
** Created: Mon 17. Mar 15:12:01 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../KDChartEnums.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KDChartEnums.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDChartEnums[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, flags, count, data
      13, 0x0,    5,   18,

 // enum data: key, value
      30, uint(KDChartEnums::LayoutJustOverwrite),
      50, uint(KDChartEnums::LayoutPolicyRotate),
      69, uint(KDChartEnums::LayoutPolicyShiftVertically),
      97, uint(KDChartEnums::LayoutPolicyShiftHorizontally),
     127, uint(KDChartEnums::LayoutPolicyShrinkFontSize),

       0        // eod
};

static const char qt_meta_stringdata_KDChartEnums[] = {
    "KDChartEnums\0TextLayoutPolicy\0"
    "LayoutJustOverwrite\0LayoutPolicyRotate\0"
    "LayoutPolicyShiftVertically\0"
    "LayoutPolicyShiftHorizontally\0"
    "LayoutPolicyShrinkFontSize\0"
};

void KDChartEnums::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData KDChartEnums::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDChartEnums::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_KDChartEnums,
      qt_meta_data_KDChartEnums, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDChartEnums::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDChartEnums::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDChartEnums::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDChartEnums))
        return static_cast<void*>(const_cast< KDChartEnums*>(this));
    return QObject::qt_metacast(_clname);
}

int KDChartEnums::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
