/****************************************************************************
** Meta object code from reading C++ file 'KDChartChart_p.h'
**
** Created: Mon 17. Mar 15:11:25 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../KDChartChart_p.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KDChartChart_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDChart__Chart__Private[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      25,   24,   24,   24, 0x0a,
      44,   24,   24,   24, 0x0a,
      66,   59,   24,   24, 0x0a,
     118,  105,   24,   24, 0x0a,
     175,  169,   24,   24, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_KDChart__Chart__Private[] = {
    "KDChart::Chart::Private\0\0slotLayoutPlanes()\0"
    "slotRelayout()\0legend\0"
    "slotUnregisterDestroyedLegend(Legend*)\0"
    "headerFooter\0"
    "slotUnregisterDestroyedHeaderFooter(HeaderFooter*)\0"
    "plane\0slotUnregisterDestroyedPlane(AbstractCoordinatePlane*)\0"
};

void KDChart::Chart::Private::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Private *_t = static_cast<Private *>(_o);
        switch (_id) {
        case 0: _t->slotLayoutPlanes(); break;
        case 1: _t->slotRelayout(); break;
        case 2: _t->slotUnregisterDestroyedLegend((*reinterpret_cast< Legend*(*)>(_a[1]))); break;
        case 3: _t->slotUnregisterDestroyedHeaderFooter((*reinterpret_cast< HeaderFooter*(*)>(_a[1]))); break;
        case 4: _t->slotUnregisterDestroyedPlane((*reinterpret_cast< AbstractCoordinatePlane*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KDChart::Chart::Private::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDChart::Chart::Private::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_KDChart__Chart__Private,
      qt_meta_data_KDChart__Chart__Private, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDChart::Chart::Private::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDChart::Chart::Private::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDChart::Chart::Private::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDChart__Chart__Private))
        return static_cast<void*>(const_cast< Private*>(this));
    return QObject::qt_metacast(_clname);
}

int KDChart::Chart::Private::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
