/****************************************************************************
** Meta object code from reading C++ file 'KDChartCartesianDiagramDataCompressor_p.h'
**
** Created: Mon 17. Mar 15:12:11 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../KDChartCartesianDiagramDataCompressor_p.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KDChartCartesianDiagramDataCompressor_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDChart__CartesianDiagramDataCompressor[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      44,   41,   40,   40, 0x08,
      91,   41,   40,   40, 0x08,
     129,   41,   40,   40, 0x08,
     175,   41,   40,   40, 0x08,
     212,   41,   40,   40, 0x08,
     262,   41,   40,   40, 0x08,
     303,   41,   40,   40, 0x08,
     352,   41,   40,   40, 0x08,
     392,   41,   40,   40, 0x08,
     446,  444,   40,   40, 0x08,
     492,   40,   40,   40, 0x08,
     517,   40,   40,   40, 0x08,
     560,   40,   40,   40, 0x08,
     575,   40,   40,   40, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_KDChart__CartesianDiagramDataCompressor[] = {
    "KDChart::CartesianDiagramDataCompressor\0"
    "\0,,\0slotRowsAboutToBeInserted(QModelIndex,int,int)\0"
    "slotRowsInserted(QModelIndex,int,int)\0"
    "slotRowsAboutToBeRemoved(QModelIndex,int,int)\0"
    "slotRowsRemoved(QModelIndex,int,int)\0"
    "slotColumnsAboutToBeInserted(QModelIndex,int,int)\0"
    "slotColumnsInserted(QModelIndex,int,int)\0"
    "slotColumnsAboutToBeRemoved(QModelIndex,int,int)\0"
    "slotColumnsRemoved(QModelIndex,int,int)\0"
    "slotModelHeaderDataChanged(Qt::Orientation,int,int)\0"
    ",\0slotModelDataChanged(QModelIndex,QModelIndex)\0"
    "slotModelLayoutChanged()\0"
    "slotDiagramLayoutChanged(AbstractDiagram*)\0"
    "rebuildCache()\0clearCache()\0"
};

void KDChart::CartesianDiagramDataCompressor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CartesianDiagramDataCompressor *_t = static_cast<CartesianDiagramDataCompressor *>(_o);
        switch (_id) {
        case 0: _t->slotRowsAboutToBeInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 1: _t->slotRowsInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 2: _t->slotRowsAboutToBeRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 3: _t->slotRowsRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 4: _t->slotColumnsAboutToBeInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 5: _t->slotColumnsInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 6: _t->slotColumnsAboutToBeRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 7: _t->slotColumnsRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 8: _t->slotModelHeaderDataChanged((*reinterpret_cast< Qt::Orientation(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 9: _t->slotModelDataChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        case 10: _t->slotModelLayoutChanged(); break;
        case 11: _t->slotDiagramLayoutChanged((*reinterpret_cast< AbstractDiagram*(*)>(_a[1]))); break;
        case 12: _t->rebuildCache(); break;
        case 13: _t->clearCache(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KDChart::CartesianDiagramDataCompressor::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDChart::CartesianDiagramDataCompressor::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_KDChart__CartesianDiagramDataCompressor,
      qt_meta_data_KDChart__CartesianDiagramDataCompressor, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDChart::CartesianDiagramDataCompressor::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDChart::CartesianDiagramDataCompressor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDChart::CartesianDiagramDataCompressor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDChart__CartesianDiagramDataCompressor))
        return static_cast<void*>(const_cast< CartesianDiagramDataCompressor*>(this));
    return QObject::qt_metacast(_clname);
}

int KDChart::CartesianDiagramDataCompressor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
