/****************************************************************************
** Meta object code from reading C++ file 'kdganttabstractgrid.h'
**
** Created: Mon 17. Mar 15:11:02 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Gantt/kdganttabstractgrid.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'kdganttabstractgrid.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDGantt__AbstractGrid[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      23,   22,   22,   22, 0x05,

 // slots: signature, parameters, type, tag, flags
      43,   37,   22,   22, 0x0a,
      77,   73,   22,   22, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_KDGantt__AbstractGrid[] = {
    "KDGantt::AbstractGrid\0\0gridChanged()\0"
    "model\0setModel(QAbstractItemModel*)\0"
    "idx\0setRootIndex(QModelIndex)\0"
};

void KDGantt::AbstractGrid::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AbstractGrid *_t = static_cast<AbstractGrid *>(_o);
        switch (_id) {
        case 0: _t->gridChanged(); break;
        case 1: _t->setModel((*reinterpret_cast< QAbstractItemModel*(*)>(_a[1]))); break;
        case 2: _t->setRootIndex((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KDGantt::AbstractGrid::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDGantt::AbstractGrid::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_KDGantt__AbstractGrid,
      qt_meta_data_KDGantt__AbstractGrid, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDGantt::AbstractGrid::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDGantt::AbstractGrid::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDGantt::AbstractGrid::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDGantt__AbstractGrid))
        return static_cast<void*>(const_cast< AbstractGrid*>(this));
    return QObject::qt_metacast(_clname);
}

int KDGantt::AbstractGrid::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void KDGantt::AbstractGrid::gridChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
