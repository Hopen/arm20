/****************************************************************************
** Meta object code from reading C++ file 'KDChartDiagramObserver.h'
**
** Created: Mon 17. Mar 15:11:59 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../KDChartDiagramObserver.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KDChartDiagramObserver.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KDChart__DiagramObserver[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      34,   26,   25,   25, 0x05,
      69,   26,   25,   25, 0x05,
     106,   26,   25,   25, 0x05,
     142,   26,   25,   25, 0x05,

 // slots: signature, parameters, type, tag, flags
     185,   25,   25,   25, 0x08,
     212,  209,   25,   25, 0x08,
     261,  259,   25,   25, 0x08,
     302,   25,   25,   25, 0x08,
     320,   25,   25,   25, 0x08,
     337,   25,   25,   25, 0x08,
     361,  259,   25,   25, 0x08,
     408,   25,   25,   25, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_KDChart__DiagramObserver[] = {
    "KDChart::DiagramObserver\0\0diagram\0"
    "diagramDestroyed(AbstractDiagram*)\0"
    "diagramDataChanged(AbstractDiagram*)\0"
    "diagramDataHidden(AbstractDiagram*)\0"
    "diagramAttributesChanged(AbstractDiagram*)\0"
    "slotDestroyed(QObject*)\0,,\0"
    "slotHeaderDataChanged(Qt::Orientation,int,int)\0"
    ",\0slotDataChanged(QModelIndex,QModelIndex)\0"
    "slotDataChanged()\0slotDataHidden()\0"
    "slotAttributesChanged()\0"
    "slotAttributesChanged(QModelIndex,QModelIndex)\0"
    "slotModelsChanged()\0"
};

void KDChart::DiagramObserver::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DiagramObserver *_t = static_cast<DiagramObserver *>(_o);
        switch (_id) {
        case 0: _t->diagramDestroyed((*reinterpret_cast< AbstractDiagram*(*)>(_a[1]))); break;
        case 1: _t->diagramDataChanged((*reinterpret_cast< AbstractDiagram*(*)>(_a[1]))); break;
        case 2: _t->diagramDataHidden((*reinterpret_cast< AbstractDiagram*(*)>(_a[1]))); break;
        case 3: _t->diagramAttributesChanged((*reinterpret_cast< AbstractDiagram*(*)>(_a[1]))); break;
        case 4: _t->slotDestroyed((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 5: _t->slotHeaderDataChanged((*reinterpret_cast< Qt::Orientation(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 6: _t->slotDataChanged((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< QModelIndex(*)>(_a[2]))); break;
        case 7: _t->slotDataChanged(); break;
        case 8: _t->slotDataHidden(); break;
        case 9: _t->slotAttributesChanged(); break;
        case 10: _t->slotAttributesChanged((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< QModelIndex(*)>(_a[2]))); break;
        case 11: _t->slotModelsChanged(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KDChart::DiagramObserver::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KDChart::DiagramObserver::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_KDChart__DiagramObserver,
      qt_meta_data_KDChart__DiagramObserver, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KDChart::DiagramObserver::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KDChart::DiagramObserver::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KDChart::DiagramObserver::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KDChart__DiagramObserver))
        return static_cast<void*>(const_cast< DiagramObserver*>(this));
    return QObject::qt_metacast(_clname);
}

int KDChart::DiagramObserver::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void KDChart::DiagramObserver::diagramDestroyed(AbstractDiagram * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void KDChart::DiagramObserver::diagramDataChanged(AbstractDiagram * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void KDChart::DiagramObserver::diagramDataHidden(AbstractDiagram * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void KDChart::DiagramObserver::diagramAttributesChanged(AbstractDiagram * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE
