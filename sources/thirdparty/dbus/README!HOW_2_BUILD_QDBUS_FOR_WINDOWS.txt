������ QDBus ��� Windows
������ DBUS
1.	��������� � ������������� QtSDK .������ ��� ����� MinGW � QTCreator.
2.	����������� ���� �� MinGW � ���������� ��������� path
3.	��������� � ������������� CMake (http://www.cmake.org/cmake/resources/software.html)
4.	��������� � ������������� libexpat 
5.	��������� ���������� dbus (http://cgit.freedesktop.org/dbus/dbus/snapshot/dbus-1.4.0.zip). ������������� � ������, �������� c:\dbus-1.4.0.
6.	�����������  dbus-1.4.0\cmake\CMakeLists.txt, ��������� � ����� ������:
set(LIBEXPAT_LIBRARIES "C:/Program Files/win32libs/lib/libexpat.lib")
set(LIBEXPAT_INCLUDE_DIR "C:/Program Files/win32libs/include")
set(LIBEXPAT_FOUND ON)
����� ������ 
project(dbus)

7.	������� ����������, ���� ����� ������������� ������, �������� c:\dbus
8.	������� � ���������� ���� dbus-arch-deps.h.in � ������� �� ����� ����� �.in�, ������������ ��� ������, ����� ����������� ����� dbus_int32_t � dbus_uint32_t � �������� ����� �� �����������, ����� ���������� ���������:
typedef int dbus_int32_t;
typedef unsigned int dbus_uint32_t;

typedef short dbus_int16_t;
typedef unsigned short dbus_uint16_t;

9.	������� � ��� � ��������� ������� cmake -G "MinGW Makefiles" ..\dbus-1.4.0\cmake -DDBUS_USE_EXPAT=on. ���� MinGW �������� ���������, �� ������ �������� ������. ��������! ���������� ������!
��������, � ���� � ����� dbus-file-win.c ���������� ���������� ��������� �������, ��� � ������� _dbus_make_file_world_readable �� ����������� ���������� ����������, ������ ��������� �,&error�. 
10.	�������. mingw32-make. ��������� ����� bin. ������ ��������� libdbus-1.dll � libdbus-1.dll.a

������ QDBUS

11.	��������� source QT (http://qt-project.org/downloads), �������������.
12.	� ������������� �������� ���� src/ dbus/ dbus.pro. ����. ������ ����������� QtCreator (���� �� ����������, ������ ������������ ���������� � ����������� �*.pro�). 
13.	������ ����� �������� ����������. ���� � ���, ��� �� ��������� QtCreator ���������� ���������� �� �������������� SDK, � �������, � ��� ��������������� �� ���� ������ ���������, ����������� ������ �������� ����������� ��� ���������� QDBus ������. ������� � ���� ����������/���������. �� ������� Qt4 �������� �����  �������. � ����������� ���� ���� qmake.exe �� ����� �������������� �������� ����������.  ���� OK, ����� ���� �� ������� ������� �������� ��������� �������. ������ ��� ������ ���������� ����� ������������ ����� ����������, � �� SDK. 
14.	� QtCreator�� ��������� ���� dbus.pro, ��������� ������ 
INCLUDEPATH += �:/dbus-1.4.0
INCLUDEPATH += ../../include/QtCore
INCLUDEPATH += �:/dbus
win32 { 
    wince*:LIBS_PRIVATE += -lws2
    else:LIBS_PRIVATE += -lws2_32 \
        -ladvapi32 \
        -lnetapi32 \
        -luser32
    CONFIG(debug  , debug|release):LIBS_PRIVATE += -ldbus-1 -L�:/dbus/bin
    CONFIG(release, debug|release):LIBS_PRIVATE += -ldbus-1 -L�:/dbus/bin
    else:LIBS_PRIVATE += -ldbus-1
}
15.	��������� ���� qdbusmacros.h, ��������� ����������� ������:
#undef QT_NO_DBUS
#define QT_LINKED_LIBDBUS
16.	��������� ������. ���� ��������� ������ �� ����������� ���� dbus_uint64_t (� ����� �� �����), ������ �������������� ��� ��� ��, � qdbusmacros.h: typedef quint64 dbus_uint64_t;
17.	���� � ����� lib, �������� ���������� libQtDBus4.a, libQtDBusd4.a, QtDBus4.dll � QtDBusd4.dll.
18.	������, ���������� ����� � �������, � ������� ����� ������������ ����������� QDBus. ��� ����� � ������� *.pro ���������, ��� ������ ����� ������������� � ����������� dbus � ��������� ������:
QT += dbus
INCLUDEPATH+= �:/Qt_4_8_3/lib
windows {
CONFIG(debug,debug|release): LIBS+=-lQtDBusd4 -L�:/Qt_4_8_3/lib
CONFIG(release,debug|release): LIBS+=-lQtDBus4 -L�:/Qt_4_8_3/lib
}

