#ifndef _QCA_OSSL_H
#define _QCA_OSSL_H

#ifdef QT_NODLL

#include <qca_core.h>
#include <qcaprovider.h>

using namespace QCA;

class opensslPlugin : public QObject, public QCAPlugin
{
	Q_OBJECT
	Q_INTERFACES(QCAPlugin)
public:
	virtual Provider *createProvider(); 
};

#endif // QT_NODLL

#endif // _QCA_OSSL_H
