/****************************************************************************
** Meta object code from reading C++ file 'qca_core.h'
**
** Created: Wed 5. Mar 11:53:21 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../include/QtCrypto/qca_core.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qca_core.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QCA__Provider__Context[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__Provider__Context[] = {
    "QCA::Provider::Context\0"
};

void QCA::Provider::Context::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::Provider::Context::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::Provider::Context::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QCA__Provider__Context,
      qt_meta_data_QCA__Provider__Context, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::Provider::Context::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::Provider::Context::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::Provider::Context::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__Provider__Context))
        return static_cast<void*>(const_cast< Context*>(this));
    return QObject::qt_metacast(_clname);
}

int QCA::Provider::Context::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__BasicContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__BasicContext[] = {
    "QCA::BasicContext\0"
};

void QCA::BasicContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::BasicContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::BasicContext::staticMetaObject = {
    { &Provider::Context::staticMetaObject, qt_meta_stringdata_QCA__BasicContext,
      qt_meta_data_QCA__BasicContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::BasicContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::BasicContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::BasicContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__BasicContext))
        return static_cast<void*>(const_cast< BasicContext*>(this));
    typedef Provider::Context QMocSuperClass;
    return QMocSuperClass::qt_metacast(_clname);
}

int QCA::BasicContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    typedef Provider::Context QMocSuperClass;
    _id = QMocSuperClass::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__EventHandler[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      30,   19,   18,   18, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QCA__EventHandler[] = {
    "QCA::EventHandler\0\0id,context\0"
    "eventReady(int,QCA::Event)\0"
};

void QCA::EventHandler::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        EventHandler *_t = static_cast<EventHandler *>(_o);
        switch (_id) {
        case 0: _t->eventReady((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QCA::Event(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QCA::EventHandler::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::EventHandler::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QCA__EventHandler,
      qt_meta_data_QCA__EventHandler, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::EventHandler::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::EventHandler::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::EventHandler::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__EventHandler))
        return static_cast<void*>(const_cast< EventHandler*>(this));
    return QObject::qt_metacast(_clname);
}

int QCA::EventHandler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void QCA::EventHandler::eventReady(int _t1, const QCA::Event & _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
static const uint qt_meta_data_QCA__PasswordAsker[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      20,   19,   19,   19, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QCA__PasswordAsker[] = {
    "QCA::PasswordAsker\0\0responseReady()\0"
};

void QCA::PasswordAsker::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PasswordAsker *_t = static_cast<PasswordAsker *>(_o);
        switch (_id) {
        case 0: _t->responseReady(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::PasswordAsker::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::PasswordAsker::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QCA__PasswordAsker,
      qt_meta_data_QCA__PasswordAsker, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::PasswordAsker::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::PasswordAsker::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::PasswordAsker::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__PasswordAsker))
        return static_cast<void*>(const_cast< PasswordAsker*>(this));
    return QObject::qt_metacast(_clname);
}

int QCA::PasswordAsker::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void QCA::PasswordAsker::responseReady()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
static const uint qt_meta_data_QCA__TokenAsker[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QCA__TokenAsker[] = {
    "QCA::TokenAsker\0\0responseReady()\0"
};

void QCA::TokenAsker::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TokenAsker *_t = static_cast<TokenAsker *>(_o);
        switch (_id) {
        case 0: _t->responseReady(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::TokenAsker::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::TokenAsker::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QCA__TokenAsker,
      qt_meta_data_QCA__TokenAsker, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::TokenAsker::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::TokenAsker::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::TokenAsker::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__TokenAsker))
        return static_cast<void*>(const_cast< TokenAsker*>(this));
    return QObject::qt_metacast(_clname);
}

int QCA::TokenAsker::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void QCA::TokenAsker::responseReady()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
