/****************************************************************************
** Meta object code from reading C++ file 'qcaprovider.h'
**
** Created: Wed 5. Mar 11:53:33 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../include/QtCrypto/qcaprovider.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qcaprovider.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QCA__InfoContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__InfoContext[] = {
    "QCA::InfoContext\0"
};

void QCA::InfoContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::InfoContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::InfoContext::staticMetaObject = {
    { &BasicContext::staticMetaObject, qt_meta_stringdata_QCA__InfoContext,
      qt_meta_data_QCA__InfoContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::InfoContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::InfoContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::InfoContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__InfoContext))
        return static_cast<void*>(const_cast< InfoContext*>(this));
    return BasicContext::qt_metacast(_clname);
}

int QCA::InfoContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicContext::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__RandomContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__RandomContext[] = {
    "QCA::RandomContext\0"
};

void QCA::RandomContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::RandomContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::RandomContext::staticMetaObject = {
    { &BasicContext::staticMetaObject, qt_meta_stringdata_QCA__RandomContext,
      qt_meta_data_QCA__RandomContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::RandomContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::RandomContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::RandomContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__RandomContext))
        return static_cast<void*>(const_cast< RandomContext*>(this));
    return BasicContext::qt_metacast(_clname);
}

int QCA::RandomContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicContext::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__HashContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__HashContext[] = {
    "QCA::HashContext\0"
};

void QCA::HashContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::HashContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::HashContext::staticMetaObject = {
    { &BasicContext::staticMetaObject, qt_meta_stringdata_QCA__HashContext,
      qt_meta_data_QCA__HashContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::HashContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::HashContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::HashContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__HashContext))
        return static_cast<void*>(const_cast< HashContext*>(this));
    return BasicContext::qt_metacast(_clname);
}

int QCA::HashContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicContext::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__CipherContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__CipherContext[] = {
    "QCA::CipherContext\0"
};

void QCA::CipherContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::CipherContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::CipherContext::staticMetaObject = {
    { &BasicContext::staticMetaObject, qt_meta_stringdata_QCA__CipherContext,
      qt_meta_data_QCA__CipherContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::CipherContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::CipherContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::CipherContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__CipherContext))
        return static_cast<void*>(const_cast< CipherContext*>(this));
    return BasicContext::qt_metacast(_clname);
}

int QCA::CipherContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicContext::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__MACContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__MACContext[] = {
    "QCA::MACContext\0"
};

void QCA::MACContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::MACContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::MACContext::staticMetaObject = {
    { &BasicContext::staticMetaObject, qt_meta_stringdata_QCA__MACContext,
      qt_meta_data_QCA__MACContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::MACContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::MACContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::MACContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__MACContext))
        return static_cast<void*>(const_cast< MACContext*>(this));
    return BasicContext::qt_metacast(_clname);
}

int QCA::MACContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicContext::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__KDFContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__KDFContext[] = {
    "QCA::KDFContext\0"
};

void QCA::KDFContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::KDFContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::KDFContext::staticMetaObject = {
    { &BasicContext::staticMetaObject, qt_meta_stringdata_QCA__KDFContext,
      qt_meta_data_QCA__KDFContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::KDFContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::KDFContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::KDFContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__KDFContext))
        return static_cast<void*>(const_cast< KDFContext*>(this));
    return BasicContext::qt_metacast(_clname);
}

int QCA::KDFContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicContext::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__DLGroupContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      21,   20,   20,   20, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QCA__DLGroupContext[] = {
    "QCA::DLGroupContext\0\0finished()\0"
};

void QCA::DLGroupContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DLGroupContext *_t = static_cast<DLGroupContext *>(_o);
        switch (_id) {
        case 0: _t->finished(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::DLGroupContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::DLGroupContext::staticMetaObject = {
    { &Provider::Context::staticMetaObject, qt_meta_stringdata_QCA__DLGroupContext,
      qt_meta_data_QCA__DLGroupContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::DLGroupContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::DLGroupContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::DLGroupContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__DLGroupContext))
        return static_cast<void*>(const_cast< DLGroupContext*>(this));
    typedef Provider::Context QMocSuperClass;
    return QMocSuperClass::qt_metacast(_clname);
}

int QCA::DLGroupContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    typedef Provider::Context QMocSuperClass;
    _id = QMocSuperClass::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void QCA::DLGroupContext::finished()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
static const uint qt_meta_data_QCA__PKeyBase[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QCA__PKeyBase[] = {
    "QCA::PKeyBase\0\0finished()\0"
};

void QCA::PKeyBase::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PKeyBase *_t = static_cast<PKeyBase *>(_o);
        switch (_id) {
        case 0: _t->finished(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::PKeyBase::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::PKeyBase::staticMetaObject = {
    { &BasicContext::staticMetaObject, qt_meta_stringdata_QCA__PKeyBase,
      qt_meta_data_QCA__PKeyBase, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::PKeyBase::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::PKeyBase::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::PKeyBase::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__PKeyBase))
        return static_cast<void*>(const_cast< PKeyBase*>(this));
    return BasicContext::qt_metacast(_clname);
}

int QCA::PKeyBase::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicContext::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void QCA::PKeyBase::finished()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
static const uint qt_meta_data_QCA__RSAContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__RSAContext[] = {
    "QCA::RSAContext\0"
};

void QCA::RSAContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::RSAContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::RSAContext::staticMetaObject = {
    { &PKeyBase::staticMetaObject, qt_meta_stringdata_QCA__RSAContext,
      qt_meta_data_QCA__RSAContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::RSAContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::RSAContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::RSAContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__RSAContext))
        return static_cast<void*>(const_cast< RSAContext*>(this));
    return PKeyBase::qt_metacast(_clname);
}

int QCA::RSAContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PKeyBase::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__DSAContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__DSAContext[] = {
    "QCA::DSAContext\0"
};

void QCA::DSAContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::DSAContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::DSAContext::staticMetaObject = {
    { &PKeyBase::staticMetaObject, qt_meta_stringdata_QCA__DSAContext,
      qt_meta_data_QCA__DSAContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::DSAContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::DSAContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::DSAContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__DSAContext))
        return static_cast<void*>(const_cast< DSAContext*>(this));
    return PKeyBase::qt_metacast(_clname);
}

int QCA::DSAContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PKeyBase::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__DHContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__DHContext[] = {
    "QCA::DHContext\0"
};

void QCA::DHContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::DHContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::DHContext::staticMetaObject = {
    { &PKeyBase::staticMetaObject, qt_meta_stringdata_QCA__DHContext,
      qt_meta_data_QCA__DHContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::DHContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::DHContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::DHContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__DHContext))
        return static_cast<void*>(const_cast< DHContext*>(this));
    return PKeyBase::qt_metacast(_clname);
}

int QCA::DHContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PKeyBase::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__PKeyContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__PKeyContext[] = {
    "QCA::PKeyContext\0"
};

void QCA::PKeyContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::PKeyContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::PKeyContext::staticMetaObject = {
    { &BasicContext::staticMetaObject, qt_meta_stringdata_QCA__PKeyContext,
      qt_meta_data_QCA__PKeyContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::PKeyContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::PKeyContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::PKeyContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__PKeyContext))
        return static_cast<void*>(const_cast< PKeyContext*>(this));
    return BasicContext::qt_metacast(_clname);
}

int QCA::PKeyContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicContext::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__CertBase[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__CertBase[] = {
    "QCA::CertBase\0"
};

void QCA::CertBase::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::CertBase::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::CertBase::staticMetaObject = {
    { &BasicContext::staticMetaObject, qt_meta_stringdata_QCA__CertBase,
      qt_meta_data_QCA__CertBase, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::CertBase::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::CertBase::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::CertBase::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__CertBase))
        return static_cast<void*>(const_cast< CertBase*>(this));
    return BasicContext::qt_metacast(_clname);
}

int QCA::CertBase::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicContext::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__CertContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__CertContext[] = {
    "QCA::CertContext\0"
};

void QCA::CertContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::CertContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::CertContext::staticMetaObject = {
    { &CertBase::staticMetaObject, qt_meta_stringdata_QCA__CertContext,
      qt_meta_data_QCA__CertContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::CertContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::CertContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::CertContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__CertContext))
        return static_cast<void*>(const_cast< CertContext*>(this));
    return CertBase::qt_metacast(_clname);
}

int QCA::CertContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CertBase::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__CSRContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__CSRContext[] = {
    "QCA::CSRContext\0"
};

void QCA::CSRContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::CSRContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::CSRContext::staticMetaObject = {
    { &CertBase::staticMetaObject, qt_meta_stringdata_QCA__CSRContext,
      qt_meta_data_QCA__CSRContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::CSRContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::CSRContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::CSRContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__CSRContext))
        return static_cast<void*>(const_cast< CSRContext*>(this));
    return CertBase::qt_metacast(_clname);
}

int QCA::CSRContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CertBase::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__CRLContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__CRLContext[] = {
    "QCA::CRLContext\0"
};

void QCA::CRLContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::CRLContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::CRLContext::staticMetaObject = {
    { &CertBase::staticMetaObject, qt_meta_stringdata_QCA__CRLContext,
      qt_meta_data_QCA__CRLContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::CRLContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::CRLContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::CRLContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__CRLContext))
        return static_cast<void*>(const_cast< CRLContext*>(this));
    return CertBase::qt_metacast(_clname);
}

int QCA::CRLContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CertBase::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__CertCollectionContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__CertCollectionContext[] = {
    "QCA::CertCollectionContext\0"
};

void QCA::CertCollectionContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::CertCollectionContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::CertCollectionContext::staticMetaObject = {
    { &BasicContext::staticMetaObject, qt_meta_stringdata_QCA__CertCollectionContext,
      qt_meta_data_QCA__CertCollectionContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::CertCollectionContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::CertCollectionContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::CertCollectionContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__CertCollectionContext))
        return static_cast<void*>(const_cast< CertCollectionContext*>(this));
    return BasicContext::qt_metacast(_clname);
}

int QCA::CertCollectionContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicContext::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__CAContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__CAContext[] = {
    "QCA::CAContext\0"
};

void QCA::CAContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::CAContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::CAContext::staticMetaObject = {
    { &BasicContext::staticMetaObject, qt_meta_stringdata_QCA__CAContext,
      qt_meta_data_QCA__CAContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::CAContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::CAContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::CAContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__CAContext))
        return static_cast<void*>(const_cast< CAContext*>(this));
    return BasicContext::qt_metacast(_clname);
}

int QCA::CAContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicContext::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__PKCS12Context[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__PKCS12Context[] = {
    "QCA::PKCS12Context\0"
};

void QCA::PKCS12Context::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::PKCS12Context::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::PKCS12Context::staticMetaObject = {
    { &BasicContext::staticMetaObject, qt_meta_stringdata_QCA__PKCS12Context,
      qt_meta_data_QCA__PKCS12Context, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::PKCS12Context::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::PKCS12Context::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::PKCS12Context::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__PKCS12Context))
        return static_cast<void*>(const_cast< PKCS12Context*>(this));
    return BasicContext::qt_metacast(_clname);
}

int QCA::PKCS12Context::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicContext::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__PGPKeyContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__PGPKeyContext[] = {
    "QCA::PGPKeyContext\0"
};

void QCA::PGPKeyContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::PGPKeyContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::PGPKeyContext::staticMetaObject = {
    { &BasicContext::staticMetaObject, qt_meta_stringdata_QCA__PGPKeyContext,
      qt_meta_data_QCA__PGPKeyContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::PGPKeyContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::PGPKeyContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::PGPKeyContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__PGPKeyContext))
        return static_cast<void*>(const_cast< PGPKeyContext*>(this));
    return BasicContext::qt_metacast(_clname);
}

int QCA::PGPKeyContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicContext::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__KeyStoreEntryContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__KeyStoreEntryContext[] = {
    "QCA::KeyStoreEntryContext\0"
};

void QCA::KeyStoreEntryContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::KeyStoreEntryContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::KeyStoreEntryContext::staticMetaObject = {
    { &BasicContext::staticMetaObject, qt_meta_stringdata_QCA__KeyStoreEntryContext,
      qt_meta_data_QCA__KeyStoreEntryContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::KeyStoreEntryContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::KeyStoreEntryContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::KeyStoreEntryContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__KeyStoreEntryContext))
        return static_cast<void*>(const_cast< KeyStoreEntryContext*>(this));
    return BasicContext::qt_metacast(_clname);
}

int QCA::KeyStoreEntryContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicContext::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__KeyStoreListContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: signature, parameters, type, tag, flags
      26,   25,   25,   25, 0x05,
      38,   25,   25,   25, 0x05,
      48,   25,   25,   25, 0x05,
      62,   58,   25,   25, 0x05,
      89,   86,   25,   25, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QCA__KeyStoreListContext[] = {
    "QCA::KeyStoreListContext\0\0busyStart()\0"
    "busyEnd()\0updated()\0str\0diagnosticText(QString)\0"
    "id\0storeUpdated(int)\0"
};

void QCA::KeyStoreListContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        KeyStoreListContext *_t = static_cast<KeyStoreListContext *>(_o);
        switch (_id) {
        case 0: _t->busyStart(); break;
        case 1: _t->busyEnd(); break;
        case 2: _t->updated(); break;
        case 3: _t->diagnosticText((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->storeUpdated((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QCA::KeyStoreListContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::KeyStoreListContext::staticMetaObject = {
    { &Provider::Context::staticMetaObject, qt_meta_stringdata_QCA__KeyStoreListContext,
      qt_meta_data_QCA__KeyStoreListContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::KeyStoreListContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::KeyStoreListContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::KeyStoreListContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__KeyStoreListContext))
        return static_cast<void*>(const_cast< KeyStoreListContext*>(this));
    typedef Provider::Context QMocSuperClass;
    return QMocSuperClass::qt_metacast(_clname);
}

int QCA::KeyStoreListContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    typedef Provider::Context QMocSuperClass;
    _id = QMocSuperClass::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void QCA::KeyStoreListContext::busyStart()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void QCA::KeyStoreListContext::busyEnd()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void QCA::KeyStoreListContext::updated()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void QCA::KeyStoreListContext::diagnosticText(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QCA::KeyStoreListContext::storeUpdated(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
static const uint qt_meta_data_QCA__TLSSessionContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__TLSSessionContext[] = {
    "QCA::TLSSessionContext\0"
};

void QCA::TLSSessionContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::TLSSessionContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::TLSSessionContext::staticMetaObject = {
    { &BasicContext::staticMetaObject, qt_meta_stringdata_QCA__TLSSessionContext,
      qt_meta_data_QCA__TLSSessionContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::TLSSessionContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::TLSSessionContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::TLSSessionContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__TLSSessionContext))
        return static_cast<void*>(const_cast< TLSSessionContext*>(this));
    return BasicContext::qt_metacast(_clname);
}

int QCA::TLSSessionContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicContext::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__TLSContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x05,
      32,   16,   16,   16, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QCA__TLSContext[] = {
    "QCA::TLSContext\0\0resultsReady()\0"
    "dtlsTimeout()\0"
};

void QCA::TLSContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TLSContext *_t = static_cast<TLSContext *>(_o);
        switch (_id) {
        case 0: _t->resultsReady(); break;
        case 1: _t->dtlsTimeout(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::TLSContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::TLSContext::staticMetaObject = {
    { &Provider::Context::staticMetaObject, qt_meta_stringdata_QCA__TLSContext,
      qt_meta_data_QCA__TLSContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::TLSContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::TLSContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::TLSContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__TLSContext))
        return static_cast<void*>(const_cast< TLSContext*>(this));
    typedef Provider::Context QMocSuperClass;
    return QMocSuperClass::qt_metacast(_clname);
}

int QCA::TLSContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    typedef Provider::Context QMocSuperClass;
    _id = QMocSuperClass::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void QCA::TLSContext::resultsReady()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void QCA::TLSContext::dtlsTimeout()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
static const uint qt_meta_data_QCA__SASLContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      18,   17,   17,   17, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QCA__SASLContext[] = {
    "QCA::SASLContext\0\0resultsReady()\0"
};

void QCA::SASLContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SASLContext *_t = static_cast<SASLContext *>(_o);
        switch (_id) {
        case 0: _t->resultsReady(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::SASLContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::SASLContext::staticMetaObject = {
    { &Provider::Context::staticMetaObject, qt_meta_stringdata_QCA__SASLContext,
      qt_meta_data_QCA__SASLContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::SASLContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::SASLContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::SASLContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__SASLContext))
        return static_cast<void*>(const_cast< SASLContext*>(this));
    typedef Provider::Context QMocSuperClass;
    return QMocSuperClass::qt_metacast(_clname);
}

int QCA::SASLContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    typedef Provider::Context QMocSuperClass;
    _id = QMocSuperClass::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void QCA::SASLContext::resultsReady()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
static const uint qt_meta_data_QCA__MessageContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      21,   20,   20,   20, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QCA__MessageContext[] = {
    "QCA::MessageContext\0\0updated()\0"
};

void QCA::MessageContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MessageContext *_t = static_cast<MessageContext *>(_o);
        switch (_id) {
        case 0: _t->updated(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::MessageContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::MessageContext::staticMetaObject = {
    { &Provider::Context::staticMetaObject, qt_meta_stringdata_QCA__MessageContext,
      qt_meta_data_QCA__MessageContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::MessageContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::MessageContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::MessageContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__MessageContext))
        return static_cast<void*>(const_cast< MessageContext*>(this));
    typedef Provider::Context QMocSuperClass;
    return QMocSuperClass::qt_metacast(_clname);
}

int QCA::MessageContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    typedef Provider::Context QMocSuperClass;
    _id = QMocSuperClass::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void QCA::MessageContext::updated()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
static const uint qt_meta_data_QCA__SMSContext[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__SMSContext[] = {
    "QCA::SMSContext\0"
};

void QCA::SMSContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::SMSContext::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::SMSContext::staticMetaObject = {
    { &BasicContext::staticMetaObject, qt_meta_stringdata_QCA__SMSContext,
      qt_meta_data_QCA__SMSContext, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::SMSContext::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::SMSContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::SMSContext::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__SMSContext))
        return static_cast<void*>(const_cast< SMSContext*>(this));
    return BasicContext::qt_metacast(_clname);
}

int QCA::SMSContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicContext::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
