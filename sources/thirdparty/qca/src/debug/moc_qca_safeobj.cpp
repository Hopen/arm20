/****************************************************************************
** Meta object code from reading C++ file 'qca_safeobj.h'
**
** Created: Wed 27. Mar 16:54:09 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qca_safeobj.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qca_safeobj.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QCA__SafeTimer[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x05,

 // slots: signature, parameters, type, tag, flags
      31,   26,   15,   15, 0x0a,
      42,   15,   15,   15, 0x0a,
      50,   15,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_QCA__SafeTimer[] = {
    "QCA::SafeTimer\0\0timeout()\0msec\0"
    "start(int)\0start()\0stop()\0"
};

void QCA::SafeTimer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SafeTimer *_t = static_cast<SafeTimer *>(_o);
        switch (_id) {
        case 0: _t->timeout(); break;
        case 1: _t->start((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->start(); break;
        case 3: _t->stop(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QCA::SafeTimer::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::SafeTimer::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QCA__SafeTimer,
      qt_meta_data_QCA__SafeTimer, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::SafeTimer::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::SafeTimer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::SafeTimer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__SafeTimer))
        return static_cast<void*>(const_cast< SafeTimer*>(this));
    return QObject::qt_metacast(_clname);
}

int QCA::SafeTimer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void QCA::SafeTimer::timeout()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
static const uint qt_meta_data_QCA__SafeSocketNotifier[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      32,   25,   24,   24, 0x05,

 // slots: signature, parameters, type, tag, flags
      54,   47,   24,   24, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_QCA__SafeSocketNotifier[] = {
    "QCA::SafeSocketNotifier\0\0socket\0"
    "activated(int)\0enable\0setEnabled(bool)\0"
};

void QCA::SafeSocketNotifier::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SafeSocketNotifier *_t = static_cast<SafeSocketNotifier *>(_o);
        switch (_id) {
        case 0: _t->activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->setEnabled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QCA::SafeSocketNotifier::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::SafeSocketNotifier::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QCA__SafeSocketNotifier,
      qt_meta_data_QCA__SafeSocketNotifier, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::SafeSocketNotifier::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::SafeSocketNotifier::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::SafeSocketNotifier::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__SafeSocketNotifier))
        return static_cast<void*>(const_cast< SafeSocketNotifier*>(this));
    return QObject::qt_metacast(_clname);
}

int QCA::SafeSocketNotifier::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void QCA::SafeSocketNotifier::activated(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
