QT += network
TARGET = thirdparty
TEMPLATE = lib
DESTDIR = ../bin
#CONFIG += staticlib


contains( CONFIG, static) {
  CONFIG  += staticlib
  DEFINES += QT_NODLL
} else {
  CONFIG  += dll
}

INCLUDEPATH += psi/src/tools
include("psi/src/tools/globalshortcut/globalshortcut.pri")
include("psi/src/tools/grepshortcutkeydialog/grepshortcutkeydialog.pri")
include("log4qt/src/log4qt/log4qt.pri")


#LIBS += -Ld:/Qt_source_4.8.3/lib
#windows {
#CONFIG(debug,debug|release): LIBS+=-lQtCored4 -Ld:/Qt_source_4.8.3/lib
#CONFIG(release,debug|release): LIBS+=-lQtCore4 -Ld:/Qt_source_4.8.3/lib
#}
