HEADERS += $$PWD/globalshortcutmanager.h $$PWD/globalshortcuttrigger.h
SOURCES += $$PWD/globalshortcutmanager.cpp
INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD
#CONFIG += staticlib

unix:!mac {
        SOURCES += $$PWD/globalshortcutmanager_x11.cpp
}
win32: {
        #HEADERS += $$PWD/globalshortcutmanager_win.h
        SOURCES += $$PWD/globalshortcutmanager_win.cpp
}
mac: {
        SOURCES += $$PWD/globalshortcutmanager_mac.cpp
}

