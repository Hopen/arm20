#ifndef GLOBALSHORTCUTMANAGER_WIN_H
#define GLOBALSHORTCUTMANAGER_WIN_H

#include "globalshortcutmanager.h"
#include "globalshortcuttrigger.h"

#include <QWidget>

#include <windows.h>

class GlobalShortcutManager::KeyTrigger::Impl : public QWidget
{
    Q_OBJECT
public:
        /**
         * Constructor registers the hotkey.
         */
        Impl(GlobalShortcutManager::KeyTrigger* t, const QKeySequence& ks);
//                : trigger_(t)
//                , id_(0)
//        {
//        UINT mod, key;
//                _winId = winId();
//        if (convertKeySequence(ks, &mod, &key))
//                        if (RegisterHotKey(_winId, nextId, mod, key))
//                id_ = nextId++;
//        }

        /**
         * Destructor unregisters the hotkey.
         */
        virtual ~Impl()
        {
        if (id_)
                        UnregisterHotKey(_winId, id_);
        }

        /**
         * Triggers activated() signal when the hotkey is activated.
         */
        bool winEvent(MSG* m, long* result)
        {
                if (m->message == WM_HOTKEY &&
                    m->wParam == id_) {
                        emit trigger_->activated();
                        return true;
                }
                return QWidget::winEvent(m, result);
        }

private:
        KeyTrigger* trigger_;
        quint32 id_;
        static quint32 nextId;
        WId _winId;

private:
        struct Qt_VK_Keymap
        {
                int key;
                UINT vk;
        };
        static Qt_VK_Keymap qt_vk_table[];

        static bool convertKeySequence(const QKeySequence& ks, UINT* mod_, UINT* key_)
        {
                int code = ks;

                UINT mod = 0;
                if (code & Qt::META)
                        mod |= MOD_WIN;
                if (code & Qt::SHIFT)
                        mod |= MOD_SHIFT;
                if (code & Qt::CTRL)
                        mod |= MOD_CONTROL;
                if (code & Qt::ALT)
                        mod |= MOD_ALT;

                UINT key = 0;
                code &= ~Qt::KeyboardModifierMask;
                if (code >= 0x20 && code <= 0x7f)
                        key = code;
                else {
                        for (int n = 0; qt_vk_table[n].key != Qt::Key_unknown; ++n) {
                                if (qt_vk_table[n].key == code) {
                                        key = qt_vk_table[n].vk;
                                        break;
                                }
                        }
                        if (!key)
                                return false;
                }

                if (mod)
                        *mod_ = mod;
                if (key)
                        *key_ = key;

                return true;
        }
};

#endif // GLOBALSHORTCUTMANAGER_WIN_H
