/****************************************************************************
** Meta object code from reading C++ file 'qca_securelayer.h'
**
** Created: Thu 6. Sep 14:06:50 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../qca/include/QtCrypto/qca_securelayer.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qca_securelayer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QCA__SecureLayer[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      18,   17,   17,   17, 0x05,
      30,   17,   17,   17, 0x05,
      50,   17,   17,   17, 0x05,
      59,   17,   17,   17, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QCA__SecureLayer[] = {
    "QCA::SecureLayer\0\0readyRead()\0"
    "readyReadOutgoing()\0closed()\0error()\0"
};

void QCA::SecureLayer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SecureLayer *_t = static_cast<SecureLayer *>(_o);
        switch (_id) {
        case 0: _t->readyRead(); break;
        case 1: _t->readyReadOutgoing(); break;
        case 2: _t->closed(); break;
        case 3: _t->error(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::SecureLayer::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::SecureLayer::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QCA__SecureLayer,
      qt_meta_data_QCA__SecureLayer, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::SecureLayer::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::SecureLayer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::SecureLayer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__SecureLayer))
        return static_cast<void*>(const_cast< SecureLayer*>(this));
    return QObject::qt_metacast(_clname);
}

int QCA::SecureLayer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void QCA::SecureLayer::readyRead()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void QCA::SecureLayer::readyReadOutgoing()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void QCA::SecureLayer::closed()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void QCA::SecureLayer::error()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}
static const uint qt_meta_data_QCA__TLS[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x05,
      29,    9,    9,    9, 0x05,
      52,    9,    9,    9, 0x05,
      79,    9,    9,    9, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QCA__TLS[] = {
    "QCA::TLS\0\0hostNameReceived()\0"
    "certificateRequested()\0"
    "peerCertificateAvailable()\0handshaken()\0"
};

void QCA::TLS::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TLS *_t = static_cast<TLS *>(_o);
        switch (_id) {
        case 0: _t->hostNameReceived(); break;
        case 1: _t->certificateRequested(); break;
        case 2: _t->peerCertificateAvailable(); break;
        case 3: _t->handshaken(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::TLS::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::TLS::staticMetaObject = {
    { &SecureLayer::staticMetaObject, qt_meta_stringdata_QCA__TLS,
      qt_meta_data_QCA__TLS, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::TLS::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::TLS::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::TLS::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__TLS))
        return static_cast<void*>(const_cast< TLS*>(this));
    if (!strcmp(_clname, "Algorithm"))
        return static_cast< Algorithm*>(const_cast< TLS*>(this));
    return SecureLayer::qt_metacast(_clname);
}

int QCA::TLS::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = SecureLayer::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void QCA::TLS::hostNameReceived()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void QCA::TLS::certificateRequested()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void QCA::TLS::peerCertificateAvailable()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void QCA::TLS::handshaken()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}
static const uint qt_meta_data_QCA__SASL[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: signature, parameters, type, tag, flags
      37,   11,   10,   10, 0x05,
      68,   10,   10,   10, 0x05,
      93,   84,   10,   10, 0x05,
     121,  114,   10,   10, 0x05,
     164,  151,   10,   10, 0x05,
     191,   10,   10,   10, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QCA__SASL[] = {
    "QCA::SASL\0\0clientInit,clientInitData\0"
    "clientStarted(bool,QByteArray)\0"
    "serverStarted()\0stepData\0nextStep(QByteArray)\0"
    "params\0needParams(QCA::SASL::Params)\0"
    "user,authzid\0authCheck(QString,QString)\0"
    "authenticated()\0"
};

void QCA::SASL::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SASL *_t = static_cast<SASL *>(_o);
        switch (_id) {
        case 0: _t->clientStarted((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< const QByteArray(*)>(_a[2]))); break;
        case 1: _t->serverStarted(); break;
        case 2: _t->nextStep((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 3: _t->needParams((*reinterpret_cast< const QCA::SASL::Params(*)>(_a[1]))); break;
        case 4: _t->authCheck((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 5: _t->authenticated(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QCA::SASL::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::SASL::staticMetaObject = {
    { &SecureLayer::staticMetaObject, qt_meta_stringdata_QCA__SASL,
      qt_meta_data_QCA__SASL, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::SASL::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::SASL::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::SASL::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__SASL))
        return static_cast<void*>(const_cast< SASL*>(this));
    if (!strcmp(_clname, "Algorithm"))
        return static_cast< Algorithm*>(const_cast< SASL*>(this));
    return SecureLayer::qt_metacast(_clname);
}

int QCA::SASL::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = SecureLayer::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void QCA::SASL::clientStarted(bool _t1, const QByteArray & _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QCA::SASL::serverStarted()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void QCA::SASL::nextStep(const QByteArray & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QCA::SASL::needParams(const QCA::SASL::Params & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QCA::SASL::authCheck(const QString & _t1, const QString & _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QCA::SASL::authenticated()
{
    QMetaObject::activate(this, &staticMetaObject, 5, 0);
}
QT_END_MOC_NAMESPACE
