/****************************************************************************
** Meta object code from reading C++ file 'qca_keystore.h'
**
** Created: Thu 6. Sep 14:06:50 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../qca/include/QtCrypto/qca_keystore.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qca_keystore.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QCA__KeyStoreEntryWatcher[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      27,   26,   26,   26, 0x05,
      39,   26,   26,   26, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QCA__KeyStoreEntryWatcher[] = {
    "QCA::KeyStoreEntryWatcher\0\0available()\0"
    "unavailable()\0"
};

void QCA::KeyStoreEntryWatcher::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        KeyStoreEntryWatcher *_t = static_cast<KeyStoreEntryWatcher *>(_o);
        switch (_id) {
        case 0: _t->available(); break;
        case 1: _t->unavailable(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::KeyStoreEntryWatcher::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::KeyStoreEntryWatcher::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QCA__KeyStoreEntryWatcher,
      qt_meta_data_QCA__KeyStoreEntryWatcher, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::KeyStoreEntryWatcher::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::KeyStoreEntryWatcher::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::KeyStoreEntryWatcher::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__KeyStoreEntryWatcher))
        return static_cast<void*>(const_cast< KeyStoreEntryWatcher*>(this));
    return QObject::qt_metacast(_clname);
}

int QCA::KeyStoreEntryWatcher::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void QCA::KeyStoreEntryWatcher::available()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void QCA::KeyStoreEntryWatcher::unavailable()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
static const uint qt_meta_data_QCA__KeyStore[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x05,
      25,   14,   14,   14, 0x05,
      47,   39,   14,   14, 0x05,
      77,   69,   14,   14, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QCA__KeyStore[] = {
    "QCA::KeyStore\0\0updated()\0unavailable()\0"
    "entryId\0entryWritten(QString)\0success\0"
    "entryRemoved(bool)\0"
};

void QCA::KeyStore::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        KeyStore *_t = static_cast<KeyStore *>(_o);
        switch (_id) {
        case 0: _t->updated(); break;
        case 1: _t->unavailable(); break;
        case 2: _t->entryWritten((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->entryRemoved((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QCA::KeyStore::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::KeyStore::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QCA__KeyStore,
      qt_meta_data_QCA__KeyStore, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::KeyStore::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::KeyStore::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::KeyStore::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__KeyStore))
        return static_cast<void*>(const_cast< KeyStore*>(this));
    if (!strcmp(_clname, "Algorithm"))
        return static_cast< Algorithm*>(const_cast< KeyStore*>(this));
    return QObject::qt_metacast(_clname);
}

int QCA::KeyStore::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void QCA::KeyStore::updated()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void QCA::KeyStore::unavailable()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void QCA::KeyStore::entryWritten(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QCA::KeyStore::entryRemoved(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
static const uint qt_meta_data_QCA__KeyStoreManager[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      22,   21,   21,   21, 0x05,
      36,   21,   21,   21, 0x05,
      54,   51,   21,   21, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QCA__KeyStoreManager[] = {
    "QCA::KeyStoreManager\0\0busyStarted()\0"
    "busyFinished()\0id\0keyStoreAvailable(QString)\0"
};

void QCA::KeyStoreManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        KeyStoreManager *_t = static_cast<KeyStoreManager *>(_o);
        switch (_id) {
        case 0: _t->busyStarted(); break;
        case 1: _t->busyFinished(); break;
        case 2: _t->keyStoreAvailable((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QCA::KeyStoreManager::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::KeyStoreManager::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QCA__KeyStoreManager,
      qt_meta_data_QCA__KeyStoreManager, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::KeyStoreManager::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::KeyStoreManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::KeyStoreManager::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__KeyStoreManager))
        return static_cast<void*>(const_cast< KeyStoreManager*>(this));
    return QObject::qt_metacast(_clname);
}

int QCA::KeyStoreManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void QCA::KeyStoreManager::busyStarted()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void QCA::KeyStoreManager::busyFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void QCA::KeyStoreManager::keyStoreAvailable(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
