/****************************************************************************
** Meta object code from reading C++ file 'qca_securemessage.h'
**
** Created: Thu 6. Sep 14:06:50 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../qca/include/QtCrypto/qca_securemessage.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qca_securemessage.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QCA__SecureMessage[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      20,   19,   19,   19, 0x05,
      38,   32,   19,   19, 0x05,
      56,   19,   19,   19, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QCA__SecureMessage[] = {
    "QCA::SecureMessage\0\0readyRead()\0bytes\0"
    "bytesWritten(int)\0finished()\0"
};

void QCA::SecureMessage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SecureMessage *_t = static_cast<SecureMessage *>(_o);
        switch (_id) {
        case 0: _t->readyRead(); break;
        case 1: _t->bytesWritten((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->finished(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QCA::SecureMessage::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::SecureMessage::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QCA__SecureMessage,
      qt_meta_data_QCA__SecureMessage, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::SecureMessage::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::SecureMessage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::SecureMessage::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__SecureMessage))
        return static_cast<void*>(const_cast< SecureMessage*>(this));
    if (!strcmp(_clname, "Algorithm"))
        return static_cast< Algorithm*>(const_cast< SecureMessage*>(this));
    return QObject::qt_metacast(_clname);
}

int QCA::SecureMessage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void QCA::SecureMessage::readyRead()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void QCA::SecureMessage::bytesWritten(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QCA::SecureMessage::finished()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
static const uint qt_meta_data_QCA__SecureMessageSystem[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__SecureMessageSystem[] = {
    "QCA::SecureMessageSystem\0"
};

void QCA::SecureMessageSystem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::SecureMessageSystem::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::SecureMessageSystem::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QCA__SecureMessageSystem,
      qt_meta_data_QCA__SecureMessageSystem, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::SecureMessageSystem::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::SecureMessageSystem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::SecureMessageSystem::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__SecureMessageSystem))
        return static_cast<void*>(const_cast< SecureMessageSystem*>(this));
    if (!strcmp(_clname, "Algorithm"))
        return static_cast< Algorithm*>(const_cast< SecureMessageSystem*>(this));
    return QObject::qt_metacast(_clname);
}

int QCA::SecureMessageSystem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__OpenPGP[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__OpenPGP[] = {
    "QCA::OpenPGP\0"
};

void QCA::OpenPGP::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::OpenPGP::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::OpenPGP::staticMetaObject = {
    { &SecureMessageSystem::staticMetaObject, qt_meta_stringdata_QCA__OpenPGP,
      qt_meta_data_QCA__OpenPGP, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::OpenPGP::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::OpenPGP::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::OpenPGP::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__OpenPGP))
        return static_cast<void*>(const_cast< OpenPGP*>(this));
    return SecureMessageSystem::qt_metacast(_clname);
}

int QCA::OpenPGP::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = SecureMessageSystem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_QCA__CMS[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QCA__CMS[] = {
    "QCA::CMS\0"
};

void QCA::CMS::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QCA::CMS::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCA::CMS::staticMetaObject = {
    { &SecureMessageSystem::staticMetaObject, qt_meta_stringdata_QCA__CMS,
      qt_meta_data_QCA__CMS, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCA::CMS::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCA::CMS::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCA::CMS::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCA__CMS))
        return static_cast<void*>(const_cast< CMS*>(this));
    return SecureMessageSystem::qt_metacast(_clname);
}

int QCA::CMS::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = SecureMessageSystem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
