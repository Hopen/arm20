/****************************************************************************
** Meta object code from reading C++ file 'QXmppOutgoingServer.h'
**
** Created: Wed 5. Mar 14:05:37 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../server/QXmppOutgoingServer.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QXmppOutgoingServer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QXmppOutgoingServer[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      30,   21,   20,   20, 0x05,

 // slots: signature, parameters, type, tag, flags
      77,   70,   20,   20, 0x0a,
     105,  100,   20,   20, 0x0a,
     127,   20,   20,   20, 0x08,
     150,   20,   20,   20, 0x08,
     174,   20,   20,   20, 0x08,
     196,  189,   20,   20, 0x08,
     234,  228,   20,   20, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_QXmppOutgoingServer[] = {
    "QXmppOutgoingServer\0\0response\0"
    "dialbackResponseReceived(QXmppDialback)\0"
    "domain\0connectToHost(QString)\0data\0"
    "queueData(QByteArray)\0_q_dnsLookupFinished()\0"
    "_q_socketDisconnected()\0sendDialback()\0"
    "errors\0slotSslErrors(QList<QSslError>)\0"
    "error\0socketError(QAbstractSocket::SocketError)\0"
};

void QXmppOutgoingServer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QXmppOutgoingServer *_t = static_cast<QXmppOutgoingServer *>(_o);
        switch (_id) {
        case 0: _t->dialbackResponseReceived((*reinterpret_cast< const QXmppDialback(*)>(_a[1]))); break;
        case 1: _t->connectToHost((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->queueData((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 3: _t->_q_dnsLookupFinished(); break;
        case 4: _t->_q_socketDisconnected(); break;
        case 5: _t->sendDialback(); break;
        case 6: _t->slotSslErrors((*reinterpret_cast< const QList<QSslError>(*)>(_a[1]))); break;
        case 7: _t->socketError((*reinterpret_cast< QAbstractSocket::SocketError(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QXmppOutgoingServer::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QXmppOutgoingServer::staticMetaObject = {
    { &QXmppStream::staticMetaObject, qt_meta_stringdata_QXmppOutgoingServer,
      qt_meta_data_QXmppOutgoingServer, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QXmppOutgoingServer::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QXmppOutgoingServer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QXmppOutgoingServer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QXmppOutgoingServer))
        return static_cast<void*>(const_cast< QXmppOutgoingServer*>(this));
    return QXmppStream::qt_metacast(_clname);
}

int QXmppOutgoingServer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QXmppStream::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void QXmppOutgoingServer::dialbackResponseReceived(const QXmppDialback & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
