/****************************************************************************
** Meta object code from reading C++ file 'QXmppIncomingClient.h'
**
** Created: Wed 5. Mar 14:05:33 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../server/QXmppIncomingClient.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QXmppIncomingClient.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QXmppIncomingClient[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      29,   21,   20,   20, 0x05,

 // slots: signature, parameters, type, tag, flags
      58,   20,   20,   20, 0x08,
      74,   20,   20,   20, 0x08,
      92,   20,   20,   20, 0x08,
     115,   20,   20,   20, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_QXmppIncomingClient[] = {
    "QXmppIncomingClient\0\0element\0"
    "elementReceived(QDomElement)\0"
    "onDigestReply()\0onPasswordReply()\0"
    "onSocketDisconnected()\0onTimeout()\0"
};

void QXmppIncomingClient::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QXmppIncomingClient *_t = static_cast<QXmppIncomingClient *>(_o);
        switch (_id) {
        case 0: _t->elementReceived((*reinterpret_cast< const QDomElement(*)>(_a[1]))); break;
        case 1: _t->onDigestReply(); break;
        case 2: _t->onPasswordReply(); break;
        case 3: _t->onSocketDisconnected(); break;
        case 4: _t->onTimeout(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QXmppIncomingClient::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QXmppIncomingClient::staticMetaObject = {
    { &QXmppStream::staticMetaObject, qt_meta_stringdata_QXmppIncomingClient,
      qt_meta_data_QXmppIncomingClient, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QXmppIncomingClient::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QXmppIncomingClient::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QXmppIncomingClient::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QXmppIncomingClient))
        return static_cast<void*>(const_cast< QXmppIncomingClient*>(this));
    return QXmppStream::qt_metacast(_clname);
}

int QXmppIncomingClient::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QXmppStream::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void QXmppIncomingClient::elementReceived(const QDomElement & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
