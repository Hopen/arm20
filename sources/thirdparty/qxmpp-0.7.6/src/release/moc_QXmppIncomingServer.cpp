/****************************************************************************
** Meta object code from reading C++ file 'QXmppIncomingServer.h'
**
** Created: Wed 5. Mar 14:05:35 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../server/QXmppIncomingServer.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QXmppIncomingServer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QXmppIncomingServer[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      28,   21,   20,   20, 0x05,
      75,   67,   20,   20, 0x05,

 // slots: signature, parameters, type, tag, flags
     113,  104,   20,   20, 0x08,
     157,   20,   20,   20, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_QXmppIncomingServer[] = {
    "QXmppIncomingServer\0\0result\0"
    "dialbackRequestReceived(QXmppDialback)\0"
    "element\0elementReceived(QDomElement)\0"
    "dialback\0slotDialbackResponseReceived(QXmppDialback)\0"
    "slotSocketDisconnected()\0"
};

void QXmppIncomingServer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QXmppIncomingServer *_t = static_cast<QXmppIncomingServer *>(_o);
        switch (_id) {
        case 0: _t->dialbackRequestReceived((*reinterpret_cast< const QXmppDialback(*)>(_a[1]))); break;
        case 1: _t->elementReceived((*reinterpret_cast< const QDomElement(*)>(_a[1]))); break;
        case 2: _t->slotDialbackResponseReceived((*reinterpret_cast< const QXmppDialback(*)>(_a[1]))); break;
        case 3: _t->slotSocketDisconnected(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QXmppIncomingServer::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QXmppIncomingServer::staticMetaObject = {
    { &QXmppStream::staticMetaObject, qt_meta_stringdata_QXmppIncomingServer,
      qt_meta_data_QXmppIncomingServer, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QXmppIncomingServer::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QXmppIncomingServer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QXmppIncomingServer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QXmppIncomingServer))
        return static_cast<void*>(const_cast< QXmppIncomingServer*>(this));
    return QXmppStream::qt_metacast(_clname);
}

int QXmppIncomingServer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QXmppStream::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void QXmppIncomingServer::dialbackRequestReceived(const QXmppDialback & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QXmppIncomingServer::elementReceived(const QDomElement & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
