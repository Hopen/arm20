/****************************************************************************
** Meta object code from reading C++ file 'QXmppServer.h'
**
** Created: Wed 5. Mar 14:05:40 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../server/QXmppServer.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QXmppServer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QXmppServer[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       1,   69, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      17,   13,   12,   12, 0x05,
      42,   13,   12,   12, 0x05,
      77,   70,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
     113,  105,   12,   12, 0x0a,
     147,  140,   12,   12, 0x08,
     180,   12,   12,   12, 0x08,
     201,   12,   12,   12, 0x08,
     234,  225,   12,   12, 0x08,
     276,   12,   12,   12, 0x08,
     308,  140,   12,   12, 0x08,
     341,   12,   12,   12, 0x08,

 // properties: name, type, flags
      70,  365, 0x0049510b,

 // properties: notify_signal_id
       2,

       0        // eod
};

static const char qt_meta_stringdata_QXmppServer[] = {
    "QXmppServer\0\0jid\0clientConnected(QString)\0"
    "clientDisconnected(QString)\0logger\0"
    "loggerChanged(QXmppLogger*)\0element\0"
    "handleElement(QDomElement)\0socket\0"
    "_q_clientConnection(QSslSocket*)\0"
    "_q_clientConnected()\0_q_clientDisconnected()\0"
    "dialback\0_q_dialbackRequestReceived(QXmppDialback)\0"
    "_q_outgoingServerDisconnected()\0"
    "_q_serverConnection(QSslSocket*)\0"
    "_q_serverDisconnected()\0QXmppLogger*\0"
};

void QXmppServer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QXmppServer *_t = static_cast<QXmppServer *>(_o);
        switch (_id) {
        case 0: _t->clientConnected((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->clientDisconnected((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->loggerChanged((*reinterpret_cast< QXmppLogger*(*)>(_a[1]))); break;
        case 3: _t->handleElement((*reinterpret_cast< const QDomElement(*)>(_a[1]))); break;
        case 4: _t->_q_clientConnection((*reinterpret_cast< QSslSocket*(*)>(_a[1]))); break;
        case 5: _t->_q_clientConnected(); break;
        case 6: _t->_q_clientDisconnected(); break;
        case 7: _t->_q_dialbackRequestReceived((*reinterpret_cast< const QXmppDialback(*)>(_a[1]))); break;
        case 8: _t->_q_outgoingServerDisconnected(); break;
        case 9: _t->_q_serverConnection((*reinterpret_cast< QSslSocket*(*)>(_a[1]))); break;
        case 10: _t->_q_serverDisconnected(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QXmppServer::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QXmppServer::staticMetaObject = {
    { &QXmppLoggable::staticMetaObject, qt_meta_stringdata_QXmppServer,
      qt_meta_data_QXmppServer, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QXmppServer::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QXmppServer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QXmppServer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QXmppServer))
        return static_cast<void*>(const_cast< QXmppServer*>(this));
    return QXmppLoggable::qt_metacast(_clname);
}

int QXmppServer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QXmppLoggable::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QXmppLogger**>(_v) = logger(); break;
        }
        _id -= 1;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setLogger(*reinterpret_cast< QXmppLogger**>(_v)); break;
        }
        _id -= 1;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QXmppServer::clientConnected(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QXmppServer::clientDisconnected(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QXmppServer::loggerChanged(QXmppLogger * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
static const uint qt_meta_data_QXmppSslServer[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      23,   16,   15,   15, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QXmppSslServer[] = {
    "QXmppSslServer\0\0socket\0"
    "newConnection(QSslSocket*)\0"
};

void QXmppSslServer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QXmppSslServer *_t = static_cast<QXmppSslServer *>(_o);
        switch (_id) {
        case 0: _t->newConnection((*reinterpret_cast< QSslSocket*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QXmppSslServer::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QXmppSslServer::staticMetaObject = {
    { &QTcpServer::staticMetaObject, qt_meta_stringdata_QXmppSslServer,
      qt_meta_data_QXmppSslServer, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QXmppSslServer::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QXmppSslServer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QXmppSslServer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QXmppSslServer))
        return static_cast<void*>(const_cast< QXmppSslServer*>(this));
    return QTcpServer::qt_metacast(_clname);
}

int QXmppSslServer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTcpServer::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void QXmppSslServer::newConnection(QSslSocket * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
