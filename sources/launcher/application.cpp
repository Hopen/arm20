#include <QStringList>
#include <QApplication>
#include <QDebug>
#include <QUrl>
#include <QTimer>
#include <QMessageBox>
#include <QDesktopServices>
#include "application.h"

#include "appcore/serviceprovider.h"
#include "appcore/apploader.h"
#include "appcore/appsettings.h"
#include "appcore/applogger.h"
#include "appcore/sessioncontroller.h"
#include "appcore/operlistcontroller.h"
#include "appcore/callscontroller.h"
#include "appcore/shortcutmanager.h"
#include "appcore/layoutmanager.h"
#include "appcore/presetmanager.h"
#include "appcore/soundeventsmanager.h"
#include "appcore/statusnotifier.h"
#include "appcore/statusoptions.h"
#include "appcore/styleloader.h"
#include "appcore/appurlhandler.h"
#include "appcore/cryptoprovider.h"
#include "appcore/operationcontroller.h"
#include "appcore/redirectlistcontroller.h"
#include "appcore/JabberController.h"
#include "appcore/globaladdressbookcontroller.h"
//#include "QtCrypto"

#ifdef QT_NODLL
#include "callsgen/callsgen.h"
#include "dashboard/dashboard.h"
#include "ldapconnector/ldapcontactsplugin.h"
#include "OpenPhone/openphone.h"
#include "operwidgets/operwidgets.h"
#include "routerclient/routerclient.h"
#include "qca/plugins/qca-ossl/qca-ossl.h"
#endif

#ifdef Q_WS_WIN
#include <QAxFactory>
#include "workspace.h"
#include "appcore/ntlmauthentication.h"
#endif

using namespace ccappcore;
#ifdef QT_NODLL
using namespace is3client;
#endif


//static const char* APPCMD_SHOW = "show";
//static const char* APPCMD_HIDE = "show";

Application::Application(int &argc, char **argv, bool GUIenabled )
    : QtSingleApplication(argc, argv, GUIenabled),
      _process(new QProcess(this)),
      _updateStarted(false),
      m_translator(new QTranslator())
{
    qRegisterMetaType<ccappcore::Call>("ccappcore::Call");
    qRegisterMetaType<ccappcore::Contact>("ccappcore::Contact");

    ServiceProvider& sp = ServiceProvider::instance();
    sp.registerService( new SessionController(this)     );
    sp.registerService( new StatusNotifier(this)        );
    sp.registerService( new OperListController(this)    );
    sp.registerService( new CallsController(this)       );
    sp.registerService( new StatusOptions(this)         );
    sp.registerService( new LayoutManager(this)         );
    sp.registerService( new PresetManager(this)         );
    sp.registerService( new ShortcutManager(this)       );
    sp.registerService( new SoundEventsManager(this)    );
    sp.registerService( new StyleLoader(this)           );
    sp.registerService( new AppUrlHandler(this)         );
    sp.registerService( new AppSettings(this)           );
    sp.registerService( new AppLogger(this)             );
    sp.registerService( new CryptoProvider(this)        );
    //sp.registerService( new OperationController(this)   );
    sp.registerService( new RedirectListController(this));
    sp.registerService( new SpamListController(this)    );
    sp.registerService( new JabberController(this)    );
    sp.registerService( new GlobalAddressBookController(this)    );

    m_translator->load( "qt_en.qm");

#ifdef Q_WS_WIN
    sp.registerService( new NTLMAuthentication() );
#endif

    connect(this, SIGNAL(messageReceived(QString)),
            this, SLOT(processMessage(QString)), Qt::QueuedConnection);

    connect(this, SIGNAL(readyToUpdate()),
            this, SLOT(onReadyToUpdate()));

    connect(_process.data(),
            SIGNAL(error(QProcess::ProcessError)),
            this,
            SLOT(onUpdateError(QProcess::ProcessError)));
    connect(_process.data(),
            SIGNAL(finished(int, QProcess::ExitStatus)),
            this,
            SLOT(onUpdateFinished(int, QProcess::ExitStatus)));

}

bool Application::activateStartedInstance()
{
    QStringList argsList = arguments();
    argsList.removeFirst(); //remove program name
    if(isRunning() && argsList.count()>0)
    {
        QString message = argsList.join(" ");
        return sendMessage(message);
    }
    return false;
}

void Application::initLogging()
{
    LateBoundObject<AppSettings> appSettings;
    appSettings->loadAppSettings();
    LateBoundObject<AppLogger>()->applySettings(*appSettings);
}

bool Application::loadPlugins()
{
    LateBoundObject<AppSettings> settings;
    settings->loadSettings();
    settings->applySettings();
    settings->flushSettings();

    connect(settings.instance(),SIGNAL(switchSessionLanguage(QString)), this, SLOT(switchLanguage(QString)));


    AppLoader loader;
    QString pluginsFolder = applicationDirPath();
    if(!loader.load(pluginsFolder))
    {
        qCritical()<<"AppLoader is unable to load the application.";
        QMessageBox::critical(NULL,
                              QObject::tr("Ошибка загрузки приложения"),
                              QObject::tr("Не удалось загрузить плагины. \n%1").arg(pluginsFolder));
        return false;
    }

    if (loader.getPluginsList().count() == 0)
    {
        qDebug() << "No plugins loaded";
        QMessageBox::critical(NULL, "Loading Error", "No plugins loaded");
        return false;
    }

    QStringList servicesList;
    foreach (QObject* service, ServiceProvider::instance().getServices())
        servicesList.push_back(service->metaObject()->className());

    QStringList pluginsList;
    foreach (QObject* plugin, loader.getPluginsList())
    {
        plugin->setParent(this); //to delete the object when exiting the application
        pluginsList.push_back(plugin->metaObject()->className());
    }

    qDebug()<<"Loaded plugins:"<<pluginsList;
    qDebug()<<"Registered services:"<<servicesList;

    LateBoundObject<SessionController>()->initialize();
    //LateBoundObject<OperationController>()->initialize();
    //LateBoundObject<OperSettings>()->re();
    return true;
}

//Q_IMPORT_PLUGIN(ccplugin_operwidgets)

void Application::initLibs()
{
    LateBoundObject<AppSettings> settings;
    settings->loadSettings();
    settings->applySettings();
    settings->flushSettings();

#ifdef QT_NODLL
//    static CallsGen           callsgen           ;
//    static Dashboard          dashboard          ;
    static LdapContactsPlugin ldapContactsPlugin ;
    static OpenPhone          openPhone          ;
    static OperWidgets        operWidgets        ;
    static RouterClient       routerClient       ;
    static opensslPlugin      opensslPlg         ;

//    callsgen           .initialize();
//    dashboard          .initialize();
    ldapContactsPlugin .initialize();
    openPhone          .initialize();
    operWidgets        .initialize();
    routerClient       .initialize();

    QCA::insertProvider (opensslPlg.createProvider());
//    LateBoundObject<CallsGen>()->initialize();
//    LateBoundObject<Dashboard>()->initialize();
//    LateBoundObject<LdapContactsPlugin>()->initialize();
//    LateBoundObject<OpenPhone>()->initialize();
//    LateBoundObject<OperWidgets>()->initialize();
//    LateBoundObject<RouterClient>()->initialize();

//    QCA::insertProvider (LateBoundObject<opensslPlugin>()->createProvider());

#endif

    settings->applySettings();
    settings->flushSettings();

    LateBoundObject<SessionController>()->initialize();
}

int Application::update(bool justcheck)
{
    LateBoundObject<AppSettings> settings;
    bool autoUpdate = settings->getOption("update/auto", false).toBool();
    if (!autoUpdate)
        return 0;

    QString file = settings->getOption("update/fileName", "").toString();

    if (file.isEmpty())
        return 0;

    if (justcheck)
        file+=tr(" /justcheck /quickcheck");
    else
        file+=tr(" /skipinfo");

    if (justcheck)
    {
        _process->start(file);
        _process->waitForFinished();
    }
    else
    {
        _process->startDetached(file);
    }

    return 1;
}

void Application::onReadyToUpdate()
{
    _updateStarted = true;
    update(false);
}

void Application::onUpdateError(QProcess::ProcessError error)
{
    qDebug() << tr("Error updating:\"%1\"").arg(error);
}

void Application::onUpdateFinished(int exitCode, QProcess::ExitStatus)
{
    if (exitCode == 2)
        emit readyToUpdate();
}

//#include "QList"
//typedef QList <int> IntContainer;



int Application::exec()
{

//    IntContainer mas1, mas2;

//    mas1.push_back(1);
//    mas1.push_back(2);
//    mas1.push_back(3);
//    mas1.push_back(4);

//    mas2.reserve(mas1.size());

//    qCopy(mas1.begin(),mas1.end(),mas2.begin());


#ifdef Q_WS_WIN
 //   QAxFactory::startServer();
   // QAxFactory::registerActiveObject(new Workspace());
#endif

    LateBoundObject<SessionController> sessionController;
    connect(sessionController.instance(),SIGNAL(sessionStarted()), this, SLOT(processArguments()));

    int retVal = QtSingleApplication::exec();

    LateBoundObject<AppSettings> appSettings;
    appSettings->flushSettings();
    appSettings->saveSettings();
    return retVal;
}

void Application::processMessage(const QString& message)
{
    qDebug()<<"Application::handleMessage("<< message <<")";
    QUrl url(message);
    if(url.isValid() && !url.scheme().isEmpty())
    {
        QDesktopServices::openUrl(url);
        return;
    }

    qDebug()<<"Application::handleMessage("<< message <<") - invalid URL";
}

void Application::processArguments()
{
    QStringList argsList = arguments();
    argsList.removeFirst();
    QString message = argsList.join(" ");
    processMessage(message);
    LateBoundObject<SessionController> sessionController;
    sessionController->disconnect(this);
}


void Application::switchLanguage(QString id)
{
    if (id == tr("RU"))
    {
        this->removeTranslator(m_translator.data());
    }
    else
    {
        this->installTranslator( m_translator.data() );
    }


}


