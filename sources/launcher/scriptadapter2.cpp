//#include "scriptadapter2.h"
//#include "appcore/lateboundobject.h"
//#include "appcore/callscontroller.h"

//using namespace ccappcore;

//ScriptAdapter2::ScriptAdapter2(QObject *application)
//    : QDBusAbstractAdaptor(application)
//{
//    _engine = QSharedPointer<QScriptEngine>(new QScriptEngine());
//    LateBoundObject<CallsController>    _callsController;
//    QScriptValue objectWnd = _engine->newQObject(_callsController.instance());
//    _engine->globalObject().setProperty("CallsController", objectWnd);
//}

//QString ScriptAdapter2::exec(const QString& scriptName)
//{
//    qDebug() << tr("Run script: \"%1\"").arg(scriptName);
//    QFile scriptFile(scriptName);
//    QString script;
//    if (!scriptFile.exists())
//    {
//        QString error (tr("Run script error: file \"%1\" does not exist").arg(scriptName));
//        qDebug() << error;
//        //return SE_FILE_NOT_EXIST;
//        return error;
//    }

//    scriptFile.open(QIODevice::ReadOnly);
//    script.append(scriptFile.readAll());
//    scriptFile.close();

//    QScriptValue result = _engine->evaluate(script);
//    if (result.isError())
//    {
//        QString error(tr("Run script error: %1").arg(result.toString()));
//        qDebug() << error;
//        //return SE_SCRIPT_EVALUATE;
//        return error;
//    }

//    return QString(tr("Script \"%1\" has successfully completed").arg(scriptName));
//}
