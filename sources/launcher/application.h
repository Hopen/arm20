#ifndef APPLICATION_H
#define APPLICATION_H

#include <QtCore/QObject>
#include <QtCore/QPointer>
#include "qtsingleapplication/qtsingleapplication.h"
#include <QtCore/QProcess>
#include <QSharedPointer>

#include <QtGui/QApplication>

enum LANGUAGE_ID
{
    LID_RUS = 0,
    LID_ENG
};

class Application: public QtSingleApplication
{
Q_OBJECT

public:
    Application(int &argc, char **argv, bool GUIenabled = true);

    void initLogging();
    bool loadPlugins();
    void initLibs();
    int  update(bool check = true);
    int  exec();
    bool isUpdateStarted(){return _updateStarted;}
    QTranslator* GetTraslator()const {return m_translator.data();}

signals:
    void readyToUpdate();

public slots:
    bool activateStartedInstance();
    void processMessage(const QString& url);
    void processArguments();
    void onUpdateError(QProcess::ProcessError);
    void onUpdateFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void onReadyToUpdate();

    void switchLanguage(QString );

private:
    QPointer <QProcess> _process;
    bool _updateStarted;

    QSharedPointer <QTranslator> m_translator;

};

#endif // APPLICATION_H
