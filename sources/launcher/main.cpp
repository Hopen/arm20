#include <QtCore/QtCore>
#include <QApplication>
#include <QMessageBox>
#include <QtCore/QDebug>
#include <QSslSocket>

#include <QPlastiqueStyle>
//#include <QMotifStyle>

#include "application.h"
//#include "scriptadapter2.h"
#include "appcore/JabberController.h"
#include <QtCore/QThread>
//#include "QtNetwork/private/qsslsocket_openssl_p.h"

void printHelp()
{
    QTextStream cout(stdout);
    cout<<"Syntax: calloper <url>|<command>"<<endl;
    cout<<"<command> := show | hide";
}

int main(int argc, char *argv[])
{
    int retVal = 0;
    {
        Application a( argc, argv);

        QTextCodec* textCodec = QTextCodec::codecForName("Windows-1251");
        QTextCodec::setCodecForCStrings(textCodec);
        QTextCodec::setCodecForLocale(textCodec);
        QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));

//        //QString translatorFileName = QLatin1String("qt_");
//        //translatorFileName += QLocale::system().name();
//        QString translatorFileName = "qt_en.qm";

//        qDebug()<<translatorFileName;
//        qDebug()<<QLibraryInfo::location(QLibraryInfo::TranslationsPath);

//        QTranslator *translator = new QTranslator(&a);
//        if (translator->load(translatorFileName/*, QLibraryInfo::location(QLibraryInfo::TranslationsPath)*/))
//            a.installTranslator(translator);

        //a.installTranslator(a.GetTraslator());

        if(a.activateStartedInstance())
            return 0;

        a.initLogging();
#ifndef Q_OS_UNIX
        a.update();
#endif

        if (a.isUpdateStarted())
            return 0;

        qDebug()<<"==============Application started==============";
#ifndef QT_NODLL
        if(!a.loadPlugins())
            return -2;
#else
        a.initLibs();
#endif

        QApplication::setStyle(new QPlastiqueStyle());

        //QFont font = QApplication::font();
        //font.setFamily("Arial");
        QFont fontArial("Arial",9);
        QApplication::setFont(fontArial);

//        if (!QDBusConnection::sessionBus().isConnected()) {
//            qDebug() << "Cannot connect to the D-Bus session bus.\n"
//                        << "To start it, run:\n"
//                           << "\teval `dbus-launch --auto-syntax`\n";
//            return -3;
//        }
//        if (!QDBusConnection::sessionBus().registerService("DBus.ARM.QScript")) {
//            qDebug() << QDBusConnection::sessionBus().lastError().message();
//            return -3;
//        }

//        ScriptAdapter2 *adapter = new ScriptAdapter2(&a);

//        if (!QDBusConnection::sessionBus().registerObject("/", adapter, QDBusConnection::ExportAllSlots))
//        {
//            qDebug() << QDBusConnection::sessionBus().lastError().message();
//            return -3;
//        }

//        QThread* mainThread = a.thread();
//        QTcpSocket *tcpSocket = new QTcpSocket(&a);

//          QSslSocket *sslSocket = new QSslSocket();
//          QThread* thread1 = translator->thread();

//          QThread* currentThread = QThread::currentThread();

//          QThread* thread3 = tcpSocket->thread();
//          QThread* thread4 = sslSocket->thread();
//          //QThreadData *threadData1 = translator->d_func()->threadData;

//        QPointer<QAbstractSocket> socket (new QSslSocket());
//        //QHostAddress host(QObject::tr("jabber.jivosite.com"));
//        //QString shost = host.toString();
//        socket->connectToHost(QObject::tr("jabber.jivosite.com"),5222);

//          QXmppClient client;
//          client.configuration().setHost(QObject::tr("jabber.jivosite.com"));
//          client.configuration().setPort(5222);
//          client.logger()->setLoggingType(QXmppLogger::StdoutLogging);
//          client.connectToServer(client.configuration());
//          //client.connectToServer("gubarev-forte-it-ru@jivosite.com", "YtjDby22");


          retVal = a.exec();
    }
    qDebug()<<"==============Application terminated==============";
    return retVal;
}
