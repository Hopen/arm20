#ifndef WORKSPACE_H
#define WORKSPACE_H

#include <QObject>

class Workspace: public QObject
{
    Q_OBJECT
    Q_CLASSINFO("ClassID", "{A4A7C271-3159-41d0-931E-ADECDFC523B7}")
    Q_CLASSINFO("InterfaceID", "{1F7DA644-75D5-46bc-BD3E-F2DC76BD0786}")
    Q_CLASSINFO("EventsID", "{82C77814-FB44-470a-96B2-BEFCAA1CBE1F}")
    Q_CLASSINFO("RegisterObject", "yes")
public slots:
signals:
public:
    Workspace(QObject* parent = 0);
};

#endif // WORKSPACE_H
