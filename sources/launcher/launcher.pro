# -------------------------------------------------
# Project created by QtCreator 2009-04-30T23:02:24
# -------------------------------------------------
TARGET = calloper
#QT += network #script  dbus
QT += dbus script declarative #network#webkit
TEMPLATE = app
DESTDIR = $$OUT_PWD/../bin
#QT -= core

win32 {
   # CONFIG += qaxserver
    RC_FILE = calloper.rc
}


contains( CONFIG, static) {
  CONFIG  += staticlib
  DEFINES += QT_NODLL
  DEFINES += QCA_STATIC
  DEFINES += KDCHART_STATICLIB
  DEFINES += QXMPP_STATIC
}

DEFINES -= QABSTRACTSOCKET_DEBUG


XMPP_PATH = ../../sources/thirdparty/qxmpp-0.7.6 \
            ../../sources/thirdparty/qxmpp-0.7.6/src \
            ../../sources/thirdparty/qxmpp-0.7.6/src/base \
            ../../sources/thirdparty/qxmpp-0.7.6/src/client

XMPP_PATH_LIB = ../../sources/thirdparty/qxmpp-0.7.6/src

#XMPP_PATH = D:/Projects/new/trunk/sources/thirdparty/qxmpp-0.7.6 \
#            D:/Projects/new/trunk/sources/thirdparty/qxmpp-0.7.6/src \
#            D:/Projects/new/trunk/sources/thirdparty/qxmpp-0.7.6/src/base \
#            D:/Projects/new/trunk/sources/thirdparty/qxmpp-0.7.6/src/client


INCLUDEPATH += -L$$XMPP_PATH

INCLUDEPATH += ../thirdparty/openssl/include

INCLUDEPATH += ../
#LIBS = -L$$DESTDIR -lappcore -lsecur32 -lthirdparty
LIBS = -L$$DESTDIR -lappcore -lthirdparty

windows {
  LIBS += -lsecur32
}

contains( CONFIG, static) {

LIBS += \
#        -lccplugin_callsgen \
#        -lccplugin_dashboard \
        -lccplugin_ldap \
        -lccplugin_openphone \
        -lccplugin_qca-ossl \
        -lccplugin_routerclient


KD_CHART_PATH=../../sources/thirdparty/kd_chart
#KD_CHART_PATH=D:/Projects/new/trunk/sources/thirdparty/kd_chart


windows {
CONFIG(debug,debug|release): LIBS+=-lkdchartd -L$$KD_CHART_PATH/lib
CONFIG(release,debug|release): LIBS+=-lkdchart -L$$KD_CHART_PATH/lib
}

LDAP_PATH = ../../sources/thirdparty/openldap-2.4.23
windows {
    LIBS += $$LDAP_PATH/libraries/libldap_r/libldap_r.dll.a \
            $$LDAP_PATH/libraries/liblber/liblber.dll.a
}
else {
    LIBS = -lldap -llber
}

LIBS += -lccplugin_operwidgets #-lQtWebKitd

windows {
#CONFIG(debug,debug|release): LIBS+=-lQtWebKitd
CONFIG(release,debug|release): LIBS+=-lQtWebKit
}


windows {
#CONFIG(debug,debug|release): LIBS+=-lwebcore -Ld:/Qt_source_4.8.3/src/3rdparty/webkit/Source/WebCore/debug
CONFIG(release,debug|release): LIBS+=-lwebcore -Ld:/Qt_source_4.8.3/src/3rdparty/webkit/Source/WebCore/release
}

windows {
#CONFIG(debug,debug|release): LIBS+=-ljscore -Ld:/Qt_source_4.8.3/src/3rdparty/webkit/Source/JavaScriptCore/debug
CONFIG(release,debug|release): LIBS+=-ljscore -Ld:/Qt_source_4.8.3/src/3rdparty/webkit/Source/JavaScriptCore/release
}

#second definition
windows {
#CONFIG(debug,debug|release): LIBS+=-lQtWebKitd
CONFIG(release,debug|release): LIBS+=-lQtWebKit
}

windows {
#CONFIG(debug,debug|release): LIBS+=-lwebcore -Ld:/Qt_source_4.8.3/src/3rdparty/webkit/Source/WebCore/debug
CONFIG(release,debug|release): LIBS+=-lwebcore -Ld:/Qt_source_4.8.3/src/3rdparty/webkit/Source/WebCore/release
}


#LIBS += -lQtWebKitd

#windows {
#CONFIG(debug,debug|release): LIBS+=-lwebcore -Ld:/Qt_source_4.8.3/src/3rdparty/webkit/Source/WebCore/debug
#CONFIG(release,debug|release): LIBS+=-lwebcore -Ld:/Qt_source_4.8.3/src/3rdparty/webkit/Source/WebCore/release
#}


LIBS += -lappcore -lthirdparty

INCLUDEPATH+=../thirdparty/qca/include/QtCrypto
windows {

    DEFINES += OSSL_097
    LIBS += -llibeay32 -lssleay32
    LIBS += -lgdi32 -lwsock32

    CONFIG(debug,debug|release): LIBS+=-lqcad
    CONFIG(release,debug|release): LIBS+=-lqca

    LIBS += -lcrypt32
}
else {
    LIBS+=-L$$DESTDIR -lqca
}

RESOURCES += ../operwidgets/resources.qrc
RESOURCES += ../dashboard/dashboard.qrc

windows {
CONFIG(debug,debug|release): LIBS+=-lqxmpp_d -L$$XMPP_PATH_LIB
CONFIG(release,debug|release): LIBS+=-lqxmpp -L$$XMPP_PATH_LIB

LIBS += -ldnsapi
}


} #if config STATIC

#unix {
#CONFIG(debug,debug|release): LIBS+=-lqxmpp_d -L$$XMPP_PATH_LIB
#CONFIG(release,debug|release): LIBS+=-lqxmpp -L$$XMPP_PATH_LIB
#}

#LIBS+=../../sources/thirdparty/qxmpp-0.7.6/src/libqxmpp_d.so

unix {
  CONFIG(debug,debug|release): LIBS+=../../sources/thirdparty/qxmpp-0.7.6/src/libqxmpp_d.so
  CONFIG(release,debug|release): LIBS+=../../sources/thirdparty/qxmpp-0.7.6/src/libqxmpp.so

  LIBS+=-lqca -L$$DESTDIR

}



SOURCES += main.cpp \
    application.cpp
HEADERS +=\
            application.h


#TRANSLATIONS += ../operwidgets/qt_ru.ts


#win32 {
##SOURCES += workspace.cpp
##HEADERS += workspace.h
#}
#else {
#LIBS+=-lqca
#}



INCLUDEPATH += ../thirdparty
include("../thirdparty/qtsingleapplication/qtsingleapplication.pri")



windows {
CONFIG(debug,debug|release): LIBS+=-lQtNetworkd -Ld:/Qt_source_4.8.3/lib
CONFIG(release,debug|release): LIBS+=-lQtNetwork -Ld:/Qt_source_4.8.3/lib
}


#INCLUDEPATH+= d:/Qt_source_4.8.3/lib
#windows {
#CONFIG(debug,debug|release): LIBS+=-lQtDBusd -Ld:/Qt_source_4.8.3/lib
#CONFIG(release,debug|release): LIBS+=-lQtDBus -Ld:/Qt_source_4.8.3/lib
#}

#windows {
#CONFIG(debug,debug|release): LIBS+=-lQtScript -Ld:/Qt_source_4.8.3/lib
#CONFIG(release,debug|release): LIBS+=-lQtScriptd -Ld:/Qt_source_4.8.3/lib
#}

#windows {
#CONFIG(debug,debug|release): LIBS+=-lQtXml -Ld:/Qt_source_4.8.3/lib
#CONFIG(release,debug|release): LIBS+=-lQtXmld -Ld:/Qt_source_4.8.3/lib
#}


#windows {
#CONFIG(debug,debug|release): LIBS+=-lQtCored -Ld:/Qt_source_4.8.3/lib
#CONFIG(release,debug|release): LIBS+=-lQtCore -Ld:/Qt_source_4.8.3/lib
#}








