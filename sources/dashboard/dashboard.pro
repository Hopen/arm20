TARGET = ccplugin_dashboard
QT += declarative
QT += xml network
TEMPLATE = lib

contains( CONFIG, static) {
  CONFIG  += staticlib
  DEFINES += QT_NODLL
  DEFINES += QCA_STATIC
  DEFINES += KDCHART_STATICLIB
  DEFINES += QXMPP_STATIC
} else {
  CONFIG += plugin lib_bundle
}

DESTDIR = $$OUT_PWD/../bin
LIBS += -L$$DESTDIR -lappcore -lccplugin_operwidgets

KD_CHART_PATH=../thirdparty/kd_chart

LIBS += -L$$KD_CHART_PATH/lib

windows {
    CONFIG(debug,debug|release) {
        #LIBS += -lkdchartd2
        #kd_chart_libs.files = $$KD_CHART_PATH/lib/kdchartd2.dll
         LIBS += -lkdchartd
         !contains( CONFIG, static) {
           kd_chart_libs.files = $$KD_CHART_PATH/lib/kdchartd.dll
         }
    }
    CONFIG(release,debug|release) {
        #LIBS += -lkdchart2
        #kd_chart_libs.files = $$KD_CHART_PATH/lib/kdchart2.dll
        LIBS += -lkdchart
        !contains( CONFIG, static) {
          kd_chart_libs.files = $$KD_CHART_PATH/lib/kdchart.dll
        }
    }
}
unix {
    LIBS += -lkdchart
    kd_chart_libs.files = \
        $$KD_CHART_PATH/lib/libkdchart.so \
        $$KD_CHART_PATH/lib/libkdchart.so.2 \
        $$KD_CHART_PATH/lib/libkdchart.so.2.3.0
}

kd_chart_libs.path=$$DESTDIR
INSTALLS+=kd_chart_libs

!contains( CONFIG, static) {
  PRE_TARGETDEPS += install_kd_chart_libs
}


INCLUDEPATH += $$PWD ../ $$KD_CHART_PATH/include \
    ../thirdparty/qxmpp-0.7.6/src/base \
    ../thirdparty/qxmpp-0.7.6/src/client


DEFINES += DASHBOARD_LIBRARY
PRECOMPILED_HEADER = dashboard_global.h
HEADERS += dashboard_global.h \
    dashboard.h \
    groupindicators.h \
    groupindicatormanager.h \
    groupindicatorsdialog.h \
    groupindicatortableview.h \
    groupindicatorgaugeview.h \
    wgstatuswidget.h
SOURCES += dashboard.cpp \
    groupindicators.cpp \
    groupindicatormanager.cpp \
    groupindicatorsdialog.cpp \
    groupindicatortableview.cpp \
    groupindicatorgaugeview.cpp \
    wgstatuswidget.cpp
RESOURCES += dashboard.qrc

FORMS += \
    groupindicatorsdialog.ui
