#include "groupindicatortableview.h"
#include "appcore/indicatorlistmodel.h"
#include "appcore/useritemdatarole.h"
#include "appcore/quantativeindicator.h"
#include "dashboard.h"
#include "operwidgets/indicatorcontextmenu.h"
#include "operwidgets/indicatoritemdelegate.h"
#include "groupindicatorgaugeview.h"
#include <QDomDocument>

using namespace ccappcore;

GroupIndicatorTableView::GroupIndicatorTableView(QObject *parent)
    : ccappcore::PresetWidget(parent)
{
}

static const char TableViewElementName[] = "table";
static const char TableRowElementName[] = "row";
static const char TableColumnElementName[] = "col";
static const char GroupCodeAttributeName[] = "groupCode";

QWidget* GroupIndicatorTableView::widget()
{
    return _tableView;
}

void GroupIndicatorTableView::create(const TableDef& tableDef)
{
    _tableDef = tableDef;

    LateBoundObject<GroupIndicatorManager> manager;
    IndicatorListModel* model = new IndicatorListModel(this);
    model->setObjectName("groupindicators");
    foreach(const RowDef& rowDef, tableDef)
    {
        IndicatorList list;
        foreach(const GroupIndicatorMetadata& im, rowDef.indicators )
        {
            QuantativeIndicator* indicator= manager->createIndicator(im, rowDef.group);
            list.append(indicator);
        }

        QString categoryName = GroupIndicator::categoryName(rowDef.category);
        model->addRow(categoryName+",\n"+rowDef.group.name(),
                      QVariant::fromValue(rowDef.group),
                      list);
    }

    _tableView = new QTableView();
    _tableView->setModel(model);
    model->setParent(_tableView);
    _tableView->setProperty("tableDef",QVariant::fromValue(tableDef));

    _tableView->verticalHeader()->show();
    _tableView->horizontalHeader()->hide();
    _tableView->setItemDelegate(new IndicatorItemDelegate(_tableView));
    _tableView->setShowGrid(false);
    _tableView->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
    _tableView->verticalHeader()->setResizeMode(QHeaderView::ResizeToContents);
    IndicatorContextMenu* menu = new IndicatorContextMenu(_tableView);
    connect(menu, SIGNAL(showGauge(ccappcore::QuantativeIndicator*)), SLOT(onShowGaugeMenuItem(ccappcore::QuantativeIndicator*)));
    _tableView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(_tableView,SIGNAL(customContextMenuRequested(QPoint)),SLOT(indicatorContextMenuRequested(QPoint)));
}

GroupIndicator* GroupIndicatorTableView::indicatorByIndex(int row, int col)
{
    if(!_tableView)
        return NULL;

    IndicatorListModel* model = qobject_cast<IndicatorListModel*>(_tableView->model());
    if(!model)
        return NULL;

    QuantativeIndicator* i = model->indicatorByIndex(model->index(row, col));
    return qobject_cast<GroupIndicator*>(i);
}

bool GroupIndicatorTableView::writeElement(QDomElement& el)
{
    if(_tableDef.isEmpty())
        return false;

    for(int i =0; i < _tableDef.count(); ++i)
    {
        const RowDef& row = _tableDef.at(i);
        QString groupCode = row.group.data(GroupCode).toString();
        QString category = _indicatorManager->categoriesEnum().valueToKey(row.category);

        QDomElement rowElement = el.ownerDocument().createElement("group");
        rowElement.setAttribute("groupCode", groupCode);
        rowElement.setAttribute("category", category);
        for(int j=0; j < row.indicators.count(); ++j)
        {
            GroupIndicatorMetadata im = row.indicators[j];
            GroupIndicator* indicator = indicatorByIndex(i, j);
            if(indicator)
            {
                im.warningValue = indicator->warningValue();
                im.criticalValue = indicator->criticalValue();
                im.inverted = indicator->isInverted();
            }
            _indicatorManager->appendElement(rowElement, im);
        }
        el.appendChild(rowElement);
    }
    return true;
}

bool GroupIndicatorTableView::readElement(const QDomElement& el)
{
    _tableDef.clear();
    foreach(QDomElement row, DomElementList(el.childNodes()))
    {
        QString groupCode = row.attribute("groupCode");
        QString categoryString = row.attribute("category");

        GroupIndicator::IndicatorCategory category = (GroupIndicator::IndicatorCategory)
                                                     _indicatorManager->categoriesEnum().keyToValue(categoryString.toAscii());
        Group group = _operListController->findGroupByCode(groupCode);

        RowDef rowDef = {group, category, QList<GroupIndicatorMetadata>()};
        GroupIndicatorMetadata im;
        foreach(QDomElement indicatorElement, DomElementList(row.childNodes()))
            if(_indicatorManager->readElemend(indicatorElement, &im))
                rowDef.indicators.append(im);

        _tableDef.append(rowDef);
    }
    return true;
}

void GroupIndicatorTableView::restoreWidget()
{
    create(_tableDef);
}

void GroupIndicatorTableView::indicatorContextMenuRequested(QPoint pos)
{
    IndicatorContextMenu* menu = sender()->findChild<IndicatorContextMenu*>();
    QPointer<QuantativeIndicator> indicator = _tableView->currentIndex()
                                .data(AssociatedObjectRole).value<QPointer<QuantativeIndicator> >();

    if(indicator.isNull())
        return;

    menu->popup(indicator, _tableView->mapToGlobal(pos));
}

void GroupIndicatorTableView::onShowGaugeMenuItem(ccappcore::QuantativeIndicator* indicator)
{
    GroupIndicator* groupIndicator = qobject_cast<GroupIndicator*>(indicator);
    LateBoundObject<GroupIndicatorManager> manager;
    GroupIndicatorMetadata im = manager->find(groupIndicator->indicatorId(), groupIndicator->format());
    if(!im.isValid())
        return;
    im.warningValue = groupIndicator->warningValue();
    im.criticalValue = groupIndicator->criticalValue();
    im.inverted = groupIndicator->isInverted();

    GroupIndicatorGaugeView* view = new GroupIndicatorGaugeView(presetManager());
    view->create(groupIndicator->group(), im);
    presetManager()->addDockedWidget(view, im.name);
}






