#ifndef GAUGEWIDGETSERIALIZER_H
#define GAUGEWIDGETSERIALIZER_H

#include <QDeclarativeView>
#include "dashboard_global.h"
#include "appcore/presetwidget.h"
#include "groupindicatormanager.h"
#include "dashboard.h"

//class QDeclarativeView;
using namespace ccappcore;

class GroupIndicatorGaugeView: public ccappcore::PresetWidget
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit GroupIndicatorGaugeView(QObject*);

    void create(ccappcore::Group, const GroupIndicatorMetadata& im);

public://PresetWidget
    virtual QWidget* widget();

    virtual bool writeElement(QDomElement& el);
    virtual bool readElement(const QDomElement& el);

    virtual void restoreWidget();

protected:
    ccappcore::Group _group;
    GroupIndicatorMetadata _indicatorMetadata;
    LateBoundObject<Dashboard> _dashboard;
    LateBoundObject<GroupIndicatorManager> _indicatorManager;
    LateBoundObject<OperListController> _operListController;
    QPointer<QDeclarativeView> _gaugeView;
};

#endif // GAUGEWIDGETSERIALIZER_H
