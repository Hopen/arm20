#ifndef GROUPINDICATORMANAGER_H
#define GROUPINDICATORMANAGER_H

#include "dashboard_global.h"
#include "appcore/quantativeindicator.h"
#include "appcore/operlistcontroller.h"
#include "appcore/callscontroller.h"
#include "groupindicators.h"

class QDomElement;

using namespace ccappcore;

struct GroupIndicatorMetadata
{
    GroupIndicatorMetadata()
        : category(GroupIndicator::CategoryUndefined)
        , valueId(GroupIndicator::ID_Undefined)
        , maxId(GroupIndicator::ID_Undefined)
        , format(QuantativeIndicator::Format_Value)
        , criticalValue(0), warningValue(0), minMaxValue(0)
        , inverted(false)
    {
    }

    GroupIndicatorMetadata(const QString& name,
                GroupIndicator::IndicatorCategory category,
                GroupIndicator::IndicatorId valueId,
                GroupIndicator::IndicatorId maxId = GroupIndicator::ID_Undefined
                      )
        : name(name), category(category), valueId(valueId)
        , maxId(maxId)
        , format(QuantativeIndicator::Format_Value)
        , criticalValue(0), warningValue(0), minMaxValue(10)
        , inverted(false)
    {
    }

    bool isValid() const { return name.size()>0; }
    bool operator==(const GroupIndicatorMetadata& other)
    {
        return valueId == other.valueId && format == other.format;
    }

    QString name;
    GroupIndicator::IndicatorCategory   category;
    GroupIndicator::IndicatorId         valueId;
    GroupIndicator::IndicatorId         maxId;
    QuantativeIndicator::Format         format;

    int criticalValue;
    int warningValue;
    int minMaxValue;//минимальное значение максимума (чтобы нормально показывать шкалу в gaugeView)
    bool inverted;
};
Q_DECLARE_METATYPE(GroupIndicatorMetadata)

class GroupIndicatorManager : public QObject
{
    Q_OBJECT
public:
    explicit GroupIndicatorManager(QObject *parent = 0);

    QList<GroupIndicatorMetadata> categoryIndicators(GroupIndicator::IndicatorCategory) const;

    GroupIndicatorMetadata find(GroupIndicator::IndicatorId, QuantativeIndicator::Format);

    GroupIndicator* createIndicator(const GroupIndicatorMetadata& im,
                                         Group group,
                                         QObject* parent = 0);

    const QMetaEnum& categoriesEnum() const     { return _categoriesEnum; }
    const QMetaEnum& indicatorIdEnum() const    { return _indicatorIdEnum; }
    const QMetaEnum& formatEnum() const    { return _formatEnum; }

    bool appendElement(QDomElement& parent, const GroupIndicatorMetadata&);
    bool readElemend(const QDomElement&, GroupIndicatorMetadata*);

signals:
    void groupOperInfoChanged();
    void groupCallInfoChanged();

private slots:
    void updateCallInfoIndicators();
    void updateOpStateCount();

private:
    QMap<Group,GroupIndicator::ValuesWeakPtr> _groupCallInfoValues;
    QMap<Group,GroupIndicator::ValuesWeakPtr> _groupOperInfoValues;

    LateBoundObject<OperListController> _operListController;
    LateBoundObject<CallsController>    _callsController;

    QTimer      _callStateUpdateTimer;
    QDateTime   _callInfoUpdateTime;
    QTimer      _opStateUpdateTimer;
    QDateTime   _opStateUpdateTime;

private: //metadata
    QList<GroupIndicatorMetadata> _metadata;
    QMetaEnum _categoriesEnum;
    QMetaEnum _indicatorIdEnum;
    QMetaEnum _formatEnum;
    void initIndicatorsMetadata();
};

#endif // GROUPINDICATORMANAGER_H
