#include "groupindicatorgaugeview.h"
//#include <QDeclarativeView>
#include <QDeclarativeContext>
#include <QDomDocument>

using namespace ccappcore;

GroupIndicatorGaugeView::GroupIndicatorGaugeView(QObject* m)
    : PresetWidget(m)
{
}

QWidget* GroupIndicatorGaugeView::widget()
{
    return _gaugeView;
}

bool GroupIndicatorGaugeView::writeElement(QDomElement& el)
{
    if(!_gaugeView)
        return false;

    QObject* indicator = _gaugeView->rootContext()->contextProperty("indicator")
                                              .value<QObject*>();

    Q_ASSERT(indicator);
    if(!indicator)
        return false;

    GroupIndicator* groupIndicator = qobject_cast<GroupIndicator*>(indicator);
    Q_ASSERT(groupIndicator);
    if(!groupIndicator)
        return false;

    GroupIndicatorMetadata im = _indicatorManager->find(groupIndicator->indicatorId(),groupIndicator->format());
    if(!im.isValid())
        return false;
    im.warningValue = groupIndicator->warningValue();
    im.criticalValue = groupIndicator->criticalValue();
    im.inverted = groupIndicator->isInverted();

    QString groupCode = groupIndicator->group().data(GroupCode).toString();
    el.setAttribute("groupCode", groupCode);
    _indicatorManager->appendElement(el, im);
    return true;
}

bool GroupIndicatorGaugeView::readElement(const QDomElement& el)
{
    QString groupCode = el.attribute("groupCode");
    LateBoundObject<OperListController> operListController;
    Group group = operListController->findGroupByCode(groupCode);
    if(!group.isValid())
    {
        qWarning()<<"GroupIndicatorManager::appendElement() - invalid groupCode"
            <<" at line "<<el.lineNumber()<<", column "<<el.columnNumber();
        return false;
    }

    GroupIndicatorMetadata im;
    if(!_indicatorManager->readElemend(el.firstChild().toElement(), &im))
        return false;

    _data["group"] = QVariant::fromValue(group);
    _data["indicatorMetadata"] = QVariant::fromValue(im);
    return true;
}

void GroupIndicatorGaugeView::restoreWidget()
{
    Group group = _data["group"].value<Group>();
    GroupIndicatorMetadata im = _data["indicatorMetadata"].value<GroupIndicatorMetadata>();
    create(group, im);
}

void GroupIndicatorGaugeView::create(ccappcore::Group group, const GroupIndicatorMetadata& im)
{
    Q_ASSERT(!_gaugeView);
    _gaugeView = new QDeclarativeView();
    _gaugeView->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    _gaugeView->setResizeMode(QDeclarativeView::SizeRootObjectToView);

    QuantativeIndicator* indicator = _indicatorManager->createIndicator(im, group, _gaugeView);
    indicator->setName(group.name()+", "+im.name);
    _gaugeView->rootContext()->setContextProperty("indicator",indicator);
    _gaugeView->setSource(QUrl::fromLocalFile("qml/GaugeUsage.qml"));
}
