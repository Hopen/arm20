#ifndef ADDGROUPINDICATORSDIALOG_H
#define ADDGROUPINDICATORSDIALOG_H

#include "dashboard_global.h"
#include "groupindicatormanager.h"

using namespace ccappcore;

namespace Ui {
    class GroupIndicatorsDialog;
}

#include "groupindicatortableview.h"

class AddGroupIndicatorsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddGroupIndicatorsDialog(QWidget *parent = 0);
    ~AddGroupIndicatorsDialog();

    const GroupIndicatorTableView::TableDef& data() const { return _data; }

private:
    typedef GroupIndicatorTableView::RowDef RowDef;
    typedef GroupIndicatorTableView::TableDef TableDef;

    Ui::GroupIndicatorsDialog *ui;

    LateBoundObject<GroupIndicatorManager> _manager;
    LateBoundObject<OperListController>    _operListController;

    bool isIndicatorChecked(int row, const GroupIndicatorMetadata& im);
    TableDef _data;

    void commitData(QListWidgetItem*);

private slots:
    void on_buttonBox_accepted();
    void on_listIndicatorType_currentItemChanged(QListWidgetItem* current, QListWidgetItem* previous);
    void on_bnAddGroup_clicked();
};

#endif // ADDGROUPINDICATORSDIALOG_H
