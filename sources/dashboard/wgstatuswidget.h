#ifndef WGSTATUSWIDGET_H
#define WGSTATUSWIDGET_H

#include <QWidget>
#include "appcore/operlistcontroller.h"
#include "appcore/filteredmodel.h"
#include "appcore/domainobjectfilter.h"
#include "dashboard_global.h"
#include "appcore/indicatorlistmodel.h"

using namespace ccappcore;

class ActiveGroupSortPredicate;
class GroupIndicatorManager;

//class TestModel : public QAbstractListModel,
//                      public FilteredModel<ccappcore::Contact>
//{
//    Q_OBJECT

//public:

//    enum ColDef
//    {
//        GroupColumn = Contact::Name,
//        NameColumn,
//        TaskIdColumn,
//        StatusColumn
//    };


//        TestModel(QObject *parent = 0);


//        int rowCount(const QModelIndex &parent = QModelIndex()) const;
//        QVariant data(const QModelIndex &index, int role) const;
//        QVariant headerData(int section, Qt::Orientation orientation,
//                            int role = Qt::DisplayRole) const;
//        int columnCount ( const QModelIndex & parent = QModelIndex() ) const;

////        bool insertRows(int position, int rows, const QModelIndex &index = QModelIndex());
////        bool removeRows(int position, int rows, const QModelIndex &index = QModelIndex());

//        void sort ( int column, Qt::SortOrder order = Qt::AscendingOrder );

//        Qt::ItemFlags flags(const QModelIndex &index) const;
//        //bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
//        //void setOwnGroupsChecked();

//private:
//        QString columnNameById(const qint32& id)const;

//signals:
//public slots:
//        void reset();

//protected: //FilteredModel
//    virtual void applyFilter(const PredicateGroup<Contact>& filter);

//    private:
//        LateBoundObject < OperListController    > _controller;
//        LateBoundObject < GroupIndicatorManager > _manager;


//        typedef QList<int> GroupCollector;
//        QList<QString> _columnsList;
//        GroupCollector _groups      ; // all displayed groups
//        GroupCollector _activeGroups; // user selected groups
////        QMap<int,IndicatorList>        _indicatorList;

//        friend class ActiveGroupSortPredicate;
//};

class WGStatusModel : public QAbstractListModel,
                      public FilteredModel<Contact>
{
    Q_OBJECT

public:

    enum ColDef
    {
        GroupColumn = Contact::Name//,
//        NameColumn,
//        TaskIdColumn,
//        StatusColumn
    };


        WGStatusModel(QObject *parent = 0);


        int rowCount(const QModelIndex &parent = QModelIndex()) const;
        QVariant data(const QModelIndex &index, int role) const;
        QVariant headerData(int section, Qt::Orientation orientation,
                            int role = Qt::DisplayRole) const;
        int columnCount ( const QModelIndex & parent = QModelIndex() ) const;

//        bool insertRows(int position, int rows, const QModelIndex &index = QModelIndex());
//        bool removeRows(int position, int rows, const QModelIndex &index = QModelIndex());

        void sort ( int column, Qt::SortOrder order = Qt::AscendingOrder );

        Qt::ItemFlags flags(const QModelIndex &index) const;
        bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
        //void setOwnGroupsChecked();

private:
        QString columnNameById(const qint32& id)const;

signals:
public slots:
        void reset();
        //void sort();

protected: //FilteredModel
    virtual void applyFilter(const PredicateGroup<Contact>& filter);

    private:
        LateBoundObject < OperListController    > _controller;
        LateBoundObject < GroupIndicatorManager > _manager;


        typedef QList</*ccappcore::Group*/int> GroupCollector;
        QList<QString> _columnsList;
        GroupCollector _groups      ; // all displayed groups
        GroupCollector _activeGroups; // user selected groups
        QMap<int,IndicatorList>        _indicatorList;


        friend class ActiveGroupSortPredicate;
};

class ActiveGroupSortPredicate
{
public:
    explicit ActiveGroupSortPredicate(WGStatusModel::GroupCollector* groups)
        :_activeGroups(groups)
    {

    }

    bool operator()(const /*Contact*/int& id1,const /*Contact*/int& id2)
    {
       bool elem1Active = _activeGroups->contains(id1);
       bool elem2Active = _activeGroups->contains(id2);

//       if (elem1Active && !elem2Active)
//           return -1;
//       if (!elem1Active && elem2Active)
//           return 1;

//       return 0;
//       return elem1Active/* && elem2Active*/;

       if (( elem1Active &&  elem2Active) ||
           (!elem1Active && !elem2Active)   )
       {
           Contact c1 = _controller->findById(_controller->createGroupId(id1));
           Contact c2 = _controller->findById(_controller->createGroupId(id2));
           return c1.name() < c2.name();
       }

       return elem1Active;
    }

private:
    WGStatusModel::GroupCollector* _activeGroups;
    LateBoundObject < OperListController    > _controller;
};


//class IsGroupWithOper: public Predicate<Group>
//{
//    LateBoundObject<OperListController> _operListController;
//public:
//    IsGroupWithOper(bool privateGroupOnly)
//        : _privateGroupOnly(privateGroupOnly)
//    {}

//    bool operator()(const Group& group) const
//    {
//        return ((!_privateGroupOnly) || (group.isParentOf(_operListController->sessionOwner())));
//    }

//private:
//    bool _privateGroupOnly;
//};

//class GroupNameDelegate : public QStyledItemDelegate
//{
//        Q_OBJECT
//public:

//    void paint(QPainter *painter,
//               const QStyleOptionViewItem &option, const QModelIndex &index) const
//    {
//        painter->save();

//        QRect borderRect = option.rect.adjusted( 1, 1, -1, -1 );

//        QFont font = painter->font();
//        font.setBold(true);
//        painter->setFont( font );
//        painter->drawText( borderRect, Qt::AlignVCenter | Qt::AlignCenter, index.data().toString() );
//        //QStyledItemDelegate::paint(painter,option,index);
//        painter->restore();
//    }

//    QSize sizeHint(const QStyleOptionViewItem &option,
//                   const QModelIndex &index) const
//    {
//        return QStyledItemDelegate::sizeHint(option, index);
//    }

//};

class WGStatusWidget : public QWidget
{
    Q_OBJECT
public:
    explicit WGStatusWidget(QWidget *parent = 0);
    
signals:
    
public slots:
    void reset(bool showPrivateGroup);
private:
    QPointer< WGStatusModel > _mainModel;
    QPointer< QTreeView     > _mainView;
};

#endif // WGSTATUSWIDGET_H
