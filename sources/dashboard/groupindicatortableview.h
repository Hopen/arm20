#ifndef GROUPINDICATORTABLEVIEW_H
#define GROUPINDICATORTABLEVIEW_H

#include "dashboard_global.h"
#include <appcore/presetwidget.h>
#include "appcore/operlistcontroller.h"
#include "groupindicatormanager.h"

class QTableView;

using namespace ccappcore;

class GroupIndicatorTableView: public ccappcore::PresetWidget
{
    Q_OBJECT
public:

    struct RowDef
    {
        Group group;
        GroupIndicator::IndicatorCategory category;
        QList<GroupIndicatorMetadata> indicators;
    };
    typedef QList<RowDef> TableDef;

    Q_INVOKABLE explicit GroupIndicatorTableView(QObject *parent = 0);

    void create(const TableDef& tableDef);

public: //PresetWidgetSerializer
    virtual QWidget* widget();

    virtual bool writeElement(QDomElement& parent);
    virtual bool readElement(const QDomElement&);
    virtual void restoreWidget();

signals:

private slots:
    void indicatorContextMenuRequested(QPoint pos);
    void onShowGaugeMenuItem(ccappcore::QuantativeIndicator*);
private:
    TableDef _tableDef;
    LateBoundObject<OperListController> _operListController;
    LateBoundObject<GroupIndicatorManager> _indicatorManager;
    QPointer<QTableView> _tableView;

    GroupIndicator* indicatorByIndex(int row, int col);
};

Q_DECLARE_METATYPE(GroupIndicatorTableView::TableDef)

#endif // GROUPINDICATORTABLEVIEW_H
