#include "groupindicatorsdialog.h"
#include "ui_groupindicatorsdialog.h"

using namespace ccappcore;

enum GroupTypeItemRole
{
    IndicatorGroupRole = Qt::UserRole,
    IndicatorCategoryRole,
    IndicatorMetadataRole
};

AddGroupIndicatorsDialog::AddGroupIndicatorsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GroupIndicatorsDialog)
{
    ui->setupUi(this);

    //init groups list
    ui->comboBoxGroup->clear();
    foreach(Group g, _operListController->groupList())
        ui->comboBoxGroup->addItem(g.name(), QVariant::fromValue(g));

    ui->comboBoxIndicatorType->clear();
    for(int i=0; i < _manager->categoriesEnum().keyCount(); i++)
    {
        GroupIndicator::IndicatorCategory category =
                (GroupIndicator::IndicatorCategory) _manager->categoriesEnum().value(i);

        if(category!=GroupIndicator::CategoryUndefined)
            ui->comboBoxIndicatorType->addItem(
                GroupIndicator::categoryName( category ), category
                );
    }
}

AddGroupIndicatorsDialog::~AddGroupIndicatorsDialog()
{
    delete ui;
}

void AddGroupIndicatorsDialog::on_bnAddGroup_clicked()
{
    int groupIndex = ui->comboBoxGroup->currentIndex();
    int categoryIndex = ui->comboBoxIndicatorType->currentIndex();
    if( groupIndex < 0 || categoryIndex < 0 )
        return;

    Group group = ui->comboBoxGroup->itemData(groupIndex).value<Group>();
    if(!group.isValid())
        return;

    GroupIndicator::IndicatorCategory category = (GroupIndicator::IndicatorCategory)
             ui->comboBoxIndicatorType->itemData(categoryIndex).toInt();

    RowDef rowDef = { group, category, QList<GroupIndicatorMetadata>() };
    foreach(GroupIndicatorMetadata im, _manager->categoryIndicators(category))
        rowDef.indicators.append(im);
    _data.append(rowDef);

    QString groupCategoryText = group.name() + ", " +GroupIndicator::categoryName(category);
    QListWidgetItem* item = new QListWidgetItem(groupCategoryText,ui->listIndicatorType);
    item->setData( IndicatorCategoryRole, category );
    item->setData( IndicatorGroupRole, QVariant::fromValue(group) );
    ui->listIndicatorType->setCurrentItem(item);
}


void AddGroupIndicatorsDialog::on_listIndicatorType_currentItemChanged(
        QListWidgetItem* current,
        QListWidgetItem* previous)
{
    commitData(previous);

    int currRow = ui->listIndicatorType->row(current);
    ui->listIndicators->clear();

    //load items for new group and category
    GroupIndicator::IndicatorCategory category = (GroupIndicator::IndicatorCategory)
                                                 current->data(IndicatorCategoryRole).toInt();
    QList<GroupIndicatorMetadata> indicators = _manager->categoryIndicators(category);
    foreach(const GroupIndicatorMetadata& im, indicators)
    {
        QListWidgetItem* item = new QListWidgetItem(ui->listIndicators);
        item->setText(im.name);
        item->setData(IndicatorMetadataRole, QVariant::fromValue(im));
        item->setData(IndicatorCategoryRole, im.category);
        item->setData(Qt::CheckStateRole, isIndicatorChecked(currRow, im) ? Qt::Checked : Qt::Unchecked );
        item->setFlags(item->flags() | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
    }
}

void AddGroupIndicatorsDialog::commitData(QListWidgetItem* groupCategoryItem)
{
    int row = ui->listIndicatorType->row(groupCategoryItem);
    if(row<0)
        return;

    _data[row].indicators.clear();
    for( int i=0; i < ui->listIndicators->count(); ++i)
    {
        if(ui->listIndicators->item(i)->checkState() == Qt::Checked)
        {
            QVariant varMetadata = ui->listIndicators->item(i)->data(IndicatorMetadataRole);
            _data[row].indicators.append(varMetadata.value<GroupIndicatorMetadata>());
        }
    }
}


bool AddGroupIndicatorsDialog::isIndicatorChecked(int row, const GroupIndicatorMetadata& im)
{
    return row >= 0 && row < _data.size() && _data[row].indicators.contains(im);
}

void AddGroupIndicatorsDialog::on_buttonBox_accepted()
{
    commitData(ui->listIndicatorType->currentItem());
}


