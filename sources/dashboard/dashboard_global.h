#ifndef DASHBOARD_GLOBAL_H
#define DASHBOARD_GLOBAL_H

#include <QtCore/qglobal.h>
#include <QtGui>

#ifndef QT_NODLL
#  if defined(DASHBOARD_LIBRARY)
#    define DASHBOARDSHARED_EXPORT Q_DECL_EXPORT
#  else
#    define DASHBOARDSHARED_EXPORT Q_DECL_IMPORT
#  endif
#else
#  define DASHBOARDSHARED_EXPORT
#endif

#endif // DASHBOARD_GLOBAL_H
