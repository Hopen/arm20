#include "dashboard.h"

#include <QApplication>

#include "appcore/qobjectutils.h"

//#include "operwidgets/appcontext.h"
//#include "operwidgets/logindialog.h"
//#include "operwidgets/trayicon.h"
//#include "operwidgets/operevents.h"
//#include "operwidgets/callcontextmenu.h"
//#include "operwidgets/indicatoritemdelegate.h"
//#include "operwidgets/indicatorcontextmenu.h"
//#include "operwidgets/contactswidget2.h"
#include "wgstatuswidget.h"

#include <QTableView>
#include "appcore/domainobjectfilter.h"
#include "appcore/layoutmanager.h"

#include "appcore/operlistcontroller.h"
#include "appcore/callslistmodel.h"
#include "appcore/contactstreemodel.h"
#include "appcore/operstatuslistmodel.h"
#include "appcore/queuelengthlistmodel.h"
#include "appcore/useritemdatarole.h"
#include "appcore/domainobjectfilter.h"
#include "appcore/indicatorlistmodel.h"
#include "groupindicatormanager.h"
#include "appcore/presetmanager.h"

#include <KDChartWidget>
#include <KDChartChart>
#include <KDChartHeaderFooter>
#include <KDChartDatasetProxyModel>
#include <KDChartAbstractCoordinatePlane>
#include <KDChartPieDiagram>
#include <KDChartPieAttributes>
#include <KDChartThreeDPieAttributes>
#include <KDChartBarDiagram>
#include <KDChartThreeDBarAttributes>
#include <KDChartTextAttributes>
#include <KDChartDataValueAttributes>
#include <KDChartLegend>
#include <KDChartBackgroundAttributes>
#include <KDChartFrameAttributes>
#include <KDChartGridAttributes>
#include <KDChartMarkerAttributes>

#include "groupindicatorsdialog.h"
#include "groupindicatortableview.h"
#include "groupindicatorgaugeview.h"


using namespace ccappcore;
using namespace KDChart;
using namespace ccappcore;

#ifndef QT_NODLL
Q_EXPORT_PLUGIN2(pnp_dashboard, Dashboard);
#endif

Dashboard::Dashboard()
{
}

//Dashboard::~Dashboard()
//{
//}

void Dashboard::initialize() throw()
{
    ServiceProvider::instance().registerService(this);
    ServiceProvider::instance().registerService(&_groupIndicatorsManager);

    LateBoundObject<CallsController> callsController;
    callsController->setSubscribeAllCallEvents(true);

    _presetManager->registerMetaObject<GroupIndicatorGaugeView>();
    _presetManager->registerMetaObject<GroupIndicatorTableView>();
}

QDockWidget* Dashboard::addDockWidget( const QString& objectName,
                               const QString& title,
                               QWidget* w )
{
    QDockWidget* dw = _mainWindow->createDockWidget(objectName,title,w);
    dw->layout()->setMargin(9);
    dw->setBaseSize(320,240);
    dw->hide();
    dw->setFloating(true);
    _dashboardMenu->addAction(dw->toggleViewAction());
    return dw;
    return 0;
}

void Dashboard::initializeMainWindow(QMainWindow *parent)
{
    _mainWindow = qobject_cast<MainWindow*>(parent);
    if(!_mainWindow)
        return;

    QMenuBar* menu = _mainWindow->menuBar();
    Q_ASSERT(menu);
    QList<QAction*> actions = menu->actions();
    QAction* actionView = qActionFind(actions, "menuView");
    Q_ASSERT(actionView && actionView->menu());
    QMenu* menuView = actionView->menu();
    QAction* dashboardAction = menuView->addAction(
            QIcon(":/dashboard/icons/pie_chart_16x16.png"),
            tr("Панель управления")
            );

    _dashboardMenu = new QMenu(_mainWindow);
    dashboardAction->setMenu(_dashboardMenu);


    _dashboardMenu->addAction(tr("Добавить табло основных показателей"),
                           this, SLOT(addGroupIndicatorsTable()));

    addDockWidget(
            "callslist",
            tr("Список звонков"),
            createCallsList()
            );

    addDockWidget(
            "operstatuspiechart",
            tr("Состояние операторов"),
            createOperStatusPieChart(tr("Состояние операторов"), NULL)
            );


//    foreach(Group g, _operListController->groupList() )
//    {
//        QString objectName = "operstatuspiechart"+g.data(GroupCode).toString();
//        QString displayName = tr("График состояния операторов: ") + g.name();

//        addDockWidget(
//                objectName,
//                displayName,
//                createOperStatusPieChart(g.name(), new IsOperInGroup(g) )
//                );
//    }


//    //Group status
//    QAction* actionWindow = qActionFind(actions, "menuWindow");
//    if (!(actionWindow && actionWindow->menu()))
//        return;
//    QMenu* menuWindow = actionWindow->menu();
//    if (!menuWindow)
//        return;

//    QAction* actionOperForm = qActionFind(menuWindow->actions(), "menu_operform_opt");
//    if (!(actionOperForm && actionOperForm->menu()))
//        return;
//    QMenu* menuOperForm = actionOperForm->menu();

//    QAction * action_showWGStatusTab = new QAction(QIcon(":/resources/icons/new/index.png"),tr("Состояние групп"),parent);
//    action_showWGStatusTab->setCheckable(true);
//    action_showWGStatusTab->setChecked(true);
//    menuOperForm->addAction(action_showWGStatusTab);
//    connect(action_showWGStatusTab,SIGNAL(triggered(bool)),_mainWindow.data(),SLOT(on_action_showWGStatusTab_triggered(bool)));

//    _wgstatus = new WGStatusWidget(parent);
//    //_mainWindow->addTab(MainWindow::TB_WGSTATUS,_wgstatus,QIcon(":/resources/icons/new/tab_group-stat16-3.png"),tr("Состояние групп"));

//    connect(_mainWindow.data(),SIGNAL(privateOnly(bool)),_wgstatus.data(),SLOT(reset(bool)));
}

KDChart::Chart* Dashboard::createChart()
{
    Chart* chart = new Chart();
    chart->setGlobalLeading(6,0,6,6);
    chart->setAutoFillBackground(true);
    QPalette palette = chart->palette();
    palette.setColor(QPalette::Window,QColor("white"));
    chart->setPalette(palette);
    chart->setSizePolicy(QSizePolicy::Ignored,QSizePolicy::Ignored);
    return chart;
}

Chart* Dashboard::createBarChart(QAbstractItemModel* model)
{
    Chart* chart = createChart();

    QString name = model->objectName().isEmpty()
                   ? model->metaObject()->className() : model->objectName();
    chart->setObjectName(name.toLower()+".barchart");

    BarDiagram* diagram = new BarDiagram(chart);
    chart->setSizePolicy(QSizePolicy::Ignored,QSizePolicy::Ignored);
    diagram->setSizePolicy(QSizePolicy::Ignored,QSizePolicy::Ignored);
    chart->coordinatePlane()->replaceDiagram(diagram);

    ThreeDBarAttributes attrs;
    attrs.setEnabled(true);
    attrs.setUseShadowColors(true);
    diagram->setThreeDBarAttributes(attrs);
    diagram->setPen( QPen( Qt::black, 0 ) );

    diagram->setModel(model);

    // add 1 pixel space at the left and at the top edge, because the
    // axis area would otherwise overwrite the left/top borders
    chart->setGlobalLeading(1,1,0,0);

    Legend* legend = new Legend( diagram, chart );
    chart->addLegend(legend);
    legend->setPosition(Position::East);
    legend->setOrientation(Qt::Vertical);
    legend->setAlignment( Qt::AlignCenter );
    legend->setTextAlignment( Qt::AlignLeft );
    legend->setShowLines( false );
    legend->setSpacing(2);
    legend->setLegendStyle(Legend::MarkersOnly);

    TextAttributes ltta = legend->titleTextAttributes();
    ltta.setVisible(false);
    legend->setTitleTextAttributes(ltta);

    TextAttributes lta = legend->textAttributes();
    lta.setMinimalFontSize(Measure(8, KDChartEnums::MeasureCalculationModeAbsolute));
    legend->setTextAttributes(lta);

    FrameAttributes lfa = legend->frameAttributes();
    lfa.setVisible(false);
    lfa.setPadding(0);
    legend->setFrameAttributes(lfa);

    QStringList labels;
    const int colCount = model->columnCount(QModelIndex());
    for ( int iColumn = 0; iColumn < colCount; ++iColumn )
    {
        QString label = model->headerData(iColumn,Qt::Horizontal,Qt::DisplayRole).toString();
        labels.append(label);

        DataValueAttributes dva = diagram->dataValueAttributes(iColumn);
        TextAttributes ta = dva.textAttributes();
        ta.setRotation(0);
        ta.setVisible(true);
        ta.setMinimalFontSize(Measure(8, KDChartEnums::MeasureCalculationModeAbsolute));
        dva.setTextAttributes(ta);

        RelativePosition posPos = dva.positivePosition();
        posPos.setReferencePosition( KDChart::Position::NorthWest);
        posPos.setAlignment( Qt::AlignCenter );
        posPos.setVerticalPadding(Measure(500));
        dva.setPositivePosition(posPos);

        dva.setVisible(true);
        diagram->setDataValueAttributes(iColumn, dva);
    }

    BarAttributes barAtt = diagram->barAttributes();
    barAtt.setFixedBarWidth(60);
    barAtt.setFixedValueBlockGap(0);
    barAtt.setUseFixedBarWidth(true);
    barAtt.setGroupGapFactor(1);
    barAtt.setBarGapFactor(1);
    diagram->setBarAttributes(barAtt);

    connect( model, SIGNAL(dataChanged(QModelIndex,QModelIndex)),
             diagram, SLOT(doItemsLayout()));
    connect( model, SIGNAL(layoutChanged()), diagram, SLOT(doItemsLayout()));

    return chart;
}

Chart* Dashboard::createPieChart(QAbstractItemModel* model)
{
    Chart* chart = createChart();

    QString name = model->objectName().isEmpty()
                   ? model->metaObject()->className() : model->objectName();
    chart->setObjectName(name.toLower()+".piechart");

    PieDiagram* diagram = new PieDiagram(chart);
    diagram->setSizePolicy(QSizePolicy::Ignored,QSizePolicy::Ignored);
    chart->replaceCoordinatePlane(new PolarCoordinatePlane(chart));
    chart->coordinatePlane()->addDiagram(diagram);

    Legend* legend = new Legend( diagram, chart );
    chart->addLegend(legend);
    legend->setPosition(Position::East);
    legend->setOrientation(Qt::Vertical);
    legend->setAlignment( Qt::AlignCenter );
    legend->setTextAlignment( Qt::AlignLeft );
    legend->setShowLines( false );
    legend->setSpacing(2);
    legend->setLegendStyle(Legend::MarkersOnly);

    TextAttributes ltta = legend->titleTextAttributes();
    ltta.setVisible(false);
    legend->setTitleTextAttributes(ltta);

    TextAttributes lta = legend->textAttributes();
    lta.setMinimalFontSize(Measure(8, KDChartEnums::MeasureCalculationModeAbsolute));
    legend->setTextAttributes(lta);

    FrameAttributes lfa = legend->frameAttributes();
    lfa.setVisible(false);
    lfa.setPadding(0);

    legend->setFrameAttributes(lfa);

    const int colCount = model->columnCount(QModelIndex());
    for ( int iColumn = 0; iColumn<colCount; ++iColumn )
    {
        DataValueAttributes dva( diagram->dataValueAttributes( iColumn ) );

        dva.setShowRepetitiveDataLabels(true);
        dva.setShowOverlappingDataLabels(true);
        diagram->setDataValueAttributes( iColumn, dva);

        TextAttributes ta = dva.textAttributes();
        ta.setRotation(0);
        ta.setAutoRotate(false);
        ta.setVisible(true);
        dva.setTextAttributes(ta);

        QModelIndex itemIndex = model->index(0,iColumn,QModelIndex());
        QColor textColor = model->data( itemIndex, Qt::TextColorRole).value<QColor>();
        QColor color2 = textColor.lighter(300);

        QLinearGradient gradient;
        gradient.setColorAt(0,textColor);
        gradient.setColorAt(1,color2);
        gradient.setCoordinateMode(QGradient::ObjectBoundingMode);
        gradient.setSpread(QGradient::PadSpread);
        QBrush brush(gradient);
        brush.setColor(textColor);
        diagram->setBrush(iColumn,brush);

        dva.setVisible( true );
        diagram->setDataValueAttributes(iColumn, dva);

        PieAttributes pieAttrs = diagram->pieAttributes(iColumn);
        //pieAttrs.setExplode(true);
        pieAttrs.setExplode(false);
        //pieAttrs.setExplodeFactor(0.05);
        diagram->setPieAttributes(iColumn, pieAttrs);
    }

    ThreeDPieAttributes attrs = diagram->threeDPieAttributes();
    attrs.setEnabled(true);
    attrs.setDepth(15);
    attrs.setUseShadowColors(false);
    diagram->setThreeDPieAttributes(attrs);
    //PolarCoordinatePlane* coord = (PolarCoordinatePlane*)diagram->coordinatePlane();
    //coord->setStartPosition(15);
    diagram->setModel(model);

    connect( model, SIGNAL(layoutChanged()),
             diagram, SLOT(doItemsLayout()));
    connect( model, SIGNAL(dataChanged(QModelIndex,QModelIndex)),
             diagram, SLOT(doItemsLayout()));

    return chart;
}

QWidget* Dashboard::createOperStatusPieChart(
        const QString& displayName,
        ccappcore::Predicate<ccappcore::Operator>* filter
        )
{
    OperStatusListModel* model = new OperStatusListModel(this);
    model->addFilter(filter);
    Chart* w = createPieChart(model);
    HeaderFooter* header = new HeaderFooter(w);
    header->setText(displayName);
    header->setPosition(Position::North);
    header->setType(HeaderFooter::Header);
    w->addHeaderFooter( header );
    return w;
}


QTableView* Dashboard::createGridView(QAbstractItemModel* model)
{
    QTableView* tableView = new QTableView();
    QString name = model->objectName().isEmpty()
                   ? model->metaObject()->className() : model->objectName();
    tableView->setObjectName(name.toLower()+".tableview");

    tableView->setSizePolicy(QSizePolicy::Ignored,QSizePolicy::Ignored);
    tableView->verticalHeader()->setVisible(false);
    tableView->horizontalHeader()->setVisible(true);
    tableView->setModel(model);
    return tableView;
}

QWidget* Dashboard::createCallsList()
{
    CallsListModel* model = new CallsListModel(this);
    model->setObjectName("queuegrid");
    QList<CallsListModel::ColDef> columns;
    columns
        <<CallsListModel::AbonPhoneColumn
        <<CallsListModel::CallStateColumn
        <<CallsListModel::GroupNameColumn
        <<CallsListModel::LastOperNameColumn
        <<CallsListModel::StateTimeColumn
        <<CallsListModel::CallLengthColumn
        <<CallsListModel::PriorityColumn
        <<CallsListModel::ExtraInfoColumn;

    model->setVisibleColumns(columns);

    QTableView* w = createGridView(model);
    w->setSelectionBehavior(QAbstractItemView::SelectRows);
    w->setSelectionMode(QAbstractItemView::SingleSelection);
    w->resizeColumnsToContents();

    w->verticalHeader()->setVisible(true);
    w->horizontalHeader()->setHighlightSections(false);

    connect(w, SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(queuedCallContextMenuRequested(QPoint)));

    connect(w->model(), SIGNAL(dataChanged(QModelIndex,QModelIndex)),
            w, SLOT(resizeRowsToContents()));
    connect(w->model(), SIGNAL(layoutChanged()),
            w, SLOT(resizeRowsToContents()));

    w->setContextMenuPolicy(Qt::CustomContextMenu);

    return w;
}

void Dashboard::queuedCallContextMenuRequested(QPoint)
{
    QAbstractItemView* view = qobject_cast<QAbstractItemView*>(sender());
    Call call = view->currentIndex().data(ccappcore::AssociatedObjectRole).value<Call>();
    if(!call.isValid())
        return;
    if(_queuedCallContextMenu.isNull())
        _queuedCallContextMenu = new CallContextMenu(view);

    _queuedCallContextMenu->popup(call);
}

void Dashboard::addGroupIndicatorsTable()
{
    AddGroupIndicatorsDialog dialog;
    if(QDialog::Accepted != dialog.exec())
        return;

    GroupIndicatorTableView* view = new GroupIndicatorTableView(&*_presetManager);
    view->create(dialog.data());
    _presetManager->addDockedWidget(view, tr("Панель индикаторов"));
}
