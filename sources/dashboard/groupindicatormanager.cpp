#include "groupindicatormanager.h"
#include "groupindicators.h"
#include "appcore/timeutils.h"
#include <QDomDocument>

const int DefaultUpdateTimerInterval = 10000; //msec
const int ForcedUpdateTimerInterval  = 1000; //msec

using namespace ccappcore;

GroupIndicatorManager::GroupIndicatorManager(QObject *parent)
    : QObject(parent)
{
    connect(_operListController.instance(),SIGNAL(contactStatusChanged(ccappcore::Contact)),
            SLOT(updateOpStateCount()));
    connect(_callsController.instance(),SIGNAL(callStateChanged(ccappcore::Call)),
            SLOT(updateCallInfoIndicators()));
    connect(&_callStateUpdateTimer, SIGNAL(timeout()),SLOT(updateCallInfoIndicators()));
    connect(&_opStateUpdateTimer, SIGNAL(timeout()),SLOT(updateOpStateCount()));

    _callStateUpdateTimer.setSingleShot(false);
    _callStateUpdateTimer.start(DefaultUpdateTimerInterval);

    _opStateUpdateTimer.setSingleShot(false);
    _opStateUpdateTimer.start(DefaultUpdateTimerInterval);

    _categoriesEnum = GroupIndicator::staticMetaObject.enumerator(
            GroupIndicator::staticMetaObject.indexOfEnumerator("IndicatorCategory")
            );
    Q_ASSERT(_categoriesEnum.isValid());

    _indicatorIdEnum = GroupIndicator::staticMetaObject.enumerator(
            GroupIndicator::staticMetaObject.indexOfEnumerator("IndicatorId")
            );
    Q_ASSERT(_indicatorIdEnum.isValid());

    _formatEnum = QuantativeIndicator::staticMetaObject.enumerator(
            QuantativeIndicator::staticMetaObject.indexOfEnumerator("Format")
            );
    Q_ASSERT(_formatEnum.isValid());

    initIndicatorsMetadata();
}

void GroupIndicatorManager::initIndicatorsMetadata()
{
    _metadata.push_back( GroupIndicatorMetadata( tr("Онлайн"),
                                            GroupIndicator::OpStateIndicator,
                                            GroupIndicator::ID_OnlineOpCount,
                                            GroupIndicator::ID_OpCount) );

    _metadata.push_back( GroupIndicatorMetadata( tr("Пауз."),
                                            GroupIndicator::OpStateIndicator,
                                            GroupIndicator::ID_PausedOpCount,
                                            GroupIndicator::ID_OnlineOpCount ) );
    _metadata.push_back( _metadata.last() );
    _metadata.last().format = QuantativeIndicator::Format_Percents;
    _metadata.last().name += "%";

    _metadata.push_back( GroupIndicatorMetadata( tr("Своб."),
                                            GroupIndicator::OpStateIndicator,
                                            GroupIndicator::ID_FreeOpCount,
                                            GroupIndicator::ID_OnlineOpCount) );
    _metadata.last().inverted = true;
    _metadata.push_back( _metadata.last() );
    _metadata.last().format = QuantativeIndicator::Format_Percents;
    _metadata.last().name += "%";

    _metadata.push_back( GroupIndicatorMetadata( tr("Зан."),
                                            GroupIndicator::OpStateIndicator,
                                            GroupIndicator::ID_BusyOpCount,
                                            GroupIndicator::ID_OnlineOpCount ) );
    _metadata.push_back( _metadata.last() );
    _metadata.last().format = QuantativeIndicator::Format_Percents;
    _metadata.last().name += "%";

    //call state indicators
    _metadata.push_back( GroupIndicatorMetadata( tr("Вход."),
                                            GroupIndicator::CallStateIndicator,
                                            GroupIndicator::ID_IncomingCallsCount ) );

    _metadata.push_back( GroupIndicatorMetadata( tr("В оч."),
                                            GroupIndicator::CallStateIndicator,
                                            GroupIndicator::ID_QueuedCallsCount,
                                            GroupIndicator::ID_IncomingCallsCount) );
    _metadata.push_back( _metadata.last() );
    _metadata.last().format = QuantativeIndicator::Format_Percents;
    _metadata.last().name += "%";

    _metadata.push_back( GroupIndicatorMetadata( tr("Распр."),
                                            GroupIndicator::CallStateIndicator,
                                            GroupIndicator::ID_AssignedCallsCount,
                                            GroupIndicator::ID_IncomingCallsCount) );
    _metadata.push_back( _metadata.last() );
    _metadata.last().format = QuantativeIndicator::Format_Percents;
    _metadata.last().name += "%";

    _metadata.push_back( GroupIndicatorMetadata( tr("Соед."),
                                            GroupIndicator::CallStateIndicator,
                                            GroupIndicator::ID_ConnectedCallsCount,
                                            GroupIndicator::ID_IncomingCallsCount) );
    _metadata.push_back( _metadata.last() );
    _metadata.last().format = QuantativeIndicator::Format_Percents;
    _metadata.last().name += "%";
    _metadata.push_back( GroupIndicatorMetadata( tr("В оч.>SL"),
                                            GroupIndicator::CallStateIndicator,
                                            GroupIndicator::ID_SlaViolatedCallsCount,
                                            GroupIndicator::ID_IncomingCallsCount) );
    _metadata.push_back( GroupIndicatorMetadata( tr("Макс.вр.ож."),
                                            GroupIndicator::CallStateIndicator,
                                            GroupIndicator::ID_MaxWaitTime) );
    _metadata.last().format = QuantativeIndicator::Format_ShortTime;
}

QList<GroupIndicatorMetadata> GroupIndicatorManager::categoryIndicators(GroupIndicator::IndicatorCategory category) const
{
    QList<GroupIndicatorMetadata> result;

    foreach(GroupIndicatorMetadata im, _metadata)
    {
        if(im.category == category)
            result.append(im);
    }

    return result;
}

GroupIndicatorMetadata GroupIndicatorManager::find(GroupIndicator::IndicatorId id, QuantativeIndicator::Format format)
{
    foreach(const GroupIndicatorMetadata& im, _metadata)
    {
        if(im.valueId == id && im.format == format)
            return im;
    }
    return GroupIndicatorMetadata();
}

GroupIndicator* GroupIndicatorManager::createIndicator(const GroupIndicatorMetadata& im, Group group, QObject* parent)
{
    static GroupIndicator invalidIndicator(GroupIndicator::ID_Undefined,
                                           GroupIndicator::ID_Undefined,
                                           Group::invalid(),
                                           GroupIndicator::ValuesSharedPtr(), NULL);
    if(!im.isValid())
        return &invalidIndicator;

    QMap<Group,GroupIndicator::ValuesWeakPtr>& groupValues =
            im.category == GroupIndicator::OpStateIndicator
            ? _groupOperInfoValues : _groupCallInfoValues;


    GroupIndicator::ValuesSharedPtr values = groupValues.contains(group)
                                ? groupValues[group].toStrongRef()
                                : GroupIndicator::ValuesSharedPtr(new GroupIndicator::Values());
    groupValues[group] = values;
    GroupIndicator* indicator = new GroupIndicator(im.valueId, im.maxId, group, values, parent);
    indicator->setName(im.name);

    indicator->setAllowedFormats(QList<QuantativeIndicator::Format>()<<im.format);
    indicator->setFormat(im.format);
    indicator->setMinMaxValue(im.minMaxValue);
    indicator->setMaxValue(im.minMaxValue);
    indicator->setWarningValue(im.warningValue);
    indicator->setCriticalValue(im.criticalValue);
    indicator->setInverted(im.inverted);
    if(im.format == QuantativeIndicator::Format_Percents)
        indicator->setMaxValue(100);

    if(im.category == GroupIndicator::OpStateIndicator)
        connect(this,SIGNAL(groupOperInfoChanged()),indicator,SLOT(update()));
    if(im.category == GroupIndicator::CallStateIndicator)
        connect(this,SIGNAL(groupCallInfoChanged()),indicator,SLOT(update()));

    return indicator;
}

void GroupIndicatorManager::updateOpStateCount()
{
    QDateTime now = QDateTime::currentDateTime();
    if(_opStateUpdateTime.secsTo(now)<1)
    {
        _opStateUpdateTimer.setInterval(ForcedUpdateTimerInterval);
        return;
    }
    QMapIterator<Group,GroupIndicator::ValuesWeakPtr> i(_groupOperInfoValues);
    while( i.hasNext() )
    {
        i.next();
        Group group = i.key();
        GroupIndicator::ValuesSharedPtr values = i.value().toStrongRef();
        if(values.isNull()) //no indicators associated with the group
            continue;

        (*values)[GroupIndicator::ID_OpCount] = group.children().count();
        (*values)[GroupIndicator::ID_OnlineOpCount] = group.data(GroupOnlineOpCount).toInt();
        (*values)[GroupIndicator::ID_FreeOpCount]   = group.data(GroupFreeOpCount).toInt();
        (*values)[GroupIndicator::ID_PausedOpCount] = group.data(GroupPausedOpCount).toInt();
        (*values)[GroupIndicator::ID_BusyOpCount]   = group.data(GroupBusyOpCount).toInt();
    }
    _opStateUpdateTimer.setInterval(DefaultUpdateTimerInterval);
    _opStateUpdateTime = now;
    //emit groupOperInfoChanged(); test123
}

void GroupIndicatorManager::updateCallInfoIndicators()
{
    QDateTime now = QDateTime::currentDateTime();
    if(_callInfoUpdateTime.secsTo(now)<1)
    {
        _callStateUpdateTimer.setInterval(ForcedUpdateTimerInterval);
        return;
    }

    QList<Call> calls = _callsController->allCalls();
    QMapIterator<Group,GroupIndicator::ValuesWeakPtr> i(_groupCallInfoValues);
    while( i.hasNext() )
    {
        i.next();
        Group group = i.key();

        GroupIndicator::ValuesSharedPtr pValues = i.value().toStrongRef();
        if(pValues.isNull()) //no indicators associated with the group
            continue;

        int maxWaitTimeSeconds = 0;
        GroupIndicator::Values values;
        foreach(Call call, calls)
        {
            if(call.groupId() != group.contactId())
                continue;

            if(!call.isIncoming())
                continue;

            ++values[GroupIndicator::ID_IncomingCallsCount];

            int waitTimeSeconds = 0;
            if(call.isQueued())
            {
                ++values[GroupIndicator::ID_QueuedCallsCount];
                waitTimeSeconds = call.stateTime().secsTo(now);
            }

            if(call.isAssigned())
                ++values[GroupIndicator::ID_AssignedCallsCount];

            if(call.isTalking())
                ++values[GroupIndicator::ID_ConnectedCallsCount];

            if( waitTimeSeconds > 15 /*TODO SL wait time*/)
                ++values[GroupIndicator::ID_SlaViolatedCallsCount];

            maxWaitTimeSeconds = std::max(maxWaitTimeSeconds,waitTimeSeconds);
        }

        values[GroupIndicator::ID_MaxWaitTime] = maxWaitTimeSeconds;

        *pValues = values;
    }
    _callInfoUpdateTime = QDateTime::currentDateTime();
    _callStateUpdateTimer.setInterval(DefaultUpdateTimerInterval);
    emit groupCallInfoChanged();

}

bool GroupIndicatorManager::appendElement(QDomElement& parent, const GroupIndicatorMetadata& im)
{
    QString iidString = indicatorIdEnum().valueToKey(im.valueId);
    QString formatString = formatEnum().valueToKey(im.format);

    if(iidString.isEmpty() || formatString.isEmpty())
    {
        qWarning()<<"GroupIndicatorManager::appendElement() - invalid metadata";
        return false;
    }

    QDomElement el = parent.ownerDocument().createElement("groupIndicator");
    el.setAttribute("id", iidString);
    el.setAttribute("format", formatString);
    el.setAttribute("warningValue", im.warningValue);
    el.setAttribute("criticalValue", im.criticalValue);
    el.setAttribute("inverted", im.inverted);
    parent.appendChild(el);
    return true;
}

bool GroupIndicatorManager::readElemend(const QDomElement& el, GroupIndicatorMetadata* imPtr)
{
    Q_ASSERT(imPtr);
    if(el.nodeName() != "groupIndicator")
    {
        qWarning()<<"GroupIndicatorManager::readElemend()  - unexpected element"
                <<" at line "<<el.lineNumber()<<", column "<<el.columnNumber();
        return false;
    }

    QString iidString = el.attribute("id");
    QString formatString = el.attribute("format");

    if(iidString.isEmpty() || formatString.isEmpty())
    {
        qWarning()<<"GroupIndicatorManager::readElemend()  - invalid metadata"
                <<" at line "<<el.lineNumber()<<", column "<<el.columnNumber();
        return false;
    }

    GroupIndicator::IndicatorId iid = (GroupIndicator::IndicatorId)
                indicatorIdEnum().keyToValue(iidString.toAscii());
    QuantativeIndicator::Format format = (QuantativeIndicator::Format)
                formatEnum().keyToValue(formatString.toAscii());
    int warningValue = el.attribute("warningValue").toInt();
    int criticalValue = el.attribute("criticalValue").toInt();
    bool inverted = QVariant(el.attribute("inverted")).toBool();

    GroupIndicatorMetadata im = find(iid, format);
    im.warningValue = warningValue;
    im.criticalValue = criticalValue;
    im.inverted = inverted;
    if(im.isValid())
        *imPtr = im;

    return true;
}
