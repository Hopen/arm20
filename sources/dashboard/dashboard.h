#ifndef DASHBOARD_H
#define DASHBOARD_H

#include "dashboard_global.h"
#include "appcore/iappmodule.h"

#include "operwidgets/ioperwidgetsplugin.h"
#include "operwidgets/mainwindow.h"

#include "appcore/lateboundobject.h"
#include "appcore/predicate.h"
#include "appcore/operlistcontroller.h"
#include "operwidgets/callcontextmenu.h"

#include "groupindicatormanager.h"

using namespace ccappcore;
namespace KDChart {
    class Chart;
}

namespace ccappcore
{
    class QuantativeIndicator;
}

class WGStatusWidget;

class DASHBOARDSHARED_EXPORT Dashboard
        : public QObject
        , public ccappcore::IAppModule
        , public IOperWidgetsPlugin

{
    Q_OBJECT
    Q_INTERFACES(ccappcore::IAppModule IOperWidgetsPlugin  )

public:
    Dashboard();
    //virtual ~Dashboard();

    virtual void initialize() throw();

    virtual void initializeMainWindow(QMainWindow *);


    QDockWidget* addDockWidget(const QString& objectName,
                       const QString& title,
                       QWidget* w);

    //QPointer<QWidget> wgstatus()const{return _wgstatus;}

private slots:
    void addGroupIndicatorsTable();

    void queuedCallContextMenuRequested(QPoint);

private:

    KDChart::Chart* createChart();
    KDChart::Chart* createBarChart(QAbstractItemModel* model);
    KDChart::Chart* createPieChart(QAbstractItemModel* model);
    QWidget* createGroupStatGrid();
    QWidget* createOperStatusPieChart(
            const QString& displayName,
            ccappcore::Predicate<ccappcore::Operator>* filter
            );

    QTableView* createGridView(QAbstractItemModel* model);
    QWidget* createCallsList();

    QPointer<QWidget> _formDashboard;
    QPointer<CallContextMenu> _queuedCallContextMenu;

    LateBoundObject<OperListController> _operListController;

    QPointer<MainWindow>     _mainWindow;
    QPointer<QMenu>          _dashboardMenu;
    QPointer<WGStatusWidget> _wgstatus;

    LateBoundObject<PresetManager> _presetManager;
    GroupIndicatorManager _groupIndicatorsManager;
};

#endif // DASHBOARD_H
