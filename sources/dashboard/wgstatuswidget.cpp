#include "wgstatuswidget.h"
#include "groupindicatormanager.h"
#include "operwidgets/indicatoritemdelegate.h"

/******************************/
/********  class MODEL ********/
/******************************/

WGStatusModel::WGStatusModel(QObject *parent)
    :QAbstractListModel(parent)
{
    _columnsList << columnNameById(GroupColumn);

    foreach(GroupIndicatorMetadata im, _manager->categoryIndicators(GroupIndicator::CallStateIndicator))
    {
        _columnsList << im.name;
    }

    connect(_manager.instance(),SIGNAL(groupCallInfoChanged()),this,SIGNAL(layoutChanged()));

    addFilter(new IsGroupWithOper(true));
    QList<ccappcore::Group> groups = _controller->findGroups(_filter);
    foreach(const Group& group, groups)
    {
        _activeGroups.push_back(group.contactId().toInt());
    }

    clearFilter();

    reset();
}

int WGStatusModel::rowCount(const QModelIndex &parent) const
{
    return _groups.count();
}

Qt::ItemFlags WGStatusModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags defaultFlags = QAbstractListModel::flags(index);

    if (index.isValid())
        return /*Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled |*/ Qt::ItemIsUserCheckable |defaultFlags;
    else
        return /*Qt::ItemIsDropEnabled |*/ defaultFlags;
}

void WGStatusModel::applyFilter(const PredicateGroup<Contact> &filter)
{
    emit layoutAboutToBeChanged();

    _filter = filter;

    _indicatorList.clear();
    _groups.clear();
    QList<Contact>  groups = _controller->findGroups(_filter);

    foreach(const Group& group, groups)
    {
        int groupId = group.contactId().toInt();
        _groups.push_back(groupId);

        IndicatorList list;
        foreach(GroupIndicatorMetadata im, _manager->categoryIndicators(GroupIndicator::CallStateIndicator))
        {
            QuantativeIndicator* indicator = _manager->createIndicator(im, group);
            list.append(indicator);
        }
        _indicatorList[groupId] = list;

    }



    emit layoutChanged();
}

void WGStatusModel::reset()
{
    applyFilter(_filter);
    sort(0);
}

void WGStatusModel::sort(int column, Qt::SortOrder order)
{
    Q_UNUSED(column);
    Q_UNUSED(order);

    emit layoutAboutToBeChanged();
    qSort(_groups.begin(),_groups.end(),ActiveGroupSortPredicate(&_activeGroups));

    emit layoutChanged();
}

QVariant WGStatusModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (index.row() >= _groups.size())
        return QVariant();

    if(index.column() <0 || index.column() >= columnCount() )
        return QVariant();

    /*Contact c*/int groupId = _groups[index.row()];

    if (role == Qt::DisplayRole)
    {
        Contact c = _controller->findById(_controller->createGroupId(groupId));
        if(!c.isValid())
            return QVariant();


        IndicatorList list = _indicatorList[c.contactId().toInt()];

        //Contact::ContactInfo dt = (Contact::ContactInfo)_columnsList[index.column()];
//        if (dt == column_messagesCount)
//            return _spamController->messageChat(c.contactId().toInt()).size();

//        if (dt == Contact::Name)
        if (index.column() == 0)
        {
            return c.data(Contact::Name);
        }

        return list.at(index.column() - 1)->value();
    }

    if(role == Qt::DecorationRole)
    {
    }

//    if(role == Qt::TextColorRole)
//    {
//        if (_activeGroups.contains(/*c*/groupId))
//            return QColor("#DB8402");

//        return QColor("gray");
//    }

    if (role == Qt::CheckStateRole && index.column() == 0)
    {
        return _activeGroups.contains(/*c*/groupId)?Qt::Checked:Qt::Unchecked;
    }


    return QVariant();
}

bool WGStatusModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;
    if (index.row() >= _groups.size())
        return false;

    if(index.column() <0 || index.column() >= _columnsList.count() )
        return false;

    if (role == Qt::EditRole)
    {
    }
    if (role == Qt::CheckStateRole && index.column() == 0)
    {
        /*Contact*/int c = _groups[index.row()];
//        if(!c.isValid())
//            return false;

        if (_activeGroups.contains(c))
            _activeGroups.removeOne(c);
        else
            _activeGroups.push_back(c);

        //emit layoutChanged();
        this->sort(0);
    }
    return false;
}

QVariant WGStatusModel::headerData(int section, Qt::Orientation orientation,
                                         int role) const
{
       if (role != Qt::DisplayRole)
            return QVariant();

        if (orientation == Qt::Horizontal)
        {
//            if (section > 0)
//            {
//                _manager->categoryIndicators()
//            }
            //return columnNameById(_columnsList.value(section));
            return _columnsList.value(section);
        }
        else
            return QString("Row %1").arg(section);
}

QString WGStatusModel::columnNameById(const qint32 &id) const
{
    switch(id)
    {
    case GroupColumn:
        return tr("Группа");
    default:
        return tr("Неизвестно");
    }

    return QString();
}

int WGStatusModel::columnCount(const QModelIndex &parent) const
{
    return _columnsList.size();// + _manager->categoryIndicators(GroupIndicator::CallStateIndicator).size();
}

//void WGStatusModel::setOwnGroupsChecked()
//{
//    FilteredModel<Contact>::FilterCriteria filter (new IsGroupWithOper(true));
//    _activeGroups = _controller->findGroups(filter);

//    reset();
//}


/******************************/
/********  class VIEW  ********/
/******************************/



WGStatusWidget::WGStatusWidget(QWidget *parent) :
    QWidget(parent)
{
    _mainModel = new /*TestModel*/WGStatusModel(this);
    //_mainModel->setOwnGroupsChecked();
    _mainView = new QTreeView(this);
    _mainView->setRootIsDecorated(false);
    _mainView->setModel(_mainModel);
    _mainView->setSelectionBehavior(QAbstractItemView::SelectRows);
    _mainView->setItemDelegate(new IndicatorItemDelegate(_mainView));
    //_deligate = new GroupNameDelegate();
    //_mainView->setItemDelegateForColumn(0, new GroupNameDelegate());
    //connect(_mainView.data(),SIGNAL(doubleClicked(QModelIndex)),this,SLOT(on_contactSelect(QModelIndex)));

    QVBoxLayout *topLayout = new QVBoxLayout();
    topLayout->addWidget(_mainView);
    setLayout(topLayout);
}

void WGStatusWidget::reset(bool showPrivateGroup)
{
    _mainModel->clearFilter();
    _mainModel->addFilter(new IsGroupWithOper(showPrivateGroup));
    _mainModel->sort(0);
}

////

//TestModel::TestModel(QObject *parent)
//    :QAbstractListModel(parent)
//{
//    _columnsList << columnNameById(GroupColumn);

//    foreach(GroupIndicatorMetadata im, _manager->categoryIndicators(GroupIndicator::CallStateIndicator))
//    {
//        _columnsList << im.name;
//    }


//    addFilter(new IsGroupWithOper(true));
//    //_activeGroups = _controller->findGroups(_filter);
//    clearFilter();

//    reset();


//}

//int TestModel::rowCount(const QModelIndex &parent) const
//{
//    return _groups.count();
//}

//Qt::ItemFlags TestModel::flags(const QModelIndex &index) const
//{
//    Qt::ItemFlags defaultFlags = QAbstractListModel::flags(index);

//    if (index.isValid())
//        return /*Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled |*/ Qt::ItemIsUserCheckable |defaultFlags;
//    else
//        return /*Qt::ItemIsDropEnabled |*/ defaultFlags;
//}

//void TestModel::applyFilter(const PredicateGroup<Contact> &filter)
//{
//    emit layoutAboutToBeChanged();

//    _filter = filter;

//    _groups.clear();
//    QList<Contact>  groups = _controller->findGroups(_filter);

//    foreach(const Group& group, groups)
//    {
//        int groupId = group.contactId().toInt();
//        _groups.push_back(groupId);

////        IndicatorList list;
////        foreach(GroupIndicatorMetadata im, _manager->categoryIndicators(GroupIndicator::CallStateIndicator))
////        {
////            QuantativeIndicator* indicator = _manager->createIndicator(im, group);
////            list.append(indicator);
////        }
////        _indicatorList[groupId] = list;

//    }

//    emit layoutChanged();
//}

//void TestModel::reset()
//{
//    applyFilter(_filter);
//    sort(0);
//}

//void TestModel::sort(int column, Qt::SortOrder order)
//{
//    Q_UNUSED(column);
//    Q_UNUSED(order);

////    emit layoutAboutToBeChanged();
////    qSort(_groups.begin(),_groups.end(),ActiveGroupSortPredicate(&_activeGroups));

////    emit layoutChanged();
//}

//QVariant TestModel::data(const QModelIndex &index, int role) const
//{
//    if (!index.isValid())
//        return QVariant();
//    if (index.row() >= _groups.size())
//        return QVariant();

//    if(index.column() <0 || index.column() >= columnCount() )
//        return QVariant();


//    if (role == Qt::DisplayRole)
//    {
//        int groupId = _groups[index.row()];
//        Contact c = _controller->findById(_controller->createGroupId(groupId));
//        if(!c.isValid())
//            return QVariant();


////        IndicatorList list = _indicatorList[c.contactId().toInt()]; test123

////        //Contact::ContactInfo dt = (Contact::ContactInfo)_columnsList[index.column()];
//////        if (dt == column_messagesCount)
//////            return _spamController->messageChat(c.contactId().toInt()).size();

//////        if (dt == Contact::Name)
//        if (index.column() == 0)
//        {
//            return c.data(Contact::Name);
//        }
//    }

//    if(role == Qt::DecorationRole)
//    {
//    }

//    if(role == Qt::TextColorRole)
//    {
////        int groupId = _groups[index.row()];
////        if (_activeGroups.contains(/*c*/groupId))
////            return QColor("#DB8402");

//        return QColor("gray");
//    }

//    if (role == Qt::CheckStateRole/* && index.column() == 0*/)
//    {
////        int groupId = _groups[index.row()];
////        return _activeGroups.contains(/*c*/groupId)?Qt::Checked:Qt::Unchecked;

//        return Qt::Unchecked;
//    }


//    return QVariant();
//}


//QVariant TestModel::headerData(int section, Qt::Orientation orientation,
//                                         int role) const
//{
//       if (role != Qt::DisplayRole)
//            return QVariant();

//        if (orientation == Qt::Horizontal)
//        {
////            if (section > 0)
////            {
////                _manager->categoryIndicators()
////            }
//            //return columnNameById(_columnsList.value(section));
//            return _columnsList.value(section);
//        }
//        else
//            return QString("Row %1").arg(section);
//}

//QString TestModel::columnNameById(const qint32 &id) const
//{
//    switch(id)
//    {
//    case GroupColumn:
//        return tr("Группа");
//    default:
//        return tr("Неизвестно");
//    }

//    return QString();
//}

//int TestModel::columnCount(const QModelIndex &parent) const
//{
//    return _columnsList.size();// + _manager->categoryIndicators(GroupIndicator::CallStateIndicator).size();
//}

