#ifndef GROUPINDICATORS_H
#define GROUPINDICATORS_H

#include "dashboard_global.h"
#include "appcore/operlistcontroller.h"
#include "appcore/callscontroller.h"
#include "appcore/quantativeindicator.h"

using namespace ccappcore;

class GroupIndicator: public QuantativeIndicator
{
    Q_OBJECT;
    Q_ENUMS(IndicatorCategory IndicatorId);
public:
    enum IndicatorCategory
    {
        CategoryUndefined,
        CallStateIndicator,
        OpStateIndicator,
        //CallHistoryIndicator
    };

    enum IndicatorId
    {
        ID_Undefined,

        //Operator state indicators
        ID_OnlineOpCount,
        ID_FreeOpCount,
        ID_PausedOpCount,
        ID_BusyOpCount,
        ID_OpCount,

        //Call state indicators
        ID_IncomingCallsCount,
        ID_QueuedCallsCount,
        ID_AssignedCallsCount,
        ID_ConnectedCallsCount,
        ID_SlaViolatedCallsCount,
        ID_MaxWaitTime,
    };

    typedef QHash<IndicatorId,int> Values;
    typedef QSharedPointer<Values> ValuesSharedPtr;
    typedef QWeakPointer<Values>   ValuesWeakPtr;

    GroupIndicator( IndicatorId valueId,
                    IndicatorId maxId,
                    const Group& group,
                    ValuesSharedPtr values,
                    QObject* parent );

    bool isValid() const { return _valueId!=ID_Undefined; }

    explicit GroupIndicator(const Group&,
                            ValuesSharedPtr values,
                            QObject* parent = 0);

    static QString categoryName(IndicatorCategory);

    const ccappcore::Group& group() const { return _group; }
    IndicatorId indicatorId() const { return _valueId; }

    void setMinMaxValue(int v) { _minMaxValue = v; }
public slots:
    void update();
protected:
    IndicatorId         _valueId;
    IndicatorId         _maxId;
    int                 _minMaxValue;//минимальное значение максимума
    ccappcore::Group    _group;
    ValuesSharedPtr     _values;
};

#endif // GROUPINDICATORS_H
