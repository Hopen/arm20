/********************************************************************************
** Form generated from reading UI file 'groupindicatorsdialog.ui'
**
** Created: Wed 27. Mar 11:40:47 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GROUPINDICATORSDIALOG_H
#define UI_GROUPINDICATORSDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_GroupIndicatorsDialog
{
public:
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QLabel *lblGroups_2;
    QComboBox *comboBoxGroup;
    QLabel *lblGroups_3;
    QPushButton *bnAddGroup;
    QComboBox *comboBoxIndicatorType;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QLabel *lblGroups;
    QListWidget *listIndicatorType;
    QVBoxLayout *verticalLayout_2;
    QLabel *lblIndicators;
    QListWidget *listIndicators;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *GroupIndicatorsDialog)
    {
        if (GroupIndicatorsDialog->objectName().isEmpty())
            GroupIndicatorsDialog->setObjectName(QString::fromUtf8("GroupIndicatorsDialog"));
        GroupIndicatorsDialog->resize(429, 371);
        verticalLayout_3 = new QVBoxLayout(GroupIndicatorsDialog);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        groupBox = new QGroupBox(GroupIndicatorsDialog);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        lblGroups_2 = new QLabel(groupBox);
        lblGroups_2->setObjectName(QString::fromUtf8("lblGroups_2"));

        gridLayout->addWidget(lblGroups_2, 0, 0, 1, 1);

        comboBoxGroup = new QComboBox(groupBox);
        comboBoxGroup->setObjectName(QString::fromUtf8("comboBoxGroup"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(comboBoxGroup->sizePolicy().hasHeightForWidth());
        comboBoxGroup->setSizePolicy(sizePolicy);

        gridLayout->addWidget(comboBoxGroup, 0, 1, 1, 1);

        lblGroups_3 = new QLabel(groupBox);
        lblGroups_3->setObjectName(QString::fromUtf8("lblGroups_3"));

        gridLayout->addWidget(lblGroups_3, 1, 0, 1, 1);

        bnAddGroup = new QPushButton(groupBox);
        bnAddGroup->setObjectName(QString::fromUtf8("bnAddGroup"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(bnAddGroup->sizePolicy().hasHeightForWidth());
        bnAddGroup->setSizePolicy(sizePolicy1);

        gridLayout->addWidget(bnAddGroup, 1, 2, 1, 1);

        comboBoxIndicatorType = new QComboBox(groupBox);
        comboBoxIndicatorType->setObjectName(QString::fromUtf8("comboBoxIndicatorType"));
        sizePolicy.setHeightForWidth(comboBoxIndicatorType->sizePolicy().hasHeightForWidth());
        comboBoxIndicatorType->setSizePolicy(sizePolicy);

        gridLayout->addWidget(comboBoxIndicatorType, 1, 1, 1, 1);


        verticalLayout_3->addWidget(groupBox);

        groupBox_2 = new QGroupBox(GroupIndicatorsDialog);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setFlat(false);
        groupBox_2->setCheckable(false);
        horizontalLayout = new QHBoxLayout(groupBox_2);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lblGroups = new QLabel(groupBox_2);
        lblGroups->setObjectName(QString::fromUtf8("lblGroups"));

        verticalLayout->addWidget(lblGroups);

        listIndicatorType = new QListWidget(groupBox_2);
        listIndicatorType->setObjectName(QString::fromUtf8("listIndicatorType"));

        verticalLayout->addWidget(listIndicatorType);


        horizontalLayout->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        lblIndicators = new QLabel(groupBox_2);
        lblIndicators->setObjectName(QString::fromUtf8("lblIndicators"));

        verticalLayout_2->addWidget(lblIndicators);

        listIndicators = new QListWidget(groupBox_2);
        listIndicators->setObjectName(QString::fromUtf8("listIndicators"));

        verticalLayout_2->addWidget(listIndicators);


        horizontalLayout->addLayout(verticalLayout_2);


        verticalLayout_3->addWidget(groupBox_2);

        buttonBox = new QDialogButtonBox(GroupIndicatorsDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        buttonBox->setCenterButtons(true);

        verticalLayout_3->addWidget(buttonBox);


        retranslateUi(GroupIndicatorsDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), GroupIndicatorsDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), GroupIndicatorsDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(GroupIndicatorsDialog);
    } // setupUi

    void retranslateUi(QDialog *GroupIndicatorsDialog)
    {
        GroupIndicatorsDialog->setWindowTitle(QApplication::translate("GroupIndicatorsDialog", "\320\235\320\260\321\201\321\202\321\200\320\276\320\271\320\272\320\260 \321\202\320\260\320\261\320\273\320\276 \320\276\321\201\320\275\320\276\320\262\320\275\321\213\321\205 \320\277\320\276\320\272\320\260\320\267\320\260\321\202\320\265\320\273\320\265\320\271", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("GroupIndicatorsDialog", "\320\222\321\213\320\261\320\265\321\200\320\270\321\202\320\265 \320\263\321\200\321\203\320\277\320\277\321\203 \320\270 \321\202\320\270\320\277 \320\277\320\276\320\272\320\260\320\267\320\260\321\202\320\265\320\273\320\265\320\271", 0, QApplication::UnicodeUTF8));
        lblGroups_2->setText(QApplication::translate("GroupIndicatorsDialog", "\320\223\321\200\321\203\320\277\320\277\320\260", 0, QApplication::UnicodeUTF8));
        comboBoxGroup->clear();
        comboBoxGroup->insertItems(0, QStringList()
         << QApplication::translate("GroupIndicatorsDialog", "\320\241\320\265\320\272\321\200\320\265\321\202\320\260\321\200\320\270", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("GroupIndicatorsDialog", "\320\234\320\265\320\275\320\265\320\264\320\266\320\265\321\200\321\213", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("GroupIndicatorsDialog", "\320\220\320\264\320\274\320\270\320\275\320\270\321\201\321\202\321\200\320\260\321\202\320\276\321\200\321\213", 0, QApplication::UnicodeUTF8)
        );
        lblGroups_3->setText(QApplication::translate("GroupIndicatorsDialog", "\320\242\320\270\320\277", 0, QApplication::UnicodeUTF8));
        bnAddGroup->setText(QApplication::translate("GroupIndicatorsDialog", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        comboBoxIndicatorType->clear();
        comboBoxIndicatorType->insertItems(0, QStringList()
         << QApplication::translate("GroupIndicatorsDialog", "\320\236\320\277\320\265\321\200\320\260\321\202\320\276\321\200\321\213", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("GroupIndicatorsDialog", "\320\227\320\262\320\276\320\275\320\272\320\270", 0, QApplication::UnicodeUTF8)
        );
        groupBox_2->setTitle(QApplication::translate("GroupIndicatorsDialog", "\320\222\321\213\320\261\321\200\320\260\320\275\320\275\321\213\320\265 \320\277\320\276\320\272\320\260\320\267\320\260\321\202\320\265\320\273\320\270", 0, QApplication::UnicodeUTF8));
        lblGroups->setText(QApplication::translate("GroupIndicatorsDialog", "\320\230\320\274\321\217 \320\263\321\200\321\203\320\277\320\277\321\213, \321\202\320\270\320\277", 0, QApplication::UnicodeUTF8));
        lblIndicators->setText(QApplication::translate("GroupIndicatorsDialog", "\320\237\320\276\320\272\320\260\320\267\320\260\321\202\320\265\320\273\320\270", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class GroupIndicatorsDialog: public Ui_GroupIndicatorsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GROUPINDICATORSDIALOG_H
