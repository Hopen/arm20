#include "formdashboard.h"
#include <QTableView>
#include "appcore/domainobjectfilter.h"
#include "appcore/layoutmanager.h"

#include "appcore/callslistmodel.h"
#include "appcore/contactstreemodel.h"
#include "appcore/operstatuslistmodel.h"
#include "appcore/queuelengthlistmodel.h"

#include "operwidgets/callcontextmenu.h"

#include <KDChartWidget>
#include <KDChartChart>
#include <KDChartHeaderFooter>
#include <KDChartDatasetProxyModel>
#include <KDChartAbstractCoordinatePlane>
#include <KDChartPieDiagram>
#include <KDChartPieAttributes>
#include <KDChartThreeDPieAttributes>
#include <KDChartBarDiagram>
#include <KDChartThreeDBarAttributes>
#include <KDChartTextAttributes>
#include <KDChartDataValueAttributes>
#include <KDChartLegend>
#include <KDChartBackgroundAttributes>
#include <KDChartFrameAttributes>
#include <KDChartGridAttributes>
#include <KDChartMarkerAttributes>

using namespace ccappcore;
using namespace KDChart;

FormDashboard::FormDashboard(QWidget *parent)
    : QMainWindow(parent)
{
    setWindowIcon(QIcon(":/resources/icons/appicon_32x32.png"));
    setWindowTitle(tr("������ ���������� Call-o-Call"));

    QMainWindow::DockOptions dockOptions;
    dockOptions = QMainWindow::AnimatedDocks
                | QMainWindow::AllowNestedDocks
                | QMainWindow::AllowTabbedDocks
                | QMainWindow::VerticalTabs;

    setDockOptions(dockOptions);
    setDockNestingEnabled(true);

    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);


    LateBoundObject<LayoutManager> layoutManager;
    layoutManager->addWatchWidget(this);
}

FormDashboard::~FormDashboard()
{
}

void FormDashboard::showGroupStatGrid(Qt::DockWidgetArea area, Qt::Orientation orientation)
{
}

QDockWidget* FormDashboard::addOperStatusPieChart(const QString& name,
                            Predicate<ccappcore::Operator>* filter,
                            Qt::DockWidgetArea area,
                            Qt::Orientation orientation)
{

    QDockWidget* dw =addDockWidget(name, w, area, orientation);
    dw->setWindowTitle(tr("������ ����������: ")+name);
    return dw;
}

void FormDashboard::showQueueGrid(Qt::DockWidgetArea area, Qt::Orientation orientation)
{
    CallsListModel* model = new CallsListModel(this);
    QList<CallsListModel::ColDef> columns;
    columns
        <<CallsListModel::AbonPhoneColumn
        <<CallsListModel::GroupNameColumn
        <<CallsListModel::StateTimeColumn
        <<CallsListModel::PriorityColumn
        <<CallsListModel::ExtraInfoColumn;

    model->setVisibleColumns(columns);
    model->addFilter(new IsQueuedCall(false));

    QTableView* w = createGridView(model);
    w->setSelectionBehavior(QAbstractItemView::SelectRows);
    w->setSelectionMode(QAbstractItemView::SingleSelection);
    w->resizeColumnsToContents();

    w->verticalHeader()->setVisible(true);
    w->horizontalHeader()->setHighlightSections(false);

    connect(w, SIGNAL(customContextMenuRequested(QPoint)),
            this, SLOT(callContextMenuRequested(QPoint)));

    connect(w->model(), SIGNAL(dataChanged(QModelIndex,QModelIndex)),
            w, SLOT(resizeRowsToContents()));
    connect(w->model(), SIGNAL(layoutChanged()),
            w, SLOT(resizeRowsToContents()));

    w->setContextMenuPolicy(Qt::CustomContextMenu);
    addDockWidget(tr("������� �������"), w, area, orientation);
}

void FormDashboard::callContextMenuRequested(const QPoint& pt)
{
    QTableView* tableView = qobject_cast<QTableView*>(sender());
    if(!tableView)
        return;

    CallsListModel* model = qobject_cast<CallsListModel*>(tableView->model());
    if(!model)
        return;

    Call call = model->callAt(tableView->currentIndex());
    CallContactsMenu* menu = new CallContactsMenu(tableView);
    menu->setAttribute(Qt::WA_DeleteOnClose);
    menu->popup(call);
}

void FormDashboard::showQueueBarChart(Qt::DockWidgetArea area, Qt::Orientation orientation)
{
    ContactsTreeModel* model = new ContactsTreeModel(this);
    model->setShowTopLevelOnly(true);
    model->setContacts(_operListController->getGroupList());

    QList<int> columns;
    columns<<Contact::Name
           <<GroupQueueLength;

    model->setColumns(columns);

    Chart* w = createBarChart(model);

    HeaderFooter* header = new HeaderFooter(w);
    header->setText(tr("������� �� �������"));
    header->setPosition(Position::North);
    header->setType(HeaderFooter::Header);
    w->addHeaderFooter( header );

    addDockWidget(tr("������� �� �������"), w, area, orientation);
}

QDockWidget* FormDashboard::addDockWidget(const QString& title,
                                  QWidget *widget,
                                  Qt::DockWidgetArea area,
                                  Qt::Orientation orientation)
{
    QDockWidget* dw = new QDockWidget(title,this);
    dw->setObjectName( widget->objectName() );
    dw->setSizePolicy(QSizePolicy::Ignored,QSizePolicy::Ignored);
    dw->setWidget(widget);
    widget->setParent(dw);
    dw->setAllowedAreas(Qt::AllDockWidgetAreas);
    QMainWindow::addDockWidget(area, dw, orientation);
    return dw;
}




