#include "groupindicators.h"

GroupIndicator::GroupIndicator( IndicatorId valueId,
                                IndicatorId maxId,
                                const Group& group,
                                ValuesSharedPtr values,
                                QObject* parent )
    : QuantativeIndicator(parent)
    , _valueId(valueId)
    , _maxId(maxId)
    , _minMaxValue(0)
    , _group(group)
    , _values(values)
{
}

QString GroupIndicator::categoryName(IndicatorCategory category)
{
    switch(category)
    {
    case CallStateIndicator:
        return tr("Звонки");
    case OpStateIndicator:
        return tr("Операторы");
    default:
        return QString();
    }
}


void GroupIndicator::update()
{
    QDateTime now = QDateTime::currentDateTime();
    shrinkHistory(10);
    shrinkHistory( now.addSecs(-60),now);

    int newValue = (*_values)[_valueId];
    int newMaxValue = _maxId!=ID_Undefined ? (*_values)[_maxId] : _minMaxValue;

    if(format()==QuantativeIndicator::Format_Percents)
    {
        double val = (double)newValue;
        double max = (double)(newMaxValue==0?1:newMaxValue);
        int percent = val/max * 100;
        setMaxValue(100);
        setValue(now, percent);
        return;
    }
    else
    {
        setMaxValue(std::max(newMaxValue,_minMaxValue));
        setValue(now, newValue);
    }
}

