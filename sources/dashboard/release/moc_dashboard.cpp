/****************************************************************************
** Meta object code from reading C++ file 'dashboard.h'
**
** Created: Wed 27. Mar 11:41:11 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../dashboard.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dashboard.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Dashboard[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x08,
      37,   10,   10,   10, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Dashboard[] = {
    "Dashboard\0\0addGroupIndicatorsTable()\0"
    "queuedCallContextMenuRequested(QPoint)\0"
};

void Dashboard::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Dashboard *_t = static_cast<Dashboard *>(_o);
        switch (_id) {
        case 0: _t->addGroupIndicatorsTable(); break;
        case 1: _t->queuedCallContextMenuRequested((*reinterpret_cast< QPoint(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Dashboard::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Dashboard::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Dashboard,
      qt_meta_data_Dashboard, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Dashboard::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Dashboard::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Dashboard::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Dashboard))
        return static_cast<void*>(const_cast< Dashboard*>(this));
    if (!strcmp(_clname, "ccappcore::IAppModule"))
        return static_cast< ccappcore::IAppModule*>(const_cast< Dashboard*>(this));
    if (!strcmp(_clname, "IOperWidgetsPlugin"))
        return static_cast< IOperWidgetsPlugin*>(const_cast< Dashboard*>(this));
    if (!strcmp(_clname, "ru.forte-it.ccappcore.iappmodule/1.0"))
        return static_cast< ccappcore::IAppModule*>(const_cast< Dashboard*>(this));
    if (!strcmp(_clname, "ru.forte-it.calloper.ioperwidgetsplugin/1.0"))
        return static_cast< IOperWidgetsPlugin*>(const_cast< Dashboard*>(this));
    return QObject::qt_metacast(_clname);
}

int Dashboard::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
