/****************************************************************************
** Meta object code from reading C++ file 'groupindicatorgaugeview.h'
**
** Created: Wed 27. Mar 11:41:23 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../groupindicatorgaugeview.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'groupindicatorgaugeview.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_GroupIndicatorGaugeView[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       1,   14, // constructors
       0,       // flags
       0,       // signalCount

 // constructors: signature, parameters, type, tag, flags
      25,   24,   24,   24, 0x0e,

       0        // eod
};

static const char qt_meta_stringdata_GroupIndicatorGaugeView[] = {
    "GroupIndicatorGaugeView\0\0"
    "GroupIndicatorGaugeView(QObject*)\0"
};

void GroupIndicatorGaugeView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::CreateInstance) {
        switch (_id) {
        case 0: { GroupIndicatorGaugeView *_r = new GroupIndicatorGaugeView((*reinterpret_cast< QObject*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast<QObject**>(_a[0]) = _r; } break;
        }
    }
    Q_UNUSED(_o);
}

const QMetaObjectExtraData GroupIndicatorGaugeView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject GroupIndicatorGaugeView::staticMetaObject = {
    { &ccappcore::PresetWidget::staticMetaObject, qt_meta_stringdata_GroupIndicatorGaugeView,
      qt_meta_data_GroupIndicatorGaugeView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &GroupIndicatorGaugeView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *GroupIndicatorGaugeView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *GroupIndicatorGaugeView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_GroupIndicatorGaugeView))
        return static_cast<void*>(const_cast< GroupIndicatorGaugeView*>(this));
    typedef ccappcore::PresetWidget QMocSuperClass;
    return QMocSuperClass::qt_metacast(_clname);
}

int GroupIndicatorGaugeView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    typedef ccappcore::PresetWidget QMocSuperClass;
    _id = QMocSuperClass::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
