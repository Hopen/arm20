/****************************************************************************
** Meta object code from reading C++ file 'groupindicators.h'
**
** Created: Wed 27. Mar 11:41:14 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../groupindicators.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'groupindicators.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_GroupIndicator[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       2,   19, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x0a,

 // enums: name, flags, count, data
      25, 0x0,    3,   27,
      43, 0x0,   12,   33,

 // enum data: key, value
      55, uint(GroupIndicator::CategoryUndefined),
      73, uint(GroupIndicator::CallStateIndicator),
      92, uint(GroupIndicator::OpStateIndicator),
     109, uint(GroupIndicator::ID_Undefined),
     122, uint(GroupIndicator::ID_OnlineOpCount),
     139, uint(GroupIndicator::ID_FreeOpCount),
     154, uint(GroupIndicator::ID_PausedOpCount),
     171, uint(GroupIndicator::ID_BusyOpCount),
     186, uint(GroupIndicator::ID_OpCount),
     197, uint(GroupIndicator::ID_IncomingCallsCount),
     219, uint(GroupIndicator::ID_QueuedCallsCount),
     239, uint(GroupIndicator::ID_AssignedCallsCount),
     261, uint(GroupIndicator::ID_ConnectedCallsCount),
     284, uint(GroupIndicator::ID_SlaViolatedCallsCount),
     309, uint(GroupIndicator::ID_MaxWaitTime),

       0        // eod
};

static const char qt_meta_stringdata_GroupIndicator[] = {
    "GroupIndicator\0\0update()\0IndicatorCategory\0"
    "IndicatorId\0CategoryUndefined\0"
    "CallStateIndicator\0OpStateIndicator\0"
    "ID_Undefined\0ID_OnlineOpCount\0"
    "ID_FreeOpCount\0ID_PausedOpCount\0"
    "ID_BusyOpCount\0ID_OpCount\0"
    "ID_IncomingCallsCount\0ID_QueuedCallsCount\0"
    "ID_AssignedCallsCount\0ID_ConnectedCallsCount\0"
    "ID_SlaViolatedCallsCount\0ID_MaxWaitTime\0"
};

void GroupIndicator::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        GroupIndicator *_t = static_cast<GroupIndicator *>(_o);
        switch (_id) {
        case 0: _t->update(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData GroupIndicator::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject GroupIndicator::staticMetaObject = {
    { &QuantativeIndicator::staticMetaObject, qt_meta_stringdata_GroupIndicator,
      qt_meta_data_GroupIndicator, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &GroupIndicator::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *GroupIndicator::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *GroupIndicator::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_GroupIndicator))
        return static_cast<void*>(const_cast< GroupIndicator*>(this));
    return QuantativeIndicator::qt_metacast(_clname);
}

int GroupIndicator::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QuantativeIndicator::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
