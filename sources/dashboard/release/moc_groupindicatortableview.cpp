/****************************************************************************
** Meta object code from reading C++ file 'groupindicatortableview.h'
**
** Created: Wed 27. Mar 11:41:21 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../groupindicatortableview.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'groupindicatortableview.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_GroupIndicatorTableView[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       2,   24, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      29,   25,   24,   24, 0x08,
      67,   24,   24,   24, 0x08,

 // constructors: signature, parameters, type, tag, flags
     127,  120,   24,   24, 0x0e,
     161,   24,   24,   24, 0x2e,

       0        // eod
};

static const char qt_meta_stringdata_GroupIndicatorTableView[] = {
    "GroupIndicatorTableView\0\0pos\0"
    "indicatorContextMenuRequested(QPoint)\0"
    "onShowGaugeMenuItem(ccappcore::QuantativeIndicator*)\0"
    "parent\0GroupIndicatorTableView(QObject*)\0"
    "GroupIndicatorTableView()\0"
};

void GroupIndicatorTableView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::CreateInstance) {
        switch (_id) {
        case 0: { GroupIndicatorTableView *_r = new GroupIndicatorTableView((*reinterpret_cast< QObject*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast<QObject**>(_a[0]) = _r; } break;
        case 1: { GroupIndicatorTableView *_r = new GroupIndicatorTableView();
            if (_a[0]) *reinterpret_cast<QObject**>(_a[0]) = _r; } break;
        }
    } else if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        GroupIndicatorTableView *_t = static_cast<GroupIndicatorTableView *>(_o);
        switch (_id) {
        case 0: _t->indicatorContextMenuRequested((*reinterpret_cast< QPoint(*)>(_a[1]))); break;
        case 1: _t->onShowGaugeMenuItem((*reinterpret_cast< ccappcore::QuantativeIndicator*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData GroupIndicatorTableView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject GroupIndicatorTableView::staticMetaObject = {
    { &ccappcore::PresetWidget::staticMetaObject, qt_meta_stringdata_GroupIndicatorTableView,
      qt_meta_data_GroupIndicatorTableView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &GroupIndicatorTableView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *GroupIndicatorTableView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *GroupIndicatorTableView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_GroupIndicatorTableView))
        return static_cast<void*>(const_cast< GroupIndicatorTableView*>(this));
    typedef ccappcore::PresetWidget QMocSuperClass;
    return QMocSuperClass::qt_metacast(_clname);
}

int GroupIndicatorTableView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    typedef ccappcore::PresetWidget QMocSuperClass;
    _id = QMocSuperClass::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
