/****************************************************************************
** Meta object code from reading C++ file 'wgstatuswidget.h'
**
** Created: Wed 27. Mar 11:41:25 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../wgstatuswidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'wgstatuswidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_WGStatusModel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_WGStatusModel[] = {
    "WGStatusModel\0\0reset()\0"
};

void WGStatusModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        WGStatusModel *_t = static_cast<WGStatusModel *>(_o);
        switch (_id) {
        case 0: _t->reset(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData WGStatusModel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject WGStatusModel::staticMetaObject = {
    { &QAbstractListModel::staticMetaObject, qt_meta_stringdata_WGStatusModel,
      qt_meta_data_WGStatusModel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &WGStatusModel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *WGStatusModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *WGStatusModel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_WGStatusModel))
        return static_cast<void*>(const_cast< WGStatusModel*>(this));
    if (!strcmp(_clname, "FilteredModel<Contact>"))
        return static_cast< FilteredModel<Contact>*>(const_cast< WGStatusModel*>(this));
    return QAbstractListModel::qt_metacast(_clname);
}

int WGStatusModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractListModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_WGStatusWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      33,   16,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_WGStatusWidget[] = {
    "WGStatusWidget\0\0showPrivateGroup\0"
    "reset(bool)\0"
};

void WGStatusWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        WGStatusWidget *_t = static_cast<WGStatusWidget *>(_o);
        switch (_id) {
        case 0: _t->reset((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData WGStatusWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject WGStatusWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_WGStatusWidget,
      qt_meta_data_WGStatusWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &WGStatusWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *WGStatusWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *WGStatusWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_WGStatusWidget))
        return static_cast<void*>(const_cast< WGStatusWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int WGStatusWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
