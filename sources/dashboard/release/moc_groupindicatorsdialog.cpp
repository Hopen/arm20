/****************************************************************************
** Meta object code from reading C++ file 'groupindicatorsdialog.h'
**
** Created: Wed 27. Mar 11:41:19 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../groupindicatorsdialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'groupindicatorsdialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_AddGroupIndicatorsDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      26,   25,   25,   25, 0x08,
      67,   50,   25,   25, 0x08,
     142,   25,   25,   25, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_AddGroupIndicatorsDialog[] = {
    "AddGroupIndicatorsDialog\0\0"
    "on_buttonBox_accepted()\0current,previous\0"
    "on_listIndicatorType_currentItemChanged(QListWidgetItem*,QListWidgetIt"
    "em*)\0"
    "on_bnAddGroup_clicked()\0"
};

void AddGroupIndicatorsDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AddGroupIndicatorsDialog *_t = static_cast<AddGroupIndicatorsDialog *>(_o);
        switch (_id) {
        case 0: _t->on_buttonBox_accepted(); break;
        case 1: _t->on_listIndicatorType_currentItemChanged((*reinterpret_cast< QListWidgetItem*(*)>(_a[1])),(*reinterpret_cast< QListWidgetItem*(*)>(_a[2]))); break;
        case 2: _t->on_bnAddGroup_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData AddGroupIndicatorsDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject AddGroupIndicatorsDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_AddGroupIndicatorsDialog,
      qt_meta_data_AddGroupIndicatorsDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AddGroupIndicatorsDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AddGroupIndicatorsDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AddGroupIndicatorsDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AddGroupIndicatorsDialog))
        return static_cast<void*>(const_cast< AddGroupIndicatorsDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int AddGroupIndicatorsDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
