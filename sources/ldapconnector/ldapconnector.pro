# -------------------------------------------------
# Project created by QtCreator 2010-08-30T14:32:20
# -------------------------------------------------
QT -= gui
QT += network
TEMPLATE = lib
TARGET = ccplugin_ldap

contains( CONFIG, static) {
  CONFIG  += staticlib
  DEFINES += QT_NODLL
  DEFINES += QCA_STATIC
} else {
  CONFIG += plugin
}
DEFINES -= QABSTRACTSOCKET_DEBUG

DESTDIR = $$OUT_PWD/../bin
RESOURCES += 

INCLUDEPATH += $$PWD ../
LIBS += -L$$DESTDIR -lappcore

win32 { 
    LDAP_DIR = $$PWD/../thirdparty/openldap-2.4.23
    INCLUDEPATH += $$LDAP_DIR/include
    LIBS += $$LDAP_DIR/libraries/libldap_r/libldap_r.dll.a \
            $$LDAP_DIR/libraries/liblber/liblber.dll.a

    ldap_libs.path = $$DESTDIR
    ldap_libs.files = \
            $$LDAP_DIR/libraries/libldap_r/libldap_r.dll \
            $$LDAP_DIR/libraries/liblber/liblber.dll

    INSTALLS+=ldap_libs
    PRE_TARGETDEPS += install_ldap_libs
}
else {
    LDAP_DIR = $$PWD/../thirdparty/openldap-2.4.23
    INCLUDEPATH += $$LDAP_DIR/include

    #LIBS = -lldap -llber
    LIBS += $$LDAP_DIR/libraries/libldap_r/libldap_r.so \
            $$LDAP_DIR/libraries/liblber/liblber.so
}


DEFINES += LDAP_DEPRECATED
HEADERS += ldapaccessor.h \
    ldapcontactsplugin.h \
    ldaploadaction.h
SOURCES += ldapaccessor.cpp \
    ldapcontactsplugin.cpp \
    ldaploadaction.cpp
