#ifndef LDAPCONTACTSPLUGIN_H
#define LDAPCONTACTSPLUGIN_H

#include <QObject>

#include "appcore/iappmodule.h"
#include "appcore/appsettings.h"
#include "appcore/contactscontroller.h"
//#include "ldapaccessor.h"

using namespace ccappcore;
class LdapLoadAction;
class LdapContactsPlugin : public ContactsController
        , public IAppModule
        , public IAppSettingsHandler
{
    Q_OBJECT
    Q_INTERFACES(ccappcore::IAppModule ccappcore::IAppSettingsHandler)
public:
    LdapContactsPlugin();
    ~LdapContactsPlugin();


public: //Options
    const QString& server() const { return _server; }
    int port() const { return _port; }
    const QString& userDN() const { return _user; }
    const QString& password() const { return _password; }
    const QString& baseDN() const { return _baseDN; }
    const QString& filter() const { return _filter; }
    const QString& scope() const  { return _scope; }
    const QMap<QString, int>& attributesMap() const { return _attributesMap; }


    void setServer(const QString& value)    { _server = value; }
    void setPort(int value)                 { _port = value; }
    void setUserDN(const QString& value)    { _user = value; }
    void setPassword(const QString& value)  { _password = value; }
    void setBaseDN(const QString& value)    { _baseDN = value; }
    void setFilter(const QString& value)    { _filter = value; }
    void setScope(const QString& value)     { _scope = value; }

public: //ContactsController

    virtual QUrl buildContactUrl(const Contact&) const;

    virtual QFuture<Contact> loadContactsAsync();
    virtual QFuture<Contact> loadChildrenAsync(Contact parent);
    virtual QFuture<Contact> searchContactsAsync(const QString &filter);

    virtual const QList<Contact>& contacts() const { return _contacts; }

public: //IAppModule
    virtual void initialize() throw();

public: //IAppSettingsHandler

    //Apply service configuration.
    virtual void applySettings(const AppSettings&);

    //Flush service configuration
    virtual void flushSettings(AppSettings&);

private:
    QString _server;
    int _port;
    QString _user;
    QString _password;
    QString _baseDN;
    QString _filter;
    QString _searchFilter;
    QString _scope; //subtree || onelevel

    QList<Contact> _contacts;
    QMap<QString, int> _attributesMap;
    friend class LdapLoadAction;
    LdapLoadAction* _searchAction;
    QTimer _searchActionTimer;
private slots:
    void startSearchAction();
private:
    void loadContacts(QString baseDN, QString filter, int ldapScope);
};

#endif // LDAPCONTACTSPLUGIN_H
