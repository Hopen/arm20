#include <QtCore/QCoreApplication>
#include "ldapaccessor.h"

#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    LDAPAccessor ldap;
    int nRes = LDAP_SUCCESS;

    string pwd;
    cout<<"Password:";
    cin>>pwd;

#ifdef __MINGW_H
    nRes = ldap.connect("mo-dc020",389,"CN=Sergey Vasiliev,OU=Users,OU=Russia,DC=emea,DC=frs",pwd.c_str());
#else
    nRes = ldap.connect("localhost",389,"cn=admin,DC=callcenter,DC=com",pwd.c_str());
#endif

    QList<LDAPEntry> result;
    QStringList attribs;
    attribs<<"name"<<"telephoneNumber"<<"mail";

#ifdef __MINGW_H
    ldap.search("OU=Users,OU=Russia,DC=emea,DC=frs","(objectClass=organizationalPerson)",attribs,result,200,LDAP_SCOPE_ONELEVEL);
#else
    ldap.search("DC=callcenter,DC=com","(objectClass=organizationalPerson)", attribs, result,200,LDAP_SCOPE_SUBTREE);
#endif

    foreach(const LDAPEntry& entry, result)
    {
        qDebug()<<entry<<endl;
    }
    return a.exec();
}
