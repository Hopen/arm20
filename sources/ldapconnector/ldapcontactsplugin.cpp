#include "ldapcontactsplugin.h"
#include "ldapaccessor.h"

#include <QThreadPool>
#include "ldaploadaction.h"

#ifndef QT_NODLL
Q_EXPORT_PLUGIN2(pnp_ldapcontacts, LdapContactsPlugin);
#endif

using namespace ccappcore;

const char* SCOPE_SUBTREE   = "subtree";
const char* SCOPE_ONELEVEL  = "onelevel";



LdapContactsPlugin::LdapContactsPlugin()
    : _port(389)
    , _scope(SCOPE_ONELEVEL)
    , _searchAction(new LdapLoadAction(this,"","",LDAP_SCOPE_SUBTREE,NULL))
{
    _searchAction->setAutoDelete(false);
    _searchActionTimer.setSingleShot(true);
    connect(&_searchActionTimer, SIGNAL(timeout()),this, SLOT(startSearchAction()));

    _attributesMap["name"] = Contact::Name;
    _attributesMap["displayName"] = Contact::LongName;
    _attributesMap["telephoneNumber"] = Contact::Phone;
    _attributesMap["mail"] = Contact::Email;
    _attributesMap["title"] = Contact::JobPosition;
    _attributesMap["streetAddress"] = Contact::Address;
    _attributesMap["l"] = Contact::Location;
    _attributesMap["company"] = Contact::Organization;
}

LdapContactsPlugin::~LdapContactsPlugin()
{
    delete _searchAction;
}

void LdapContactsPlugin::initialize() throw()
{
    ServiceProvider::instance().registerService(this);
}

QUrl LdapContactsPlugin::buildContactUrl(const Contact& c) const
{
    if(!c.isValid())
        return QUrl();
    return QString("ldap://"+c.contactId().toString());
}

QFuture<Contact> LdapContactsPlugin::loadContactsAsync()
{
    int scope = _scope.toLower() == SCOPE_SUBTREE
                       ? LDAP_SCOPE_SUBTREE : LDAP_SCOPE_ONELEVEL;

    LdapLoadAction* a = new LdapLoadAction(this,
                   _baseDN,
                   _filter,
                   scope,
                   &LdapLoadAction::AssignContactsToController );
    QThreadPool::globalInstance()->start(a);
    return a->loadResult.future();
}

QFuture<Contact> LdapContactsPlugin::loadChildrenAsync(Contact parent)
{
    parent.setChildrenLoaded(true);
    QString parentDN = parent.contactId().toString();
    LdapLoadAction* a = new LdapLoadAction(this,
                   parentDN,
                   _filter,
                   LDAP_SCOPE_ONELEVEL,
                   &LdapLoadAction::AssignContactsToParent);
    QThreadPool::globalInstance()->start(a);
    return a->loadResult.future();
}

QFuture<Contact> LdapContactsPlugin::searchContactsAsync(const QString &filter)
{
    QWriteLocker lock(syncRoot());
    Q_UNUSED(lock);
    QString ldapFilter = _searchFilter;
    ldapFilter.replace("%SEARCH_TEXT%",filter);
    _searchAction->prepare();
    _searchAction->baseDN = _baseDN;
    _searchAction->filter = ldapFilter;        
    _searchActionTimer.start(1000);
    return _searchAction->loadResult.future();
}

void LdapContactsPlugin::startSearchAction()
{
    QReadLocker lock(syncRoot());
    Q_UNUSED(lock);
    QThreadPool::globalInstance()->start(_searchAction);
}

void LdapContactsPlugin::applySettings(const AppSettings& settings)
{
    static const char* defFilter = "(|(&(objectclass=organizationalUnit)(!(name=Disabled)))(&(objectclass=person)(!(objectclass=computer))))";
    static const char* defSearchFilter = "(&(|(objectclass=person)(objectclass=organizationalUnit))(!(objectclass=computer))(name=*%SEARCH_TEXT%*))";

    _server         = settings.getOption("ldap/server", "").toString();
    _port           = settings.getOption("ldap/port", 389).toInt();
    _user           = settings.getOption("ldap/user").toString();
    _password       = settings.getOption("ldap/password").toString();
    _baseDN         = settings.getOption("ldap/baseDN").toString();
    _filter         = settings.getOption("ldap/filter", defFilter).toString();
    _searchFilter   = settings.getOption("ldap/searchFilter", defSearchFilter).toString();
    _scope          = settings.getOption("ldap/scope", SCOPE_ONELEVEL).toString();
}

void LdapContactsPlugin::flushSettings(AppSettings& settings)
{
    settings.setUserOption("ldap/server", _server);
    settings.setUserOption("ldap/port", _port);
    settings.setUserOption("ldap/user", _user);
    settings.setUserOption("ldap/password", _password);
    settings.setUserOption("ldap/baseDN", _baseDN);
    settings.setUserOption("ldap/filter", _filter);
    settings.setUserOption("ldap/searchFilter", _searchFilter);
    settings.setUserOption("ldap/scope", _scope);
}


