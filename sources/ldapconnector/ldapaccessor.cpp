//=================================================================================
//
//  Project:	LDAPACCESS.CPP
//
//	Created:	5/1/2002
//	Author:		Nelson Fernandez
// 
//	Purpose:	This class is used to access the LDAP data
//
//
//=================================================================================
#include "ldapaccessor.h"
#include <cerrno>

LdapAccessor::LdapAccessor()
{
        _logErrors = true;
        _filter = "(|(objectClass=organizationalPerson)(objectclass=person)(objectclass=user)(objectclass=inetorgperson)(objectclass=ntuser))";
        _ldapSession = NULL;
}

LdapAccessor::~LdapAccessor()
{
        disconnect();
}

//=================================================================================
//
// Connect and bind to LDAP server
//
//=================================================================================
int LdapAccessor::connect(QString strServer, int iPort, QString szUser, QString szPassword)
{
    // disconnect if connected..
    disconnect();

    // Init the session
    _ldapSession = ldap_init(strServer.toUtf8(), iPort);
    // Success?
    if ( _ldapSession != NULL )
    {
       // Connect as version 3 if able.
        int iVer = LDAP_VERSION3;
        if (ldap_set_option(_ldapSession, LDAP_OPT_PROTOCOL_VERSION, &iVer ) == LDAP_SUCCESS )
        {
                qDebug()<<"Server is Version 3";
        }

        // Grab a few options
        ldap_get_option(_ldapSession, LDAP_OPT_PROTOCOL_VERSION, &_uiVersion);
        ldap_get_option(_ldapSession, LDAP_OPT_SIZELIMIT, &_uiMaxSearchResultSize);

        // authenticate to the LDAP server
        int iRes = ldap_simple_bind_s(_ldapSession,
                                      szUser.isEmpty() ? NULL : (const char*)szUser.toUtf8(),
                                      szPassword.isEmpty() ? NULL : (const char*)szPassword.toUtf8());
        if ( iRes != LDAP_SUCCESS )
        {
                QString strErr = tr("LDAP Bind Error: %1").arg(getLDAPErrorStr(iRes));
                reportError(strErr);
        }

        quint32 ulLDAPOpt = LDAP_DEREF_ALWAYS;
        ldap_set_option(NULL, LDAP_OPT_DEREF, &ulLDAPOpt);

        return iRes;
    }
    else
    {
        QString strErr = tr("LDAP Init Error: %1").arg(getLastErrorStr());
        reportError(strErr);
        return errno;
    }
}


//=================================================================================
//
// Disconnects from the LDAP server
//
//=================================================================================
int LdapAccessor::disconnect()
{
    if (_ldapSession)
    {
        quint32 ret = ldap_unbind(_ldapSession);
            _ldapSession = NULL;
        return ret;
    }
    else
        return TRUE;
}


int LdapAccessor::checkDN(QString baseDN)
{
    int iRet;
    QList<LdapEntry> result;
    QStringList attribs;
    iRet = search(baseDN, "(objectclass=*)", attribs, result, 1, LDAP_SCOPE_SUBTREE);

    // ignore if error was due to too many results, we only need to know if at least 1 entry exists.
    if (iRet == LDAP_SIZELIMIT_EXCEEDED)
            iRet = LDAP_SUCCESS;

    return iRet;
}


//=================================================================================
//
// Wrapper around the ldap_search_s function. This will return the results
// as a list of LDAPEntry.
//
// note; if iMaxRecords<1 then all matches will be returned.
//
// returns: number of attributes found
//
//=================================================================================
int LdapAccessor::search(QString baseDN,
                         QString filter,
                         QStringList attrFilter,
                         QList<LdapEntry>& result,
                         int iMaxRecords,
                         quint32 ulScope)
{
        quint32 ulLDAPOpt;
        quint32 ulRet;

        ulRet = ldap_set_option(_ldapSession, LDAP_OPT_REFERRALS, LDAP_OPT_OFF);

	ulLDAPOpt = LDAP_DEREF_ALWAYS;
        ulRet = ldap_set_option(_ldapSession, LDAP_OPT_DEREF, &ulLDAPOpt);

	ulLDAPOpt = LDAP_NO_LIMIT;
        ulRet = ldap_set_option(_ldapSession, LDAP_OPT_TIMELIMIT, &ulLDAPOpt);

        ulRet = ldap_set_option(_ldapSession, LDAP_OPT_SIZELIMIT, &iMaxRecords);


        // The following creates an array needed to pass
	// to ldap_search_s, when we want to limit the returned 
	// attributes.
        QVector<char*> attrsFilterBuf;
        QList<QByteArray> stringArray;
        if (attrFilter.count() > 0)
	{
            foreach(QString attr, attrFilter)
            {
                stringArray.append(attr.toUtf8());
                attrsFilterBuf.append(stringArray.last().data());
            }
            attrsFilterBuf.append(NULL);
        }


            // Processing
        quint32 ulErr = LDAP_SUCCESS;
        LDAPMessage* ldapMessage = NULL;
        ulErr = ldap_search_s( _ldapSession,
                              baseDN.toUtf8(),
                              ulScope,
                              filter.toUtf8(),
                              attrsFilterBuf.count() ? &attrsFilterBuf[0] : NULL,
                              0,
                              &ldapMessage );

	
	if ( (ulErr == LDAP_SUCCESS) || (ulErr ==LDAP_SIZELIMIT_EXCEEDED))
	{


		if (ulErr ==LDAP_SIZELIMIT_EXCEEDED)
                        qDebug()<<"LDAP search size limit exceeded.";

                ldapMessage = ldap_first_entry( _ldapSession, ldapMessage );
                while (ldapMessage != NULL)
		{
                        LdapEntry entry;
		
			BerElement *ber;

			// Grab the first attribute
                        char* ldapAttribute = ldap_first_attribute(_ldapSession, ldapMessage, &ber);
                        while (ldapAttribute != NULL)
			{				
                                char** ldapValue = ldap_get_values(_ldapSession, ldapMessage, ldapAttribute);
				// Check if more than one return value for this attribute
                                int ldapValueCount = ldap_count_values(ldapValue);
                                if ( ldapValueCount > 1)
				{
                                    // iterate the list of values
                                    for (int i=0; i< ldapValueCount; i++ )
                                    {
                                        entry.insertMulti(ldapAttribute, ldapValue[i]);
                                    }
                                }
                                else
                                {
                                    entry.insertMulti(ldapAttribute,ldapValue?*ldapValue:QString());
                                }

                                // free
                                ldap_value_free( ldapValue );
                                ldap_memfree( ldapAttribute );

                                // grab next attribute
                                ldapAttribute = ldap_next_attribute(_ldapSession, ldapMessage, ber);
			}

                        result.append(entry);
                        // determine if we are done
                        if ( result.count() >= iMaxRecords && iMaxRecords>0 )
			{
                            ldapMessage = NULL;
                            // consider it successful if we get back the requested number of records.
                            ulErr = LDAP_SUCCESS;
			}
			else
                            ldapMessage = ldap_next_entry(_ldapSession, ldapMessage );


		}
                ldap_msgfree( ldapMessage );
	}
	else
	{
                QString strErr = tr("LDAP search error: baseDN=%1, filter = %2, error=%3")
                                 .arg(baseDN).arg(filter).arg(getLastErrorStr());
                reportError(strErr);
	}

        return ulErr;
}

void LdapAccessor::reportError(QString strText)
{
        qWarning()<<"LDAPAccess Error: "<<strText;	
}

QString LdapAccessor::getLastErrorStr()
{    
    return getLDAPErrorStr(errno);
}

QString LdapAccessor::getLDAPErrorStr(int iErr)
{
        char* pcErr = ldap_err2string(iErr);
        QString strErr = pcErr;
	return strErr;
}

QString LdapAccessor::makeUserFilter(QString sFilter)
{
        QStringList slClasses;
        QString strTemp, strClass, strFilter;

        slClasses.append("inetOrgPerson");

        for (int iClass = 0; iClass < slClasses.count(); ++iClass)
	{
                strClass = slClasses.at(iClass);

                if (!strClass.isEmpty())
		{
                        strTemp = QString("(objectclass=%1)").arg(strClass);
			strFilter += strTemp;
		}

	}

	// add the OR operator
        if (!strFilter.isEmpty())
		strFilter = "(|" + strFilter + ")";
	
	return "(&"+strFilter+sFilter+")";
	
}
