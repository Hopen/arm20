#ifndef LDAPLOADACTION_H
#define LDAPLOADACTION_H

#include <QRunnable>
#include "appcore/iappmodule.h"
#include "appcore/appsettings.h"
#include "appcore/contactscontroller.h"
#include "ldapaccessor.h"

using namespace ccappcore;

class LdapContactsPlugin;
class LdapLoadAction : public QRunnable
{
public:

    typedef void (LdapLoadAction::*PostLoadFuncPtr) (const QList<Contact>& );
    LdapContactsPlugin* ldapController;
    QString baseDN;
    QString filter;
    int scope;
    PostLoadFuncPtr postLoadAction;
    LdapAccessor ldap;
    QFutureInterface<Contact> loadResult;

    LdapLoadAction(LdapContactsPlugin* ldapController,
                     QString baseDN,
                     QString filter,
                     int scope,
                     PostLoadFuncPtr postLoadAction);
    void prepare();
    void run();
    void AssignContactsToController(const QList<Contact>& loadResult);
    void AssignContactsToParent(const QList<Contact>& loadResult);
};


#endif // LDAPLOADACTION_H
