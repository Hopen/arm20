//=================================================================================
//
//  Project:	LDAPACCESS.CPP
//
//	Created:	5/1/2002
//	Author:		Nelson Fernandez
// 
//	Purpose:	This class is used to access the LDAP data
//
//
//=================================================================================
#ifndef _LDAPAccess_
#define _LDAPAccess_

#include <QtCore>
#include <ldap.h>

typedef QMultiHash<QString,QString> LdapEntry;

class LdapAccessor: public QObject
{
    Q_OBJECT;
public:

	// Constructor/Destructor
        LdapAccessor();
        ~LdapAccessor();

	// Methods
        int connect(QString strServer, int iPort, QString szUser, QString szPassword);
        int disconnect();
        int checkDN(QString baseDN);
        int search(QString baseDN,
                   QString filter,
                   QStringList slAttribs,
                   QList<LdapEntry>& result,
                   int iMaxRecords=0,
                   quint32 ulScope=LDAP_SCOPE_SUBTREE);

        QString getLastErrorStr();
        QString getLDAPErrorStr(int iErr);

        void reportError(QString strText);

private:
        QString		_baseDN;	// Base DN
        QString		_filter;	// This should be a filter to indicate a person ie: (objectClass=organizationalPerson)
        unsigned    _uiVersion;
        unsigned    _uiMaxSearchResultSize;

        bool _logErrors;

        LDAP*		_ldapSession;

        QString getAttribs(QString text);
        QString makeUserFilter(QString filter);
};


#endif
