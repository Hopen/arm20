#include "ldaploadaction.h"
#include "ldapcontactsplugin.h"

LdapLoadAction::LdapLoadAction(LdapContactsPlugin* ldapController,
                 QString baseDN,
                 QString filter,
                 int scope,
                 PostLoadFuncPtr postLoadAction)
     : ldapController(ldapController)
     , baseDN(baseDN), filter(filter), scope(scope)
     , postLoadAction(postLoadAction)
{

}

void LdapLoadAction::prepare()
{
    QFutureInterfaceBase::State newState =
            (QFutureInterfaceBase::State)(QFutureInterfaceBase::Running | QFutureInterfaceBase::Started);
    loadResult = QFutureInterface<Contact>(newState);
}

void LdapLoadAction::run()
{
    loadResult.reportStarted();
    qDebug()<<"LdapLoadAction::run("<<baseDN<<","<<filter<<","<<scope<<")";

    if(ldapController->server().isEmpty())
    {
        qDebug()<<"LDAP server is empty.";
        return;
    }

    QList<Contact> contacts;

    int nRes = ldap.connect(ldapController->server(),
                            ldapController->port(),
                            ldapController->userDN(),
                            ldapController->password());
    if(LDAP_SUCCESS!=nRes)
    {
        qWarning()<<"ldap.connect() failed: "<<ldap.getLastErrorStr();;
        loadResult.reportCanceled();
        return;
    };

    QList<LdapEntry> result;
    QStringList attribs;
    attribs<<"objectClass"<<"distinguishedName"<<"entryDN"<<"cn";
    attribs.append(ldapController->attributesMap().keys());

    ldap.search( baseDN, filter, attribs, result, 0, scope );

    foreach(const LdapEntry& entry, result)
    {
        if(loadResult.isCanceled() )
            return;
        qDebug()<<entry<<endl;

        //get contact id (distinguished name)
        QString dn;
        if(entry.contains("entryDN"))
            dn = entry.value("entryDN");
        else if(entry.contains("distinguishedName"))
            dn = entry.value("distinguishedName");
        else if(entry.contains("cn"))
            dn = "cn="+entry.value("cn") + "," + baseDN;
        else
        {
            qWarning()<<"Unable to recognize entry DN: "<<entry;
            continue;
        }

        Contact ldapContact(ldapController,dn);

        if(entry.values("objectClass").contains("organizationalUnit"))
            ldapContact.setIsContainer(true);

        foreach(QString attributeName, ldapController->attributesMap().keys())
        {
            if(entry.contains(attributeName))
            {
                int infoId = ldapController->attributesMap().value(attributeName);
                foreach(const QVariant& v, entry.values(attributeName))
                    ldapContact.addData(infoId,v);
            }
        }
        contacts.append(ldapContact);
    }

    ldap.disconnect();
    if(postLoadAction)
        (this->*postLoadAction)(contacts);

    if(contacts.count()>0)
    {
        loadResult.reportResults(contacts.toVector(),0,contacts.count());
    }
    loadResult.reportFinished();
}


void LdapLoadAction::AssignContactsToController(const QList<Contact>& loadResult)
{
    QWriteLocker lock(ldapController->syncRoot());
    Q_UNUSED(lock);
    ldapController->_contacts = loadResult;
    emit ldapController->dataChanged();
}

void LdapLoadAction::AssignContactsToParent(const QList<Contact>& loadResult)
{
    QWriteLocker lock(ldapController->syncRoot());
    Q_UNUSED(lock);
    Contact parent = ldapController->findById(ContactId(ldapController,baseDN));
    if(parent.isValid())
    {
        parent.setChildren(loadResult);
    }
    emit ldapController->dataChanged();
}
