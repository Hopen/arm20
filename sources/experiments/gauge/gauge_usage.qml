import Qt 4.7

Rectangle {
    width: 300
    height: 300

    ListModel {
        id: customScale
        ListElement { label: "";   labelValue: 0.00 }
        ListElement { label: "1";  labelValue: 0.05 }
        ListElement { label: "2";  labelValue: 0.11 }
        ListElement { label: "3";  labelValue: 0.22 }
        ListElement { label: "5";  labelValue: 0.22 }
        ListElement { label: "6";   labelValue: 0.00 }
        ListElement { label: "7";  labelValue: 0.05 }
        ListElement { label: "8";  labelValue: 0.05 }
        ListElement { label: "9";  labelValue: 0.11 }
        ListElement { label: "10";  labelValue: 0.22 }
        ListElement { label: "11";  labelValue: 0.05 }
        ListElement { label: "12";  labelValue: 0.22 }
        ListElement { label: "13";  labelValue: 0.22 }
        ListElement { label: "14";  labelValue: 0.22 }
        ListElement { label: "15";  labelValue: 0.22 }
    }

    Gauge {
        id:gauge;
        width: 200
        height: 200
        pos.y:30;
        anchors.horizontalCenter: parent.horizontalCenter;
        value: slider.x *1.57
        model: customScale
    }


    Rectangle {
           id: sliderContainer
           anchors { bottom: parent.bottom; left: parent.left
               right: parent.right; leftMargin: 20; rightMargin: 20
               bottomMargin: 10
           }
           height: 16

           Text {
               anchors.centerIn:parent;
               text:gauge.value
           }

           radius: 8
           opacity: 0.7
           smooth: true
           gradient: Gradient {
               GradientStop { position: 0.0; color: "gray" }
               GradientStop { position: 1.0; color: "white" }
           }

           Rectangle {
               id: slider
               x: 1; y: 1; width: 30; height: 14
               radius: 6
               smooth: true
               gradient: Gradient {
                   GradientStop { position: 0.0; color: "#424242" }
                   GradientStop { position: 1.0; color: "black" }
               }

               MouseArea {
                   anchors.fill: parent
                   drag.target: parent; drag.axis: Drag.XAxis
                   drag.minimumX: 2; drag.maximumX: sliderContainer.width - 32
               }
           }
       }


}
