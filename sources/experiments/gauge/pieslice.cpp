/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the documentation of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/
#include "pieslice.h"

#include <QPainter>
#include <QConicalGradient>
#include <math.h>

PieSlice::PieSlice(QDeclarativeItem *parent)
    : QDeclarativeItem(parent)
{
    // need to disable this flag to draw inside a QDeclarativeItem
    setFlag(QGraphicsItem::ItemHasNoContents, false);
}

QColor PieSlice::startColor() const
{
    return m_startColor;
}

void PieSlice::setStartColor(const QColor &color)
{
    if(color == m_startColor)
        return;
    m_startColor = color;
    emit startColorChanged(color);
}

QColor PieSlice::endColor() const
{
    return m_endColor;
}

void PieSlice::setEndColor(const QColor &color)
{
    if(m_endColor == color)
        return;

    m_endColor = color;
    emit endColorChanged(color);
}

int PieSlice::fromAngle() const
{
    return m_fromAngle;
}

void PieSlice::setFromAngle(int angle)
{
    if(angle == m_fromAngle)
        return;
    m_fromAngle = angle;
    emit fromAngleChanged(angle);
}

int PieSlice::angleSpan() const
{
    return m_angleSpan;
}

void PieSlice::setAngleSpan(int angle)
{
    if(angle == m_angleSpan)
        return;
    m_angleSpan = angle;
    emit angleSpanChanged(angle);
}

void PieSlice::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->save();
    qreal start = angleSpan()>=0 ? fromAngle() : fromAngle() + 360 + angleSpan();
    qreal end = fabs(qreal(angleSpan())/360.0);

    QColor cStart = angleSpan()>=0?startColor():endColor();
    QColor cEnd = angleSpan()>=0?endColor():startColor();

    QConicalGradient gradient(boundingRect().center(), start);
    gradient.setColorAt(0, cStart);
    gradient.setColorAt(end, cEnd);

    painter->setBrush(QBrush(gradient));
    painter->setPen(Qt::NoPen);
    painter->setRenderHints(QPainter::Antialiasing, true);
    painter->drawPie(boundingRect(), m_fromAngle * 16, m_angleSpan * 16);
    painter->restore();
}


ArcSlice::ArcSlice(QDeclarativeItem *parent)
    : PieSlice(parent)
{

}

int ArcSlice::width() const
{
    return m_width;
}

void ArcSlice::setWidth(int width)
{
    if(m_width==width)
        return;

    m_width = width;
    emit widthChanged(width);
}

void ArcSlice::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->save();
    qreal start = angleSpan()>=0 ? fromAngle() : fromAngle() + 360 + angleSpan();
    qreal end = fabs(qreal(angleSpan())/360.0);

    QColor cStart = angleSpan()>=0?startColor():endColor();
    QColor cEnd = angleSpan()>=0?endColor():startColor();

    QConicalGradient gradient(boundingRect().center(), start);
    gradient.setColorAt(0, cStart);
    gradient.setColorAt(end, cEnd);

    painter->setBrush(QBrush(gradient));
    painter->setPen(QPen(QBrush(gradient),width(),Qt::SolidLine,Qt::FlatCap));
    painter->setRenderHints(QPainter::Antialiasing, true);
    painter->drawArc(boundingRect(), fromAngle() * 16, angleSpan() * 16);
    painter->restore();
}
