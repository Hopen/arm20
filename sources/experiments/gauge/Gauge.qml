import Qt 4.7
import "lib"

Rectangle {
    id: gauge
    width: 300
    height: gauge.width
    property real value : 0
    property ListModel model: defaultScale


    ListModel {
        id: defaultScale
        ListElement { label: "0";    }
        ListElement { label: "30";  }
        ListElement { label: "60";  }
        ListElement { label: "90";  }
        ListElement { label: "120";  }
        ListElement { label: "150";  }
        ListElement { label: "180"; }
        ListElement { label: "210";  }
        ListElement { label: "240";  }
        ListElement { label: "270";  }
        ListElement { label: "300";  }
        ListElement { label: "330";  }
    }

    Image {
        anchors.fill: parent;
        id: gaugeBackground;
        source: "GaugeBackground.png"
        smooth: true;
    }

    Item {
        id: severityColors;
        anchors.centerIn: gauge

        PieChart {
               anchors.centerIn: parent
               width: gauge.width *0.48
               height: gauge.height *0.48
               slices: [
                   ArcSlice {
                       width:10
                       anchors.fill: parent
                       startColor: "green"
                       endColor: "yellow"
                       fromAngle: -120; angleSpan: -150;
                   },
                   ArcSlice {
                       width:10
                       anchors.fill: parent
                       startColor: "yellow"
                       endColor: "red"
                       fromAngle: -270; angleSpan: -150;
                   }
               ]
        }

    }

    Image {
        x:parent.width/2 - 7
        y:parent.height/2 - 65
        source: "needle_shadow.png"
        transform: Rotation {
            origin.x: 9; origin.y: 67
            angle: needleRotation.angle
        }
    }
    Image {
        id: needle
        x:parent.width/2 - 7
        y:parent.height/2 - 65
        smooth: true

        source: "needle.png"
        transform: Rotation {
            id: needleRotation
            origin.x: 5; origin.y: 65

            angle: gauge.value - 180;
            Behavior on angle {
                SpringAnimation {
                    spring: 1.4
                    damping: .15
                }
            }
        }
    }   

    Image {
        x:parent.width/2 - 81
        y:parent.height/2 - 81
        source: "overlay.png"
    }

    Component {
        id: gaugeDelegate
        Text { id: labelText; text: label }
    }

    PathView {
        id: gaugeView
        anchors.fill: parent
        interactive: true
        model: gauge.model
        delegate: gaugeDelegate

        property int paddingBottom: gauge.height / 5
        property int paddingTop: gauge.height / 5
        property int paddingLeft: gauge.width / 10
        property int paddingRight: gauge.width / 10

        path: Path {
            startX: gaugeView.width/2
            startY: gaugeView.height-gaugeView.paddingBottom

            PathCubic {
                x: gaugeView.width/2;
                y: gaugeView.paddingTop;
                control1X: gaugeView.paddingLeft; control1Y: gaugeView.height-gaugeView.paddingBottom
                control2X: gaugeView.paddingLeft; control2Y: gaugeView.paddingTop
            }
            PathCubic {
                x: gaugeView.width/2
                y: gaugeView.height-gaugeView.paddingBottom;
                control1X: gaugeView.width-gaugeView.paddingRight; control1Y: gaugeView.paddingTop
                control2X: gaugeView.width-gaugeView.paddingRight; control2Y: gaugeView.height-gaugeView.paddingBottom
            }
        }
    }
}



