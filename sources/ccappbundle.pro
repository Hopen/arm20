TEMPLATE = subdirs
SUBDIRS += \
    thirdparty \
    appcore \
    routerclient \
    launcher \
    operwidgets \
    #callsgen \
    cryptoprovider \
    ldapconnector \
    #dashboard \
    #testsapp \
    runarmscript \
    openphone
#win32: SUBDIRS+=openphone

DESTDIR = bin
CONFIG += ordered
SOURCES+=buildnotes.txt
INCLUDEPATH = $$QTDIR


