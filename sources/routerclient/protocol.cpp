#include "protocol.h"

namespace is3client
{
    QDataStream& operator>>(QDataStream& ds, CLIENT_ADDRESS& addr )
    {
            return ds>>addr.clientId>>addr.childId;
    }
    QDataStream& operator<<(QDataStream& ds, const CLIENT_ADDRESS& addr )
    {
            return ds<<addr.clientId<<addr.childId;
    }

    QDebug operator<<(QDebug out, const CLIENT_ADDRESS& addr )
    {
            return out.nospace()<<addr.clientId<<":"<<addr.childId;
    }

    QDataStream& operator>>(QDataStream& ds, PACKET_HEADER& hdr )
    {
#if (_CDSVER >= 0x0400)
            return ds>>hdr.size>>hdr.to>>hdr.from>>hdr.commandId;
#else
            return ds;
#endif
    }
    QDataStream& operator<<(QDataStream& ds, const PACKET_HEADER& hdr )
    {
#if (_CDSVER >= 0x0400)
            return ds<<hdr.size<<hdr.to<<hdr.from<<hdr.commandId;
#else
            return ds;
#endif
    }
    QDebug operator<<(QDebug out, const PACKET_HEADER& hdr )
    {
            static const char* r2rName[] = {
                            "R2R_UPDATE_ROUTE_TABLE",
                            "R2R_REGISTER_ROUTER",
                            "R2R_REGISTER_CLIENT",
                            "R2R_REGISTER_ECHO",
                            "R2R_ROUTE_ERROR",
                            "R2R_STATUS_NOTICE",
                            "R2R_SUBSCRIBE_NOTICE",
                            "R2R_SUBSCRIBE_ADD",
                            "R2R_SUBSCRIBE_REMOVE",
                            "R2R_GET_SUBSCRIBE",
                            "R2R_MONITOR_NOTICE",
                            "R2R_MONITOR_ADD",
                            "R2R_MONITOR_REMOVE",
                            "R2R_USER"		};

            QString command = hdr.commandId > 0 && ( hdr.commandId < sizeof(r2rName) / sizeof(char*) ) ? r2rName[hdr.commandId] : "R2R_USER";
            out.nospace()<<command<<"\tfrom="<<hdr.from<<"\tto="<<hdr.to<<"\tsize="<<hdr.size;

            return out.space();
    }
}
