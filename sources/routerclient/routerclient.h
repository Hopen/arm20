/*
 * RouterClient.h
 *
 *  Created on: 25.02.2009
 *      Author: root
 */

#ifndef RouterClient_H_
#define RouterClient_H_

#include "appcore/appcore.h"
#include "routerclientshared.h"
#include "protocol.h"
#include "messageserializer.h"

using namespace ccappcore;

namespace is3client {

class ROUTERCLIENT_EXPORT RouterClient
        : public IRouterClient
        , public IAppModule
        , public IAppSettingsHandler
{

       Q_OBJECT
       Q_INTERFACES(ccappcore::IAppModule ccappcore::IRouterClient ccappcore::IAppSettingsHandler)

public:

	enum RouterMessageCode
	{
            R2R_UPDATE_ROUTE_TABLE = 0,
                R2R_REGISTER_ROUTER,
                R2R_REGISTER_CLIENT,
                R2R_REGISTER_ECHO,
                R2R_ROUTE_ERROR,
                R2R_STATUS_NOTICE,
                R2R_SUBSCRIBE_NOTICE,
                R2R_SUBSCRIBE_ADD,
                R2R_SUBSCRIBE_REMOVE,
                R2R_GET_SUBSCRIBE,
                R2R_MONITOR_NOTICE,
                R2R_MONITOR_ADD,
                R2R_MONITOR_REMOVE,
                R2R_USER,
	};


private:
        QPointer<QAbstractSocket> socket;
        ClientAddress thisClientAddress;
	bool disconnectRequested;
        QMutex sendingLock;
        QString host;
        int port;
        int responseTimeoutInSeconds;
        QString lastError;
        MessageSerializer messageSerializer;
public: //IAppSettingHandler
        virtual void applySettings(const AppSettings& settings);
        virtual void flushSettings(AppSettings& settings);
public:

        RouterClient(QAbstractSocket* socket = new QTcpSocket());
        virtual ~RouterClient();

        void initialize() throw();

        //router response timout, in milliseconds
        int getResponseTimeout() { return responseTimeoutInSeconds * 1000; }
        bool isConnected() const;

        void connectToServer();
        void connectToServer(const QString& host, int port);
        void disconnectFromServer();

        void waitForConnected(int msecs = 30000);
        void waitForDisconnected(int msecs = 30000);

        const ClientAddress& clientAddress() const { return thisClientAddress; }

        void send(const Message& msg, const ClientAddress& from, const ClientAddress& to, RouterMessageCode code);
        void send(const Message& msg, const ClientAddress& to, RouterMessageCode code);
        void send(const Message& msg, const ClientAddress& from, const ClientAddress& to);
        void send(const Message& msg, const ClientAddress& to);

        void subscribeEvent(const QString& eventName);
        void unsubscribeEvent(const QString& eventName);

        QString getError() const;


private slots:

	void onConnected();
	void onDisconnected();
	void onReadyRead();
        void onError(QAbstractSocket::SocketError err);

};

}



#endif /* RouterClient_H_ */
