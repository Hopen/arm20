/****************************************************************************
** Meta object code from reading C++ file 'routerclient.h'
**
** Created: Tue 26. Mar 14:05:28 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../routerclient.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'routerclient.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_is3client__RouterClient[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      25,   24,   24,   24, 0x08,
      39,   24,   24,   24, 0x08,
      56,   24,   24,   24, 0x08,
      74,   70,   24,   24, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_is3client__RouterClient[] = {
    "is3client::RouterClient\0\0onConnected()\0"
    "onDisconnected()\0onReadyRead()\0err\0"
    "onError(QAbstractSocket::SocketError)\0"
};

void is3client::RouterClient::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        RouterClient *_t = static_cast<RouterClient *>(_o);
        switch (_id) {
        case 0: _t->onConnected(); break;
        case 1: _t->onDisconnected(); break;
        case 2: _t->onReadyRead(); break;
        case 3: _t->onError((*reinterpret_cast< QAbstractSocket::SocketError(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData is3client::RouterClient::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject is3client::RouterClient::staticMetaObject = {
    { &IRouterClient::staticMetaObject, qt_meta_stringdata_is3client__RouterClient,
      qt_meta_data_is3client__RouterClient, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &is3client::RouterClient::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *is3client::RouterClient::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *is3client::RouterClient::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_is3client__RouterClient))
        return static_cast<void*>(const_cast< RouterClient*>(this));
    if (!strcmp(_clname, "IAppModule"))
        return static_cast< IAppModule*>(const_cast< RouterClient*>(this));
    if (!strcmp(_clname, "IAppSettingsHandler"))
        return static_cast< IAppSettingsHandler*>(const_cast< RouterClient*>(this));
    if (!strcmp(_clname, "ru.forte-it.ccappcore.iappmodule/1.0"))
        return static_cast< ccappcore::IAppModule*>(const_cast< RouterClient*>(this));
    if (!strcmp(_clname, "ru.forte-it.ccappcore.irouterclient/1.0"))
        return static_cast< ccappcore::IRouterClient*>(const_cast< RouterClient*>(this));
    if (!strcmp(_clname, "ru.forte-it.ccappcore.iappsettingshandler/1.0"))
        return static_cast< ccappcore::IAppSettingsHandler*>(const_cast< RouterClient*>(this));
    return IRouterClient::qt_metacast(_clname);
}

int is3client::RouterClient::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = IRouterClient::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
