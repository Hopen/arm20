/****************************************************************************
** Meta object code from reading C++ file 'messageserializer.h'
**
** Created: Tue 26. Mar 14:05:26 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../messageserializer.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'messageserializer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_is3client__MessageSerializer[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_is3client__MessageSerializer[] = {
    "is3client::MessageSerializer\0"
};

void is3client::MessageSerializer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData is3client::MessageSerializer::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject is3client::MessageSerializer::staticMetaObject = {
    { &IMessageSerializer::staticMetaObject, qt_meta_stringdata_is3client__MessageSerializer,
      qt_meta_data_is3client__MessageSerializer, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &is3client::MessageSerializer::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *is3client::MessageSerializer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *is3client::MessageSerializer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_is3client__MessageSerializer))
        return static_cast<void*>(const_cast< MessageSerializer*>(this));
    if (!strcmp(_clname, "ru.forte-it.ccappcore.imessageserializer/1.0"))
        return static_cast< ccappcore::IMessageSerializer*>(const_cast< MessageSerializer*>(this));
    return IMessageSerializer::qt_metacast(_clname);
}

int is3client::MessageSerializer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = IMessageSerializer::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
