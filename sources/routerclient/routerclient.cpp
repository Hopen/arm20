#include "routerclient.h"
#include "messageserializer.h"
#include "appcore/lateboundobject.h"
#include "appcore/appsettings.h"
#include "../thirdparty/cds3/consts.h"

namespace is3client {

#ifndef QT_NODLL
Q_EXPORT_PLUGIN2(pnp_routerclient, RouterClient);
#endif

const int DefaultResponseTimeoutInSeconds = 15;

RouterClient::RouterClient(QAbstractSocket* socket)
    : socket(socket)
    , host("127.0.0.1")
    , port(50000)
    , responseTimeoutInSeconds(DefaultResponseTimeoutInSeconds)
{
        connect(this->socket, SIGNAL(connected()), this, SLOT(onConnected()) );
        connect(this->socket, SIGNAL(disconnected()), this, SLOT(onDisconnected()) );
        connect(this->socket, SIGNAL(readyRead()), this, SLOT(onReadyRead()) );
        connect(this->socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onError(QAbstractSocket::SocketError)));
}

RouterClient::~RouterClient() {
}

void RouterClient::initialize() throw()
{
    qDebug()<<"Initializing IRouterClient...";
    ServiceProvider::instance().registerService( this );
    ServiceProvider::instance().registerService( &messageSerializer );

    LateBoundObject<AppSettings> settings;
    applySettings(*settings);
    flushSettings(*settings);
}

void RouterClient::connectToServer()
{
    if(isConnected())
    {
        emit connected();
        return;
    }
    qDebug()<<"Connecting to router at "<<host<<":"<<port;
    disconnectRequested = false;
    socket->connectToHost(host, port);
}

bool RouterClient::isConnected() const
{
    return socket->state() == QAbstractSocket::ConnectedState && clientAddress().isValid();
}

void RouterClient::connectToServer(const QString& host, int port)
{
    this->host = host;
    this->port = port;
    connectToServer();
}

void RouterClient::waitForConnected(int msecs /*= 30000*/)
{
    QTime startTime = QTime::currentTime();
    socket->waitForConnected(msecs);
    if(socket->state() != QAbstractSocket::ConnectedState)
        return;

    while( !isConnected() && (startTime.msecsTo(QTime::currentTime()) < msecs) )
        qApp->processEvents(QEventLoop::WaitForMoreEvents);
}

void RouterClient::waitForDisconnected(int msecs /*= 30000*/)
{

    socket->waitForDisconnected(msecs);
}

void RouterClient::applySettings(const AppSettings& settings)
{
    host = settings.getOption("router/hostName", "127.0.0.1").toString();
    port = settings.getOption("router/port", 50000).toInt();
    responseTimeoutInSeconds = settings.getOption("router/responseTimeout", DefaultResponseTimeoutInSeconds).toInt();
}

void RouterClient::flushSettings(AppSettings& settings)
{
    settings.setAppOption("router/hostName", host );
    settings.setAppOption("router/port", port );
    settings.setAppOption("router/responseTimeout", responseTimeoutInSeconds );
}

void RouterClient::disconnectFromServer()
{
	disconnectRequested = true;
        socket->disconnectFromHost();
}

void RouterClient::onConnected()
{
#if (_CDSVER >= 0x0400)
        qDebug()<<"Connected to router at "<<host<<":"<<port;
        Message msg;
        msg["client_id"] = 0;
        send(msg, ClientAddress::All(), ClientAddress::All(), R2R_REGISTER_CLIENT);
#else
        emit connected();
#endif
}
void RouterClient::onDisconnected()
{    
        if(disconnectRequested) {
            qDebug()<<"Router disconnected. Reason = ClientDisconnected";
            emit disconnected(RouterClient::ClientDisconnected);
        } else {
             qDebug()<<"Router disconnected. Reason = RouterDisconnected";
            emit disconnected(RouterClient::RouterDisconnected);
        }        
}
void RouterClient::onReadyRead()
{
    PACKET_HEADER hdr;
    while(true)
    {
        socket->peek((char*)&hdr.size, sizeof(hdr.size));

        if (socket->bytesAvailable()<hdr.size)
                return;

        QDataStream ds(socket);
        ds.setByteOrder(QDataStream::LittleEndian);

        Message msg;

//        QFile file("c:\\response.dat");
//        file.open(QIODevice::WriteOnly);
//        QDataStream fs(&file);
//        ds >> fs;
//        file.close();

        ds>>hdr>>msg;

        if(ds.status()==QDataStream::ReadPastEnd)
        {
            qDebug()<<"is3client::RouterClient::onReadyRead() - Read past end after message deserialization.";
            return;
        }

        if(ds.status()==QDataStream::ReadCorruptData)
        {
            qDebug()<<"is3client::RouterClient::onReadyRead() - Read corrupt data after message deserialization.";
            return;
        }

        qDebug().nospace()<<"RECV\t"<<hdr<<msg;

        switch((RouterMessageCode)hdr.commandId)
        {
                case R2R_REGISTER_ECHO:
                {
                        thisClientAddress = msg["client_id"];
                        qDebug()<<"Router has assigned client id = 0x"<<hex<<thisClientAddress.clientId();
                        emit connected();
                }	break;
                case R2R_STATUS_NOTICE:
                {
                        qint32 status = msg["status"];
                        quint32 clientId = msg["client_id"];
                        emit statusChanged(clientId,(ClientStatus) status);
                }	break;
                case R2R_SUBSCRIBE_NOTICE:
                {
                        quint32 eventHash = msg["event"];
                        quint32 subscribersCount = msg["count"];
                        quint32 consumers = msg["consumers"];
                        quint32 status = msg["status"];
                        QByteArray who = msg["who"];
                        QVector<quint32> subscribers(subscribersCount);
                        for(quint32 i = 0; i < subscribersCount; i++)
                                subscribers.append( ((quint32*)who.data())[i] );

                        emit sibscribeNoticeReceived(eventHash,(EventSubscriptionStatus) status, consumers, subscribers);
                }
                break;
                case R2R_MONITOR_NOTICE:
                {
                        emit monitorNoticeReceived(msg);
                }
                case R2R_ROUTE_ERROR:
                {
                        qWarning()<<"ROUTE_ERROR: "<<msg.name();
                        emit routeError(msg);
                }
                break;
                default:
                {
                        emit messageReceived(msg,
                                             ClientAddress(hdr.from.clientId, hdr.from.childId),
                                             ClientAddress(hdr.to.clientId, hdr.to.childId));
                }
                break;
        }
    }
}
void RouterClient::onError(QAbstractSocket::SocketError err)
{
    Q_UNUSED(err);
    lastError = socket->errorString();
    qWarning()<<"Router error. Socket error ("<<lastError<<").";
    socket->close();
    emit error(lastError);
}


void RouterClient::send(const Message& msg, const ClientAddress& from, const ClientAddress& to, RouterMessageCode command)
{

#if (_CDSVER >= 0x0400)
#else
//        qint32 cmd = CmdConverter::GetInstance()->GetCmdByString(msg.name().toStdString());
//        msg.add("cmd",cmd);
        msg.addCmd();
#endif

        Q_ASSERT(socket->state() == QAbstractSocket::ConnectedState);
        if( socket->state() != QAbstractSocket::ConnectedState )
            return;

        QMutexLocker lock(&sendingLock);
        Q_UNUSED(lock);

        emit messageSending(msg, from, to);

        PACKET_HEADER hdr;

        hdr.from.clientId = from.clientId();
        hdr.from.childId = from.childId();
        hdr.to.clientId = to.clientId();
        hdr.to.childId = to.childId();

	hdr.commandId = command;
        hdr.size = sizeof(PACKET_HEADER)+MessageSerializer::messageLength(msg);

        qDebug().nospace()<<"SEND\t"<<hdr<<msg;

        //QFile file("c:\\3.dat");
        //file.open(QIODevice::WriteOnly);
        //QDataStream ds(&file);
        QDataStream ds(socket);
        ds.setByteOrder(QDataStream::LittleEndian);
        //ds << qint32(24);  // size of message
        //ds << qint32(1);   // key count
        //ds << qint32(3);
        //ds << qint8('c');        // length of name
        //ds << qint8('m');
        //ds << qint8('d');
        //ds << qint8(1);    // type value
        //ds << qint32(1000);// value
        //ds << quint32(0); // flags
        ds<<hdr<<msg;
        //file.close();

        socket->flush();

        emit messageSent(msg, from, to);
}

void RouterClient::send(const Message& msg, const ClientAddress& from, const ClientAddress& to)
{
        quint32 hashCode = msg.getHashCode();
	RouterMessageCode code = hashCode < R2R_USER ? R2R_USER: (RouterMessageCode) hashCode;
        send(msg, from, to, code);
}

void RouterClient::send(const Message& msg, const ClientAddress& to, RouterMessageCode code)
{
        send(msg, thisClientAddress, to, code);
}

void RouterClient::send(const Message& msg, const ClientAddress& to)
{
        send(msg, thisClientAddress, to);
}

void RouterClient::subscribeEvent(const QString& eventName)
{
#if (_CDSVER >= 0x0400)
    Message msg(eventName);
    msg["event"] = msg.getHashCode();
    send(msg,ClientAddress::All(),(RouterMessageCode) RouterClient::R2R_SUBSCRIBE_ADD);
#else
    Message msg("O2C_SUBSCRIBE_CMD");
    //Message msg2(eventName);
    //msg2.addCmd();
    msg["subscr_cmd"] = Message(eventName).addCmd();//msg2["cmd"].value().toInt();
    send(msg, ClientAddress::Any());
#endif
}

void RouterClient::unsubscribeEvent(const QString& eventName)
{
#if (_CDSVER >= 0x0400)
    Q_UNUSED(eventName);
  //  Message msg(eventName);
  //  msg["event"] = msg.getHashCode();
  //  send(msg,ClientAddress::All(),(RouterMessageCode) RouterClient::R2R_SUBSCRIBE_REMOVE);
#else
    Message msg("O2C_UNSUBSCRIBE_CMD");
    msg["subscr_cmd"] = Message(eventName).addCmd();
    send(msg, ClientAddress::Any());
#endif
}

QString RouterClient::getError() const
{
    return lastError;
}

}


