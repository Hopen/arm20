/*
 * protocol.h
 *
 *  Created on: 22.02.2009
 *      Author: root
 */

#ifndef PROTOCOL_H_
#define PROTOCOL_H_

#include "routerclientshared.h"
#include "variant.h"

#pragma pack(push,8)

namespace is3client
{
	#define	MSG_SIGNATURE		0x34DF7A6B994DACFFULL
	#define	MAX_NAME_BUFFER_LEN		64

        struct PARAM_HEADER
	{
		quint32		size;				// Total param size with header (in bytes)
		char		name[64];	// Param name
		quint32 	rawDataSize;				// Raw data size (if any)
                quint32		rawDataPtr;					// Raw data ptr (if any)
                quint32         pack1;  // align
		VARIANT  	value;						// Variant value (if not raw data)
                //char		data[2];					// Actually, param data
                quint64         pack2;  // align
	};

	struct MESSAGE_HEADER
	{
		quint64	signature;				// Unique message signature for data validation
		quint32		size;				// Total message size with header (in bytes)
		quint32		messageId;					// Message ID = for send mode
		char		name[MAX_NAME_BUFFER_LEN];// Message Name
		quint32		paramCount;				// Number of params
		char		data[1];					// Actually, param data
	};

	#pragma pack(pop)

	#pragma pack (push, 1)

	struct CLIENT_ADDRESS
	{
		quint32	clientId;
		quint32	childId;		
                CLIENT_ADDRESS(): clientId(0), childId(0)
                {

                }
	};

	struct PACKET_HEADER
	{
		quint32			size;
		CLIENT_ADDRESS	to;
		CLIENT_ADDRESS	from;
		quint32			commandId;
		char			data[0];
                PACKET_HEADER(): commandId(1025), size(0)
                {

                }
	};

#pragma pack (pop)

	QDataStream& operator>>(QDataStream& ds, CLIENT_ADDRESS& addr );
	QDataStream& operator<<(QDataStream& ds, const CLIENT_ADDRESS& addr );
	QDebug operator<<(QDebug out, const CLIENT_ADDRESS& addr );

	QDataStream& operator>>(QDataStream& ds, PACKET_HEADER& hdr );
	QDataStream& operator<<(QDataStream& ds, const PACKET_HEADER& hdr );
	QDebug operator<<(QDebug out, const PACKET_HEADER& hdr );
}

#endif /* PROTOCOL_H_ */
