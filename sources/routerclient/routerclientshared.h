#ifndef ROUTERCLIENT_H
#define ROUTERCLIENT_H

#include <QtCore>
#include <QtNetwork/QtNetwork>
#include <qdebug.h>

#include "appcore/iappmodule.h"
#include "appcore/irouterclient.h"
#include "appcore/appsettings.h"

#ifndef QT_NODLL
#  ifdef ROUTERCLIENT_PACKAGE
#    define ROUTERCLIENT_EXPORT Q_DECL_EXPORT
#  else
#    define ROUTERCLIENT_EXPORT Q_DECL_IMPORT
#  endif
#else
#  define ROUTERCLIENT_EXPORT
#endif


#endif // ROUTERCLIENT_H
