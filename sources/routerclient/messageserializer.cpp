#include "messageserializer.h"
#include "../thirdparty/cds3/consts.h"

namespace is3client
{
    quint32 MessageSerializer::messageLength(const Message& msg)
    {
            quint32 len = sizeof(MESSAGE_HEADER);
            for(quint32 i = 0; i < msg.paramsCount(); i++)
            {
                    len += paramLength(msg[i]);
            }

            return len;
    }

    void MessageSerializer::save(QDataStream& stream, const Message& message)
    {
#if (_CDSVER >= 0x0400)
        stream.setByteOrder(QDataStream::LittleEndian);
        stream << (quint64)MSG_SIGNATURE;
        stream << messageLength(message);
        stream << (quint32) 0; //message id

        QByteArray nameBuffer(MAX_NAME_BUFFER_LEN,'\0');
        nameBuffer.replace(0,message.name().length(),message.name().toAscii());

        stream.writeRawData(nameBuffer,MAX_NAME_BUFFER_LEN);

        stream << (quint32) message.paramsCount();

        for(uint i=0; i < message.paramsCount(); i++)
        {
                stream << message[i];
        }

        stream << (quint32)0;
#else
        quint32 size = (sizeof(quint32) + message.size());
        stream << size;
        stream << quint32(message.paramsCount()); // keys quantity
        for(uint i=0; i < message.paramsCount(); i++)
        {
            //!! operator << of QDataArray already first of all write the size of it
            // stream << quint32(message[i].name().length()); // length of each
            stream << message[i].name().toAscii();         // name
            stream << message[i];                          // value
        }

#endif
    }

    Message MessageSerializer::load(QDataStream& stream)
    {
#if (_CDSVER >= 0x0400)
        stream.setByteOrder(QDataStream::LittleEndian);
        MESSAGE_HEADER hdr = {0, 0, 0, {0}, 0, {0} };

        stream>>hdr.signature;
        stream>>hdr.size;
        stream>>hdr.messageId;
        stream.readRawData(hdr.name,MAX_NAME_BUFFER_LEN);
        stream>>hdr.paramCount;

        QList<MessageParam> params;
        for(quint32 i=0; i< hdr.paramCount; i++)
        {
                MessageParam p("");
                stream>>p;
                params.append(p);
        }

        quint32 alignment;
        stream>>alignment;

        if(stream.status()!=QDataStream::Ok)
                return Message();

        Message message(hdr.name);
        message.setParams(params);

        if(hdr.signature!=MSG_SIGNATURE)
            qWarning()<<"is3client::MessageSerializer::load() - Invalid message signature.";

        return message;
#else
        stream.setByteOrder(QDataStream::LittleEndian);
        quint32 message_size = 0;
        stream >> message_size;
        quint32 params_count = 0;
        stream >> params_count;

        QList<MessageParam> params;
        for(quint32 i=0; i< params_count; i++)
        {
//            QString param_name;
//            quint32 param_size;
//            stream >> param_size;
//            stream.readRawData(param_name,param_size);
            MessageParam p("");
            stream >> p;
            params.append(p);
        }

        Message message;
        message.setParams(params);

        if (const MessageParam* pCmd = message.find("cmd"))
        {
            message.setName(GetStringByCmd(pCmd->value().toInt()));
        }

        return message;

#endif
    }

    void MessageSerializer::saveParam(QDataStream& stream, const MessageParam& param)
    {
#if (_CDSVER >= 0x0400)
        stream.setByteOrder(QDataStream::LittleEndian);
        QByteArray nameBuffer = param.name().toAscii();
        nameBuffer.resize(MAX_NAME_BUFFER_LEN);

        quint32 paramLen = paramLength(param);
        quint32 rawDataLen = param.value().type() == QVariant::ByteArray ?
                             param.value().toByteArray().count() : 0;
        stream << paramLen;
        stream.writeRawData(nameBuffer,MAX_NAME_BUFFER_LEN);
        stream << (quint32) rawDataLen; //Raw data size
        stream << (quint32) 0; //Raw data pointer
        stream << (quint32) 0; //Pack bytes
        saveVariant(stream, param.value());
        stream << (quint64) 0; // Pack bytes;
#else
        switch (param.value().type())
        {
        case QVariant::UInt:
        case QVariant::Int:
            stream << qint8(MessageParam::cds_int);
            stream << qint32(param.value().toInt());
            //stream << sizeof(qint32);
            break;
        case QVariant::ULongLong:
        case QVariant::LongLong:
            stream << qint8(MessageParam::cds_int64);
            stream << qint64(param.value().toLongLong());
            //stream << sizeof(qint64);
            break;
        case QVariant::Double:
            stream << qint8(MessageParam::cds_double);
            stream << qreal(param.value().toDouble());
            //stream << sizeof(qreal);
                break;
        case QVariant::String:
        {
            stream << qint8(MessageParam::cds_string);
            //stream << qint32(param.value().toString().length());
            QByteArray data;
            quint32 bufferLen = param.value().toString().length();//*2+2;
            stream << qint32(bufferLen);
            data.append( /*(const char*) */param.value().toString()/*.utf16()*//*, bufferLen -2*/);
            //data.append('\0').append('\0');
            //data[bufferLen-1] = data[bufferLen-2] = '\0';
            stream.writeRawData(data,data.length());
            break;
        }
        case QVariant::ByteArray:
        {
            stream << qint8(MessageParam::cds_binary);
            QByteArray data;
            data = param.value().toByteArray();
            stream << qint32(data.length());
            stream.writeRawData(data,data.length());
            break;
        }
        default:
                {
                    qWarning()<<"is3client::MessageSerializer::saveVariant()"
                            <<" - unsupported variant type:"<< param.value();
                    //stream << char(MessageParam::cds_unknown);
                    break;
                }
        }

        //stream << quint32(0);// flags - outdate -> see cds_proto.h
        stream <<  quint32(param.flag()); // doesn't outedate -> see save_list and CDS_TABLE::CDS_TABLE(const CDS_VALUE& value)

#endif
    }

    MessageParam MessageSerializer::loadParam(QDataStream& stream)
    {
        stream.setByteOrder(QDataStream::LittleEndian);
#if (_CDSVER >= 0x0400)
        quint32 pack1;
        quint64	pack2;

        PARAM_HEADER hdr;
        stream>>hdr.size;
        stream.readRawData(hdr.name, MAX_NAME_BUFFER_LEN);

        stream>>hdr.rawDataSize;
        stream>>hdr.rawDataPtr;
        stream>>pack1;
        QVariant value = loadVariant(stream, hdr.rawDataSize > 0
                                     ? hdr.rawDataSize
                                     : hdr.size - sizeof(PARAM_HEADER));
        stream>>pack2;

        MessageParam param(hdr.name);
        param.setValue(value);
        return param;
#else

        qint32 param_size;
        stream >> param_size;
        QByteArray buffer(param_size,'\0');
        stream.readRawData(buffer.data_ptr()->data,param_size);
        QString param_name(buffer);

        MessageParam param;//(param_name);
        param.setName(param_name);

        quint8 param_type = MessageParam::cds_unknown;
        stream >> param_type;

        switch (param_type)
        {
        case MessageParam::cds_int:
        {
            qint32 value = 0;
            stream >> value;
            param.setValue(value);
            break;
        }
        case MessageParam::cds_double:
        {
            qreal value = 0;
            stream >> value;
            param.setValue(value);
            break;
        }
        case MessageParam::cds_int64:
        {
            qint64 value = 0;
            stream >> value;
            param.setValue(value);
            break;
        }
        case MessageParam::cds_string:
        {
            quint32 param_value_size;
            stream >> param_value_size;
            QByteArray buffer(param_value_size,'\0');
            stream.readRawData(buffer.data_ptr()->data,param_value_size);
            QString param_value(buffer);
            param.setValue(param_value);
            break;
        }
        case MessageParam::cds_binary:
        {
            QByteArray data;
            quint32 data_size;
            stream >> data_size;
            data.resize(data_size);
            stream.readRawData(data.data_ptr()->data,data_size);
            param.setValue(data);
            break;
        }

        default:
                {
                    qWarning()<<"is3client::MessageSerializer::loadParam()"
                            <<" - unsupported variant type:"<< param_type;
                    //stream << char(MessageParam::cds_unknown);
                    break;
                }

        }

        quint32 flags = 0;
        stream >> flags;

        return param;

#endif
     }

    quint32 MessageSerializer::paramLength(const MessageParam& param)
    {
            quint32 len = sizeof(PARAM_HEADER);
            switch( param.value().type() )
            {
                    case QVariant::ByteArray:
                            len += param.value().toByteArray().length();
                            break;

                    case QVariant::String:
//see CMessageParam::Pack - (::SysStringLen(m_Val.bstrVal) + 1) * sizeof(OLECHAR)
                            len += param.value().toString().length() * 2 + 2;
                            break;
                    default:
                            break;//no special handling
            }

            return len;
    }

    void MessageSerializer::saveVariant(QDataStream& stream, const QVariant& var)
    {
            stream.setByteOrder(QDataStream::LittleEndian);

            VARIANT v = {0, 0, 0, 0, {0} };

            bool ok = true;

            QByteArray data;
            switch(var.type())
            {
                    case QVariant::UInt:
                            v.vt =VT_UI4;
                            v.val.ui4 = var.toUInt(&ok);
                            break;
                    case QVariant::Int:
                            v.vt =VT_I4;
                            v.val.i4 = var.toInt(&ok);
                            break;
                    case QVariant::ULongLong:
                            v.vt =VT_UI8;
                            v.val.ui8 = var.toULongLong(&ok);
                            break;
                    case QVariant::LongLong:
                            v.vt = VT_I8;
                            v.val.i8 = var.toLongLong(&ok);
                            break;
                    case QVariant::Double:
                            v.vt = VT_R8;
                            v.val.r8 = var.toDouble(&ok);
                            break;
                    case QVariant::String:
                    {
                            v.vt = VT_BSTR;
                            quint32 bufferLen = var.toString().length()*2+2;
                            data.append( (const char*) var.toString().utf16(), bufferLen -2);
                            //data.append('\0').append('\0');
                            data[bufferLen-1] = data[bufferLen-2] = '\0';
                            break;
                    }
                    case QVariant::ByteArray:
                            v.vt = VT_EMPTY;
                            data = var.toByteArray();
                            break;
                    default:
                            {
                                qWarning()<<"is3client::MessageSerializer::saveVariant()"
                                        <<" - unsupported variant type:"<<var;
                                break;
                            }
            }

            stream<<v.vt;
            stream<<v.reserved1<<v.reserved2<<v.reserved3;
            stream<<v.val.ui8;
            if(data.length()>0)
                    stream.writeRawData(data,data.length());
    }

    QVariant MessageSerializer::loadVariant(QDataStream& stream, quint32 dataLength)
    {
        stream.setByteOrder(QDataStream::LittleEndian);
        QVariant var;

        VARIANT v;

        stream >> v.vt;
        stream >> v.reserved1 >> v.reserved2 >> v.reserved3;
        stream >> v.val.ui8;

        switch(v.vt)
        {
                case VT_I4:
                        var = (qint32) v.val.i4;
                        break;
                case VT_UI4:
                        var = (quint32) v.val.ui4;
                        break;
                case VT_I8:
                        var = (qint64) v.val.i8;
                        break;
                case VT_UI8:
                        var = (quint64) v.val.ui8;
                        break;
                case VT_R4:
                        var = (double) v.val.r4;
                        break;
                case VT_R8:
                        var = (double) v.val.r8;
                        break;
                case VT_BSTR:
                {
                        if(dataLength>0)
                        {
                        QByteArray buffer(dataLength,'\0');
                        stream.readRawData(buffer.data_ptr()->data,dataLength);
                        var = QString::fromUtf16( (const ushort*) buffer.data_ptr()->data);
                        }
                        break;
                }
                case VT_EMPTY:
                case VT_NULL:
                {
                        if(dataLength>0)
                        {
                                QByteArray buffer;
                                buffer.resize(dataLength);
                                stream.readRawData(buffer.data_ptr()->data, dataLength );
                                var = buffer;
                        }
                        break;
                }
                default:
                        {
                            qWarning()<<"is3client::MessageSerializer::loadVariant()"
                                    <<" - unsupported variant type:"<<v.vt;
                            break;
                        }
        }
        return var;
    }

    void MessageSerializer::saveList(QByteArray& array, const QList<Message>& list)
    {
        QBuffer buffer(&array);
        buffer.open(QIODevice::ReadWrite);

        QDataStream stream(&buffer);
        stream.setByteOrder(QDataStream::LittleEndian);

        // colums
        const Message& msg = list.first();
        stream << msg.paramsCount();
        QList<MessageParam> param_list = msg.params();
        foreach(const MessageParam& param, param_list)
        {
            stream << param.name().toAscii(); // name length + name
            stream << qint32(0);//colIndex
        }

        // rows
        stream << list.size();
        foreach(const Message& msg, list)
        {
            QList<MessageParam> param_list = msg.params();
            stream << msg.paramsCount();
            foreach (const MessageParam& param, param_list)
            {
                stream << param.name().toAscii();
                switch (param.value().type())
                {
                case QVariant::Int:
                {
                    stream << static_cast<qint8>(MessageParam::cds_int);
                    stream << static_cast<qint32>(param.value().toInt());
                    break;
                }
                case QVariant::String:
                {
                    stream << static_cast<qint8>(MessageParam::cds_string);
//                    stream << param.value().toString().length();
//                    stream << param.value().toString().toAscii();
                    stream << qint32(param.value().toString().length());
                    QByteArray data;
                    data.append( param.value().toString());
                    stream.writeRawData(data,data.length());
                    break;
                }
                case QVariant::ByteArray:
                {
                    stream << qint8(MessageParam::cds_binary);
                    QByteArray data;
                    data = param.value().toByteArray();
                    stream << qint32(data.length());
                    stream.writeRawData(data,data.length());
                    break;
                }
                case QVariant::Double:
                {
                    stream << static_cast<qint8>(MessageParam::cds_double);
                    stream << static_cast<double>(param.value().toDouble());
                    break;
                }
                default:
                {
                    qCritical()<<QString("Unexpected datatype %1, name = '%2'")
                           .arg(param.value().type()).arg(param.name()).toAscii();

                }
                }
                stream << qint32();// empty number
            }
        }

        buffer.close();
        stream.unsetDevice();
    }

    //this is a temporary implementation created by analyzing
    //received message binary data
    QList<Message> MessageSerializer::loadList(const QByteArray& data)
    {

//        enum CDS_DATA_TYPE {
//          cds_unknown,
//          cds_int,
//          cds_double,
//          cds_datetime = cds_double,
//          cds_binary,
//          cds_string,
//          cds_memo
//        };

        QList<Message> result;
        QDataStream stream(data);
        stream.setByteOrder(QDataStream::LittleEndian);

        qint32 colsCount = 0;
        qint32 rowsCount =0;
        qint32 nameSize = 0;
        char nameBuf[MAX_NAME_BUFFER_LEN] = {};

        //read column names
        stream>>colsCount;
        QStringList columns;
        for(int i=0;i<colsCount;i++)
        {
            stream>>nameSize;
            stream.readRawData(nameBuf,nameSize);
            nameBuf[nameSize]=0;
            columns.push_back(QLatin1String(nameBuf));
            quint32 colIndex = 0;
            stream>>colIndex;
        }

        //read rows
        stream>>rowsCount;
        for(int row = 0;row<rowsCount; row++)
        {
            Message rowData;
            stream>>colsCount;
            for(int col = 0; col<colsCount; col++)
            {
                //each column contains name, read it
                stream>>nameSize;
                stream.readRawData(nameBuf,nameSize);
                nameBuf[nameSize]=0;
                MessageParam colData(nameBuf);
                quint8 dataType = 0;
                stream>>dataType;

                switch(dataType)
                {
                    case MessageParam::cds_int:  //value is int32
                    {
                        quint32 value;
                        stream>>value;
                        colData.setValue(value);
                    } break;
                    case MessageParam::cds_double:  //value is int32
                    {
                        double value;
                        stream>>value;
                        colData.setValue(value);
                    } break;
                    case MessageParam::cds_string: //value is ascii string
                    {
                        quint32 valueSize;
                        stream>>valueSize;
                        std::string valueString(valueSize,0);
                        stream.readRawData(&*valueString.begin(),valueSize);
                        colData.setValue(QString::fromAscii(valueString.c_str()));
                    } break;
                case MessageParam::cds_binary:
                {
                    quint32 valueSize;
                    stream>>valueSize;
                    QByteArray valueArray(valueSize,0);
                    stream.readRawData(&*valueArray.begin(),valueSize);
                    colData.setValue(valueArray);
                    break;
                }
                    default:
                    {
                    qCritical()<<QString("Unexpected datatype %1, name = '%2'")
                           .arg(dataType).arg(nameBuf).toAscii();
                    } break;
                }
                quint32 emptyNumber = 0;
                stream>>emptyNumber;
                rowData[colData.name().toAscii()] = colData;
            }
            result.append(rowData);
        }
        return result;
    }

}
