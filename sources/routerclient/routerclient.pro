TEMPLATE = lib
TARGET = ccplugin_routerclient
DESTDIR = $$OUT_PWD/../bin
QT += \
    core \
    network

contains( CONFIG, static) {
  CONFIG  += staticlib
  DEFINES += QT_NODLL
  DEFINES += QCA_STATIC
} else {
  CONFIG += plugin
}

DEFINES -= QABSTRACTSOCKET_DEBUG

INCLUDEPATH += ../ \
        ../thirdparty/cds3
LIBS = -L$$DESTDIR -lappcore
DEFINES += ROUTERCLIENT_PACKAGE
HEADERS += routerclientshared.h \
    variant.h \
    protocol.h \
    messageserializer.h \
    routerclient.h
SOURCES += protocol.cpp \
    messageserializer.cpp \
    routerclient.cpp \
    ../thirdparty/cds3/consts.cpp
FORMS += 
RESOURCES += 
