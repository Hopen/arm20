#ifndef MessageSerializer_H
#define MessageSerializer_H

#include <QList>
#include "appcore/imessageserializer.h"

#include "routerclientshared.h"
#include "protocol.h"

using namespace ccappcore;

namespace is3client
{    
    class ROUTERCLIENT_EXPORT MessageSerializer:
            public IMessageSerializer
    {
        Q_OBJECT;
        Q_INTERFACES(ccappcore::IMessageSerializer)
        public:
        static quint32 messageLength( const Message& message );
        static quint32 paramLength(const MessageParam& param);

        void save(QDataStream& ds, const Message& msg);
        Message load(QDataStream& ds);

        void saveParam(QDataStream& ds, const MessageParam& msg);
        MessageParam loadParam(QDataStream& ds);

        void saveList(QByteArray& data, const QList<Message>& msg);
        QList<Message> loadList(const QByteArray& data);

        void saveVariant(QDataStream& stream, const QVariant& var);
        QVariant loadVariant(QDataStream& stream, quint32 dataLength);

    };

    inline QDataStream& operator<<(QDataStream& ds, const Message& m)
    {
        MessageSerializer().save(ds, m);
        return ds;
    }

    inline QDataStream& operator>>(QDataStream& ds, Message& m)
    {
        m = MessageSerializer().load(ds);
        return ds;
    }

    inline QDataStream& operator<<(QDataStream& ds, const MessageParam& param)
    {
        MessageSerializer().saveParam(ds, param);
        return ds;
    }

    inline QDataStream& operator>>(QDataStream& ds, MessageParam& param)
    {
        param = MessageSerializer().loadParam(ds);
        return ds;
    }
}

#endif // MessageSerializer_H
