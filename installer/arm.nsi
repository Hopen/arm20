;------------------------------------------------------------------------------
; Name     : arm.nsi                                                          ;
; Author   : Andrey Alekseev                                                  ;
; Company  : Forte-CT                                                         ;
; Date     : 16 Aug 2013                                                      ;
;------------------------------------------------------------------------------

!ifdef VER_MAJOR & VER_MINOR
  !define /ifndef VER_REVISION 0
  !define /ifndef VER_BUILD 0
!endif

!define /ifndef VERSION 'anonymous-build'

; The name of the installer
Name "��� ���������"

;--------------------------------
;Configuration


!ifdef OUTFILE
  OutFile "${OUTFILE}"
!else
  OutFile ..\arm-${VERSION}-setup.exe
!endif

Unicode true
SetCompressor /SOLID lzma

; The default installation directory
;InstallDir $PROGRAMFILES\Forte-IT\ARM_2.0
InstallDir C:\ARM_2.0

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\FORTE-IT\ARM_2.0" "Install_Dir"

; Request application privileges for Windows Vista
RequestExecutionLevel admin


Caption "��������� ��� ��������� ${VERSION}"


;--------------------------------
;Header Files

!include "MUI2.nsh"
!include "Sections.nsh"
!include "LogicLib.nsh"
!include "Memento.nsh"
!include "WordFunc.nsh"
!include "EnvVarUpdate.nsh"
!include "TextFunc.nsh"	


;Interface Settings
!define MUI_ABORTWARNING

!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP "${NSISDIR}\Contrib\Graphics\Header\orange.bmp"
;!define MUI_HEADERIMAGE_BITMAP_NOSTRETCH  
;!define MUI_WELCOMEFINISHPAGE_BITMAP " ${NSISDIR}\Contrib\Graphics\Wizard\nsis.bmp"
!define MUI_WELCOMEFINISHPAGE_BITMAP "D:\Projects\new\trunk\installer\logo3.bmp" 

!define MUI_COMPONENTSPAGE_SMALLDESC

;Pages
!define MUI_WELCOMEPAGE_TITLE "������ ��������� ��� ��������� ${VERSION}"
!define MUI_WELCOMEPAGE_TEXT "$\r$\n��� ��������� ��������� ��� ��������� ${VERSION} �� ��� ���������.$\r$\n$\r$\n����� ������� ��������� ������������� ������� ��� ���������� ���������.$\r$\n$\r$\n������� ������ '�����' ��� �����������."

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "D:\Projects\new\trunk\installer\License.txt"
;!ifdef VER_MAJOR & VER_MINOR & VER_REVISION & VER_BUILD
;Page custom PageReinstall PageLeaveReinstall
;!endif
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES

!define MUI_FINISHPAGE_LINK "finish page link"
!define MUI_FINISHPAGE_LINK_LOCATION "http://forte-it.ru/"

;!define MUI_FINISHPAGE_RUN "$INSTDIR\calloper.exe"
!define MUI_FINISHPAGE_NOREBOOTSUPPORT
;!define MUI_FINISHPAGE_TEXT_REBOOT "��� ���������� ��������� � ���� ���������� ������������� ���������"
;!define MUI_FINISHPAGE_TEXT_REBOOTNOW "������������� ������"
;!define MUI_FINISHPAGE_TEXT_REBOOTLATER "������������� �����"

;!define MUI_FINISHPAGE_SHOWREADME
;!define MUI_FINISHPAGE_SHOWREADME_TEXT "Show release notes"
;!define MUI_FINISHPAGE_SHOWREADME_FUNCTION ShowReleaseNotes

!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
;--------------------------------

;Languages

!insertmacro MUI_LANGUAGE "Russian"

;--------------------------------
;Version information

!ifdef VER_MAJOR & VER_MINOR & VER_REVISION & VER_BUILD
VIProductVersion ${VER_MAJOR}.${VER_MINOR}.${VER_REVISION}.${VER_BUILD}
VIAddVersionKey "FileVersion" "${VERSION}"
VIAddVersionKey "FileDescription" "Forte-IT ARM Operator"
VIAddVersionKey "LegalCopyright" "http://forte-it.ru/License"
!endif


;--------------------------------
;Installer Functions

Function .onInit

  !insertmacro MUI_LANGDLL_DISPLAY

FunctionEnd


;--------------------------------

; The stuff to install
Section "ARM (required)"

  SectionIn RO
  
  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  ; Put file there
  File "d:\share\arm_4.8.3_static_release\*.*"

  SetOutPath $INSTDIR\bin
  File "d:\share\arm_4.8.3_static_release\bin\*.*"
  SetOutPath $INSTDIR\bus
  File "d:\share\arm_4.8.3_static_release\bus\*.*"
  SetOutPath $INSTDIR\OpenPhone
  File "d:\share\arm_4.8.3_static_release\OpenPhone\*.*"
  SetOutPath $INSTDIR

  ; Create and write to file runscript.bat
  FileOpen $4 "$INSTDIR\runarmscript.bat" w
  FileWrite $4 `start runarmscript.exe "$INSTDIR\call.js" %1 %2`
  FileClose $4

  ; Create and write to file xtransfer.bat
  FileOpen $4 "$INSTDIR\xtransfer.bat" w
  FileWrite $4 `start runarmscript.exe "$INSTDIR\xferto.js" %1 %2`
  FileClose $4

;  ; Create and write to file runscriptcalltoaction.reg
;  FileOpen $4 "$INSTDIR\runscriptcalltoaction.reg" w
;  FileWrite $4 `REGEDIT4 $\r$\n$\r$\n`
;  FileWrite $4 `[HKEY_CLASSES_ROOT\callto\shell\open\command] $\r$\n`
;  FileWrite $4 `@="\"$INSTDIR\runarmscript.bat\" \"%l\"" $\r$\n$\r$\n`
;  FileWrite $4 `[HKEY_CLASSES_ROOT\xferto\shell\open\command] $\r$\n`
;  FileWrite $4 `@="\"$INSTDIR\xtransfer.bat\" \"%l\""`
;  FileClose $4

;  ; Create and write to file regcalltoaction.bat
;  FileOpen $4 "$INSTDIR\regcalltoaction.bat" w  
;  FileWrite $4 `if exist "$INSTDIR\backupcalltoaction.reg" goto exit $\r$\n`
;  FileWrite $4 `regedit.exe -ea "$INSTDIR\backupcalltoaction.reg" "HKEY_CLASSES_ROOT\callto\shell\open\command"$\r$\n`
;  FileWrite $4 `:exit $\r$\n`
;  FileWrite $4 `regedit.exe -s "$INSTDIR\runscriptcalltoaction.reg"`
;  FileClose $4

;  ; Run and register
;  ;ExecWait "$INSTDIR\regcalltoaction.bat"
  nsExec::Exec 'regedit.exe -s $INSTDIR\OpenPhone\openphone.reg'
  SetOutPath $INSTDIR

  
  ; Write the installation path into the registry
  WriteRegStr HKCR "callto\shell\open\command" "" '"$INSTDIR\runarmscript.bat" "%l"'
  WriteRegStr HKCR "xferto\shell\open\command" "" '"$INSTDIR\xtransfer.bat" "%l"'
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FORTE-IT\ARM_2.0" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteUninstaller "$INSTDIR\uninstall.exe"

  ; Update enviroment PATH
  ${EnvVarUpdate} $0 "PATH" "A" "HKLM" "$INSTDIR" 
  ${EnvVarUpdate} $0 "PATH" "A" "HKLM" "$INSTDIR\bin" 
  ${EnvVarUpdate} $0 "PATH" "A" "HKLM" "$INSTDIR\bus" 

  SetRebootFlag true
  
SectionEnd

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

  CreateDirectory "$SMPROGRAMS\FORTE-IT\ARM_2.0"
  CreateShortCut "$SMPROGRAMS\FORTE-IT\ARM_2.0\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
  CreateShortCut "$SMPROGRAMS\FORTE-IT\ARM_2.0\calloper.lnk" "$INSTDIR\calloper.exe"
  CreateShortCut "$DESKTOP\calloper.lnk" "$INSTDIR\calloper.exe"
  
SectionEnd

;Function .onInstSuccess
; HideWindow
; MessageBox MB_YESNO|MB_ICONQUESTION "��� ���������� ��������� � ���� ���������� ������������� ���������. ��������� ������?" IDNO +2
; Reboot
;FunctionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FORTE-IT\ARM_2.0"

  DeleteRegKey HKCR "callto\shell\open\command"
  DeleteRegKey HKCR "xferto\shell\open\command"

  DeleteRegKey HKCR "Software\Vox Lucida\Open Phone"



;  ; Create and write to file unregcalltoaction.bat
;  FileOpen $4 "$INSTDIR\unregcalltoaction.bat" w  
;  FileWrite $4 `if exist "$INSTDIR\backupcalltoaction.reg"`
;  FileWrite $4 `regedit.exe -s "$INSTDIR\backupcalltoaction.reg"`
;  FileClose $4


;  ; Run and unregister
;  ;ExecWait "$INSTDIR\unregcalltoaction.bat"
;  nsExec::Exec '$INSTDIR\unregcalltoaction.bat'

;  nsExec::Exec 'regedit.exe -s $INSTDIR\OpenPhone\openphone.reg'

  ; Remove files and uninstaller
  Delete $INSTDIR\*.*
  Delete $INSTDIR\bus\*.*
  Delete $INSTDIR\bin\*.*
  Delete $INSTDIR\OpenPhone\*.*
  


  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\FORTE-IT\ARM_2.0\*.*"
  Delete "$DESKTOP\calloper.lnk"

  ; Remove directories used
  RMDir "$SMPROGRAMS\FORTE-IT\ARM_2.0"
  RMDir "$INSTDIR\bus"
  RMDir "$INSTDIR\bin"
  RMDir "$INSTDIR\OpenPhone"
  RMDir "$INSTDIR"

  ; Update enviroment PATH
  ${un.EnvVarUpdate} $0 "PATH" "R" "HKLM" "$INSTDIR\bus" 
  ${un.EnvVarUpdate} $0 "PATH" "R" "HKLM" "$INSTDIR\bin" 
  ${un.EnvVarUpdate} $0 "PATH" "R" "HKLM" "$INSTDIR" 


SectionEnd

;--------------------------------
;Uninstaller Functions

Function un.onInit

  !insertmacro MUI_UNGETLANGUAGE
  
FunctionEnd